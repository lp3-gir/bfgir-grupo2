/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ruben
 */
public class CodigoPostalTest {



    /**
     * Teste para retirar um código postal.
     */
    @Test
    public void testGetCodPostal() {
        System.out.println("getCodPostal");
        CodigoPostal instance = new CodigoPostal();
        instance.setCodPostal("4415-900");
        assertEquals("4415-900", instance.getCodPostal());

    }

    /**
     * Teste para definir um código postal.
     */
    @Test
    public void testSetCodPostal() {
        System.out.println("setCodPostal");
        CodigoPostal instance = new CodigoPostal();
        instance.setCodPostal("4415-900");

    }

    /**
     * Teste para retirar um código postal.
     */
    @Test
    public void testGetCidade() {
        System.out.println("getCidade");
        CodigoPostal instance = new CodigoPostal();
        instance.setCidade("Teste");
        assertEquals("Teste", instance.getCidade());

    }

    /**
     * Teste para definir uma cidade para um código postal.
     */
    @Test
    public void testSetCidade() {
        System.out.println("setCidade");
        CodigoPostal instance = new CodigoPostal();
        instance.setCidade("Teste");

    }

}
