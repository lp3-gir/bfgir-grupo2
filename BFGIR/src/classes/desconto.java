/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import bfgir.BDConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe codigoPostal com métodos de acesso a tabela de codigoPostalna BD.
 *
 */
public class desconto {

    private int idDesconto;
    private float quantDesconto;

    /**
     * Construtor da classe desconto.
     */
    public desconto() {
    }

    /**
     * Construtor da classe desconto.<br>
     *
     * @param idDesconto id do desconto.
     * @param quantDesconto quantidade de desconto em percentagem.
     */
    public desconto(int idDesconto, float quantDesconto) {
        this.idDesconto = idDesconto;
        this.quantDesconto = quantDesconto;
    }

    /**
     * Método get idDesconto.<br>
     *
     * @return the idDesconto.
     */
    public int getIdDesconto() {
        return idDesconto;
    }

    /**
     * Método set idDesconto.<br>
     *
     * @param idDesconto the idDesconto to set
     */
    public void setIdDesconto(int idDesconto) {
        this.idDesconto = idDesconto;
    }

    /**
     * Método get quantDesconto.<br>
     * @return the quantDesconto.
     */
    public float getQuantDesconto() {
        return quantDesconto;
    }

    /**
     * Método set quantDesconto.<br>
     * @param quantDesconto the quantDesconto to set.
     */
    public void setQuantDesconto(float quantDesconto) {
        this.quantDesconto = quantDesconto;
    }

    /**
     * O método recebe um desconto a ser introduzido na BD.<br>
     *
     * @param desconto recebe um desconto.
     */
    public static void inserirDescontos(float desconto) {

        String sql = "INSERT INTO descontos (quantDesconto) VALUES (?)";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setFloat(1, desconto);

            pstmt.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

    }

    /**
     *
     * O método retorna uma lista de descontos a listar.<br>
     *
     * @return uma lista de descontos.
     */
    public static List<desconto> getListaDescontos() {

        List<desconto> listaDescontos = new ArrayList();
        String sql = "Select * from descontos";

        PreparedStatement stmt = null;
        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                desconto des = new desconto();
                des.setIdDesconto(results.getInt(1));
                des.setQuantDesconto(results.getFloat(2));
                listaDescontos.add(des);
            }
        } catch (SQLException ex) {
            Logger.getLogger(desconto.class.getName()).log(Level.SEVERE, null, ex);
        }

        return listaDescontos;

    }

    /**
     * O método vai retornar um desconto escolhido pelo utilizador.<br>
     *
     * @param idDesc recebe o id de desconto a pesquisar.
     * @return um desconto.
     */
    public static List<desconto> getListaDescontosID(int idDesc) {

        List<desconto> listaDescontos = new ArrayList();
        String sql = "Select * from descontos where idDesconto = (?)";

        PreparedStatement stmt = null;
        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setInt(1, idDesc);
            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                desconto des = new desconto();
                des.setIdDesconto(results.getInt(1));
                des.setQuantDesconto(results.getFloat(2));
                listaDescontos.add(des);
            }
        } catch (SQLException ex) {
            Logger.getLogger(desconto.class.getName()).log(Level.SEVERE, null, ex);
        }

        return listaDescontos;

    }

    /**
     * O método vai retornar um desconto escolhido pelo utilizador.<br>
     *
     * @param quantDesconto recebe a quantidade de desconto a pesquisar.
     * @return um desconto.
     */
    public static List<desconto> getListaDescontosValor(float quantDesconto) {

        List<desconto> listaDescontos = new ArrayList();
        String sql = "Select * from descontos where quantDesconto = (?)";

        PreparedStatement stmt = null;
        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setFloat(1, quantDesconto);
            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                desconto des = new desconto();
                des.setIdDesconto(results.getInt(1));
                des.setQuantDesconto(results.getFloat(2));
                listaDescontos.add(des);
            }
        } catch (SQLException ex) {
            Logger.getLogger(desconto.class.getName()).log(Level.SEVERE, null, ex);
        }

        return listaDescontos;

    }

    /**
     * O método recebe um id de desconto a atualizar com a nova quantidade de
     * desconto.<br>
     *
     * @param desconto recebe um desconto.
     * @param idDesc recebe o id de desconto a atualizar.
     */
    public static void atualizarDesconto(int idDesc, float desconto) {

        String sql = "update descontos set quantDesconto = (?) where idDesconto = (?)";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setInt(1, idDesc);
            pstmt.setFloat(2, desconto);

            pstmt.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

    }

    /**
     * O método recebe um id de desconto a eliminar.<br>
     *
     * @param idDesc recebe o id de desconto a eliminar.
     */
    public static void eliminarDes(int idDesc) {

        String sql = "delete from descontos where idDesconto = (?)";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setInt(1, idDesc);

            pstmt.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

    }

}
