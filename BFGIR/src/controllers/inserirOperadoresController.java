/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import bfgir.inserirOperadores;
import bfgir.login;
import bfgir.menuAdministrador;
import static classes.PasswordHelper.generatePassword;
import static classes.email.enviarEmail;
import static classes.email.isValid;
import classes.passwordUtils;
import classes.utilizador;
import static classes.utilizador.inserirUtilizadores;
import static classes.utilizador.procurarDadosLogin;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

/**
 * FXML Controller class
 */
public class inserirOperadoresController implements Initializable {

    @FXML
    private Label lbEmail;
    @FXML
    private TextArea email;
    @FXML
    private Button inserirOperador;
    @FXML
    private MenuBar menu;
    Alert a = new Alert(Alert.AlertType.NONE);

    /**
     * Initializes the controller class.
     *
     * @param url url
     * @param rb rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    /**
     * Metodo que executa varias verificações para ver se o utilizador 
     * pode ser registado na base de dados, se todas as validaçôes forem validas 
     * o utilizador é registado na BD<br>
     * 
     * @param event evento
     */
    @FXML
    private void inserirOperador(ActionEvent event) {

        if ("".equals(email.getText())) {
            a.setAlertType(Alert.AlertType.ERROR);
            a.setTitle("Operador");
            a.setHeaderText("Erro ao inserir operador!");
            a.setContentText("Falta de dados para poder registar o operador!");
            a.setResizable(false);
            Optional<ButtonType> result2 = a.showAndWait();
        } else if (!isValid(email.getText())) {
            a.setAlertType(Alert.AlertType.ERROR);
            a.setTitle("Operador");
            a.setHeaderText("Email com formato incorreto!");
            a.setContentText("Introduza um email com formato correto para poder efetuar o registo do operador!");
            a.setResizable(false);
            Optional<ButtonType> result2 = a.showAndWait();

        } else {
            String myPassword = generatePassword(20);
            // Generate Salt. The generated value can be stored in DB.
            String salt = passwordUtils.getSalt(30);
            // Protect user's password. The generated value can be stored in DB.
            String mySecurePassword = passwordUtils.generateSecurePassword(myPassword, salt);
            utilizador user = new utilizador();
            user.setEmail(email.getText());
            user.setPassword(mySecurePassword);
            user.setSalt(salt);
            user.setEstado("ativo");
            user.setNivel(1);
            List<utilizador> utilizadores = procurarDadosLogin(email.getText());
            int count = 0;
            for (utilizador uti : utilizadores) {
                count = 1;
            }
            if (count != 0) {
                a.setAlertType(Alert.AlertType.INFORMATION);
                a.setTitle("Operador");
                a.setHeaderText("Email já registado!");
                a.setContentText("Email inserido já se encontra registado!");
                a.setResizable(false);
                Optional<ButtonType> result2 = a.showAndWait();
            } else {
                enviarEmail(email.getText(), myPassword, "");
                inserirUtilizadores(user);

                a.setAlertType(Alert.AlertType.INFORMATION);
                a.setTitle("Operador");
                a.setHeaderText("Inserido com sucesso!");
                a.setContentText("Operador inserido com sucesso!");
                a.setResizable(false);
                Optional<ButtonType> result2 = a.showAndWait();
            }
        }
    }

    /**
     * Metodo para terminar sessao do utilizador<br>
     * 
     * @param event evento
     */
    @FXML
    private void terminarSessao(ActionEvent event) {
        try {
            login.setIdLogin(0);
            login.setNivelLogin(10000);
            login log = new login();
            fecha();
            log.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(inserirOperadoresController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Método para encerrar a aplicação
     */
    @FXML
    private void sair(ActionEvent event) {
        System.exit(0);

    }

    /**
     * Método para aceder ao menu do administrador
     */
    @FXML
    private void openMenu(ActionEvent event) {
        try {
            menuAdministrador menuAdmin = new menuAdministrador();
            fecha();
            menuAdmin.start(new Stage());

        } catch (Exception ex) {
            Logger.getLogger(inserirOperadoresController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * método para fechar o ecrã de inserir operadores
     */
    private void fecha() {
        inserirOperadores.getStage().close();
    }

}
