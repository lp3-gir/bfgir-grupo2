/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ruben
 */
public class DescontosTest {
    


    /**
     * Teste para retirar o id de um desconto.
     */
    @Test
    public void testGetIdDesconto() {
        System.out.println("getIdDesconto");
        Descontos instance = new Descontos();
        instance.setIdDesconto(1);
        Integer expResult = 1;
        assertEquals(expResult, instance.getIdDesconto());
        
    }

    /**
     * Teste para definir o id para um desconto.
     */
    @Test
    public void testSetIdDesconto() {
        System.out.println("setIdDesconto");
        Descontos instance = new Descontos();
        instance.setIdDesconto(1);
        
    }

    /**
     * Teste para retirar a quantidade de desconto em %.
     */
    @Test
    public void testGetQuantDesconto() {
        System.out.println("getQuantDesconto");
        Descontos instance = new Descontos();
        instance.setQuantDesconto(10);
        assertEquals(10, instance.getQuantDesconto(), 0.0);
        
    }

    /**
     * Teste para definir a taxa de desconto em %.
     */
    @Test
    public void testSetQuantDesconto() {
        System.out.println("setQuantDesconto");
        Descontos instance = new Descontos();
        instance.setQuantDesconto(10);
        
    }
    
}
