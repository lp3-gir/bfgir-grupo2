/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bfgir;

import classes.helper;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * classe do ecrã listarOperadores
 */
public class listarDescontos extends Application {

    private static Stage stage;

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/ecras/listarDescontos.fxml"));

        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.setTitle("F&O - Listar Descontos");
        stage.getIcons().add(helper.appIcon);
        stage.setResizable(false);
        stage.show();
        setStage(stage);

    }

    /**
     * método main do ecrã listarOperadores
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * método getStage do ecrã listarOperadores
     *
     * @return retorna o JAVAFX container do ecrã listarOperadores
     */
    public static Stage getStage() {
        return stage;
    }

    /**
     * método setStage do ecrã listarOperadores
     *
     * @param aStage the aStage to set
     */
    public static void setStage(Stage aStage) {
        stage = aStage;
    }

}
