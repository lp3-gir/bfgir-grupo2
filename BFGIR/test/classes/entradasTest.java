/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import bfgir.BDConnection;
import static classes.entradas.inserirEntradas;
import static classes.entradas.verifEnt;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 */
public class entradasTest {

    private static final entradas ent = new entradas();

    /**
     * construtor da classe entradas teste
     */
    public entradasTest() {
    }

    /**
     * set up class
     */
    @BeforeClass
    public static void setUpClass() {
        ent.setIdEntrada("RC0000");
        ent.setIdFornecedor("111111");
        ent.setDataEntrada("2000/01/01");
        ent.setEstado("pendente");

        inserirEntradas(ent);
    }

    /**
     * tear down class
     */
    @AfterClass
    public static void tearDownClass() {
        String sql = "Delete FROM entomenda where idEntrada = 'RC0000'";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(entradasTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * set up
     */
    @Before
    public void setUp() {
    }

    /**
     * tear down
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getIdEntrada method, of class entradas.
     */
    @Test
    public void testGetIdEntrada() {
        System.out.println("getIdEntrada");
        entradas instance = new entradas();
        String expResult = "12";
        instance.setIdEntrada("12");
        String result = instance.getIdEntrada();
        if (result.equals(expResult)) {
            System.out.println("sucesso");
        } else {
            // TODO review the generated test code and remove the default call to fail.
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of setIdEntrada method, of class entradas.
     */
    @Test
    public void testSetIdEntrada() {
        System.out.println("setIdEntrada");
        entradas instance = new entradas();
        String expResult = "12";
        instance.setIdEntrada(expResult);
        String result = instance.getIdEntrada();
        if (result.equals(expResult)) {
            System.out.println("sucesso");
        } else {
            // TODO review the generated test code and remove the default call to fail.
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of getIdFornecedor method, of class entradas.
     */
    @Test
    public void testGetIdFornecedor() {
        System.out.println("getIdFornecedor");
        entradas instance = new entradas();
        String expResult = "for";
        instance.setIdFornecedor("for");
        String result = instance.getIdFornecedor();
        if (result.equals(expResult)) {
            System.out.println("sucesso");
        } else {
            // TODO review the generated test code and remove the default call to fail.
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of setIdFornecedor method, of class entradas.
     */
    @Test
    public void testSetIdFornecedor() {
        System.out.println("setIdFornecedor");
        entradas instance = new entradas();
        String expResult = "for";
        instance.setIdFornecedor(expResult);
        String result = instance.getIdFornecedor();
        if (result.equals(expResult)) {
            System.out.println("sucesso");
        } else {
            // TODO review the generated test code and remove the default call to fail.
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of getDataEntrada method, of class entradas.
     */
    @Test
    public void testGetDataEntrada() {
        System.out.println("getDataEntrada");
        entradas instance = new entradas();
        String expResult = "data";
        instance.setDataEntrada("data");
        String result = instance.getDataEntrada();
        if (result.equals(expResult)) {
            System.out.println("sucesso");
        } else {
            // TODO review the generated test code and remove the default call to fail.
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of setDataEntrada method, of class entradas.
     */
    @Test
    public void testSetDataEntrada() {
        System.out.println("getDataEntrada");
        entradas instance = new entradas();
        String expResult = "data";
        instance.setDataEntrada(expResult);
        String result = instance.getDataEntrada();
        if (result.equals(expResult)) {
            System.out.println("sucesso");
        } else {
            // TODO review the generated test code and remove the default call to fail.
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of getEstado method, of class entradas.
     */
    @Test
    public void testGetEstado() {
        System.out.println("getEstado");
        entradas instance = new entradas();
        String expResult = "estado";
        instance.setEstado("estado");
        String result = instance.getEstado();
        if (result.equals(expResult)) {
            System.out.println("sucesso");
        } else {
            // TODO review the generated test code and remove the default call to fail.
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of setEstado method, of class entradas.
     */
    @Test
    public void testSetEstado() {
        System.out.println("getEstado");
        entradas instance = new entradas();
        String expResult = "estado";
        instance.setEstado(expResult);
        String result = instance.getEstado();
        if (result.equals(expResult)) {
            System.out.println("sucesso");
        } else {
            // TODO review the generated test code and remove the default call to fail.
            fail("The test case is a prototype.");
        }
    }

}
