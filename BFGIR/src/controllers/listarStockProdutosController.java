/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import bfgir.ecraPessoalOperador;
import bfgir.listarStockProdutos;
import bfgir.login;
import bfgir.menuAdministrador;
import bfgir.menuOperador;
import classes.categoria;
import static classes.categoria.procurarCategoriaID;
import classes.helper;
import classes.produto;
import static classes.produto.pesquisarProdutoID;
import classes.produtoSubCategoria;
import static classes.produtoSubCategoria.getListaProdutosSubCat;
import classes.utilizador;
import static classes.utilizador.procurarEmail;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * FXML Controller class
 */
public class listarStockProdutosController implements Initializable {

    @FXML
    private Pane painelStockProdutos;
    @FXML
    private ScrollPane Produtos;
    @FXML
    private MenuBar menu;
    @FXML
    private VBox vBoxProdutos;
    private int altura_vbox = 0;
    @FXML
    private TextArea descricaoTF;
    @FXML
    private TextArea QuantTF;
    @FXML
    private TextArea estadoTF;
    @FXML
    private Label lblDescricao;
    @FXML
    private Label lblQuant;
    @FXML
    private Label lblEstado;
    @FXML
    private Menu opcoes;
    @FXML
    private Label lbCodProd;
    @FXML
    private TextArea codigoProd;
    @FXML
    private Label lbQuantStockUnit;
    @FXML
    private TextArea quantStockUnit;
    @FXML
    private Label lbPrecoUnit;
    @FXML
    private TextArea precoUnit;
    @FXML
    private TextField codProdutoPesquisar;
    @FXML
    private Button pesquisarCodProduto;
    @FXML
    private Label equivalencia;


    /**
     * Initializes the controller class. este metodo inicializa um metodo para listar produtos e muda o estado de visiblidade de informações 
     *
     * @param url url
     * @param rb rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        adicionarEcraPessoalOperadorMenuOpcoes();

        lblDescricao.setVisible(false);
        lblQuant.setVisible(false);
        lblEstado.setVisible(false);
        lbCodProd.setVisible(false);
        lbQuantStockUnit.setVisible(false);
        lbPrecoUnit.setVisible(false);
        descricaoTF.setVisible(false);
        QuantTF.setVisible(false);
        estadoTF.setVisible(false);
        codigoProd.setVisible(false);
        quantStockUnit.setVisible(false);
        precoUnit.setVisible(false);


        pesquisarProdutos();
    }

    /**
     * Método para aceder ao menu do operador ou do administrador conforme o
     * nivel de login
     */
    @FXML
    private void openMenu(ActionEvent event) {
        try {
            if (login.getNivelLogin() == 1) {
                menuOperador menuO = new menuOperador();
                fecha();
                menuO.start(new Stage());
            } else {
                menuAdministrador menuAdmin = new menuAdministrador();
                fecha();
                menuAdmin.start(new Stage());
            }
        } catch (Exception ex) {
            Logger.getLogger(listarStockProdutosController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Metodo para terminar sessao do utilizador
     *
     * @param event evento
     */
    @FXML
    private void terminarSessao(ActionEvent event) {
        try {
            login.setIdLogin(0);
            login.setNivelLogin(10000);
            login log = new login();
            fecha();
            log.start(new Stage());

        } catch (Exception ex) {
            Logger.getLogger(menuAdministradorController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Método para encerrar a aplicação com um alerta de confirmação
     *
     * @param event evento
     */
    @FXML
    private void sair(ActionEvent event) {

        helper.opcaoSair(event);
    }

    /**
     * método para fechar o ecrã de listar o stock de produtos
     */
    private void fecha() {
        listarStockProdutos.getStage().close();
    }

    /**
     *
     * Método com a lógica da interface
     *
     *
     * Neste método criada uma lista com todos os produtos obtidos pelo método
     * getListaProdutos. De seguida para cada elemento dessa lista de produtos é
     * criado um botão com o id do produto. Depois de selecionar um produto é
     * feito um select com o codigo do mesmo (codProdPa) através do método
     * pesquisarProdutoID que retorna o produto correspondente ao codProdPa,
     * este produto é descriminado por cada atributo e listado no ecrã.
     *
     */
    private void pesquisarProdutos() {
        List<produto> produtos = produto.getListaProdutos();
        for (produto prod : produtos) {

            String codProdPa = prod.getCodProdPa();
            Button botao = new Button(codProdPa);
            botao.setId("botaoVbox");
            botao.setText(codProdPa);
            botao.setPrefHeight(30);
            botao.setPrefWidth(275);
            botao.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {
                    botao.setId("botaoVboxChangeCollour");
                    pesquisarDadosProduto(codProdPa);

                }
            });
            altura_vbox = altura_vbox + 30;
            vBoxProdutos.getChildren().addAll(botao);
            if (altura_vbox > 600) {
                Produtos.setPrefWidth(218);
                vBoxProdutos.setPrefHeight(altura_vbox);
            }
        }
    }

    /**
     * método para substituir o conteudo do menu de opções para colocar as
     * opções por ordem caso seja o operador a abrir o ecrã
     */
    private void adicionarEcraPessoalOperadorMenuOpcoes() {

        List<utilizador> operadores = procurarEmail(login.getIdLogin());
        for (utilizador operador : operadores) {
            if (operador.getNivel() == 1) {
                opcoes.getItems().clear();
                MenuItem ePOp = new MenuItem("Ecrã Pessoal");
                MenuItem terminarSessao = new MenuItem("Terminar Sessão");
                MenuItem sair = new MenuItem("Sair");
                opcoes.getItems().add(ePOp);
                opcoes.getItems().add(terminarSessao);
                opcoes.getItems().add(sair);
                ePOp.setOnAction((ActionEvent e) -> {
                    try {
                        ecraPessoalOperador ePO = new ecraPessoalOperador();
                        fecha();
                        ePO.start(new Stage());
                    } catch (Exception ex) {
                        Logger.getLogger(listarStockProdutosController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
                terminarSessao.setOnAction((ActionEvent e) -> {
                    try {
                        login.setIdLogin(0);
                        login.setNivelLogin(10000);
                        login log = new login();
                        fecha();
                        log.start(new Stage());
                    } catch (Exception ex) {
                        Logger.getLogger(listarStockProdutosController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
                sair.setOnAction((ActionEvent e) -> {
                    System.exit(0);
                });
            }
        }
    }

    /**
     * Este metodo vai passar todas as informacoes do produto pesquisado para visivel 
     * 
     * @param event evento
     */
    @FXML
    private void pesquisarCodProduto(ActionEvent event) {
        lblDescricao.setVisible(true);
        lblQuant.setVisible(true);
        lblEstado.setVisible(true);
        lbCodProd.setVisible(true);
        lbQuantStockUnit.setVisible(true);
        lbPrecoUnit.setVisible(true);
        descricaoTF.setVisible(true);
        QuantTF.setVisible(true);
        estadoTF.setVisible(true);
        codigoProd.setVisible(true);
        quantStockUnit.setVisible(true);
        precoUnit.setVisible(true);

        descricaoTF.setText("");
        QuantTF.setText("");
        estadoTF.setText("");
        codigoProd.setText("");
        quantStockUnit.setText("");
        precoUnit.setText("");


        List<produto> produtos = pesquisarProdutoID(codProdutoPesquisar.getText());
        for (produto prod : produtos) {
            codigoProd.setText(prod.getCodProdPa());
            descricaoTF.setText(prod.getDescricao());
            QuantTF.setText(String.valueOf(prod.getQuantStock()) + " " + prod.getTipoQuantStock());
            if (prod.getQuantStockUnit() != 0) {
                quantStockUnit.setText(String.valueOf(prod.getQuantStockUnit()) + " " + prod.getTipoQuantStockUnit());
                equivalencia.setText("Cada " + prod.getTipoQuantStock() + " contêm " + prod.getQuantStockUnit() / prod.getQuantStock() + " " + prod.getTipoQuantStockUnit());
            } else {
                equivalencia.setText("");
                lbQuantStockUnit.setVisible(false);
                quantStockUnit.setVisible(false);
            }
            precoUnit.setText(String.valueOf(prod.getPrecoUnitSIVA()) + "EUR");
            estadoTF.setText(prod.getEstado());
        }

     
    }

    /**
     * Este metodo vai passar todas as informacoes do produto pesquisado para visivel 
     * 
     * @param idProd id do produto a pesquisar
     */
    private void pesquisarDadosProduto(String idProd) {

        lblDescricao.setVisible(true);
        lblQuant.setVisible(true);
        lblEstado.setVisible(true);
        lbCodProd.setVisible(true);
        lbQuantStockUnit.setVisible(true);
        lbPrecoUnit.setVisible(true);
        descricaoTF.setVisible(true);
        QuantTF.setVisible(true);
        estadoTF.setVisible(true);
        codigoProd.setVisible(true);
        quantStockUnit.setVisible(true);
        precoUnit.setVisible(true);


        descricaoTF.setText("");
        QuantTF.setText("");
        estadoTF.setText("");
        codigoProd.setText("");
        quantStockUnit.setText("");
        precoUnit.setText("");



        List<produto> produtos = pesquisarProdutoID(idProd);
        for (produto prod : produtos) {
            codigoProd.setText(prod.getCodProdPa());
            descricaoTF.setText(prod.getDescricao());
            QuantTF.setText(String.valueOf(prod.getQuantStock()) + " " + prod.getTipoQuantStock());
            if (prod.getQuantStockUnit() != 0) {
                quantStockUnit.setText(String.valueOf(prod.getQuantStockUnit()) + " " + prod.getTipoQuantStockUnit());
                equivalencia.setText("Cada " + prod.getTipoQuantStock() + " contêm " + prod.getQuantStockUnit() / prod.getQuantStock() + " " + prod.getTipoQuantStockUnit());
            } else {
                equivalencia.setText("");
                lbQuantStockUnit.setVisible(false);
                quantStockUnit.setVisible(false);
            }
            precoUnit.setText(String.valueOf(prod.getPrecoUnitSIVA()) + " EUR");
            estadoTF.setText(prod.getEstado());

        }

    }

}
