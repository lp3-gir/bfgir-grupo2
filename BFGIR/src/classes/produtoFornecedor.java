/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import bfgir.BDConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe produtoFornecedor com metodos de acesso a tabela de produtoFornecedor na BD
 */
public class produtoFornecedor {

    private String ifFornecedor;
    private String codProdForn;
    private String codProdPa;

    /**
     * construtor da class produto fornecedor
     */
    public produtoFornecedor() {
    }

    /**
     * Construtor da classe produtoFornecedor
     *
     * @param idFornecedor id do fornecedor
     * @param codProdForn codigo do produto do forncedor
     * @param codProdPa codProdPa
     */
    public produtoFornecedor(String idFornecedor, String codProdForn, String codProdPa) {
        this.ifFornecedor = idFornecedor;
        this.codProdForn = codProdForn;
        this.codProdPa = codProdPa;
    }

    /**
     * @return the ifFornecedor
     */
    public String getIfFornecedor() {
        return ifFornecedor;
    }

    /**
     * @param ifFornecedor the ifFornecedor to set
     */
    public void setIfFornecedor(String ifFornecedor) {
        this.ifFornecedor = ifFornecedor;
    }

    /**
     * @return the codProdForn
     */
    public String getCodProdForn() {
        return codProdForn;
    }

    /**
     * @param codProdForn the codProdForn to set
     */
    public void setCodProdForn(String codProdForn) {
        this.codProdForn = codProdForn;
    }

    /**
     * @return the codProdPa
     */
    public String getCodProdPa() {
        return codProdPa;
    }

    /**
     * @param codProdPa the codProdPa to set
     */
    public void setCodProdPa(String codProdPa) {
        this.codProdPa = codProdPa;
    }

    /**
     * Método para selecionar todo da produtofornecedor na BD<br>
     *
     * Este método faz um select à BD adicionando a um arrayList todos os
     * atributos de um produtofornecedor em que o codigo do produto do
     * fornecedor seja aquele.<br>
     *
     * @param codProdForn codigo do produto do fornecedor
     * @return lista de produtos do fornecedor
     *
     *
     */
    public static List<produtoFornecedor> procurarCodProdPa(String codProdForn) {

        List<produtoFornecedor> produtosFornecedores = new ArrayList<produtoFornecedor>();
        String sql = "Select * from produtofornecedor where codProdForn = (?)";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setString(1, codProdForn);

            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                produtoFornecedor encProd = new produtoFornecedor();
                encProd.setIfFornecedor(results.getString(1));
                encProd.setCodProdForn(results.getString(2));
                encProd.setCodProdPa(results.getString(3));
                produtosFornecedores.add(encProd);
            }

        } catch (SQLException ex) {
            Logger.getLogger(produtoFornecedor.class.getName()).log(Level.SEVERE, null, ex);
        }

        return produtosFornecedores;
    }

}
