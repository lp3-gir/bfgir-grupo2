/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import bfgir.BDConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe utilizador com metodos de acesso a tabela de utilizador na BD
 *
 */
public class utilizador {

    private int idUtilizador;
    private String email;
    private String password;
    private String salt;
    private int nivel;
    private String estado;

    /**
     * Construtor da classe utilizador
     *
     * @param idUtilizador id de utilizador
     * @param email email
     * @param password PassWord
     * @param salt Salt
     * @param nivel Nivel
     * @param estado estado
     */
    public utilizador(int idUtilizador, String email, String password, String salt, int nivel, String estado) {
        this.idUtilizador = idUtilizador;
        this.email = email;
        this.password = password;
        this.nivel = nivel;
        this.estado = estado;
    }

    /**
     * Construtor da classe utilizador
     */
    public utilizador() {
    }

    /**
     * Método get ID de utilizador
     *
     * @return the idUtilizador
     */
    public int getIdUtilizador() {
        return idUtilizador;
    }

    /**
     * Método set ID do utilizador
     *
     * @param idUtilizador the idUtilizador to set
     */
    public void setIdUtilizador(int idUtilizador) {
        this.idUtilizador = idUtilizador;
    }

    /**
     * Método get Email do utilizador
     *
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Método set Email do utilizador
     *
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Método get PassWord do utilizador
     *
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Método set PassWord do utilizador
     *
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Método get salt da password
     *
     * @return the idFornecedor
     */
    public String getSalt() {
        return salt;
    }

    /**
     * Método set Salt da PassWord
     *
     * @param salt the salt to set
     */
    public void setSalt(String salt) {
        this.salt = salt;
    }

    /**
     * Método get Nivel do utilizaodr
     *
     * @return the nivel
     */
    public int getNivel() {
        return nivel;
    }

    /**
     * Método set Nivel do utilizador
     *
     * @param nivel the nivel to set
     */
    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    /**
     * Método get estado do utilizador
     *
     * @return the estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * Método set estado do utilizador
     *
     * @param estado the esatdo to set
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * Método para selecionar todos os atributos do utilizador na BD<br>
     *
     * Este método recebe por parametro o email do utilizador (email). Posto
     * isto faz um select à BD adicionando a um arrayList todos os atributos de
     * um fornecedor com aquele email.<br>
     *
     * @param email email a pesquisar
     * @return lista utilizadores
     *
     *
     */
    public static List<utilizador> procurarDadosLogin(String email) {

        List<utilizador> utilizadores = new ArrayList<utilizador>();
        String sql = "SELECT * from utilizador WHERE email like (?)";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setString(1, email);

            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                utilizador utilizador = new utilizador();

                utilizador.setIdUtilizador(results.getInt(1));
                utilizador.setEmail(results.getString(2));
                utilizador.setPassword(results.getString(3));
                utilizador.setSalt(results.getString(4));
                utilizador.setNivel(results.getInt(5));
                utilizador.setEstado(results.getString(6));

                utilizadores.add(utilizador);
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return utilizadores;
    }

    /**
     * Método para inserir um utilizador na BD<br>
     *
     * Este método recebe por parametro um objeto do tipo utilizador (user).
     * Posto isto faz um insert à BD defenindo cada atributo do utilizador que
     * pertença aquele objeto.<br>
     *
     * @param user objeto do tipo utilizador
     *
     *
     *
     */
    public static void inserirUtilizadores(utilizador user) {

        List<utilizador> utilizadores = new ArrayList<utilizador>();
        String sql = "INSERT INTO utilizador (email,password,salt,nivel,estado) VALUES (?,?,?,?,?)";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setString(1, user.getEmail());
            pstmt.setString(2, user.getPassword());
            pstmt.setString(3, user.getSalt());
            pstmt.setInt(4, user.getNivel());
            pstmt.setString(5, user.getEstado());
            pstmt.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

    }

    /**
     * Método para selecionar todos os atributos do utilizador na BD<br>
     *
     * Este método recebe por parametro o ID de Utilizador (idUtilizador). Posto
     * isto faz um select à BD adicionando a um arrayList todos os atributos de
     * um utilizador com aquele ID de Utilizador.<br>
     *
     * @param idUtilizador id de utilizador
     * @return lista utilizadores
     *
     *
     *
     *
     */
    public static List<utilizador> procurarEmail(int idUtilizador) {

        List<utilizador> utilizadores = new ArrayList<utilizador>();
        String sql = "SELECT * from utilizador WHERE idutilizador = (?)";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setInt(1, idUtilizador);

            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                utilizador utilizador = new utilizador();

                utilizador.setIdUtilizador(results.getInt(1));
                utilizador.setEmail(results.getString(2));
                utilizador.setPassword(results.getString(3));
                utilizador.setSalt(results.getString(4));
                utilizador.setNivel(results.getInt(5));
                utilizador.setEstado(results.getString(6));

                utilizadores.add(utilizador);
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return utilizadores;
    }

    /**
     * método para obter uma lista de todos utilizadores (ativos e desativos)
     * existentes na base de dados <br>
     *
     * @return retorna lista de utilizadores
     */
    public static List<utilizador> listarUtilizadores() {

        List<utilizador> utilizadores = new ArrayList<utilizador>();
        String sql = "SELECT * from utilizador";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                utilizador utilizador = new utilizador();

                utilizador.setIdUtilizador(results.getInt(1));
                utilizador.setEmail(results.getString(2));
                utilizador.setPassword(results.getString(3));
                utilizador.setSalt(results.getString(4));
                utilizador.setNivel(results.getInt(5));
                utilizador.setEstado(results.getString(6));
                utilizadores.add(utilizador);
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return utilizadores;
    }
    

}
