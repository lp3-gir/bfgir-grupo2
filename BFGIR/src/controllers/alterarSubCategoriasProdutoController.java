/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import bfgir.alterarSubCategoriasProduto;
import bfgir.ecraPessoalOperador;
import bfgir.login;
import bfgir.menuAdministrador;
import bfgir.menuOperador;
import classes.categoria;
import static classes.categoria.listarCategoriasPorNivel;
import classes.helper;
import classes.produto;
import static classes.produto.pesquisarProdutoID;
import classes.produtoSubCategoria;
import static classes.produtoSubCategoria.atualizarSubCategoriaProdutoSubCategoria;
import static classes.produtoSubCategoria.getAllProdutosSubCat;
import static classes.produtoSubCategoria.getListaProdutosSubCat;
import static classes.produtoSubCategoria.getListaProdutosSubCatID;
import static classes.produtoSubCategoria.getSameRecordProdSubCat;
import classes.utilizador;
import static classes.utilizador.procurarEmail;
import static controllers.alterarCategoriasController.isNewIdCategoria;
import static controllers.alterarProdutosController.getIDCategoria;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Classe controladora do ecrã alterarSubCategoriasProduto.<br>
 *
 * @author Ilídio Magalhães
 */
public class alterarSubCategoriasProdutoController implements Initializable {

    //variáveis para o algoritmo
    private int altura_vbox = 0;
    static Alert a = new Alert(Alert.AlertType.NONE);
    static produtoSubCategoria produtoSubCatBase = new produtoSubCategoria();
    static List<produtoSubCategoria> listaProdutoSubCategoriasBD = new ArrayList();
    static List<categoria> listaCategoriasBDN1 = new ArrayList<categoria>();
    static List<categoria> listaCategoriasBDN2 = new ArrayList<categoria>();
    static List<categoria> listaCategoriasBDN3 = new ArrayList<categoria>();
    static String semCategoria = "Sem Categoria";
    static String semSelecao = "-";
    static String nivelUmBase = "";
    static String nivelDoisBase = "";
    static String nivelTresBase = "";

    @FXML
    private Menu opcoes;
    @FXML
    private MenuBar menu;
    @FXML
    private Label lblN2;
    @FXML
    private TextField idSubCatProdPesquisar;
    @FXML
    private Button pesquisarIdSubCategoriaProd;
    @FXML
    private ChoiceBox<String> cbN2;
    @FXML
    private Button botaoAtualizarSubCategorias;
    @FXML
    private ChoiceBox<String> cbN3;
    @FXML
    private Label lblN3;
    @FXML
    private TextArea idSubCatProd;
    @FXML
    private Label lblN1;
    @FXML
    private ChoiceBox<String> cbN1;
    @FXML
    private ChoiceBox<String> cbFP;
    @FXML
    private Pane painelSubCat;
    @FXML
    private ScrollPane SPSubCat;
    @FXML
    private VBox vBoxSubCategorias;
    @FXML
    private Label lblID;
    @FXML
    private Label lblDescrProd;
    @FXML
    private TextArea descrProd;
    @FXML
    private Label lblCodProd;
    @FXML
    private TextArea codProd;

    /**
     * Método de arranque do ecrã.<br>
     *
     * @param url url
     * @param rb rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            carregarDadosIniciarEcra();
        } catch (Exception initA) {
            mensagemErroFatal();
        }
    }

    /**
     * Metodo para terminar sessao do utilizador.<br>
     *
     * @param event evento.
     */
    @FXML
    private void terminarSessao(ActionEvent event) {
        try {
            login.setIdLogin(0);
            login.setNivelLogin(10000);
            login log = new login();
            fecha();
            log.start(new Stage());

        } catch (Exception ex) {
            mensagemErroFatal();
            Logger.getLogger(alterarSubCategoriasProdutoController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Método para encerrar a aplicação.<br>
     */
    @FXML
    private void sair(ActionEvent event) {
        helper.opcaoSair(event);
    }

    /**
     * Método para aceder ao menu do operador.<br>
     */
    @FXML
    private void openMenu(ActionEvent event) {
        try {
            if (login.getNivelLogin() == 1) {
                menuOperador menuO = new menuOperador();
                fecha();
                menuO.start(new Stage());
            } else {
                menuAdministrador menuAdmin = new menuAdministrador();
                fecha();
                menuAdmin.start(new Stage());
            }
        } catch (Exception ex) {
            Logger.getLogger(alterarSubCategoriasProdutoController.class.getName()).log(Level.SEVERE, null, ex);
            mensagemErroFatal();
        }

    }

    /**
     * Método para procurar uma registo da tabela produtosubcategoria na BD e
     * listar os seus atributos.<br>
     * Método valida o id do registo e verifica se este existe e depois lista os
     * seus atributos caso o mesmo exista.<br>
     *
     */
    @FXML
    private void pesquisarIdSubCategoriaProd(ActionEvent event) {
        try {
            Alert alertaDeNovo = new Alert(Alert.AlertType.NONE);
            if (!idSubCatProdPesquisar.getText().equals("") && Pattern.compile("^[1-9]+[0-9]*$").matcher(idSubCatProdPesquisar.getText()).find() == true
                    && isNewIdCategoria(Integer.parseInt(idSubCatProdPesquisar.getText())) == false && idSubCatProdPesquisar.getText().length() <= 10) {
                int idsubProdCat = 0;
                try {
                    idsubProdCat = Integer.parseInt(idSubCatProdPesquisar.getText());
                } catch (NumberFormatException numbPQCPSC) {
                    mensagemErroRecarregarEcra();
                }
                listarDetalhesRegistoBD(idsubProdCat);
                setVisibleTrue();
                idSubCatProdPesquisar.setText("");
            } else {
                setVisibleEditableFalse();
                alertaDeNovo.setAlertType(Alert.AlertType.WARNING);
                alertaDeNovo.getButtonTypes().setAll(ButtonType.OK);
                alertaDeNovo.setTitle("ID de Registo Inválido");
                alertaDeNovo.setHeaderText("O registo não existe ou inseriu um valor incorreto");
                alertaDeNovo.setContentText("Insira um registo na forma de número inteiro");
                Optional<ButtonType> result = alertaDeNovo.showAndWait();
                if (result.get() == ButtonType.OK) {
                    idSubCatProdPesquisar.setText("");
                }
            }
        } catch (NumberFormatException e) {
            setVisibleEditableFalse();
            Alert alertaDeNovo = new Alert(Alert.AlertType.NONE);
            alertaDeNovo.setAlertType(Alert.AlertType.WARNING);
            alertaDeNovo.getButtonTypes().setAll(ButtonType.OK);
            alertaDeNovo.setTitle("ID de Registo Inválido");
            alertaDeNovo.setHeaderText("O registo não existe ou inseriu um valor incorreto");
            alertaDeNovo.setContentText("Insira um registo na forma de número inteiro");
            Optional<ButtonType> result = alertaDeNovo.showAndWait();
            if (result.get() == ButtonType.OK) {
                idSubCatProdPesquisar.setText("");
            }
        } catch (Exception pesquisaFalhada) {
            Alert alertaDeNovo = new Alert(Alert.AlertType.NONE);
            alertaDeNovo.setAlertType(Alert.AlertType.WARNING);
            alertaDeNovo.getButtonTypes().setAll(ButtonType.OK);
            alertaDeNovo.setTitle("ID de Registo Inválido");
            alertaDeNovo.setHeaderText("O registo não existe ou inseriu um valor incorreto");
            alertaDeNovo.setContentText("Insira um registo na forma de número inteiro");
            Optional<ButtonType> result = alertaDeNovo.showAndWait();
            if (result.get() == ButtonType.OK) {
                idSubCatProdPesquisar.setText("");
            }
        }
    }

    /**
     * Método para validar selecção de subcategoria nível 2.<br>
     */
    @FXML
    private void selecionarCatN2(MouseEvent event) {
        try {
            if (cbN2.getValue().equals("") || cbN2.getValue() == null) {
                a.setAlertType(Alert.AlertType.WARNING);
                a.getButtonTypes().setAll(ButtonType.OK);
                a.setTitle("Nivel de Subcategoria Inválido");
                a.setHeaderText("A subcategoria 1 assumiu um valor inválido");
                a.setContentText("Retornando ao estado inicial");
                Optional<ButtonType> result = a.showAndWait();
                if (result.get() == ButtonType.OK) {
                    cbN2.setValue(nivelDoisBase);
                }
            } else if (cbN2.getValue().equals(semCategoria)) {
                cbN3.setValue(semCategoria);
            }
        } catch (Exception nivel2Val) {
            mensagemErroRecarregarEcra();
        }
    }

    /**
     * Método para atualizar um registo da tabela produtossubcategorias.<br>
     * Método começa por verificar se a tabela tem registos e há concordância de
     * dados com os visualizdos no ecrã.<br> Caso não hajam, será impossível
     * atualizar os registos.<br> Depois, o método converter os nomes das
     * subcategorias no respetivo id de categoria.<br>
     * Para evitar a redundância de dados, o método verifica se há registos na
     * BD em tudo iguais menos pelo id do mesmo.<br>Para os níveis 2 e 3 em que
     * é possível não ter categoria, o placeholder respetivo receberá o id de
     * zero. Caso isto falhe, o utilizador é notificado.<br>
     * Finalmente o método procede à atualização de cada nível.<br>Caso
     * verifique alterações no atributo, realiza a atualização e procura
     * confirmá-la. No fim de tudo, o utilizador é notificado do sucesso,
     * insucesso ou ausência de alteração ou até mesmo da falha técnica da
     * operação.<br>
     */
    @FXML
    private void botaoAtualizarSubCategoria(ActionEvent event) {
        int falhou = 0;
        int sucesso = -1;
        List<produtoSubCategoria> listaProdutoSubCategorias = new ArrayList();
        try {
            listaProdutoSubCategorias = getAllProdutosSubCat();

            //obter ids das subcategorias
            int idSubCatN1 = 0;
            int idSubCatN2 = 0;
            int idSubCatN3 = 0;
            try {
                idSubCatN1 = getIDCategoria(cbN1.getValue(), semCategoria);
                idSubCatN2 = getIDCategoria(cbN2.getValue(), semCategoria);
                idSubCatN3 = getIDCategoria(cbN3.getValue(), semCategoria);
            } catch (Exception semCategoria) {
                falhou = -2;
                sucesso = -2;
            }

            //atualizar as subcategorias
            if (!listaProdutoSubCategorias.isEmpty()) {

                //ver se há registos redundantes e impedir a atualização aqui
                boolean canUpdate = doNotexistsSameRecord(produtoSubCatBase.getCodProdPa(), idSubCatN1, idSubCatN2, idSubCatN3);
                if (canUpdate == true) {
                    // não faz nada  
                } else {
                    falhou = -4;
                    sucesso = -4;
                }

                if (idSubCatN1 != produtoSubCatBase.getNivel1() && idSubCatN1 > 0 && falhou == 0) {

                    atualizarSubCategoriaProdutoSubCategoria(idSubCatN1, 1, produtoSubCatBase.getIdProdSub());

                    List<produtoSubCategoria> updatedN1List = new ArrayList();
                    updatedN1List = getListaProdutosSubCatID(produtoSubCatBase.getIdProdSub());
                    if (!updatedN1List.isEmpty()) {
                        for (produtoSubCategoria psb : updatedN1List) {
                            if (psb.getIdProdSub() == produtoSubCatBase.getIdProdSub()
                                    && psb.getNivel1() == idSubCatN1) {
                                sucesso = 1;
                            } else {
                                sucesso = 0;
                                falhou = 1;
                            }
                        }
                    } else {
                        sucesso = 0;
                        falhou = 1;
                    }

                }
                if (idSubCatN2 != produtoSubCatBase.getNivel2() && idSubCatN2 >= 0 && falhou == 0) {

                    if (idSubCatN2 == 0 && idSubCatN3 > 0) {
                        sucesso = -3;
                        falhou = -3;
                    } else {
                        atualizarSubCategoriaProdutoSubCategoria(idSubCatN2, 2, produtoSubCatBase.getIdProdSub());

                        List<produtoSubCategoria> updatedN2List = new ArrayList();
                        updatedN2List = getListaProdutosSubCatID(produtoSubCatBase.getIdProdSub());
                        if (!updatedN2List.isEmpty()) {
                            for (produtoSubCategoria psb : updatedN2List) {
                                if (psb.getIdProdSub() == produtoSubCatBase.getIdProdSub()
                                        && psb.getNivel2() == idSubCatN2) {
                                    sucesso = 1;
                                } else {
                                    sucesso = 0;
                                    falhou = 1;
                                }
                            }
                        } else {
                            sucesso = 0;
                            falhou = 1;
                        }
                    }

                }
                if (idSubCatN3 != produtoSubCatBase.getNivel3() && idSubCatN3 >= 0 && falhou == 0) {
                    if (idSubCatN2 == 0 && idSubCatN3 > 0) {
                        sucesso = -3;
                        falhou = -3;

                    } else {
                        atualizarSubCategoriaProdutoSubCategoria(idSubCatN3, 3, produtoSubCatBase.getIdProdSub());
                        List<produtoSubCategoria> updatedN3List = new ArrayList();
                        updatedN3List = getListaProdutosSubCatID(produtoSubCatBase.getIdProdSub());
                        if (!updatedN3List.isEmpty()) {
                            for (produtoSubCategoria psb : updatedN3List) {
                                if (psb.getIdProdSub() == produtoSubCatBase.getIdProdSub()
                                        && psb.getNivel3() == idSubCatN3) {
                                    sucesso = 1;
                                } else {
                                    sucesso = 0;
                                    falhou = 1;
                                }
                            }
                        } else {
                            sucesso = 0;
                            falhou = 1;
                        }
                    }
                }

                //lidar com as mensagens
                if (falhou == 0 && sucesso == -1) {
                    mensagemSubCategoriasProdutoInalteradas(produtoSubCatBase.getCodProdPa());
                } else if (falhou == 0 && sucesso == 1) {
                    mensagemSucessoAlterarSubCategoriasProduto(produtoSubCatBase.getCodProdPa());
                } else if (falhou == 1 && sucesso == 0) {
                    mensagemInSucessoAlterarSubCategoriasProduto(produtoSubCatBase.getCodProdPa());
                } else if (falhou == -2 && sucesso == -2) {
                    mensagemErroFalharValidarIdCategoria();
                } else if (falhou == -3 && sucesso == -3) {
                    mensagemErroFalharValidarAlteracao();
                } else if (falhou == -4 && sucesso == -4) {
                    mensagemErroRegistoRedundante();
                } else {
                }
            } else {
                mensagemErroPassagemMenUtilizador();
            }
        } catch (Exception atualizarSubCat) {
            mensagemErroRecarregarEcra();
        }
    }

    /**
     * Método para validar selecção de subcategoria nível 3.<br>
     */
    @FXML
    private void selecionarCatN3(MouseEvent event
    ) {
        try {
            if (cbN3.getValue().equals("") || cbN3.getValue() == null) {
                a.setAlertType(Alert.AlertType.WARNING);
                a.getButtonTypes().setAll(ButtonType.OK);
                a.setTitle("Nivel de Subcategoria Inválido");
                a.setHeaderText("A subcategoria 1 assumiu um valor inválido");
                a.setContentText("Retornando ao estado inicial");
                Optional<ButtonType> result = a.showAndWait();
                if (result.get() == ButtonType.OK) {
                    cbN3.setValue(nivelTresBase);
                }
            } else if (!cbN3.getValue().equals(semCategoria) && cbN2.equals(semCategoria)) {
                cbN3.setValue(semCategoria);
            }
        } catch (Exception nivel3Val) {
            mensagemErroRecarregarEcra();
        }
    }

    /**
     * Método para validar selecção de subcategoria nível 1.<br>
     */
    @FXML
    private void selecionarCatN1(MouseEvent event
    ) {
        try {
            if (cbN1.getValue().equals("") || cbN1.getValue() == null || cbN1.equals(semCategoria)) {
                a.setAlertType(Alert.AlertType.WARNING);
                a.getButtonTypes().setAll(ButtonType.OK);
                a.setTitle("Nivel de Subcategoria Inválido");
                a.setHeaderText("A subcategoria 1 assumiu um valor inválido");
                a.setContentText("Retornando ao estado inicial");
                Optional<ButtonType> result = a.showAndWait();
                if (result.get() == ButtonType.OK) {
                    cbN1.setValue(nivelUmBase);
                }
            }
        } catch (Exception nivel1Val) {
            mensagemErroRecarregarEcra();
        }
    }

    /**
     * Método para filtrar a lista da tabela produtosubcategoria pelo código de
     * um produto.<br>
     * Método seleciona o código de um produto (ou de não selecção) e vai
     * procurar na BD pelos registos associados a esse produto.<br>Caso encontre
     * preenche a vbox da lista de objetos produtoSubCategoria mostrando o id de
     * cada registo associado ao produto.<br>Caso não faça selecção alguma, o
     * método vai mostar a lista toda.<br>
     * Por outro lado, caso não encontre nada ou dê erro, o método informa o
     * utilizador da ausência de registos.<br>
     */
    @FXML
    private void filtrarCodProd(ActionEvent event
    ) {
        try {
            setVisibleEditableFalse();
            List<produtoSubCategoria> listaProdutoSubCategoriasFiltradas = new ArrayList();
            listaProdutoSubCategoriasFiltradas.clear();
            if (cbFP.getValue().equals("") || cbFP == null) {
                mensagemErroRegistoNaoEncontrado();
            } else if (cbFP.getValue().equals(semSelecao)) {
                listarProdSubCategorias(listaProdutoSubCategoriasBD);
            } else {
                listaProdutoSubCategoriasFiltradas = getListaProdutosSubCat(cbFP.getValue());
                if (listaProdutoSubCategoriasFiltradas.isEmpty()) {
                    mensagemErroRegistoNaoEncontrado();
                } else {
                    listarProdSubCategorias(listaProdutoSubCategoriasFiltradas);
                }
            }

        } catch (Exception filtroFalhou) {
            mensagemErroRecarregarEcra();
        }

    }

    /**
     * Método para substituir o conteudo do menu de opções para colocar as
     * opções por ordem caso seja o operador a abrir o ecrã.<br>
     * Método recebe um objeto do tipo menu referente ao menu de opções e depois
     * refaz o mesmo com as novas opções consonante o utilizador seja operador
     * ou administrador.<br>
     */
    private void adicionarEcraPessoalOperadorMenuOpcoes() {
        try {
            List<utilizador> operadores = procurarEmail(login.getIdLogin());
            for (utilizador operador : operadores) {
                if (operador.getNivel() == 1) {

                    opcoes.getItems().clear();
                    MenuItem ePOp = new MenuItem("Ecrã Pessoal");
                    MenuItem terminarSessao = new MenuItem("Terminar Sessão");
                    MenuItem sair = new MenuItem("Sair");
                    opcoes.getItems().add(ePOp);
                    opcoes.getItems().add(terminarSessao);
                    opcoes.getItems().add(sair);
                    ;

                    ePOp.setOnAction((ActionEvent e) -> {
                        try {
                            ecraPessoalOperador ePO = new ecraPessoalOperador();
                            fecha();
                            ePO.start(new Stage());
                        } catch (Exception ex) {
                            mensagemErroFatal();
                        }
                    });
                    terminarSessao.setOnAction((ActionEvent e) -> {
                        try {
                            login.setIdLogin(0);
                            login.setNivelLogin(10000);
                            login log = new login();
                            fecha();
                            log.start(new Stage());
                        } catch (Exception ex) {
                            mensagemErroFatal();
                        }
                    });
                    sair.setOnAction((ActionEvent e) -> {
                        System.exit(0);
                    });
                }
            }

        } catch (Exception menuOpt) {
            mensagemErroFatal();
        }
    }

    /**
     * Método para fechar o ecrã.<br>
     */
    private static void fecha() {
        alterarSubCategoriasProduto.getStage().close();
    }

    /**
     * Mensagem de erro para tentar fechar em segurança a aplicação no caso
     * desta encontrar um erro muito grave.<br>
     * Se este erro for lançado é porque algo inesperado ocorreu (ex: falha de
     * acesso à base de dados, lançamento de alguns excepções em métodos
     * críticos ou falhas gerais e básicas de programação).<br>
     * É a mensagem de erro de severidade mais elevada deste ecrã.<br>
     */
    public static void mensagemErroFatal() {
        a.setAlertType(Alert.AlertType.ERROR);
        a.setTitle("ERRO Fatal");
        a.setHeaderText("A aplicação comportou-se de forma inesperada!");
        a.setContentText("Encerrando a aplicação");
        a.getButtonTypes().setAll(ButtonType.OK);
        Optional<ButtonType> result3 = a.showAndWait();
        if (result3.get() == ButtonType.OK) {
            System.exit(0);
        }
    }

    /**
     * Método para recarregar o ecrã de alterar categorias.<br>
     */
    private static void recarregarEcra() {
        alterarSubCategoriasProduto altCat = new alterarSubCategoriasProduto();
        fecha();
        try {
            altCat.start(new Stage());
        } catch (Exception ex) {
            mensagemErroFatal();
        }
    }

    /**
     * Método para lançar uam mensagem de erro e recarregar o ecrã caso algo
     * inesperado ocorra com o algoritmo.<br>
     * Método usado quando as falhas são graves mas não graves de mais que a
     * funcionalidade se encontre inutilizável.<br>
     * É a mensagem de erro de severidade mais leve neste ecrã.<br>
     */
    public static void mensagemErroRecarregarEcra() {
        Alert alertaNovo = new Alert(Alert.AlertType.NONE);
        alertaNovo.setAlertType(Alert.AlertType.ERROR);
        alertaNovo.getButtonTypes().setAll(ButtonType.OK);
        alertaNovo.setTitle("ERRO Lógica Aplicação");
        alertaNovo.setHeaderText("Os dados processados não são válidos");
        alertaNovo.setContentText("Ocorreu um erro ao validar os dados selecionados ou estes podem \nestar inacessíveis. "
                + "Tente novamente e caso não conseguir contacte o administrador");
        Optional<ButtonType> resulta = alertaNovo.showAndWait();
        if (resulta.get() == ButtonType.OK) {
            recarregarEcra();
        }

    }

    /**
     * Método para mostrar os atributos de cada registo quando carregados.<br>
     */
    private void setVisibleTrue() {
        lblID.setVisible(true);
        lblCodProd.setVisible(true);
        lblDescrProd.setVisible(true);
        lblN1.setVisible(true);
        lblN2.setVisible(true);
        lblN3.setVisible(true);
        botaoAtualizarSubCategorias.setVisible(true);

        idSubCatProd.setVisible(true);
        codProd.setVisible(true);
        descrProd.setVisible(true);
        cbN1.setVisible(true);
        cbN2.setVisible(true);
        cbN3.setVisible(true);

    }

    /**
     * Método para esconder e bloquear a edição do ID do registo e do código e
     * descrição do produto.<br>
     */
    private void setVisibleEditableFalse() {
        lblID.setVisible(false);
        lblCodProd.setVisible(false);
        lblDescrProd.setVisible(false);
        lblN1.setVisible(false);
        lblN2.setVisible(false);
        lblN3.setVisible(false);
        botaoAtualizarSubCategorias.setVisible(false);

        idSubCatProd.setVisible(false);
        codProd.setVisible(false);
        descrProd.setVisible(false);
        cbN1.setVisible(false);
        cbN2.setVisible(false);
        cbN3.setVisible(false);

        idSubCatProd.setEditable(false);
        codProd.setEditable(false);
        descrProd.setEditable(false);

    }

    /**
     * Método para listar numa vbox todos os registos de uma lista de objetos
     * produtoSubCategoria.<br>
     * Método recebe uma lista de objetos produtoSubCategoria, gera botões com
     * os IDs dos mesmos e fica a espera que o utilizador clique num dos botões
     * para mostrar os dados do registo.<br>
     */
    private void listarProdSubCategorias(List<produtoSubCategoria> listaProdutoSubCategoriasBD) {
        try {
            altura_vbox = 0; // isto é preciso para manter a largura dos botões quando se usa o filtro por produto - sim, largura

            vBoxSubCategorias.getChildren().removeAll(vBoxSubCategorias.getChildren());

            for (produtoSubCategoria prodSubCat : listaProdutoSubCategoriasBD) {
                int idProdSubCat = prodSubCat.getIdProdSub();
                Button botao = new Button(String.valueOf(idProdSubCat));
                botao.setId("botaoVbox");
                botao.setPrefHeight(30);
                botao.setPrefWidth(vBoxSubCategorias.getPrefWidth());
                botao.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent e) {
                        botao.setId("botaoVboxChangeCollour");

                        listarDetalhesRegistoBD(idProdSubCat);
                        setVisibleTrue();

                    }
                });
                altura_vbox = altura_vbox + 30;

                vBoxSubCategorias.getChildren().addAll(botao);
                if (altura_vbox > 548) {
                    vBoxSubCategorias.setPrefHeight(altura_vbox);
                }
            }

        } catch (Exception lista) {
            mensagemErroFatal();
        }
    }

    /**
     * Método para popular a lista de objetos produtoSubCategoria
     * listaProdutoSubCategoriasBD com todos os registos da tabela
     * produtossubcategoria da BD.<br>
     *
     * @return : retorna uma lista de objetos produtoSubCategoria.<br>
     */
    private static List<produtoSubCategoria> obterListaRegistosProdSubCatBD() {
        List<produtoSubCategoria> produSubCategorias = new ArrayList();
        try {
            produSubCategorias = getAllProdutosSubCat();
        } catch (Exception semLista) {
            mensagemErroFatal();
        }

        return produSubCategorias;
    }

    /**
     * Método para popular uma lista de objetos categoria com todos os registos
     * da tabela categoria da BD para um dado nivel de categoria.<br>
     *
     * @param nivel : nivel da categoria.
     * @return : retorna uma lista de objetos categoria com o nível selecionado.
     */
    private List<categoria> obterListaCategoriasBDNivel(int nivel) {
        List<categoria> categorias = new ArrayList<categoria>();
        try {
            categorias = listarCategoriasPorNivel(nivel);
        } catch (Exception semListaCat) {
            mensagemErroFatal();
        }
        return categorias;
    }

    /**
     * Método geral para invocar todos os métodos com as configurações iniciais
     * do ecrâ.<br>
     */
    private void carregarDadosIniciarEcra() {
        try {
            adicionarEcraPessoalOperadorMenuOpcoes();
            setVisibleEditableFalse();
            carregarDadosListasCategoriasNiveis();
            popularCBNivelNomeCategorias();
            listaProdutoSubCategoriasBD = obterListaRegistosProdSubCatBD();
            listarProdSubCategorias(listaProdutoSubCategoriasBD);
            popularCBCodProd(listaProdutoSubCategoriasBD, semSelecao);
        } catch (Exception inicioAbortado) {
            mensagemErroFatal();
        }
    }

    /**
     * Método para gerar uma lista de objetos produtoSubCategoria
     * listaProdutoSubCategoriasBD com o registo produtossubcategoria
     * correspondente ao idProdSub pesquisado.<br>
     * Método retorna uma lista em vez de um objeto para melhor integração com
     * método de listar os detalhes do registo.<br>
     *
     * @return : retorna uma lista de objetos produtoSubCategoria.
     */
    private static List<produtoSubCategoria> getRegProdSubCatByIDProdSub(int idProdSub) {
        List<produtoSubCategoria> produSubCategorias = new ArrayList();
        try {
            produSubCategorias = getListaProdutosSubCatID(idProdSub);
        } catch (Exception semLista) {
            mensagemErroRegistoNaoEncontrado();
        }

        return produSubCategorias;
    }

    /**
     * Método para listar os atributos de um registo da tabela
     * produtosubcategoria.<br>
     * Método recebe o id de um produto, procura os atributos do registo
     * associado na tabela produtosubcategoria e pelo nome das categorias na
     * tabela categorias.<br>
     * Guarda os dados relevantes num obbjeto produtoSubCategoria e lista os
     * atributos no ecrã.<br>
     * @param idProdSub : identificador de um produto.
     */
    private void listarDetalhesRegistoBD(int idProdSub) {
        try {

            cbN1.setValue("");
            cbN2.setValue("");
            cbN3.setValue("");
            nivelUmBase = "";
            nivelDoisBase = "";
            nivelTresBase = "";
            List<produtoSubCategoria> produSubCategorias = getRegProdSubCatByIDProdSub(idProdSub);
            for (produtoSubCategoria prodSubCat : produSubCategorias) {
                if (prodSubCat.getIdProdSub() == 0) {
                    mensagemErroRegistoNaoEncontrado();
                } else {
                    produtoSubCatBase.setIdProdSub(prodSubCat.getIdProdSub());
                    produtoSubCatBase.setCodProdPa(prodSubCat.getCodProdPa());
                    produtoSubCatBase.setNivel1(prodSubCat.getNivel1());
                    produtoSubCatBase.setNivel2(prodSubCat.getNivel2());
                    produtoSubCatBase.setNivel3(prodSubCat.getNivel3());

                }
            }
            idSubCatProd.setText(String.valueOf(produtoSubCatBase.getIdProdSub()));
            codProd.setText(produtoSubCatBase.getCodProdPa());
            descrProd.setText(obterDescricaoProduto(produtoSubCatBase.getCodProdPa()));
            cbN1.setValue(obterNomeCategoria(produtoSubCatBase.getNivel1(), listaCategoriasBDN1, semCategoria));
            cbN2.setValue(obterNomeCategoria(produtoSubCatBase.getNivel2(), listaCategoriasBDN2, semCategoria));
            cbN3.setValue(obterNomeCategoria(produtoSubCatBase.getNivel3(), listaCategoriasBDN3, semCategoria));
            nivelUmBase = cbN1.getValue();
            nivelDoisBase = cbN2.getValue();
            nivelTresBase = cbN3.getValue();
        } catch (Exception listDetails) {
            mensagemErroRecarregarEcra();
        }
    }

    /**
     * Método para lançar uam mensagem de erro e recarregar o ecrã caso algo
     * inesperado ocorra quando se faz um pedido à base de dados depois do
     * arranque do ecrã.<br>
     */
    public static void mensagemErroRegistoNaoEncontrado() {
        Alert alertaNovo = new Alert(Alert.AlertType.NONE);
        alertaNovo.setAlertType(Alert.AlertType.ERROR);
        alertaNovo.getButtonTypes().setAll(ButtonType.OK);
        alertaNovo.setTitle("Registo Não Encontrado");
        alertaNovo.setHeaderText("Não foi possível obter o registo");
        alertaNovo.setContentText("O registo não existe ou está temporariamente inacessível\nTente mais tarde");
        Optional<ButtonType> resultMERNE = alertaNovo.showAndWait();
        if (resultMERNE.get() == ButtonType.OK) {
            recarregarEcra();
        }

    }

    /**
     * Método para retornar a descrição de um produto a partir do seu código.<br>
     * Método vai buscar a descrição de um produto à BD após pesquina do seu
     * código.<br>
     *
     * @param codProdPa : código de produto.
     * @return : retorna um String com a descrição do produto.
     */
    private String obterDescricaoProduto(String codProdPa) {
        String descricaoProduto = "";
        List<produto> listaProdutos = new ArrayList();
        try {
            listaProdutos = pesquisarProdutoID(codProdPa);
            if (listaProdutos.isEmpty()) {
                mensagemErroRegistoNaoEncontrado();
            } else {
                for (produto produto : listaProdutos) {
                    if (produto.getCodProdPa().equals(codProdPa)) {
                        descricaoProduto = produto.getDescricao();
                    }
                }
            }
        } catch (Exception oNP) {
            mensagemErroRecarregarEcra();
        }
        return descricaoProduto;
    }

    /**
     * Método para retornar o nome de uma categoria de produto através do seu
     * id.<br>
     * Método procura na lista de categorias aquela com o id correspondente para
     * retornar o nome da respetiva categoria.<br>
     * Caso o id seja zero ou não encontre a categoria, retorna um String a
     * dizer "Sem Categoria".<br>
     *
     * @param idCat : id de uma categoria.
     * @param categorias : lista de categorias.
     * @param semCategoria : String para representar a inexistência de categoria.
     * @return : retorna um String com o nome da Categoria ou falta dela.
     */
    private String obterNomeCategoria(int idCat, List<categoria> categorias, String semCategoria) {
        String nomeCat = "";
        try {
            if (idCat == 0) {
                nomeCat = semCategoria;
            } else {
                for (categoria cat : categorias) {
                    if (cat.getIdCategoria() == idCat) {
                        nomeCat = cat.getNomeCategoria();
                        break;
                    }
                }
            }
            if (nomeCat.equals("")) {
                nomeCat = semCategoria;
            }
        } catch (Exception oNC) {
            mensagemErroRecarregarEcra();
        }
        return nomeCat;
    }

    /**
     * Método para popular uma choiceBox de nível de categoria com os valores
     * respectivos.<br>
     *
     *
     * Método verifica se a lista de categorias está vazia e o nível da
     * categoria para saber se adiciona o String de Sem Categoria ou não além da
     * lista de nomes de categorias. De resto, preenche a lista de nomes de
     * categorias.<br>
     *
     * @param nivel: nivel da categoria/choicebox.
     * @param cbN : choicebox de um nivel de categoria.
     * @param categorias : lista de categorias da BD.
     * @param semCategoria : String referente a ausência de categoria designada.
     *
     */
    private void popularCBNiCat(int nivel, ChoiceBox<String> cbN, List<categoria> categorias, String semCategoria) {
        try {
            if (categorias.isEmpty()) {
                cbN.getItems().add(semCategoria);
            } else {
                if (nivel == 1) {
                    for (categoria categoria : categorias) {
                        cbN.getItems().add(categoria.getNomeCategoria());
                    }
                } else if (nivel > 1) {
                    for (categoria categoria : categorias) {
                        cbN.getItems().add(categoria.getNomeCategoria());
                    }
                    cbN.getItems().add(semCategoria);

                } else {
                    cbN.getItems().add(semCategoria);
                }
                cbN.getItems().sorted();
            }
        } catch (Exception e) {
            cbN.getItems().add(semCategoria);

        }
    }

    /**
     * Método para popular as choiceboxes dos niveis de categorias com os nomes
     * das categorias do nivel apropriado mais um String de ausência de
     * categoria para os níveis diferentes de 1.<br>
     * Caso tenha ocorrido algum erro seja enviada uma mensagem e no caso de ser
     * mostrado o String Sem Categoria em todos os níveis é porque existiu um
     * erro a procurar categorias na BD.<br>
     */
    private void popularCBNivelNomeCategorias() {

        try {
            popularCBNiCat(1, cbN1, listaCategoriasBDN1, semCategoria);
            popularCBNiCat(2, cbN2, listaCategoriasBDN2, semCategoria);
            popularCBNiCat(3, cbN3, listaCategoriasBDN3, semCategoria);

        } catch (Exception popCBN) {
            mensagemErroFatal();
        }
    }

    /**
     * Método para preencher a choicebox de pesquisa de registos por código do
     * produto com com os códigos de produto existentes na tabela
     * produtossubcategoria sem repetição de valores.<br>
     * Método verifica se a lista de objetos produtoSubCategoria está vazia e
     * depois começa a preenchela com os códigos dos produtos encontrados.<br>Como
     * irão existir repetidos, através de hashes, o método remove os valores
     * repetidos.<br>
     * Caso não encontre nenhum valor ou a tabela esteja vazia, o método
     * preenche a choicebox com um String a informar o utilizador.<br>
     *
     * @param listaProdutoSubCategorias : lista de todos os registos da tabela
     * produtossubcategoria.
     * @param semSelecao : String do placeholder sem selecao.
     */
    private void popularCBCodProd(List<produtoSubCategoria> listaProdutoSubCategorias, String semSelecao) {
        try {
            if (listaProdutoSubCategorias.isEmpty()) {
                cbFP.getItems().add(semSelecao);
            } else {
                for (produtoSubCategoria prodSubCategoria : listaProdutoSubCategorias) {
                    cbFP.getItems().add(prodSubCategoria.getCodProdPa());
                }
                cbFP.getItems().add(semSelecao);
                //para remover elementos repetidos
                Set<String> set = new LinkedHashSet<>(cbFP.getItems());
                cbFP.getItems().clear();
                cbFP.getItems().addAll(set);
                cbFP.getItems().sorted();
                cbFP.setValue(semSelecao);
            }
        } catch (Exception e) {
            cbFP.getItems().add(semCategoria);

        }
    }

    /**
     * Método para enviar o utilizador para o seu menu em caso de falha da
     * aplicação.<br>
     *
     * Método deteta o tipo de utilizador consonante o seu nível e reencaminha
     * para o seu menu.<br>Em caso de falha, fecha a aplicação.<br>Este método,
     * semelhante ao método do botão menu, é invocado quando o programa
     * encontrar uma falha na lógica da aplicação (como aquando da atualização
     * do produto) para permitir o utilizador continuar a usar outras
     * funcionalidades da aplicação.<br>
     */
    public static void abrirMenUtilizador() {
        if (login.getNivelLogin() == 1) { //se for operador ir para o menu do operador
            menuOperador menuOp = new menuOperador();
            fecha();
            try {
                menuOp.start(new Stage());
            } catch (Exception ex) {
                mensagemErroFatal();
            }

        } else if (login.getNivelLogin() == 2) {
            menuAdministrador menuAdmin = new menuAdministrador();
            fecha();
            try {
                menuAdmin.start(new Stage());
            } catch (Exception ex) {
                mensagemErroFatal();
            }
        } else {
            mensagemErroFatal();
        }
    }

    /**
     * Método para lançar uma mensagem de erro caso um erro caso algo essencial
     * falhe na funcionalidade.<br>
     * Se este erro é lançado é porque a funcionalidade pode estar inoperável
     * temporaria ou permanentemente.<br>
     * É mensagem de erro de severidade intermédia do ecrã.<br>
     */
    public static void mensagemErroPassagemMenUtilizador() {
        Alert a = new Alert(Alert.AlertType.NONE);
        a.setAlertType(Alert.AlertType.ERROR);
        a.setTitle("ERRO Fatal");
        a.setHeaderText("Impossível validar os dados do ecrã!");
        a.setContentText("Algumas funcionalidades do aplicação estão inacessíveis");
        a.getButtonTypes().setAll(ButtonType.OK);
        Optional<ButtonType> resultMEPMU = a.showAndWait();
        if (resultMEPMU.get() == ButtonType.OK) {
            abrirMenUtilizador();
        }
    }

    /**
     * Método para carregar as listas de categorias correspondentes aos níveis
     * de categorias existentes com os registos apropriados.<br>
     */
    private void carregarDadosListasCategoriasNiveis() {
        try {
            listaCategoriasBDN1 = obterListaCategoriasBDNivel(1);
            listaCategoriasBDN2 = obterListaCategoriasBDNivel(2);
            listaCategoriasBDN3 = obterListaCategoriasBDNivel(3);

        } catch (Exception carregarCategorias) {
            mensagemErroFatal();
        }

    }

    /**
     * Método para lançar uam mensagem de erro e recarregar o ecrã caso algo
     * inesperado ocorra com o algoritmo a validar ids de categorias.<br>
     */
    private void mensagemErroFalharValidarIdCategoria() {
        Alert alertaNovo = new Alert(Alert.AlertType.NONE);
        alertaNovo.setAlertType(Alert.AlertType.ERROR);
        alertaNovo.getButtonTypes().setAll(ButtonType.OK);
        alertaNovo.setTitle("ERRO Lógica Aplicação");
        alertaNovo.setHeaderText("Os dados processados não são válidos");
        alertaNovo.setContentText("Não foi possível obter o identicador da categoria");
        Optional<ButtonType> resulta = alertaNovo.showAndWait();
        if (resulta.get() == ButtonType.OK) {
            recarregarEcra();
        }
    }

    /**
     * Método para notificar o utilizador do sucesso da atualização das
     * subCategorias do registo requisitar uma nova acção.<br>
     * Método informa o utilizador do sucesso da operação e pergunta se o
     * utilizador pretende atualizar uma nova categoria ou não.<br> Em caso
     * afirmativo, recarrega o ecrã mas em caso negativo envia o utilizador para
     * o menu do utilizador.<br>
     *
     * @param codProdPa : código do produto associado ao registo.
     */
    private static void mensagemSucessoAlterarSubCategoriasProduto(String codProdPa) {
        Alert alerta = new Alert(Alert.AlertType.NONE);
        alerta.setAlertType(Alert.AlertType.CONFIRMATION);
        alerta.getButtonTypes().setAll(ButtonType.YES, ButtonType.NO);
        alerta.setTitle("Alterar Subcategorias de Produtos");
        alerta.setHeaderText("Registo de subcategorias do produto " + codProdPa + " atualizado com sucesso!");
        alerta.setContentText("A atualização foi um sucesso\nDeseja realizar outra?");
        alerta.setResizable(false);
        Optional<ButtonType> resultMSASCP = alerta.showAndWait();
        if (resultMSASCP.get() == ButtonType.YES) {
            recarregarEcra();
        } else if (resultMSASCP.get() == ButtonType.NO) {
            abrirMenUtilizador();
        } else {
            mensagemErroFatal();
        }
    }

    /**
     * Método para notificar o utilizador do insucesso da atualização das
     * subCategorias do registo requisitar uma nova acção.<br>
     * Método informa o utilizador do insucesso da operação depois reinicia o
     * ecrã.<br>
     *
     * @param codProdPa : código do produto associado ao registo.
     */
    private static void mensagemInSucessoAlterarSubCategoriasProduto(String codProdPa) {
        Alert alerta = new Alert(Alert.AlertType.NONE);
        alerta.setAlertType(Alert.AlertType.ERROR);
        alerta.getButtonTypes().setAll(ButtonType.OK);
        alerta.setTitle("Alterar Subcategorias de Produtos");
        alerta.setHeaderText("Registo de subcategorias do produto " + codProdPa + " atualizado sem sucesso!");
        alerta.setContentText("Algo correu mal a atualizar o registo associado ao produto "
                + codProdPa + "A base de dados pode estar indisponível"
                + "\nTente novamente");
        alerta.setResizable(false);
        Optional<ButtonType> resultMISASCP = alerta.showAndWait();
        if (resultMISASCP.get() == ButtonType.YES) {
            recarregarEcra();
        }
    }

    /**
     * Método para notificar o utilizador da inalteração das subcategorias de um
     * registo produtossubcategorias associado a um produto e requisitar uma
     * nova acção.<br>
     * Método informa o utilizador do facto deste não ter efectuado alguma
     * alteração e pergunta se o utilizador pretende atualizar um nov registo ou
     * não.<br>Em caso afirmativo, recarrega o ecrã mas em caso negativo envia o
     * utilizador para o menu do utilizador.<br>
     *
     * @param codProdPa : código do produto associado ao registo.
     */
    private static void mensagemSubCategoriasProdutoInalteradas(String codProdPa) {
        Alert alerta = new Alert(Alert.AlertType.NONE);
        alerta.setAlertType(Alert.AlertType.CONFIRMATION);
        alerta.getButtonTypes().setAll(ButtonType.YES, ButtonType.NO);
        alerta.setTitle("Alterar Subcategorias de Produtos");
        alerta.setHeaderText("Não alterou o registo de subcategorias do produto " + codProdPa + "!");
        alerta.setContentText("Não modificou a lista de subcategorias associadas ao produto " + codProdPa
                + "\nDeseja tentar novamente ou atualizar outro registo?");
        alerta.setResizable(false);
        Optional<ButtonType> resultMSCPI = alerta.showAndWait();
        if (resultMSCPI.get() == ButtonType.YES) {
            recarregarEcra();
        } else if (resultMSCPI.get() == ButtonType.NO) {
            abrirMenUtilizador();
        } else {
            mensagemErroFatal();
        }
    }

    /**
     * Método para lançar uam mensagem de erro e recarregar o ecrã caso o
     * utilizador tente realizar uma atualização proibida.<br>
     */
    private void mensagemErroFalharValidarAlteracao() {
        Alert alertaNovo = new Alert(Alert.AlertType.NONE);
        alertaNovo.setAlertType(Alert.AlertType.ERROR);
        alertaNovo.getButtonTypes().setAll(ButtonType.OK);
        alertaNovo.setTitle("Alteração Proibida Registo");
        alertaNovo.setHeaderText("Existência de subcategoria nível 3 sem subcategoria nível 2");
        alertaNovo.setContentText("Não pode realizar a atualização");
        Optional<ButtonType> resultMEFVA = alertaNovo.showAndWait();
        if (resultMEFVA.get() == ButtonType.OK) {
            recarregarEcra();
        }
    }

    /**
     * Método para verificar se existe um registo na BD igual ao que vai ser
     * atualizado.<br>
     * Método passa os atributos do objeto.<br>
     *
     * @param codProd : código do produto.
     * @param idCatN1 : id subcategoria nível 1.
     * @param idCatN2 : id subcategoria nível 2.
     * @param idCatN3 : id subcategoria nível 3.
     * @return : verdadeiro caso não encontre nada e falso caso encontre.
     */
    public static boolean doNotexistsSameRecord(String codProd, int idCatN1, int idCatN2, int idCatN3) {
        boolean doNotExists = false;
        List<produtoSubCategoria> prodSubCategorias = new ArrayList();
        try {
            prodSubCategorias = getSameRecordProdSubCat(codProd, idCatN1, idCatN2, idCatN3);
            doNotExists = prodSubCategorias.isEmpty();
        } catch (Exception eSR) {
            doNotExists = false;
        }
        return doNotExists;
    }

    /**
     * Método para lançar uam mensagem de erro e recarregar o ecrã caso o
     * utilizador tente realizar uma atualização redunda.<br>
     */
    private void mensagemErroRegistoRedundante() {
        Alert alertaNovo = new Alert(Alert.AlertType.NONE);
        alertaNovo.setAlertType(Alert.AlertType.WARNING);
        alertaNovo.getButtonTypes().setAll(ButtonType.OK);
        alertaNovo.setTitle("Alteração Proibida Registo");
        alertaNovo.setHeaderText("O Registo é redundante!");
        alertaNovo.setContentText("Existe um registo com os atributos que selecionou");
        Optional<ButtonType> resultMERR = alertaNovo.showAndWait();
        if (resultMERR.get() == ButtonType.OK) {
            recarregarEcra();
        }
    }
}
