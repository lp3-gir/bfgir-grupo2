/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ruben
 */
public class ProdutosubcategoriaTest {



    /**
     * Test of getIdProdSub method, of class Produtosubcategoria.
     */
    @Test
    public void testGetIdProdSub() {
        System.out.println("getIdProdSub");
        Produtosubcategoria instance = new Produtosubcategoria();
        instance.setIdProdSub(1);
        Integer expResult = 1;
        assertEquals(expResult, instance.getIdProdSub());

    }

    /**
     * Test of setIdProdSub method, of class Produtosubcategoria.
     */
    @Test
    public void testSetIdProdSub() {
        System.out.println("setIdProdSub");
        Produtosubcategoria instance = new Produtosubcategoria();
        instance.setIdProdSub(1);

    }

    /**
     * Test of getCodProdPa method, of class Produtosubcategoria.
     */
    @Test
    public void testGetCodProdPa() {
        System.out.println("getCodProdPa");
        Produtosubcategoria instance = new Produtosubcategoria();
        instance.setCodProdPa("T.123");
        assertEquals("T.123", instance.getCodProdPa());

    }

    /**
     * Test of setCodProdPa method, of class Produtosubcategoria.
     */
    @Test
    public void testSetCodProdPa() {
        System.out.println("setCodProdPa");
        Produtosubcategoria instance = new Produtosubcategoria();
        instance.setCodProdPa("T.123");

    }

    /**
     * Test of getIdCatNivel1 method, of class Produtosubcategoria.
     */
    @Test
    public void testGetIdCatNivel1() {
        System.out.println("getIdCatNivel1");
        Produtosubcategoria instance = new Produtosubcategoria();
        instance.setIdCatNivel1(1);
        assertEquals(1, instance.getIdCatNivel1());

    }

    /**
     * Test of setIdCatNivel1 method, of class Produtosubcategoria.
     */
    @Test
    public void testSetIdCatNivel1() {
        System.out.println("setIdCatNivel1");
        Produtosubcategoria instance = new Produtosubcategoria();
        instance.setIdCatNivel1(1);

    }

    /**
     * Test of getIdCatNivel2 method, of class Produtosubcategoria.
     */
    @Test
    public void testGetIdCatNivel2() {
        System.out.println("getIdCatNivel2");
        Produtosubcategoria instance = new Produtosubcategoria();
        instance.setIdCatNivel2(2);
        assertEquals(2, instance.getIdCatNivel2());

    }

    /**
     * Test of setIdCatNivel2 method, of class Produtosubcategoria.
     */
    @Test
    public void testSetIdCatNivel2() {
        System.out.println("setIdCatNivel2");
        Produtosubcategoria instance = new Produtosubcategoria();
        instance.setIdCatNivel2(2);

    }

    /**
     * Test of getIdCatNivel3 method, of class Produtosubcategoria.
     */
    @Test
    public void testGetIdCatNivel3() {
        System.out.println("getIdCatNivel3");
        Produtosubcategoria instance = new Produtosubcategoria();
        instance.setIdCatNivel3(3);
        assertEquals(3, instance.getIdCatNivel3());

    }

    /**
     * Test of setIdCatNivel3 method, of class Produtosubcategoria.
     */
    @Test
    public void testSetIdCatNivel3() {
        System.out.println("setIdCatNivel3");
        Produtosubcategoria instance = new Produtosubcategoria();
        instance.setIdCatNivel3(3);

    }

}
