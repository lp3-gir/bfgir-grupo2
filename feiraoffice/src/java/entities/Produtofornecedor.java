/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Ruben
 */
@Entity
@Table(name = "produtofornecedor")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Produtofornecedor.findAll", query = "SELECT p FROM Produtofornecedor p")
    , @NamedQuery(name = "Produtofornecedor.findByIdFornecedor", query = "SELECT p FROM Produtofornecedor p WHERE p.idFornecedor = :idFornecedor")
    , @NamedQuery(name = "Produtofornecedor.findByCodProdForn", query = "SELECT p FROM Produtofornecedor p WHERE p.produtofornecedorPK.codProdForn = :codProdForn")
    , @NamedQuery(name = "Produtofornecedor.findByCodProdPa", query = "SELECT p FROM Produtofornecedor p WHERE p.produtofornecedorPK.codProdPa = :codProdPa")})
public class Produtofornecedor implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @EmbeddedId
    protected ProdutofornecedorPK produtofornecedorPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "idFornecedor")
    private String idFornecedor;

    /**
     *
     */
    public Produtofornecedor() {
    }

    /**
     *
     * @param produtofornecedorPK
     */
    public Produtofornecedor(ProdutofornecedorPK produtofornecedorPK) {
        this.produtofornecedorPK = produtofornecedorPK;
    }

    /**
     *
     * @param produtofornecedorPK
     * @param idFornecedor
     */
    public Produtofornecedor(ProdutofornecedorPK produtofornecedorPK, String idFornecedor) {
        this.produtofornecedorPK = produtofornecedorPK;
        this.idFornecedor = idFornecedor;
    }

    /**
     *
     * @param codProdForn
     * @param codProdPa
     */
    public Produtofornecedor(String codProdForn, String codProdPa) {
        this.produtofornecedorPK = new ProdutofornecedorPK(codProdForn, codProdPa);
    }

    /**
     *
     * @return produtofornecedorPK
     */
    public ProdutofornecedorPK getProdutofornecedorPK() {
        return produtofornecedorPK;
    }

    /**
     *
     * @param produtofornecedorPK
     */
    public void setProdutofornecedorPK(ProdutofornecedorPK produtofornecedorPK) {
        this.produtofornecedorPK = produtofornecedorPK;
    }

    /**
     *
     * @return idFornecedor
     */
    public String getIdFornecedor() {
        return idFornecedor;
    }

    /**
     *
     * @param idFornecedor
     */
    public void setIdFornecedor(String idFornecedor) {
        this.idFornecedor = idFornecedor;
    }
}
