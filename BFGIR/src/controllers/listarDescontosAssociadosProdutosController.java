package controllers;

import bfgir.ecraPessoalOperador;
import bfgir.listarDescontosAssociadosProdutos;
import bfgir.login;
import bfgir.menuAdministrador;
import bfgir.menuOperador;
import classes.desconto;
import static classes.desconto.getListaDescontosID;
import classes.descontosProduto;
import static classes.descontosProduto.getListaDescontosPordutosCodProd;
import static classes.descontosProduto.getListaDescontosPordutosCodProdDistinct;
import static classes.descontosProduto.getListaDescontosPordutosDistinct;
import static classes.descontosProduto.getListaDescontosPordutosIDDesCod;
import classes.produto;
import static classes.produto.pesquisarProdutoID;
import classes.utilizador;
import static classes.utilizador.procurarEmail;
import static controllers.inserirDescontosController.a;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author chico
 */
public class listarDescontosAssociadosProdutosController implements Initializable {

    private int altura_vbox = 0;
    private float desct;
    private String codPP;
    private int qtVenda;
    private String codigoProd;

    @FXML
    private VBox vBoxProdutos;

    @FXML
    private AnchorPane painelDescontos;

    @FXML
    private ScrollPane descontosScrollPane;

    @FXML
    private VBox vBoxDesconto;

    @FXML
    private Label lbDescricao;

    @FXML
    private TextArea descricao;

    @FXML
    private Label lbID;

    @FXML
    private TextArea ID;

    @FXML
    private Label lbQuantVenda;

    @FXML
    private TextArea txtQuantVenda;

    @FXML
    private Button btnPesquisa;

    @FXML
    private TextField pesquisa;

    @FXML
    private Button btnPesquisa2;

    @FXML
    private TextField pesquisa2;

    @FXML
    private MenuBar menu;

    @FXML
    private Menu opcoes;
    @FXML
    private AnchorPane painelProdutos;
    @FXML
    private ScrollPane produtosScrollPane;
    @FXML
    private Label lbDesconto;
    @FXML
    private TextArea descontoTF;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            adicionarEcraPessoalOperadorMenuOpcoes();
            setVisibleFalse();

            List<descontosProduto> descProdutos = getListaDescontosPordutosDistinct();
            for (descontosProduto prod : descProdutos) {
                String idP = prod.getCodProdPa();
                codPP = idP;
                Button botao = new Button(idP);
                botao.setId("botaoVbox");
                botao.setPrefHeight(30);
                botao.setPrefWidth(vBoxProdutos.getPrefWidth());
                botao.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent e) {
                        botao.setId("botaoVboxChangeCollour");
                        setVisibleTrueTabela();
                        listarTabelaDesconto(idP);

                    }
                });

                altura_vbox = altura_vbox + 30;
                vBoxProdutos.getChildren().addAll(botao);
                if (altura_vbox > 468) {
                    vBoxProdutos.setPrefHeight(altura_vbox);
                }

            }
        } catch (Exception ab) {
            mensagemErroFatal();
        }

    }

    private void listarTabelaDesconto(String codigProd) {

        codigoProd = codigProd;
        altura_vbox = 0;

        vBoxDesconto.getChildren().removeAll(vBoxDesconto.getChildren());
        List<descontosProduto> descProd = getListaDescontosPordutosCodProd(codigProd);
        for (descontosProduto dtP : descProd) {

            List<desconto> d = getListaDescontosID(dtP.getIdDesconto());
            for (desconto des : d) {
                int id = des.getIdDesconto();
                String nome = String.valueOf(id) + " - " + String.valueOf(des.getQuantDesconto() + " %");
                Button botao = new Button(nome);
                botao.setId("botaoVbox");
                botao.setPrefHeight(30);
                botao.setPrefWidth(275);
                botao.setPrefWidth(vBoxDesconto.getPrefWidth());
                botao.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent e) {
                        altura_vbox = 0;
                        botao.setId("botaoVboxChangeCollour");
                        setVisibleTrue();

                        List<desconto> desc = desconto.getListaDescontosID(id);
                        desconto dt = desc.get(0);
                        desct = dt.getQuantDesconto();

                        List<descontosProduto> descProdQt = getListaDescontosPordutosIDDesCod(id, codigProd);
                        for (descontosProduto qtP : descProdQt) {
                            qtVenda = qtP.getQuantVend();

                            List<produto> produtos = pesquisarProdutoID(codigProd);
                            for (produto prod : produtos) {
                                ID.setText(codigProd);
                                descricao.setText(prod.getDescricao());
                                txtQuantVenda.setText(String.valueOf(qtVenda));
                                descontoTF.setText(String.valueOf(desct) + " %");
                            }
                        }

                    }
                });
                altura_vbox = altura_vbox + 30;
                vBoxDesconto.getChildren().addAll(botao);
                if (altura_vbox > 468) {
                    vBoxDesconto.setPrefHeight(altura_vbox);
                }

            }
        }
    }

    /**
     * Método para tornar visivel todos os atributos da categoria selecionado e
     * torna não editáveis as textareas
     */
    private void setVisibleTrue() {

        lbID.setVisible(true);
        lbDescricao.setVisible(true);
        lbQuantVenda.setVisible(true);
        lbDesconto.setVisible(true);

        ID.setVisible(true);
        descricao.setVisible(true);
        txtQuantVenda.setVisible(true);
        descontoTF.setVisible(true);

        ID.setEditable(false);
        descricao.setEditable(false);

    }

    private void setVisibleTrueTabela() {

        painelDescontos.setVisible(true);
        descontosScrollPane.setVisible(true);
        vBoxDesconto.setVisible(true);

        pesquisa2.setVisible(true);
        btnPesquisa2.setVisible(true);

    }

    /**
     * Método para não tornar visivel os atributos da categoria selecionado.
     */
    private void setVisibleFalse() {

        lbID.setVisible(false);
        lbDescricao.setVisible(false);
        lbQuantVenda.setVisible(false);
        lbDesconto.setVisible(false);

        ID.setVisible(false);
        descricao.setVisible(false);
        txtQuantVenda.setVisible(false);
        descontoTF.setVisible(false);

        painelDescontos.setVisible(false);
        descontosScrollPane.setVisible(false);
        vBoxDesconto.setVisible(false);

        pesquisa2.setVisible(false);
        btnPesquisa2.setVisible(false);

    }

    /**
     * Método para procurar uma categoria na BD e listar os seus atributos<br>
     * Método valida o id da categoria e verifica se esta existe e depois lista
     * os seus atributos caso a mesma exista.<br>
     * Caso algo corra mal ou a categoria não exista, o utilizador é informado.
     */
    @FXML
    private void pesquisarID(ActionEvent event) throws Exception {

        altura_vbox = 0;

        if (getListaDescontosPordutosCodProd(String.valueOf(pesquisa.getText())).isEmpty() == false) {

            try {

                vBoxProdutos.getChildren().clear();

                setVisibleFalse();

                List<descontosProduto> descProdutos = getListaDescontosPordutosCodProdDistinct(String.valueOf(pesquisa.getText()));
                for (descontosProduto prod : descProdutos) {
                    String idP = prod.getCodProdPa();
                    codPP = idP;
                    Button botao = new Button(idP);
                    botao.setId("botaoVbox");
                    botao.setPrefHeight(30);
                    botao.setPrefWidth(vBoxProdutos.getPrefWidth());
                    botao.setOnAction(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent e) {
                            botao.setId("botaoVboxChangeCollour");
                            setVisibleTrueTabela();
                            listarTabelaDesconto(idP);

                        }
                    });

                    altura_vbox = altura_vbox + 30;
                    vBoxProdutos.getChildren().addAll(botao);
                    if (altura_vbox > 468) {
                        vBoxProdutos.setPrefHeight(altura_vbox);
                    }

                }

            } catch (Exception ab) {
                mensagemErroFatal();
            }

        } else {

            a.setAlertType(Alert.AlertType.INFORMATION);
            a.getButtonTypes().setAll(ButtonType.OK);
            a.setTitle("codigo incorreto ");
            a.setHeaderText("O codigo de produto pode estar incorreto!");
            a.showAndWait();
            fecha();
            listarDescontosAssociadosProdutos listDescP = new listarDescontosAssociadosProdutos();
            listDescP.start(new Stage());
        }
    }

    @FXML
    private void pesquisarIDdesc(ActionEvent event) throws Exception {

        altura_vbox = 0;

        vBoxDesconto.getChildren().clear();
        vBoxDesconto.getChildren().removeAll(vBoxDesconto.getChildren());
        vBoxDesconto.getChildren().removeAll();

        int resulPesq = 0;
        try {

            resulPesq = Integer.parseInt(pesquisa2.getText());

            if ((getListaDescontosID(resulPesq)).isEmpty() == false) {

                try {

                    vBoxDesconto.getChildren().removeAll(vBoxDesconto.getChildren());

                    List<desconto> d = getListaDescontosID(resulPesq);
                    for (desconto des : d) {

                        int id = des.getIdDesconto();
                        String nome = String.valueOf(id) + " - " + String.valueOf(des.getQuantDesconto() + " %");
                        Button botao = new Button(nome);
                        botao.setId("botaoVbox");
                        botao.setPrefHeight(30);
                        botao.setPrefWidth(275);
                        botao.setPrefWidth(vBoxDesconto.getPrefWidth());
                        botao.setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent e) {

                                botao.setId("botaoVboxChangeCollour");
                                setVisibleTrue();

                                List<descontosProduto> descProd = descontosProduto.getListaDescontosPordutosCodProd(codPP);
                                descontosProduto descP = descProd.get(0);
                                int idDesc = descP.getIdDesconto();

                                List<desconto> desc = desconto.getListaDescontosID(idDesc);
                                desconto dt = desc.get(0);
                                desct = dt.getQuantDesconto();

                                List<descontosProduto> descProdQt = getListaDescontosPordutosIDDesCod(id, codigoProd);
                                for (descontosProduto qtP : descProdQt) {
                                    qtVenda = qtP.getQuantVend();

                                    List<produto> produtos = pesquisarProdutoID(codigoProd);
                                    for (produto prod : produtos) {
                                        ID.setText(codigoProd);
                                        descricao.setText(prod.getDescricao());
                                        txtQuantVenda.setText(String.valueOf(qtVenda));
                                        descontoTF.setText(String.valueOf(desct) + " %");
                                    }
                                }
                            }
                        });

                        altura_vbox = altura_vbox + 30;
                        vBoxDesconto.getChildren().addAll(botao);
                        if (altura_vbox > 468) {
                            vBoxDesconto.setPrefHeight(altura_vbox);
                        }

                    }
                } catch (Exception ab) {
                    mensagemErroFatal();
                }

            } else {

                a.setAlertType(Alert.AlertType.INFORMATION);
                a.getButtonTypes().setAll(ButtonType.OK);
                a.setTitle("codigo incorreto ");
                a.setHeaderText("O codigo de desconto pode estar incorreto!");
                a.showAndWait();
                fecha();
                listarDescontosAssociadosProdutos listDescP = new listarDescontosAssociadosProdutos();
                listDescP.start(new Stage());
            }

        } catch (Exception ab) {
            a.setAlertType(Alert.AlertType.INFORMATION);
            a.getButtonTypes().setAll(ButtonType.OK);
            a.setTitle("codigo incorreto ");
            a.setHeaderText("O codigo de desconto pode estar incorreto!");
            Optional<ButtonType> result2 = a.showAndWait();
            fecha();
            listarDescontosAssociadosProdutos listDescP = new listarDescontosAssociadosProdutos();
            listDescP.start(new Stage());
        }

    }

    /**
     * Método para verificar se a Quantidade de venda é valida
     *
     * @param text Quantidade de venda a ser verificado
     * @return boolean se a Quantidade de Venda é valida ou não
     */
    public boolean isValidQuantidadeVenda(String text) {
        try {
            Integer.parseInt(text);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    /**
     * método para fechar o ecrã de listar as categorias
     */
    private static void fecha() {
        listarDescontosAssociadosProdutos.getStage().close();
    }

    /**
     * Método para aceder ao menu do administrador
     */
    @FXML
    void openMenu(ActionEvent event) {
        try {
            if (login.getNivelLogin() == 1) {
                menuOperador menuO = new menuOperador();
                fecha();
                menuO.start(new Stage());
            } else {
                menuAdministrador menuAdmin = new menuAdministrador();
                fecha();
                menuAdmin.start(new Stage());
            }
        } catch (Exception ex) {
            Logger.getLogger(listarDescontosAssociadosProdutosController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Método para encerrar a aplicação
     */
    @FXML
    void sair(ActionEvent event) {
        System.exit(0);
    }

    /**
     * Metodo para terminar sessao do utilizador
     *
     * @param event evento
     */
    @FXML
    void terminarSessao(ActionEvent event) {
        try {
            login.setIdLogin(0);
            login.setNivelLogin(10000);
            login log = new login();
            fecha();
            log.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(listarDescontosAssociadosProdutosController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Método para lançar uam mensagem de erro e recarregar o ecrã caso algo
     * inesperado ocorra com o algoritmo<br>
     * Método usado quando as falhas são graves mas não graves de mais que a
     * funcionalidade se encontre inutilizável.<br>
     * É a mensagem de erro de severidade mais leve neste ecrã
     */
    public static void mensagemErroRecarregarEcra() {
        Alert alertaNovo = new Alert(Alert.AlertType.NONE);
        alertaNovo.setAlertType(Alert.AlertType.ERROR);
        alertaNovo.getButtonTypes().setAll(ButtonType.OK);
        alertaNovo.setTitle("ERRO Lógica Aplicação");
        alertaNovo.setHeaderText("Os dados processados não são válidos");
        alertaNovo.setContentText("Ocorreu um erro ao validar os dados selecionados ou estes podem estar inacessíveis. "
                + "\nTente novamente e caso não conseguir contacte o administrador");
        Optional<ButtonType> resulta = alertaNovo.showAndWait();
        if (resulta.get() == ButtonType.OK) {
            recarregarEcra();
        }

    }

    /**
     * Método para recarregar o ecrã de inserir categorias
     */
    private static void recarregarEcra() {
        listarDescontosAssociadosProdutos listDescP = new listarDescontosAssociadosProdutos();
        fecha();
        try {
            listDescP.start(new Stage());
        } catch (Exception ex) {
            mensagemErroFatal();
        }
    }

    /**
     * Mensagem de erro para tentar fechar em segurança a aplicação no caso
     * desta encontrar um erro muito grave<br>
     * Se este erro for lançado é porque algo inesperado ocorreu (ex: falha de
     * acesso à base de dados, lançamento de alguns excepções em métodos
     * críticos ou falhas gerais e básicas de programação)<br>
     * É a mensagem de erro de severidade mais elevada deste ecrã
     */
    public static void mensagemErroFatal() {
        Alert a = new Alert(Alert.AlertType.NONE);
        a.setAlertType(Alert.AlertType.ERROR);
        a.setTitle("ERRO Fatal");
        a.setHeaderText("A aplicação comportou-se de forma inesperada!");
        a.setContentText("Encerrando a aplicação");
        a.getButtonTypes().setAll(ButtonType.OK);
        Optional<ButtonType> result3 = a.showAndWait();
        if (result3.get() == ButtonType.OK) {
            System.exit(0);
        }
    }

    /**
     * método para substituir o conteudo do menu de opções para colocar as
     * opções por ordem caso seja o operador a abrir o ecrã
     */
    private void adicionarEcraPessoalOperadorMenuOpcoes() {
        //substituir o conteudo do menu de opções para colocar as opções por ordem caso seja o operador a abrir o ecrã
        List<utilizador> operadores = procurarEmail(login.getIdLogin());
        for (utilizador operador : operadores) {
            if (operador.getNivel() == 1) {
                opcoes.getItems().clear();
                MenuItem ePOp = new MenuItem("Ecrã Pessoal");
                MenuItem terminarSessao = new MenuItem("Terminar Sessão");
                MenuItem sair = new MenuItem("Sair");
                opcoes.getItems().add(ePOp);
                opcoes.getItems().add(terminarSessao);
                opcoes.getItems().add(sair);
                ePOp.setOnAction((ActionEvent e) -> {
                    try {
                        ecraPessoalOperador ePO = new ecraPessoalOperador();
                        fecha();
                        ePO.start(new Stage());
                    } catch (Exception ex) {
                        Logger.getLogger(listarDescontosAssociadosProdutosController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
                terminarSessao.setOnAction((ActionEvent e) -> {
                    try {
                        login.setIdLogin(0);
                        login.setNivelLogin(10000);
                        login log = new login();
                        fecha();
                        log.start(new Stage());
                    } catch (Exception ex) {
                        Logger.getLogger(listarDescontosAssociadosProdutosController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
                sair.setOnAction((ActionEvent e) -> {
                    System.exit(0);
                });
            }
        }
    }

}
