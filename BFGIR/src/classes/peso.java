/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import bfgir.BDConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * Classe peso com metodos de acesso a tabela de peso na BD
 */
public class peso {

    private String codPeso;
    private float peso;
    private String tipoPeso;

    /**
     * Construtor da classe peso
     *
     * @param codPeso codigo do peso
     * @param peso peso
     * @param tipoPeso tipo do peso
     */
    private peso(String codPeso, float peso, String tipoPeso) {
        this.codPeso = codPeso;
        this.peso = peso;
        this.tipoPeso = tipoPeso;
    }

    /**
     *Construtor da classe peso
     */
    public peso() {

    }

    /**
     * @return the codPeso
     */
    public String getCodPeso() {
        return codPeso;
    }

    /**
     * @param codPeso the codPeso to set
     */
    public void setCodPeso(String codPeso) {
        this.codPeso = codPeso;
    }

    /**
     * @return the peso
     */
    public float getPeso() {
        return peso;
    }

    /**
     * @param peso the peso to set
     */
    public void setPeso(float peso) {
        this.peso = peso;
    }

    /**
     * @return the tipoPeso
     */
    public String getTipoPeso() {
        return tipoPeso;
    }

    /**
     * @param tipoPeso the tipoPeso to set
     */
    public void setTipoPeso(String tipoPeso) {
        this.tipoPeso = tipoPeso;
    }

    /**
     * Método para selecionar todos os atributos da tabela peso na BD<br>
     *
     * Este método recebe por parametro o Codigo do peso (codPeso). Posto isto
     * faz um select à BD para pesquisar o peso do produto consoante o codigo do
     * produto associado ao peso.<br>
     *
     * @param cod codigo do peso
     * @return peso

     */
    public static float pesquisarPeso(String cod) {
        float peso = 0;

        String sql = "Select * from pesos where codPeso = (?)";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setString(1, cod);
            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                peso = results.getFloat(2);
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return peso;
    }

}
