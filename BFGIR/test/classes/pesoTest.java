/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static classes.peso.*;

/**
 *
 * @author bruno
 */
public class pesoTest {

    public pesoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    @Test
    public void pesoTest() {

        System.out.println("-> Teste pesquisar peso por código \nA verificar peso...");
        
        peso novoPeso = new peso();

        novoPeso.setCodPeso("108128028");
        novoPeso.setPeso((float) 6.88);
        novoPeso.setTipoPeso("gramas");

        float pesoProdutoNovo = novoPeso.getPeso();

        float pesoRecebido = peso.pesquisarPeso("108128028");

        assertEquals(pesoProdutoNovo, pesoRecebido, pesoRecebido);
        
        if(pesoProdutoNovo == pesoRecebido){
            System.out.println("Teste concluído com sucesso!");
        } else {
            System.out.println("O teste falhou!");
        }

    }
}
