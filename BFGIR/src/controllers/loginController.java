/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import bfgir.login;
import bfgir.menuAdministrador;
import bfgir.menuFornecedor;
import bfgir.menuOperador;
import classes.passwordUtils;
import classes.utilizador;
import static classes.utilizador.procurarDadosLogin;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 */
public class loginController implements Initializable {

    @FXML
    private Button buttonEntrar;

    Alert a = new Alert(Alert.AlertType.NONE);
    @FXML
    private TextField email;
    @FXML
    private PasswordField password;

    /**
     * Initializes the controller class.
     *
     * @param url url
     * @param rb rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    /**
     * Inicia o metodo loginMetodo
     * 
     * @param event evento
     */
    @FXML
    private void login(ActionEvent event) {
        loginMetodo();
    }

    /**
     * Este metodo permite que ao fazer o login carregando na tecla 'ENTER' depois de inserir a password, faça o login
     * 
     * @param event evento
     */
    @FXML
    private void passwordOnKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
            loginMetodo();
        }
    }

    /**
     * Este método vai fazer verificação se as credencias de login do utilizador estão corretas e se o seu estado é 'ativo'
     * depois vai buscar o nivel do utilizador e faz um case para inisiar sesão com esse nivel
     */
    private void loginMetodo() {
        int valLogin = 0;
        int valLogin2 = 0;

        try {
            List<utilizador> utilizadores = procurarDadosLogin(email.getText());
            for (utilizador utilizador : utilizadores) {
                valLogin2 = 1;

                boolean passwordMatch = passwordUtils.verifyUserPassword(password.getText(), utilizador.getPassword(), utilizador.getSalt());
                if (passwordMatch) {

                    if ("ativo".equals(utilizador.getEstado())) {
                        valLogin = 1;

                        login.setIdLogin(utilizador.getIdUtilizador());

                        switch (utilizador.getNivel()) {
                            case 0:
                                login.setNivelLogin(0);
                                menuFornecedor menuF = new menuFornecedor();
                                fecha();
                                menuF.start(new Stage());
                                break;
                            case 1:
                                login.setNivelLogin(1);
                                menuOperador menuO = new menuOperador();
                                fecha();
                                menuO.start(new Stage());
                                break;

                            case 2:
                                login.setNivelLogin(2);
                                menuAdministrador menuA = new menuAdministrador();
                                fecha();
                                menuA.start(new Stage());
                                break;
                            case 10:
                                a.setAlertType(Alert.AlertType.ERROR);
                                a.setTitle("Login");
                                a.setHeaderText("Erro ao afetuar Login!");
                                a.setContentText("A sua conta ainda não foi validada!");
                                a.show();
                                break;
                        }
                    }
                    System.out.println("");
                    if (valLogin == 0) {
                        a.setAlertType(Alert.AlertType.ERROR);
                        a.setTitle("Login");
                        a.setHeaderText("Erro ao afetuar Login!");
                        a.setContentText("A sua conta encontra-se desativa."
                                + "\nContacte o administrador para ativar a sua conta!");
                        a.show();
                    }
                } else {
                    a.setAlertType(Alert.AlertType.ERROR);
                    a.setTitle("Login");
                    a.setHeaderText("Erro ao afetuar Login!");
                    a.setContentText("Dados incorretos!");
                    a.show();
                }

            }
            if (valLogin2 == 0) {
                a.setAlertType(Alert.AlertType.ERROR);
                a.setTitle("Login");
                a.setHeaderText("Erro ao afetuar Login!");
                a.setContentText("Dados inexistentes na BD!");
                a.show();
            }

        } catch (SQLException ex) {
            Logger.getLogger(loginController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(loginController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(loginController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * fechar
     */
    public void fecha() {
        login.getStage().close();
    }
}
