/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author gonca
 */
public class descontosProdutoTest {

    public descontosProdutoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getIdDesconto method, of class descontosProduto.
     */
    @Test
    public void testGetIdDesconto() {
        System.out.println("getIdDesconto");
        descontosProduto instance = new descontosProduto();
        instance.setIdDesconto(1);
        int expResult = 1;
        int result = instance.getIdDesconto();
        if (expResult == result) {
            System.out.println("O método passou com sucesso");
        } else {
            fail("The test case is a prototype.");
        }
        // TODO review the generated test code and remove the default call to fail.

    }

    /**
     * Test of setIdDesconto method, of class descontosProduto.
     */
    @Test
    public void testSetIdDesconto() {
        System.out.println("setIdDesconto");
        descontosProduto instance = new descontosProduto();
        int expResult = 1;
        instance.setIdDesconto(expResult);
        int result = instance.getIdDesconto();
        if (expResult == result) {
            System.out.println("O método passou com sucesso");
        } else {
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of getCodProdPa method, of class descontosProduto.
     */
    @Test
    public void testGetCodProdPa() {
        System.out.println("getCodPordPa");
        descontosProduto instance = new descontosProduto();
        instance.setCodProdPa("207871295");
        String expResult = "207871295";
        String result = instance.getCodProdPa();
        if (expResult.equals(result)) {
            System.out.println("O método passou com sucesso");
        } else {
            fail("The test case is a prototype.");
            System.out.println(result);
        }
    }

    /**
     * Test of setCodProdPa method, of class descontosProduto.
     */
    @Test
    public void testSetCodProdPa() {
        System.out.println("setCodPordPa");
        descontosProduto instance = new descontosProduto();
        String expResult = "207871295";
        instance.setCodProdPa(expResult);
        String result = instance.getCodProdPa();
        if (expResult.equals(result)) {
            System.out.println("O método passou com sucesso");
        } else {
            fail("The test case is a prototype.");
            System.out.println(result);
        }
    }

    /**
     * Test of getQuantVend method, of class descontosProduto.
     */
    @Test
    public void testGetQuantVend() {
        System.out.println("getQuantVend");
        descontosProduto instance = new descontosProduto();
        instance.setQuantVend(5);
        int quant = 5;
        int result = instance.getQuantVend();
        if (quant == result) {
            System.out.println("O método passou com sucesso");
        } else {
            fail("The test case is a prototype.");
            System.out.println(result);
        }
    }

    /**
     * Test of setQuantVend method, of class descontosProduto.
     */
    @Test
    public void testSetQuantVend() {

        System.out.println("setQuantVend");
        descontosProduto instance = new descontosProduto();
        int quant = 5;
        instance.setQuantVend(quant);
        int result = instance.getQuantVend();
        if (quant == result) {
            System.out.println("O método passou com sucesso");
        } else {
            fail("The test case is a prototype.");
            System.out.println(result);
        }
    }

    /**
     * Test of getListaDescontosPordutosIDDes method, of class descontosProduto.
     */
    @Test
    public void testGetListaDescontosPordutosIDDes() {
        System.out.println("getListaDescontosPordutosIDDes");
        int idDesc = 1;
        boolean teste = false;
        int fai = 0;

        descontosProduto instance = new descontosProduto();
        List<descontosProduto> result = descontosProduto.getListaDescontosPordutosIDDes(idDesc);
        do {
            for (descontosProduto d : result) {
                String exCod = d.getCodProdPa();
                int exQuant = d.getQuantVend();
                int exId = d.getIdDesconto();
                String cod = "207871295";
                int quant = 60;
                int id = 1;

                if (exCod.equals(cod) && exQuant == quant && exId == id) {
                    System.out.println("O teste passou com sucesso");
                    teste = true;
                    fai = 1;
                }
            }

        } while (teste == false);

        if (fai == 0) {
            fail("The test case is a prototype.");
        }

    }

    /**
     * Test of getListaDescontosPordutosIDDesCod method, of class
     * descontosProduto.
     */
    @Test
    public void testGetListaDescontosPordutosIDDesCod() {
        System.out.println("getListaDescontosPordutosIDDesCod");
        int idDesc = 1;
        String codProd = "207871295";
        boolean teste = false;
        int fai = 0;

        descontosProduto instance = new descontosProduto();
        List<descontosProduto> result = descontosProduto.getListaDescontosPordutosIDDesCod(idDesc, codProd);
        do {
            for (descontosProduto d : result) {
                String exCod = d.getCodProdPa();
                int exQuant = d.getQuantVend();
                int exId = d.getIdDesconto();
                String cod = "207871295";
                int quant = 60;
                int id = 1;

                if (exCod.equals(cod) && exQuant == quant && exId == id) {
                    System.out.println("O teste passou com sucesso");
                    teste = true;
                    fai = 1;
                }
            }

        } while (teste == false);

        if (fai == 0) {
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of getListaDescontosPordutos method, of class descontosProduto.
     */
    @Test
    public void testGetListaDescontosPordutos() {
        System.out.println("getListaDescontosPordutos");
        boolean teste = false;
        int fai = 0;

        descontosProduto instance = new descontosProduto();
        List<descontosProduto> result = descontosProduto.getListaDescontosPordutos();
        do {
            for (descontosProduto d : result) {
                String exCod = d.getCodProdPa();
                int exQuant = d.getQuantVend();
                int exId = d.getIdDesconto();
                String cod = "207871295";
                int quant = 60;
                int id = 1;

                if (exCod.equals(cod) && exQuant == quant && exId == id) {
                    System.out.println("O teste passou com sucesso");
                    teste = true;
                    fai = 1;
                }
            }

        } while (teste == false);

        if (fai == 0) {
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of inserirDescontos method, of class descontosProduto.
     */
    @Test
    public void testInserirDescontos() {
        System.out.println("inserirDescontos");
        int idDesc = 0;
        String CodProd = "";
        int quant = 0;

        try {
            descontosProduto.inserirDescontos(idDesc, CodProd, quant);
        } catch (Exception e) {
            fail("The test case is a prototype.");
            // Another code
        }
        // TODO review the generated test code and remove the default call to fail.

    }

    /**
     * Test of eliminarDescontosProdutos method, of class descontosProduto.
     */
    @Test
    public void testEliminarDescontosProdutos() {
        int idDesc = 202020;
        String CodProd = "202020";
        int quant = 202020;

        try {
            descontosProduto.inserirDescontos(idDesc, CodProd, quant);
        } catch (Exception e) {
            fail("The test case is a prototype.");
            // Another code
        }
        System.out.println("eliminarDescontosProdutos");
        int id = 202020;
        String codProd = "202020";

        try {
            descontosProduto.eliminarDescontosProdutos(id, codProd);
        } catch (Exception e) {
            fail("The test case is a prototype.");
        }

    }

    /**
     * Test of getListaDescontosPordutosCodProd method, of class
     * descontosProduto.
     */
    @Test
    public void testGetListaDescontosPordutosCodProd() {
        System.out.println("getListaDescontosPordutosCodProd");

        String codProd = "207871295";
        boolean teste = false;
        int fai = 0;

        descontosProduto instance = new descontosProduto();
        List<descontosProduto> result = descontosProduto.getListaDescontosPordutosCodProd(codProd);
        do {
            for (descontosProduto d : result) {
                String exCod = d.getCodProdPa();
                int exQuant = d.getQuantVend();
                int exId = d.getIdDesconto();
                String cod = "207871295";
                int quant = 60;
                int id = 1;

                if (exCod.equals(cod) && exQuant == quant && exId == id) {
                    System.out.println("O teste passou com sucesso");
                    teste = true;
                    fai = 1;
                }
            }

        } while (teste == false);

        if (fai == 0) {
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of alterarQuantidadeVenda method, of class descontosProduto.
     */
    @Test
    public void testAlterarQuantidadeVenda() {
        System.out.println("alterarQuantidadeVenda");
        String quantVenda = "50";
        int idDesconto = 7;
        String codProdPa = "698473";
        try{
           descontosProduto.alterarQuantidadeVenda(quantVenda, idDesconto, codProdPa); 
        }catch (Exception e) {
            fail("The test case is a prototype.");
    }
    }
    /**
     * Test of alterarDescontoProduto method, of class descontosProduto.
     */
    @Test
    public void testAlterarDescontoProduto() {
     
        System.out.println("alterarDescontoProduto");
        int idDesc = 7;
        int idDesconto = 8;
        String codProdPa = "";
        
        try{
        descontosProduto.alterarDescontoProduto(idDesc, idDesconto, codProdPa);
        }catch (Exception e) {
            fail("The test case is a prototype.");
        // TODO review the generated test code and remove the default call to fail.
       
    }
    }
}
