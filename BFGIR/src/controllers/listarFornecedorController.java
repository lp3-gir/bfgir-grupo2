/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import bfgir.listarFornecedores;
import bfgir.login;
import bfgir.menuAdministrador;
import bfgir.menuOperador;
import classes.codigoPostal;
import static classes.codigoPostal.procurarCodPostal;
import classes.fornecedor;
import static classes.fornecedor.listarFornecedor;
import static classes.fornecedor.listarFornecedorTodos;
import classes.utilizador;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * FXML Controller class
 */
public class listarFornecedorController implements Initializable {

    Alert a = new Alert(Alert.AlertType.NONE);
    @FXML
    private AnchorPane painelFornecedores;
    @FXML
    private ScrollPane fornecedorScrollPane;
    @FXML
    private VBox vBoxFornecedores;
    @FXML
    private Label lbNome;
    @FXML
    private Label lbCodigoPostal;
    @FXML
    private Label lbMorada;
    @FXML
    private Label lbContribuinte;
    @FXML
    private Label lbPais;
    @FXML
    private Label lbEstado;
    @FXML
    private Label lbEmail;
    @FXML
    private Label lbCidade;
    @FXML
    private TextArea nome;
    @FXML
    private TextArea codigoPostal;
    @FXML
    private TextArea morada;
    @FXML
    private TextArea contribuinte;
    @FXML
    private TextArea pais;
    @FXML
    private TextArea estado;
    @FXML
    private TextArea email;
    @FXML
    private TextArea cidade;
    private VBox vBox;
    @FXML
    private MenuBar menu;

    private int altura_vbox = 0;

    /**
     * Método Intialize
     *
     * Neste método é criada uma lista com todos os fornecedores ordenados por
     * ordem crescente do seu ID de Utilizador obtidos pelo método
     * listarFornecedorTodos.<br>
     * De seguida para cada elemento dessa lista de fornecedores é criado um
     * botão com o ID Utilizador do fornecedor.<br>
     * Depois de selecionar um fornecedor é feito um select com o codigo do
     * mesmo (idUtilizador) através do método listarFornecedor que retorna o
     * fornecedor correspondente ao idUtilizador, este fornecedor é descriminado
     * por cada atributo e listado no ecrã.
     *
     * @param url url
     * @param rb rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        lbNome.setVisible(false);
        lbMorada.setVisible(false);
        lbCodigoPostal.setVisible(false);
        lbContribuinte.setVisible(false);
        lbPais.setVisible(false);
        lbEstado.setVisible(false);
        lbEmail.setVisible(false);
        lbCidade.setVisible(false);

        nome.setVisible(false);
        morada.setVisible(false);
        codigoPostal.setVisible(false);
        contribuinte.setVisible(false);
        pais.setVisible(false);
        estado.setVisible(false);
        email.setVisible(false);
        cidade.setVisible(false);

        List<fornecedor> forn = listarFornecedorTodos();
        for (fornecedor fornecedores : forn) {

            int id = fornecedores.getIdUtilizador();
            Button botao = new Button(String.valueOf(id));
            botao.setId("botaoVbox");
            botao.setPrefHeight(30);
            botao.setPrefWidth(275);

            botao.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {
                    botao.setId("botaoVboxChangeCollour");
                    setVisibleTrue();
                    List<fornecedor> fornecedores = listarFornecedor(id);
                    List<utilizador> utEm = utilizador.procurarEmail(id);
                    utilizador ut = utEm.get(0);
                    String email123 = ut.getEmail();
                    fornecedor forn = fornecedores.get(0);
                    String codP = forn.getCodPostal();
                    List<codigoPostal> codCidade = procurarCodPostal(codP);
                    codigoPostal cdO = codCidade.get(0);
                    String city = cdO.getCidade();

                    for (fornecedor fornecedor : fornecedores) {
                        nome.setText(fornecedor.getNome());
                        morada.setText(fornecedor.getMorada());
                        codigoPostal.setText(fornecedor.getCodPostal());
                        contribuinte.setText(String.valueOf(fornecedor.getContribuinte()));
                        pais.setText(fornecedor.getPais());
                        estado.setText(fornecedor.getEstado());
                        email.setText(email123);
                        cidade.setText(city);
                    }

                }
            });

            altura_vbox = altura_vbox + 30;
            vBoxFornecedores.getChildren().addAll(botao);
            if (altura_vbox > 550) {
                vBoxFornecedores.setPrefHeight(altura_vbox);
            }

        }

    }

    /**
     * Método para tornar visivel todos os atributos do fornecedor selecionado.
     */
    private void setVisibleTrue() {

        lbNome.setVisible(true);
        lbMorada.setVisible(true);
        lbCodigoPostal.setVisible(true);
        lbContribuinte.setVisible(true);
        lbPais.setVisible(true);
        lbEstado.setVisible(true);
        lbEmail.setVisible(true);
        lbCidade.setVisible(true);

        nome.setVisible(true);
        morada.setVisible(true);
        codigoPostal.setVisible(true);
        contribuinte.setVisible(true);
        pais.setVisible(true);
        estado.setVisible(true);
        email.setVisible(true);
        cidade.setVisible(true);

    }

    /**
     * método para fechar o ecrã de listar os fornecedores
     */
    private void fecha() {
        listarFornecedores.getStage().close();
    }

    /**
     * Metodo para terminar sessao do utilizador
     *
     * @param event evento
     */
    @FXML
    private void terminarSessao(ActionEvent event) {
        try {
            login.setIdLogin(0);
            login.setNivelLogin(10000);
            login log = new login();
            fecha();
            log.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuAdministradorController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Método para encerrar a aplicação
     */
    @FXML
    private void sair(ActionEvent event) {
        System.exit(0);
    }

    /**
     * Método para aceder ao menu do administrador
     */
    @FXML
    private void openMenu(ActionEvent event) {
        try {
            if (login.getNivelLogin() == 1) {
                menuOperador menuO = new menuOperador();
                fecha();
                menuO.start(new Stage());
            } else {
                menuAdministrador menuAdmin = new menuAdministrador();
                fecha();
                menuAdmin.start(new Stage());
            }
        } catch (Exception ex) {
            Logger.getLogger(listarFornecedorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
