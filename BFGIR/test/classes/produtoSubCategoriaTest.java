/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author chico
 */
public class produtoSubCategoriaTest {
    
    /**
     * construtor da classe produto sub categoria teste
     */
    public produtoSubCategoriaTest() {
    }
    
    /**
     * set up class
     */
    @BeforeClass
    public static void setUpClass() {
    }
    
    /**
     * tear down class
     */
    @AfterClass
    public static void tearDownClass() {
    }
    
    /**
     * set up
     */
    @Before
    public void setUp() {
    }
    
    /**
     * set up
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getCodProdPa method, of class produtoSubCategoria.
     */
    @Test
    public void testGetCodProdPa() {
        System.out.println("getCodProdPa");
        produtoSubCategoria instance = new produtoSubCategoria();
        instance.setCodProdPa("123456");
        String expResult = "123456";
        String result = instance.getCodProdPa();
        if (expResult == result) {
            System.out.println("O método passou com sucesso");
        } else {
            fail("The test case is a prototype.");
        }
        
    }

    /**
     * Test of setCodProdPa method, of class produtoSubCategoria.
     */
    @Test
    public void testSetCodProdPa() {
        System.out.println("setCodProdPa");
        produtoSubCategoria instance = new produtoSubCategoria();
        String expResult  = "123456";
        instance.setCodProdPa(expResult);
        String result = instance.getCodProdPa();
        if (expResult == result) {
            System.out.println("O método passou com sucesso");
        } else {
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of getNivel1 method, of class produtoSubCategoria.
     */
    @Test
    public void testGetNivel1() {
        System.out.println("getNivel1");
        produtoSubCategoria instance = new produtoSubCategoria();
        instance.setNivel1(1);
        int expResult = 1;
        int result = instance.getNivel1();
        if (expResult == result) {
            System.out.println("O método passou com sucesso");
        } else {
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of setNivel1 method, of class produtoSubCategoria.
     */
    @Test
    public void testSetNivel1() {
        System.out.println("setNivel1");
        produtoSubCategoria instance = new produtoSubCategoria();
        int expResult  = 1;
        instance.setNivel1(expResult);
        int result = instance.getNivel1();
        if (expResult == result) {
            System.out.println("O método passou com sucesso");
        } else {
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of getNivel2 method, of class produtoSubCategoria.
     */
    @Test
    public void testGetNivel2() {
        System.out.println("getNivel2");
        produtoSubCategoria instance = new produtoSubCategoria();
        instance.setNivel2(2);
        int expResult = 2;
        int result = instance.getNivel2();
        if (expResult == result) {
            System.out.println("O método passou com sucesso");
        } else {
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of setNivel2 method, of class produtoSubCategoria.
     */
    @Test
    public void testSetNivel2() {
        System.out.println("setNivel2");
        produtoSubCategoria instance = new produtoSubCategoria();
        int expResult  = 2;
        instance.setNivel2(expResult);
        int result = instance.getNivel2();
        if (expResult == result) {
            System.out.println("O método passou com sucesso");
        } else {
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of getNivel3 method, of class produtoSubCategoria.
     */
    @Test
    public void testGetNivel3() {
        System.out.println("getNivel3");
        produtoSubCategoria instance = new produtoSubCategoria();
        instance.setNivel3(3);
        int expResult = 3;
        int result = instance.getNivel3();
        if (expResult == result) {
            System.out.println("O método passou com sucesso");
        } else {
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of setNivel3 method, of class produtoSubCategoria.
     */
    @Test
    public void testSetNivel3() {
        System.out.println("setNivel3");
        produtoSubCategoria instance = new produtoSubCategoria();
        int expResult  = 3;
        instance.setNivel3(expResult);
        int result = instance.getNivel3();
        if (expResult == result) {
            System.out.println("O método passou com sucesso");
        } else {
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of getIdProdSub method, of class produtoSubCategoria.
     */
    @Test
    public void testGetIdProdSub() {
        System.out.println("getIdprodSub");
        produtoSubCategoria instance = new produtoSubCategoria();
        instance.setIdProdSub(3);
        int expResult = 3;
        int result = instance.getIdProdSub();
        if (expResult == result) {
            System.out.println("O método passou com sucesso");
        } else {
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of setIdProdSub method, of class produtoSubCategoria.
     */
    @Test
    public void testSetIdProdSub() {
        System.out.println("setIdProdSub");
        produtoSubCategoria instance = new produtoSubCategoria();
        int expResult  = 3;
        instance.setIdProdSub(expResult);
        int result = instance.getIdProdSub();
        if (expResult == result) {
            System.out.println("O método passou com sucesso");
        } else {
            fail("The test case is a prototype.");
        }
    }
    
}
