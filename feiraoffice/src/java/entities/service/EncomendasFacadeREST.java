/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.service;

import entities.Encomendas;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author ruben
 */
@Stateless
@Path("/encomendas/")
public class EncomendasFacadeREST extends AbstractFacade<Encomendas> {

    @PersistenceContext(unitName = "feiraofficePU")
    private EntityManager em;

    /**
     *
     */
    public EncomendasFacadeREST() {
        super(Encomendas.class);
    }

    /**
     *
     * @param entity recebe uma entidade do tipo encomendas
     */
    @POST
    @Override
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void create(Encomendas entity) {
        super.create(entity);
    }

    /**
     *
     * @param id recebe um id de encomeda
     * @param entity recebe uma entidade do tipo encomendas
     */
    @PUT
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void edit(@PathParam("id") Integer id, Encomendas entity) {
        super.edit(entity);
    }

    /**
     *
     * @param id recebe um id de encomeda
     */
    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        super.remove(super.find(id));
    }

    /**
     *
     * @param id recebe um id de encomeda
     * @return um objeto do tipo encomenda
     */
    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Encomendas find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    /**
     *
     * @return uma lista de objetos do tipo encomendas
     */
    @GET
    @Override
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Encomendas> findAll() {
        return super.findAll();
    }

    /**
     *
     * @return a entidade encomendas
     */
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
