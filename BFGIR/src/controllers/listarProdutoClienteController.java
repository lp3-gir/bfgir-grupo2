/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import bfgir.ecraPessoalOperador;
import bfgir.listarProdutoCliente;
import bfgir.login;
import bfgir.menuAdministrador;
import bfgir.menuOperador;
import classes.cliente;
import static classes.cliente.listarClienteAct;
import classes.clienteProduto;
import static classes.clienteProduto.getClienteProdutoCodProdPaIdCliente;
import static classes.clienteProduto.prodPorIdCliente;
import classes.produto;
import static classes.produto.pesquisarProdutoID;
import classes.utilizador;
import static classes.utilizador.procurarEmail;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 */
public class listarProdutoClienteController implements Initializable {

    @FXML
    private Pane painelProdutosClientes;
    @FXML
    private ScrollPane produtos;
    private int altura_vbox = 0;
    @FXML
    private VBox vBoxProdutos;
    @FXML
    private ScrollPane produtosCliente;
    @FXML
    private VBox vBoxClientes;
    @FXML
    private Menu opcoes;
    @FXML
    private MenuBar menu;

    int ids;
    @FXML
    private TextField codClientePesquisar;
    @FXML
    private Button pesquisarCodCliente;
    @FXML
    private ScrollPane produtosInfo;
    @FXML
    private VBox vBoxProdutosInfo;
    @FXML
    private TextField codProdutoPesquisar;
    @FXML
    private Button pesquisarCodProduto;

    Alert a2 = new Alert(Alert.AlertType.NONE);

    /**
     * Initializes the controller class. Lista por id e nome todos os clientes.
     * Cria tambem um botão para cada cliente e com o seu id deCliente e nome
     *
     * @param url url
     * @param rb rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        codProdutoPesquisar.setVisible(false);
        pesquisarCodProduto.setVisible(false);

        adicionarEcraPessoalOperadorMenuOpcoes();
        List<cliente> cl = listarClienteAct();

        for (cliente cli : cl) {

            int id = cli.getIdCliente();
            ids = id;
            String nome = cli.getNome() + " - " + cli.getIdCliente();
            Button botao = new Button(nome);
            botao.setId("botaoVbox");
            botao.setPrefHeight(30);
            botao.setPrefWidth(275);
            botao.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {
                    botao.setId("botaoVboxChangeCollour");
                    listarProdutos(id);
                    codProdutoPesquisar.setVisible(true);
                    pesquisarCodProduto.setVisible(true);
                    ids = id;
                }
            });

            vBoxClientes.getChildren().addAll(botao);
            if (altura_vbox > 600) {
                vBoxClientes.setPrefWidth(218);
                vBoxClientes.setPrefHeight(altura_vbox);
            }

        }

    }

    /**
     * Metodo para terminar sessao do utilizador
     *
     * @param event evento
     */
    @FXML
    private void terminarSessao(ActionEvent event) {
        try {
            login.setIdLogin(0);
            login.setNivelLogin(10000);
            login log = new login();
            fecha();
            log.start(new Stage());

        } catch (Exception ex) {
            Logger.getLogger(menuAdministradorController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Método para encerrar a aplicação
     */
    @FXML
    private void sair(ActionEvent event) {
        System.exit(0);
    }

    /**
     * Método para aceder ao menu do operador ou do administrador conforme o seu
     * nivel de login
     */
    @FXML
    private void openMenu(ActionEvent event) {
        try {
            if (login.getNivelLogin() == 1) {
                menuOperador menuO = new menuOperador();
                fecha();
                menuO.start(new Stage());
            } else {
                menuAdministrador menuAdmin = new menuAdministrador();
                fecha();
                menuAdmin.start(new Stage());
            }
        } catch (Exception ex) {
            Logger.getLogger(eliminarFornecedorController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Este metodo lista por codigo do produto e a sua descrição de todos os
     * produtos associado a um cliente. Cria tambem um botão para cada produto
     * com o seu codigo e descreção
     *
     * @param ids parametro com id do cliente
     */
    private void listarProdutos(int ids) {
        vBoxProdutos.getChildren().removeAll(vBoxProdutos.getChildren());
        List<clienteProduto> cpl = prodPorIdCliente(ids);
        for (clienteProduto cp : cpl) {
            List<produto> prod = pesquisarProdutoID(cp.getCodProdPa());
            for (produto pr : prod) {
                String id = pr.getCodProdPa();
                String codpr = pr.getCodProdPa();
                String nome = pr.getCodProdPa() + " - " + pr.getDescricao();
                Button botao = new Button(nome);
                botao.setId("botaoVbox");
                botao.setPrefHeight(30);
                botao.setPrefWidth(570);
                botao.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent e) {
                        botao.setId("botaoVboxChangeCollour");
                        listarInfoProdutos(pr.getCodProdPa(), ids);

                    }
                });

                vBoxProdutos.getChildren().addAll(botao);
                if (altura_vbox > 550) {
                    vBoxProdutos.setPrefHeight(altura_vbox);
                }

            }

        }
    }

    /**
     *
     * Este metodo vai listar informação do prudoto como o precço base. este
     * metodo recebe por parametros o codigo do produto e o id cliente para
     * pesquisar as suas informações
     *
     * @param codProd codigo produto
     * @param ide id cliente
     */
    public void listarInfoProdutos(String codProd, int ide) {
        vBoxProdutosInfo.getChildren().removeAll(vBoxProdutosInfo.getChildren());
        List<produto> prod = pesquisarProdutoID(codProd);
        for (produto pr : prod) {
            String id = pr.getCodProdPa();
            String codpr = pr.getCodProdPa();
            String nome = pr.getCodProdPa();
            Label label1;
            label1 = new Label("Preço Base: " + pr.getPrecoUnitSIVA() + " EUR");

            vBoxProdutosInfo.getChildren().addAll(label1);
            if (altura_vbox > 550) {
                vBoxProdutosInfo.setPrefHeight(altura_vbox);
            }

        }

        List<clienteProduto> clienteProd = getClienteProdutoCodProdPaIdCliente(ide, codProd);
        for (clienteProduto prs : clienteProd) {
            String id = prs.getCodProdPa();
            String codpr = prs.getCodProdPa();
            String nome = prs.getCodProdPa();
            Label label2;
            label2 = new Label("Preço Cliente: " + prs.getPrecoUnitSIVA() + " EUR");

            vBoxProdutosInfo.getChildren().addAll(label2);
            if (altura_vbox > 550) {
                vBoxProdutosInfo.setPrefHeight(altura_vbox);
            }

        }

    }

    /**
     * Este metodo lista o produto pesquisado pela intrudoção por codigo do
     * produto e a sua descrição de todos os produtos associado a um cliente.
     * Cria tambem um botão para cada produto com o seu codigo e descreção
     *
     * @param codProd codigo do produto
     */
    public void listarProdutoPesquisa(String codProd) {
        vBoxProdutos.getChildren().removeAll(vBoxProdutos.getChildren());
        List<produto> prod = pesquisarProdutoID(codProd);
        for (produto pr : prod) {
            String id = pr.getCodProdPa();
            String codpr = pr.getCodProdPa();
            String nome = pr.getCodProdPa() + " - " + pr.getDescricao();
            Button botao = new Button(nome);
            botao.setId("botaoVbox");
            botao.setPrefHeight(30);
            botao.setPrefWidth(570);
            botao.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {
                    botao.setId("botaoVboxChangeCollour");
                    listarInfoProdutos(pr.getCodProdPa(), ids);

                }
            });

            vBoxProdutos.getChildren().addAll(botao);
            if (altura_vbox > 550) {
                vBoxProdutos.setPrefHeight(altura_vbox);
            }

        }

    }

    /**
     * Método para substituir o conteudo do menu de opções para colocar as
     * opções por ordem caso seja o operador a abrir o ecrã.<br>
     * Método recebe um objeto do tipo menu referente ao menu de opções e depois
     * refaz o mesmo com as novas opções consonante o utilizador seja operador
     * ou administrador.
     */
    private void adicionarEcraPessoalOperadorMenuOpcoes() {
        //substituir o conteudo do menu de opções para colocar as opções por ordem caso seja o operador a abrir o ecrã
        List<utilizador> operadores = procurarEmail(login.getIdLogin());
        for (utilizador operador : operadores) {
            if (operador.getNivel() == 1) {
                opcoes.getItems().clear();
                MenuItem ePOp = new MenuItem("Ecrã Pessoal");
                MenuItem terminarSessao = new MenuItem("Terminar Sessão");
                MenuItem sair = new MenuItem("Sair");
                opcoes.getItems().add(ePOp);
                opcoes.getItems().add(terminarSessao);
                opcoes.getItems().add(sair);
                ePOp.setOnAction((ActionEvent e) -> {
                    try {
                        ecraPessoalOperador ePO = new ecraPessoalOperador();
                        fecha();
                        ePO.start(new Stage());
                    } catch (Exception ex) {
                        Logger.getLogger(aprovarEntradaStocksController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
                terminarSessao.setOnAction((ActionEvent e) -> {
                    try {
                        login.setIdLogin(0);
                        login.setNivelLogin(10000);
                        login log = new login();
                        fecha();
                        log.start(new Stage());
                    } catch (Exception ex) {
                        Logger.getLogger(aprovarEntradaStocksController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
                sair.setOnAction((ActionEvent e) -> {
                    System.exit(0);
                });
            }
        }
    }

    /**
     * método para fechar o ecrã de listar os produtos clientes
     */
    private void fecha() {
        listarProdutoCliente.getStage().close();
    }

    /**
     * ete metodo recebe a informação da caixa de texto par a pesquisa de
     * clientes e depois inicia o metodo para listar produtos
     *
     * @param event evento
     */
    @FXML
    private void pesquisarCodCliente(ActionEvent event) {

        try {
            int idCliente = Integer.parseInt(codClientePesquisar.getText());
            listarProdutos(idCliente);
            codProdutoPesquisar.setVisible(true);
            pesquisarCodProduto.setVisible(true);
        } catch (Exception e) {

            a2.setAlertType(Alert.AlertType.INFORMATION);
            a2.setTitle("Clientes");
            a2.setHeaderText("Id inválido!");
            a2.setContentText("Introduza apenas valores numéricos!");
            a2.getButtonTypes().setAll(ButtonType.OK);
            Optional<ButtonType> result2 = a2.showAndWait();
        }

    }

    /**
     * ete metodo recebe a informação da caixa de texto par a pesquisa de
     * produtos e depois inicia o metodo para listar produtos
     *
     * @param event evento
     */
    @FXML
    private void pesquisarCodProduto(ActionEvent event) {
        try {
            listarProdutoPesquisa(codProdutoPesquisar.getText());
        } catch (Exception e) {

            a2.setAlertType(Alert.AlertType.INFORMATION);
            a2.setTitle("Clientes");
            a2.setHeaderText("Id inválido!");
            a2.setContentText("Introduza apenas valores numéricos!");
            a2.getButtonTypes().setAll(ButtonType.OK);
            Optional<ButtonType> result2 = a2.showAndWait();
        }
    }
}
