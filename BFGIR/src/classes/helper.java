/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.util.Optional;
import javafx.event.Event;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;

/**
 *Classe Helper com metodos de alerta
 */
public class helper {

    /**
     *Metodo de imagem
     * 
     */
    public static final Image appIcon = new Image("file:FeiraOffice.png");

    /**
     *Este método vai fazer um alerta ao utlizador 
     * a perdir a confirmação se quer mesmo sair da aplicação
     * se sim ira fechar a aplicação, se não fecha o alerta
     * 
     * @param event evento
     */
    public static void opcaoSair(Event event) {

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Feira & Office ");
        alert.setHeaderText("Deseja mesmo sair da aplicação?");
        ButtonType buttonTypeYES = new ButtonType("Sim");
        ButtonType buttonTypeNO = new ButtonType("Não");

        alert.getButtonTypes().setAll(buttonTypeYES, buttonTypeNO);
        Optional<ButtonType> result = alert.showAndWait();

        if (result.get() == buttonTypeYES) {
            System.exit(0);
        } else {
            event.consume();
        }
    }

}
