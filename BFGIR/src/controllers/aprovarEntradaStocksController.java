/*
 * To change this license header, choose License Headers in Projenp Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import bfgir.aprovarEntradaStocks;
import bfgir.ecraPessoalOperador;
import bfgir.login;
import bfgir.menuAdministrador;
import bfgir.menuOperador;
import classes.entradas;
import static classes.entradas.atualizarPendenteEnt;
import static classes.entradas.listarPendentes;
import static classes.entradas.retornaIdFornecedor;
import classes.entradasProdutos;
import static classes.entradasProdutos.atualizarPendenteEntProd;
import static classes.entradasProdutos.listarProdutosEntrada;
import classes.fornecedor;
import static classes.fornecedor.procurarFornecedorID;
import classes.produto;
import static classes.produto.pesquisarProdutoID;
import static classes.produto.atualizarQuantStock;
import classes.utilizador;
import static classes.utilizador.procurarEmail;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Classe controladora do ecrã aprovarEntradaStocks.<br>
 */
public class aprovarEntradaStocksController implements Initializable {

    String ids = "";

    @FXML
    private Button aprovarStock;
    @FXML
    private ScrollPane produtos;
    @FXML
    private ScrollPane entradaStock;
    @FXML
    private MenuBar menu;
    @FXML
    private VBox vBoxProdutos;

    @FXML
    private VBox vBoxFornecedores;

    private String idEnt = "";
    @FXML
    private Menu opcoes;
    @FXML
    private VBox vBoxEntradas;

    private int altura_vbox = 0;

    /**
     *
     * Faz a listagem das encomendas pendentes, para cada encomenda é criada um
     * botão pelo sue id de entrada.<br>
     *
     * @param url url.
     * @param rb rb.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        adicionarEcraPessoalOperadorMenuOpcoes();

        List<entradas> entrada = listarPendentes();
        for (entradas et : entrada) {

            String id = et.getIdEntrada();
            ids = id;
            String nome = et.getIdEntrada();
            Button botao = new Button(nome);
            botao.setId("botaoVbox");
            botao.setPrefHeight(30);
            botao.setPrefWidth(275);
            botao.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {
                    botao.setId("botaoVboxChangeCollour");
                    idEnt = id;
                    atualizarProdutos(id);
                    listarFornecedores(id);

                }
            });

            altura_vbox = altura_vbox + 30;
            vBoxEntradas.getChildren().addAll(botao);
            if (altura_vbox > 548) {
                vBoxEntradas.setPrefHeight(altura_vbox);
            }

        }

    }

    /**
     * Método para fechar o ecrã de aprovação de stock.<br>
     */
    private void fecha() {
        aprovarEntradaStocks.getStage().close();
    }

    /**
     * Método para terminar sessao do utilizador.<br>
     *
     * @param event evento.
     */
    @FXML
    private void terminarSessao(ActionEvent event) {
        try {
            login.setIdLogin(0);
            login.setNivelLogin(10000);
            login log = new login();
            fecha();
            log.start(new Stage());

        } catch (Exception ex) {
            Logger.getLogger(menuAdministradorController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Método para encerrar a aplicação.<br>
     */
    @FXML
    private void sair(ActionEvent event) {
        System.exit(0);
    }

    /**
     * Método para aceder ao menu do operador ou do administrador conforme o seu
     * nivel de login.<br>
     */
    @FXML
    private void openMenu(ActionEvent event) {
        try {
            if (login.getNivelLogin() == 1) {
                menuOperador menuO = new menuOperador();
                fecha();
                menuO.start(new Stage());
            } else {
                menuAdministrador menuAdmin = new menuAdministrador();
                fecha();
                menuAdmin.start(new Stage());
            }
        } catch (Exception ex) {
            Logger.getLogger(aprovarEntradaStocksController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Este método vai fazer um alerda de confirmação se quer aprovar a
     * encomenda, se sim, vai fazer as verificações necessarias e depois
     * atualiza o stock na base de dados, se não, apenas fecha o alerta.<br>
     *
     * @param event evento.
     */
    @FXML
    private void aprovarStock(ActionEvent event) {
        Float atualizarPrecoUnit = 0.0f;

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Feira & Office ");
        alert.setHeaderText("Deseja mesmo aprovar entrada?");
        ButtonType buttonTypeYES = new ButtonType("Sim");
        ButtonType buttonTypeNO = new ButtonType("Não");

        alert.getButtonTypes().setAll(buttonTypeYES, buttonTypeNO);
        Optional<ButtonType> result = alert.showAndWait();

        if (result.get() == buttonTypeYES) {
            try {
                List<entradasProdutos> entradaProdutos = listarProdutosEntrada(idEnt);
                for (entradasProdutos ep : entradaProdutos) {
                    //
                    atualizarPendenteEntProd(idEnt);
                    //
                    List<produto> produto = pesquisarProdutoID(ep.getCodProdPa());
                    String tipoQuantStock = ep.getTipoVenda().toLowerCase();
                    String tipoQuantStockUnit = ep.getTipoVendaUni().toLowerCase();

                    for (produto prd : produto) {

                        if (prd.getPrecoUnitSIVA() < ep.getPrecoUnitSIVA()) {
                            atualizarPrecoUnit = ep.getPrecoUnitSIVA();
                        } else {
                            atualizarPrecoUnit = prd.getPrecoUnitSIVA();
                        }
                        if ("box".equals(tipoQuantStock) || "ream".equals(tipoQuantStock)) {

                            int quantStock = prd.getQuantStock() + ep.getQuantVendida();
                            int quantStockUnit = prd.getQuantStockUnit() + ep.getQuantTotalVendaUni();

                            atualizarQuantStock(ep.getCodProdPa(), quantStock, ep.getTipoVenda(), quantStockUnit, ep.getTipoVendaUni(), atualizarPrecoUnit);
                        } else if ("box".equals(tipoQuantStockUnit) || "ream".equals(tipoQuantStockUnit)) {

                            int quantStock = prd.getQuantStock() + ep.getQuantVendida();
                            int quantStockUnit = prd.getQuantStockUnit() + ep.getQuantTotalVendaUni();

                            atualizarQuantStock(ep.getCodProdPa(), quantStockUnit, ep.getTipoVendaUni(), quantStock, ep.getTipoVenda(), atualizarPrecoUnit);
                        } else if ("unit".equals(tipoQuantStock) && "".equals(tipoQuantStockUnit)) {

                            int quantStock = prd.getQuantStock() + ep.getQuantVendida();
                            atualizarQuantStock(ep.getCodProdPa(), quantStock, ep.getTipoVenda(), 0, "", atualizarPrecoUnit);
                        } else if ("unit".equals(tipoQuantStockUnit)) {

                            int quantStock = prd.getQuantStock() + ep.getQuantVendida();
                            int quantStockUnit = prd.getQuantStockUnit() + ep.getQuantTotalVendaUni();

                            atualizarQuantStock(ep.getCodProdPa(), quantStockUnit, ep.getTipoVendaUni(), quantStock, ep.getTipoVenda(), atualizarPrecoUnit);
                        }

                    }
                }

                atualizarPendenteEnt(idEnt);

                fecha();
                aprovarEntradaStocks ap = new aprovarEntradaStocks();
                ap.start(new Stage());

            } catch (Exception ex) {
                Logger.getLogger(aprovarEntradaStocksController.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else {
            event.consume();
        }
    }

    /**
     * Este método vai fazer a listagem organizada de varias categorias ( valor
     * total, tipo do preço e tipo de quantidade de stock) e de todas as
     * informações do produto selecionado.<br>
     *
     * @param idEnt recebe o ID de entrada do produto.
     */
    private void atualizarProdutos(String idEnt) {
        vBoxProdutos.getChildren().removeAll(vBoxProdutos.getChildren());
        vBoxFornecedores.getChildren().removeAll(vBoxFornecedores.getChildren());
        float valorTotal = 0;
        String tipoMoeda = "";
        List<entradasProdutos> entradaProdutos = listarProdutosEntrada(idEnt);

        for (entradasProdutos enp : entradaProdutos) {
            valorTotal = valorTotal + enp.getPrecoTotalCIVA();
            tipoMoeda = enp.getTipoPrecos();
            List<produto> produtos = pesquisarProdutoID(enp.getCodProdPa());

            for (produto prod : produtos) {
                String tipoQuantStock = enp.getTipoVenda().toLowerCase();
                String id = enp.getIdEntrada();
                String nome = prod.getDescricao();
                Label label1;

                if (!tipoQuantStock.equals("box") | !tipoQuantStock.equals("ream")) {
                    if (nome.length() > 25) {

                        label1 = new Label(enp.getCodProdPa() + " | " + nome.substring(0, 25) + " | " + enp.getQuantVendida() + " " + enp.getTipoVenda() + " | " + " Total: " + enp.getPrecoTotalCIVA() + " " + enp.getTipoPrecos() + " C.IVA ");
                    } else {
                        label1 = new Label(enp.getCodProdPa() + " | " + nome.substring(0, nome.length()) + " | " + enp.getQuantVendida() + " " + enp.getTipoVenda() + " | " + " Total: " + enp.getPrecoTotalCIVA() + " " + enp.getTipoPrecos() + " C.IVA ");
                    }
                } else {
                    if (nome.length() > 25) {
                        label1 = new Label(enp.getCodProdPa() + " | " + nome.substring(0, 25) + " | " + enp.getQuantTotalVendaUni() + " " + enp.getTipoVenda() + " | " + " Total: " + enp.getPrecoTotalCIVA() + " " + enp.getTipoPrecos() + " C.IVA ");
                    } else {
                        label1 = new Label(enp.getCodProdPa() + " | " + nome.substring(0, nome.length()) + " | " + enp.getQuantTotalVendaUni() + " " + enp.getTipoVenda() + " | " + " Total: " + enp.getPrecoTotalCIVA() + " " + enp.getTipoPrecos() + " C.IVA ");
                    }

                }

                label1.setId("labelProdutos");
                label1.setPrefHeight(30);
                label1.setPrefWidth(480);

                vBoxProdutos.getChildren().addAll(label1);

            }
        }
        DecimalFormat df = new DecimalFormat("###########.##");
        float valor = valorTotal;
        Label label2;
        label2 = new Label("Total: " + df.format(valor) + " " + tipoMoeda);
        label2.setId("labelProdutos");
        label2.setPrefHeight(30);
        label2.setPrefWidth(593);
        vBoxProdutos.getChildren().addAll(label2);
    }

    /**
     * Este método vai listar as informações do fornecedor que forneceu aquela
     * encomenda.<br>
     *
     * @param idEnt recebe o ID de entrada do produto.
     */
    private void listarFornecedores(String idEnt) {

        List<entradas> entradas = retornaIdFornecedor(idEnt);
        for (entradas enp : entradas) {

            List<fornecedor> fornecedor = procurarFornecedorID(enp.getIdFornecedor());
            for (fornecedor fr : fornecedor) {
                String id = enp.getIdEntrada();
                String nome = fr.getNome();
                Label label1;
                Label label2;
                if (nome.length() > 25) {
                    label1 = new Label("Nome: " + nome.substring(0, 25));
                    label2 = new Label("Contribuinte: " + fr.getContribuinte());
                } else {
                    label1 = new Label("Nome: " + nome.substring(0, nome.length()));
                    label2 = new Label("Contribuinte: " + fr.getContribuinte());

                }

                label1.setId("labelProdutos");
                label1.setPrefHeight(30);
                label1.setPrefWidth(365);

                label2.setId("labelProdutos");
                label2.setPrefHeight(30);
                label2.setPrefWidth(365);

                vBoxFornecedores.getChildren().addAll(label1, label2);

            }
        }

    }

    /**
     * Este método vai dar a opção ao operador (procurado pelo seu email) de ir
     * ao seu ecra pessoal na barra de menu.<br>
     *
     * @param event evento.
     */
    private void ecraPessoalOperador(ActionEvent event) {
        List<utilizador> operadores = procurarEmail(login.getIdLogin());
        for (utilizador operador : operadores) {
            if (operador.getNivel() == 1) {
                MenuItem ePOp = new MenuItem("Ecrã Pessoal");
                opcoes.getItems().add(ePOp);
                ePOp.setOnAction((ActionEvent e) -> {
                    try {
                        ecraPessoalOperador ePO = new ecraPessoalOperador();
                        fecha();
                        ePO.start(new Stage());
                    } catch (Exception ex) {
                        Logger.getLogger(ecraPessoalOperadorController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
            }

        }
    }

    /**
     * Método para substituir o conteudo do menu de opções para colocar as
     * opções por ordem caso seja o operador a abrir o ecrã.<br>
     */
    private void adicionarEcraPessoalOperadorMenuOpcoes() {
        //substituir o conteudo do menu de opções para colocar as opções por ordem caso seja o operador a abrir o ecrã
        List<utilizador> operadores = procurarEmail(login.getIdLogin());
        for (utilizador operador : operadores) {
            if (operador.getNivel() == 1) {
                opcoes.getItems().clear();
                MenuItem ePOp = new MenuItem("Ecrã Pessoal");
                MenuItem terminarSessao = new MenuItem("Terminar Sessão");
                MenuItem sair = new MenuItem("Sair");
                opcoes.getItems().add(ePOp);
                opcoes.getItems().add(terminarSessao);
                opcoes.getItems().add(sair);
                ePOp.setOnAction((ActionEvent e) -> {
                    try {
                        ecraPessoalOperador ePO = new ecraPessoalOperador();
                        fecha();
                        ePO.start(new Stage());
                    } catch (Exception ex) {
                        Logger.getLogger(aprovarEntradaStocksController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
                terminarSessao.setOnAction((ActionEvent e) -> {
                    try {
                        login.setIdLogin(0);
                        login.setNivelLogin(10000);
                        login log = new login();
                        fecha();
                        log.start(new Stage());
                    } catch (Exception ex) {
                        Logger.getLogger(aprovarEntradaStocksController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
                sair.setOnAction((ActionEvent e) -> {
                    System.exit(0);
                });
            }
        }
    }

}
