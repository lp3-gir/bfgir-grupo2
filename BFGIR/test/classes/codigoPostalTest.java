/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.util.List;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Classe de testes da classe codigoPostal.
 */
public class codigoPostalTest {

    private static codigoPostal cp = new codigoPostal();

    /**
     * Construdor da classe codigoPostalTest
     */
    public codigoPostalTest() {
    }

    /**
     *
     * Método para estabelecer os atributos do objeto cp.
     */
    @Before
    public void setUp() {
        cp.setCodPostal("1111-111");
        cp.setCidade("teste");
    }

    /**
     * Teste ao método procurarCodPostal da classe codigoPostal.<br>
     * Método vai procurar por um código postal e informa o utilizador de testes
     * caso encontre o registo.<br>
     * O teste é válido caso o registo seja encontrado.
     */
    @Test
    public void testProcurarCodPostal() {
        String codPostal = "9999-999";
        System.out.println("\nTeste ao método procurarCodPostal " + codPostal);
        List<codigoPostal> detalhes = codigoPostal.procurarCodPostal(codPostal);
        boolean result = !detalhes.isEmpty();
        if (result) {
            System.out.println("Teste ao método procurarCodPostal passado com um sucesso!\n");
        }
        assertTrue(result);
    }

    /**
     * Teste ao método getCodPostal da classe codigoPostal.<br>
     * Método tenta buscar o código postal do objeto previamente criado na
     * classe.<br>
     * O teste é válido caso o valor obtido seja igual ao valor esperado.
     *
     */
    @Test
    public void testGetCodPostal() {
        System.out.println("\nTeste ao método getCodPostal");
        String expResult = "1111-111";
        String result = cp.getCodPostal();
        if (expResult.equals(result)) {
            System.out.println("Teste ao método getCodPostal passado com um sucesso!\n");
        }
        assertEquals(expResult, result);
    }

    /**
     * Teste ao método setCodPostal da classe codigoPostal.<br>
     * Método modifica o código postal do objeto cp e depois verifica se o
     * consegiu alterar.<br>
     * O teste é válido caso tal se verifique.<br>
     * No fim, restaura o atributo ao estado inicial.
     */
    @Test
    public void testSetCodPostal() {
        System.out.println("\nTeste ao método setCodPostal");
        String cpI = "4561-567";
        String cpF = cp.getCodPostal();
        cp.setCodPostal(cpI);
        boolean result = false;
        if (cpI.equals(cp.getCodPostal())) {
            result = true;
            System.out.println("Teste ao método setCodPostal passado com um sucesso!\n");
        }
        assertTrue(result);
        cp.setCodPostal(cpF);
    }

    /**
     * Teste ao método getCidade da classe codigoPostal.<br>
     * Método tenta buscar a cidade do objeto previamente criado na classe.<br>
     * O teste é válido caso o valor obtido seja igual ao valor esperado.
     *
     */
    @Test
    public void testGetCidade() {
        System.out.println("\nTeste ao método getCidade");
        String expResult = "teste";
        String result = cp.getCidade();
        if (expResult.equals(result)) {
            System.out.println("Teste ao método getCidade passado com um sucesso!\n");
        }
        assertEquals(expResult, result);
    }

    /**
     * Teste ao método setCidade da classe codigoPostal.<br>
     * Método modifica a cidade do objeto cp e depois verifica se o
     * consegiu alterar.<br>
     * O teste é válido caso tal se verifique.<br>
     * No fim, restaura o atributo ao estado inicial.
     */
    @Test
    public void testSetCidade() {
        System.out.println("\nTeste ao método setCidade");
        String ctI = "Porto";
        String ctF = cp.getCidade();
        cp.setCidade(ctI);
        boolean result = false;
        if (ctI.equals(cp.getCidade())) {
            result = true;
            System.out.println("Teste ao método setCidade passado com um sucesso!\n");
        }
        assertTrue(result);
        cp.setCodPostal(ctF);
    }

}
