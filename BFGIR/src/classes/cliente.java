/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import bfgir.BDConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe cliente com metodos de acesso a tabela de cliente na BD.
 */
public class cliente {

    private int idCliente;
    private String nome;
    private int contribuinte;
    private String morada;
    private String codPostal;
    private String pais;
    private String estado;
    private String email;

    /**
     * Construtor da classe cliente.
     */
    public cliente() {
    }

    /**
     * Construtor da classe cliente com atributos.
     *
     * @param idCliente id do cliente
     * @param nome nome do cliente
     * @param morada morada do cliente
     * @param codPostal codigo postal do cliente
     * @param pais pais do cliente
     * @param estado estado do cliente
     * @param contribuinte contribuinte do cliente
     * @param email email do cliente
     */
    public cliente(int idCliente, String nome, String email, int contribuinte, String morada, String codPostal, String pais, String estado) {
        this.idCliente = idCliente;
        this.nome = nome;
        this.morada = morada;
        this.codPostal = codPostal;
        this.pais = pais;
        this.estado = estado;
        this.contribuinte = contribuinte;
        this.email = email;
    }

    /**
     * Método set email do cliente.<br>
     *
     * @param email the email to set.
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Método get email.<br>
     *
     * @return the email.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Método get ID do cliente.<br>
     *
     * @return the idCliente.
     */
    public int getIdCliente() {
        return idCliente;
    }

    /**
     * Método set ID do cliente.<br>
     *
     * @param idCliente the idCliente to set.
     */
    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    /**
     * Método get Nome do cliente.<br>
     *
     * @return the nome.
     */
    public String getNome() {
        return nome;
    }

    /**
     * Método set ID do cliente.<br>
     *
     * @param nome the nome to set.
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * Método get Morada do cliente.<br>
     *
     * @return the morada.
     */
    public String getMorada() {
        return morada;
    }

    /**
     * Método set ID do cliente.<br>
     *
     * @param morada the morada to set.
     */
    public void setMorada(String morada) {
        this.morada = morada;
    }

    /**
     * Método get Codigo Postal do cliente.<br>
     *
     * @return the codPostal.
     */
    public String getCodPostal() {
        return codPostal;
    }

    /**
     * Método set ID do cliente.<br>
     *
     * @param codPostal the codPostal to set.
     */
    public void setCodPostal(String codPostal) {
        this.codPostal = codPostal;
    }

    /**
     * Método get Pais do cliente.<br>
     *
     * @return the pais.
     */
    public String getPais() {
        return pais;
    }

    /**
     * Método set ID do cliente.<br>
     *
     * @param pais the pais to set.
     */
    public void setPais(String pais) {
        this.pais = pais;
    }

    /**
     * Método get Estado do cliente.<br>
     *
     * @return the estado.
     */
    public String getEstado() {
        return estado;
    }

    /**
     * Método set ID do cliente.<br>
     *
     * @param estado the estado to set.
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * Método get contribuinte do cliente.<br>
     *
     * @return the contribuinte.
     */
    public int getContribuinte() {
        return contribuinte;
    }

    /**
     * Método set ID do cliente.<br>
     *
     * @param contribuinte the contribuinte to set.
     */
    public void setContribuinte(int contribuinte) {
        this.contribuinte = contribuinte;
    }

    /**
     * Método para selecionar todos os atributos do cliente na BD.<br>
     *
     * Posto isto faz um select à BD adicionando a um arrayList todos os
     * atributos de um cliente ordenados pelo seu ID de cliente.<br>
     *
     * @return lista clientes ordenado pelo id cliente.
     */
    public static List<cliente> listarClientesPendentes() {

        List<cliente> cliente = new ArrayList<cliente>();
        String sql = "SELECT * FROM cliente WHERE estado = 'pendente' ORDER BY idCliente ";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                cliente cli = new cliente();

                cli.setIdCliente(results.getInt(1));
                cli.setNome(results.getString(2));
                cli.setEmail(results.getString(3));
                cli.setMorada(results.getString(4));
                cli.setCodPostal(results.getString(5));
                cli.setPais(results.getString(6));
                cli.setContribuinte(results.getInt(7));
                cli.setEstado(results.getString(8));

                cliente.add(cli);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        return cliente;
    }

    /**
     * Metodo para alterar o estado do cliente para ativo onde o id do cliente
     * seja aquele.<br>
     *
     * @param idCliente id do cliente a receber.
     */
    public static void alterarEstadoCliente(int idCliente) {

        //List<cliente> clientes = new ArrayList<cliente>();
        String sql = "UPDATE cliente SET estado = 'ativo' WHERE idCliente = (?) ";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setInt(1, idCliente);

            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Método para obter uma lista contendo todos os clientes existentes na
     * BD.<br>
     *
     * @return : retorna uma lista contendo todos os objetos do tipo cliente
     * existente na BD.
     */
    public static List<cliente> listarCliente() {

        List<cliente> listaCliente = new ArrayList();
        String sql = "Select * from cliente ";

        PreparedStatement stmt = null;
        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                cliente cl = new cliente();
                cl.setIdCliente(results.getInt(1));
                cl.setNome(results.getString(2));
                cl.setEmail(results.getString(3));
                cl.setMorada(results.getString(4));
                cl.setCodPostal(results.getString(5));
                cl.setPais(results.getString(6));
                cl.setContribuinte(results.getInt(7));
                cl.setEstado(results.getString(8));
                listaCliente.add(cl);
            }
        } catch (SQLException ex) {
            Logger.getLogger(cliente.class.getName()).log(Level.SEVERE, null, ex);
        }

        return listaCliente;

    }

    /**
     * Método para selecionar todos os atributos do cliente na BD.<br>
     *
     * Posto isto faz um select à BD adicionando a um arrayList todos os
     * atributos onde o estado do cliente for ativo.<br>
     *
     * @return lista clientes quando estes forem ativos.
     *
     *
     *
     */
    public static List<cliente> listarClienteAct() {

        List<cliente> listaCliente = new ArrayList();
        String sql = "Select * from cliente  WHERE estado = 'ativo'";

        PreparedStatement stmt = null;
        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                cliente cl = new cliente();
                cl.setIdCliente(results.getInt(1));
                cl.setNome(results.getString(2));
                cl.setEmail(results.getString(3));
                cl.setMorada(results.getString(4));
                cl.setCodPostal(results.getString(5));
                cl.setPais(results.getString(6));
                cl.setContribuinte(results.getInt(7));
                cl.setEstado(results.getString(8));
                listaCliente.add(cl);
            }
        } catch (SQLException ex) {
            Logger.getLogger(cliente.class.getName()).log(Level.SEVERE, null, ex);
        }

        return listaCliente;

    }


    /**
     * Método para remover um produto associado a um cliente da BD.<br>
     *
     * Este método recebe por parametro o codigo de um produto associado a um
     * cliente (codProdPa). Posto isto o método apaga da tabela clienteproduto o
     * produto que tenha aquele codigo de produto e id de cliente.<br>
     *
     * @param idCliente id do cliente com o produto associado.
     * @param codProdPa codigo do produto a remover.
     *
     */
    public static void removerClienteAssociado(int idCliente, String codProdPa) {

        List<cliente> cliente = new ArrayList<cliente>();
        String sql = "DELETE FROM clienteproduto WHERE idCliente = (?) AND codProdPa = (?)";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setInt(1, idCliente);
            pstmt.setString(2, codProdPa);

            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Método para selecionar todos os atributos do cliente na BD.<br>
     *
     * Posto isto faz um select à BD adicionando a um arrayList todos os
     * atributos de um cliente ordenados pelo seu ID de cliente.<br>
     *
     * @param idCliente : id do cliente associado ao produto a pesquisar.
     * @return lista clientes ordenado pelo id cliente.
     *
     *
     *
     */
    public static List<cliente> listarProdutoCliente(int idCliente) {

        List<cliente> cliente = new ArrayList<cliente>();
        String sql = "SELECT * FROM clienteproduto WHERE idCliente = (?) ";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setInt(1, idCliente);

            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                cliente cli = new cliente();

                cli.setNome(results.getString(2));

                cliente.add(cli);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        return cliente;
    }

    /**
     * Método para retornar todos os dados de um cliente após pesquisa pelo seu
     * id de cliente na BD.<br>
     *
     * Posto isto faz um select à BD adicionando a um arrayList todos os
     * atributos do cliente.<br>
     *
     * @param idCliente : id do cliente a pesquisar.
     * @return : retorna o cliente se o encontrar.
     */
    public static List<cliente> getClienteID(int idCliente) {

        List<cliente> cliente = new ArrayList<cliente>();
        String sql = "SELECT * FROM cliente WHERE idCliente = (?)";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setInt(1, idCliente);

            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                cliente cli = new cliente();

                cli.setIdCliente(results.getInt(1));
                cli.setNome(results.getString(2));
                cli.setEmail(results.getString(3));
                cli.setMorada(results.getString(4));
                cli.setCodPostal(results.getString(5));
                cli.setPais(results.getString(6));
                cli.setContribuinte(results.getInt(7));
                cli.setEstado(results.getString(8));

                cliente.add(cli);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        return cliente;
    }

    /**
     * Método para retornar todos os dados de um cliente após pesquisa pelo seu
     * id de cliente e estado pendente na BD.<br>
     *
     * Posto isto faz um select à BD adicionando a um arrayList todos os
     * atributos do cliente.<br>
     *
     * @param idCliente : id do cliente a pesquisar.
     * @return : retorna o cliente se o encontrar.
     */
    public static List<cliente> pesquisaClientePendente(int idCliente) {

        List<cliente> cliente = new ArrayList<cliente>();
        String sql = "SELECT * FROM cliente WHERE idCliente = (?) AND estado = 'pendente'";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setInt(1,idCliente);

            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                cliente cli = new cliente();

                cli.setIdCliente(results.getInt(1));
                cli.setNome(results.getString(2));
                cli.setEmail(results.getString(3));
                cli.setMorada(results.getString(4));
                cli.setCodPostal(results.getString(5));
                cli.setPais(results.getString(6));
                cli.setContribuinte(results.getInt(7));
                cli.setEstado(results.getString(8));

                cliente.add(cli);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        return cliente;
    }

    /**
     * Método para obter uma lista contendo todos os clientes existentes na BD
     * com produtos associados.<br>
     * Metodo procura por todos os registos da tabela cliente com registos na
     * tabela clienteProduto, ie, idCliente da tabela cliente tem de existir na
     * tabela clienteProduto.<br>
     *
     * @return : retorna uma lista contendo todos os objetos do tipo cliente.
     * existente na BD com correspondência na tabela clienteProduto.
     */
    public static List<cliente> getClientesComProdutos() {

        List<cliente> listaCliente = new ArrayList();
        String sql = "select * from cliente where cliente.idCliente "
                + "in (select clienteproduto.idCliente from clienteproduto)";

        PreparedStatement stmt = null;
        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                cliente cl = new cliente();
                cl.setIdCliente(results.getInt(1));
                cl.setNome(results.getString(2));
                cl.setEmail(results.getString(3));
                cl.setMorada(results.getString(4));
                cl.setCodPostal(results.getString(5));
                cl.setPais(results.getString(6));
                cl.setContribuinte(results.getInt(7));
                cl.setEstado(results.getString(8));
                listaCliente.add(cl);
            }
        } catch (SQLException ex) {
            Logger.getLogger(cliente.class.getName()).log(Level.SEVERE, null, ex);
        }

        return listaCliente;

    }
}
