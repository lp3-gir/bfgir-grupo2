/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.service;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author Ruben
 */
@javax.ws.rs.ApplicationPath("resources")
public class ApplicationConfig extends Application {

    /**
     *
     * @return
     */
    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method. It is automatically
     * populated with all resources defined in the project. If required, comment
     * out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(entities.service.ClienteFacadeREST.class);
        resources.add(entities.service.EncomendasFacadeREST.class);
        resources.add(entities.service.EncomendasprodutoFacadeREST.class);
        resources.add(entities.service.OrderFR.class);
        resources.add(entities.service.ProdutoFacadeREST.class);
        resources.add(entities.service.TreeFacadeREST.class);

    }

}
