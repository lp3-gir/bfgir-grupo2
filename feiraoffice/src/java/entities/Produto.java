/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ruben
 */
@Entity
@Table(name = "produto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Produto.findAll", query = "SELECT p FROM Produto p")
    , @NamedQuery(name = "Produto.findByCodProdPa", query = "SELECT p FROM Produto p WHERE p.codProdPa = :codProdPa")
    , @NamedQuery(name = "Produto.findByDescricao", query = "SELECT p FROM Produto p WHERE p.descricao = :descricao")
    , @NamedQuery(name = "Produto.findByQuantStock", query = "SELECT p FROM Produto p WHERE p.quantStock = :quantStock")
    , @NamedQuery(name = "Produto.findByTipoQuantStock", query = "SELECT p FROM Produto p WHERE p.tipoQuantStock = :tipoQuantStock")
    , @NamedQuery(name = "Produto.findByQuantStockUnit", query = "SELECT p FROM Produto p WHERE p.quantStockUnit = :quantStockUnit")
    , @NamedQuery(name = "Produto.findByTipoQuantStockUnit", query = "SELECT p FROM Produto p WHERE p.tipoQuantStockUnit = :tipoQuantStockUnit")
    , @NamedQuery(name = "Produto.findByPrecoUnitSIVA", query = "SELECT p FROM Produto p WHERE p.precoUnitSIVA = :precoUnitSIVA")
    , @NamedQuery(name = "Produto.findByIdCategoria", query = "SELECT p FROM Produto p WHERE p.idCategoria = :idCategoria")
    , @NamedQuery(name = "Produto.findByEstado", query = "SELECT p FROM Produto p WHERE p.estado = :estado")})
public class Produto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "codProdPa")
    private String codProdPa;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "descricao")
    private String descricao;
    @Basic(optional = false)
    @NotNull
    @Column(name = "quantStock")
    private int quantStock;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "tipoQuantStock")
    private String tipoQuantStock;
    @Basic(optional = false)
    @NotNull
    @Column(name = "quantStockUnit")
    private int quantStockUnit;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "tipoQuantStockUnit")
    private String tipoQuantStockUnit;
    @Basic(optional = false)
    @NotNull
    @Column(name = "precoUnitSIVA")
    private double precoUnitSIVA;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idCategoria")
    private int idCategoria;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "estado")
    private String estado;

    /**
     *
     */
    public Produto() {
    }

    /**
     *
     * @param codProdPa recebe um código de produto
     */
    public Produto(String codProdPa) {
        this.codProdPa = codProdPa;
    }

    /**
     *
     * @param codProdPa recebe um código de produto
     * @param descricao recebe uma descrição do produto
     * @param quantStock recebe uma quantidade de stock do produto
     * @param tipoQuantStock recebe um tipo de quantidade do stock do produto
     * @param quantStockUnit recebe uma quantidade de stock em unidades do produto
     * @param tipoQuantStockUnit recebe um tipo de quantidade de stock do produto
     * @param precoUnitSIVA recebe um preço por unidade sem IVA do produto
     * @param idCategoria recebe um id de categoria do produto
     * @param estado recebe um estado do produto
     */
    public Produto(String codProdPa, String descricao, int quantStock, String tipoQuantStock, int quantStockUnit, String tipoQuantStockUnit, double precoUnitSIVA, int idCategoria, String estado) {
        this.codProdPa = codProdPa;
        this.descricao = descricao;
        this.quantStock = quantStock;
        this.tipoQuantStock = tipoQuantStock;
        this.quantStockUnit = quantStockUnit;
        this.tipoQuantStockUnit = tipoQuantStockUnit;
        this.precoUnitSIVA = precoUnitSIVA;
        this.idCategoria = idCategoria;
        this.estado = estado;
    }

    /**
     *
     * @return codProdPa
     */
    public String getCodProdPa() {
        return codProdPa;
    }

    /**
     *
     * @param codProdPa
     */
    public void setCodProdPa(String codProdPa) {
        this.codProdPa = codProdPa;
    }

    /**
     *
     * @return descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     *
     * @param descricao
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     *
     * @return quantStock
     */
    public int getQuantStock() {
        return quantStock;
    }

    /**
     *
     * @param quantStock
     */
    public void setQuantStock(int quantStock) {
        this.quantStock = quantStock;
    }

    /**
     *
     * @return tipoQuantStock
     */
    public String getTipoQuantStock() {
        return tipoQuantStock;
    }

    /**
     *
     * @param tipoQuantStock
     */
    public void setTipoQuantStock(String tipoQuantStock) {
        this.tipoQuantStock = tipoQuantStock;
    }

    /**
     *
     * @return quantStockUnit
     */
    public int getQuantStockUnit() {
        return quantStockUnit;
    }

    /**
     *
     * @param quantStockUnit
     */
    public void setQuantStockUnit(int quantStockUnit) {
        this.quantStockUnit = quantStockUnit;
    }

    /**
     *
     * @return tipoQuantStockUnit
     */
    public String getTipoQuantStockUnit() {
        return tipoQuantStockUnit;
    }

    /**
     *
     * @param tipoQuantStockUnit
     */
    public void setTipoQuantStockUnit(String tipoQuantStockUnit) {
        this.tipoQuantStockUnit = tipoQuantStockUnit;
    }

    /**
     *
     * @return precoUnitSIVA
     */
    public double getPrecoUnitSIVA() {
        return precoUnitSIVA;
    }

    /**
     *
     * @param precoUnitSIVA
     */
    public void setPrecoUnitSIVA(double precoUnitSIVA) {
        this.precoUnitSIVA = precoUnitSIVA;
    }

    /**
     *
     * @return idCategoria
     */
    public int getIdCategoria() {
        return idCategoria;
    }

    /**
     *
     * @param idCategoria
     */
    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }

    /**
     *
     * @return estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     *
     * @param estado
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }
}
