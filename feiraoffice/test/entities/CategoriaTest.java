/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ruben
 */
public class CategoriaTest {

    /**
     * Teste para retirar o id categoria.
     */
    @Test
    public void testGetIdCategoria() {
        System.out.println("getIdCategoria");
        Categoria instance = new Categoria();
        instance.setIdCategoria(1);

        Integer expResult = 1;
        assertEquals(expResult, instance.getIdCategoria());

    }

    /**
     * Teste para definir um id da categoria.
     */
    @Test
    public void testSetIdCategoria() {
        System.out.println("setIdCategoria");
        Categoria instance = new Categoria();
        instance.setIdCategoria(1);

    }

    /**
     * Teste para retirar o nome da categoria.
     */
    @Test
    public void testGetNomeCategoria() {
        System.out.println("getNomeCategoria");
        Categoria instance = new Categoria();
        instance.setNomeCategoria("teste");

        assertEquals("teste", instance.getNomeCategoria());

    }

    /**
     * Teste para definir o nome da categoria.
     */
    @Test
    public void testSetNomeCategoria() {
        System.out.println("setNomeCategoria");
        Categoria instance = new Categoria();
        instance.setNomeCategoria("Teste");

    }

    /**
     * Teste para retirar a descrição da categoria.
     */
    @Test
    public void testGetDescricaoCategoria() {
        System.out.println("getDescricaoCategoria");
        Categoria instance = new Categoria();
        instance.setDescricaoCategoria("Descricao Teste");

        assertEquals("Descricao Teste", instance.getDescricaoCategoria());

    }

    /**
     * Teste para definir a descrição da categoria.
     */
    @Test
    public void testSetDescricaoCategoria() {
        System.out.println("setDescricaoCategoria");
        Categoria instance = new Categoria();
        instance.setDescricaoCategoria("Descricao Teste");

    }

    /**
     * Teste para retirar o nivel da categoria.
     */
    @Test
    public void testGetNivelCategoria() {
        System.out.println("getNivelCategoria");
        Categoria instance = new Categoria();
        instance.setNivelCategoria(3);
        
        int expResult = 3;
        assertEquals(expResult, instance.getNivelCategoria());

    }

    /**
     * Teste para definir um nivel da categoria.
     */
    @Test
    public void testSetNivelCategoria() {
        System.out.println("setNivelCategoria");
        Categoria instance = new Categoria();
        instance.setNivelCategoria(3);

    }

    /**
     * Teste para retirar o estado da categoria.
     */
    @Test
    public void testGetEstado() {
        System.out.println("getEstado");
        Categoria instance = new Categoria();
        instance.setEstado("ativa");
        
        assertEquals("ativa", instance.getEstado());

    }

    /**
     * Teste para definir estado da categoria.
     */
    @Test
    public void testSetEstado() {
        System.out.println("setEstado");
        Categoria instance = new Categoria();
        instance.setEstado("ativa");

    }

}
