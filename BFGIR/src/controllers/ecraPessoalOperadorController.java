/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import bfgir.ecraPessoalOperador;
import bfgir.login;
import bfgir.menuOperador;
import static classes.PasswordHelper.validarNovaPassInseridaUtilizador;
import static classes.administrador.alterarPassUtilizador;
import static classes.administrador.procurarUtilizadorIDEmail;
import static classes.email.enviarEmail;
import classes.passwordUtils;
import classes.utilizador;
import static classes.utilizador.procurarEmail;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * Classe controladora do ecrã ecraPessoalOperador.<br>
 *
 */
public class ecraPessoalOperadorController implements Initializable {

    int id_operador_atualizar = 0;
    String oldEmail = "";
    String newEmail = "";
    int nivel_operador_atualizar = 1;
    String estado_Operador = "";
    String oldPass = "";
    String oldSalt = "";
    String newPass = "";
    Alert a = new Alert(Alert.AlertType.NONE);

    @FXML
    private Label lbEmail;
    @FXML
    private TextArea email;
    @FXML
    private Label lbPass;
    @FXML
    private Pane PaneComp;
    @FXML
    private MenuBar menu;
    @FXML
    private Button alterarPassOperador;
    @FXML
    private PasswordField password;
    @FXML
    private TextField passText;
    @FXML
    private CheckBox mostrarPassword;

    /**
     * Método de inicialização do ecrã para apresentar os dados do operador e
     * carregar informação necessária à validação das alterações.<br>
     *
     *
     * @param url url.
     * @param rb rb.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        esconderPasswoord();

        lbEmail.setVisible(true);
        lbPass.setVisible(true);

        email.setVisible(true);
        email.setEditable(false); // email PK por isso não se pode alterar para já
        carregarDadosOperador();

    }

    /**
     * Método para terminar sessão.<br>
     */
    @FXML
    private void terminarSessao(ActionEvent event) {
        try {
            login.setIdLogin(0);
            login.setNivelLogin(10000);
            login log = new login();
            fecha();
            log.start(new Stage());

        } catch (Exception ex) {
            Logger.getLogger(ecraPessoalOperadorController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Método para o operador alterar a sua password.<br>
     *
     * O método envia a nova password para o email do utilizador.<br>
     *
     * @param event evento.
     */
    @FXML
    private void alterarOperador(ActionEvent event) {

        //verificar se o utilizador ainda existe na BD
        List<utilizador> utilizadores = procurarUtilizadorIDEmail(id_operador_atualizar, oldEmail);

        int count = 0;
        for (utilizador uti : utilizadores) {
            count = 1;
        }
        if (count == 0) {
            a.setAlertType(Alert.AlertType.ERROR);
            a.setTitle("Ecrã Pessoal Utilizador");
            a.setHeaderText("A sua conta que foi removida!");
            a.setContentText("A sua conta foi eliminada enquanto a alterava.");
            a.setResizable(false);
            a.getButtonTypes().setAll(ButtonType.OK);
            Optional<ButtonType> result = a.showAndWait();
            if (result.get() == ButtonType.OK) {
                System.exit(0);
            }

        } else {
            //alterar password sem alterar email e com mail antigo escrito/mantido aka ecrã inicial
            if (!email.getText().equals("") && email.getText().equals(oldEmail) && !password.getText().equals("")) {
                a.setAlertType(Alert.AlertType.CONFIRMATION);
                a.setTitle("Ecrã Pessoal Utilizador");
                a.setHeaderText("Deseja mesmo atualizar a sua password?");
                a.setResizable(false);
                a.getButtonTypes().setAll(ButtonType.YES, ButtonType.NO);
                Optional<ButtonType> result = a.showAndWait();
                if (result.get() == ButtonType.YES) {
                    atualizarPassword(oldEmail);
                } else {
                    ecraPessoalOperador ePOp = new ecraPessoalOperador();
                    fecha();
                    try {
                        ePOp.start(new Stage());
                    } catch (Exception ex) {
                        a.setAlertType(Alert.AlertType.ERROR);
                        a.setTitle("ERRO Fatal");
                        a.setHeaderText("A aplicação está corrompida!");
                        a.setContentText("Algumas funcionalidades do aplicação estão inacessíveis. Tente uma reinstalação.");
                        a.show();
                        System.out.println("Erro Exception ex " + ex.getMessage());
                        Logger.getLogger(ecraPessoalOperadorController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

            } else if (password.getText().equals("") && email.getText().equals("")) {
                a.setAlertType(Alert.AlertType.INFORMATION);
                a.setTitle("Ecrã Pessoal Utilizador");
                a.setHeaderText("Operação Inválida!");
                a.setContentText("Campos vazios. Insira novos dados caso pretenda alterar o seu email ou a sua password.");
                a.setResizable(false);
                a.getButtonTypes().setAll(ButtonType.OK);
                Optional<ButtonType> result = a.showAndWait();
                if (result.get() == ButtonType.OK) {
                    ecraPessoalOperador ePOp = new ecraPessoalOperador();
                    fecha();
                    try {
                        ePOp.start(new Stage());
                    } catch (Exception ex) {
                        a.setAlertType(Alert.AlertType.ERROR);
                        a.setTitle("ERRO Fatal");
                        a.setHeaderText("A aplicação está corrompida!");
                        a.setContentText("Algumas funcionalidades do aplicação estão inacessíveis. Tente uma reinstalação.");
                        a.show();
                        System.out.println("Erro Exception ex " + ex.getMessage());
                        Logger.getLogger(ecraPessoalOperadorController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            } else {
                Alert a2 = new Alert(Alert.AlertType.ERROR);
                a2.setTitle("Ecrã Pessoal Utilizador");
                a2.setHeaderText("Impossível alterar os dados!");
                a2.setContentText("A funcionalidade está inacessível. Tente uma reinstalação.");
                a2.setResizable(false);
                a2.getButtonTypes().setAll(ButtonType.OK);
                Optional<ButtonType> resultB = a2.showAndWait();
                if (resultB.get() == ButtonType.OK) {
                    ecraPessoalOperador ePOp = new ecraPessoalOperador();
                    fecha();
                    try {
                        ePOp.start(new Stage());
                    } catch (Exception ex) {
                        a.setAlertType(Alert.AlertType.ERROR);
                        a.setTitle("ERRO Fatal");
                        a.setHeaderText("A aplicação está corrompida!");
                        a.setContentText("Algumas funcionalidades do aplicação estão inacessíveis. Tente uma reinstalação.");
                        a.show();
                        System.out.println("Erro Exception ex " + ex.getMessage());
                        Logger.getLogger(ecraPessoalOperadorController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }

        }
    }

    /**
     * Método para encerrar a aplicação.<br>
     */
    @FXML
    private void sair(ActionEvent event
    ) {
        System.exit(0);
    }

    /**
     * *
     * Método para aceder ao menu do operador.<br>
     */
    @FXML
    private void openMenu(ActionEvent event
    ) {
        try {
            menuOperador menuOperador = new menuOperador();
            fecha();
            menuOperador.start(new Stage());

        } catch (Exception ex) {
            a.setAlertType(Alert.AlertType.ERROR);
            a.setTitle("ERRO Fatal");
            a.setHeaderText("A aplicação está corrompida!");
            a.setContentText("Algumas funcionalidades do aplicação estão inacessíveis. Tente uma reinstalação.");
            a.show();
            System.out.println("Erro Exception ex " + ex.getMessage());
            Logger
                    .getLogger(ecraPessoalOperadorController.class
                            .getName()).log(Level.SEVERE, null, ex);

        }
    }

    /**
     * Método para fechar o ecrã pessoal do operador.<br>
     */
    private void fecha() {
        ecraPessoalOperador.getStage().close();
    }

    /**
     * Método para mostrar e ocultar a nova password inserida pelo utilizador
     * ligando um passwordfield a um textfield tudo sob controlo de uma
     * checkbox.<br>
     */
    private void esconderPasswoord() {
        passText.setManaged(false);
        passText.setVisible(false);
        passText.managedProperty().bind(mostrarPassword.selectedProperty());
        passText.visibleProperty().bind(mostrarPassword.selectedProperty());
        password.managedProperty().bind(mostrarPassword.selectedProperty().not());
        password.visibleProperty().bind(mostrarPassword.selectedProperty().not());
        passText.textProperty().bindBidirectional(password.textProperty());
    }

    /**
     * Método para mostar alguns dados do operador no ecrã e para carregar os
     * dados do mesmo em memória para posterior uso aquando da alteração da
     * password.<br>
     */
    private void carregarDadosOperador() {
        List<utilizador> operadores = procurarEmail(login.getIdLogin());
        for (utilizador operador : operadores) {
            id_operador_atualizar = operador.getIdUtilizador();
            nivel_operador_atualizar = operador.getNivel();
            email.setText(operador.getEmail());
            oldEmail = email.getText();
            oldPass = operador.getPassword();
            oldSalt = operador.getSalt();
            password.setText("");
            passText.setText("");
            estado_Operador = operador.getEstado();

            if (estado_Operador.equals("") || estado_Operador.equals(null) || oldPass.equals("") || oldPass.equals(null) || oldSalt.equals("") || oldSalt.equals(null)
                    || id_operador_atualizar == 0 || nivel_operador_atualizar != 1 || oldEmail.equals("") || oldEmail.equals(null)) {
                a.setAlertType(Alert.AlertType.ERROR);
                a.setTitle("ERRO SQL");
                a.setHeaderText("Dados do operador em falta!");
                a.setContentText("Não foi possível obter os seus dados pessoais. Contacte o Administrador.");
                a.showAndWait();
                ecraPessoalOperador ePOp = new ecraPessoalOperador();
                fecha();
                try {
                    ePOp.start(new Stage());
                } catch (Exception ex) {
                    a.setAlertType(Alert.AlertType.ERROR);
                    a.setTitle("ERRO Fatal");
                    a.setHeaderText("A aplicação está corrompida!");
                    a.setContentText("Algumas funcionalidades do aplicação estão inacessíveis. Tente uma reinstalação.");
                    a.show();
                    System.out.println("Erro Exception ex " + ex.getMessage());
                    Logger
                            .getLogger(ecraPessoalOperadorController.class
                                    .getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    /**
     * Método para atualizar a password do utlizador sem alterar o email.<br>
     * Método recebe o email do utilizador, faz validações, altera a password na
     * base de dados, envia email ao utilizador com a nova password, informa o
     * utilizador do ocrrido e atualiza o ecrã.<br>
     *
     * @param email : email do utilizador inicial sem alteração.
     */
    public void atualizarPassword(String email) {

        String novaPass = null;
        Alert a = new Alert(Alert.AlertType.NONE);

        if (password.getText() == null) {

            a.setAlertType(Alert.AlertType.INFORMATION);
            a.setResizable(false);
            a.setTitle("Ecrã Pessoal Utilizador");
            a.setHeaderText("Operação inválida!");
            a.setContentText("Não pode remover a sua password! Insira uma nova caso pretenda susbtituir a atual.");
            a.getButtonTypes().setAll(ButtonType.OK);
            Optional<ButtonType> result = a.showAndWait();
            if (result.get() == ButtonType.OK) {
                ecraPessoalOperador ePOp = new ecraPessoalOperador();
                fecha();
                try {
                    ePOp.start(new Stage());
                } catch (Exception ex) {
                    a.setAlertType(Alert.AlertType.ERROR);
                    a.setTitle("ERRO Fatal");
                    a.setHeaderText("A aplicação está corrompida!");
                    a.setContentText("Algumas funcionalidades do aplicação estão inacessíveis. Tente uma reinstalação.");
                    a.show();
                    System.out.println("Erro Exception ex " + ex.getMessage());
                    Logger
                            .getLogger(ecraPessoalOperadorController.class
                                    .getName()).log(Level.SEVERE, null, ex);
                }
            }

        } else {
            novaPass = password.getText();
        }

        if (validarNovaPassInseridaUtilizador(novaPass, oldPass, oldSalt)) {
            try {
                String salt = passwordUtils.getSalt(30);

                // Protect user's password. The generated value can be stored in DB.
                String mySecurePassword = passwordUtils.generateSecurePassword(novaPass, salt);

                //enviar email ao operador
                enviarEmail(email, novaPass, "");

                //alterar pass operador
                alterarPassUtilizador(mySecurePassword, salt, id_operador_atualizar, email, nivel_operador_atualizar, estado_Operador);

                //guardar nova pass como
                a.setAlertType(Alert.AlertType.INFORMATION);
                a.setTitle("Ecrã Pessoal do Utilizador");
                a.setHeaderText("Password alterada com sucesso!");
                a.setContentText("A nova password foi enviada para o seu email. Reveja-a e guarde-a num local seguro.");
                a.getButtonTypes().setAll(ButtonType.OK);
                Optional<ButtonType> result2 = a.showAndWait();
                if (result2.get() == ButtonType.OK) {
                    ecraPessoalOperador ePOp = new ecraPessoalOperador();
                    fecha();
                    try {
                        ePOp.start(new Stage());
                    } catch (Exception ex) {
                        a.setAlertType(Alert.AlertType.ERROR);
                        a.setTitle("ERRO Fatal");
                        a.setHeaderText("A aplicação está corrompida!");
                        a.setContentText("Algumas funcionalidades do aplicação estão inacessíveis. Tente uma reinstalação.");
                        a.show();
                        System.out.println("Erro Exception ex " + ex.getMessage());
                        Logger
                                .getLogger(ecraPessoalOperadorController.class
                                        .getName()).log(Level.SEVERE, null, ex);
                    }
                }
            } catch (Exception passwordUpdate) {
                a.setAlertType(Alert.AlertType.ERROR);
                a.setTitle("Ecrã Pessoal Utilizador");
                a.setHeaderText("Não foi possível atualizar a password do utilizador!");
                a.setContentText("A base de dados pode estar ocupada ou indisponível ou não foi possível aceder aos seus dados.");
                a.show();
                System.out.println("Erro SQL: " + passwordUpdate.getMessage());
                Logger
                        .getLogger(ecraPessoalOperadorController.class
                                .getName()).log(Level.SEVERE, null, passwordUpdate);
            }
        } else {

            password.clear();
            //mensagens de erro vem do método de validar
            ecraPessoalOperador ePOp = new ecraPessoalOperador();
            fecha();
            try {
                ePOp.start(new Stage());
            } catch (Exception ex) {
                a.setAlertType(Alert.AlertType.ERROR);
                a.setTitle("ERRO Fatal");
                a.setHeaderText("A aplicação está corrompida!");
                a.setContentText("Algumas funcionalidades do aplicação estão inacessíveis. Tente uma reinstalação.");
                a.show();
                System.out.println("Erro Exception ex " + ex.getMessage());
                Logger
                        .getLogger(ecraPessoalOperadorController.class
                                .getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
