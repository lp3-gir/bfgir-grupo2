/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;

/**
 *
 * @author ruben
 */
public class OrderTest {



    private static Order instance = new Order();

    /**
     *
     * @throws Exception
     */
    @BeforeClass
    public static void setUpClass() throws Exception {

        Encomendas enc = new Encomendas();
        enc.setIdEncomenda(1);
        enc.setIdCliente(1);
        enc.setDataEncomenda("11/01/2021");
        enc.setMoradaEntrega("Rua de teste");
        enc.setCodPostalEntrega("4415-900");
        enc.setPaisEntrega("Portugal");
        enc.setMoradaFaturacao("Rua dos testes");
        enc.setCodPostalFaturacao("4415-900");
        enc.setPaisFaturacao("Portugal");
        enc.setPrecoTotalSIVA(50.3);
        enc.setIva(23);
        enc.setTotalIVA(20);
        enc.setPrecoTotalCIVA(70);
        enc.setEstado("ativa");

        Encomendasproduto encProds = new Encomendasproduto();
        encProds.setIdEncProd(1);
        encProds.setIdEncomenda(1);
        encProds.setCodProdPa("T.123");
        encProds.setQuantVendida(100);
        encProds.setTipoVenda("Teste");
        encProds.setPrecoUnitSIVA(10.5);
        encProds.setPrecoUnitCIVA(24);
        encProds.setEstado("ativo");

        Produto prod = new Produto();
        prod.setCodProdPa("T.123");
        prod.setDescricao("Descricao Teste");
        prod.setQuantStock(5000);
        prod.setTipoQuantStock("Teste");
        prod.setQuantStockUnit(500000);
        prod.setTipoQuantStockUnit("Teste");
        prod.setPrecoUnitSIVA(500);
        prod.setIdCategoria(1);
        prod.setEstado("Teste");

        List<Encomendas> Encomendas = new ArrayList<>();
        Encomendas.add(enc);
        List<Encomendasproduto> Encomendasproduto = new ArrayList<>();
        Encomendasproduto.add(encProds);
        List<Produto> Produto = new ArrayList<>();
        Produto.add(prod);

        instance.setEncomendas(Encomendas);
        instance.setEncomendasproduto(Encomendasproduto);
        instance.setProduto(Produto);

    }

    /**
     * Test of getEncomendas method, of class Order.
     */
    @Test
    public void testGetEncomendas() {
        System.out.println("getEncomendas");

        List<Encomendas> Encomendas = instance.getEncomendas();

        assertEquals(Encomendas, instance.getEncomendas());

    }

    /**
     * Test of setEncomendas method, of class Order.
     */
    @Test
    public void testSetEncomendas() {
        System.out.println("setEncomendas");

        Order order = new Order();
        order.setEncomendas(instance.getEncomendas());

    }

    /**
     * Test of getEncomendasproduto method, of class Order.
     */
    @Test
    public void testGetEncomendasproduto() {
        System.out.println("getEncomendasproduto");
        List<Encomendasproduto> Encomendasproduto = instance.getEncomendasproduto();

        assertEquals(Encomendasproduto, instance.getEncomendasproduto());

    }

    /**
     * Test of setEncomendasproduto method, of class Order.
     */
    @Test
    public void testSetEncomendasproduto() {
        System.out.println("setEncomendasproduto");

        Order order = new Order();
        order.setEncomendasproduto(instance.getEncomendasproduto());

    }

    /**
     * Test of getProduto method, of class Order.
     */
    @Test
    public void testGetProduto() {
        System.out.println("getProduto");

        List<Produto> Produto = instance.getProduto();
        assertEquals(Produto, instance.getProduto());

    }

    /**
     * Test of setProduto method, of class Order.
     */
    @Test
    public void testSetProduto() {
        System.out.println("setProduto");

        Order order = new Order();
        order.setProduto(instance.getProduto());

    }

}
