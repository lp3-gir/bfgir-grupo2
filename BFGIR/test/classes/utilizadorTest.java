
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 */
public class utilizadorTest {

    /**
     * construtod da classe utilizador teste
     */
    public utilizadorTest() {
    }

    /**
     * set up class
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * tear down class
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * set up
     */
    @Before
    public void setUp() {
    }

    /**
     * tear down
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getIdUtilizador method, of class utilizador.
     */
    @Test
    public void testGetIdUtilizador() {
        System.out.println("getIdUtilizador");
        utilizador instance = new utilizador();
        instance.setIdUtilizador(1);
        int expResult = 1;
        int result = instance.getIdUtilizador();
        if (expResult == result) {
            System.out.println("O método passou com sucesso");
        } else {
            fail("The test case is a prototype.");
        }
        
    }

    /**
     * Test of setIdUtilizador method, of class utilizador.
     */
    @Test
    public void testSetIdUtilizador() {
        System.out.println("setIdUtilizador");
        utilizador instance = new utilizador();
        int expResult  = 1;
        instance.setIdUtilizador(expResult);
        int result = instance.getIdUtilizador();
        if (expResult == result) {
            System.out.println("O método passou com sucesso");
        } else {
            fail("The test case is a prototype.");
        }
       
    }

    /**
     * Test of getEmail method, of class utilizador.
     */
    @Test
    public void testGetEmail() {
        System.out.println("getEmail");
        utilizador instance = new utilizador();
        instance.setEmail("email@email.com");
        String expResult = "email@email.com";
        String result = instance.getEmail();
        if (expResult == result) {
            System.out.println("O método passou com sucesso");
        } else {
            fail("The test case is a prototype.");
        }
        
    }

    /**
     * Test of setEmail method, of class utilizador.
     */
    @Test
    public void testSetEmail() {
        System.out.println("setEmail");
        utilizador instance = new utilizador();
        String expResult  = "email@email.com";
        instance.setEmail(expResult);
        String result = instance.getEmail();
        if (expResult == result) {
            System.out.println("O método passou com sucesso");
        } else {
            fail("The test case is a prototype.");
        }
       
    }

    /**
     * Test of getPassword method, of class utilizador.
     */
    @Test
    public void testGetPassword() {
        System.out.println("getPassword");
        utilizador instance = new utilizador();
        instance.setPassword("1234ola");
        String expResult = "1234ola";
        String result = instance.getPassword();
        if (expResult == result) {
            System.out.println("O método passou com sucesso");
        } else {
            fail("The test case is a prototype.");
        }
        
    }

    /**
     * Test of setPassword method, of class utilizador.
     */
    @Test
    public void testSetPassword() {
        System.out.println("setPassword");
        utilizador instance = new utilizador();
        String expResult  = "1234ola";
        instance.setPassword(expResult);
        String result = instance.getPassword();
        if (expResult == result) {
            System.out.println("O método passou com sucesso");
        } else {
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of getSalt method, of class utilizador.
     */
    @Test
    public void testGetSalt() {
        System.out.println("getSalt");
        utilizador instance = new utilizador();
        instance.setSalt("1234");
        String expResult = "1234";
        String result = instance.getSalt();
        if (expResult == result) {
            System.out.println("O método passou com sucesso");
        } else {
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of setSalt method, of class utilizador.
     */
    @Test
    public void testSetSalt() {
        System.out.println("setSalt");
        utilizador instance = new utilizador();
        String expResult  = "1234";
        instance.setSalt(expResult);
        String result = instance.getSalt();
        if (expResult == result) {
            System.out.println("O método passou com sucesso");
        } else {
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of getNivel method, of class utilizador.
     */
    @Test
    public void testGetNivel() {
        System.out.println("getNivel");
        utilizador instance = new utilizador();
        instance.setNivel(1);
        int expResult = 1;
        int result = instance.getNivel();
        if (expResult == result) {
            System.out.println("O método passou com sucesso");
        } else {
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of setNivel method, of class utilizador.
     */
    @Test
    public void testSetNivel() {
        System.out.println("setNivel");
        utilizador instance = new utilizador();
        int expResult  = 1;
        instance.setNivel(expResult);
        int result = instance.getNivel();
        if (expResult == result) {
            System.out.println("O método passou com sucesso");
        } else {
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of getEstado method, of class utilizador.
     */
    @Test
    public void testGetEstado() {
        System.out.println("getEstado");
        utilizador instance = new utilizador();
        instance.setEstado("ativo");
        String expResult = "ativo";
        String result = instance.getEstado();
        if (expResult == result) {
            System.out.println("O método passou com sucesso");
        } else {
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of setEstado method, of class utilizador.
     */
    @Test
    public void testSetEstado() {
        System.out.println("setEstado");
        utilizador instance = new utilizador();
        String expResult  = "ativo";
        instance.setEstado(expResult);
        String result = instance.getEstado();
        if (expResult == result) {
            System.out.println("O método passou com sucesso");
        } else {
            fail("The test case is a prototype.");
        }
    }

}
