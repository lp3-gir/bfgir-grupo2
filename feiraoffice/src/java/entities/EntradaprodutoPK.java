/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Ruben
 */
@Embeddable
public class EntradaprodutoPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "idEntrada")
    private String idEntrada;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "codProdPa")
    private String codProdPa;

    /**
     *
     */
    public EntradaprodutoPK() {
    }

    /**
     *
     * @param idEntrada recebe um id de entrada 
     * @param codProdPa recebe um código de produto
     */
    public EntradaprodutoPK(String idEntrada, String codProdPa) {
        this.idEntrada = idEntrada;
        this.codProdPa = codProdPa;
    }

    /**
     *
     * @return idEntrada
     */
    public String getIdEntrada() {
        return idEntrada;
    }

    /**
     *
     * @param idEntrada
     */
    public void setIdEntrada(String idEntrada) {
        this.idEntrada = idEntrada;
    }

    /**
     * 
     * @return codProdPa
     */
    public String getCodProdPa() {
        return codProdPa;
    }

    /**
     *
     * @param codProdPa
     */
    public void setCodProdPa(String codProdPa) {
        this.codProdPa = codProdPa;
    }

}
