/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import bfgir.BDConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe entradasProdutos com metodos de acesso a tabela de entradasProdutos na
 * BD
 */
public class entradasProdutos {

    private String idEntrada;
    private String codProdPa;
    private float precoUnitSIVA;
    private String tipoPrecos;
    private float precoTotalSIVA;
    private int iva;
    private float totalIVA;
    private String localTax;
    private int quantVendida;
    private String tipoVenda;
    private float pesoTotalVenda;
    private String tipoPeso;
    private int quantTotalVendaUni;
    private String tipoVendaUni;
    private float precoTotalCIVA;
    private String estado;

    /**
     * Construtor da classe entradasProdutos
     */
    public entradasProdutos() {
    }

    /**
     * Construtor da classe entradasProduto<br>
     *
     * @param idEntrada id da entrada
     * @param codProdPa codProdPa
     * @param precoTotalSIVA preço total sem iva
     * @param iva iva
     * @param totalIVA total do iva
     * @param localTax total de taxas
     * @param quantVendida quantidade vendida
     * @param tipoVenda tipo da venda
     * @param pesoTotalVenda peso total da venda
     * @param tipoPeso tipo de peso
     * @param quantTotalVendaUni quantidade total de unidades vendidas
     * @param tipoVendaUni tipo de unidades vendidas
     * @param precoTotalCIVA preço total com iva
     * @param estado estado
     */
    public entradasProdutos(String idEntrada, String codProdPa, float precoTotalSIVA, int iva, float totalIVA, String localTax, int quantVendida, String tipoVenda, float pesoTotalVenda, String tipoPeso, int quantTotalVendaUni, String tipoVendaUni, float precoTotalCIVA, String estado) {
        this.idEntrada = idEntrada;
        this.codProdPa = codProdPa;
        this.precoTotalSIVA = precoTotalSIVA;
        this.iva = iva;
        this.totalIVA = totalIVA;
        this.localTax = localTax;
        this.quantVendida = quantVendida;
        this.tipoVenda = tipoVenda;
        this.pesoTotalVenda = pesoTotalVenda;
        this.tipoPeso = tipoPeso;
        this.quantTotalVendaUni = quantTotalVendaUni;
        this.tipoVendaUni = tipoVendaUni;
        this.precoTotalCIVA = precoTotalCIVA;
        this.estado = estado;

    }

    /**
     * @return the idntrada
     */
    public String getIdEntrada() {
        return idEntrada;
    }

    /**
     * @param idEntrada the idEntrada to set
     */
    public void setIdEntrada(String idEntrada) {
        this.idEntrada = idEntrada;
    }

    /**
     * @return the codProdPa
     */
    public String getCodProdPa() {
        return codProdPa;
    }

    /**
     * @param codProdPa the codProPa to set
     */
    public void setCodProdPa(String codProdPa) {
        this.codProdPa = codProdPa;
    }

    /**
     * @return the precoUnitSIVA
     */
    public float getPrecoUnitSIVA() {
        return precoUnitSIVA;
    }

    /**
     * @param precoUnitSIVA the precoUnitSIVA to set
     */
    public void setPrecoUnitSIVA(float precoUnitSIVA) {
        this.precoUnitSIVA = precoUnitSIVA;
    }

    /**
     * @return the tipoPrecos
     */
    public String getTipoPrecos() {
        return tipoPrecos;
    }

    /**
     * @param tipoPrecos the tipoPrecos to set
     */
    public void setTipoPrecos(String tipoPrecos) {
        this.tipoPrecos = tipoPrecos;
    }

    /**
     * @return the precoTotalSIVA
     */
    public float getPrecoTotalSIVA() {
        return precoTotalSIVA;
    }

    /**
     * @param precoTotalSIVA the precoTotalSIVA to set
     */
    public void setPrecoTotalSIVA(float precoTotalSIVA) {
        this.precoTotalSIVA = precoTotalSIVA;
    }

    /**
     * @return the iva
     */
    public int getIva() {
        return iva;
    }

    /**
     * @param iva the iva to set
     */
    public void setIva(int iva) {
        this.iva = iva;
    }

    /**
     * @return the totalIVA
     */
    public float getTotalIVA() {
        return totalIVA;
    }

    /**
     * @param totalIVA the totalIVA to set
     */
    public void setTotalIVA(float totalIVA) {
        this.totalIVA = totalIVA;
    }

    /**
     * @return the localTax
     */
    public String getLocalTax() {
        return localTax;
    }

    /**
     * @param localTax the localTax to set
     */
    public void setLocalTax(String localTax) {
        this.localTax = localTax;
    }

    /**
     * @return the quantVendida
     */
    public int getQuantVendida() {
        return quantVendida;
    }

    /**
     * @param quantVendida the quantVendida to set
     */
    public void setQuantVendida(int quantVendida) {
        this.quantVendida = quantVendida;
    }

    /**
     * @return the tipoVenda
     */
    public String getTipoVenda() {
        return tipoVenda;
    }

    /**
     * @param tipoVenda the tipoVenda to set
     */
    public void setTipoVenda(String tipoVenda) {
        this.tipoVenda = tipoVenda;
    }

    /**
     * @return the pesoTotalVenda
     */
    public float getPesoTotalVenda() {
        return pesoTotalVenda;
    }

    /**
     * @param pesoTotalVenda the pesoTotalVenda to set
     */
    public void setPesoTotalVenda(float pesoTotalVenda) {
        this.pesoTotalVenda = pesoTotalVenda;
    }

    /**
     * @return the tipoPeso
     */
    public String getTipoPeso() {
        return tipoPeso;
    }

    /**
     * @param tipoPeso the tipoPeso to set
     */
    public void setTipoPeso(String tipoPeso) {
        this.tipoPeso = tipoPeso;
    }

    /**
     * @return the quantTotalVendaUni
     */
    public int getQuantTotalVendaUni() {
        return quantTotalVendaUni;
    }

    /**
     * @param quantTotalVendaUni the quantTotalVendaUni to set
     */
    public void setQuantTotalVendaUni(int quantTotalVendaUni) {
        this.quantTotalVendaUni = quantTotalVendaUni;
    }

    /**
     * @return the tipoVendaUni
     */
    public String getTipoVendaUni() {
        return tipoVendaUni;
    }

    /**
     * @param tipoVendaUni the tipoVendaUni to set
     */
    public void setTipoVendaUni(String tipoVendaUni) {
        this.tipoVendaUni = tipoVendaUni;
    }

    /**
     * @return the precoTotalCIVA
     */
    public float getPrecoTotalCIVA() {
        return precoTotalCIVA;
    }

    /**
     * @param precoTotalCIVA the precoTotalCIVA to set
     */
    public void setPrecoTotalCIVA(float precoTotalCIVA) {
        this.precoTotalCIVA = precoTotalCIVA;
    }

    /**
     * @return the estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     *
     * Método para inserir todos os atributos de uma entrada de produtos na
     * BD<br>
     *
     * Este método recebe por parametro o objeto entrada (entProd). Posto isto
     * faz um insert à BD adicionando a um arrayList todos os atributos de uma
     * entradaProduto com aquele ID de entradaProduto.<br>
     *
     * @param entProd objeto do tipo entrada produto
     *
     *
     */
    public static void inserirEntradasProdutos(entradasProdutos entProd) {

        String sql = "INSERT INTO entradaproduto (idEntrada,codProdPa,precoUnitSIVA,tipoPrecos,precoTotalSIVA,IVA,totalIVA,localTAX,quantVendida,tipoVenda,pesoTotalVenda,tipoPeso,quantTotalVendaUni,tipoVendaUni,precoTotalCIVA,estado) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setString(1, entProd.getIdEntrada());
            pstmt.setString(2, entProd.getCodProdPa());
            pstmt.setFloat(3, entProd.getPrecoUnitSIVA());
            pstmt.setString(4, entProd.getTipoPrecos());
            pstmt.setFloat(5, entProd.getPrecoTotalSIVA());
            pstmt.setInt(6, entProd.getIva());
            pstmt.setFloat(7, entProd.getTotalIVA());
            pstmt.setString(8, entProd.getLocalTax());
            pstmt.setLong(9, entProd.getQuantVendida());
            pstmt.setString(10, entProd.getTipoVenda());
            pstmt.setFloat(11, entProd.getPesoTotalVenda());
            pstmt.setString(12, entProd.getTipoPeso());
            pstmt.setLong(13, entProd.getQuantTotalVendaUni());
            pstmt.setString(14, entProd.getTipoVendaUni());
            pstmt.setFloat(15, entProd.getPrecoTotalCIVA());
            pstmt.setString(16, entProd.getEstado());

            pstmt.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

    }

    /**
     * Método para selecionar ID da entrada na BD<br>
     *
     * Este método recebe por parametro o ID da entrada (idEnt). Posto isto faz
     * um select à BD adicionando a um arrayList todos os atributos de um objeto
     * entrada onde o id da entrada seja aquele.<br>
     *
     * @param idEnt ID da entrada a pesquisar
     * @return lista dos produtos da entrada
     *
     *
     */
    public static List<entradasProdutos> listarProdutosEntrada(String idEnt) {

        List<entradasProdutos> entradaProduto = new ArrayList<>();
        String sql = "SELECT * FROM entradaproduto where idEntrada = (?)";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setString(1, idEnt);

            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                entradasProdutos etp = new entradasProdutos();
                etp.setIdEntrada(results.getString(1));
                etp.setCodProdPa(results.getString(2));
                etp.setPrecoUnitSIVA(results.getFloat(3));
                etp.setTipoPrecos(results.getString(4));
                etp.setPrecoTotalSIVA(results.getFloat(5));
                etp.setIva(results.getInt(6));
                etp.setTotalIVA(results.getFloat(7));
                etp.setLocalTax(results.getString(8));
                etp.setQuantVendida(results.getInt(9));
                etp.setTipoVenda(results.getString(10));
                etp.setPesoTotalVenda(results.getFloat(11));
                etp.setTipoPeso(results.getString(12));
                etp.setQuantTotalVendaUni(results.getInt(13));
                etp.setTipoVendaUni(results.getString(14));
                etp.setPrecoTotalCIVA(results.getFloat(15));
                etp.setEstado(results.getString(16));
                entradaProduto.add(etp);

            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return entradaProduto;
    }

    /**
     * Método para selecionar ID da entrada na BD<br>
     *
     * Este método recebe por parametro o Codigo do produto (codProd). Posto
     * isto faz um select à BD adicionando a um arrayList todos os atributos de
     * um objeto entrada onde o codigo do produto seja aquele.<br>
     *
     * @param codProd codigo do produto a pesquisar
     * @return lista dos produtos da entrada
     *
     */
    public static List<entradasProdutos> procurarTipoQuantidadeProduto(String codProd) {

        List<entradasProdutos> entradaProduto = new ArrayList<>();
        String sql = "SELECT * FROM entradaproduto where codProdPa = (?)";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setString(1, codProd);

            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                entradasProdutos etp = new entradasProdutos();
                etp.setIdEntrada(results.getString(1));
                etp.setCodProdPa(results.getString(2));
                etp.setPrecoUnitSIVA(results.getFloat(3));
                etp.setTipoPrecos(results.getString(4));
                etp.setPrecoTotalSIVA(results.getFloat(5));
                etp.setIva(results.getInt(6));
                etp.setTotalIVA(results.getFloat(7));
                etp.setLocalTax(results.getString(8));
                etp.setQuantVendida(results.getInt(9));
                etp.setTipoVenda(results.getString(10));
                etp.setPesoTotalVenda(results.getFloat(11));
                etp.setTipoPeso(results.getString(12));
                etp.setQuantTotalVendaUni(results.getInt(13));
                etp.setTipoVendaUni(results.getString(14));
                etp.setPrecoTotalCIVA(results.getFloat(15));
                etp.setEstado(results.getString(16));
                entradaProduto.add(etp);

            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return entradaProduto;
    }

    /**
     * Método para selecionar ID da entrada na BD onde o estado é aprovado<br>
     *
     * Este método recebe por parametro o Codigo do produto (codProd). Posto
     * isto faz um select à BD adicionando a um arrayList todos os atributos de
     * um objeto entrada onde o codigo do produto seja aquele e o seu estado
     * seja aprovado.<br>
     *
     * @param codProd codigo do produto a pesquisar
     * @return lista dos produtos da entrada
     *
     */
    public static List<entradasProdutos> procurarTipoQuantidadeProdutoActivos(String codProd) {

        List<entradasProdutos> entradaProduto = new ArrayList<>();
        String sql = "SELECT * FROM entradaproduto where codProdPa = (?) and where estado='aprovado'";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setString(1, codProd);

            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                entradasProdutos etp = new entradasProdutos();
                etp.setIdEntrada(results.getString(1));
                etp.setCodProdPa(results.getString(2));
                etp.setPrecoUnitSIVA(results.getFloat(3));
                etp.setTipoPrecos(results.getString(4));
                etp.setPrecoTotalSIVA(results.getFloat(5));
                etp.setIva(results.getInt(6));
                etp.setTotalIVA(results.getFloat(7));
                etp.setLocalTax(results.getString(8));
                etp.setQuantVendida(results.getInt(9));
                etp.setTipoVenda(results.getString(10));
                etp.setPesoTotalVenda(results.getFloat(11));
                etp.setTipoPeso(results.getString(12));
                etp.setQuantTotalVendaUni(results.getInt(13));
                etp.setTipoVendaUni(results.getString(14));
                etp.setPrecoTotalCIVA(results.getFloat(15));
                etp.setEstado(results.getString(16));
                entradaProduto.add(etp);

            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return entradaProduto;
    }

    /**
     * Método para Atualizar o estado da entrada na BD<br>
     *
     * Este método recebe por parametro o ID da entrada (id). Posto isto faz um
     * update à BD alterando o estado da entradaProduto para 'aprovado' para
     * aquele ID da entrada.<br>
     *
     * @param id id da entrada a pesquisar
     *
     */
    public static void atualizarPendenteEntProd(String id) {

        String sql = "UPDATE entradaproduto set estado='aprovado' WHERE idEntrada=(?)";

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, id);

            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Método para validar se o produto já se encontra registado na entrada
     * introduzida na BD<br>
     *
     * Este método recebe por parametro o Código da entrada (codEnt) e o Codigo
     * do produto (codProd).<br>
     * Posto isto faz um select à BD adicionando para verificar se o produto
     * associado aquela entrada já se encontra na BD.<br>
     *
     * @param codEnt codigo da entrada a pesquisar
     * @param codProd codigo do produto a pesquisar
     * @return lista dos produtos da entrada
     *
     */
    public static List<entradasProdutos> validaProdutoEntrada(String codEnt, String codProd) {

        List<entradasProdutos> entradaProduto = new ArrayList<>();
        String sql = "SELECT * FROM entradaproduto where idEntrada = (?) and codProdPa = (?)";

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setString(1, codEnt);
            pstmt.setString(2, codProd);

            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                entradasProdutos etp = new entradasProdutos();
                etp.setIdEntrada(results.getString(1));
                etp.setCodProdPa(results.getString(2));
                etp.setPrecoUnitSIVA(results.getFloat(3));
                etp.setTipoPrecos(results.getString(4));
                etp.setPrecoTotalSIVA(results.getFloat(5));
                etp.setIva(results.getInt(6));
                etp.setTotalIVA(results.getFloat(7));
                etp.setLocalTax(results.getString(8));
                etp.setQuantVendida(results.getInt(9));
                etp.setTipoVenda(results.getString(10));
                etp.setPesoTotalVenda(results.getFloat(11));
                etp.setTipoPeso(results.getString(12));
                etp.setQuantTotalVendaUni(results.getInt(13));
                etp.setTipoVendaUni(results.getString(14));
                etp.setPrecoTotalCIVA(results.getFloat(15));
                etp.setEstado(results.getString(16));
                entradaProduto.add(etp);

            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return entradaProduto;
    }

    /**
     *
     * Método para atualizar o produto associado a uma entrada<br>
     *
     * Este método recebe por parametro o objeto entrada (entProd), codigo da
     * entrada (codEnt) e o codigo do produto a alterar (codProd). Posto isto
     * faz um update a BD alterando os atributos do produto com o codigo de
     * produto e codigo da entrada enviados por parâmetros.<br>
     *
     * @param entProd objeto do tipo entradas produto
     * @param codEnt codigo da entrada
     * @param codProd codigo do produto associado a uma entrada para ser
     * atualizado
     *
     *
     */
    public static void atualizarEntradasProdutos(entradasProdutos entProd, String codEnt, String codProd) {

        String sql = "UPDATE entradaproduto set  precoTotalSIVA = (?),totalIVA  = (?),quantVendida = (?),pesoTotalVenda = (?),quantTotalVendaUni = (?),precoTotalCIVA = (?) where idEntrada = (?) and codProdPa = (?)";

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setFloat(1, entProd.getPrecoTotalSIVA());
            pstmt.setFloat(2, entProd.getTotalIVA());
            pstmt.setLong(3, entProd.getQuantVendida());
            pstmt.setFloat(4, entProd.getPesoTotalVenda());
            pstmt.setLong(5, entProd.getQuantTotalVendaUni());
            pstmt.setFloat(6, entProd.getPrecoTotalCIVA());
            pstmt.setString(7, codEnt);
            pstmt.setString(8, codProd);

            pstmt.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

    }

    /**
     * ToString<br>
     *
     * @return Este texto remete para o objeto produto
     */
    @Override
    public String toString() {
        return "entrada{" + getIdEntrada() + "," + getCodProdPa() + "," + getPrecoUnitSIVA() + ","
                + getTipoPrecos() + "," + getPrecoTotalSIVA() + "," + getIva() + "," + getTotalIVA() + ","
                + getLocalTax() + "," + getQuantVendida() + "," + getTipoVenda() + "," + getPesoTotalVenda() + ","
                + getTipoPeso() + "," + getQuantTotalVendaUni() + "," + getTipoVendaUni() + "," + getPrecoTotalCIVA() + "," + getEstado() + '}';

    }

}
