/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ruben
 */
public class DescontosprodutosTest {

    /**
     * Teste para retirar um objeto do tipo descontosprodutosPK.
     */
    @Test
    public void testGetDescontosprodutosPK() {
        System.out.println("getDescontosprodutosPK");

        DescontosprodutosPK dppk = new DescontosprodutosPK();
        dppk.setIdDesconto(1);
        dppk.setCodProdPa("T.123");

        Descontosprodutos instance = new Descontosprodutos();
        instance.setDescontosprodutosPK(dppk);

        assertEquals(dppk, instance.getDescontosprodutosPK());

    }

    /**
     * Teste para definir um objeto do tipo descontosprodutosPK.
     */
    @Test
    public void testSetDescontosprodutosPK() {
        System.out.println("setDescontosprodutosPK");
        DescontosprodutosPK dppk = new DescontosprodutosPK();
        dppk.setIdDesconto(1);
        dppk.setCodProdPa("T.123");

        Descontosprodutos instance = new Descontosprodutos();
        instance.setDescontosprodutosPK(dppk);

    }

    /**
     * Teste para retirar a quantidade minima de venda.
     */
    @Test
    public void testGetQuantVenda() {
        System.out.println("getQuantVenda");
        Descontosprodutos instance = new Descontosprodutos();
        instance.setQuantVenda(100);
        assertEquals(100, instance.getQuantVenda());

    }

    /**
     * Teste para definir a quantidade minima de venda.
     */
    @Test
    public void testSetQuantVenda() {
        System.out.println("setQuantVenda");
        Descontosprodutos instance = new Descontosprodutos();
        instance.setQuantVenda(100);

    }

}
