/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Ruben
 */
@Entity
@Table(name = "entrada")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Entrada.findAll", query = "SELECT e FROM Entrada e")
    , @NamedQuery(name = "Entrada.findByIdEntrada", query = "SELECT e FROM Entrada e WHERE e.idEntrada = :idEntrada")
    , @NamedQuery(name = "Entrada.findByIdFornecedor", query = "SELECT e FROM Entrada e WHERE e.idFornecedor = :idFornecedor")
    , @NamedQuery(name = "Entrada.findByDataEntrada", query = "SELECT e FROM Entrada e WHERE e.dataEntrada = :dataEntrada")
    , @NamedQuery(name = "Entrada.findByEstado", query = "SELECT e FROM Entrada e WHERE e.estado = :estado")})
public class Entrada implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "idEntrada")
    private String idEntrada;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "idFornecedor")
    private String idFornecedor;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "dataEntrada")
    private String dataEntrada;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "estado")
    private String estado;

    /**
     *
     */
    public Entrada() {
    }

    /**
     *
     * @param idEntrada recebe um id de entrada de produtos
     */
    public Entrada(String idEntrada) {
        this.idEntrada = idEntrada;
    }

    /**
     *
     * @param idEntrada recebe um id de entrada de produtos
     * @param idFornecedor recebe um id de fornecedor
     * @param dataEntrada recebe uma data de entrada de produtos
     * @param estado recebe um estado da entrada de produtos
     */
    public Entrada(String idEntrada, String idFornecedor, String dataEntrada, String estado) {
        this.idEntrada = idEntrada;
        this.idFornecedor = idFornecedor;
        this.dataEntrada = dataEntrada;
        this.estado = estado;
    }

    /**
     *
     * @return idEntrada
     */
    public String getIdEntrada() {
        return idEntrada;
    }

    /**
     *
     * @param idEntrada
     */
    public void setIdEntrada(String idEntrada) {
        this.idEntrada = idEntrada;
    }

    /**
     *
     * @return idFornecedor
     */
    public String getIdFornecedor() {
        return idFornecedor;
    }

    /**
     *
     * @param idFornecedor
     */
    public void setIdFornecedor(String idFornecedor) {
        this.idFornecedor = idFornecedor;
    }

    /**
     *
     * @return dataEntrada
     */
    public String getDataEntrada() {
        return dataEntrada;
    }

    /**
     *
     * @param dataEntrada
     */
    public void setDataEntrada(String dataEntrada) {
        this.dataEntrada = dataEntrada;
    }

    /**
     *
     * @return estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     *
     * @param estado
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }
}
