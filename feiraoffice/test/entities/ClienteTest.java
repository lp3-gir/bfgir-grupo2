/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ruben
 */
public class ClienteTest {



    /**
     * Teste para retirar o id de cliente.
     */
    @Test
    public void testGetIdCliente() {
        System.out.println("getIdCliente");
        Cliente instance = new Cliente();
        instance.setIdCliente(1);

        Integer expResult = 1;
        assertEquals(expResult, instance.getIdCliente());

    }

    /**
     * Teste para definir o id de cliente.
     */
    @Test
    public void testSetIdCliente() {
        System.out.println("setIdCliente");
        Cliente instance = new Cliente();
        instance.setIdCliente(1);

    }

    /**
     * Teste para retirar o nome do cliente.
     */
    @Test
    public void testGetNome() {
        System.out.println("getNome");
        Cliente instance = new Cliente();
        instance.setNome("Nome Teste");

        assertEquals("Nome Teste", instance.getNome());

    }

    /**
     * Teste para definir o nome do cliente.
     */
    @Test
    public void testSetNome() {
        System.out.println("setNome");
        Cliente instance = new Cliente();
        instance.setNome("Nome Teste");
    }

    /**
     * Teste para retirar o email do cliente.
     */
    @Test
    public void testGetEmail() {
        System.out.println("getEmail");
        Cliente instance = new Cliente();
        instance.setEmail("teste@teste.com");

        assertEquals("teste@teste.com", instance.getEmail());
    }

    /**
     * Teste para definir o email de um cliente.
     */
    @Test
    public void testSetEmail() {
        System.out.println("setEmail");
        Cliente instance = new Cliente();
        instance.setEmail("teste@gmail.com");
    }

    /**
     * Teste para retirar a morada de um cliente.
     */
    @Test
    public void testGetMorada() {
        System.out.println("getMorada");
        Cliente instance = new Cliente();
        instance.setMorada("Rua de testes");
        assertEquals("Rua de testes", instance.getMorada());

    }

    /**
     * Teste para definir a morada do cliente.
     */
    @Test
    public void testSetMorada() {
        System.out.println("setMorada");
        Cliente instance = new Cliente();
        instance.setMorada("Rua de testes");

    }

    /**
     * Teste para retirar o código postal do cliente.
     */
    @Test
    public void testGetCodPostal() {
        System.out.println("getCodPostal");
        Cliente instance = new Cliente();
        instance.setCodPostal("4415-900");

        assertEquals("4415-900", instance.getCodPostal());
    }

    /**
     * Teste para definir o código postal do cliente.
     */
    @Test
    public void testSetCodPostal() {
        System.out.println("setCodPostal");
        Cliente instance = new Cliente();
        instance.setCodPostal("4415-900");

    }

    /**
     * Teste para retirar o pais do cliente.
     */
    @Test
    public void testGetPais() {
        System.out.println("getPais");
        Cliente instance = new Cliente();
        instance.setPais("Portugal");

        assertEquals("Portugal", instance.getPais());

    }

    /**
     * Teste para definir o pais do cliente.
     */
    @Test
    public void testSetPais() {
        System.out.println("setPais");
        Cliente instance = new Cliente();
        instance.setPais("Portugal");

    }

    /**
     * Teste para definir o contribuinte do cliente.
     */
    @Test
    public void testGetContribuinte() {
        System.out.println("getContribuinte");
        Cliente instance = new Cliente();
        instance.setContribuinte(156398712);
        int expResult = 156398712;

        assertEquals(expResult, instance.getContribuinte());

    }

    /**
     * Teste para definir o contribuinte do cliente.
     */
    @Test
    public void testSetContribuinte() {
        System.out.println("setContribuinte");
        Cliente instance = new Cliente();
        instance.setContribuinte(156329874);

    }

    /**
     * Teste para retirar o estado do cliente.
     */
    @Test
    public void testGetEstado() {
        System.out.println("getEstado");
        Cliente instance = new Cliente();
        instance.setEstado("ativo");
        assertEquals("ativo", instance.getEstado());

    }

    /**
     * Teste para definir o estado do cliente.
     */
    @Test
    public void testSetEstado() {
        System.out.println("setEstado");
        Cliente instance = new Cliente();
        instance.setEstado("ativo");
    }

}
