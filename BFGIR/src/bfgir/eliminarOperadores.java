/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bfgir;

import classes.helper;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * classe do ecrã eliminarOperadores
 */
public class eliminarOperadores extends Application {

    private static Stage stage;

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/ecras/eliminarOperadores.fxml"));

        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.setTitle("F&O - Eliminar Operadores");
        stage.getIcons().add(helper.appIcon);
        stage.setResizable(false);
        stage.show();
        setStage(stage);

    }

    /**
     * método main do ecrã eliminarOperadores
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * método getStage do ecrã eliminarOperadores
     *
     * @return retorna o JAVAFX container do ecrã eliminarOperadores
     */
    public static Stage getStage() {
        return stage;
    }

    /**
     * método setStage do ecrã eliminarOperadores
     *
     * @param aStage the aStage to set
     */
    public static void setStage(Stage aStage) {
        stage = aStage;
    }

}
