/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import bfgir.BDConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe encomendas com metodos de acesso a tabela de encomendas na BD
 */
public class encomendas {

    private int idEncomenda;
    private int idCliente;
    private String dataEncomenda;
    private String moradaEntrega;
    private String codPostalEntrega;
    private String paisEntrega;
    private String moradaFaturacao;
    private String codPostalFaturacao;
    private String paisFaturacao;
    private Float precoTotalSIVA;
    private int IVA;
    private Float totalIVA;
    private Float precoTotalCIVA;
    private String estado;

    /**
     * Construtor da classe encomendas
     */
    public encomendas() {
    }

    /**
     * Construtor da classe encomendas<br>
     *
     * @param idEncomenda id da encomenda
     * @param idCliente id do cliente associado a encomenda
     * @param precoTotalCIVA preco total com iva da encomenda
     * @param moradaEntrega morada de entrega da encomenda
     * @param dataEncomenda data da encomenda
     * @param codPostalEntrega codigo postal de entraga da encomenda
     * @param moradaFaturacao morada de faturação da encomenda
     * @param totalIVA total de iva da encomenda
     * @param paisEntrega pais de entrega da encomenda
     * @param paisFaturacao pais de faturacao da encomenda
     * @param codPostalFaturacao codigo postal de faturacao da encomenda
     * @param precoTotalSIVA preco total sem iva da encomenda
     * @param IVA taxa de IVA da encomenda
     * @param estado estado da encomenda
     */
    public encomendas(int idEncomenda, int idCliente, String dataEncomenda, String moradaEntrega, String codPostalEntrega, String paisEntrega, String moradaFaturacao, String codPostalFaturacao, String paisFaturacao, Float precoTotalSIVA, int IVA, Float totalIVA, Float precoTotalCIVA, String estado) {
        this.idEncomenda = idEncomenda;
        this.dataEncomenda = dataEncomenda;
        this.moradaEntrega = moradaEntrega;
        this.codPostalEntrega = codPostalEntrega;
        this.paisEntrega = paisEntrega;
        this.moradaFaturacao = moradaFaturacao;
        this.codPostalFaturacao = codPostalFaturacao;
        this.paisFaturacao = paisFaturacao;
        this.precoTotalSIVA = precoTotalSIVA;
        this.IVA = IVA;
        this.totalIVA = totalIVA;
        this.precoTotalCIVA = precoTotalCIVA;
        this.estado = estado;

    }

    /**
     * @return the idEncomenda
     */
    public int getIdEncomenda() {
        return idEncomenda;
    }

    /**
     * @param idEncomenda the idEncomenda to set
     */
    public void setIdEncomenda(int idEncomenda) {
        this.idEncomenda = idEncomenda;
    }

    /**
     * @return the idCliente
     */
    public int getIdCliente() {
        return idCliente;
    }

    /**
     * @param idCliente the idCliente to set
     */
    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    /**
     * @return the dataEncomenda
     */
    public String getDataEncomenda() {
        return dataEncomenda;
    }

    /**
     * @param dataEncomenda the dataEncomenda to set
     */
    public void setDataEncomenda(String dataEncomenda) {
        this.dataEncomenda = dataEncomenda;
    }

    /**
     * @return the moradaEntrega
     */
    public String getMoradaEntrega() {
        return moradaEntrega;
    }

    /**
     * @param moradaEntrega the moradaEntrega to set
     */
    public void setMoradaEntrega(String moradaEntrega) {
        this.moradaEntrega = moradaEntrega;
    }

    /**
     * @return the codPostalEntrega
     */
    public String getCodPostalEntrega() {
        return codPostalEntrega;
    }

    /**
     * @param codPostalEntrega the codPostalEntrega to set
     */
    public void setCodPostalEntrega(String codPostalEntrega) {
        this.codPostalEntrega = codPostalEntrega;
    }

    /**
     * @return the paisEntrega
     */
    public String getPaisEntrega() {
        return paisEntrega;
    }

    /**
     * @param paisEntrega the paisEntrega to set
     */
    public void setPaisEntrega(String paisEntrega) {
        this.paisEntrega = paisEntrega;
    }

    /**
     * @return the moradaFaturacao
     */
    public String getMoradaFaturacao() {
        return moradaFaturacao;
    }

    /**
     * @param moradaFaturacao the moradaFaturacao to set
     */
    public void setMoradaFaturacao(String moradaFaturacao) {
        this.moradaFaturacao = moradaFaturacao;
    }

    /**
     * @return the codPostalFaturacao
     */
    public String getCodPostalFaturacao() {
        return codPostalFaturacao;
    }

    /**
     * @param codPostalFaturacao the codPostalFaturacao to set
     */
    public void setCodPostalFaturacao(String codPostalFaturacao) {
        this.codPostalFaturacao = codPostalFaturacao;
    }

    /**
     * @return the paisFaturacao
     */
    public String getPaisFaturacao() {
        return paisFaturacao;
    }

    /**
     * @param paisFaturacao the paisFaturacao to set
     */
    public void setPaisFaturacao(String paisFaturacao) {
        this.paisFaturacao = paisFaturacao;
    }

    /**
     * @return the precoTotalSIVA
     */
    public Float getPrecoTotalSIVA() {
        return precoTotalSIVA;
    }

    /**
     * @param precoTotalSIVA the precoTotalSIVA to set
     */
    public void setPrecoTotalSIVA(Float precoTotalSIVA) {
        this.precoTotalSIVA = precoTotalSIVA;
    }

    /**
     * @return the IVA
     */
    public int getIVA() {
        return IVA;
    }

    /**
     * @param IVA the IVA to set
     */
    public void setIVA(int IVA) {
        this.IVA = IVA;
    }

    /**
     * @return the totalIVA
     */
    public Float getTotalIVA() {
        return totalIVA;
    }

    /**
     * @param totalIVA the totalIVA to set
     */
    public void setTotalIVA(Float totalIVA) {
        this.totalIVA = totalIVA;
    }

    /**
     * @return the precoTotalCIVA
     */
    public Float getPrecoTotalCIVA() {
        return precoTotalCIVA;
    }

    /**
     * @param precoTotalCIVA the precoTotalCIVA to set
     */
    public void setPrecoTotalCIVA(Float precoTotalCIVA) {
        this.precoTotalCIVA = precoTotalCIVA;
    }

    /**
     * @return the estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * Método para selecionar o objeto todo da tabela encomenda pelo ID de
     * Cliente na BD<br>
     *
     * Este método recebe por parametro o ID do Cliente (idCliente). Posto isto
     * faz um select à BD adicionando a um arrayList o atributo idCliente de um
     * cliente com aquele ID de cliente.<br>
     *
     * @param idCliente id de cliente a pesquisar
     * @return id de cliente
     *
     *
     *
     */
    public static List<encomendas> verificarClienteEncomenda(int idCliente) {

        List<encomendas> encomenda = new ArrayList<encomendas>();
        String sql = "SELECT * from encomendas where idCliente = (?) ";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setInt(1, idCliente);

            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                encomendas enc = new encomendas();
                enc.setIdEncomenda(results.getInt(1));
                enc.setIdCliente(results.getInt(2));

                encomenda.add(enc);
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return encomenda;
    }

}
