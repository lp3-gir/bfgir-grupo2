/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import bfgir.eliminarFornecedores;
import bfgir.login;
import bfgir.menuAdministrador;
import bfgir.menuOperador;
import static classes.administrador.eliminarUtilizador;
import classes.codigoPostal;
import static classes.codigoPostal.procurarCodPostal;
import classes.fornecedor;
import static classes.fornecedor.eliminarFornecedorr;
import static classes.fornecedor.listarFornecedor;
import static classes.fornecedor.listarFornecedorTodosAtivos;
import classes.utilizador;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * FXML Controller class
 */
public class eliminarFornecedorController implements Initializable {

    int nivel_fornecedor = 0;
    int id_idUtilizador = 0;
    String idFornecedor;
    Alert a = new Alert(Alert.AlertType.NONE);

    @FXML
    private AnchorPane painelFornecedor;
    @FXML
    private ScrollPane fornecedoresScrollPane;
    @FXML
    private VBox vBoxFornecedores;
    @FXML
    private Label lbNome;
    @FXML
    private Label lbMorada;
    @FXML
    private Label lbContribuinte;
    @FXML
    private TextArea nome;
    @FXML
    private TextArea contribuinte;
    @FXML
    private TextArea codigoPostal;
    @FXML
    private Label lbCodigoPostal;
    @FXML
    private TextArea morada;
    @FXML
    private Button eliminarFornecedor;
    @FXML
    private Label lbPais;
    @FXML
    private TextArea pais;
    @FXML
    private Label lbEstado;
    @FXML
    private TextArea estado;
    @FXML
    private Label lbEmail;
    @FXML
    private TextArea email;
    @FXML
    private Label lbCidade;
    @FXML
    private TextArea cidade;
    @FXML
    private MenuBar menu;

    private int altura_vbox = 0;

    /**
     * Método Intialize<br>
     *
     * Neste método criada uma lista com todos os fornecedores ativos ordenados
     * por ordem crescente do seu ID de Utilizador obtidos pelo método
     * listarFornecedorTodos. De seguida para cada elemento dessa lista de
     * fornecedores é criado um botão com o ID Utilizador do fornecedor. Depois
     * de selecionar um fornecedor é feito um select com o codigo do mesmo
     * (idUtilizador) através do método listarFornecedor que retorna o
     * fornecedor correspondente ao idUtilizador, este fornecedor é descriminado
     * por cada atributo e listado no ecrã.<br>
     *
     * @param url url
     * @param rb rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        lbNome.setVisible(false);
        lbMorada.setVisible(false);
        lbCodigoPostal.setVisible(false);
        lbContribuinte.setVisible(false);
        lbPais.setVisible(false);
        lbEstado.setVisible(false);
        lbCidade.setVisible(false);
        lbEmail.setVisible(false);

        eliminarFornecedor.setVisible(false);

        nome.setVisible(false);
        morada.setVisible(false);
        codigoPostal.setVisible(false);
        contribuinte.setVisible(false);
        pais.setVisible(false);
        estado.setVisible(false);
        cidade.setVisible(false);
        email.setVisible(false);

        List<fornecedor> forn = listarFornecedorTodosAtivos();
        for (fornecedor fornecedores : forn) {
            String idF = fornecedores.getIdFornecedor();
            int id = fornecedores.getIdUtilizador();
            Button botao = new Button(String.valueOf(id));
            botao.setId("botaoVbox");
            botao.setPrefHeight(30);
            botao.setPrefWidth(275);
            botao.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {
                    botao.setId("botaoVboxChangeCollour");
                    setVisebleTrue();
                    List<fornecedor> fornecedores = listarFornecedor(id);
                    List<utilizador> utEm = utilizador.procurarEmail(id);
                    utilizador ut = utEm.get(0);
                    String email123 = ut.getEmail();
                    fornecedor forn = fornecedores.get(0);
                    String codP = forn.getCodPostal();
                    List<codigoPostal> codCidade = procurarCodPostal(codP);
                    codigoPostal cdO = codCidade.get(0);
                    String city = cdO.getCidade();
                    for (fornecedor fornecedor : fornecedores) {
                        nome.setText(fornecedor.getNome());
                        morada.setText(fornecedor.getMorada());
                        codigoPostal.setText(fornecedor.getCodPostal());
                        contribuinte.setText(String.valueOf(fornecedor.getContribuinte()));
                        pais.setText(fornecedor.getPais());
                        estado.setText(fornecedor.getEstado());
                        email.setText(email123);
                        cidade.setText(city);
                        id_idUtilizador = id;
                        idFornecedor = idF;

                    }

                }
            });
            altura_vbox = altura_vbox + 30;
            vBoxFornecedores.getChildren().addAll(botao);
            if (altura_vbox > 550) {
                vBoxFornecedores.setPrefHeight(altura_vbox);
            }
        }

    }

    /**
     * Método para tornar visivel os atributos do fornecedor selecionado.
     */
    private void setVisebleTrue() {

        lbNome.setVisible(true);
        lbMorada.setVisible(true);
        lbCodigoPostal.setVisible(true);
        lbContribuinte.setVisible(true);
        lbPais.setVisible(true);
        lbEstado.setVisible(true);
        lbEmail.setVisible(true);
        lbCidade.setVisible(true);

        eliminarFornecedor.setVisible(true);

        nome.setVisible(true);
        morada.setVisible(true);
        codigoPostal.setVisible(true);
        contribuinte.setVisible(true);
        pais.setVisible(true);
        estado.setVisible(true);
        email.setVisible(true);
        cidade.setVisible(true);

    }

    /**
     * método para fechar o ecrã de eliminação de fornecedores
     */
    private void fecha() {
        eliminarFornecedores.getStage().close();
    }

    /**
     * metodo para aceder ao menu administrador<br>
     *
     * @param event evento
     */
    private void ecraPessoal(ActionEvent event) {
        menuAdministrador menuAdmin = new menuAdministrador();
        fecha();
        menuAdmin.start(new Stage());

    }

    /**
     * Metodo para terminar sessao do utilizador
     *
     * @param event evento
     */
    @FXML
    private void terminarSessao(ActionEvent event) {
        try {
            login.setIdLogin(0);
            login.setNivelLogin(10000);
            login log = new login();
            fecha();
            log.start(new Stage());

        } catch (Exception ex) {
            Logger.getLogger(menuAdministradorController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Método para encerrar a aplicação
     */
    @FXML
    private void sair(ActionEvent event) {
        System.exit(0);
    }

    /**
     * Método para aceder ao menu do administrador
     */
    @FXML
    private void openMenu(ActionEvent event) {
        try {
            if (login.getNivelLogin() == 1) {
                menuOperador menuO = new menuOperador();
                fecha();
                menuO.start(new Stage());
            } else {
                menuAdministrador menuAdmin = new menuAdministrador();
                fecha();
                menuAdmin.start(new Stage());
            }
        } catch (Exception ex) {
            Logger.getLogger(eliminarFornecedorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Esta método vai verificar se o fornecedor tem encomendas ativas, se se
     * verificar não é possivel apaga-lo da BD se não se verificar é possivel
     * apaga-lo e ele sera de facto eliminado<br>
     *
     * @param event evento
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    @FXML
    private void eliminarFornecedor(ActionEvent event) {

        fornecedor forn = new fornecedor();
        //notificação se quer mesmo eliminar o fornecedor
        a.setAlertType(Alert.AlertType.CONFIRMATION);
        a.setTitle("Fornecedores");
        a.setHeaderText("Deseja mesmo eliminar o Fornecedor da lista de Fornecedores ativos?");
        a.setResizable(false);
        a.getButtonTypes().setAll(ButtonType.YES, ButtonType.NO);
        Optional<ButtonType> result = a.showAndWait();

        if (result.get() == ButtonType.YES) {
            //verifica se o fornecedor tem encomendas ou não, se não, elimina e notifica
            if (forn.verificarFornecedorEntrada(idFornecedor).isEmpty() == true) {

                Alert a2 = new Alert(Alert.AlertType.NONE);

                eliminarFornecedorr(id_idUtilizador);
                eliminarUtilizador(id_idUtilizador, nivel_fornecedor);

                a2.setAlertType(Alert.AlertType.INFORMATION);
                a2.setTitle("Fornecedores");
                a2.setHeaderText("Fornecedor eliminado com sucesso!");
                a2.setContentText("O Fornecedor selecionado foi eliminado da lista de Fornecedores ativos!");
                a2.getButtonTypes().setAll(ButtonType.OK);
                Optional<ButtonType> result2 = a2.showAndWait();

                eliminarFornecedores eliForn = new eliminarFornecedores();
                fecha();

                try {
                    eliForn.start(new Stage());
                } catch (Exception ex) {
                    a.setAlertType(Alert.AlertType.ERROR);
                    a.setTitle("ERRO Fatal");
                    a.setHeaderText("A aplicação está corrompida!");
                    a.setContentText("Algumas funcionalidades do aplicação estão inacessíveis. Tente uma reinstalação.");
                    a.show();
                    System.out.println("Erro Exception ex " + ex.getMessage());
                    Logger.getLogger(eliminarFornecedorController.class.getName()).log(Level.SEVERE, null, ex);
                }

            } else {
                Alert a2 = new Alert(Alert.AlertType.NONE);

                a2.setAlertType(Alert.AlertType.INFORMATION);
                a2.setTitle("Fornecedores");
                a2.setHeaderText("Fornecedor com entrdas!");
                a2.setContentText("O Fornecedor selecionado tem entradas de produto ativas e NÃO pode ser eliminado!");
                a2.getButtonTypes().setAll(ButtonType.OK);
                Optional<ButtonType> result2 = a2.showAndWait();

                eliminarFornecedores eliForn = new eliminarFornecedores();
                fecha();

                try {
                    eliForn.start(new Stage());
                } catch (Exception ex) {
                    a.setAlertType(Alert.AlertType.ERROR);
                    a.setTitle("ERRO Fatal");
                    a.setHeaderText("A aplicação está corrompida!");
                    a.setContentText("Algumas funcionalidades do aplicação estão inacessíveis. Tente uma reinstalação.");
                    a.show();
                    System.out.println("Erro Exception ex " + ex.getMessage());
                    Logger.getLogger(eliminarFornecedorController.class.getName()).log(Level.SEVERE, null, ex);
                }

            }

        }

    }

}
