/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import bfgir.eliminarDescontosProdutos;
import bfgir.login;
import bfgir.menuAdministrador;
import bfgir.menuOperador;
import classes.clienteProduto;
import static classes.clienteProduto.getProdAVendaCodProd;
import classes.desconto;
import static classes.desconto.getListaDescontos;
import static classes.desconto.getListaDescontosID;

import classes.descontosProduto;
import static classes.descontosProduto.eliminarDescontosProdutos;
import static classes.descontosProduto.getListaDescontosPordutosIDDes;
import static classes.descontosProduto.getListaDescontosPordutosIDDesCod;
import classes.entradasProdutos;
import static classes.entradasProdutos.procurarTipoQuantidadeProdutoActivos;
import classes.produto;
import static classes.produto.pesquisarProdutoID;
import static controllers.inserirDescontosController.a;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author ruben
 */
public class eliminarDescontosProdutosController implements Initializable {

    @FXML
    private Pane painelListarOperadores;
    @FXML
    private ScrollPane descontosScrollPane;
    @FXML
    private VBox vBoxListarDescontos;
    @FXML
    private AnchorPane fundo;
    @FXML
    private Label ldIdDes;
    @FXML
    private TextArea idDesconto;
    @FXML
    private Label lbQuantDes;
    @FXML
    private TextArea quantDes;
    @FXML
    private Button eliminarDesconto;
    @FXML
    private MenuBar menu;

    private int altura_vbox = 0;

    private int idDesEliminar = 0;
    @FXML
    private Label IdIdCodProd;
    @FXML
    private TextArea idCodProd;
    @FXML
    private Label IdIdDescricao;
    @FXML
    private TextArea idDescricao;
    private String codPra = "";
    @FXML
    private ScrollPane ProdutosScrollPane;
    @FXML
    private VBox vBoxListarProdutos;

    /**
     * Initializes the controller class.<br>
     *
     * Faz a listagem dos descontos para cada desconto é criado um cliente com o
     * seu ID (idDesconto), carregando no butao é dado feita uma lista dos seus
     * produtos as informações sobre este produto são inicializadas a false o
     * que as tornam inviveis<br>
     *
     * @param url url
     * @param rb rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        IdIdCodProd.setVisible(false);
        idCodProd.setVisible(false);
        IdIdDescricao.setVisible(false);
        idDescricao.setVisible(false);
        ldIdDes.setVisible(false);
        idDesconto.setVisible(false);
        lbQuantDes.setVisible(false);
        quantDes.setVisible(false);
        eliminarDesconto.setVisible(false);

        List<desconto> descontos = getListaDescontos();
        for (desconto des : descontos) {

            int id = des.getIdDesconto();
            String idInt = String.valueOf(id);
            String nome = idInt + " - " + des.getQuantDesconto() + " %";
            Button botao = new Button(String.valueOf(nome));
            botao.setId("botaoVbox");
            botao.setPrefHeight(30);
            botao.setPrefWidth(275);

            botao.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {
                    botao.setId("botaoVboxChangeCollour");
                    idDesEliminar = id;
                    listarProdutos(id);

                }
            });

            vBoxListarDescontos.getChildren().addAll(botao);
            if (altura_vbox > 550) {
                vBoxListarDescontos.setPrefHeight(altura_vbox);
            }

        }
    }

    /**
     * Metodo utilizado para eliminar o desconto que o administrador seleciou.
     * Primeiro o metodo faz uma verificação se o produto já não se encontra
     * numa encomeda, e em seguida se não estiver numa encomenda utiliza o id do
     * desconto , idDesEliminar , e o codigo do produto ,codPra, para eliminar a
     * associação que foi realizada com esse id e codigo de produto
     */
    @FXML
    private void eliminarDesconto(ActionEvent event) throws Exception {
        int i = 0;
        List<entradasProdutos> listaDeProdEnc = procurarTipoQuantidadeProdutoActivos(codPra);
        for (entradasProdutos enc : listaDeProdEnc) {
            i++;

        }

        int j = 0;
        List<clienteProduto> cli = getProdAVendaCodProd(codPra);
        for (clienteProduto c : cli) {
            j++;

        }

        if (j == 1) {
            a.setAlertType(Alert.AlertType.WARNING);
            a.getButtonTypes().setAll(ButtonType.OK);
            a.setTitle("Produto com clientes");
            a.setHeaderText(" O produto escolhido já tem um cliente!");
            a.show();

        }

        if (i == 1) {
            a.setAlertType(Alert.AlertType.WARNING);
            a.getButtonTypes().setAll(ButtonType.OK);
            a.setTitle("Produto numa encomenda");
            a.setHeaderText(" O produto encontra-se numa encomenda!");
            a.show();

        }

        if (i == 0 && j == 0) {
            eliminarDescontosProdutos(idDesEliminar, codPra);
            a.setAlertType(Alert.AlertType.INFORMATION);
            a.getButtonTypes().setAll(ButtonType.OK);
            a.setTitle("Desconto eliminado");
            a.setHeaderText("A associação foi eliminada com sucesso!");
            a.showAndWait();
            fecha();
            eliminarDescontosProdutos ep = new eliminarDescontosProdutos();
            ep.start(new Stage());

        }

    }

    /**
     * método para fechar o ecrã de listar as categorias
     */
    private void fecha() {
        eliminarDescontosProdutos.getStage().close();
    }

    /**
     * Método para aceder ao menu do administrador
     */
    @FXML
    void openMenu(ActionEvent event) {
        try {
            if (login.getNivelLogin() == 1) {
                menuOperador menuO = new menuOperador();
                fecha();
                menuO.start(new Stage());
            } else {
                menuAdministrador menuAdmin = new menuAdministrador();
                fecha();
                menuAdmin.start(new Stage());
            }
        } catch (Exception ex) {
            Logger.getLogger(eliminarDescontosController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Método para encerrar a aplicação
     */
    @FXML
    void sair(ActionEvent event) {
        System.exit(0);
    }

    /**
     * Metodo para terminar sessao do utilizador<br>
     *
     * @param event evento
     */
    @FXML
    void terminarSessao(ActionEvent event) {
        try {
            login.setIdLogin(0);
            login.setNivelLogin(10000);
            login log = new login();
            fecha();
            log.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(eliminarDescontosController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo que recebe o id do desconto e tem como função tornar as
     * informaçõoes da associação de invisiveis para visiveis e mostrar no ecrã
     * o id do produto, a sua descrição, o desconto e a sua taxa
     *
     */
    private void listarDescontos(int id) {

        ldIdDes.setVisible(true);
        idDesconto.setVisible(true);
        lbQuantDes.setVisible(true);
        quantDes.setVisible(true);
        eliminarDesconto.setVisible(true);
        IdIdCodProd.setVisible(true);
        idCodProd.setVisible(true);
        IdIdDescricao.setVisible(true);
        idDescricao.setVisible(true);

        List<descontosProduto> desco = getListaDescontosPordutosIDDesCod(id, codPra);
        for (descontosProduto desc : desco) {
            List<desconto> descs = getListaDescontosID(desc.getIdDesconto());

            for (desconto d : descs) {
                idDesconto.setText(String.valueOf(d.getIdDesconto()));
                quantDes.setText(String.valueOf(d.getQuantDesconto()) + " %");

            }
            List<produto> prod = pesquisarProdutoID(desc.getCodProdPa());
            for (produto pr : prod) {
                idDescricao.setText(pr.getDescricao());
                idCodProd.setText(pr.getCodProdPa());

            }

        }

    }

    /**
     * Metodo que recebe o id dos descontos e faz uma listagem de butões com os
     * ids dos produtos que foram associados a esse desconto. Assim que um dos
     * botões é clicado vai ao método listarDesconto(), onde vai fazer um método
     * para esse botão
     *
     */
    private void listarProdutos(int ids) {
        vBoxListarProdutos.getChildren().removeAll(vBoxListarProdutos.getChildren());
        List<descontosProduto> desco = getListaDescontosPordutosIDDes(ids);
        for (descontosProduto d : desco) {
            List<produto> prod = pesquisarProdutoID(d.getCodProdPa());
            for (produto pr : prod) {
                String id = pr.getCodProdPa();
                String codpr = pr.getCodProdPa();
                String nome = pr.getCodProdPa();
                Button botao = new Button(nome);
                botao.setId("botaoVbox");
                botao.setPrefHeight(30);
                botao.setPrefWidth(570);
                botao.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent e) {
                        botao.setId("botaoVboxChangeCollour");
                        codPra = pr.getCodProdPa();
                        listarDescontos(ids);

                    }
                });

                vBoxListarProdutos.getChildren().addAll(botao);
                if (altura_vbox > 550) {
                    vBoxListarProdutos.setPrefHeight(altura_vbox);
                }

            }
        }
    }
}
