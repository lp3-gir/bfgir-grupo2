/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ruben
 */
public class UtilizadorTest {



    /**
     * Test of getIdUtilizador method, of class Utilizador.
     */
    @Test
    public void testGetIdUtilizador() {
        System.out.println("getIdUtilizador");
        Utilizador instance = new Utilizador();
        instance.setIdUtilizador(1);
        Integer expResult = 1;
        assertEquals(expResult, instance.getIdUtilizador());

    }

    /**
     * Test of setIdUtilizador method, of class Utilizador.
     */
    @Test
    public void testSetIdUtilizador() {
        System.out.println("setIdUtilizador");
        Utilizador instance = new Utilizador();
        instance.setIdUtilizador(1);

    }

    /**
     * Test of getEmail method, of class Utilizador.
     */
    @Test
    public void testGetEmail() {
        System.out.println("getEmail");
        Utilizador instance = new Utilizador();
        instance.setEmail("teste@teste.com");
        assertEquals("teste@teste.com", instance.getEmail());

    }

    /**
     * Test of setEmail method, of class Utilizador.
     */
    @Test
    public void testSetEmail() {
        System.out.println("setEmail");
        Utilizador instance = new Utilizador();
        instance.setEmail("teste@teste.com");

    }

    /**
     * Test of getPassword method, of class Utilizador.
     */
    @Test
    public void testGetPassword() {
        System.out.println("getPassword");
        Utilizador instance = new Utilizador();
        instance.setPassword("teste");
        assertEquals("teste", instance.getPassword());

    }

    /**
     * Test of setPassword method, of class Utilizador.
     */
    @Test
    public void testSetPassword() {
        System.out.println("setPassword");
        Utilizador instance = new Utilizador();
        instance.setPassword("teste");

    }

    /**
     * Test of getSalt method, of class Utilizador.
     */
    @Test
    public void testGetSalt() {
        System.out.println("getSalt");
        Utilizador instance = new Utilizador();
        instance.setSalt("lkhdrglkdrbnglkdrg");
        assertEquals("lkhdrglkdrbnglkdrg", instance.getSalt());

    }

    /**
     * Test of setSalt method, of class Utilizador.
     */
    @Test
    public void testSetSalt() {
        System.out.println("setSalt");
        Utilizador instance = new Utilizador();
        instance.setSalt("lkhdrglkdrbnglkdrg");

    }

    /**
     * Test of getNivel method, of class Utilizador.
     */
    @Test
    public void testGetNivel() {
        System.out.println("getNivel");
        Utilizador instance = new Utilizador();
        instance.setNivel(1);
        assertEquals(1, instance.getNivel());

    }

    /**
     * Test of setNivel method, of class Utilizador.
     */
    @Test
    public void testSetNivel() {
        System.out.println("setNivel");
        Utilizador instance = new Utilizador();
        instance.setNivel(1);

    }

    /**
     * Test of getEstado method, of class Utilizador.
     */
    @Test
    public void testGetEstado() {
        System.out.println("getEstado");
        Utilizador instance = new Utilizador();
        instance.setEstado("ativo");
        assertEquals("ativo", instance.getEstado());

    }

    /**
     * Test of setEstado method, of class Utilizador.
     */
    @Test
    public void testSetEstado() {
        System.out.println("setEstado");
        Utilizador instance = new Utilizador();
        instance.setEstado("ativo");

    }

}
