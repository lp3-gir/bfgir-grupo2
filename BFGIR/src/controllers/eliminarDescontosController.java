/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import bfgir.eliminarDescontos;
import bfgir.login;
import bfgir.menuAdministrador;
import bfgir.menuOperador;
import classes.desconto;
import static classes.desconto.eliminarDes;
import static classes.desconto.getListaDescontos;
import static classes.desconto.getListaDescontosID;
import classes.descontosProduto;
import static classes.descontosProduto.getListaDescontosPordutosIDDes;
import static controllers.inserirDescontosController.a;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author ruben
 */
public class eliminarDescontosController implements Initializable {

    @FXML
    private Pane painelListarOperadores;
    @FXML
    private ScrollPane descontosScrollPane;
    @FXML
    private VBox vBoxListarDescontos;
    @FXML
    private AnchorPane fundo;
    @FXML
    private Label ldIdDes;
    @FXML
    private TextArea idDesconto;
    @FXML
    private Label lbQuantDes;
    @FXML
    private TextArea quantDes;
    @FXML
    private Button eliminarDesconto;
    @FXML
    private MenuBar menu;

    private int altura_vbox = 0;

    private int idDesEliminar = 0;

    /**
     * Método Intialize<br>
     *
     * Neste método criada uma lista com todas os descontos obtidas pelo método
     * getListaDescontos. De seguida para cada elemento dessa lista de categorias
     * é criado um botão com o id do desconto. <br>
     *
     * @param url url
     * @param rb rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO

        ldIdDes.setVisible(false);
        idDesconto.setVisible(false);
        lbQuantDes.setVisible(false);
        quantDes.setVisible(false);
        eliminarDesconto.setVisible(false);

        List<desconto> descontos = getListaDescontos();
        for (desconto des : descontos) {

            int id = des.getIdDesconto();
            Button botao = new Button(String.valueOf(id));
            botao.setId("botaoVbox");
            botao.setPrefHeight(30);
            botao.setPrefWidth(275);

            botao.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {
                    botao.setId("botaoVboxChangeCollour");
                    idDesEliminar = id;
                    listarDescontos(id);

                }
            });

            altura_vbox = altura_vbox + 30;
            vBoxListarDescontos.getChildren().addAll(botao);
            if (altura_vbox > 550) {
                vBoxListarDescontos.setPrefHeight(altura_vbox);
            }

        }
    }

    /**
     * Eliminar desconto<br>
     *
     * Neste método é realizado um teste para verificar se a taxa de desconto não foi associada a nenhum produto. <br>
     * Se se verificar que o desconto não tem nenhum produton associado é realizada a eliminação do desconto através do
     * id do mesmo<br>
     * 
     * @param idDesconto=id do desconto a eliminar 
     */
    @FXML
    private void eliminarDesconto(ActionEvent event) {

        int i = 0;
        List<descontosProduto> listaDescontosProds = getListaDescontosPordutosIDDes(idDesEliminar);
        for (descontosProduto desProd : listaDescontosProds) {
            i++;
        }

        if (i == 0) {
            try {
                eliminarDes(idDesEliminar);
                a.setAlertType(Alert.AlertType.INFORMATION);
                a.getButtonTypes().setAll(ButtonType.OK);
                a.setTitle("Desconto eliminado");
                a.setHeaderText("A taxa de desconto escolhida foi eliminada com sucesso!");
                a.showAndWait();
                fecha();
                eliminarDescontos eliDes = new eliminarDescontos();
                eliDes.start(new Stage());
            } catch (Exception ex) {
                Logger.getLogger(eliminarDescontosController.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else {
            a.setAlertType(Alert.AlertType.WARNING);
            a.getButtonTypes().setAll(ButtonType.OK);
            a.setTitle("Desconto com associações");
            a.setHeaderText("A taxa de desconto escolhida tem produtos associados!");
            a.show();
        }
    }

    /**
     * método para fechar o ecrã de listar as categorias
     */
    private void fecha() {
        eliminarDescontos.getStage().close();
    }

    /**
     * Método para aceder ao menu do administrador
     */
    @FXML
    void openMenu(ActionEvent event) {
        try {
            if (login.getNivelLogin() == 1) {
                menuOperador menuO = new menuOperador();
                fecha();
                menuO.start(new Stage());
            } else {
                menuAdministrador menuAdmin = new menuAdministrador();
                fecha();
                menuAdmin.start(new Stage());
            }
        } catch (Exception ex) {
            Logger.getLogger(eliminarDescontosController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Método para encerrar a aplicação
     */
    @FXML
    void sair(ActionEvent event) {
        System.exit(0);
    }

    /**
     * Metodo para terminar sessao do utilizador<br>
     *
     * @param event evento
     */
    @FXML
    void terminarSessao(ActionEvent event) {
        try {
            login.setIdLogin(0);
            login.setNivelLogin(10000);
            login log = new login();
            fecha();
            log.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(eliminarDescontosController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Lista informação de descontos
     *
     *Este método recebe um id do qual faz uma lista de toda a informção do desconto
     *que tenha ess id. <br>
     *
     * @param id= id que vai utilizar para fazer a listagem de informação sobre o desconto.
     * 
     */
    private void listarDescontos(int id) {

        ldIdDes.setVisible(true);
        idDesconto.setVisible(true);
        lbQuantDes.setVisible(true);
        quantDes.setVisible(true);
        eliminarDesconto.setVisible(true);

        List<desconto> desco = getListaDescontosID(id);
        for (desconto desc : desco) {

            idDesconto.setText(String.valueOf(desc.getIdDesconto()));
            String nome = String.valueOf(desc.getQuantDesconto() + " %");
            quantDes.setText(nome);

        }
    }
}
