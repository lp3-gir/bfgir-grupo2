/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ruben
 */
public class ProdutofornecedorPKTest {


    /**
     * Test of getCodProdForn method, of class ProdutofornecedorPK.
     */
    @Test
    public void testGetCodProdForn() {
        System.out.println("getCodProdForn");
        ProdutofornecedorPK instance = new ProdutofornecedorPK();
        instance.setCodProdForn("Teste");
        assertEquals("Teste", instance.getCodProdForn());

    }

    /**
     * Test of setCodProdForn method, of class ProdutofornecedorPK.
     */
    @Test
    public void testSetCodProdForn() {
        System.out.println("setCodProdForn");
        ProdutofornecedorPK instance = new ProdutofornecedorPK();
        instance.setCodProdForn("Teste");

    }

    /**
     * Test of getCodProdPa method, of class ProdutofornecedorPK.
     */
    @Test
    public void testGetCodProdPa() {
        System.out.println("getCodProdPa");
        ProdutofornecedorPK instance = new ProdutofornecedorPK();
        instance.setCodProdPa("T.123");
        assertEquals("T.123", instance.getCodProdPa());

    }

    /**
     * Test of setCodProdPa method, of class ProdutofornecedorPK.
     */
    @Test
    public void testSetCodProdPa() {
        System.out.println("setCodProdPa");
        ProdutofornecedorPK instance = new ProdutofornecedorPK();
        instance.setCodProdPa("T.123");

    }

}
