/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ruben
 */
@Entity
@Table(name = "encomendasproduto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Encomendasproduto.findAll", query = "SELECT e FROM Encomendasproduto e")
    , @NamedQuery(name = "Encomendasproduto.findByIdEncProd", query = "SELECT e FROM Encomendasproduto e WHERE e.idEncProd = :idEncProd")
    , @NamedQuery(name = "Encomendasproduto.findByIdEncomenda", query = "SELECT e FROM Encomendasproduto e WHERE e.idEncomenda = :idEncomenda")
    , @NamedQuery(name = "Encomendasproduto.findByCodProdPa", query = "SELECT e FROM Encomendasproduto e WHERE e.codProdPa = :codProdPa")
    , @NamedQuery(name = "Encomendasproduto.findByQuantVendida", query = "SELECT e FROM Encomendasproduto e WHERE e.quantVendida = :quantVendida")
    , @NamedQuery(name = "Encomendasproduto.findByTipoVenda", query = "SELECT e FROM Encomendasproduto e WHERE e.tipoVenda = :tipoVenda")
    , @NamedQuery(name = "Encomendasproduto.findByPrecoUnitSIVA", query = "SELECT e FROM Encomendasproduto e WHERE e.precoUnitSIVA = :precoUnitSIVA")
    , @NamedQuery(name = "Encomendasproduto.findByPrecoUnitCIVA", query = "SELECT e FROM Encomendasproduto e WHERE e.precoUnitCIVA = :precoUnitCIVA")
    , @NamedQuery(name = "Encomendasproduto.findByEstado", query = "SELECT e FROM Encomendasproduto e WHERE e.estado = :estado")})
public class Encomendasproduto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idEncProd", nullable = false, unique = true)
    private Integer idEncProd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idEncomenda")
    private int idEncomenda;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "codProdPa")
    private String codProdPa;
    @Basic(optional = false)
    @NotNull
    @Column(name = "quantVendida")
    private long quantVendida;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "tipoVenda")
    private String tipoVenda;
    @Basic(optional = false)
    @NotNull
    @Column(name = "precoUnitSIVA")
    private double precoUnitSIVA;
    @Basic(optional = false)
    @NotNull
    @Column(name = "precoUnitCIVA")
    private double precoUnitCIVA;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "estado")
    private String estado;

    /**
     *
     */
    public Encomendasproduto() {
    }

    /**
     *
     * @param idEncProd recebe um id de uma encomenda produto
     */
    public Encomendasproduto(Integer idEncProd) {
        this.idEncProd = idEncProd;
    }

    /**
     *
     * @param idEncProd recebe um id de uma encomenda produto
     * @param idEncomenda recebe um id de uma encomenda
     * @param codProdPa recebe um código de um produto
     * @param quantVendida recebe um quantidade vendida
     * @param tipoVenda recebe um tipo de venda
     * @param precoUnitSIVA recebe um preço por unidade sem IVA
     * @param precoUnitCIVA recebe um preço por unidade com IVA
     * @param estado recebe um estado da encomenda produto
     */
    public Encomendasproduto(Integer idEncProd, int idEncomenda, String codProdPa, long quantVendida, String tipoVenda, double precoUnitSIVA, double precoUnitCIVA, String estado) {
        this.idEncProd = idEncProd;
        this.idEncomenda = idEncomenda;
        this.codProdPa = codProdPa;
        this.quantVendida = quantVendida;
        this.tipoVenda = tipoVenda;
        this.precoUnitSIVA = precoUnitSIVA;
        this.precoUnitCIVA = precoUnitCIVA;
        this.estado = estado;
    }

    /**
     *
     * @return idEncProd
     */
    public Integer getIdEncProd() {
        return idEncProd;
    }

    /**
     *
     * @param idEncProd
     */
    public void setIdEncProd(Integer idEncProd) {
        this.idEncProd = idEncProd;
    }

    /**
     *
     * @return idEncomenda
     */
    public int getIdEncomenda() {
        return idEncomenda;
    }

    /**
     *
     * @param idEncomenda
     */
    public void setIdEncomenda(int idEncomenda) {
        this.idEncomenda = idEncomenda;
    }

    /**
     *
     * @return codProdPa
     */
    public String getCodProdPa() {
        return codProdPa;
    }

    /**
     *
     * @param codProdPa
     */
    public void setCodProdPa(String codProdPa) {
        this.codProdPa = codProdPa;
    }

    /**
     *
     * @return quantVendida
     */
    public long getQuantVendida() {
        return quantVendida;
    }

    /**
     *
     * @param quantVendida
     */
    public void setQuantVendida(long quantVendida) {
        this.quantVendida = quantVendida;
    }

    /**
     *
     * @return tipoVenda
     */
    public String getTipoVenda() {
        return tipoVenda;
    }

    /**
     *
     * @param tipoVenda
     */
    public void setTipoVenda(String tipoVenda) {
        this.tipoVenda = tipoVenda;
    }

    /**
     *
     * @return precoUnitSIVA
     */
    public double getPrecoUnitSIVA() {
        return precoUnitSIVA;
    }

    /**
     *
     * @param precoUnitSIVA
     */
    public void setPrecoUnitSIVA(double precoUnitSIVA) {
        this.precoUnitSIVA = precoUnitSIVA;
    }

    /**
     *
     * @return precoUnitCIVA
     */
    public double getPrecoUnitCIVA() {
        return precoUnitCIVA;
    }

    /**
     *
     * @param precoUnitCIVA
     */
    public void setPrecoUnitCIVA(double precoUnitCIVA) {
        this.precoUnitCIVA = precoUnitCIVA;
    }

    /**
     *
     * @return estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     *
     * @param estado
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }
}
