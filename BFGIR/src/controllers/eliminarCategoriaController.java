package controllers;

import bfgir.ecraPessoalOperador;
import bfgir.login;
import bfgir.eliminarCategorias;
import bfgir.menuAdministrador;
import bfgir.menuOperador;
import classes.categoria;
import static classes.categoria.eliminarCategorias;
import static classes.categoria.listarCategorias;
import static classes.categoria.listarCategoriasPorId;
import classes.utilizador;
import static classes.utilizador.procurarEmail;
import static controllers.alterarCategoriasController.hasProductsCategoria;
import static controllers.alterarCategoriasController.isNewIdCategoria;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class eliminarCategoriaController implements Initializable {

    Alert a = new Alert(Alert.AlertType.NONE);

    int idCat;

    @FXML
    private AnchorPane painelCategorias;

    @FXML
    private ScrollPane categoriasScrollPane;

    @FXML
    private VBox vBoxCategorias;

    @FXML
    private Label lbEstado;

    @FXML
    private TextArea estado;

    @FXML
    private Label lbDescricao;

    @FXML
    private TextArea descricao;

    @FXML
    private Label lbNome;

    @FXML
    private TextArea nome;

    @FXML
    private MenuBar menu;

    @FXML
    private Button eliminarCategoria;

    @FXML
    private Button btnPesquisa;

    @FXML
    private TextField pesquisa;
    @FXML
    private Label lbNivel;

    private int altura_vbox = 0;
    @FXML
    private TextArea nivel;
    @FXML
    private Menu opcoes;
    @FXML
    private Label lblID;
    @FXML
    private TextArea txtID;

    /**
     * Método Intialize<br>
     *
     * Neste método criada uma lista com todas as categorias obtidas pelo método
     * listarCategorias. De seguida para cada elemento dessa lista de categorias
     * é criado um botão com o nome da categoria. Depois de selecionar uma
     * categoria é feito um select com o nome do mesmo (nomeCategoria), através
     * do método procurarCategoriaNome que retorna a categoria correspondente ao
     * nomeCategoria, esta categoria é descriminada por cada atributo e listada
     * no ecrã.<br>
     *
     * @param url url
     * @param rb rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            setVisibleFalse();
            adicionarEcraPessoalOperadorMenuOpcoes();

            List<categoria> cat = listarCategorias();
            for (categoria categorias : cat) {
                int id = categorias.getIdCategoria();
                Button botao = new Button(String.valueOf(id));
                botao.setId("botaoVbox");
                botao.setPrefHeight(30);

                botao.setPrefWidth(vBoxCategorias.getPrefWidth());
                botao.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent e) {
                        botao.setId("botaoVboxChangeCollour");
                        setVisibleTrue();

                        List<categoria> categorias = listarCategoriasPorId(id);
                        for (categoria categoria : categorias) {
                            txtID.setText(String.valueOf(categoria.getIdCategoria()));
                            nome.setText(categoria.getNomeCategoria());
                            estado.setText(categoria.getEstado());
                            descricao.setText(categoria.getDescricaoCategoria());
                            nivel.setText(String.valueOf(categoria.getNivelCategoria()));
                            idCat = categoria.getIdCategoria();
                        }
                    }
                });
                altura_vbox = altura_vbox + 30;
                vBoxCategorias.getChildren().addAll(botao);
                if (altura_vbox > 468) {
                    vBoxCategorias.setPrefHeight(altura_vbox);
                }
            }
        } catch (Exception a) {
            mensagemErroFatal();
        }
    }

    /**
     * Método para procurar uma categoria na BD e listar os seus atributos<br>
     * Método valida o id da categoria e verifica se esta existe e depois lista
     * os seus atributos caso a mesma exista.<br>
     * Caso algo corra mal ou a categoria não exista, o utilizador é informado.
     */
    @FXML
    private void pesquisarID(ActionEvent event) {
        try {
            Alert alertaDeNovo = new Alert(Alert.AlertType.NONE);

            if (!pesquisa.getText().equals("") && Pattern.compile("^[1-9]+[0-9]*$").matcher(pesquisa.getText()).find() == true
                    && isNewIdCategoria(Integer.parseInt(pesquisa.getText())) == false && pesquisa.getText().length() <= 10) {
                try {
                    setVisibleTrue();
                    List<categoria> categorias = listarCategoriasPorId(Integer.parseInt(pesquisa.getText()));
                    for (categoria categoria : categorias) {
                        idCat = Integer.parseInt(pesquisa.getText());
                        txtID.setText(String.valueOf(categoria.getIdCategoria()));
                        nome.setText(categoria.getNomeCategoria());
                        estado.setText(categoria.getEstado());
                        descricao.setText(categoria.getDescricaoCategoria());
                        nivel.setText(String.valueOf(categoria.getNivelCategoria()));
                    }
                    pesquisa.setText("");

                } catch (NumberFormatException ac) {
                    mensagemErroRecarregarEcra();
                } catch (Exception aq) {
                    mensagemErroRecarregarEcra();
                }

            } else {
                alertaDeNovo.setAlertType(Alert.AlertType.WARNING);
                alertaDeNovo.getButtonTypes().setAll(ButtonType.OK);
                alertaDeNovo.setTitle("ID de categoria de produto Inválido");
                alertaDeNovo.setHeaderText("Categoria não existe ou inseriu paramêtros incorretos");
                alertaDeNovo.setContentText("Insira um ID na forma de número inteiro");
                Optional<ButtonType> result = alertaDeNovo.showAndWait();
                if (result.get() == ButtonType.OK) {
                    pesquisa.setText("");
                }
            }
        } catch (NumberFormatException e) {
            Alert alertaDeNovo = new Alert(Alert.AlertType.NONE);
            alertaDeNovo.setAlertType(Alert.AlertType.WARNING);
            alertaDeNovo.getButtonTypes().setAll(ButtonType.OK);
            alertaDeNovo.setTitle("ID de categoria de produto Inválido");
            alertaDeNovo.setHeaderText("Categoria não existe ou inseriu paramêtros incorretos");
            alertaDeNovo.setContentText("Insira um ID na forma de número inteiro com 10 digitos (máx)");
            Optional<ButtonType> result = alertaDeNovo.showAndWait();
            if (result.get() == ButtonType.OK) {
                pesquisa.setText("");
            }
        }
    }

    /**
     * Método para tornar visiveis (e não editáveis) todos os atributos da
     * categoria selecionada.
     */
    private void setVisibleTrue() {
        
        lblID.setVisible(true);
        lbEstado.setVisible(true);
        lbNome.setVisible(true);
        lbDescricao.setVisible(true);
        lbNivel.setVisible(true);

        estado.setVisible(true);
        nome.setVisible(true);
        descricao.setVisible(true);
        nivel.setVisible(true);
        txtID.setVisible(true);

        nome.setEditable(false);
        descricao.setEditable(false);
        nivel.setEditable(false);
        estado.setEditable(false);
        txtID.setEditable(false);

        eliminarCategoria.setVisible(true);

    }

    /**
     * Método para não tornar visivel os atributos da categoria selecionado.
     */
    private void setVisibleFalse() {
        
        lblID.setVisible(false);
        lbNome.setVisible(false);
        lbDescricao.setVisible(false);
        lbEstado.setVisible(false);
        lbNivel.setVisible(false);

        estado.setVisible(false);
        nome.setVisible(false);
        descricao.setVisible(false);
        nivel.setVisible(false);
        txtID.setVisible(false);

        eliminarCategoria.setVisible(false);

    }

    /**
     * Método para eliminar categorias sem produtos associados.<br>
     * Método verifica se a categoria tem produtos associados e em caso positivo
     * informa o utilizador desse facto, não prossegue com a eliminação e
     * recarrega o ecrã.<br>
     * Caso a categoria não tenha produtos associados, o método chama o método
     * de eliminar categorias e depois chama outro método para verificar se
     * categoria foi eliminada com sucesso. Em ambos os casos, informa o
     * utilizador do verificado e recarrega o ecrã.<br>
     * Se algo correr mal, o utilizador é notificado e o ecrã recarregado.<br>
     *
     * @param event evento
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    @FXML
    private void eliminarCategoria(ActionEvent event) {
        try {
            Alert a2 = new Alert(Alert.AlertType.NONE);
            //notificação se quer mesmo eliminar a Categoria
            a.setAlertType(Alert.AlertType.CONFIRMATION);
            a.setTitle("Categoria");
            a.setHeaderText("Deseja mesmo eliminar esta Categoria da lista de Categorias?");
            a.setResizable(false);
            a.getButtonTypes().setAll(ButtonType.YES, ButtonType.NO);
            Optional<ButtonType> result = a.showAndWait();

            if (result.get() == ButtonType.YES) {
                //verifica se a Categoria tem produtos associados ou não em duas tabelas, se não, elimina e notifica
                if (hasProductsCategoria(idCat) == false) {
                    eliminarCategorias(idCat);
                    if (isNewIdCategoria(idCat) == true) {
                        mensagemSucessoEliminarCategoria(idCat);
                    } else {
                        mensagemInsucessoEliminarCategoria(idCat);
                    }
                } else {
                    mensagemExistenciaProdutosAssociadosCategoria(idCat);
                }
            }
        } catch (Exception correuMal) {
            mensagemErroRecarregarEcra();
        }

    }

    /**
     * método para fechar o ecrã de listar as categorias
     */
    private static void fecha() {
        eliminarCategorias.getStage().close();
    }

    /**
     * Método para aceder ao menu do administrador
     */
    @FXML
    void openMenu(ActionEvent event) {
        try {
            if (login.getNivelLogin() == 1) {
                menuOperador menuO = new menuOperador();
                fecha();
                menuO.start(new Stage());
            } else {
                menuAdministrador menuAdmin = new menuAdministrador();
                fecha();
                menuAdmin.start(new Stage());
            }
        } catch (Exception ex) {
            Logger.getLogger(eliminarCategoriaController.class.getName()).log(Level.SEVERE, null, ex);
            mensagemErroRecarregarEcra();
        }
    }

    /**
     * Método para encerrar a aplicação
     */
    @FXML
    void sair(ActionEvent event) {
        System.exit(0);
    }

    /**
     * Metodo para terminar sessao do utilizador<br>
     *
     * @param event evento
     */
    @FXML
    void terminarSessao(ActionEvent event) {
        try {
            login.setIdLogin(0);
            login.setNivelLogin(10000);
            login log = new login();
            fecha();
            log.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuAdministradorController.class.getName()).log(Level.SEVERE, null, ex);
            mensagemErroFatal();
        }
    }

    /**
     * Método para lançar uam mensagem de erro e recarregar o ecrã caso algo
     * inesperado ocorra com o algoritmo<br>
     * Método usado quando as falhas são graves mas não graves de mais que a
     * funcionalidade se encontre inutilizável.<br>
     * É a mensagem de erro de severidade mais leve neste ecrã
     */
    public static void mensagemErroRecarregarEcra() {
        Alert alertaNovo = new Alert(Alert.AlertType.NONE);
        alertaNovo.setAlertType(Alert.AlertType.ERROR);
        alertaNovo.getButtonTypes().setAll(ButtonType.OK);
        alertaNovo.setTitle("ERRO Lógica Aplicação");
        alertaNovo.setHeaderText("Os dados processados não são válidos!");
        alertaNovo.setContentText("Ocorreu um erro ao validar os dados selecionados ou estes podem estar inacessíveis. "
                + "\nTente novamente e caso não conseguir contacte o administrador.");
        Optional<ButtonType> resulta = alertaNovo.showAndWait();
        if (resulta.get() == ButtonType.OK) {
            recarregarEcra();
        }

    }

    /**
     * Método para recarregar o ecrã de inserir categorias
     */
    private static void recarregarEcra() {
        eliminarCategorias eliCat = new eliminarCategorias();
        fecha();
        try {
            eliCat.start(new Stage());
        } catch (Exception ex) {
            mensagemErroFatal();
        }
    }

    /**
     * Mensagem de erro para tentar fechar em segurança a aplicação no caso
     * desta encontrar um erro muito grave<br>
     * Se este erro for lançado é porque algo inesperado ocorreu (ex: falha de
     * acesso à base de dados, lançamento de alguns excepções em métodos
     * críticos ou falhas gerais e básicas de programação)<br>
     * É a mensagem de erro de severidade mais elevada deste ecrã
     */
    public static void mensagemErroFatal() {
        Alert a = new Alert(Alert.AlertType.NONE);
        a.setAlertType(Alert.AlertType.ERROR);
        a.setTitle("ERRO Fatal");
        a.setHeaderText("A aplicação comportou-se de forma inesperada!");
        a.setContentText("Encerrando a aplicação");
        a.getButtonTypes().setAll(ButtonType.OK);
        Optional<ButtonType> result3 = a.showAndWait();
        if (result3.get() == ButtonType.OK) {
            System.exit(0);
        }
    }

    /**
     * método para substituir o conteudo do menu de opções para colocar as
     * opções por ordem caso seja o operador a abrir o ecrã
     */
    private void adicionarEcraPessoalOperadorMenuOpcoes() {
        //substituir o conteudo do menu de opções para colocar as opções por ordem caso seja o operador a abrir o ecrã
        List<utilizador> operadores = procurarEmail(login.getIdLogin());
        for (utilizador operador : operadores) {
            if (operador.getNivel() == 1) {
                opcoes.getItems().clear();
                MenuItem ePOp = new MenuItem("Ecrã Pessoal");
                MenuItem terminarSessao = new MenuItem("Terminar Sessão");
                MenuItem sair = new MenuItem("Sair");
                opcoes.getItems().add(ePOp);
                opcoes.getItems().add(terminarSessao);
                opcoes.getItems().add(sair);
                ePOp.setOnAction((ActionEvent e) -> {
                    try {
                        ecraPessoalOperador ePO = new ecraPessoalOperador();
                        fecha();
                        ePO.start(new Stage());
                    } catch (Exception ex) {
                        Logger.getLogger(aprovarEntradaStocksController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
                terminarSessao.setOnAction((ActionEvent e) -> {
                    try {
                        login.setIdLogin(0);
                        login.setNivelLogin(10000);
                        login log = new login();
                        fecha();
                        log.start(new Stage());
                    } catch (Exception ex) {
                        Logger.getLogger(aprovarEntradaStocksController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
                sair.setOnAction((ActionEvent e) -> {
                    System.exit(0);
                });
            }
        }
    }

    /**
     * Método para notificar o utilizador do insucesso da eliminação da
     * categoria de produto.<br>
     * Método informa o utilizador do insucesso da operação.<br>
     *
     * @param idCategoria : id da categoria a eliminar
     */
    private static void mensagemInsucessoEliminarCategoria(int idCategoria) {
        Alert alerta = new Alert(Alert.AlertType.NONE);
        alerta.setAlertType(Alert.AlertType.ERROR);
        alerta.getButtonTypes().setAll(ButtonType.YES);
        alerta.setTitle("Eliminar Categoria");
        alerta.setHeaderText("Eliminar categoria sem sucesso!");
        alerta.setContentText("Não foi possível eliminar a categoria " + idCategoria + ".\n"
                + "Ocorreu algum problema ou a base de dados pode estar ocupada.\nTente Novamente.");
        alerta.setResizable(false);
        Optional<ButtonType> resultA = alerta.showAndWait();
        if (resultA.get() == ButtonType.YES) {
            recarregarEcra();
        }
    }

    /**
     * Método para notificar o utilizador do sucesso da eliminação da categoria
     * de produto.<br>
     * Método informa o utilizador do sucesso da operação. <br>
     *
     * @param idCategoria : id da categoria a eliminar
     */
    private static void mensagemSucessoEliminarCategoria(int idCategoria) {
        Alert alerta = new Alert(Alert.AlertType.NONE);
        alerta.setAlertType(Alert.AlertType.INFORMATION);
        alerta.getButtonTypes().setAll(ButtonType.YES);
        alerta.setTitle("Eliminar Categoria");
        alerta.setHeaderText("Eliminar categoria com sucesso!");
        alerta.setContentText("A a categoria " + idCategoria + " foi eliminada com sucesso!");
        alerta.setResizable(false);
        Optional<ButtonType> resultA = alerta.showAndWait();
        if (resultA.get() == ButtonType.YES) {
            recarregarEcra();
        }
    }

    /**
     * Método para notificar o utilizador da existência de produtos associados à
     * categoria que pretende eliminar.<br>
     * Método informa o utilizador que não pode eliminar a categoria por essa
     * estar associada a um produto. <br>
     * Método usado em conjugação com um método ou código de verificação da
     * existência de produtos associados a uma dada categoria.<br>
     *
     * @param idCategoria : id da categoria a eliminar
     */
    private static void mensagemExistenciaProdutosAssociadosCategoria(int idCategoria) {
        Alert alerta = new Alert(Alert.AlertType.NONE);
        alerta.setAlertType(Alert.AlertType.WARNING);
        alerta.setTitle("Categoria");
        alerta.setHeaderText("Categoria com produtos associados!");
        alerta.setContentText("A Categoria " + idCategoria + " tem produtos associados e NÃO pode ser eliminada!");
        alerta.getButtonTypes().setAll(ButtonType.OK);
        alerta.setResizable(false);
        Optional<ButtonType> resultA = alerta.showAndWait();
        if (resultA.get() == ButtonType.YES) {
            recarregarEcra();
        }
    }

}
