/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import static classes.administrador.procurarUtilizadorID;

import static classes.codigoPostal.procurarCodPostal;
import static classes.fornecedor.procurarFornecedorID;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Classe de testes da classe administrador.<br>
 * @author Ilídio Magalhães<br>
 */
public class administradorTest {

    private utilizador admin = new utilizador();

    /**
     * Construtor da classe administradorTest.<br>
     */
    public administradorTest() {
    }

    /**
     * Método para estabelecer os atributos do objeto admin.
     */
    @Before
    public void setUp() {
        admin.setIdUtilizador(1003);
        admin.setEmail("testAdmin@gmail.com");
        admin.setPassword("h4YH9+xAotASey1pPDWNhXdrkhbCZktENx2r2//GOEQ=");
        admin.setSalt("nl6iFFXR4DDNY3bWwGLB8ED13ZkIDy");
        admin.setNivel(2);
        admin.setEstado("ativo");

    }

    /**
     * Teste ao método listarUtilizadoresPorTipo da classe administrador.<br>
     * Método vai verificar se existem utilizadores dos tipos existentes na BD e
     * se não existem utilizadores dos tipos que não existem na BD.<br>
     * O teste é passado com sucesso caso o método consiga obter os utilizadores
     * do tipo pretendido e não consiga obter utilizadores do tipo não
     * pretendido.<br>
     * O teste é válido caso o método encontre utilizadores dos tipos 0 a 3 e
     * não encontre utilizadores do tipo 4.
     */
    @Test
    public void testListarUtilizadoresPorTipo() {

        for (int i = 0; i < 5; i++) {
            System.out.println("\nVerificando se existem utilizadores de vários tipos ");
            List<utilizador> adm = administrador.listarUtilizadoresPorTipo(i);
            int nivel = i;
            boolean result = !adm.isEmpty();
            boolean resultF = adm.isEmpty();
            switch (nivel) {
                case 0:
                    System.out.println("Existem " + adm.size() + " utilizadores do tipo fornecedor");
                    assertTrue(result);
                    break;
                case 1:
                    System.out.println("Existem " + adm.size() + " utilizadores do tipo operador");
                    assertTrue("Existem " + adm.size() + " utilizadores do tipo operador", result);
                    break;
                case 2:
                    System.out.println("Existem " + adm.size() + " utilizadores do tipo administrador");
                    assertTrue(result);
                    break;
                case 3:
                    System.out.println("Existem " + adm.size() + " utilizadores do tipo cliente");
                    assertTrue(result);
                    break;
                case 4:
                    System.out.println("Existem " + adm.size() + " utilizadores do tipo selecionado\n");
                    assertTrue(resultF);
                    break;
                default:
                    System.out.println("Impossível obter utilizadores!\b");
                    assertTrue(resultF);
                    break;
            }
        }
    }

    /**
     * Teste ao método procurarUtilizadorID da classe administrador.<br>
     * Método vai procurar pelo um utilizador com o ID 4 e informa o utilizador
     * de testes caso encontre o registo.<br>
     * O teste é válido caso os atributos do utilizador 4 sejam iguais ao
     * esperado.
     */
    @Test
    public void testProcurarUtilizadorID() {

        utilizador a = new utilizador();
        a.setIdUtilizador(4);
        a.setEmail("inativo@gmail.com");
        a.setPassword("h4YH9+xAotASey1pPDWNhXdrkhbCZktENx2r2//GOEQ=");
        a.setSalt("nl6iFFXR4DDNY3bWwGLB8ED13ZkIDy");
        a.setNivel(0);
        a.setEstado("inativo");

        System.out.println("\nProcura de utilizador com ID: " + a.getIdUtilizador());
        List<utilizador> inicio = new ArrayList<>();
        inicio.add(a);
        List<utilizador> fim = procurarUtilizadorID(a.getIdUtilizador());

        for (utilizador ut : fim) {
            assertEquals(a.getIdUtilizador(), ut.getIdUtilizador());
            assertEquals(a.getEmail(), ut.getEmail());
            assertEquals(a.getPassword(), ut.getPassword());
            assertEquals(a.getSalt(), ut.getSalt());
            assertEquals(a.getNivel(), ut.getNivel());
            assertEquals(a.getEstado(), ut.getEstado());
            System.out.println("O utilizador existe realmente!\n");
        }
    }

    /**
     * Teste ao método eliminarUtilizador da classe administrador.<br>
     * Teste programado para falhar: método tenta eliminar um utilizador que não
     * existe e com um tipo que não está em uso e é válido caso não consiga de
     * facto eliminar o utilizador.<br>
     * O teste é válido caso não seja possível eliminar o utilizador.<br>
     */
    @Test
    public void testEliminarUtilizador() {
        int id = 444;
        System.out.println("\nEliminar utilizador com o id " + id);
        int nivel = 5;
        boolean result = false;
        administrador.eliminarUtilizador(id, nivel);
        assertFalse(result);
        if (result) {
            System.out.println("Impossível eliminar utilizador!\nVerifique se este existe ou se o tipo está correto!\n");
        }
    }

    /**
     * Teste ao método ativarInativarUtilizador da classe administrador.<br>
     * Método tenta ativar (ou inativar se desejar) um utilizador - o utilizador
     * definido na classe de testes, depois verifica se tal aconteceu e informa
     * o utilizador do sucedido.<br>
     * O teste é válido caso o estado selecionado para modificação seja igual ao
     * estado do utilizador que se encontra na BD.
     */
    @Test
    public void testAtivarInativarUtilizador() {
        int id = admin.getIdUtilizador();
        System.out.println("\nAlterar estado do utilizador com ID " + id);
        int nivel = admin.getNivel();
        String estado = "ativo";
        boolean result = false;
        administrador.ativarInativarUtilizador(estado, id, nivel);
        List<utilizador> util = procurarUtilizadorID(id);
        for (utilizador ut : util) {
            if (ut.getIdUtilizador() == admin.getIdUtilizador() && ut.getNivel() == admin.getNivel()
                    && ut.getEstado().equals(estado)) {
                result = true;
            }
        }
        if (result) {
            System.out.println("Estado do utilizador " + id + " alterado com sucesso!\n");
            assertTrue(result);
        }

    }

    /**
     * Teste ao método alterarPassUtilizador da classe administrador.<br>
     * Método tenta alterar a password e o salt do utilizador e verificar se
     * esta alteração ocorreu de facto.<br>
     * Depois do teste, retorna tudo ao estado inicial.<br>
     * Teste é válido caso se prove a alteração da password e do salt.
     */
    @Test
    public void testAlterarPassUtilizador() {
        int id = 1003;
        System.out.println("\nAlterar password e salt do utilizador " + id);
        String password = "1234";
        String salt = "teste";
        String email = "testAdmin@gmail.com";
        int nivel = 2;
        String estado = "ativo";
        boolean result = false;
        administrador.alterarPassUtilizador(password, salt, id, email, nivel, estado);
        List<utilizador> util = procurarUtilizadorID(id);
        for (utilizador ut : util) {
            if (ut.getIdUtilizador() == id
                    && ut.getPassword().equals(password)
                    && ut.getSalt().equals(password)) {
                result = true;
            }
        }
        if (result) {
            System.out.println("Password e salt do utilizador " + id + " alteradas com sucesso!\n");
            assertTrue(result);
        }
        String passBackup = "h4YH9+xAotASey1pPDWNhXdrkhbCZktENx2r2//GOEQ=";
        String saltBackup = "nl6iFFXR4DDNY3bWwGLB8ED13ZkIDy";
        administrador.alterarPassUtilizador(passBackup, saltBackup, id, email, nivel, estado);

    }

    /**
     * Teste ao método alterarPassEstadoUtilizador da classe administrador.<br>
     * Método tenta alterar a password, o salt e o estado do utilizador e
     * verificar se esta alteração ocorreu de facto.<br>
     * Depois do teste, retorna tudo ao estado inicial.<br>
     * Teste é válido caso se prove a alteração da password, do salt e do
     * estado.
     */
    @Test
    public void testAlterarPassEstadoUtilizador() {
        int id = 1003;
        System.out.println("\nAlterar password, salt e estado do utilizador " + id);
        String password = "1234";
        String salt = "teste";
        String email = "testAdmin@gmail.com";
        int nivel = 2;
        String estado = "inativo";
        boolean result = false;
        administrador.alterarPassEstadoUtilizador(password, estado, salt, id, email, nivel);
        List<utilizador> util = procurarUtilizadorID(id);
        for (utilizador ut : util) {
            if (ut.getIdUtilizador() == id
                    && ut.getPassword().equals(password)
                    && ut.getSalt().equals(salt)
                    && ut.getEstado().equals(estado)) {
                result = true;
            }
        }
        if (result) {
            System.out.println("Password, salt e estado do utilizador " + id + " alterados com sucesso!\n");
            assertTrue(result);
        }
        String passBackup = "h4YH9+xAotASey1pPDWNhXdrkhbCZktENx2r2//GOEQ=";
        String saltBackup = "nl6iFFXR4DDNY3bWwGLB8ED13ZkIDy";
        String estadoBackup = "ativo";
        administrador.alterarPassEstadoUtilizador(passBackup, saltBackup, estadoBackup, id, email, nivel);
    }

    /**
     * Teste ao método testAlterarEstadoFornecedor da classe administrador.<br>
     * Método tenta alterar o estado de um dado fornecedor e verificar o
     * resultado da alteração.<br>
     * O teste é válido caso esta se prove ter existido.<br>
     * No fim do método, o estado do fornecedor de testes é restaurado ao valor
     * inicial.
     */
    @Test
    public void testAlterarEstadoFornecedor() {
        int id = 1004;
        System.out.println("Alterar estado do fornecedor com id " + id);
        String estadoInicial = "ativo";
        String estadoFinal = "inativo";
        boolean result = false;
        administrador.alterarEstadoFornecedor(estadoInicial, id);
        List<utilizador> util = procurarUtilizadorID(id);
        for (utilizador ut : util) {
            if (ut.getIdUtilizador() == id
                    && ut.getEstado().equals(estadoFinal)) {
                result = true;
            }
        }
        if (result) {
            System.out.println("Estado do fornecedor " + id + " alterado com sucesso!\n");
            assertTrue(result);
        }
        administrador.alterarEstadoFornecedor(estadoFinal, id);
    }

    /**
     * Teste ao método alterarEmail da classe administrador.<br>
     * Método tenta alterar o email de um utilizador de testes e verificar o
     * resultado da alteração.<br>
     * O teste é válido caso esta se prove ter existido.<br>
     * No fim do método, o email do utilizador de testes é restaurado ao valor
     * inicial.
     */
    @Test
    public void testAlterarEmail() {

        String emailInicial = "fornecedor@gmail.com";
        String emailFinal = "testeFornecedor@gmail.com";
        int id = 1004;
        int nivel = 0;
        System.out.println("\nAlterar email do utilizador " + id);
        boolean result = false;
        administrador.alterarEmail(emailInicial, id, nivel);
        List<utilizador> util = procurarUtilizadorID(id);
        for (utilizador ut : util) {
            if (ut.getIdUtilizador() == id
                    && ut.getNivel() == nivel
                    && ut.getEmail().equals(emailFinal)) {
                result = true;
            }
        }
        if (result) {
            System.out.println("Email do utilizador " + id + " alterado com sucesso!\n");
            assertTrue(result);
        }
        administrador.alterarEmail(emailFinal, id, nivel);
    }

    /**
     * Teste ao método alterarPaisMoradaCP da classe administrador.<br>
     * Método procura alterar a morada e o código postal do fornecedor de testes
     * com id 999999999 e nif 199999999 e depois verifica se tal teve
     * sucesso.<br>
     * O teste é válido caso as alterações se verifiquem.<br>
     * No final, retorna os valores alterados ao estado inicial.
     */
    @Test
    public void testAlterarPaisMoradaCP() {
        fornecedor testeF = new fornecedor();
        testeF.setIdFornecedor("999999999");
        testeF.setIdUtilizador(1004);
        testeF.setNome("FornecedorTestes");
        testeF.setContribuinte(199999999);
        testeF.setMorada("Avenida do Admin");
        testeF.setCodPostal("9999-999");
        testeF.setPais("Portugal");
        testeF.setEstado("inativo");
        System.out.println("\nAlterar morada do fornecedor com nº contribuinte " + testeF.getContribuinte());
        String pais = "testland";
        String morada = "rua dos testes";
        String cp = "CP teste";
        boolean result = false;
        administrador.alterarPaisMoradaCP(pais, morada, cp, testeF.getIdFornecedor(), testeF.getNome());
        List<fornecedor> forn = procurarFornecedorID(testeF.getIdFornecedor());
        for (fornecedor fo : forn) {
            if (fo.getIdFornecedor().equals(testeF.getIdFornecedor())
                    && fo.getContribuinte() == testeF.getContribuinte()
                    && fo.getCodPostal().equals(testeF.getCodPostal())
                    && fo.getMorada().equals(testeF.getMorada())) {
                result = true;
            }
        }
        if (result) {
            System.out.println("Morada e Código Postal do Fornecedor com o NIF " + testeF.getContribuinte() + " alterados com sucesso!\n");
            assertTrue(result);
        }
        administrador.alterarPaisMoradaCP(testeF.getPais(), testeF.getMorada(), testeF.getCodPostal(), testeF.getIdFornecedor(), testeF.getNome());

    }

    /**
     * Teste ao método alterarCidade da classe administrador.<br>
     * Método procura alterar a cidade de um código postal e depois verificar se
     * a alteração teve sucesso.<br>
     * O teste é válido caso a alteração da cidade se verifique.<br>
     * No fim, a cidade inicial é restaurada.
     */
    @Test
    public void testAlterarCidade() {
        String cp = "9999-999";
        String cidadeInicial = "teste";
        String cidadeFinal = "Rio";
        System.out.println("\nAlterar cidade associada ao CP");
        administrador.alterarCidade(cidadeInicial, cp);
        boolean result = false;

        List<codigoPostal> codigosPostais = procurarCodPostal(cp);

        for (codigoPostal cpT : codigosPostais) {
            if (cpT.getCodPostal().equals(cp)
                    && cpT.getCidade().equals(cidadeFinal)) {
                result = true;
            }
        }
        if (result) {
            System.out.println("A Cidade do código Postal " + cp + " foi alterada com sucesso!\n");
            assertTrue(result);
        }
        assertFalse(result);

        administrador.alterarCidade(cidadeFinal, cp);
    }

    /**
     * Teste ao método procurarUtilizadorIDEmail da classe administrador.<br>
     * Método tenta encontrar um utilizador pelo seu email e id.<br>
     * O teste é válido caso o utilizador seja encontrado.
     */
    @Test
    public void testProcurarUtilizadorIDEmail() {
        System.out.println("\nPesquisar Utilizador pelo email seu email\n");
        int id = 5;
        String email = "pendente@gmail.com";
        System.out.println("\nTeste ao email " + email + " do utilizador " + id);

        List<utilizador> adm = administrador.procurarUtilizadorIDEmail(id, email);
        boolean result = !adm.isEmpty();
        if (result) {
            System.out.println("Utilizador encontrado!\n");
        } else {
            System.out.println("Utilizador encontrado!");
        }
        assertTrue(result);
    }

}
