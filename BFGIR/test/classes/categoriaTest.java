/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Classe de testes da classe categoria.
 *
 * @author ruben
 */
public class categoriaTest {

    private static categoria testes = new categoria();

    /**
     * Construtor da classe categoriaTest.
     */
    public categoriaTest() {
    }

    /**
     * Método para estabelecer os atributos do objeto testes.
     */
    @BeforeClass
    public static void setUpClass() {
        testes.setIdCategoria(99999);
        testes.setNomeCategoria("catJunit");
        testes.setDescricaoCategoria("Testes");
        testes.setNivelCategoria(1);
        testes.setEstado("inativo");

    }

    /**
     * Teste ao método testGetIdCategoria da classe categoria.<br>
     * Método tenta buscar o id do objeto previamente criado na classe.<br>
     * O teste é válido caso o valor obtido seja igual ao valor esperado.
     *
     */
    @Test
    public void testGetIdCategoria() {
        System.out.println("\nTeste ao método getIdCategoria");
        int expResult = 99999;
        int result = testes.getIdCategoria();
        if (expResult == result) {
            System.out.println("Teste ao método getIdCategoria passado com um sucesso!\n");
        }
        assertEquals(expResult, result);
    }

    /**
     * Teste ao método setIdCategoria da classe categoria.<br>
     * Método modifica o id do objeto testes e depois verifica se o consegiu
     * alterar.<br>
     * O teste é válido caso tal se verifique.<br>
     * No fim, restaura o atributo ao estado inicial.
     */
    @Test
    public void testSetIdCategoria() {
        System.out.println("\nTeste ao método setIdCategoria");
        int idCategoria = 1;
        int idBackup = testes.getIdCategoria();
        testes.setIdCategoria(idCategoria);
        boolean result = false;
        if (idCategoria == testes.getIdCategoria()) {
            result = true;
            System.out.println("Teste ao método setIdCategoria passado com um sucesso!\n");
        }
        assertTrue(result);
        testes.setIdCategoria(idBackup);
    }

    /**
     * Teste ao método getDescricaoCategoria da classe categoria.<br>
     * Método tenta buscar a descrição do objeto previamente criado na
     * classe.<br>
     * O teste é válido caso o valor obtido seja igual ao valor esperado.
     */
    @Test
    public void testGetDescricaoCategoria() {
        System.out.println("\nTeste ao método getDescricaoCategoria");
        String expResult = "Testes";
        String result = testes.getDescricaoCategoria();
        if (expResult.equals(result)) {
            System.out.println("Teste ao método getDescricaoCategoria passado com um sucesso!\n");
        }
        assertEquals(expResult, result);
    }

    /**
     * Teste ao método setDescricaoCategoria da classe categoria.<br>
     * Método modifica a descrição do objeto testes e depois verifica se o
     * consegiu alterar.<br>
     * O teste é válido caso tal se verifique.<br>
     * No fim, restaura o atributo ao estado inicial.
     */
    @Test
    public void testSetDescricaoCategoria() {
        System.out.println("\nTeste ao método setDescricaoCategoria");
        String dI = "catD";
        String dF = testes.getDescricaoCategoria();
        testes.setDescricaoCategoria(dI);
        boolean result = false;
        if (dI.equals(testes.getDescricaoCategoria())) {
            result = true;
            System.out.println("Teste ao método setDescricaoCategoria passado com um sucesso!\n");
        }
        assertTrue(result);
        testes.setDescricaoCategoria(dF);

    }

    /**
     * Teste ao método getNivelCategoria da classe categoria.<br>
     * Método tenta buscar o nível do objeto previamente criado na classe.<br>
     * O teste é válido caso o valor obtido seja igual ao valor esperado.
     */
    @Test
    public void testGetNivelCategoria() {
        System.out.println("\nTeste ao método getNivelCategoria");
        int expResult = 1;
        int result = testes.getNivelCategoria();
        if (expResult == result) {
            System.out.println("Teste ao método getNivelCategoria passado com um sucesso!\n");
        }
        assertEquals(expResult, result);
    }

    /**
     * Teste ao método setNivelCategoria da classe categoria.<br>
     * Método modifica o nivel do objeto testes e depois verifica se o consegiu
     * alterar.<br>
     * O teste é válido caso tal se verifique.<br>
     * No fim, restaura o atributo ao estado inicial.
     */
    @Test
    public void testSetNivelCategoria() {
        System.out.println("\nTeste ao método setNivelCategoria");
        int nivelIni = 5;
        int nivelF = testes.getNivelCategoria();
        testes.setNivelCategoria(nivelIni);
        boolean result = false;
        if (nivelIni == testes.getNivelCategoria()) {
            result = true;
            System.out.println("Teste ao método setNivelCategoria passado com um sucesso!\n");
        }
        assertTrue(result);
        testes.setNivelCategoria(nivelF);

    }

    /**
     * Teste ao método getEstado da classe categoria.<br>
     * Método tenta buscar o estado do objeto previamente criado na classe.<br>
     * O teste é válido caso o valor obtido seja igual ao valor esperado.
     */
    @Test
    public void testGetEstado() {
        System.out.println("\nTeste ao método getEstado");
        String expResult = "inativo";
        String result = testes.getEstado();
        if (expResult.equals(result)) {
            System.out.println("Teste ao método getEstado passado com um sucesso!\n");
        }
        assertEquals(expResult, result);
    }

    /**
     * Teste ao método setEstado da classe categoria.<br>
     * Método modifica o estado do objeto testes e depois verifica se o consegiu
     * alterar.<br>
     * O teste é válido caso tal se verifique.<br>
     * No fim, restaura o atributo ao estado inicial.
     */
    @Test
    public void testSetEstado() {
        System.out.println("\nTeste ao método setEstado");
        String sI = "pendente";
        String sF = testes.getEstado();
        testes.setEstado(sI);
        boolean result = false;
        if (sI.equals(testes.getEstado())) {
            result = true;
            System.out.println("Teste ao método setEstado passado com um sucesso!\n");
        }
        assertTrue(result);
        testes.setEstado(sF);
    }

    /**
     * Teste ao método getNomeCategoria da classe categoria.<br>
     * Método tenta buscar o nome do objeto previamente criado na classe.<br>
     * O teste é válido caso o valor obtido seja igual ao valor esperado.
     */
    @Test
    public void testGetNomeCategoria() {
        System.out.println("\nTeste ao método getNomeCategoria");
        String expResult = "catJunit";
        String result = testes.getNomeCategoria();
        if (expResult.equals(result)) {
            System.out.println("Teste ao método getNomeCategoria passado com um sucesso!\n");
        }
        assertEquals(expResult, result);
    }

    /**
     * Teste ao método setNomeCategoria da classe categoria.<br>
     * Método modifica o nome do objeto testes e depois verifica se o consegiu
     * alterar.<br>
     * O teste é válido caso tal se verifique.<br>
     * No fim, restaura o atributo ao estado inicial.
     */
    @Test
    public void testSetNomeCategoria() {
        System.out.println("\nTeste ao método setNomeCategoria");
        String nI = "gUnit";
        String nF = testes.getNomeCategoria();
        testes.setNomeCategoria(nI);
        boolean result = false;
        if (nI.equals(testes.getNomeCategoria())) {
            result = true;
            System.out.println("Teste ao método setNomeCategoria passado com um sucesso!\n");
        }
        assertTrue(result);
        testes.setEstado(nF);
    }

}
