/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.security.SecureRandom;
import java.util.Optional;
import java.util.Random;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

/**
 * Classe PasswordHelper com métodos de para a gerar novas passwords.
 */
public class PasswordHelper {

    /**
     * Método para gerar uma nova password para um novo fornecedor ou operador.<br>
     *
     * @param length recebe o tamanho da password a gerar.
     * @return retorna password já gerada.
     */
    public static String generatePassword(int length) {

        //minimum length of 6
        if (length < 4) {
            length = 6;
        }

        final char[] lowercase = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        final char[] uppercase = "ABCDEFGJKLMNPRSTUVWXYZ".toCharArray();
        final char[] numbers = "0123456789".toCharArray();
        final char[] symbols = "^$?!@#%&".toCharArray();
        final char[] allAllowed = "abcdefghijklmnopqrstuvwxyzABCDEFGJKLMNPRSTUVWXYZ0123456789^$?!@#%&".toCharArray();

        //Use cryptographically secure random number generator
        Random random = new SecureRandom();

        StringBuilder password = new StringBuilder();

        for (int i = 0; i < length - 4; i++) {
            password.append(allAllowed[random.nextInt(allAllowed.length)]);
        }

        //Ensure password policy is met by inserting required random chars in random positions
        password.insert(random.nextInt(password.length()), lowercase[random.nextInt(lowercase.length)]);
        password.insert(random.nextInt(password.length()), uppercase[random.nextInt(uppercase.length)]);
        password.insert(random.nextInt(password.length()), numbers[random.nextInt(numbers.length)]);
        password.insert(random.nextInt(password.length()), symbols[random.nextInt(symbols.length)]);
        return password.toString();
    }

    /**
     * Método aplicável a todos os utilizadores para validar uma nova password
     * por tamanho, tipo de careteres e por semelhante com a anterior.<br>
     *
     * @param novaPass nova password inserida pelo utilizador.
     * @param oldPass password atual do utilizador cifrada.
     * @param oldSalt salt adicionada à password atual antes da aplicação da
     * função de hash.
     * @return retorna verdadeiro caso a nova password seja válida E realmente.
     * nova e retorna falso em caso contrário.
     */
    public static boolean validarNovaPassInseridaUtilizador(String novaPass, String oldPass, String oldSalt) {
        char[] novaPassArray = novaPass.toCharArray();
        char[] alfabeto = "abcdefghijklmnopqrstuvwxyzABCDEFGJKLMNPRSTUVWXYZ0123456789^$?!@#%&".toCharArray();
        int tamanhoMinNovaPass = 4; //por questões de retro-compatbilidade apenas
        int tamanhoMaxNovaPass = 10;
        int charsOK = 0;
        boolean novaPassOK = false;
        Alert a = new Alert(Alert.AlertType.NONE);

        //cifrar a nova password usando o salt da pasword antiga para obter uma password comparável com esta última
        String testeNovaPassIgualOldPass = passwordUtils.generateSecurePassword(novaPass, oldSalt);

        if (novaPass.equals("") || novaPass == null) {
            a.setAlertType(Alert.AlertType.INFORMATION);
            a.setTitle("Ecrã Pessoal Utilizador");
            a.setHeaderText("Operação inválida!");
            a.setContentText("Não pode remover a sua password! Insira uma nova caso pretenda susbtituir a atual.");
            a.getButtonTypes().setAll(ButtonType.OK);
            Optional<ButtonType> result2 = a.showAndWait();
            novaPassOK = false;
            return novaPassOK;
        } else if (testeNovaPassIgualOldPass.equals(oldPass)) {
            a.setAlertType(Alert.AlertType.INFORMATION);
            a.setTitle("Ecrã Pessoal Utilizador");
            a.setHeaderText("Não efectuou nenhuma alteração à sua password!");
            a.setContentText("Insira uma nova password caso pretenda alterar a atual.");
            a.getButtonTypes().setAll(ButtonType.OK);
            Optional<ButtonType> result2 = a.showAndWait();
            novaPassOK = false;
            return novaPassOK;
        } else if (novaPassArray.length < tamanhoMinNovaPass || novaPassArray.length > tamanhoMaxNovaPass) {
            a.setAlertType(Alert.AlertType.INFORMATION);
            a.setTitle("Ecrã Pessoal Utilizador");
            a.setHeaderText("Nova password com tamanho inválido!");
            a.setContentText("Insira uma password contendo 4-10 caracteres sem espaços entre eles.");
            a.getButtonTypes().setAll(ButtonType.OK);
            Optional<ButtonType> result2 = a.showAndWait();
            novaPassOK = false;
            return novaPassOK;
        } else {
            for (int i = 0; i < novaPassArray.length; i++) {
                for (int j = 0; j < alfabeto.length; j++) {
                    if (novaPassArray[i] == alfabeto[j]) {
                        charsOK++;
                    }
                }
            }
            if (charsOK == novaPass.length()) {
                novaPassOK = true;
                return novaPassOK;
            } else {
                a.setAlertType(Alert.AlertType.INFORMATION);
                a.setTitle("Ecrã Pessoal Utilizador");
                a.setHeaderText("Password contém caracteres inválidos!");
                a.setContentText("Só pode escolher caracteres do tipo 'AaZz09^$?!@#%&' e sem espaços entre eles.");
                a.getButtonTypes().setAll(ButtonType.OK);
                Optional<ButtonType> result2 = a.showAndWait();
                novaPassOK = false;
                return novaPassOK;
            }
        }
    }

}
