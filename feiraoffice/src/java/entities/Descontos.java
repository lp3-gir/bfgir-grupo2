/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ruben
 */
@Entity
@Table(name = "descontos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Descontos.findAll", query = "SELECT d FROM Descontos d")
    , @NamedQuery(name = "Descontos.findByIdDesconto", query = "SELECT d FROM Descontos d WHERE d.idDesconto = :idDesconto")
    , @NamedQuery(name = "Descontos.findByQuantDesconto", query = "SELECT d FROM Descontos d WHERE d.quantDesconto = :quantDesconto")})
public class Descontos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idDesconto")
    private Integer idDesconto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "quantDesconto")
    private double quantDesconto;

    /**
     *
     */
    public Descontos() {
    }

    /**
     *
     * @param idDesconto recebe um id de desconto
     */
    public Descontos(Integer idDesconto) {
        this.idDesconto = idDesconto;
    }

    /**
     *
     * @param idDesconto recebe um id de desconto
     * @param quantDesconto recebe uma quantidade de desconto
     */
    public Descontos(Integer idDesconto, double quantDesconto) {
        this.idDesconto = idDesconto;
        this.quantDesconto = quantDesconto;
    }

    /**
     *
     * @return idDesconto
     */
    public Integer getIdDesconto() {
        return idDesconto;
    }

    /**
     *
     * @param idDesconto
     */
    public void setIdDesconto(Integer idDesconto) {
        this.idDesconto = idDesconto;
    }

    /**
     *
     * @return quantDesconto
     */
    public double getQuantDesconto() {
        return quantDesconto;
    }

    /**
     *
     * @param quantDesconto
     */
    public void setQuantDesconto(double quantDesconto) {
        this.quantDesconto = quantDesconto;
    }
}
