/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ruben
 */
@Entity
@Table(name = "encomendas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Encomendas.findAll", query = "SELECT e FROM Encomendas e")
    , @NamedQuery(name = "Encomendas.findByIdEncomenda", query = "SELECT e FROM Encomendas e WHERE e.idEncomenda = :idEncomenda")
    , @NamedQuery(name = "Encomendas.findByIdCliente", query = "SELECT e FROM Encomendas e WHERE e.idCliente = :idCliente")
    , @NamedQuery(name = "Encomendas.findByDataEncomenda", query = "SELECT e FROM Encomendas e WHERE e.dataEncomenda = :dataEncomenda")
    , @NamedQuery(name = "Encomendas.findByMoradaEntrega", query = "SELECT e FROM Encomendas e WHERE e.moradaEntrega = :moradaEntrega")
    , @NamedQuery(name = "Encomendas.findByCodPostalEntrega", query = "SELECT e FROM Encomendas e WHERE e.codPostalEntrega = :codPostalEntrega")
    , @NamedQuery(name = "Encomendas.findByPaisEntrega", query = "SELECT e FROM Encomendas e WHERE e.paisEntrega = :paisEntrega")
    , @NamedQuery(name = "Encomendas.findByMoradaFaturacao", query = "SELECT e FROM Encomendas e WHERE e.moradaFaturacao = :moradaFaturacao")
    , @NamedQuery(name = "Encomendas.findByCodPostalFaturacao", query = "SELECT e FROM Encomendas e WHERE e.codPostalFaturacao = :codPostalFaturacao")
    , @NamedQuery(name = "Encomendas.findByPaisFaturacao", query = "SELECT e FROM Encomendas e WHERE e.paisFaturacao = :paisFaturacao")
    , @NamedQuery(name = "Encomendas.findByPrecoTotalSIVA", query = "SELECT e FROM Encomendas e WHERE e.precoTotalSIVA = :precoTotalSIVA")
    , @NamedQuery(name = "Encomendas.findByIva", query = "SELECT e FROM Encomendas e WHERE e.iva = :iva")
    , @NamedQuery(name = "Encomendas.findByTotalIVA", query = "SELECT e FROM Encomendas e WHERE e.totalIVA = :totalIVA")
    , @NamedQuery(name = "Encomendas.findByPrecoTotalCIVA", query = "SELECT e FROM Encomendas e WHERE e.precoTotalCIVA = :precoTotalCIVA")
    , @NamedQuery(name = "Encomendas.findByEstado", query = "SELECT e FROM Encomendas e WHERE e.estado = :estado")})
public class Encomendas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idEncomenda", nullable = false, unique = true)
    private Integer idEncomenda;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idCliente")
    private int idCliente;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "dataEncomenda")
    private String dataEncomenda;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "moradaEntrega")
    private String moradaEntrega;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "codPostalEntrega")
    private String codPostalEntrega;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "paisEntrega")
    private String paisEntrega;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "moradaFaturacao")
    private String moradaFaturacao;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "codPostalFaturacao")
    private String codPostalFaturacao;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "paisFaturacao")
    private String paisFaturacao;
    @Basic(optional = false)
    @NotNull
    @Column(name = "precoTotalSIVA")
    private double precoTotalSIVA;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IVA")
    private int iva;
    @Basic(optional = false)
    @NotNull
    @Column(name = "totalIVA")
    private double totalIVA;
    @Basic(optional = false)
    @NotNull
    @Column(name = "precoTotalCIVA")
    private double precoTotalCIVA;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "estado")
    private String estado;

    /**
     *
     */
    public Encomendas() {
    }

    /**
     *
     * @param idEncomenda recebe o id de uma encomenda
     */
    public Encomendas(Integer idEncomenda) {
        this.idEncomenda = idEncomenda;
    }

    /**
     *
     * @param idEncomenda recebe o id de uma encomenda
     * @param idCliente revcebe o id de um cliente
     * @param dataEncomenda recebe um data de encomenda
     * @param moradaEntrega recebe uma morada de entrega
     * @param codPostalEntrega recebe um código postal de entrega
     * @param paisEntrega recebe um pais de entrega
     * @param moradaFaturacao recebe uma morada de faturação
     * @param codPostalFaturacao recebe um código postal de faturação
     * @param paisFaturacao recebe um pais de faturação
     * @param precoTotalSIVA recebe um preço total sem IVA
     * @param iva recebe uma taxa de IVA
     * @param totalIVA recebe um total de IVA
     * @param precoTotalCIVA recebe um preço total com IVA
     * @param estado recebe um estado da encomenda
     */
    public Encomendas(Integer idEncomenda, int idCliente, String dataEncomenda, String moradaEntrega, String codPostalEntrega, String paisEntrega, String moradaFaturacao, String codPostalFaturacao, String paisFaturacao, double precoTotalSIVA, int iva, double totalIVA, double precoTotalCIVA, String estado) {
        this.idEncomenda = idEncomenda;
        this.idCliente = idCliente;
        this.dataEncomenda = dataEncomenda;
        this.moradaEntrega = moradaEntrega;
        this.codPostalEntrega = codPostalEntrega;
        this.paisEntrega = paisEntrega;
        this.moradaFaturacao = moradaFaturacao;
        this.codPostalFaturacao = codPostalFaturacao;
        this.paisFaturacao = paisFaturacao;
        this.precoTotalSIVA = precoTotalSIVA;
        this.iva = iva;
        this.totalIVA = totalIVA;
        this.precoTotalCIVA = precoTotalCIVA;
        this.estado = estado;
    }

    /**
     *
     * @return idEncomenda
     */
    public Integer getIdEncomenda() {
        return idEncomenda;
    }

    /**
     *
     * @param idEncomenda
     */
    public void setIdEncomenda(Integer idEncomenda) {
        this.idEncomenda = idEncomenda;
    }

    /**
     *
     * @return idCliente
     */
    public int getIdCliente() {
        return idCliente;
    }

    /**
     *
     * @param idCliente
     */
    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    /**
     *
     * @return dataEncomenda
     */
    public String getDataEncomenda() {
        return dataEncomenda;
    }

    /**
     *
     * @param dataEncomenda
     */
    public void setDataEncomenda(String dataEncomenda) {
        this.dataEncomenda = dataEncomenda;
    }

    /**
     *
     * @return moradaEntrega
     */
    public String getMoradaEntrega() {
        return moradaEntrega;
    }

    /**
     *
     * @param moradaEntrega
     */
    public void setMoradaEntrega(String moradaEntrega) {
        this.moradaEntrega = moradaEntrega;
    }

    /**
     *
     * @return codPostalEntrega
     */
    public String getCodPostalEntrega() {
        return codPostalEntrega;
    }

    /**
     *
     * @param codPostalEntrega
     */
    public void setCodPostalEntrega(String codPostalEntrega) {
        this.codPostalEntrega = codPostalEntrega;
    }

    /**
     *
     * @return paisEntrega
     */
    public String getPaisEntrega() {
        return paisEntrega;
    }

    /**
     *
     * @param paisEntrega
     */
    public void setPaisEntrega(String paisEntrega) {
        this.paisEntrega = paisEntrega;
    }

    /**
     *
     * @return moradaFaturacao
     */
    public String getMoradaFaturacao() {
        return moradaFaturacao;
    }

    /**
     *
     * @param moradaFaturacao
     */
    public void setMoradaFaturacao(String moradaFaturacao) {
        this.moradaFaturacao = moradaFaturacao;
    }

    /**
     *
     * @return codPostalFaturacao
     */
    public String getCodPostalFaturacao() {
        return codPostalFaturacao;
    }

    /**
     *
     * @param codPostalFaturacao
     */
    public void setCodPostalFaturacao(String codPostalFaturacao) {
        this.codPostalFaturacao = codPostalFaturacao;
    }

    /**
     *
     * @return paisFaturacao
     */
    public String getPaisFaturacao() {
        return paisFaturacao;
    }

    /**
     *
     * @param paisFaturacao
     */
    public void setPaisFaturacao(String paisFaturacao) {
        this.paisFaturacao = paisFaturacao;
    }

    /**
     *
     * @return precoTotalSIVA
     */
    public double getPrecoTotalSIVA() {
        return precoTotalSIVA;
    }

    /**
     *
     * @param precoTotalSIVA
     */
    public void setPrecoTotalSIVA(double precoTotalSIVA) {
        this.precoTotalSIVA = precoTotalSIVA;
    }

    /**
     *
     * @return iva
     */
    public int getIva() {
        return iva;
    }

    /**
     *
     * @param iva
     */
    public void setIva(int iva) {
        this.iva = iva;
    }

    /**
     *
     * @return totalIVA
     */
    public double getTotalIVA() {
        return totalIVA;
    }

    /**
     *
     * @param totalIVA
     */
    public void setTotalIVA(double totalIVA) {
        this.totalIVA = totalIVA;
    }

    /**
     *
     * @return precoTotalCIVA
     */
    public double getPrecoTotalCIVA() {
        return precoTotalCIVA;
    }

    /**
     *
     * @param precoTotalCIVA
     */
    public void setPrecoTotalCIVA(double precoTotalCIVA) {
        this.precoTotalCIVA = precoTotalCIVA;
    }

    /**
     *
     * @return estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     *
     * @param estado
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }
}
