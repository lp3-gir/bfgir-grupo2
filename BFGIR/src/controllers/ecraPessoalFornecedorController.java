/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import bfgir.ecraPessoalFornecedor;
import bfgir.login;
import bfgir.menuFornecedor;
import static classes.administrador.alterarEmail;
import static classes.administrador.alterarPassUtilizador;
import static classes.administrador.procurarUtilizadorID;
import static classes.administrador.procurarUtilizadorIDEmail;
import classes.codigoPostal;
import static classes.codigoPostal.inserirCodPostal;
import static classes.codigoPostal.procurarCodPostal;
import static classes.email.enviarEmail;
import static classes.email.enviarEmailConfirmacao;
import classes.fornecedor;
import static classes.fornecedor.alterarCodPosFornecedor;
import static classes.fornecedor.alterarMoradaFornecedor;
import static classes.fornecedor.alterarNomeFornecedor;
import static classes.fornecedor.alterarPaisFornecedor;
import static classes.fornecedor.listarFornecedor;
import static classes.fornecedor.procurarFornecedorID;
import classes.passwordUtils;
import classes.utilizador;
import static classes.utilizador.procurarEmail;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * Classe controladora do ecrã ecraPessoalFornecedor.<br>
 *
 */
public class ecraPessoalFornecedorController implements Initializable {

    //dados para carregar o fornecedor e realizar validações e comparações
    utilizador utilizadorBase = new utilizador();
    fornecedor fornecedorBase = new fornecedor();
    codigoPostal codPostFornecedorBase = new codigoPostal();
    String cpComparacao = ""; // para evitar notificoes repetidas de cps novos

    Alert a = new Alert(Alert.AlertType.NONE);

    @FXML
    private Label lbNome;
    @FXML
    private TextArea nome;
    @FXML
    private Label lbContribuinte;
    @FXML
    private TextArea contribuinte;
    @FXML
    private Label lbCodigoPostal;
    @FXML
    private Label lbMorada;
    @FXML
    private TextArea codigoPostal;
    @FXML
    private TextArea morada;
    @FXML
    private Label lbPais;
    @FXML
    private TextArea pais;
    @FXML
    private Label lbEmail;
    @FXML
    private TextArea email;
    @FXML
    private MenuBar menu;
    @FXML
    private TextArea cidade;
    @FXML
    private Label lbCidade;
    @FXML
    private Button botaoAlterarFornecedor;
    @FXML
    private Label lbPass1;
    @FXML
    private PasswordField password;
    @FXML
    private TextField passText;
    @FXML
    private CheckBox mostrarPassword;

    public ecraPessoalFornecedorController() {
    }

    /**
     * Método Intialize para apresentar os dados do fornecedor e carregar
     * informação necessária à validação das alterações.<br>
     *
     *
     * @param url url.
     * @param rb rb.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        esconderPasswoord();
        carregarDadosFornecedor();
        contribuinte.setEditable(false); 
        email.setEditable(false); 
        nome.setEditable(true);
        codigoPostal.setEditable(true);
        cidade.setEditable(false);
        try {
            //validar nome
            nome.focusedProperty()
                    .addListener((arg0, oldValue, newValue) -> {
                        if (!newValue) {
                            if (Pattern.compile("[\\p{L}\\p{Nd}]*").
                                    matcher(nome.getText()).find() == false || nome.getText().length() > 50) {
                                a.setAlertType(Alert.AlertType.WARNING);
                                a.getButtonTypes().setAll(ButtonType.OK);
                                a.setTitle("Nome Inválido");
                                a.setHeaderText("Formato do Nome Inválido");
                                a.setContentText("Insira um nome para a sua Empresa válido");
                                Optional<ButtonType> result = a.showAndWait();
                                if (result.get() == ButtonType.OK) {
                                    nome.setText(fornecedorBase.getNome());
                                }
                            } else if (nome.getText().equals("")) {
                                nome.setText(fornecedorBase.getNome());
                            }
                        }
                    }
                    );
            //validar morada
            morada.focusedProperty()
                    .addListener((arg0, oldValue, newValue) -> {
                        if (!newValue) {
                            if (Pattern.compile("[\\p{L}\\p{Nd}]*").
                                    matcher(morada.getText()).find() == false || morada.getText().length() > 100) {
                                a.setAlertType(Alert.AlertType.WARNING);
                                a.getButtonTypes().setAll(ButtonType.OK);
                                a.setTitle("Morada Inválida");
                                a.setHeaderText("Formato da Morada Inválido");
                                a.setContentText("Insira uma morada Válida");

                                Optional<ButtonType> result = a.showAndWait();
                                if (result.get() == ButtonType.OK) {
                                    morada.setText(fornecedorBase.getMorada());
                                }
                            } else if (morada.getText().equals("")) {
                                morada.setText(fornecedorBase.getMorada());
                            }
                        }
                    }
                    );

            //validar código postal
            codigoPostal.focusedProperty()
                    .addListener((arg0, oldValue, newValue) -> {
                        if (!newValue) {
                            String cpTemp = "";
                            if (Pattern.compile("^\\d{4}-\\d{3}?$").
                                    matcher(codigoPostal.getText()).find() == false) {
                                a.setAlertType(Alert.AlertType.WARNING);
                                a.getButtonTypes().setAll(ButtonType.OK);
                                a.setTitle("Código Postal Inválido");
                                a.setHeaderText("Formato da do Código Postal Inválido");
                                a.setContentText("Insira um código postal válido");

                                Optional<ButtonType> result = a.showAndWait();
                                if (result.get() == ButtonType.OK) {
                                    codigoPostal.setText(codPostFornecedorBase.getCodPostal());
                                    cidade.setText(codPostFornecedorBase.getCidade());
                                    cidade.setEditable(false);
                                }
                            } else if (isUniqueCP(codigoPostal.getText()) == false /*&& !codigoPostal.getText().equals("")*/) {
                                //o codigo postal base é unico também

                                if (codigoPostal.getText().equals(codPostFornecedorBase.getCodPostal())) { //para mitigar notificação irritante
                                    cidade.setText(codPostFornecedorBase.getCidade());
                                    cidade.setEditable(false);
                                } else if (cpComparacao.equals(codigoPostal.getText()) && !cpComparacao.equals("")) {

                                    cidade.setEditable(false);
                                } else {

                                    a.setAlertType(Alert.AlertType.CONFIRMATION);
                                    a.getButtonTypes().setAll(ButtonType.YES, ButtonType.NO);
                                    a.setTitle("Código Postal Detetado");
                                    a.setHeaderText("O código postal já existe na base de dados");
                                    a.setContentText("A cidade será alterada");
                                    Optional<ButtonType> result = a.showAndWait();
                                    if (result.get() == ButtonType.YES) {
                                        // se codigo postal existe, mudar valor do cp e da cidade 
                                        List<codigoPostal> newCPList = procurarCodPostal(codigoPostal.getText());
                                        for (codigoPostal cp : newCPList) {
                                            if (codigoPostal.getText().equals(cp.getCodPostal())) {
                                                codigoPostal.setText(cp.getCodPostal());
                                                cidade.setText(cp.getCidade());
                                                cidade.setEditable(false);
                                                cpComparacao = cp.getCodPostal();

                                            }
                                        }

                                    } else if (result.get() == ButtonType.NO) { //se não pretende alterar fica tudo igual
                                        codigoPostal.setText(codPostFornecedorBase.getCodPostal());
                                        cidade.setText(codPostFornecedorBase.getCidade());
                                        cidade.setEditable(false);
                                    }
                                }

                            } else if (isUniqueCP(codigoPostal.getText()) == true && !codigoPostal.getText().equals("")) {

                                a.setAlertType(Alert.AlertType.INFORMATION);
                                a.getButtonTypes().setAll(ButtonType.OK);
                                a.setTitle("Novo Código Postal");
                                a.setHeaderText("Inseriu um novo código postal");
                                a.setContentText("Já pode alterar a cidade");
                                Optional<ButtonType> result = a.showAndWait();
                                if (result.get() == ButtonType.OK) {
                                    cidade.setEditable(true); //cidade pode ficar igual, tem é de ficar editavel
                                }

                            } else if (codigoPostal.getText().equals("")) {
                                codigoPostal.setText(codPostFornecedorBase.getCodPostal());
                                cidade.setText(codPostFornecedorBase.getCidade());
                                cidade.setEditable(false);
                            }

                        }
                    }
                    );
           

            //validar pais
            pais.focusedProperty()
                    .addListener((arg0, oldValue, newValue) -> {
                        if (!newValue) {
                            if (Pattern.compile("^[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ'\\s]+$").
                                    matcher(pais.getText()).find() == false || pais.getText().length() > 30) {
                                a.setAlertType(Alert.AlertType.WARNING);
                                a.getButtonTypes().setAll(ButtonType.OK);
                                a.setTitle("Pais Inválido");
                                a.setHeaderText("Formato do Pais inválido");
                                a.setContentText("Insira um país válido");

                                Optional<ButtonType> result = a.showAndWait();
                                if (result.get() == ButtonType.OK) {
                                    pais.setText(fornecedorBase.getPais());
                                }
                            } else if (pais.getText().equals("")) {
                                pais.setText(fornecedorBase.getPais());
                            }
                        }
                    }
                    );

            //validar password
            password.focusedProperty()
                    .addListener((arg0, oldValue, newValue) -> {
                        if (!newValue) {
                            char[] novaPassArray = password.getText().toCharArray();
                            char[] alfabeto = "abcdefghijklmnopqrstuvwxyzABCDEFGJKLMNPRSTUVWXYZ0123456789^$?!@#%&".toCharArray();
                            int tamanhoMinNovaPass = 4; //por questões de retro-compatbilidade apenas
                            int tamanhoMaxNovaPass = 10;
                            int charsOK = 0;
                            String testeNovaPassIgualOldPass = passwordUtils.generateSecurePassword(password.getText(), utilizadorBase.getSalt());
                            if (password.getText().equals("")) {
                                /*é para deixar o campo em branco e já deixa*/
                            } else if (testeNovaPassIgualOldPass.equals(utilizadorBase.getPassword())) {
                                a.setAlertType(Alert.AlertType.INFORMATION);
                                a.setTitle("Password Inválida");
                                a.setHeaderText("Não efectuou nenhuma alteração à sua password");
                                a.setContentText("Insira uma nova password caso pretenda alterar a atual");
                                a.getButtonTypes().setAll(ButtonType.OK);
                                Optional<ButtonType> result = a.showAndWait();
                                if (result.get() == ButtonType.OK) {
                                    password.setText("");
                                }
                            } else if (novaPassArray.length < tamanhoMinNovaPass || novaPassArray.length > tamanhoMaxNovaPass) {
                                a.setAlertType(Alert.AlertType.INFORMATION);
                                a.setTitle("Password Inválida");
                                a.setHeaderText("Nova password com tamanho inválido!");
                                a.setContentText("Insira uma password contendo 4-10 caracteres sem espaços");
                                a.getButtonTypes().setAll(ButtonType.OK);
                                Optional<ButtonType> result = a.showAndWait();
                                if (result.get() == ButtonType.OK) {
                                    password.setText("");
                                }
                            } else {
                                for (int i = 0; i < novaPassArray.length; i++) {
                                    for (int j = 0; j < alfabeto.length; j++) {
                                        if (novaPassArray[i] == alfabeto[j]) {
                                            charsOK++;
                                        }
                                    }
                                }
                                if (charsOK == password.getText().length()) {

                                } else {
                                    a.setAlertType(Alert.AlertType.INFORMATION);
                                    a.setTitle("Password Inválida");
                                    a.setHeaderText("Password contém caracteres inválidos");
                                    a.setContentText("Só pode escolher caracteres do tipo 'AaZz09^$?!@#%&' e sem espaços entre eles.");
                                    a.getButtonTypes().setAll(ButtonType.OK);
                                    Optional<ButtonType> result = a.showAndWait();
                                    if (result.get() == ButtonType.OK) {
                                        password.setText("");
                                    }

                                }
                            }
                        }

                    }
                    );
        } catch (Exception estourouWTF) {
            Logger.getLogger(ecraPessoalFornecedorController.class.getName()).log(Level.SEVERE, null, estourouWTF);
        }
    }

    /**
     * Método para alterar os dados pessoais do fornecedor.<br>
     * Método faz varias confirmações e validações às alteraç~es que ocorreram ao
     * fornecedor e depois de passar a elas todas vai alterar os valores na BD.<br>
     *
     * @param event evento.
     *
     */
    @FXML
    private void botaoAlterarFornecedor(ActionEvent event
    ) {
        Alert alerta = new Alert(Alert.AlertType.NONE);
        alerta.setAlertType(Alert.AlertType.CONFIRMATION);
        alerta.setTitle("Fornecedores");
        alerta.setHeaderText("Deseja mesmo atualizar os seus dados?");
        alerta.setResizable(false);
        alerta.getButtonTypes().setAll(ButtonType.YES, ButtonType.NO);
        Optional<ButtonType> result = alerta.showAndWait();

        if (result.get() == ButtonType.YES) {

            //verificar se o utilizador ainda existe na BD
            List<utilizador> utilizadores = procurarUtilizadorIDEmail(utilizadorBase.getIdUtilizador(), utilizadorBase.getEmail());
            int count = 0;
            for (utilizador uti : utilizadores) {
                count = 1;
            }
            if (count == 0) {
                alerta.setAlertType(Alert.AlertType.ERROR);
                alerta.setTitle("Ecrã Pessoal Utilizador");
                alerta.setHeaderText("A sua conta foi removida!");
                alerta.setContentText("A sua conta foi eliminada enquanto a alterava");
                alerta.setResizable(false);
                alerta.getButtonTypes().setAll(ButtonType.OK);
                Optional<ButtonType> resultA = alerta.showAndWait();
                if (resultA.get() == ButtonType.OK) {
                    System.exit(0);
                }

            } else {
                //aqui entra o alterar todo - com precaução de campos ficarem vazios tirando a password
                int contadorSucesso = -1; // para validar a alteração
                int emailEnviado = 0;
                int emailEnviadoPass = 0;
                int falhou = 0; //parar método após falha de uma alteração
                //atualizar nome
                if (!nome.getText().equals(fornecedorBase.getNome()) && !nome.getText().equals("") && falhou == 0) {
                    alterarNomeFornecedor(nome.getText(), fornecedorBase.getIdUtilizador());
                    List<fornecedor> fornecedoresNome = procurarFornecedorID(fornecedorBase.getIdFornecedor());
                    for (fornecedor fornecedor : fornecedoresNome) {
                        if (fornecedor.getNome().equals(nome.getText())) {
                            contadorSucesso = 1;
                        } else {
                            contadorSucesso = 0;
                            falhou = 1;
                            alerta.setAlertType(Alert.AlertType.ERROR);
                            alerta.getButtonTypes().setAll(ButtonType.OK);
                            alerta.setTitle("Ecrã Pessoal Utilizador");
                            alerta.setHeaderText("Actualização Nome Fornecedor");
                            alerta.setContentText("Não foi possível atualizar o nome da sua Empresa");
                            alerta.setResizable(false);
                            alerta.showAndWait();
                            Optional<ButtonType> resultA = alerta.showAndWait();
                            if (resultA.get() == ButtonType.OK) {
                                ecraPessoalFornecedor ePForn = new ecraPessoalFornecedor();
                                fecha();
                                try {
                                    ePForn.start(new Stage());
                                } catch (Exception ex) {
                                    a.setAlertType(Alert.AlertType.ERROR);
                                    a.setTitle("ERRO Fatal");
                                    a.setHeaderText("A aplicação está corrompida!");
                                    a.setContentText("Algumas funcionalidades do aplicação estão inacessíveis");
                                    Logger
                                            .getLogger(ecraPessoalFornecedorController.class
                                                    .getName()).log(Level.SEVERE, null, ex);
                                    a.getButtonTypes().setAll(ButtonType.OK);
                                    Optional<ButtonType> result3 = a.showAndWait();
                                    if (result3.get() == ButtonType.OK) {
                                        System.exit(0);
                                    }
                                }
                            }

                        }
                    }
                }

                //não é permitido atualizar nif senão entrava aqui
                //atualizar email
                if (!email.getText().equals(utilizadorBase.getEmail()) && !email.getText().equals("") && falhou == 0) {
                    alterarEmail(email.getText(), utilizadorBase.getIdUtilizador(), utilizadorBase.getNivel());
                    List<utilizador> utilizadoresEmail = procurarUtilizadorID(utilizadorBase.getIdUtilizador());
                    for (utilizador utilizador : utilizadoresEmail) {
                        if (utilizador.getEmail().equals(email.getText())) {
                            enviarEmailConfirmacao(email.getText(), utilizadorBase.getEmail());
                            contadorSucesso = 1;

                            emailEnviado = 1; //notificação de envio de email de alteração de email
                        } else {
                            contadorSucesso = 0;
                            falhou = 1;
                            alerta.setAlertType(Alert.AlertType.ERROR);
                            alerta.setTitle("Ecrã Pessoal Utilizador");
                            alerta.setHeaderText("Actualização Email Fornecedor");
                            alerta.setContentText("Não foi possível atualizar o email da sua Empresa");
                            alerta.setResizable(false);
                            alerta.getButtonTypes().setAll(ButtonType.OK);
                            Optional<ButtonType> resultA = alerta.showAndWait();
                            if (resultA.get() == ButtonType.OK) {
                                ecraPessoalFornecedor ePForn = new ecraPessoalFornecedor();
                                fecha();
                                try {
                                    ePForn.start(new Stage());
                                } catch (Exception ex) {
                                    a.setAlertType(Alert.AlertType.ERROR);
                                    a.setTitle("ERRO Fatal");
                                    a.setHeaderText("A aplicação está corrompida!");
                                    a.setContentText("Algumas funcionalidades do aplicação estão inacessíveis");
                                    Logger
                                            .getLogger(ecraPessoalFornecedorController.class
                                                    .getName()).log(Level.SEVERE, null, ex);
                                    a.getButtonTypes().setAll(ButtonType.OK);
                                    Optional<ButtonType> result3 = a.showAndWait();
                                    if (result3.get() == ButtonType.OK) {
                                        System.exit(0);
                                    }
                                }
                            }
                        }
                    }
                }
                //atualizar morada
                if (!morada.getText().equals(fornecedorBase.getMorada()) && !morada.getText().equals("") && falhou == 0) {
                    alterarMoradaFornecedor(morada.getText(), fornecedorBase.getIdUtilizador());
                    List<fornecedor> fornecedoresMorada = procurarFornecedorID(fornecedorBase.getIdFornecedor());
                    for (fornecedor fornecedor : fornecedoresMorada) {
                        if (fornecedor.getMorada().equals(morada.getText())) {
                            contadorSucesso = 1;

                        } else {
                            contadorSucesso = 0;
                            falhou = 1;
                            alerta.setAlertType(Alert.AlertType.ERROR);
                            alerta.setTitle("Ecrã Pessoal Utilizador");
                            alerta.setHeaderText("Actualização Morada Fornecedor");
                            alerta.setContentText("Não foi possível atualizar a morada da sua Empresa");
                            alerta.setResizable(false);
                            alerta.getButtonTypes().setAll(ButtonType.OK);
                            Optional<ButtonType> resultA = alerta.showAndWait();
                            if (resultA.get() == ButtonType.OK) {
                                ecraPessoalFornecedor ePForn = new ecraPessoalFornecedor();
                                fecha();
                                try {
                                    ePForn.start(new Stage());
                                } catch (Exception ex) {
                                    a.setAlertType(Alert.AlertType.ERROR);
                                    a.setTitle("ERRO Fatal");
                                    a.setHeaderText("A aplicação está corrompida!");
                                    a.setContentText("Algumas funcionalidades do aplicação estão inacessíveis");
                                    Logger
                                            .getLogger(ecraPessoalFornecedorController.class
                                                    .getName()).log(Level.SEVERE, null, ex);
                                    a.getButtonTypes().setAll(ButtonType.OK);
                                    Optional<ButtonType> result3 = a.showAndWait();
                                    if (result3.get() == ButtonType.OK) {
                                        System.exit(0);
                                    }
                                }
                            }
                        }
                    }
                }

                //atualizar código postal e cidade - cidade pode ficar igual desde que código postal seja diferente
                if (!codigoPostal.getText().equals(fornecedorBase.getCodPostal())
                        && !codigoPostal.getText().equals("") && !cidade.getText().equals("") && falhou == 0) {
                    //primeiro inserir CP e Cidade na tabela código postal e depois tratar da tabela fornecedor
                    List<codigoPostal> codigosPostais = procurarCodPostal(codigoPostal.getText());
                    int totalCPs = 0;
                    for (codigoPostal codPos : codigosPostais) {
                        totalCPs = 1;
                    }

                    if (totalCPs == 0) {
                        //inserir novo código postal e cidade na tabela codigo postal
                        inserirCodPostal(codigoPostal.getText(), cidade.getText());
                        List<codigoPostal> novosCPSs = procurarCodPostal(codigoPostal.getText());
                        for (codigoPostal codPos : novosCPSs) {
                            if (codPos.getCodPostal().equals(codigoPostal.getText()) && codPos.getCidade().equals(cidade.getText())) {
                                /*sucessoparcial*/
                            } else {
                                contadorSucesso = 0;
                                falhou = 1;
                                alerta.setAlertType(Alert.AlertType.ERROR);
                                alerta.setTitle("Ecrã Pessoal Utilizador");
                                alerta.setHeaderText("Actualização Código Postal & Cidade Fornecedor");
                                alerta.setContentText("Não foi possível inserir o código postal e a cidade na base de dados");
                                alerta.setResizable(false);
                                alerta.getButtonTypes().setAll(ButtonType.OK);
                                Optional<ButtonType> resultA = alerta.showAndWait();
                                if (resultA.get() == ButtonType.OK) {
                                }
                            }
                        }
                        //atualizar CP do fornecedor na tabela fornecedor
                        alterarCodPosFornecedor(codigoPostal.getText(), fornecedorBase.getIdUtilizador());
                        List<fornecedor> fornCPMudados = procurarFornecedorID(fornecedorBase.getIdFornecedor());

                        for (fornecedor fornCPMD : fornCPMudados) {

                            if (fornCPMD.getCodPostal().equals(codigoPostal.getText())) {
                                contadorSucesso = 1;
                            } else {
                                contadorSucesso = 0;
                                falhou = 1;
                                alerta.setAlertType(Alert.AlertType.ERROR);
                                alerta.setTitle("Ecrã Pessoal Utilizador");
                                alerta.setHeaderText("Actualização Código Postal Fornecedor");
                                alerta.setContentText("Não foi possível atualizar o código postal da Empresa");
                                alerta.setResizable(false);
                                alerta.getButtonTypes().setAll(ButtonType.OK);
                                Optional<ButtonType> resultA = alerta.showAndWait();
                                if (resultA.get() == ButtonType.OK) {
                                    /*quem fecha é o global*/
                                }
                            }

                        }
                    } else if (totalCPs == 1) {//se código postal já existe, só se adiciona o fornecedor ao mesmo

                        //atualizar CP do fornecedor na tabela fornecedor
                        alterarCodPosFornecedor(codigoPostal.getText(), fornecedorBase.getIdUtilizador());
                        List<fornecedor> fornecedoresCPNovosB = procurarFornecedorID(fornecedorBase.getIdFornecedor());
                        for (fornecedor fornecedor : fornecedoresCPNovosB) {

                            if (fornecedor.getCodPostal().equals(codigoPostal.getText())) {
                                contadorSucesso = 1;
                            } else {
                                contadorSucesso = 0;
                                falhou = 1;
                                alerta.setAlertType(Alert.AlertType.ERROR);
                                alerta.setTitle("Ecrã Pessoal Utilizador");
                                alerta.setHeaderText("Actualização Código Postal Fornecedor");
                                alerta.setContentText("Não foi possível atualizar o código postal da Empresa");
                                alerta.setResizable(false);
                                alerta.getButtonTypes().setAll(ButtonType.OK);
                                Optional<ButtonType> resultA = alerta.showAndWait();
                                if (resultA.get() == ButtonType.OK) {
                                    /*quem fecha é o global*/
                                }
                            }

                        }

                    }
                }

                //atualizar país
                if (!pais.getText().equals(fornecedorBase.getPais()) && !pais.getText().equals("") && falhou == 0) {
                    alterarPaisFornecedor(pais.getText(), utilizadorBase.getIdUtilizador());
                    List<fornecedor> fornecedoresPais = procurarFornecedorID(fornecedorBase.getIdFornecedor());

                    for (fornecedor fornecedor : fornecedoresPais) {

                        if (fornecedor.getPais().equals(pais.getText())) {
                            contadorSucesso = 1;
                        } else {
                            contadorSucesso = 0;
                            falhou = 1;
                            alerta.setAlertType(Alert.AlertType.ERROR);
                            alerta.setTitle("Ecrã Pessoal Utilizador");
                            alerta.setHeaderText("Actualização País Fornecedor");
                            alerta.setContentText("Não foi possível atualizar o país onde a sua Empresa está sediada");
                            alerta.setResizable(false);
                            alerta.getButtonTypes().setAll(ButtonType.OK);
                            Optional<ButtonType> resultA = alerta.showAndWait();
                            if (resultA.get() == ButtonType.OK) {
                            }
                        }
                    }
                }
                //atualizar password
                if (!password.getText().equals("") && falhou == 0) { //diferente de vazio já está validada e assume que é única
                    //definir o email para onde enviar a password
                    String emailEnviarPassword = ""; //email pode ter sido alterado entretanto como acima
                    List<utilizador> utilizadoresEmailPassword = procurarUtilizadorID(utilizadorBase.getIdUtilizador());
                    for (utilizador utilizador : utilizadoresEmailPassword) {
                        emailEnviarPassword = utilizador.getEmail();
                        //uma segurança
                        if (emailEnviarPassword.equals("")) {
                            emailEnviarPassword = email.getText(); //fica o default ou o novo
                        }
                    }

                    //obter a password e salt
                    String salt = passwordUtils.getSalt(30);
                    String mySecurePassword = passwordUtils.generateSecurePassword(password.getText(), salt);
                    //enviar email ao fornecedor

                    alterarPassUtilizador(mySecurePassword, salt, utilizadorBase.getIdUtilizador(), emailEnviarPassword, utilizadorBase.getNivel(), utilizadorBase.getEstado());

                    List<utilizador> utilizadoresPasswordNova = procurarUtilizadorID(utilizadorBase.getIdUtilizador());

                    for (utilizador utilizadorPass : utilizadoresPasswordNova) {
                        if (utilizadorPass.getPassword().equals(mySecurePassword) && utilizadorPass.getSalt().equals(salt)) {
                            //só envia o mail se a password for mesmo alterada

                            enviarEmail(emailEnviarPassword, password.getText(), fornecedorBase.getIdFornecedor());
                            contadorSucesso = 1;

                            emailEnviadoPass = 1; //notificação de envio de email de alteração de password

                        } else {
                            contadorSucesso = 0;
                            falhou = 1;
                            alerta.setAlertType(Alert.AlertType.ERROR);
                            alerta.setTitle("Ecrã Pessoal Utilizador");
                            alerta.setHeaderText("Actualização Password Fornecedor");
                            alerta.setContentText("Não foi possível atualizar a password da conta da sua conta");
                            alerta.setResizable(false);
                            alerta.getButtonTypes().setAll(ButtonType.OK);
                            Optional<ButtonType> resultA = alerta.showAndWait();
                            if (resultA.get() == ButtonType.OK) {

                            }
                        }

                    }
                }
                //validar alteração de dados
                if (contadorSucesso == 1 && falhou == 0) {
                    a.setAlertType(Alert.AlertType.INFORMATION);
                    a.setTitle("Ecrã Pessoal do Utilizador");
                    a.setHeaderText("Dados alterados com sucesso!");
                    if (emailEnviado == 1 && emailEnviadoPass == 1) {
                        a.setContentText("Foram-lhe enviados emails para confirmar o novo email e a nova password");
                    } else if (emailEnviado == 1 && emailEnviadoPass == 0) {
                        a.setContentText("Foi-lhe enviado um email de confirmação para o seu novo endereço de email");
                    } else if (emailEnviado == 0 && emailEnviadoPass == 1) {
                        a.setContentText("Foi-lhe enviado um email com a sua nova password");
                    } else {
                        a.setContentText("As suas alterações foram aplicadas!");
                    }
                    a.getButtonTypes().setAll(ButtonType.OK);
                    Optional<ButtonType> result2 = a.showAndWait();
                    if (result2.get() == ButtonType.OK) {
                        ecraPessoalFornecedor ePForn = new ecraPessoalFornecedor();
                        fecha();
                        try {
                            ePForn.start(new Stage());
                        } catch (Exception ex) {
                            a.setAlertType(Alert.AlertType.ERROR);
                            a.setTitle("ERRO Fatal");
                            a.setHeaderText("A aplicação está corrompida!");
                            a.setContentText("Algumas funcionalidades do aplicação estão inacessíveis");
                            a.show();
                            System.out.println("Erro Exception ex " + ex.getMessage());
                            Logger
                                    .getLogger(ecraPessoalFornecedorController.class
                                            .getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                } else if (contadorSucesso == -1) {
                    a.setAlertType(Alert.AlertType.ERROR);
                    a.setTitle("Ecrã Pessoal Utilizador");
                    a.setHeaderText("Actualização Dados Utilizador");
                    a.setContentText("Não efectuou nenhuma alteração!");
                    a.setResizable(false);
                    a.getButtonTypes().setAll(ButtonType.OK);
                    Optional<ButtonType> resultZ = a.showAndWait();
                    if (resultZ.get() == ButtonType.OK) {
                        ecraPessoalFornecedor ePForn = new ecraPessoalFornecedor();
                        fecha();
                        try {
                            ePForn.start(new Stage());
                        } catch (Exception ex) {
                            a.setAlertType(Alert.AlertType.ERROR);
                            a.setTitle("ERRO Fatal");
                            a.setHeaderText("A aplicação está corrompida!");
                            a.setContentText("Algumas funcionalidades do aplicação estão inacessíveis");
                            Logger
                                    .getLogger(ecraPessoalFornecedorController.class
                                            .getName()).log(Level.SEVERE, null, ex);
                            a.getButtonTypes().setAll(ButtonType.OK);
                            Optional<ButtonType> result3 = a.showAndWait();
                            if (result3.get() == ButtonType.OK) {
                                System.exit(0);
                            }
                        }
                    }
                } else {
                    a.setAlertType(Alert.AlertType.ERROR);
                    a.setTitle("Ecrã Pessoal Utilizador");
                    a.setHeaderText("Actualização Dados Utilizador");
                    a.setContentText("Não foi possível alterar os dados na base de dados");
                    a.setResizable(false);
                    a.getButtonTypes().setAll(ButtonType.OK);
                    Optional<ButtonType> resultZ = a.showAndWait();
                    if (resultZ.get() == ButtonType.OK) {
                        ecraPessoalFornecedor ePForn = new ecraPessoalFornecedor();
                        fecha();
                        try {
                            ePForn.start(new Stage());
                        } catch (Exception ex) {
                            a.setAlertType(Alert.AlertType.ERROR);
                            a.setTitle("ERRO Fatal");
                            a.setHeaderText("A aplicação está corrompida!");
                            a.setContentText("Algumas funcionalidades do aplicação estão inacessíveis");
                            Logger
                                    .getLogger(ecraPessoalFornecedorController.class
                                            .getName()).log(Level.SEVERE, null, ex);
                            a.getButtonTypes().setAll(ButtonType.OK);
                            Optional<ButtonType> result3 = a.showAndWait();
                            if (result3.get() == ButtonType.OK) {
                                System.exit(0);
                            }
                        }
                    }
                }
            }

        } else if (result.get() == ButtonType.NO) {
            ecraPessoalFornecedor ePForn = new ecraPessoalFornecedor();
            fecha();
            try {
                ePForn.start(new Stage());
            } catch (Exception ex) {
                a.setAlertType(Alert.AlertType.ERROR);
                a.setTitle("ERRO Fatal");
                a.setHeaderText("A aplicação está corrompida!");
                a.setContentText("Algumas funcionalidades do aplicação estão inacessíveis");
                Logger
                        .getLogger(ecraPessoalFornecedorController.class
                                .getName()).log(Level.SEVERE, null, ex);
                a.getButtonTypes().setAll(ButtonType.OK);
                Optional<ButtonType> result3 = a.showAndWait();
                if (result3.get() == ButtonType.OK) {
                    System.exit(0);
                }
            }
        } else {
            a.setAlertType(Alert.AlertType.ERROR);
            a.setTitle("ERRO Fatal");
            a.setHeaderText("A aplicação está corrompida!");
            a.setContentText("Algumas funcionalidades do aplicação estão inacessíveis");
            a.getButtonTypes().setAll(ButtonType.OK);
            Optional<ButtonType> result3 = a.showAndWait();
            if (result3.get() == ButtonType.OK) {
                System.exit(0);
            }

        }

    }

   /**
     * Método para fechar o ecrã.<br>
     */
    private void fecha() {
        ecraPessoalFornecedor.getStage().close();
    }

    /**
     * Método para terminar sessao do utilizador.<br>
     *
     * @param event evento.
     */
    @FXML
    private void terminarSessao(ActionEvent event) {
        try {
            login.setIdLogin(0);
            login.setNivelLogin(10000);
            login log = new login();
            fecha();
            log.start(new Stage());

        } catch (Exception ex) {
            Logger.getLogger(ecraPessoalFornecedorController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Método para encerrar a aplicação.<br>
     */
    @FXML
    private void sair(ActionEvent event) {
        System.exit(0);
    }

    /**
     * Método para aceder ao menu do fornecedor.<br>
     */
    @FXML
    private void openMenu(ActionEvent event) {
        try {
            menuFornecedor mF = new menuFornecedor();
            fecha();
            mF.start(new Stage());

        } catch (Exception ex) {
            Logger.getLogger(ecraPessoalFornecedorController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Método para mostar alguns dados do fornecedor no ecrã e para carregar os
     * dados do mesmo em memória para posterior uso aquando da alteração (ou
     * não) dos vários atributos e respectivas validações. Os parâmateros de
     * base do fornecedor servirão como valores default caso o fornecedor não
     * altere nenhum valor ou decida apagar valores que não deva apagar.<br>
     */
    private void carregarDadosFornecedor() {
        //buscar dados de id do utilizador fornecedor
        List<utilizador> fornecedoresUtilizadores = procurarEmail(login.getIdLogin());
        for (utilizador fornecedor : fornecedoresUtilizadores) {
            utilizadorBase.setEmail(fornecedor.getEmail());
            utilizadorBase.setIdUtilizador(fornecedor.getIdUtilizador());
            utilizadorBase.setNivel(fornecedor.getNivel());
            utilizadorBase.setPassword(fornecedor.getPassword());
            utilizadorBase.setSalt(fornecedor.getSalt());
            utilizadorBase.setEstado(fornecedor.getEstado());
        }

        //buscar dados do fornecedor em si
        List<fornecedor> dadosFornecedorBase = listarFornecedor(utilizadorBase.getIdUtilizador());
        for (fornecedor fornecedorBaseBD : dadosFornecedorBase) {
            fornecedorBase.setIdFornecedor(fornecedorBaseBD.getIdFornecedor());
            fornecedorBase.setIdUtilizador(fornecedorBaseBD.getIdUtilizador()); //deve ser igual ao anterior mas valida-se depois
            fornecedorBase.setNome(fornecedorBaseBD.getNome());
            fornecedorBase.setContribuinte(fornecedorBaseBD.getContribuinte());
            fornecedorBase.setMorada(fornecedorBaseBD.getMorada());
            fornecedorBase.setCodPostal(fornecedorBaseBD.getCodPostal());
            fornecedorBase.setPais(fornecedorBaseBD.getPais());
            fornecedorBase.setEstado(fornecedorBaseBD.getEstado());
        }

        //buscar cidade a partir do código postal 
        List<codigoPostal> codigoPostalFornecedorBase = procurarCodPostal(fornecedorBase.getCodPostal());
        //como está, isto vai dar última cidade do cp pois este pode ter várias cidades - um terceiro campo para validar não ficaria mal
        for (codigoPostal cp : codigoPostalFornecedorBase) {
            codPostFornecedorBase.setCidade(cp.getCidade());
            codPostFornecedorBase.setCodPostal(cp.getCodPostal());
        }

        if (utilizadorBase.getIdUtilizador() != fornecedorBase.getIdUtilizador()
                || !codPostFornecedorBase.getCodPostal().equals(fornecedorBase.getCodPostal())) {
            a.setAlertType(Alert.AlertType.ERROR);
            a.setTitle("ERRO SQL");
            a.setHeaderText("Conflito dados Utilizador!");
            a.setContentText("Os seus dados estão corrompidos. Contacte o Administrador.");
            a.getButtonTypes().setAll(ButtonType.OK);
            Optional<ButtonType> result = a.showAndWait();
            if (result.get() == ButtonType.OK) {
                System.exit(0);
            }

        }

        nome.setText(fornecedorBase.getNome());
        contribuinte.setText(String.valueOf(fornecedorBase.getContribuinte()));
        email.setText(utilizadorBase.getEmail());
        morada.setText(fornecedorBase.getMorada());
        codigoPostal.setText(codPostFornecedorBase.getCodPostal());
        cidade.setText(codPostFornecedorBase.getCidade());
        pais.setText(fornecedorBase.getPais());
        password.setText("");
        passText.setText("");

        if (nome.getText().equals("") || contribuinte.getText().equals("")
                || email.getText().equals("") || morada.getText().equals("")
                || codigoPostal.getText().equals("") || cidade.getText().equals("")
                || pais.getText().equals("") || fornecedorBase.getIdUtilizador() == 0
                || fornecedorBase.getIdFornecedor().equals("") || fornecedorBase.getEstado().equals("")
                || fornecedorBase.getEstado().equals("inativo") || utilizadorBase.getNivel() != 0 //nivel é zero
                || utilizadorBase.getPassword().equals("") || utilizadorBase.getSalt().equals("")) {
            a.setAlertType(Alert.AlertType.ERROR);
            a.setTitle("ERRO SQL");
            a.setHeaderText("Dados Utilizador em falta!");
            a.setContentText("Os seus dados estão corrompidos. Contacte o Administrador.");
            a.getButtonTypes().setAll(ButtonType.OK);
            Optional<ButtonType> result = a.showAndWait();
            if (result.get() == ButtonType.OK) {
                System.exit(0);
            }

        }

    }

    /**
     * Método para mostrar e ocultar a nova password inserida pelo utilizador
     * ligando um passwordfield a um textfield tudo sob controlo de uma checkbox.<br>
     */
    private void esconderPasswoord() {
        passText.setManaged(false);
        passText.setVisible(false);
        passText.managedProperty().bind(mostrarPassword.selectedProperty());
        passText.visibleProperty().bind(mostrarPassword.selectedProperty());
        password.managedProperty().bind(mostrarPassword.selectedProperty().not());
        password.visibleProperty().bind(mostrarPassword.selectedProperty().not());
        passText.textProperty().bindBidirectional(password.textProperty());
    }

    /**
     * Método para verficar se um código postal a inserir é único substutido por
     * outra lógica de programação.<br>
     *
     * @param newCP : o novo código postal a inserir.
     * @return : verdadeiro se o novo código postal for único e falso em caso
     * contrário.
     */
    public boolean isUniqueCP(String newCP) {
        boolean isUniqueCP = false;

        List<codigoPostal> codigosPostais = procurarCodPostal(newCP);
        int count = 0;
        for (codigoPostal cp : codigosPostais) {
            if (cp.getCodPostal().equals(newCP) /*&& !newCP.equals(codPostFornecedorBase.getCodPostal())*/) {
                count = 1;
            }
        }
        if (count == 1) {
            isUniqueCP = false;
        } else if (count == 0) {
            isUniqueCP = true;
        }

        return isUniqueCP;
    }

}
