/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

/**
 * Classe gerarPasswordManual para gerar manualmente uma password para utlizador
 * novo
 */
public class gerarPasswordManual {

    /**
     * serve para gerar passwords manuais
     *
     * @param args args
     */
    public static void main(String[] args) {
        String myPassword = "1234";

        // Generate Salt. The generated value can be stored in DB. 
        String salt = passwordUtils.getSalt(30);

        // Protect user's password. The generated value can be stored in DB.
        String mySecurePassword = passwordUtils.generateSecurePassword(myPassword, salt);

        // Print out protected password 
        System.out.println("My secure password = " + mySecurePassword);
        System.out.println("Salt value = " + salt);
    }

}
