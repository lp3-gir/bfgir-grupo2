/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ruben
 */
public class ProdutofornecedorTest {


    /**
     * Test of getProdutofornecedorPK method, of class Produtofornecedor.
     */
    @Test
    public void testGetProdutofornecedorPK() {
        System.out.println("getProdutofornecedorPK");
        Produtofornecedor instance = new Produtofornecedor();
        ProdutofornecedorPK pfpk = new ProdutofornecedorPK();
        pfpk.setCodProdForn("Teste");
        pfpk.setCodProdPa("T.123");

        instance.setProdutofornecedorPK(pfpk);
        assertEquals(pfpk, instance.getProdutofornecedorPK());

    }

    /**
     * Test of setProdutofornecedorPK method, of class Produtofornecedor.
     */
    @Test
    public void testSetProdutofornecedorPK() {
        System.out.println("setProdutofornecedorPK");

        ProdutofornecedorPK pfpk = new ProdutofornecedorPK();
        pfpk.setCodProdForn("Teste");
        pfpk.setCodProdPa("T.123");
        
        Produtofornecedor instance = new Produtofornecedor();
        instance.setProdutofornecedorPK(pfpk);

    }

    /**
     * Test of getIdFornecedor method, of class Produtofornecedor.
     */
    @Test
    public void testGetIdFornecedor() {
        System.out.println("getIdFornecedor");
        Produtofornecedor instance = new Produtofornecedor();
        instance.setIdFornecedor("Teste");
        assertEquals("Teste", instance.getIdFornecedor());

    }

    /**
     * Test of setIdFornecedor method, of class Produtofornecedor.
     */
    @Test
    public void testSetIdFornecedor() {
        System.out.println("setIdFornecedor");
        Produtofornecedor instance = new Produtofornecedor();
        instance.setIdFornecedor("Teste");

    }

}
