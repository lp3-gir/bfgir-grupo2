/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ruben
 */
public class EntradaprodutoTest {



    /**
     * Test of getEntradaprodutoPK method, of class Entradaproduto.
     */
    @Test
    public void testGetEntradaprodutoPK() {
        System.out.println("getEntradaprodutoPK");

        EntradaprodutoPK eppk = new EntradaprodutoPK();
        eppk.setIdEntrada("1");
        eppk.setCodProdPa("T.123");

        Entradaproduto result = new Entradaproduto();
        result.setEntradaprodutoPK(eppk);

        assertEquals(eppk, result.getEntradaprodutoPK());

    }

    /**
     * Test of setEntradaprodutoPK method, of class Entradaproduto.
     */
    @Test
    public void testSetEntradaprodutoPK() {
        System.out.println("setEntradaprodutoPK");

        EntradaprodutoPK eppk = new EntradaprodutoPK();
        eppk.setIdEntrada("1");
        eppk.setCodProdPa("T.123");

        Entradaproduto instance = new Entradaproduto();
        instance.setEntradaprodutoPK(eppk);

    }

    /**
     * Test of getPrecoUnitSIVA method, of class Entradaproduto.
     */
    @Test
    public void testGetPrecoUnitSIVA() {
        System.out.println("getPrecoUnitSIVA");
        Entradaproduto instance = new Entradaproduto();
        instance.setPrecoUnitSIVA(50.3);
        assertEquals(50.3, instance.getPrecoUnitSIVA(), 0.0);

    }

    /**
     * Test of setPrecoUnitSIVA method, of class Entradaproduto.
     */
    @Test
    public void testSetPrecoUnitSIVA() {
        System.out.println("setPrecoUnitSIVA");
        Entradaproduto instance = new Entradaproduto();
        instance.setPrecoUnitSIVA(50.3);

    }

    /**
     * Test of getTipoPrecos method, of class Entradaproduto.
     */
    @Test
    public void testGetTipoPrecos() {
        System.out.println("getTipoPrecos");
        Entradaproduto instance = new Entradaproduto();
        instance.setTipoPeso("Teste");
        assertEquals("Teste", instance.getTipoPeso());

    }

    /**
     * Test of setTipoPrecos method, of class Entradaproduto.
     */
    @Test
    public void testSetTipoPrecos() {
        System.out.println("setTipoPrecos");
        Entradaproduto instance = new Entradaproduto();
        instance.setTipoPrecos("Teste");

    }

    /**
     * Test of getPrecoTotalSIVA method, of class Entradaproduto.
     */
    @Test
    public void testGetPrecoTotalSIVA() {
        System.out.println("getPrecoTotalSIVA");
        Entradaproduto instance = new Entradaproduto();
        instance.setPrecoTotalSIVA(5000.30);
        assertEquals(5000.30, instance.getPrecoTotalSIVA(), 0.0);

    }

    /**
     * Test of setPrecoTotalSIVA method, of class Entradaproduto.
     */
    @Test
    public void testSetPrecoTotalSIVA() {
        System.out.println("setPrecoTotalSIVA");
        Entradaproduto instance = new Entradaproduto();
        instance.setPrecoTotalSIVA(5000.30);

    }

    /**
     * Test of getIva method, of class Entradaproduto.
     */
    @Test
    public void testGetIva() {
        System.out.println("getIva");
        Entradaproduto instance = new Entradaproduto();
        instance.setIva(23);
        assertEquals(23, instance.getIva());

    }

    /**
     * Test of setIva method, of class Entradaproduto.
     */
    @Test
    public void testSetIva() {
        System.out.println("setIva");
        Entradaproduto instance = new Entradaproduto();
        instance.setIva(23);

    }

    /**
     * Test of getTotalIVA method, of class Entradaproduto.
     */
    @Test
    public void testGetTotalIVA() {
        System.out.println("getTotalIVA");
        Entradaproduto instance = new Entradaproduto();
        instance.setTotalIVA(1500);
        assertEquals(1500, instance.getTotalIVA(), 0.0);

    }

    /**
     * Test of setTotalIVA method, of class Entradaproduto.
     */
    @Test
    public void testSetTotalIVA() {
        System.out.println("setTotalIVA");
        Entradaproduto instance = new Entradaproduto();
        instance.setTotalIVA(500);

    }

    /**
     * Test of getLocalTAX method, of class Entradaproduto.
     */
    @Test
    public void testGetLocalTAX() {
        System.out.println("getLocalTAX");
        Entradaproduto instance = new Entradaproduto();
        instance.setLocalTAX("PT");
        assertEquals("PT", instance.getLocalTAX());

    }

    /**
     * Test of setLocalTAX method, of class Entradaproduto.
     */
    @Test
    public void testSetLocalTAX() {
        System.out.println("setLocalTAX");
        Entradaproduto instance = new Entradaproduto();
        instance.setLocalTAX("PT");

    }

    /**
     * Test of getQuantVendida method, of class Entradaproduto.
     */
    @Test
    public void testGetQuantVendida() {
        System.out.println("getQuantVendida");
        Entradaproduto instance = new Entradaproduto();
        instance.setQuantVendida(500);
        assertEquals(500, instance.getQuantVendida());

    }

    /**
     * Test of setQuantVendida method, of class Entradaproduto.
     */
    @Test
    public void testSetQuantVendida() {
        System.out.println("setQuantVendida");
        Entradaproduto instance = new Entradaproduto();
        instance.setQuantVendida(500);

    }

    /**
     * Test of getTipoVenda method, of class Entradaproduto.
     */
    @Test
    public void testGetTipoVenda() {
        System.out.println("getTipoVenda");
        Entradaproduto instance = new Entradaproduto();
        instance.setTipoVenda("Teste");
        assertEquals("Teste", instance.getTipoVenda());

    }

    /**
     * Test of setTipoVenda method, of class Entradaproduto.
     */
    @Test
    public void testSetTipoVenda() {
        System.out.println("setTipoVenda");
        Entradaproduto instance = new Entradaproduto();
        instance.setTipoVenda("Teste");

    }

    /**
     * Test of getPesoTotalVenda method, of class Entradaproduto.
     */
    @Test
    public void testGetPesoTotalVenda() {
        System.out.println("getPesoTotalVenda");
        Entradaproduto instance = new Entradaproduto();
        instance.setPesoTotalVenda(20);
        assertEquals(20, instance.getPesoTotalVenda(), 0.0);

    }

    /**
     * Test of setPesoTotalVenda method, of class Entradaproduto.
     */
    @Test
    public void testSetPesoTotalVenda() {
        System.out.println("setPesoTotalVenda");
        Entradaproduto instance = new Entradaproduto();
        instance.setPesoTotalVenda(20);

    }

    /**
     * Test of getTipoPeso method, of class Entradaproduto.
     */
    @Test
    public void testGetTipoPeso() {
        System.out.println("getTipoPeso");
        Entradaproduto instance = new Entradaproduto();
        instance.setTipoPeso("Teste");
        assertEquals("Teste", instance.getTipoPeso());

    }

    /**
     * Test of setTipoPeso method, of class Entradaproduto.
     */
    @Test
    public void testSetTipoPeso() {
        System.out.println("setTipoPeso");
        Entradaproduto instance = new Entradaproduto();
        instance.setTipoPeso("Teste");

    }

    /**
     * Test of getQuantTotalVendaUni method, of class Entradaproduto.
     */
    @Test
    public void testGetQuantTotalVendaUni() {
        System.out.println("getQuantTotalVendaUni");
        Entradaproduto instance = new Entradaproduto();
        instance.setQuantTotalVendaUni(10000);
        assertEquals(10000, instance.getQuantTotalVendaUni());

    }

    /**
     * Test of setQuantTotalVendaUni method, of class Entradaproduto.
     */
    @Test
    public void testSetQuantTotalVendaUni() {
        System.out.println("setQuantTotalVendaUni");
        Entradaproduto instance = new Entradaproduto();
        instance.setQuantTotalVendaUni(10000);

    }

    /**
     * Test of getTipoVendaUni method, of class Entradaproduto.
     */
    @Test
    public void testGetTipoVendaUni() {
        System.out.println("getTipoVendaUni");
        Entradaproduto instance = new Entradaproduto();
        instance.setTipoVendaUni("Teste");
        assertEquals("Teste", instance.getTipoVendaUni());

    }

    /**
     * Test of setTipoVendaUni method, of class Entradaproduto.
     */
    @Test
    public void testSetTipoVendaUni() {
        System.out.println("setTipoVendaUni");
        Entradaproduto instance = new Entradaproduto();
        instance.setTipoVendaUni("Teste");

    }

    /**
     * Test of getPrecoTotalCIVA method, of class Entradaproduto.
     */
    @Test
    public void testGetPrecoTotalCIVA() {
        System.out.println("getPrecoTotalCIVA");
        Entradaproduto instance = new Entradaproduto();
        instance.setPrecoTotalCIVA(5000.96);
        assertEquals(5000.96, instance.getPrecoTotalCIVA(), 0.0);

    }

    /**
     * Test of setPrecoTotalCIVA method, of class Entradaproduto.
     */
    @Test
    public void testSetPrecoTotalCIVA() {
        System.out.println("setPrecoTotalCIVA");
        Entradaproduto instance = new Entradaproduto();
        instance.setPrecoTotalCIVA(5000.96);

    }

    /**
     * Test of getEstado method, of class Entradaproduto.
     */
    @Test
    public void testGetEstado() {
        System.out.println("getEstado");
        Entradaproduto instance = new Entradaproduto();
        instance.setEstado("Teste");
        assertEquals("Teste", instance.getEstado());

    }

    /**
     * Test of setEstado method, of class Entradaproduto.
     */
    @Test
    public void testSetEstado() {
        System.out.println("setEstado");
        Entradaproduto instance = new Entradaproduto();
        instance.setEstado("Teste");

    }

}
