/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import bfgir.BDConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe categoria com métodos de acesso a tabela de categoria na BD.
 */
public class categoria {

    private int idCategoria;
    private String nomeCategoria;
    private String descricaoCategoria;
    private int nivelCategoria;
    private String estado;

    /**
     * Construtor da classe categoria.
     */
    public categoria() {
    }

    /**
     * Construtor da classe categoria.
     *
     * @param idCategoria id da categoria
     * @param nomeCategoria nome da categoria
     * @param descricaoCategoria descricao da categoria
     * @param nivelCategoria nivel da categoria
     * @param estado estado da categoria
     */
    public categoria(int idCategoria, String nomeCategoria, String descricaoCategoria, int nivelCategoria, String estado) {
        this.idCategoria = idCategoria;
        this.nomeCategoria = nomeCategoria;
        this.descricaoCategoria = descricaoCategoria;
        this.nivelCategoria = nivelCategoria;
        this.estado = estado;
    }

    /**
     * Método get getIdCategoria.<br>
     * @return the idCategoria.
     */
    public int getIdCategoria() {
        return idCategoria;
    }

    /**
     * Método set idCategoria.<br>
     * @param idCategoria the idCategoria to set.
     */
    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }

    /**
     * Método get descricaoCategoria.<br>
     * @return the descricaoCategoria.
     */
    public String getDescricaoCategoria() {
        return descricaoCategoria;
    }

    /**
     * Método set descricaoCategoria.<br>
     * @param descricaoCategoria the descricaoCategoria to set.
     */
    public void setDescricaoCategoria(String descricaoCategoria) {
        this.descricaoCategoria = descricaoCategoria;
    }

    /**
     * Método get nivelCategoria.<br>
     * @return the nivelCategoria.
     */
    public int getNivelCategoria() {
        return nivelCategoria;
    }

    /**
     * Método set nivelCategoria.<br>
     * @param nivelCategoria the nivelCategoria to set
     */
    public void setNivelCategoria(int nivelCategoria) {
        this.nivelCategoria = nivelCategoria;
    }

    /**
     * Método get estado.<br>
     * @return the estado.
     */
    public String getEstado() {
        return estado;
    }

    /**
     * Método set estado.<br>
     * @param estado the estado to set.
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * Método get nomeCategoria.<br>
     * @return o nome da categoria.
     */
    public String getNomeCategoria() {
        return nomeCategoria;
    }

    /**
     * Método set nomeCategoria.<br>
     * @param nomeCategoria o nome da categoria to set.
     */
    public void setNomeCategoria(String nomeCategoria) {
        this.nomeCategoria = nomeCategoria;
    }

    /**
     * Método para procurar uma categoria pelo id da categoria.<br>
     *
     * @param idCategoria o ide categoria a procurar.
     * @return retorna uma lista contendo a categoria selecionada.
     */
    public static List<categoria> procurarCategoriaID(int idCategoria) {

        List<categoria> categorias = new ArrayList<categoria>();
        String sql = "SELECT * from categoria WHERE idCategoria = (?)";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setInt(1, idCategoria);

            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                categoria categoria = new categoria();
                categoria.setIdCategoria(results.getInt(1));
                categoria.setNomeCategoria(results.getString(2));
                categoria.setDescricaoCategoria(results.getString(3));
                categoria.setNivelCategoria(results.getInt(4));
                categoria.setEstado(results.getString(5));

                categorias.add(categoria);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        return categorias;
    }

    /**
     * Método para listar todoas as categorias existentes na BD ordenadas pelo
     * nome.<br>
     *
     * @return retorna uma lista contendo todos as categorias registadas.
     */
    public static List<categoria> listarCategorias() {

        List<categoria> categorias = new ArrayList<categoria>();
        String sql = "SELECT * from categoria order by idCategoria";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                categoria categoria = new categoria();
                categoria.setIdCategoria(results.getInt(1));
                categoria.setNomeCategoria(results.getString(2));
                categoria.setDescricaoCategoria(results.getString(3));
                categoria.setNivelCategoria(results.getInt(4));
                categoria.setEstado(results.getString(5));

                categorias.add(categoria);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        return categorias;
    }

    /**
     * Método para procurar uma categoria pelo seu nome.<br>
     *
     * @param nomeCategoria nome da categoria.
     * @return retorna uma lista contendo a categoria selecionada.
     */
    public static List<categoria> procurarCategoriaNome(String nomeCategoria) {

        List<categoria> categorias = new ArrayList<categoria>();
        String sql = "SELECT * from categoria WHERE nomeCategoria = (?)";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setString(1, nomeCategoria);

            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                categoria categoria = new categoria();
                categoria.setIdCategoria(results.getInt(1));
                categoria.setNomeCategoria(results.getString(2));
                categoria.setDescricaoCategoria(results.getString(3));
                categoria.setNivelCategoria(results.getInt(4));
                categoria.setEstado(results.getString(5));

                categorias.add(categoria);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        return categorias;
    }

    /**
     * Método para procurar uma categoria pelo seu id.<br>
     *
     * @param idCategoria id da categoria.
     * @return retorna uma lista contendo a categoria selecionada.
     */
    public static List<categoria> listarCategoriasPorId(int idCategoria) {

        List<categoria> categorias = new ArrayList<categoria>();
        String sql = "SELECT * from categoria WHERE idCategoria = (?)";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setInt(1, idCategoria);

            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                categoria categoria = new categoria();
                categoria.setIdCategoria(results.getInt(1));
                categoria.setNomeCategoria(results.getString(2));
                categoria.setDescricaoCategoria(results.getString(3));
                categoria.setNivelCategoria(results.getInt(4));
                categoria.setEstado(results.getString(5));

                categorias.add(categoria);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        return categorias;
    }

    /**
     * Método para procurar uma categoria pelo que é introduzido na barra de
     * pesquisa.<br>
     *
     * @param pesquisa elemento usado para a pesquisa do id da categoria.
     * @return retorna uma lista contendo a(s) categoria(s) selecionada(s).
     */
    public static List<categoria> listarCategoriasPelaPesquisa(String pesquisa) {

        List<categoria> categorias = new ArrayList<categoria>();
        String sql = "SELECT * from categoria WHERE idCategoria LIKE ?";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setString(1, "%" + pesquisa + "%");

            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                categoria categoria = new categoria();
                categoria.setIdCategoria(results.getInt(1));
                categoria.setNomeCategoria(results.getString(2));
                categoria.setDescricaoCategoria(results.getString(3));
                categoria.setNivelCategoria(results.getInt(4));
                categoria.setEstado(results.getString(5));

                categorias.add(categoria);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        return categorias;
    }

    /**
     *
     * Método usado para inserir uma categoria na BD.<br>
     *
     * O método recebe os atributos da categoria a inserir (nome, descrição,
     * nivel e estado) e insereo-os na BD.<br>
     *
     * @param nomeCategoria : nome da categoria do produto
     * @param descricaoCategoria : descrição da categoria do produto
     * @param nivelCat : nivel da categoria do produto
     * @param estado : estado da categoria (de preferencia ativo)
     *
     *
     */
    public static void inserirCategoria(String nomeCategoria, String descricaoCategoria, int nivelCat, String estado) {

        String sql = "INSERT INTO categoria (nomeCategoria,descricaoCategoria,nivelCategoria,estado) VALUES (?,?,?,?)";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, nomeCategoria);
            pstmt.setString(2, descricaoCategoria);
            pstmt.setInt(3, nivelCat);
            pstmt.setString(4, estado);

            pstmt.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

    }

    /**
     * Método para listar categorias que possuam os atributos selecionados.<br>
     * Método pensado para verificar se uma categoria foi inserida na BD.<br>
     *
     *
     * @param nomeCategoria : nome da categoria do produto.
     * @param descricaoCategoria : descrição da categoria do produto.
     * @param nivel : nivel categoria.
     * @param estado : estado do produto (de preferencia ativo).
     * @return retorna uma lista contendo todos as categorias registadas,
     * idealmente só uma.
     */
    public static List<categoria> listarCategoriaAtributos(String nomeCategoria, String descricaoCategoria, int nivel, String estado) {

        List<categoria> categorias = new ArrayList<categoria>();
        String sql = "SELECT * from categoria WHERE nomeCategoria = (?) AND descricaoCategoria = (?) "
                + "AND nivelCategoria = (?) AND estado = (?)";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, nomeCategoria);
            pstmt.setString(2, descricaoCategoria);
            pstmt.setInt(3, nivel);
            pstmt.setString(4, estado);
            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                categoria categoria = new categoria();
                categoria.setIdCategoria(results.getInt(1));
                categoria.setNomeCategoria(results.getString(2));
                categoria.setDescricaoCategoria(results.getString(3));
                categoria.setNivelCategoria(results.getInt(4));
                categoria.setEstado(results.getString(5));

                categorias.add(categoria);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        return categorias;
    }

    /**
     * Método para listar todoas as categorias ativas existentes na BD ordenadas
     * pelo nome.<br>
     *
     * @return retorna uma lista contendo todos as categorias ativas.
     */
    public static List<categoria> listarCategoriasAtivas() {

        List<categoria> categorias = new ArrayList<categoria>();
        String sql = "SELECT * from categoria WHERE estado = (?) order by nomeCategoria";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, "ativo");
            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                categoria categoria = new categoria();
                categoria.setIdCategoria(results.getInt(1));
                categoria.setNomeCategoria(results.getString(2));
                categoria.setDescricaoCategoria(results.getString(3));
                categoria.setNivelCategoria(results.getInt(4));
                categoria.setEstado(results.getString(5));

                categorias.add(categoria);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        return categorias;
    }

    /**
     * Método usado para atualizar o estado de uma Categoria na BD.<br>
     *
     * Este método recebe 2 parametros, o código da categoria que se pretende
     * alterar e o estado da categoria que se pretende atualizar. Posto isto,
     * este método faz um update do estado da categoria com o respectivo
     * idCategoria.<br>
     *
     * @param estado : estado da categoria a alterar.
     * @param idCategoria código (id) da categoria a alterar.
     */
    public static void atualizarEstadoCategoria(String estado, int idCategoria) {
        String sql = "UPDATE categoria set estado = (?) WHERE idCategoria = (?)";

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, estado);
            pstmt.setInt(2, idCategoria);

            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Método para apagar uma categoria da BD.<br>
     *
     * Este método recebe por parametro o nome da categoria Posto isto o método
     * apaga da tabela categoria a categoria que tenha aquele nome.<br>
     *
     *
     * @param id recebe o id da categoria.
     */
    public static void eliminarCategorias(int id) {
        String sql = "DELETE FROM categoria WHERE idCategoria = (?)";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setInt(1, id);

            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Método para selecionar o objeto todo da tabela produto pelo ID de
     * categoria na BD.<br>
     *
     * Este método recebe por parametro o ID da Categoria (idCategoria). Posto
     * isto faz um select à BD adicionando a um arrayList o atributo idCategoria
     * de uma Categoria com aquele ID de Categoria.<br>
     *
     * @param idCat id da categoria a pesquisar.
     * @return id da Categoria.
     *
     */
    public static List<categoria> verificarCategoriaProduto(int idCat) {

        List<categoria> categoria = new ArrayList<categoria>();
        String sql = "SELECT * FROM produto WHERE idCategoria = (?)";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setInt(1, idCat);

            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                categoria cat = new categoria();

                cat.setIdCategoria(results.getInt(1));

                categoria.add(cat);
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return categoria;
    }

    /**
     * Método usado para atualizar o nome de uma Categoria na BD.<br>
     *
     * Este método recebe 2 parametros, o código da categoria que se pretende
     * alterar e o nome da categoria que se pretende atualizar. Posto isto, este
     * método faz um update do nome da categoria com o respectivo
     * idCategoria.<br>
     *
     * Método (a ser) aplicado após uma validação para verificar se o nome é
     * único.<br>
     *
     * @param nome : nome da categoria a alterar.
     * @param idCategoria código (id) da categoria a alterar.
     */
    public static void atualizarNomeCategoria(String nome, int idCategoria) {

        String sql = "UPDATE categoria set nomeCategoria = (?) WHERE idCategoria = (?)";

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, nome);
            pstmt.setInt(2, idCategoria);

            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Método usado para atualizar a descrição de uma Categoria na BD.<br>
     *
     * Este método recebe 2 parametros, o código da categoria que se pretende
     * alterar e a descrição da categoria que se pretende atualizar. Posto isto,
     * este método faz um update do nome da categoria com o respectivo
     * idCategoria.<br>
     *
     * @param descricao : descricao da categoria a alterar.
     * @param idCategoria código (id) da categoria a alterar.
     */
    public static void atualizarDescricaoCategoria(String descricao, int idCategoria) {

        String sql = "UPDATE categoria set descricaoCategoria = (?) WHERE idCategoria = (?)";

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, descricao);
            pstmt.setInt(2, idCategoria);

            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Método usado para atualizar o nivel de uma Categoria na BD.<br>
     *
     * Este método recebe 2 parametros, o código da categoria que se pretende
     * alterar e o nivel da categoria que se pretende atualizar. Posto isto,
     * este método faz um update do nome da categoria com o respectivo
     * idCategoria.<br>
     *
     * @param nivelCategoria : nivel da categoria.
     * @param idCategoria código (id) da categoria a alterar.
     */
    public static void atualizarNivelCategoria(int nivelCategoria, int idCategoria) {

        String sql = "UPDATE categoria set nivelCategoria = (?) WHERE idCategoria = (?)";

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setInt(1, nivelCategoria);
            pstmt.setInt(2, idCategoria);

            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Método para se obter uma lista de categorias com um dado nível.<br>
     *
     * @param nivelCategoria recebe um nivel da categoria.
     * @return retorna uma lista contendo as categorias com o nível selecionado.
     */
    public static List<categoria> listarCategoriasPorNivel(int nivelCategoria) {

        List<categoria> categorias = new ArrayList<categoria>();
        String sql = "SELECT * from categoria WHERE nivelCategoria = (?)";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setInt(1, nivelCategoria);

            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                categoria categoria = new categoria();
                categoria.setIdCategoria(results.getInt(1));
                categoria.setNomeCategoria(results.getString(2));
                categoria.setDescricaoCategoria(results.getString(3));
                categoria.setNivelCategoria(results.getInt(4));
                categoria.setEstado(results.getString(5));

                categorias.add(categoria);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        return categorias;
    }

}
