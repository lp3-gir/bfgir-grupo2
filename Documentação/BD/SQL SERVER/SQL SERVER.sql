create database LP3;
GO
use lp3_grupo2;
Go

CREATE TABLE codigoPostal (
 [codPostal] varchar(255) NOT NULL,
 [cidade] varchar(255) NOT NULL,
 PRIMARY KEY ([codPostal])
)  

CREATE TABLE entrada (
 [idEntrada] varchar(255) NOT NULL,
 [idFornecedor] varchar(255) NOT NULL,
 [dataEntrada] varchar(255) NOT NULL,
 [estado] varchar(255) NOT NULL,
 PRIMARY KEY ([idEntrada])
)  

CREATE TABLE entradaproduto (
 [idEntrada] varchar(255) NOT NULL,
 [codProdPa] varchar(255) NOT NULL,
 [precoUnitSIVA] float NOT NULL,
 [tipoPrecos] varchar(255) NOT NULL,
 [precoTotalSIVA] float NOT NULL,
 [IVA] int NOT NULL,
 [totalIVA] float NOT NULL,
 [localTAX] varchar(255) NOT NULL,
 [quantVendida] bigint NOT NULL,
 [tipoVenda] varchar(255) NOT NULL,
 [pesoTotalVenda] float NOT NULL,
 [tipoPeso] varchar(255) NOT NULL,
 [quantTotalVendaUni] bigint NOT NULL,
 [tipoVendaUni] varchar(255) NOT NULL,
 [precoTotalCIVA] float NOT NULL,
 [estado] varchar(255) NOT NULL,
 PRIMARY KEY ([idEntrada],[codProdPa])
)  


CREATE TABLE fornecedor (
 [idFornecedor] varchar(255) NOT NULL,
 [idUtilizador] int NOT NULL,
 [nome] varchar(255) NOT NULL,
 [contribuinte] int NOT NULL,
 [morada] varchar(255) NOT NULL,
 [codPostal] varchar(255) NOT NULL,
 [pais] varchar(255) NOT NULL,
 [estado] varchar(255) NOT NULL,
 PRIMARY KEY ([contribuinte])
)  


CREATE TABLE produto (
 [codProdPa] varchar(255) NOT NULL,
 [descricao] varchar(255) NOT NULL,
 [quantStock] int NOT NULL,
 [tipoQuantStock] varchar(255) NOT NULL,
 [quantStockUnit] int NOT NULL,
 [tipoQuantStockUnit] varchar(255) NOT NULL,
 [precoUnitSIVA] float NOT NULL,
 [idCategoria] int NOT NULL,
 [estado] varchar(255) NOT NULL,
 PRIMARY KEY ([codProdPa])
)  


CREATE TABLE utilizador (
 [idUtilizador] int NOT NULL identity,
 [email] varchar(255) NOT NULL,
 [password] varchar(255) NOT NULL,
 [salt] varchar(255) NOT NULL,
 [nivel] int NOT NULL,
 [estado] varchar(255) NOT NULL,
 PRIMARY KEY ([idUtilizador])
)   


CREATE TABLE pesos (
 [codPeso] varchar(255) NOT NULL,
 [peso] float NOT NULL,
 [tipoPeso] varchar(255) NOT NULL,
 PRIMARY KEY ([codPeso])
) 


CREATE TABLE produtofornecedor (
 [idFornecedor] varchar(255) NOT NULL,
 [codProdForn] varchar(255) NOT NULL,
 [codProdPa] varchar(255) NOT NULL,
 PRIMARY KEY ([codProdForn],[codProdPa])
) 


--novas tabelas entrega final

CREATE TABLE cliente (
 [idCliente] int NOT NULL identity,
 [nome] varchar(255) NOT NULL,
 [email] varchar(255) NOT NULL,
 [morada] varchar(255) NOT NULL,
 [codPostal] varchar(255) NOT NULL,
 [pais] varchar(255) NOT NULL,
 [contribuinte] int NOT NULL unique,
 [estado] varchar(255) NOT NULL,
 PRIMARY KEY ([idCliente])
)


CREATE TABLE encomendasproduto (
 [idEncProd] int NOT NULL identity,
 [idEncomenda] int NOT NULL,
 [codProdPa] varchar(255) NOT NULL,
 [quantVendida] bigint NOT NULL,
 [tipoVenda] varchar(255) NOT NULL,
 [precoUnitSIVA] float NOT NULL,
 [precoUnitCIVA] float NOT NULL,
 [estado] varchar(255) NOT NULL,
 PRIMARY KEY ([idEncProd])
)

CREATE TABLE encomendas (
 [idEncomenda] int NOT NULL identity,
 [idCliente] int NOT NULL,
 [dataEncomenda] varchar(255) NOT NULL,
 [moradaEntrega] varchar(255) NOT NULL,
 [codPostalEntrega] varchar(255) NOT NULL,
 [paisEntrega] varchar(255) NOT NULL,
 [moradaFaturacao] varchar(255) NOT NULL,
 [codPostalFaturacao] varchar(255) NOT NULL,
 [paisFaturacao] varchar(255) NOT NULL,
 [precoTotalSIVA] float NOT NULL,
 [IVA] int NOT NULL,
 [totalIVA] float NOT NULL,
 [precoTotalCIVA] float NOT NULL,
 [estado] varchar(255) NOT NULL,
 PRIMARY KEY ([idEncomenda])
)  


CREATE TABLE clienteproduto (
 [idCliente] int NOT NULL,
 [codProdPa] varchar(255) NOT NULL,
 [precoUnitSIVA] float NOT NULL,
 PRIMARY KEY ([idCliente],[codProdPa])
)



CREATE TABLE categoria (
 [idCategoria] int NOT NULL identity,
 [nomeCategoria] varchar(255) NOT NULL,
 [descricaoCategoria] varchar(255) NOT NULL,
 [nivelCategoria] int NOT NULL,
 [estado] varchar(255) NOT NULL,
 PRIMARY KEY ([idCategoria])
) 

CREATE TABLE produtosubcategoria (
 [idProdSub] int NOT NULL identity,
 [codProdPa] varchar(255) NOT NULL,
 [idCatNivel1] int NOT NULL,
 [idCatNivel2] int NOT NULL,
 [idCatNivel3] int NOT NULL,
 PRIMARY KEY ([idProdSub])
) 


CREATE TABLE descontos (
 [idDesconto] int NOT NULL identity,
 [quantDesconto] float not null
 PRIMARY KEY ([idDesconto])
) 


CREATE TABLE descontosprodutos (
 [idDesconto] int NOT NULL,
 [codProdPa] varchar(255) NOT NULL,
 [quantVenda] int not null
 PRIMARY KEY ([idDesconto],[codProdPa])
) 