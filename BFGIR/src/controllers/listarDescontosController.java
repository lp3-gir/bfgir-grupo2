/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import bfgir.listarDescontos;
import bfgir.login;
import bfgir.menuAdministrador;
import bfgir.menuOperador;
import classes.desconto;
import static classes.desconto.getListaDescontos;
import static classes.desconto.getListaDescontosID;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author ruben
 */
public class listarDescontosController implements Initializable {

    @FXML
    private Pane painelListarOperadores;
    @FXML
    private ScrollPane descontosScrollPane;
    @FXML
    private VBox vBoxListarDescontos;
    @FXML
    private AnchorPane fundo;
    @FXML
    private Label ldIdDes;
    @FXML
    private TextArea idDesconto;
    @FXML
    private Label lbQuantDes;
    @FXML
    private TextArea quantDes;
    @FXML
    private MenuBar menu;

    private int altura_vbox = 0;

     /**
     * Método Intialize<br>
     *
     * Neste método criada uma lista com todas os descontos pelo método
     * getListaDescontos. De seguida para cada elemento dessa lista de descontos
     * é criado um botão com o nome da categoria. <br>
     * @param url url
     * @param rb rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO

        ldIdDes.setVisible(false);
        idDesconto.setVisible(false);
        lbQuantDes.setVisible(false);
        quantDes.setVisible(false);

        List<desconto> descontos = getListaDescontos();
        for (desconto des : descontos) {

            int id = des.getIdDesconto();
            Button botao = new Button(String.valueOf(id));
            botao.setId("botaoVbox");
            botao.setPrefHeight(30);
            botao.setPrefWidth(275);

            botao.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {
                    botao.setId("botaoVboxChangeCollour");
                    listarDescontos(id);

                }
            });

            altura_vbox = altura_vbox + 30;
            vBoxListarDescontos.getChildren().addAll(botao);
            if (altura_vbox > 550) {
                vBoxListarDescontos.setPrefHeight(altura_vbox);
            }

        }
    }

    /**
     * método para fechar o ecrã de listar os descontos
     */
    private void fecha() {
        listarDescontos.getStage().close();
    }

    /**
     * Metodo para terminar sessao do utilizador
     *
     * @param event evento
     */
    @FXML
    private void terminarSessao(ActionEvent event) {
        try {
            login.setIdLogin(0);
            login.setNivelLogin(10000);
            login log = new login();
            fecha();
            log.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(listarDescontosController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Método para aceder ao menu do administrador ou do operador
     */
    @FXML
    private void openMenu(ActionEvent event) {
        try {
            if (login.getNivelLogin() == 1) {
                menuOperador menuO = new menuOperador();
                fecha();
                menuO.start(new Stage());
            } else {
                menuAdministrador menuAdmin = new menuAdministrador();
                fecha();
                menuAdmin.start(new Stage());
            }

        } catch (Exception ex) {
            Logger.getLogger(listarDescontosController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Método para encerrar a aplicação
     */
    @FXML
    private void sair(ActionEvent event) {
        System.exit(0);

    }

    private void listarDescontos(int id) {

        ldIdDes.setVisible(true);
        idDesconto.setVisible(true);
        lbQuantDes.setVisible(true);
        quantDes.setVisible(true);

        List<desconto> desco = getListaDescontosID(id);
        for (desconto desc : desco) {

            idDesconto.setText(String.valueOf(desc.getIdDesconto()));
            quantDes.setText(String.valueOf(desc.getQuantDesconto()) + " %");

        }
    }

}
