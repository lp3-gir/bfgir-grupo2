/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import bfgir.alterarCategorias;
import bfgir.alterarDescontoAssociadoProduto;
import bfgir.alterarFornecedores;
import bfgir.alterarPrecoProdutoCliente;
import bfgir.alterarProdutos;
import bfgir.alterarSubCategoriasProduto;
import bfgir.aprovarClientes;
import bfgir.aprovarEntradaStocks;
import bfgir.associarDescontoProduto;
import bfgir.associarPrecoProdutoCliente;
import bfgir.associarSubCategoria;
import bfgir.ecraPessoalOperador;
import bfgir.eliminarCategorias;
import bfgir.eliminarDescontos;
import bfgir.eliminarDescontosProdutos;
import bfgir.eliminarFornecedores;
import bfgir.inserirCategorias;
import bfgir.inserirDesconto;
import bfgir.inserirFornecedores;
import bfgir.listarCategorias;
import bfgir.listarDescontos;
import bfgir.listarDescontosAssociadosProdutos;
import bfgir.listarFornecedores;
import bfgir.listarProdutoCliente;
import bfgir.listarStockProdutos;
import bfgir.login;
import bfgir.menuOperador;
import bfgir.movimentos;
import bfgir.movimentosProduto;
import bfgir.removerClienteAssociado;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.stage.Stage;

/**
 * FXML Controller class
 */
public class menuOperadorController implements Initializable {

    @FXML
    private Button consultarMovimentos;
    @FXML
    private Button consultarMovimentosProduto;
    @FXML
    private Button alterarProdutos;
    @FXML
    private Button alterarCategorias;
    @FXML
    private Button listarCategorias;
    @FXML
    private Button eliminarCategorias;
    @FXML
    private Button inserirCategorias;
    @FXML
    private Button removerPrecoCliente;
    @FXML
    private Button listarPreçoCliente;
    @FXML
    private Button alterarPrecoCliente;
    @FXML
    private Button associarPrecosClientes;
    @FXML
    private Button eliminarDescontos;
    @FXML
    private Button listarDescontos;
    @FXML
    private Button inserirDescontos;
    @FXML
    private Button eliminarDescontosAssociados;
    @FXML
    private Button listarDescontosAssociados;
    @FXML
    private Button alterarDescontosAssociados;
    @FXML
    private Button associarDescontos;
    @FXML
    private Button eliminarFornecedor;
    @FXML
    private Button listarFornecedores;
    @FXML
    private Button alterar_Fornecedores;
    @FXML
    private Button inserirFornecedores;
    @FXML
    private Button aprovacaoEntradaStock;
    @FXML
    private Button listarStockProdutos;
    @FXML
    private Button aprovarClientes;
    @FXML
    private Button associarSubCatProd;
    @FXML
    private Button alterarSubCatProdutos;

    /**
     * Initializes the controller class.
     *
     * @param url url
     * @param rb rb
     */
    @Override

    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    /**
     * Metodo para iniciar a pagina registoFornecedores
     *
     * @param event evento
     */
    private void registoFornecedores(ActionEvent event) {
        try {
            inserirFornecedores insFor = new inserirFornecedores();
            fecha();
            insFor.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuOperadorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para iniciar a pagina aprovarEntradaStock
     *
     * @param event evento
     */
    private void aprovarEntradaStock(ActionEvent event) {
        try {
            aprovarEntradaStocks sc = new aprovarEntradaStocks();
            fecha();
            sc.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuOperadorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para iniciar a pagina listarSockProdutos
     *
     * @param event evento
     */
    private void listarSockProdutos(ActionEvent event) {
        try {
            listarStockProdutos list = new listarStockProdutos();
            fecha();
            list.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuOperadorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para iniciar a pagina consultarMovimentos
     *
     * @param event evento
     */
    @FXML
    private void consultarMovimentos(ActionEvent event) {
        try {
            movimentos mv = new movimentos();
            fecha();
            mv.start(new Stage());

        } catch (Exception ex) {
            Logger.getLogger(menuOperadorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para terminar sessao do utilizador
     *
     * @param event evento
     */
    @FXML
    private void terminarSessao(ActionEvent event) {
        try {
            login.setIdLogin(0);
            login.setNivelLogin(10000);
            login log = new login();
            fecha();
            log.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuOperadorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * fecha
     */
    public void fecha() {
        menuOperador.getStage().close();
    }

    /**
     * Método para encerrar a aplicação
     */
    @FXML
    private void sair(ActionEvent event) {
        System.exit(0);
    }

    /**
     * Metodo para iniciar a pagina consultarMovimentosProduto
     *
     * @param event evento
     */
    @FXML
    private void consultarMovimentosProduto(ActionEvent event) {
        try {
            movimentosProduto mvp = new movimentosProduto();
            fecha();
            mvp.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuOperadorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para iniciar a pagina ecraPessoalOperador
     *
     * @param event evento
     */
    @FXML
    private void ecraPessoalOperador(ActionEvent event) {
        try {
            ecraPessoalOperador ePOp = new ecraPessoalOperador();
            fecha();
            ePOp.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuOperadorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para iniciar a pagina de alterar produtos
     *
     * @param event evento
     */
    @FXML
    private void alterarProdutos(ActionEvent event) {
        try {
            alterarProdutos altProd = new alterarProdutos();
            fecha();
            altProd.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuOperadorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para iniciar a pagina de alterarCategorias
     *
     * @param event evento
     */
    @FXML
    private void alterarCategorias(ActionEvent event) {
        try {
            alterarCategorias altCat = new alterarCategorias();
            fecha();
            altCat.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuOperadorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para iniciar a pagina de listarCategorias
     *
     * @param event evento
     */
    @FXML
    private void listarCategorias(ActionEvent event) {
        try {
            listarCategorias listCat = new listarCategorias();
            fecha();
            listCat.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuOperadorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para iniciar a pagina de eliminarCategorias
     *
     * @param event evento
     */
    @FXML
    private void eliminarCategorias(ActionEvent event) {
        try {
            eliminarCategorias eliCat = new eliminarCategorias();
            fecha();
            eliCat.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuOperadorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
        
    /**
     * Metodo para iniciar a pagina de inserirCategorias
     *
     * @param event evento
     */
    @FXML
    private void inserirCategorias(ActionEvent event) {
        try {
            inserirCategorias insCat = new inserirCategorias();
            fecha();
            insCat.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuOperadorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para iniciar a pagina de removerPrecoCliente
     *
     * @param event evento
     */
    @FXML
    private void removerPrecoCliente(ActionEvent event) {
        try {
            removerClienteAssociado remvCli = new removerClienteAssociado();
            fecha();
            remvCli.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuOperadorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para iniciar a pagina de listarPrecoCliente
     *
     * @param event evento
     */
    @FXML
    private void listarPrecoCliente(ActionEvent event) {
        try {
            listarProdutoCliente lc = new listarProdutoCliente();
            fecha();
            lc.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuOperadorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para iniciar a pagina de alterarPrecoCliente
     *
     * @param event evento
     */
    @FXML
    private void alterarPrecoCliente(ActionEvent event) {
        try {
            alterarPrecoProdutoCliente altPPC = new alterarPrecoProdutoCliente();
            fecha();
            altPPC.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuOperadorController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Metodo para iniciar a pagina de aprovarClientes
     *
     * @param event evento
     */
    @FXML
    private void aprovarClientes(ActionEvent event) {
        try {
            aprovarClientes AC = new aprovarClientes();
            fecha();
            AC.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuOperadorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para iniciar a pagina de associarPrecosClientes
     *
     * @param event evento
     */
    @FXML
    private void associarPrecosClientes(ActionEvent event) {
        try {
            associarPrecoProdutoCliente ap = new associarPrecoProdutoCliente();
            fecha();
            ap.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuOperadorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para iniciar a pagina eliminarFornecedor
     *
     * @param event evento
     */
    @FXML
    private void eliminarFornecedor(ActionEvent event) {
        try {
            eliminarFornecedores ef = new eliminarFornecedores();
            fecha();
            ef.start(new Stage());

        } catch (Exception ex) {
            Logger.getLogger(menuOperadorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para iniciar a pagina listarFornecedores
     *
     * @param event evento
     */
    @FXML
    private void listarFornecedores(ActionEvent event) {
        try {
            listarFornecedores lf = new listarFornecedores();
            fecha();
            lf.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuOperadorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para iniciar a pagina alterar_Fornecedores
     *
     * @param event evento
     */
    @FXML
    private void alterar_Fornecedores(ActionEvent event) {
        try {
            alterarFornecedores af = new alterarFornecedores();
            fecha();
            af.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuOperadorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para iniciar a pagina inserirFornecedores
     *
     * @param event evento
     */
    @FXML
    private void inserirFornecedores(ActionEvent event) {
        try {
            inserirFornecedores insFor = new inserirFornecedores();
            fecha();
            insFor.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuAdministradorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para iniciar a pagina aprovacaoEntradaStock
     *
     * @param event evento
     */
    @FXML
    private void aprovacaoEntradaStock(ActionEvent event) {
        try {
            aprovarEntradaStocks sc = new aprovarEntradaStocks();
            fecha();
            sc.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuOperadorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para iniciar a pagina listarStockProdutos
     *
     * @param event evento
     */
    @FXML
    private void listarStockProdutos(ActionEvent event) {
        try {
            listarStockProdutos list = new listarStockProdutos();
            fecha();
            list.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuOperadorController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Metodo para iniciar a pagina de eliminar descontos
     *
     * @param event evento
     */
    @FXML
    private void eliminarDescontos(ActionEvent event) {
        try {
            eliminarDescontos eliDes = new eliminarDescontos();
            fecha();
            eliDes.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuOperadorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para iniciar a pagina de listar descontos
     *
     * @param event evento
     */
    @FXML
    private void listarDescontos(ActionEvent event) {
        try {
            listarDescontos ldes = new listarDescontos();
            fecha();
            ldes.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuOperadorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para iniciar a pagina de inserir descontos
     *
     * @param event evento
     */
    @FXML
    private void inserirDescontos(ActionEvent event) {
        try {
            inserirDesconto insdes = new inserirDesconto();
            fecha();
            insdes.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuOperadorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para iniciar a pagina de eliminarDescontosAssociados
     *
     * @param event evento
     */
    @FXML
    private void eliminarDescontosAssociados(ActionEvent event) {

        try {
            eliminarDescontosProdutos ep = new eliminarDescontosProdutos();
            fecha();
            ep.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuOperadorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void listarDescontosAssociados(ActionEvent event) {
        try {
            listarDescontosAssociadosProdutos listPA = new listarDescontosAssociadosProdutos();
            fecha();
            listPA.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(listarDescontosAssociadosProdutosController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void alterarDescontosAssociados(ActionEvent event) {
        try {
            alterarDescontoAssociadoProduto altDescP = new alterarDescontoAssociadoProduto();
            fecha();
            altDescP.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(alterarDescontoAssociadoProdutoController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para iniciar a pagina de associarDescontos
     *
     * @param event evento
     */
    @FXML
    private void associarDescontos(ActionEvent event) {
        try {
            associarDescontoProduto ap = new associarDescontoProduto();
            fecha();
            ap.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuOperadorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para iniciar a pagina de alterarSubCatProdutos
     *
     * @param event evento
     */
    @FXML
    private void alterarSubCatProdutos(ActionEvent event) {
        try {
            alterarSubCategoriasProduto altSCP = new alterarSubCategoriasProduto();
            fecha();
            altSCP.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuOperadorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para iniciar a pagina de associarSubCatProd
     *
     * @param event evento
     */
    @FXML
    private void associarSubCatProd(ActionEvent event) {
        try {
            associarSubCategoria as = new associarSubCategoria();
            fecha();
            as.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuOperadorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
