/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.service;

import entities.Categoria;
import entities.Produto;
import entities.Produtosubcategoria;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author ruben
 */
@Stateless
@Path("/product/")
public class TreeFacadeREST {

    @PersistenceContext(unitName = "feiraofficePU")
    private EntityManager em;

    /**
     *
     * @param id recebe um id de categoria
     * @return uma string em formato XML
     */
    @GET
    @Path("/{id}/tree/")
    @Consumes({MediaType.APPLICATION_XML})
    @Produces("application/xml")
    public String getTreeXML(@PathParam("id") int id) {

        List<Categoria> listCat = new ArrayList<>();

        int idCat = id;
        Categoria Categ = new Categoria();

        String dadosAListar = "";

        Query query = getEntityManager().createNamedQuery("Categoria.findByIdCategoria");
        query.setParameter("idCategoria", idCat);
        List<Categoria> Categorias = query.getResultList();
        for (Categoria cats : Categorias) {
            Categ.setNivelCategoria(cats.getNivelCategoria());
        }

        if (Categ.getNivelCategoria() == 1) {
            query = getEntityManager().createNamedQuery("Produtosubcategoria.findByIdCatNivel1");
            query.setParameter("idCatNivel1", idCat);
            dadosAListar = getProdsCatXML(query, 1);

        } else if (Categ.getNivelCategoria() == 2) {
            query = getEntityManager().createNamedQuery("Produtosubcategoria.findByIdCatNivel2");
            query.setParameter("idCatNivel2", idCat);
            dadosAListar = getProdsCatXML(query, 2);

        } else if (Categ.getNivelCategoria() == 3) {
            query = getEntityManager().createNamedQuery("Produtosubcategoria.findByIdCatNivel3");
            query.setParameter("idCatNivel3", idCat);
            dadosAListar = getProdsCatXML(query, 3);

        }

        return dadosAListar;
    }

    /**
     *
     * @param id recebe um id de categoria
     * @return uma string em formato JSON
     */
    @GET
    @Path("/{id}/tree/")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces("application/json")
    public String getTreeJSON(@PathParam("id") int id) {

        List<Categoria> listCat = new ArrayList<Categoria>();

        int idCat = id;
        Categoria Categ = new Categoria();

        String dadosAListar = "";

        Query query = getEntityManager().createNamedQuery("Categoria.findByIdCategoria");
        query.setParameter("idCategoria", idCat);
        List<Categoria> Categorias = query.getResultList();
        for (Categoria cats : Categorias) {
            Categ.setNivelCategoria(cats.getNivelCategoria());
        }

        if (Categ.getNivelCategoria() == 1) {
            query = getEntityManager().createNamedQuery("Produtosubcategoria.findByIdCatNivel1");
            query.setParameter("idCatNivel1", idCat);
            dadosAListar = getProdsCatJSON(query, 1);

        } else if (Categ.getNivelCategoria() == 2) {
            query = getEntityManager().createNamedQuery("Produtosubcategoria.findByIdCatNivel2");
            query.setParameter("idCatNivel2", idCat);
            dadosAListar = getProdsCatJSON(query, 2);

        } else if (Categ.getNivelCategoria() == 3) {
            query = getEntityManager().createNamedQuery("Produtosubcategoria.findByIdCatNivel3");
            query.setParameter("idCatNivel3", idCat);
            dadosAListar = getProdsCatJSON(query, 3);

        }

        return dadosAListar;
    }

    private String getProdsCatXML(Query query, int nivel) {

        String dadosProd = "";
        String dadosAListar = "";
        String dadosCat = "";

        List<Produtosubcategoria> prodSubCat = query.getResultList();
        for (Produtosubcategoria psc : prodSubCat) {

            if (nivel == 1) {
                dadosCat = getCatsXML(psc.getIdCatNivel1());
            } else if (nivel == 2) {
                dadosCat = getCatsXML(psc.getIdCatNivel1());
                dadosCat += getCatsXML(psc.getIdCatNivel2());
            } else if (nivel == 3) {
                dadosCat = getCatsXML(psc.getIdCatNivel1());
                dadosCat += getCatsXML(psc.getIdCatNivel2());
                dadosCat += getCatsXML(psc.getIdCatNivel3());
            }

            query = getEntityManager().createNamedQuery("Produto.findByCodProdPa");
            query.setParameter("codProdPa", psc.getCodProdPa());
            List<Produto> produtos = query.getResultList();
            for (Produto prods : produtos) {

                dadosProd = ""
                        + "<produtos>\n"
                        + "    <produto>\n"
                        + "        <codProdPa>" + prods.getCodProdPa() + "</codProdPa>"
                        + "        <descricao>" + prods.getDescricao() + "</descricao>"
                        + "        <estado>" + prods.getEstado() + "</estado>"
                        + "        <precoUnitSIVA>" + (Math.round(prods.getPrecoUnitSIVA() * 100.0) / 100.0) + "</precoUnitSIVA>"
                        + "        <quantStock>" + prods.getQuantStock() + "</quantStock>"
                        + "        <quantStockUnit>" + prods.getQuantStockUnit() + "</quantStockUnit>"
                        + "        <tipoQuantStock>" + prods.getTipoQuantStock() + "</tipoQuantStock>"
                        + "        <tipoQuantStockUnit>" + prods.getTipoQuantStockUnit() + "</tipoQuantStockUnit>"
                        + "        <categorias>"
                        + dadosCat
                        + "        </categorias>"
                        + "    </produto>"
                        + "</produtos>\n";
            }
            dadosAListar += dadosProd;
        }
        return dadosAListar;
    }

    private String getProdsCatJSON(Query query, int nivel) {

        int i = 0;
        String dadosProd = "[";
        String dadosAListar = "";
        String dadosCat = "";

        List<Produtosubcategoria> prodSubCat = query.getResultList();
        for (Produtosubcategoria psc : prodSubCat) {

            if (nivel == 1) {
                dadosCat = getCatsJSON(psc.getIdCatNivel1());
            } else if (nivel == 2) {
                dadosCat = getCatsJSON(psc.getIdCatNivel1()) + ",";
                dadosCat += getCatsJSON(psc.getIdCatNivel2());
            } else if (nivel == 3) {
                dadosCat = getCatsJSON(psc.getIdCatNivel1()) + ",";
                dadosCat += getCatsJSON(psc.getIdCatNivel2()) + ",";
                dadosCat += getCatsJSON(psc.getIdCatNivel3());
            }

            query = getEntityManager().createNamedQuery("Produto.findByCodProdPa");
            query.setParameter("codProdPa", psc.getCodProdPa());
            List<Produto> produtos = query.getResultList();
            for (Produto prods : produtos) {
                dadosProd += ""
                        + "    {\n"
                        + "        \"codProdPa\": \"" + prods.getCodProdPa() + "\","
                        + "        \"descricao\": \"" + prods.getDescricao() + "\","
                        + "        \"quantStock\": " + prods.getQuantStock() + ","
                        + "        \"tipoQuantStock\": \"" + prods.getTipoQuantStock() + "\","
                        + "        \"quantStockUnit\": " + prods.getQuantStockUnit() + ","
                        + "        \"tipoQuantStockUnit\": \"" + prods.getTipoQuantStockUnit() + "\","
                        + "        \"precoUnitSIVA\": " + (Math.round(prods.getPrecoUnitSIVA() * 100.0) / 100.0) + ","
                        + "        \"estado\": \"" + prods.getEstado() + "\","
                        + "        \"categorias\": \n"
                        + "        [{"
                        + dadosCat
                        + "\n"
                        + "        }]"
                        + "    }";

            }
            i++;
            if (prodSubCat.size() == i) {
                dadosProd += "";
            } else {
                dadosProd += ",";
            }
        }
        dadosAListar = dadosProd + "]";
        return dadosAListar;
    }

    private String getCatsXML(int idCat) {

        String dadosCat = "";
        Query query = getEntityManager().createNamedQuery("Categoria.findByIdCategoria");
        query.setParameter("idCategoria", idCat);
        List<Categoria> Categorias = query.getResultList();
        for (Categoria cats : Categorias) {
            dadosCat = ""
                    + "            <categoria>"
                    + "                <idCategoria>" + cats.getIdCategoria() + "</idCategoria>"
                    + "                <nivelCategoria>" + cats.getNivelCategoria() + "</nivelCategoria>"
                    + "                <nomeCategoria>" + cats.getNomeCategoria() + "</nomeCategoria>"
                    + "            </categoria>";
        }
        return dadosCat;
    }

    private String getCatsJSON(int idCat) {
        String dadosCat = "";
        Query query = getEntityManager().createNamedQuery("Categoria.findByIdCategoria");
        query.setParameter("idCategoria", idCat);
        List<Categoria> Categorias = query.getResultList();
        for (Categoria cats : Categorias) {
            dadosCat = ""
                    + "            \"categoria\": "
                    + "            {"
                    + "                \"idCategoria\": " + cats.getIdCategoria() + ","
                    + "                \"nivelCategoria\": " + cats.getNivelCategoria() + ","
                    + "                \"nomeCategoria\": \"" + cats.getNomeCategoria() + "\","
                    + "            }";

        }
        return dadosCat;
    }

    /**
     *
     * @return a entiade
     */
    protected EntityManager getEntityManager() {
        return em;
    }
}
