package controllers;

import bfgir.ecraPessoalOperador;
import bfgir.login;
import bfgir.menuAdministrador;
import bfgir.menuOperador;
import bfgir.alterarDescontoAssociadoProduto;
import classes.desconto;
import static classes.desconto.getListaDescontosID;
import classes.descontosProduto;
import static classes.descontosProduto.alterarDescontoProduto;
import static classes.descontosProduto.getListaDescontosPordutosCodProd;
import static classes.descontosProduto.getListaDescontosPordutosCodProdDistinct;
import static classes.descontosProduto.getListaDescontosPordutosDistinct;
import static classes.descontosProduto.getListaDescontosPordutosIDDesCod;
import classes.produto;
import static classes.produto.pesquisarProdutoID;
import classes.utilizador;
import static classes.utilizador.procurarEmail;
import static controllers.inserirDescontosController.a;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *Classe controladora do ecrã alterarFornecedores.<br>
 * @author chico
 */
public class alterarDescontoAssociadoProdutoController implements Initializable {

    private int altura_vbox = 0;
    private float desct;
    private String codPP;
    private int qtVenda;
    private String codigoProd;
    private int idDesc;
    private int idDescD;

    @FXML
    private VBox vBoxProdutos;

    @FXML
    private AnchorPane painelDescontos;

    @FXML
    private ScrollPane descontosScrollPane;

    @FXML
    private VBox vBoxDesconto;

    @FXML
    private Label lbDescricao;

    @FXML
    private TextArea descricao;

    @FXML
    private Label lbID;

    @FXML
    private TextArea ID;

    @FXML
    private Label lbDesconto;

    @FXML
    private Label lbQuantVenda;

    @FXML
    private TextArea txtQuantVenda;

    @FXML
    private Button btnPesquisa;

    @FXML
    private TextField pesquisa;

    @FXML
    private Button btnPesquisa2;

    @FXML
    private TextField pesquisa2;

    @FXML
    private MenuBar menu;

    @FXML
    private Menu opcoes;

    @FXML
    private Button btnAlterarDesconto;
    @FXML
    private AnchorPane painelProdutos;
    @FXML
    private ScrollPane produtosScrollPane;
    @FXML
    private TextArea descontoTF;

    /**
     * Método Intialize
     *
     * Neste método criada uma lista com todas as categorias obtidas pelo método
     * alterarDescontoAssociadoProduto. De seguida para cada elemento dessa
     * lista de categorias é criado um botão com o nome da categoria. Depois de
     * selecionar uma categoria é feito um select pelo ID da categoria do mesmo,
     * através do método listarCategoriasPorId que retorna a categoria
     * correspondente ao id da categoria. Os atributos da categoria são depois
     * apresentados no ecrã.<br>
     * Método também inicia ocultando os elementos de uma categoria e só as
     * mostra após clicar num botão com o id da mesma. <br>
     * Aquando do arraque, o método também chama o método para refazer o menu de
     * opções consoante o utilizador seja operador ou não para lhe permitir
     * acesso ao menu pessoal.<br>
     *
     * @param url url.
     * @param rb rb.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            adicionarEcraPessoalOperadorMenuOpcoes();
            setVisibleFalse();

            List<descontosProduto> descProdutos = getListaDescontosPordutosDistinct();
            for (descontosProduto prod : descProdutos) {
                String idP = prod.getCodProdPa();
                codPP = idP;
                Button botao = new Button(idP);
                botao.setId("botaoVbox");
                botao.setPrefHeight(30);
                botao.setPrefWidth(vBoxProdutos.getPrefWidth());
                botao.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent e) {
                        botao.setId("botaoVboxChangeCollour");
                        setVisibleTrueTabela();
                        listarTabelaDesconto(idP);

                    }
                });

                altura_vbox = altura_vbox + 30;
                vBoxProdutos.getChildren().addAll(botao);
                if (altura_vbox > 468) {
                    vBoxProdutos.setPrefHeight(altura_vbox);
                }

            }
        } catch (Exception ab) {
            mensagemErroFatal();
        }

    }

    /**
     * Método para listar os descontos associados a um produto.<br>
     * Método recebe o código de um produto e gera uma lista de botões
     * correspondentes aos descontos associados ao produto.<br>
     * Ao clicar no botão de um desconto, é apresentado o id e e a descrição do
     * produto mais os dados do desconto: o número de unidades vendidas a partir
     * do qual o desconto se aplica e a % de desconto.<br>
     *
     * @param codigProd : código do produto com descontos associados.
     */
    private void listarTabelaDesconto(String codigProd) {

        codigoProd = codigProd;
        altura_vbox = 0;

        vBoxDesconto.getChildren().removeAll(vBoxDesconto.getChildren());
        List<descontosProduto> descProd = getListaDescontosPordutosCodProd(codigProd);
        for (descontosProduto dtP : descProd) {

            List<desconto> d = getListaDescontosID(dtP.getIdDesconto());
            for (desconto des : d) {
                int id = des.getIdDesconto();
                String nome = String.valueOf(id) + " - " + String.valueOf(des.getQuantDesconto() + " %");
                Button botao = new Button(nome);
                botao.setId("botaoVbox");
                botao.setPrefHeight(30);
                botao.setPrefWidth(275);
                botao.setPrefWidth(vBoxDesconto.getPrefWidth());
                botao.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent e) {
                        altura_vbox = 0;
                        botao.setId("botaoVboxChangeCollour");
                        setVisibleTrue();

                        List<desconto> desc = desconto.getListaDescontosID(id);
                        desconto dt = desc.get(0);
                        desct = dt.getQuantDesconto();

                        List<descontosProduto> descProdQt = getListaDescontosPordutosIDDesCod(id, codigProd);
                        for (descontosProduto qtP : descProdQt) {
                            qtVenda = qtP.getQuantVend();
                            idDescD = qtP.getIdDesconto();

                            List<produto> produtos = pesquisarProdutoID(codigProd);
                            for (produto prod : produtos) {
                                ID.setText(codigProd);
                                descricao.setText(prod.getDescricao());
                                txtQuantVenda.setText(String.valueOf(qtVenda));
                                descontoTF.setText(String.valueOf(desct) + " %");
                                idDesc = id;
                            }
                        }

                    }
                });
                altura_vbox = altura_vbox + 30;
                vBoxDesconto.getChildren().addAll(botao);
                if (altura_vbox > 468) {
                    vBoxDesconto.setPrefHeight(altura_vbox);
                }

            }
        }
    }

    /**
     * Método para tornar visivel todos os atributos do produto e do desconto
     * selecionado e torna não editável e selecionável a quantidade de unidades
     * de produto vendidas e a % de desconto.<br>
     */
    private void setVisibleTrue() {

        lbID.setVisible(true);
        lbDescricao.setVisible(true);
        lbQuantVenda.setVisible(true);
        lbDesconto.setVisible(true);

        ID.setVisible(true);
        descricao.setVisible(true);
        txtQuantVenda.setVisible(true);
        descontoTF.setVisible(true);

        ID.setEditable(false);
        descricao.setEditable(false);
        txtQuantVenda.setEditable(true);

        btnAlterarDesconto.setVisible(true);

    }

    /**
     * Método para a lista de descontos de produtos e ferramentas de pesquisa
     * associadas.<br>
     */
    private void setVisibleTrueTabela() {

        painelDescontos.setVisible(true);
        descontosScrollPane.setVisible(true);
        vBoxDesconto.setVisible(true);

        pesquisa2.setVisible(true);
        btnPesquisa2.setVisible(true);

    }

    /**
     * Método para ocultar todos os elementos do ecrã à excepção da lista de
     * produtos, dos menus e das ferramentas de pesquisa de produtos.<br>
     */
    private void setVisibleFalse() {

        lbID.setVisible(false);
        lbDescricao.setVisible(false);
        lbQuantVenda.setVisible(false);
        lbDesconto.setVisible(false);

        ID.setVisible(false);
        descricao.setVisible(false);
        txtQuantVenda.setVisible(false);
        descontoTF.setVisible(false);

        painelDescontos.setVisible(false);
        descontosScrollPane.setVisible(false);
        vBoxDesconto.setVisible(false);

        btnAlterarDesconto.setVisible(false);
        pesquisa2.setVisible(false);
        btnPesquisa2.setVisible(false);

    }

    /**
     * Método para filtrar a lista de produtos pelo identificador do
     * produto.<br>
     * Método procura por um produto com descontos associados na base de dados e
     * gera un botão com o seu identificador no ecrã.<br>
     * Caso não consiga encontrar um produto, informa o utilizador desse
     * facto.<br>
     */
    @FXML
    private void pesquisarID(ActionEvent event) throws Exception {

        altura_vbox = 0;

        if (getListaDescontosPordutosCodProd(String.valueOf(pesquisa.getText())).isEmpty() == false) {

            try {

                vBoxProdutos.getChildren().clear();

                setVisibleFalse();

                List<descontosProduto> descProdutos = getListaDescontosPordutosCodProdDistinct(String.valueOf(pesquisa.getText()));
                for (descontosProduto prod : descProdutos) {
                    String idP = prod.getCodProdPa();
                    codPP = idP;
                    Button botao = new Button(idP);
                    botao.setId("botaoVbox");
                    botao.setPrefHeight(30);
                    botao.setPrefWidth(vBoxProdutos.getPrefWidth());
                    botao.setOnAction(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent e) {
                            botao.setId("botaoVboxChangeCollour");
                            setVisibleTrueTabela();
                            listarTabelaDesconto(idP);

                        }
                    });

                    altura_vbox = altura_vbox + 30;
                    vBoxProdutos.getChildren().addAll(botao);
                    if (altura_vbox > 468) {
                        vBoxProdutos.setPrefHeight(altura_vbox);
                    }

                }

            } catch (Exception ab) {
                mensagemErroFatal();
            }

        } else {

            a.setAlertType(Alert.AlertType.INFORMATION);
            a.getButtonTypes().setAll(ButtonType.OK);
            a.setTitle("codigo incorreto ");
            a.setHeaderText("O codigo de produto pode estar incorreto!");
            a.showAndWait();
            fecha();
            alterarDescontoAssociadoProduto altDescP = new alterarDescontoAssociadoProduto();
            altDescP.start(new Stage());
        }
    }

    /**
     * Método para filtrar a lista de descontos associados ao produto pelo
     * identificador do desconto.<br>
     * Método procura pelo desconto associado ao produto na base de dados e gera
     * un botão com o identificador e a % de desconto no ecrã.<br>
     * Caso não consiga encontrar um produto, informa o utilizador desse
     * facto.<br>
     * Ao clicar no botão de um desconto, é apresentado o id e e a descrição do
     * produto mais os dados do desconto: o número de unidades vendidas a partir
     * do qual o desconto se aplica e a % de desconto.<br>
     */
    @FXML
    private void pesquisarIDdesc(ActionEvent event) throws Exception {

        altura_vbox = 0;

        vBoxDesconto.getChildren().clear();
        vBoxDesconto.getChildren().removeAll(vBoxDesconto.getChildren());
        vBoxDesconto.getChildren().removeAll();

        int resulPesq = 0;
        try {

            resulPesq = Integer.parseInt(pesquisa2.getText());

            if ((getListaDescontosID(resulPesq)).isEmpty() == false) {

                try {

                    vBoxDesconto.getChildren().removeAll(vBoxDesconto.getChildren());

                    List<desconto> d = getListaDescontosID(resulPesq);
                    for (desconto des : d) {

                        int id = des.getIdDesconto();
                        String nome = String.valueOf(id) + " - " + String.valueOf(des.getQuantDesconto() + " %");
                        Button botao = new Button(nome);
                        botao.setId("botaoVbox");
                        botao.setPrefHeight(30);
                        botao.setPrefWidth(275);
                        botao.setPrefWidth(vBoxDesconto.getPrefWidth());
                        botao.setOnAction(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent e) {

                                botao.setId("botaoVboxChangeCollour");
                                setVisibleTrue();

                                List<descontosProduto> descProd = descontosProduto.getListaDescontosPordutosCodProd(codPP);
                                descontosProduto descP = descProd.get(0);
                                int idDesc = descP.getIdDesconto();

                                List<desconto> desc = desconto.getListaDescontosID(idDesc);
                                desconto dt = desc.get(0);
                                desct = dt.getQuantDesconto();

                                List<descontosProduto> descProdQt = getListaDescontosPordutosIDDesCod(id, codigoProd);
                                for (descontosProduto qtP : descProdQt) {
                                    qtVenda = qtP.getQuantVend();

                                    List<produto> produtos = pesquisarProdutoID(codigoProd);
                                    for (produto prod : produtos) {
                                        ID.setText(codigoProd);
                                        descricao.setText(prod.getDescricao());
                                        txtQuantVenda.setText(String.valueOf(qtVenda));
                                        descontoTF.setText(String.valueOf(desct) + " %");
                                    }
                                }
                            }
                        });

                        altura_vbox = altura_vbox + 30;
                        vBoxDesconto.getChildren().addAll(botao);
                        if (altura_vbox > 468) {
                            vBoxDesconto.setPrefHeight(altura_vbox);
                        }

                    }
                } catch (Exception ab) {
                    mensagemErroFatal();
                }

            } else {

                a.setAlertType(Alert.AlertType.INFORMATION);
                a.getButtonTypes().setAll(ButtonType.OK);
                a.setTitle("codigo incorreto ");
                a.setHeaderText("O codigo de desconto pode estar incorreto!");
                a.showAndWait();
                fecha();
                alterarDescontoAssociadoProduto altDescP = new alterarDescontoAssociadoProduto();
                altDescP.start(new Stage());
            }

        } catch (Exception ab) {
            a.setAlertType(Alert.AlertType.INFORMATION);
            a.getButtonTypes().setAll(ButtonType.OK);
            a.setTitle("codigo incorreto ");
            a.setHeaderText("O codigo de desconto pode estar incorreto!");
            Optional<ButtonType> result2 = a.showAndWait();
            fecha();
            alterarDescontoAssociadoProduto altDescP = new alterarDescontoAssociadoProduto();
            altDescP.start(new Stage());
        }

    }

    /**
     * Método para alterar a quantidade mínima de produtos vendidos a partir da
     * qual se aplica um dado produto.<br>
     * Método verifica se o registo ainda existe antes de o poder alterar e
     * valida a quantidade de produtos inserida antes de proceder à sua
     * alteração.<br>
     * Por fim, reinicia o ecrã.<br>
     */
    @FXML
    void alterarDesconto(ActionEvent event) {

        a.setAlertType(Alert.AlertType.CONFIRMATION);
        a.setTitle("Desconto Produto");
        a.setHeaderText("Deseja mesmo atualizar o Desconto neste produto?");
        a.setResizable(false);
        a.getButtonTypes().setAll(ButtonType.YES, ButtonType.NO);
        Optional<ButtonType> result = a.showAndWait();

        if (result.get() == ButtonType.YES) {

            Alert a2 = new Alert(Alert.AlertType.NONE);

            //verificar se o desconto ainda existe na BD
            List<desconto> descontos = getListaDescontosID(idDesc);

            int count = 0;
            for (desconto desc : descontos) {
                count = 1;
            }
            //notificação se o desconto não existir na BD
            if (count == 0) {
                a.setAlertType(Alert.AlertType.INFORMATION);
                a.setTitle("Desconto Produto");
                a.setHeaderText("O Desconto que estava a alterar foi removido!");
                a.setContentText("O Desconto foi eliminado enquanto o alterava.");
                a.setResizable(false);
                Optional<ButtonType> result2 = a.showAndWait();
                alterarDescontoAssociadoProduto altDescProd = new alterarDescontoAssociadoProduto();
                fecha();
                try {
                    altDescProd.start(new Stage());
                } catch (Exception ex) {
                    a.setAlertType(Alert.AlertType.ERROR);
                    a.setTitle("ERRO Fatal");
                    a.setHeaderText("A aplicação está corrompida!");
                    a.setContentText("Algumas funcionalidades do aplicação estão inacessíveis. Tente uma reinstalação.");
                    Optional<ButtonType> result3 = a.showAndWait();
                    Logger.getLogger(alterarDescontoAssociadoProdutoController.class.getName()).log(Level.SEVERE, null, ex);
                }
                // verificação se os campos do desconto n estao nulos, se sim, notificação 
            } else if (txtQuantVenda.equals("") || txtQuantVenda == null) {

                a.setAlertType(Alert.AlertType.INFORMATION);
                a.setTitle("Desconto Produtos");
                a.setHeaderText("Alterações com campos nulos.");
                a.setContentText("O desconto não pode ter campos NULOS!");
                a.setResizable(false);
                Optional<ButtonType> result2 = a.showAndWait();
                alterarDescontoAssociadoProduto altDescProd = new alterarDescontoAssociadoProduto();
                fecha();
                try {
                    altDescProd.start(new Stage());
                } catch (Exception ex) {
                    a.setAlertType(Alert.AlertType.ERROR);
                    a.setTitle("ERRO Fatal");
                    a.setHeaderText("A aplicação está corrompida!");
                    a.setContentText("Algumas funcionalidades do aplicação estão inacessíveis. Tente uma reinstalação.");
                    Optional<ButtonType> result3 = a.showAndWait();
                    Logger.getLogger(alterarDescontoAssociadoProdutoController.class.getName()).log(Level.SEVERE, null, ex);
                }
                // verificação e notificação se os campos do desconto são validos
            } else if (isValidQuantidadeVenda(txtQuantVenda.getText()) == false) {

                a.setAlertType(Alert.AlertType.INFORMATION);
                a.setTitle("Desconto");
                a.setHeaderText("Quantidade de venda minima.");
                a.setContentText("O campo da quantidade de venda esta invalida!");
                a.setResizable(false);
                Optional<ButtonType> result2 = a.showAndWait();
                alterarDescontoAssociadoProduto altDescProd = new alterarDescontoAssociadoProduto();
                fecha();
                try {
                    altDescProd.start(new Stage());
                } catch (Exception ex) {
                    a.setAlertType(Alert.AlertType.ERROR);
                    a.setTitle("ERRO Fatal");
                    a.setHeaderText("A aplicação está corrompida!");
                    a.setContentText("Algumas funcionalidades do aplicação estão inacessíveis. Tente uma reinstalação.");
                    Optional<ButtonType> result3 = a.showAndWait();
                    Logger.getLogger(alterarDescontoAssociadoProdutoController.class.getName()).log(Level.SEVERE, null, ex);
                }

            } else {

                alterarDescontoProduto(Integer.parseInt(txtQuantVenda.getText()), idDescD, codigoProd);

                a2.setAlertType(Alert.AlertType.INFORMATION);
                //notificação de alteração de que o desconto foi alterado com sucesso
                a2.setTitle("Desconto Produto");
                a2.setHeaderText("O Desconto foi alterado com sucesso.");
                a2.setContentText("As alterações feitas ao Desconto do produto foram registadas com sucesso!");
                a2.getButtonTypes().setAll(ButtonType.OK);
                Optional<ButtonType> result2 = a2.showAndWait();

                alterarDescontoAssociadoProduto altDescProd = new alterarDescontoAssociadoProduto();
                fecha();
                try {
                    altDescProd.start(new Stage());
                } catch (Exception ex) {
                    a.setAlertType(Alert.AlertType.ERROR);
                    a.setTitle("ERRO Fatal");
                    a.setHeaderText("A aplicação está corrompida!");
                    a.setContentText("Algumas funcionalidades do aplicação estão inacessíveis. Tente uma reinstalação.");
                    Optional<ButtonType> result3 = a.showAndWait();
                    Logger.getLogger(alterarDescontoAssociadoProdutoController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    /**
     * Método para verificar se a Quantidade de venda é valida.<br>
     *
     * @param text Quantidade de venda a ser verificado.
     * @return boolean se a Quantidade de Venda é valida ou não.
     */
    public boolean isValidQuantidadeVenda(String text) {
        try {
            Integer.parseInt(text);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    /**
     * Método para fechar o ecrã.<br>
     */
    private static void fecha() {
        alterarDescontoAssociadoProduto.getStage().close();
    }

    /**
     * Método para aceder ao menu do utilizador.<br>
     */
    @FXML
    void openMenu(ActionEvent event) {
        try {
            if (login.getNivelLogin() == 1) {
                menuOperador menuO = new menuOperador();
                fecha();
                menuO.start(new Stage());
            } else {
                menuAdministrador menuAdmin = new menuAdministrador();
                fecha();
                menuAdmin.start(new Stage());
            }
        } catch (Exception ex) {
            mensagemErroRecarregarEcra();
            Logger.getLogger(alterarDescontoAssociadoProdutoController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Método para encerrar a aplicação.<br>
     */
    @FXML
    void sair(ActionEvent event) {
        System.exit(0);
    }

    /**
     * Método para terminar sessao do utilizador.<br>
     *
     * @param event evento.
     */
    @FXML
    void terminarSessao(ActionEvent event) {
        try {
            login.setIdLogin(0);
            login.setNivelLogin(10000);
            login log = new login();
            fecha();
            log.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuAdministradorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Método para lançar uam mensagem de erro e recarregar o ecrã caso algo
     * inesperado ocorra com o algoritmo.<br>
     * Método usado quando as falhas são graves mas não graves de mais que a
     * funcionalidade se encontre inutilizável.<br>
     * É a mensagem de erro de severidade mais leve neste ecrã.<br>
     */
    public static void mensagemErroRecarregarEcra() {
        Alert alertaNovo = new Alert(Alert.AlertType.NONE);
        alertaNovo.setAlertType(Alert.AlertType.ERROR);
        alertaNovo.getButtonTypes().setAll(ButtonType.OK);
        alertaNovo.setTitle("ERRO Lógica Aplicação");
        alertaNovo.setHeaderText("Os dados processados não são válidos");
        alertaNovo.setContentText("Ocorreu um erro ao validar os dados selecionados ou estes podem estar inacessíveis. "
                + "\nTente novamente e caso não conseguir contacte o administrador");
        Optional<ButtonType> resulta = alertaNovo.showAndWait();
        if (resulta.get() == ButtonType.OK) {
            recarregarEcra();
        }

    }

    /**
     * Método para recarregar o ecrã de inserir categorias.<br>
     */
    private static void recarregarEcra() {
        alterarDescontoAssociadoProduto altDescP = new alterarDescontoAssociadoProduto();
        fecha();
        try {
            altDescP.start(new Stage());
        } catch (Exception ex) {
            mensagemErroFatal();
        }
    }

    /**
     * Mensagem de erro para tentar fechar em segurança a aplicação no caso
     * desta encontrar um erro muito grave.<br>
     * Se este erro for lançado é porque algo inesperado ocorreu (ex: falha de
     * acesso à base de dados, lançamento de alguns excepções em métodos
     * críticos ou falhas gerais e básicas de programação).<br>
     * É a mensagem de erro de severidade mais elevada deste ecrã.<br>
     */
    public static void mensagemErroFatal() {
        Alert a = new Alert(Alert.AlertType.NONE);
        a.setAlertType(Alert.AlertType.ERROR);
        a.setTitle("ERRO Fatal");
        a.setHeaderText("A aplicação comportou-se de forma inesperada!");
        a.setContentText("Encerrando a aplicação");
        a.getButtonTypes().setAll(ButtonType.OK);
        Optional<ButtonType> result3 = a.showAndWait();
        if (result3.get() == ButtonType.OK) {
            System.exit(0);
        }
    }

    /**
     * método para substituir o conteudo do menu de opções para colocar as
     * opções por ordem caso seja o operador a abrir o ecrã.<br>
     */
    private void adicionarEcraPessoalOperadorMenuOpcoes() {
        //substituir o conteudo do menu de opções para colocar as opções por ordem caso seja o operador a abrir o ecrã
        List<utilizador> operadores = procurarEmail(login.getIdLogin());
        for (utilizador operador : operadores) {
            if (operador.getNivel() == 1) {
                opcoes.getItems().clear();
                MenuItem ePOp = new MenuItem("Ecrã Pessoal");
                MenuItem terminarSessao = new MenuItem("Terminar Sessão");
                MenuItem sair = new MenuItem("Sair");
                opcoes.getItems().add(ePOp);
                opcoes.getItems().add(terminarSessao);
                opcoes.getItems().add(sair);
                ePOp.setOnAction((ActionEvent e) -> {
                    try {
                        ecraPessoalOperador ePO = new ecraPessoalOperador();
                        fecha();
                        ePO.start(new Stage());
                    } catch (Exception ex) {
                        Logger.getLogger(alterarDescontoAssociadoProdutoController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
                terminarSessao.setOnAction((ActionEvent e) -> {
                    try {
                        login.setIdLogin(0);
                        login.setNivelLogin(10000);
                        login log = new login();
                        fecha();
                        log.start(new Stage());
                    } catch (Exception ex) {
                        Logger.getLogger(alterarDescontoAssociadoProdutoController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
                sair.setOnAction((ActionEvent e) -> {
                    System.exit(0);
                });
            }
        }
    }

}
