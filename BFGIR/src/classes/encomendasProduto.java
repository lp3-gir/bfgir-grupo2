/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import bfgir.BDConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe encomendas com metodos de acesso a tabela de encomendas na BD
 */
public class encomendasProduto {

    private int idEncomenda;
    private String codProdPa;
    private int quantVendida;
    private String tipoVenda;
    private Float precoUnitSIVA;
    private Float precoUnitCIVA;
    private String estado;

    /**
     * Construtor da classe encomendasProduto
     */
    public encomendasProduto() {
    }

    /**
     * Construtor da classe encomendasProduto<br>
     * @param idEncomenda id da encomenda associada
     * @param codProdPa codigo do produto associado
     * @param estado estado da encomenda associada
     * @param quantVendida quantidade vendida
     * @param precoUnitSIVA preco de cada unidade sem iva
     * @param precoUnitCIVA preco de cada unidade com iva
     */
    public encomendasProduto(int idEncomenda, String codProdPa, int quantVendida, Float precoUnitSIVA, Float precoUnitCIVA, String estado) {
        this.idEncomenda = idEncomenda;
        this.codProdPa = codProdPa;
        this.quantVendida = quantVendida;
        this.precoUnitSIVA = precoUnitSIVA;
        this.precoUnitCIVA = precoUnitCIVA;
        this.estado = estado;

    }

    /**
     * @return the idEncomenda
     */
    public int getIdEncomenda() {
        return idEncomenda;
    }

    /**
     * @param idEncomenda the idEncomenda to set
     */
    public void setIdEncomenda(int idEncomenda) {
        this.idEncomenda = idEncomenda;
    }

    /**
     * @return the codProdPa
     */
    public String getCodProdPa() {
        return codProdPa;
    }

    /**
     * @param codProdPa the codProdPa to set
     */
    public void setCodProdPa(String codProdPa) {
        this.codProdPa = codProdPa;
    }

    /**
     * @return the quantVendida
     */
    public int getQuantVendida() {
        return quantVendida;
    }

    /**
     * @param quantVendida the quantVendida to set
     */
    public void setQuantVendida(int quantVendida) {
        this.quantVendida = quantVendida;
    }

    /**
     * @return the tipoVenda
     */
    public String getTipoVenda() {
        return tipoVenda;
    }

    /**
     * @param tipoVenda the tipoVenda to set
     */
    public void setTipoVenda(String tipoVenda) {
        this.tipoVenda = tipoVenda;
    }

    /**
     * @return the precoUnitSIVA
     */
    public Float getPrecoUnitSIVA() {
        return precoUnitSIVA;
    }

    /**
     * @param precoUnitSIVA the precoUnitSIVA to set
     */
    public void setPrecoUnitSIVA(Float precoUnitSIVA) {
        this.precoUnitSIVA = precoUnitSIVA;
    }

    /**
     * @return the precoUnitCIVA
     */
    public Float getPrecoUnitCIVA() {
        return precoUnitCIVA;
    }

    /**
     * @param precoUnitCIVA the precoUnitCIVA to set
     */
    public void setPrecoUnitCIVA(Float precoUnitCIVA) {
        this.precoUnitCIVA = precoUnitCIVA;
    }

    /**
     * @return the estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    
    /**
     * Método para selecionar o objeto todo da tabela encomenda pelo ID de
     * Cliente na BD<br>
     *
     * Este método recebe por parametro o ID do Cliente (idCliente). Posto isto
     * faz um select à BD adicionando a um arrayList o atributo idCliente de um
     * cliente com aquele ID de cliente.<br>
     *
     * @param idEncomenda recebe um id de encomenda
     * @return id de cliente
     *
     *
     *
     */
    public static List<encomendasProduto> listaEncomendaProduto(int idEncomenda) {

        List<encomendasProduto> encomendasProdutos = new ArrayList<encomendasProduto>();
        String sql = "SELECT * from encomendasproduto where idEncomenda = (?) ";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setInt(1, idEncomenda);

            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                encomendasProduto encProds = new encomendasProduto();
                encProds.setCodProdPa(results.getString(3));

                encomendasProdutos.add(encProds);
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return encomendasProdutos;
    }
 

}
