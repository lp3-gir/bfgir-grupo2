package controllers;

import bfgir.aprovarClientes;
import bfgir.login;
import bfgir.menuAdministrador;
import bfgir.menuOperador;
import classes.cliente;
import static classes.cliente.alterarEstadoCliente;
import static classes.cliente.listarClientesPendentes;
import static classes.cliente.pesquisaClientePendente;
import static classes.cliente.getClienteID;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Classe controladora do ecrã aprovarClientes.<br>
 */
public class aprovarClientesController implements Initializable {

    private int altura_vbox = 0;
    Alert a2 = new Alert(Alert.AlertType.NONE);
    @FXML
    private Pane bPane;

    @FXML
    private AnchorPane painelAprovarCliente;

    @FXML
    private ScrollPane ClientesScrollPane;

    @FXML
    private VBox vBoxClientes;

    @FXML
    private Label lbNome;

    @FXML
    private Label lbMorada;

    @FXML
    private Label lbContribuinte;

    @FXML
    private TextArea nome;

    @FXML
    private TextArea contribuinte;

    @FXML
    private TextArea codigoPostal;

    @FXML
    private Label lbCodigoPostal;

    @FXML
    private TextArea morada;

    @FXML
    private Button AprovarCliente;

    @FXML
    private Label lbPais;

    @FXML
    private TextArea pais;

    @FXML
    private Label lbEmail;

    @FXML
    private TextArea email;

    @FXML
    private MenuBar menu;

    @FXML
    private TextField pesquisa;

    @FXML
    private Button btnPesquisa;

    private int idC;

    /**
     * Método de arranque do ecrã.<br>
     *
     * Método faz a listagem dos clientes pendentes, para cada cliente é criado
     * um cliente com o seu ID (idCliente), carregando no butao é dado um
     * display das suas informações.<br>
     *
     * @param url url.
     * @param rb rb.
     */
    @Override

    public void initialize(URL url, ResourceBundle rb) {

        setVisibleFalse();

        List<cliente> cli = null;
        cli = listarClientesPendentes();
        for (cliente clientes : cli) {

            int id = clientes.getIdCliente();
            Button botao = new Button(String.valueOf(id));
            botao.setId("botaoVbox");
            botao.setPrefHeight(30);
            botao.setPrefWidth(275);
            botao.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {
                    botao.setId("botaoVboxChangeCollour");
                    setVisibleTrue();

                    List<cliente> clientes = listarClientesPendentes();
                    for (cliente cli : clientes) {
                        idC = id;
                        nome.setText(cli.getNome());
                        morada.setText(cli.getMorada());
                        codigoPostal.setText(cli.getCodPostal());
                        contribuinte.setText(String.valueOf(cli.getContribuinte()));
                        pais.setText(cli.getPais());
                        email.setText(cli.getEmail());
                    }

                }
            });
            vBoxClientes.getChildren().addAll(botao);
            altura_vbox = altura_vbox + 30;
            if (altura_vbox > 515) {
                vBoxClientes.setPrefHeight(altura_vbox);
            }

        }

    }

    /**
     * Método de pesquisar clientes pendentes pelo seu id para listar os seus
     * atributos.<br>
     */
    @FXML
    void pesquisarID(ActionEvent event) {
        try {
            vBoxClientes.getChildren().clear();

            List<cliente> cli = pesquisaClientePendente(Integer.parseInt(pesquisa.getText()));
            for (cliente clientes : cli) {
                int id = clientes.getIdCliente();
                Button botao = new Button(String.valueOf(id));
                botao.setId("botaoVbox");
                botao.setPrefHeight(30);
                botao.setPrefWidth(275);
                botao.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent e) {
                        botao.setId("botaoVboxChangeCollour");
                        setVisibleTrue();

                        List<cliente> clientes = getClienteID(id);
                        for (cliente cli : clientes) {
                            idC = id;
                            nome.setText(cli.getNome());
                            morada.setText(cli.getMorada());
                            codigoPostal.setText(cli.getCodPostal());
                            contribuinte.setText(String.valueOf(cli.getContribuinte()));
                            pais.setText(cli.getPais());
                            email.setText(cli.getEmail());
                        }
                    }
                });
                vBoxClientes.getChildren().addAll(botao);
            }
        } catch (Exception e) {

            a2.setAlertType(Alert.AlertType.INFORMATION);
            a2.setTitle("Clientes");
            a2.setHeaderText("Id inválido!");
            a2.setContentText("Introduza apenas valores numÃ©ricos!");
            a2.getButtonTypes().setAll(ButtonType.OK);
            Optional<ButtonType> result2 = a2.showAndWait();
        }
    }

    /**
     * Método para tornar visivel todos os atributos do cliente selecionado.<br>
     */
    private void setVisibleTrue() {

        lbNome.setVisible(true);
        lbContribuinte.setVisible(true);
        lbEmail.setVisible(true);
        lbMorada.setVisible(true);
        lbCodigoPostal.setVisible(true);
        lbPais.setVisible(true);

        nome.setVisible(true);
        contribuinte.setVisible(true);
        email.setVisible(true);
        morada.setVisible(true);
        codigoPostal.setVisible(true);
        pais.setVisible(true);

        AprovarCliente.setVisible(true);
    }

    /**
     * Método para não tornar visivel os atributos do cliente selecionado.<br>
     */
    private void setVisibleFalse() {

        lbNome.setVisible(false);
        lbContribuinte.setVisible(false);
        lbEmail.setVisible(false);
        lbMorada.setVisible(false);
        lbCodigoPostal.setVisible(false);
        lbPais.setVisible(false);

        nome.setVisible(false);
        contribuinte.setVisible(false);
        email.setVisible(false);
        morada.setVisible(false);
        codigoPostal.setVisible(false);
        pais.setVisible(false);

        AprovarCliente.setVisible(false);

    }

    /**
     * Método para aprovar o cliente.<br>
     * Método ativa o cliente.<br>
     */
    @FXML
    void AprovarCliente(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Feira & Office ");
        alert.setHeaderText("Deseja mesmo aprovar este Cliente?");
        ButtonType buttonTypeYES = new ButtonType("Sim");
        ButtonType buttonTypeNO = new ButtonType("Não");

        alert.getButtonTypes().setAll(buttonTypeYES, buttonTypeNO);
        Optional<ButtonType> result = alert.showAndWait();

        if (result.get() == buttonTypeYES) {
            try {

                Alert a2 = new Alert(Alert.AlertType.NONE);
                alterarEstadoCliente(idC);
                a2.setAlertType(Alert.AlertType.INFORMATION);
                a2.setTitle("Clientes");
                a2.setHeaderText("Cliente aprovado!");
                a2.setContentText("O Cliente selecionado foi aprovado com sucesso!");
                a2.getButtonTypes().setAll(ButtonType.OK);
                Optional<ButtonType> result2 = a2.showAndWait();

                aprovarClientes apCli = new aprovarClientes();
                fecha();

                apCli.start(new Stage());

            } catch (Exception ex) {
                Logger.getLogger(aprovarClientesController.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else {
            event.consume();
        }

    }

    /**
     * Método para fechar o ecrã de aprovação de clientes.<br>
     */
    private void fecha() {
        aprovarClientes.getStage().close();
    }

    /**
     * Método para aceder ao menu do operador ou do administrador conforme o seu
     * nivel de login.<br>
     */
    @FXML
    void openMenu(ActionEvent event) {
        try {
            if (login.getNivelLogin() == 1) {
                menuOperador menuO = new menuOperador();
                fecha();
                menuO.start(new Stage());
            } else {
                menuAdministrador menuAdmin = new menuAdministrador();
                fecha();
                menuAdmin.start(new Stage());
            }
        } catch (Exception ex) {
            Logger.getLogger(aprovarClientesController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Método para encerrar a aplicação.<br>
     */
    @FXML
    void sair(ActionEvent event) {
        System.exit(0);
    }

    /**
     * Metodo para terminar sessao do utilizador
     *
     * @param event evento.
     */
    @FXML
    void terminarSessao(ActionEvent event) {
        try {
            login.setIdLogin(0);
            login.setNivelLogin(10000);
            login log = new login();
            fecha();
            log.start(new Stage());

        } catch (Exception ex) {
            Logger.getLogger(menuAdministradorController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

}
