package controllers;

import bfgir.associarDescontoProduto;
import bfgir.ecraPessoalOperador;
import bfgir.login;
import bfgir.menuAdministrador;
import bfgir.menuOperador;
import classes.desconto;
import static classes.desconto.getListaDescontos;
import static classes.desconto.getListaDescontosID;
import static classes.descontosProduto.inserirDescontos;
import classes.produto;
import static classes.produto.getListaProdutos;
import static classes.produto.pesquisarProdutoID;
import classes.utilizador;
import static classes.utilizador.procurarEmail;
import static controllers.alterarCategoriasController.a;
import static controllers.alterarPrecoProdutoClienteController.validarPesquisaAtibutoCodProdPa;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Classe controladora do ecrã associarDescontoProduto.<br>
 */
public class associarDescontoProdutoController implements Initializable {

    @FXML
    private Pane painelDescontos;

    @FXML
    private ScrollPane produtos;

    @FXML
    private VBox vBoxProdutos;

    @FXML
    private ScrollPane produtosCliente;

    @FXML
    private Menu opcoes;

    @FXML
    private MenuBar menu;

    @FXML
    private TextField codClienteProd;

    @FXML
    private Button CodProd;

    @FXML
    private TextField quantVendida;

    @FXML
    private Button guardaValor;

    @FXML
    private TextField codDesconto;
    @FXML
    private Button descontos;
    String ids = "";
    String idProduto = "";
    int idDesconto = 0;
    float precoProd = 0;
    float desconto = 0;
    @FXML
    private VBox vBoxDescontos;

    /**
     * Método de arranque do ecrã.<br>
     *
     * Faz a listagem de todos os produtos e é criado um botão com o codigo do
     * produto mais a sua descrição o botão de fazer a pesquisa por desconto é
     * invisivel para impedir que o uitlizador pesquise um desconto sem ter
     * escolhido um produto.<br> Depois de escolher um produto essa ferramenta
     * de pesquisa passa a visivel.<br>
     *
     * @param url url.
     * @param rb rb.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        codDesconto.setVisible(false);
        descontos.setVisible(false);

        adicionarEcraPessoalOperadorMenuOpcoes();
        List<produto> pr = getListaProdutos();
        for (produto prod : pr) {

            String id = prod.getCodProdPa();
            ids = id;

            String nome = prod.getCodProdPa() + " - " + prod.getDescricao();
            Button botao = new Button(nome);
            botao.setId("botaoVbox");
            botao.setPrefHeight(30);
            //botao.setPrefWidth(275);
            botao.setPrefWidth(vBoxProdutos.getPrefWidth());
            botao.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {

                    botao.setId("botaoVboxChangeCollour");
                    codDesconto.setVisible(true);
                    descontos.setVisible(true);
                    idProduto = null;
                    idProduto = id;
                    precoProd = prod.getPrecoUnitSIVA();
                    listarDescontos();

                }
            });
            vBoxProdutos.getChildren().addAll(botao);

        }

    }

    /**
     * Méootodo para pesquisar um produto pelo seu codigo.<br>
     *
     * Faz a listagem de todos os produtos que recebe atrávés da pesquisa
     * recebida realizada pelo utilizador e cria um botão com o codigo do
     * produto e a sua descrição.<br>
     *
     * @param cod : variavel que é recebida do método de pesquisa e serve para
     * filtrar os produtos para o codigo recebido.
     */
    public void listarProdCod(String cod) {
        vBoxProdutos.getChildren().removeAll(vBoxProdutos.getChildren());
        List<produto> pr = pesquisarProdutoID(cod);
        for (produto prod : pr) {

            String id = cod;
            ids = id;

            String nome = prod.getCodProdPa() + " - " + prod.getDescricao();
            Button botao = new Button(nome);
            botao.setId("botaoVbox");
            botao.setPrefHeight(30);
            //botao.setPrefWidth(275);
            botao.setPrefWidth(vBoxProdutos.getPrefWidth());
            botao.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {

                    botao.setId("botaoVboxChangeCollour");
                    codDesconto.setVisible(true);
                    descontos.setVisible(true);
                    idProduto = null;
                    idProduto = id;
                    precoProd = prod.getPrecoUnitSIVA();
                    listarDescontos();

                }
            });
            vBoxProdutos.getChildren().addAll(botao);

        }
    }

    /**
     * Método que lista os descontos.<br>
     *
     * Faz a listagem de todos os descontos que é possivel associar a um
     * produto,é criado um botão que vai conter o id e a quantidede do
     * desconto.<br>
     */
    public void listarDescontos() {
        vBoxDescontos.getChildren().removeAll(vBoxDescontos.getChildren());
        List<desconto> d = getListaDescontos();
        for (desconto des : d) {

            int id = des.getIdDesconto();
            ids = String.valueOf(id);

            String nome = String.valueOf(id) + " - " + String.valueOf(des.getQuantDesconto() + " %");
            Button botao = new Button(nome);
            botao.setId("botaoVbox");
            botao.setPrefHeight(30);
            //botao.setPrefWidth(275);
            botao.setPrefWidth(vBoxDescontos.getPrefWidth());
            botao.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {

                    botao.setId("botaoVboxChangeCollour");
                    idDesconto = 0;
                    idDesconto = id;
                    desconto = des.getQuantDesconto();

                }
            });
            vBoxDescontos.getChildren().addAll(botao);

        }

    }

    /**
     * Método para listar os descontos por uma pesquisa.<br>
     *
     * Faz a listagem de um desconto que o utilizador pesquisou através do
     * metodo de pesquisar desconto a um produto,é criado um botão que vai
     * conter o id e a quantidede do desconto.<br>
     *
     * @param idDescT : variavel recebida do método de pesquisar desconto que
     * permite selecionar de acordo com o que foi introduzido um determinado
     * desconto.
     */
    public void listarDescontosPorID(String idDescT) {
        int idDesc = Integer.parseInt(idDescT);;
        vBoxDescontos.getChildren().removeAll(vBoxDescontos.getChildren());
        List<desconto> d = getListaDescontosID(idDesc);
        for (desconto des : d) {

            int id = idDesc;
            ids = String.valueOf(id);

            String nome = String.valueOf(id) + " - " + String.valueOf(des.getQuantDesconto() + " %");
            Button botao = new Button(nome);
            botao.setId("botaoVbox");
            botao.setPrefHeight(30);
            //botao.setPrefWidth(275);
            botao.setPrefWidth(vBoxDescontos.getPrefWidth());
            botao.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {

                    botao.setId("botaoVboxChangeCollour");
                    idDesconto = 0;
                    idDesconto = id;
                    desconto = des.getQuantDesconto();

                }
            });
            vBoxDescontos.getChildren().addAll(botao);

        }

    }

    /**
     * Método para substituir o conteudo do menu de opções para colocar as
     * opções por ordem caso seja o operador a abrir o ecrã.<br>
     * Método recebe um objeto do tipo menu referente ao menu de opções e depois
     * refaz o mesmo com as novas opções consonante o utilizador seja operador
     * ou administrador.<br>
     */
    private void adicionarEcraPessoalOperadorMenuOpcoes() {
        //substituir o conteudo do menu de opções para colocar as opções por ordem caso seja o operador a abrir o ecrã
        List<utilizador> operadores = procurarEmail(login.getIdLogin());
        for (utilizador operador : operadores) {
            if (operador.getNivel() == 1) {
                opcoes.getItems().clear();
                MenuItem ePOp = new MenuItem("Ecrã Pessoal");
                MenuItem terminarSessao = new MenuItem("Terminar Sessão");
                MenuItem sair = new MenuItem("Sair");
                opcoes.getItems().add(ePOp);
                opcoes.getItems().add(terminarSessao);
                opcoes.getItems().add(sair);
                ePOp.setOnAction((ActionEvent e) -> {
                    try {
                        ecraPessoalOperador ePO = new ecraPessoalOperador();
                        fecha();
                        ePO.start(new Stage());
                    } catch (Exception ex) {
                        Logger.getLogger(aprovarEntradaStocksController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
                terminarSessao.setOnAction((ActionEvent e) -> {
                    try {
                        login.setIdLogin(0);
                        login.setNivelLogin(10000);
                        login log = new login();
                        fecha();
                        log.start(new Stage());
                    } catch (Exception ex) {
                        Logger.getLogger(aprovarEntradaStocksController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
                sair.setOnAction((ActionEvent e) -> {
                    System.exit(0);
                });
            }
        }
    }

    /**
     * Método para fechar o ecrã.<br>
     */
    private void fecha() {
        associarDescontoProduto.getStage().close();

    }

    /**
     * Método para associar descontos a um produto.<br>
     *
     * Este metodo recebe os valores dos outros de métodos e vai fazer a
     * inserção se possivel da associação na base de dados.<br>
     *
     * Antes de fazer o envio tem de passar no teste onde verifica se o preço da
     * unidade já com desconto é menor que o preço base e se o teste passar não
     * é possivel fazer o envio de dados e dá um erro ao utilizador a informar
     * que não foi possivel realizar a operação. Se for possivel os dados são
     * enviados e o utilizador recebe uma notiificação a dizer que foi associado
     * com sucesso.<br>
     *
     */
    @FXML
    void guardarValor(ActionEvent event) throws Exception {

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Feira & Office ");
        alert.setHeaderText("Deseja mesmo associar este produto ao desconto?");
        ButtonType buttonTypeYES = new ButtonType("Sim");
        ButtonType buttonTypeNO = new ButtonType("Não");

        alert.getButtonTypes().setAll(buttonTypeYES, buttonTypeNO);
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == buttonTypeYES) {

            double precoCLSIVA = precoProd * 1.20;

            double precoCLSIVACD = precoCLSIVA / ((desconto / 100) + 1);

            if (precoCLSIVACD <= precoProd) {

                Alert alerts = new Alert(Alert.AlertType.ERROR);
                alerts.setTitle("Feira & Office ");
                alerts.setHeaderText("Associação completa sem sucesso");
                alerts.setContentText("O valor da unidade com desconto fica inferior ao do produto base");
                ButtonType buttonTypeOK = new ButtonType("Confirmar");
                alerts.getButtonTypes().setAll(buttonTypeOK);
                Optional<ButtonType> results = alerts.showAndWait();
                if (result.get() == buttonTypeOK) {
                    event.consume();
                    fecha();
                    associarDescontoProduto ap = new associarDescontoProduto();
                    ap.start(new Stage());
                }

            } else {

                inserirDescontos(idDesconto, idProduto, Integer.parseInt(quantVendida.getText()));
                Alert alerts = new Alert(Alert.AlertType.INFORMATION);
                alerts.setTitle("Feira & Office ");
                alerts.setHeaderText("Associação completa com sucesso");
                ButtonType buttonTypeOK = new ButtonType("Confirmar");
                alerts.getButtonTypes().setAll(buttonTypeOK);
                Optional<ButtonType> results = alerts.showAndWait();
                if (result.get() == buttonTypeOK) {
                    event.consume();
                    fecha();
                    associarDescontoProduto ap = new associarDescontoProduto();
                    ap.start(new Stage());

                } else {
                }
                event.consume();
                fecha();
                associarDescontoProduto ap = new associarDescontoProduto();
                ap.start(new Stage());
            }
        }

    }

    /**
     * Método para aceder ao menu do utilizador.<br>
     */
    @FXML
    private void openMenu(ActionEvent event) {
        try {
            if (login.getNivelLogin() == 1) {
                menuOperador menuO = new menuOperador();
                fecha();
                menuO.start(new Stage());
            } else {
                menuAdministrador menuAdmin = new menuAdministrador();
                fecha();
                menuAdmin.start(new Stage());

            }
        } catch (Exception ex) {
            Logger.getLogger(eliminarFornecedorController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Método do botão de pesquisar um produto pelo seu código.<br>
     * Este método faz ligação com o método de pesquisar o produto por codigo e
     * envia o que recebe do campo codClienteProd.<br>
     * Caso a pesquisa falhe, o utilizador é informado e o ecrã recarregado.<br>
     *
     */
    @FXML
    void pesquisarCodProd(ActionEvent event) {
        if (validarPesquisaAtibutoCodProdPa(codClienteProd.getText()) == true) {
            listarProdCod(codClienteProd.getText());
        } else {
            Alert ab = new Alert(Alert.AlertType.NONE);
            ab.setAlertType(Alert.AlertType.WARNING);
            ab.getButtonTypes().setAll(ButtonType.OK);
            ab.setTitle("Código de Produto Inválido");
            ab.setHeaderText("Formato do Código de Produto Inválido");
            ab.setContentText("Insira um código de produto contendo apenas careteres alfanuméricos");
            Optional<ButtonType> result = ab.showAndWait();
            if (result.get() == ButtonType.OK) {
                recarregarEcra();
            }
        }

    }

    /**
     * Método para encerrar a aplicação.<br>
     */
    @FXML
    private void sair(ActionEvent event) {
        System.exit(0);
    }

    /**
     * Método para terminar sessao do utilizador.<br>
     *
     * @param event evento.
     */
    @FXML
    private void terminarSessao(ActionEvent event) {
        try {
            login.setIdLogin(0);
            login.setNivelLogin(10000);
            login log = new login();
            fecha();
            log.start(new Stage());

        } catch (Exception ex) {
            Logger.getLogger(menuAdministradorController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Método para selecionar um desconto específico associado a um produto.<br>
     *
     */
    @FXML
    private void pesquisaDescontos(ActionEvent event) {
        try {
            listarDescontosPorID(codDesconto.getText());
        } catch (Exception enfimNaoEstoures) {
        }

    }

    /**
     * Método para recarregar o ecrã.<br>
     */
    private void recarregarEcra() {
        associarDescontoProduto altPPC = new associarDescontoProduto();
        fecha();
        try {
            altPPC.start(new Stage());
        } catch (Exception ex) {
            mensagemErroFatal();
        }
    }

    /**
     * Mensagem de erro para tentar fechar em segurança a aplicação no caso
     * desta encontrar um erro muito grave.<br>
     * Se este erro for lançado é porque algo inesperado ocorreu (ex: falha de
     * acesso à base de dados, lançamento de alguns excepções em métodos
     * críticos ou falhas gerais e básicas de programação).<br>
     * É a mensagem de erro de severidade mais elevada deste ecrã.<br>
     */
    public static void mensagemErroFatal() {
        a.setAlertType(Alert.AlertType.ERROR);
        a.setTitle("ERRO Fatal");
        a.setHeaderText("A aplicação comportou-se de forma inesperada!");
        a.setContentText("Encerrando a aplicação");
        a.getButtonTypes().setAll(ButtonType.OK);
        Optional<ButtonType> result3 = a.showAndWait();
        if (result3.get() == ButtonType.OK) {
            System.exit(0);
        }
    }

}
