/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import bfgir.alterarOperadores;
import bfgir.eliminarOperadores;
import bfgir.login;
import bfgir.menuAdministrador;
import static classes.administrador.eliminarUtilizador;
import static classes.administrador.listarUtilizadoresPorTipo;
import static classes.administrador.procurarUtilizadorID;
import static classes.administrador.procurarUtilizadorIDEmail;
import classes.utilizador;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * FXML Controller class
 */
public class eliminarOperadoresController implements Initializable {

    int id_operador_eliminar = 0;
    String email_operador_eliminar = "";
    int nivel_operador = 1;
    Alert a = new Alert(Alert.AlertType.NONE);

    @FXML
    private Pane PaneComp;
    @FXML
    private Label lbEmail;
    @FXML
    private TextArea email;
    @FXML
    private Label lbPass;
    @FXML
    private TextArea password;
    @FXML
    private Button eliminarOperador;
    @FXML
    private AnchorPane painelOperadores;
    @FXML
    private ScrollPane operadoresScrollPane;
    @FXML
    private VBox vBoxOperadores;
    @FXML
    private Label lbEstado;
    @FXML
    private TextArea estado;
    @FXML
    private MenuBar menu;

    private int altura_vbox = 0;

    /**
     * Initializes the controller class. Lista por id todos os utlizadores
     * operadores.<br>
     *
     * @param url url
     * @param rb rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        lbEmail.setVisible(false);
        lbPass.setVisible(false);
        lbEstado.setVisible(false);
        email.setVisible(false);
        password.setVisible(false);
        estado.setVisible(false);
        eliminarOperador.setVisible(false);
        email.setEditable(false);
        password.setEditable(false);
        estado.setEditable(false);

        List<utilizador> operadores = listarUtilizadoresPorTipo(1);
        for (utilizador operador : operadores) {

            int id = operador.getIdUtilizador();
            Button botao = new Button(String.valueOf(id));
            botao.setId("botaoVbox");
            botao.setPrefHeight(30);
            botao.setPrefWidth(275);
            botao.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {
                    botao.setId("botaoVboxChangeCollour");
                    apresentarOperador(id);
                }
            });
            altura_vbox = altura_vbox + 30;
            vBoxOperadores.getChildren().addAll(botao);
            if (altura_vbox > 493) {
                vBoxOperadores.setPrefHeight(altura_vbox);
            }
        }

    }

    /**
     * Método para mostrar alguns atributos do operador selecionado. Grava o seu
     * id de utilizador para posterior inativação do mesmo. Não mostra a real
     * password.<br>
     *
     * @param id_operador recebe o ID do operador
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    private void apresentarOperador(int id_operador)  {

        lbEmail.setVisible(true);
        lbPass.setVisible(true);
        lbEstado.setVisible(true);
        email.setVisible(true);
        password.setVisible(true);
        estado.setVisible(true);
        eliminarOperador.setVisible(true);

        List<utilizador> operadores = procurarUtilizadorID(id_operador);

        for (utilizador operador : operadores) {
            email_operador_eliminar = operador.getEmail();
            email.setText(email_operador_eliminar);
            password.setText("*********");
            estado.setText(operador.getEstado());
            id_operador_eliminar = id_operador;
        }

    }

    /**
     * método para fechar o ecrã de eliminação de operadores
     */
    private void fecha() {
        eliminarOperadores.getStage().close();
    }

    /**
     * Método para aceder ao menu do operador
     */
    @FXML
    private void openMenu(ActionEvent event) {
        try {
            menuAdministrador menuAdmin = new menuAdministrador();
            fecha();
            menuAdmin.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(eliminarOperadoresController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para terminar sessao do utilizador<br>
     *
     * @param event evento
     */
    @FXML
    private void terminarSessao(ActionEvent event) {
        try {
            login.setIdLogin(0);
            login.setNivelLogin(10000);
            login log = new login();
            fecha();
            log.start(new Stage());

        } catch (Exception ex) {
            Logger.getLogger(menuAdministradorController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Este método vai fazer um alerta de confirmação se quer mesmo apagar o
     * operador, se sim vai confirmar a BD se o operador ja foi ou nao eliminado
     * se sim faz um alerta com essa informação, se o operador ainda ester na BD
     * é apagado, se não o alerta fecha.<br>
     *
     *
     * @param event evento
     */
    @FXML
    private void eliminarOperador(ActionEvent event) {

        a.setAlertType(Alert.AlertType.CONFIRMATION);
        a.setTitle("Operadores");
        a.setHeaderText("Deseja mesmo eliminar o operador?");
        a.setResizable(false);
        a.getButtonTypes().setAll(ButtonType.YES, ButtonType.NO);
        Optional<ButtonType> result = a.showAndWait();

        if (result.get() == ButtonType.YES) {

            Alert a2 = new Alert(Alert.AlertType.NONE);

            //verificar se o utilizador ainda existe na BD
            List<utilizador> utilizadores = procurarUtilizadorIDEmail(id_operador_eliminar, email_operador_eliminar);

            int count = 0;
            for (utilizador uti : utilizadores) {
                count = 1;
            }
            if (count == 0) {
                a.setAlertType(Alert.AlertType.INFORMATION);
                a.setTitle("Operador");
                a.setHeaderText("O operador que estava a tentar eliminar já foi removido!");
                a.setContentText("O operador for eliminado por outro utlizador.");
                a.setResizable(false);
                a.show();
                //Optional<ButtonType> result2 = a.showAndWait();
                alterarOperadores altOps = new alterarOperadores();
                fecha();
                try {
                    altOps.start(new Stage());
                } catch (Exception ex) {
                    a.setAlertType(Alert.AlertType.ERROR);
                    a.setTitle("ERRO Fatal");
                    a.setHeaderText("A aplicação está corrompida!");
                    a.setContentText("Algumas funcionalidades do aplicação estão inacessíveis. Tente uma reinstalação.");
                    a.show();
                    System.out.println("Erro Exception ex " + ex.getMessage());
                    Logger.getLogger(alterarOperadoresController.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {

                eliminarUtilizador(id_operador_eliminar, nivel_operador);
                a2.setAlertType(Alert.AlertType.INFORMATION);

                a2.setTitle("Operadores");
                a2.setHeaderText("Operador eliminado com sucesso!");
                a2.setContentText("O operador selecionado foi eliminado!");
                a2.getButtonTypes().setAll(ButtonType.OK);
                Optional<ButtonType> result2 = a2.showAndWait();

                eliminarOperadores eliOps = new eliminarOperadores();
                fecha();

                try {
                    eliOps.start(new Stage());
                } catch (Exception ex) {
                    a.setAlertType(Alert.AlertType.ERROR);
                    a.setTitle("ERRO Fatal");
                    a.setHeaderText("A aplicação está corrompida!");
                    a.setContentText("Algumas funcionalidades do aplicação estão inacessíveis. Tente uma reinstalação.");
                    a.show();
                    System.out.println("Erro Exception ex " + ex.getMessage());
                    Logger
                            .getLogger(eliminarOperadoresController.class
                                    .getName()).log(Level.SEVERE, null, ex);
                }

            }
        }

    }

    /**
     * Método para encerrar a aplicação
     */
    @FXML
    private void sair(ActionEvent event
    ) {
        System.exit(0);
    }
}
