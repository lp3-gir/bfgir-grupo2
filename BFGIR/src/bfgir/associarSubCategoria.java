/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bfgir;

import classes.helper;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author chico
 */
public class associarSubCategoria extends Application {

    private static Stage stage;

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/ecras/associarSubCategoriaProduto.fxml"));

        //stage.setMinWidth(770);
        //stage.setMinHeight(710);
        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.setTitle("F&O - Associar subcategoria produto");
        stage.getIcons().add(helper.appIcon);
        stage.setResizable(false);
        stage.show();
        setStage(stage);

    }

    /**
     * método main do ecrã aprovarClientes
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * método getStage do ecrã aprovarClientes
     *
     * @return retorna o JAVAFX container do ecrã aprovarClientes
     */
    public static Stage getStage() {
        return stage;
    }

    /**
     * método setStage do ecrã aprovarClientes
     *
     * @param aStage the aStage to set
     */
    public static void setStage(Stage aStage) {
        stage = aStage;
    }
    
}
