/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import bfgir.ecraPessoalOperador;
import bfgir.inserirFornecedores;
import bfgir.login;
import bfgir.menuAdministrador;
import bfgir.menuOperador;
import static classes.PasswordHelper.generatePassword;
import classes.codigoPostal;
import static classes.codigoPostal.inserirCodPostal;
import static classes.codigoPostal.procurarCodPostal;
import static classes.email.enviarEmail;
import static classes.email.isValid;
import classes.fornecedor;
import static classes.fornecedor.inserirFornecedores;
import static classes.fornecedor.procurarFornecedorNIF;
import classes.passwordUtils;
import classes.utilizador;
import static classes.utilizador.inserirUtilizadores;
import static classes.utilizador.procurarDadosLogin;
import static classes.utilizador.procurarEmail;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

/**
 * FXML Controller class
 */
public class inserirFornecedoresController implements Initializable {

    @FXML
    private Label lbNome;
    @FXML
    private Label lbCodPostal;
    @FXML
    private Label lbMorada;
    @FXML
    private Label lbNif;
    @FXML
    private Label lbPais;
    @FXML
    private TextArea nome;
    @FXML
    private TextArea morada;
    @FXML
    private TextArea codigoPostal;
    @FXML
    private TextArea contribuinte;
    @FXML
    private Button registar;
    @FXML
    private TextArea pais;
    @FXML
    private MenuBar menu;
    @FXML
    private Label lbEmail;
    @FXML
    private TextArea email;

    Alert a = new Alert(Alert.AlertType.NONE);
    @FXML
    private Label lbCidade;
    @FXML
    private TextArea cidade;
    @FXML
    private Menu opcoes;

    /**
     * Initializes the controller class.<br>
     *Adiciona o menu pessoal do administrador <br>
     * @param url url
     * @param rb rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        adicionarEcraPessoalOperadorMenuOpcoes();
    }

    /**
     * Metodo que executa varias verificações para ver se o utilizador 
     * pode ser registado na base de dados, se todas as validaçôes forem validas 
     * o utilizador é registado na BD<br>
     * 
     * @param event evento
     */
    @FXML
    private void handleButtonRegistar(ActionEvent event) {
        try {
            if ("".equals(nome.getText()) || "".equals(email.getText()) || "".equals(morada.getText()) || "".equals(codigoPostal.getText()) || "".equals(contribuinte.getText()) || "".equals(cidade.getText()) || "".equals(pais.getText())) {
                a.setAlertType(Alert.AlertType.ERROR);
                a.setTitle("Fornecedor");
                a.setHeaderText("Erro ao inserir fornecedor!");
                a.setContentText("Falta de dados para poder registar o fornecedor!");
                a.setResizable(false);
                Optional<ButtonType> result2 = a.showAndWait();
            } else if (Integer.parseInt(contribuinte.getText()) < 100000000 || Integer.parseInt(contribuinte.getText()) > 999999999) {
                a.setAlertType(Alert.AlertType.ERROR);
                a.setTitle("Utilizador");
                a.setHeaderText("Erro ao inserir utilizador!");
                a.setContentText("Formato de contribuinte errado!");
                a.setResizable(false);
                Optional<ButtonType> result2 = a.showAndWait();
            } else if (!isValid(email.getText())) {
                a.setAlertType(Alert.AlertType.ERROR);
                a.setTitle("Fornecedor");
                a.setHeaderText("Email com formato incorreto!");
                a.setContentText("Introduza um email com formato correto para poder efetuar o registo do fornecedor!");
                a.setResizable(false);
                Optional<ButtonType> result2 = a.showAndWait();

            } else {
                String myPassword = generatePassword(20);
                // Generate Salt. The generated value can be stored in DB.
                String salt = passwordUtils.getSalt(30);
                // Protect user's password. The generated value can be stored in DB.
                String mySecurePassword = passwordUtils.generateSecurePassword(myPassword, salt);
                utilizador user = new utilizador();
                user.setEmail(email.getText());
                user.setPassword(mySecurePassword);
                user.setSalt(salt);
                user.setEstado("ativo");
                user.setNivel(0);
                List<utilizador> utilizadores = procurarDadosLogin(email.getText());
                int count = 0;
                for (utilizador uti : utilizadores) {
                    count = 1;
                }
                int contri = Integer.parseInt(contribuinte.getText());
                List<fornecedor> fornecedores = procurarFornecedorNIF(contri);
                int count2 = 0;
                for (fornecedor forns : fornecedores) {
                    count2 = 1;
                }
                if (count != 0) {
                    a.setAlertType(Alert.AlertType.INFORMATION);
                    a.setTitle("Fornecedor");
                    a.setHeaderText("Email já registado!");
                    a.setContentText("Email inserido já se encontra registado!");
                    a.setResizable(false);
                    Optional<ButtonType> result2 = a.showAndWait();
                } else if (count2 != 0) {
                    a.setAlertType(Alert.AlertType.INFORMATION);
                    a.setTitle("Fornecedor");
                    a.setHeaderText("Contribuinte já registado!");
                    a.setContentText("O contribuinte inserido já se encontra registado!");
                    a.setResizable(false);
                    Optional<ButtonType> result2 = a.showAndWait();
                } else {
                    Random rand = new Random();

                    int idForn = rand.nextInt(999999999);
                    enviarEmail(email.getText(), myPassword, String.valueOf(idForn));
                    inserirUtilizadores(user);

                    fornecedor forn = new fornecedor();
                    forn.setIdFornecedor(String.valueOf(idForn));
                    forn.setNome(nome.getText());
                    forn.setContribuinte(Integer.parseInt(contribuinte.getText()));
                    forn.setMorada(morada.getText());
                    forn.setCodPostal(codigoPostal.getText());
                    forn.setPais(pais.getText());
                    forn.setEstado("ativo");

                    List<utilizador> utilizadores2 = procurarDadosLogin(email.getText());
                    for (utilizador uti : utilizadores2) {
                        forn.setIdUtilizador(uti.getIdUtilizador());
                    }

                    inserirFornecedores(forn);

                    List<codigoPostal> codigosPostais = procurarCodPostal(codigoPostal.getText());
                    int count3 = 0;
                    for (codigoPostal codPos : codigosPostais) {
                        count3 = 1;
                    }
                    if (count3 == 0) {
                        //codigoPostal cod = new codigoPostal();
                        //cod.setCodPostal(codigoPostal.getText());
                        //cod.setCidade(cidade.getText());

                        inserirCodPostal(codigoPostal.getText(), cidade.getText());
                    }

                    a.setAlertType(Alert.AlertType.INFORMATION);
                    a.setTitle("Fornecedor");
                    a.setHeaderText("Inserido com sucesso!");
                    a.setContentText("Fornecedor inserido com sucesso!");
                    a.setResizable(false);
                    Optional<ButtonType> result2 = a.showAndWait();
                }
            }
        } catch (NumberFormatException e) {
            a.setAlertType(Alert.AlertType.ERROR);
            a.setTitle("Utilizador");
            a.setHeaderText("Erro ao inserir utilizador!");
            a.setContentText("Formato de contribuinte errado!");
            a.setResizable(false);
            Optional<ButtonType> result2 = a.showAndWait();
        }
    }

    /**
     * Metodo para terminar sessao do utilizador <br>
     * 
     * @param event evento
     */
    @FXML
    private void terminarSessao(ActionEvent event) {
        try {
            login.setIdLogin(0);
            login.setNivelLogin(10000);
            login log = new login();
            fecha();
            log.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(inserirFornecedoresController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Método para encerrar a aplicação
     */
    @FXML
    private void sair(ActionEvent event) {
        System.exit(0);

    }

    /**
     * Método para aceder ao menu do operador ou do administrador conforme o nivel de login
     */
    @FXML
    private void openMenu(ActionEvent event) {
        try {
            if (login.getNivelLogin() == 1) {
                menuOperador menuO = new menuOperador();
                fecha();
                menuO.start(new Stage());
            } else {
                menuAdministrador menuAdmin = new menuAdministrador();
                fecha();
                menuAdmin.start(new Stage());
            }

        } catch (Exception ex) {
            Logger.getLogger(inserirFornecedoresController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    /**
     * método para fechar o ecrã de inserir fornecedores
     */
    private void fecha() {
        inserirFornecedores.getStage().close();
    }

    /**
     * método para substituir o conteudo do menu de opções para colocar as
     * opções por ordem caso seja o operador a abrir o ecrã
     */
    private void adicionarEcraPessoalOperadorMenuOpcoes() {
        List<utilizador> operadores = procurarEmail(login.getIdLogin());
        for (utilizador operador : operadores) {
            if (operador.getNivel() == 1) {
                opcoes.getItems().clear();
                MenuItem ePOp = new MenuItem("Ecrã Pessoal");
                MenuItem terminarSessao = new MenuItem("Terminar Sessão");
                MenuItem sair = new MenuItem("Sair");
                opcoes.getItems().add(ePOp);
                opcoes.getItems().add(terminarSessao);
                opcoes.getItems().add(sair);
                ePOp.setOnAction((ActionEvent e) -> {
                    try {
                        ecraPessoalOperador ePO = new ecraPessoalOperador();
                        fecha();
                        ePO.start(new Stage());
                    } catch (Exception ex) {
                        Logger.getLogger(inserirFornecedoresController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
                terminarSessao.setOnAction((ActionEvent e) -> {
                    try {
                        login.setIdLogin(0);
                        login.setNivelLogin(10000);
                        login log = new login();
                        fecha();
                        log.start(new Stage());
                    } catch (Exception ex) {
                        Logger.getLogger(inserirFornecedoresController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
                sair.setOnAction((ActionEvent e) -> {
                    System.exit(0);
                });
            }
        }
    }

}
