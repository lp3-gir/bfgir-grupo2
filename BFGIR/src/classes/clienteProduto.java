/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import bfgir.BDConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe cliente com métodos de acesso a tabela de clienteProduto na BD.
 */
public class clienteProduto {

    private int idCliente;
    private String codProdPa;
    private Float precoUnitSIVA;

    /**
     * Construtor da classe clienteProduto.
     */
    public clienteProduto() {
    }

    /**
     * Construtor da classe clienteProduto.
     *
     * @param idCliente id do clienteProduto.
     * @param codProdPa codigo do produto.
     * @param precoUnitSIVA preco do produto associado ao cliente.
     */
    public clienteProduto(int idCliente, String codProdPa, Float precoUnitSIVA) {
        this.idCliente = idCliente;
        this.codProdPa = codProdPa;
        this.precoUnitSIVA = precoUnitSIVA;

    }

    /**
     * Método  Método get idCliente.<br>
     * @return the idCliente.
     */
    public int getIdCliente() {
        return idCliente;
    }

    /**
     * Método set idCliente.<br>
     * @param idCliente the idCliente to set.
     */
    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    /**
     * Método get codProdPa.<br>
     * @return the codProdPa.
     */
    public String getCodProdPa() {
        return codProdPa;
    }

    /**
     * Método set codProdPa.<br>
     * @param codProdPa the codProdPa to set.
     */
    public void setCodProdPa(String codProdPa) {
        this.codProdPa = codProdPa;
    }

    /**
     * Método get precoUnitSIVA.<br>
     * @return the precoUnitSIVA.
     */
    public Float getPrecoUnitSIVA() {
        return precoUnitSIVA;
    }

    /**
     * Método set precoUnitSIVA.<br>
     * @param precoUnitSIVA the precoUnitSIVA to set.
     */
    public void setPrecoUnitSIVA(Float precoUnitSIVA) {
        this.precoUnitSIVA = precoUnitSIVA;
    }

    /**
     * Método para procurar registos da tabela clienteProduto pelo id do
     * cliente.<br>
     * Método recebe o id do cliente e retorna uma lista de objetos
     * clienteProduto associados ao mesmo.<br>
     *
     * @param idCliente : id do cliente a pesquisar.
     * @return : lista de objetos clienteProduto associados ao código do produto.
     */
    public static List<clienteProduto> prodPorIdCliente(int idCliente) {

        List<clienteProduto> listarCodProd = new ArrayList();
        String sql = "Select * from clienteproduto where idCliente = (?)";

        PreparedStatement stmt = null;
        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setInt(1, idCliente);

            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                clienteProduto cpi = new clienteProduto();
                cpi.setIdCliente(results.getInt(1));
                cpi.setCodProdPa(results.getString(2));
                cpi.setPrecoUnitSIVA(results.getFloat(3));

                listarCodProd.add(cpi);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return listarCodProd;

    }

    /**
     * Método para se obter os registos da tabela clienteProduto após pesquisa
     * pelo id do cliente e o código de um produto.<br>
     * Método obtém um produto específico (nomeadamente o seu preço) de um
     * cliente específico.<br>
     * Método usado para auxílio de outros métodos.
     *
     * @param idCliente : id do cliente.
     * @param codProdPa : código do produto.
     * @return : retorna o(s) registo(s) da tabela clienteProduto aassociados ao
     * cliente e ao produto.
     */
    public static List<clienteProduto> getClienteProdutoCodProdPaIdCliente(int idCliente, String codProdPa) {

        List<clienteProduto> listarCodProd = new ArrayList();
        String sql = "Select * from clienteproduto where idCliente = (?) and codProdPa = (?)";

        PreparedStatement stmt = null;
        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setInt(1, idCliente);
            pstmt.setString(2, codProdPa);
            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                clienteProduto cpi = new clienteProduto();
                cpi.setIdCliente(results.getInt(1));
                cpi.setCodProdPa(results.getString(2));
                cpi.setPrecoUnitSIVA(results.getFloat(3));

                listarCodProd.add(cpi);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return listarCodProd;

    }

    /**
     * Método para alterar o preço de um produto de um cliente.<br>
     *
     * @param precoUnitSIVA : preço de produto a atualizadr.
     * @param IdCliente : id do cliente associado ao produto.
     * @param codProdPa : código do produto associado ao cliente.
     */
    public static void alterarPrecoUnitSIVAClienteProduto(float precoUnitSIVA, int IdCliente, String codProdPa) {

        String sql = "UPDATE clienteproduto SET precoUnitSIVA = (?) WHERE idCliente = (?) and codProdPa = (?) ";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setFloat(1, precoUnitSIVA);
            pstmt.setInt(2, IdCliente);
            pstmt.setString(3, codProdPa);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
  /**
     * Método para iinserir  um produto a um cliente.<br>
     *
     * @param idCli : id do cliente associado ao produto.
     * @param CodProd : código do produto associado ao cliente.
     * @param preco : preço de produto a atualizadr.
     */
    public static void inserirProdutos(int idCli, String CodProd, float preco) {

        String sql = "INSERT INTO clienteproduto (idCliente,codProdPa,precoUnitSIVA) VALUES (?,?,?)";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setInt(1, idCli);
            pstmt.setString(2, CodProd);
            pstmt.setFloat(3, preco);

            pstmt.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

    }

    /**
     * Método para procurar o preço de venda mais baixo de um produto.<br>
     * Método presquisa pelo preço de venda unitário sem iva mais baixo um
     * produto para o retornar na forma de número float.<br>
     * Método usado para depois calcular o preço mais alto pelo qual o produto
     * deve ser comprado para se manter a margem de lucro desejada.<br>
     *
     * @param codProdPa : código do produto a pesquisar.
     * @return : retorna um número float referente ao preço de venda mais baixo
     * do produto.
     */
    public static float getPrecoMinVendaProduto(String codProdPa) {

        float precoMaxCompra = 0.0f;

        String sql = "select min(precoUnitSIVA) from clienteproduto where codProdPa = (?)";

        PreparedStatement stmt = null;
        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setString(1, codProdPa);
            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                precoMaxCompra = results.getFloat(1);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return precoMaxCompra;

    }

    /**
     * Método para procurar registos da tabela clienteProduto pelo código do
     * produto.<br>
     * Método recebe o código do produto e retorna uma lista de objetos
     * clienteProduto associados ao mesmo.<br>
     *
     * @param codProdPa: código do produto a pesquisar.
     * @return : lista de objetos clienteProduto associados ao código do produto.
     */
    public static List<clienteProduto> getProdAVendaCodProd(String codProdPa) {

        List<clienteProduto> listarCodProd = new ArrayList();
        String sql = "Select * from clienteproduto where codProdPa = (?)";

        PreparedStatement stmt = null;
        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setString(1, codProdPa);

            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                clienteProduto cpi = new clienteProduto();
                cpi.setIdCliente(results.getInt(1));
                cpi.setCodProdPa(results.getString(2));
                cpi.setPrecoUnitSIVA(results.getFloat(3));

                listarCodProd.add(cpi);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return listarCodProd;

    }

}
