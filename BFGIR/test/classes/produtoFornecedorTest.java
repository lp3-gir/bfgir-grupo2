/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 */
public class produtoFornecedorTest {

    /**
     * construtor da classe produto fornecedor teste
     */
    public produtoFornecedorTest() {
    }

    /**
     * set up class
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * tear down class
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * set up
     */
    @Before
    public void setUp() {
    }

    /**
     * set up
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of procurarCodProdPa method, of class produtoFornecedor.
     */
    @Test
    public void testProcurarCodProdPa() {
        System.out.println("procurarCodProdPa");
        String codProdForn = "103753960";
        List<produtoFornecedor> detalhes = produtoFornecedor.procurarCodProdPa(codProdForn);
        boolean result = detalhes.isEmpty();
        assertFalse(result);
        System.out.println("Resultado: " + result);

    }

}
