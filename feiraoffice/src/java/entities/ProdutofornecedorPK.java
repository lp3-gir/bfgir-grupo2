/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Ruben
 */
@Embeddable
public class ProdutofornecedorPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "codProdForn")
    private String codProdForn;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "codProdPa")
    private String codProdPa;

    /**
     *
     */
    public ProdutofornecedorPK() {
    }

    /**
     *
     * @param codProdForn recebe o código de um produto do fornecedor
     * @param codProdPa recebe o código de um produto
     */
    public ProdutofornecedorPK(String codProdForn, String codProdPa) {
        this.codProdForn = codProdForn;
        this.codProdPa = codProdPa;
    }

    /**
     *
     * @return codProdForn
     */
    public String getCodProdForn() {
        return codProdForn;
    }

    /**
     *
     * @param codProdForn
     */
    public void setCodProdForn(String codProdForn) {
        this.codProdForn = codProdForn;
    }

    /**
     *
     * @return codProdPa
     */
    public String getCodProdPa() {
        return codProdPa;
    }

    /**
     *
     * @param codProdPa
     */
    public void setCodProdPa(String codProdPa) {
        this.codProdPa = codProdPa;
    }
   
}
