package controllers;

import bfgir.ecraPessoalOperador;
import bfgir.login;
import bfgir.menuAdministrador;
import bfgir.menuOperador;
import bfgir.removerClienteAssociado;
import classes.cliente;
import static classes.cliente.getClienteID;
import static classes.cliente.getClientesComProdutos;
import static classes.cliente.removerClienteAssociado;
import classes.clienteProduto;
import static classes.clienteProduto.getClienteProdutoCodProdPaIdCliente;
import static classes.clienteProduto.prodPorIdCliente;
import classes.encomendas;
import static classes.encomendas.verificarClienteEncomenda;
import classes.encomendasProduto;
import static classes.encomendasProduto.listaEncomendaProduto;
import classes.helper;
import classes.produto;
import static classes.produto.pesquisarProdutoID;
import classes.utilizador;
import static classes.utilizador.procurarEmail;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Classe controladora do ecrã removerClienteAssociado
 *
 * @author Ilídio Magalhães
 */
public class removerClienteAssociadoController implements Initializable {

    Alert a = new Alert(Alert.AlertType.NONE);
    private int altura_vbox = 0;
    private int altura_vbox2 = 0;
    private List<cliente> listaClientes = new ArrayList<>();
    private static List<clienteProduto> listaProdutosCliente = new ArrayList<>();
    private static clienteProduto clienteProdutoBase = new clienteProduto();
    private static int idClientee;
    private static String codProdPas;

    @FXML
    private Pane painelMovimentos;
    @FXML
    private Menu opcoes;
    @FXML
    private VBox vBoxProdutos;
    @FXML
    private Label lblPrecoUniSIVA;
    @FXML
    private TextArea precoUniSIVA;
    @FXML
    private Button pesquisarCodProdutoCliente;
    @FXML
    private Button removerClienteAss;
    @FXML
    private VBox vBoxClientes;
    @FXML
    private Label lblNomeCliente;
    @FXML
    private TextArea nomeCliente;
    @FXML
    private ScrollPane scPProdutos;
    @FXML
    private TextField idClientePesquisar;
    @FXML
    private TextField codProdutoClientePesquisar;
    @FXML
    private Label lblDescricaoProduto;
    @FXML
    private TextArea descricaoProduto;
    @FXML
    private Label lblPrecoUniSIVABase;
    @FXML
    private TextArea precoUniSIVABase;
    @FXML
    private MenuBar menu;
    @FXML
    private ScrollPane scPClientes;
    @FXML
    private Button pesquisarIDCliente;

    /**
     * Initializes the controller class. Método de inicializalão do ecrã que
     * prepara o ecrã para o utilizador.<br>
     * Reformula o menu de opções adicionando a opção de ecrã pessoal caso o
     * utilizador seja um operador e mostra apenas a lista de todos os
     * clientes.<br>
     * Os métodos invocados, chamam (quase) todos os elementos do ecrã e
     * variáveis adicionais que depois são passados aos métodos seguintes.<br>
     *
     * @param url : não é usado
     * @param rb : não é usado
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            adicionarEcraPessoalOperadorMenuOpcoes();
            esconderListaClienteDadosClienteProduto(codProdutoClientePesquisar,
                    pesquisarCodProdutoCliente, scPProdutos, vBoxProdutos, lblNomeCliente,
                    nomeCliente, lblDescricaoProduto, descricaoProduto, lblPrecoUniSIVA, precoUniSIVA,
                    removerClienteAss, lblPrecoUniSIVABase, precoUniSIVABase);
            listarTodosClientesBD(listaClientes, vBoxClientes, vBoxProdutos, listaProdutosCliente,
                    altura_vbox, clienteProdutoBase, altura_vbox2, codProdutoClientePesquisar,
                    pesquisarCodProdutoCliente, scPProdutos, lblNomeCliente, nomeCliente,
                    lblDescricaoProduto, descricaoProduto, lblPrecoUniSIVA,
                    precoUniSIVA, removerClienteAss, lblPrecoUniSIVABase, precoUniSIVABase);
        } catch (Exception erroInicial) {

        }

    }

    /**
     * Metodo para terminar sessao do utilizador
     *
     * @param event evento
     */
    @FXML
    private void terminarSessao(ActionEvent event) {
        try {
            login.setIdLogin(0);
            login.setNivelLogin(10000);
            login log = new login();
            fecha();
            log.start(new Stage());

        } catch (Exception ex) {
            mensagemErroFatal();
            Logger.getLogger(removerClienteAssociadoController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Método para encerrar a aplicação
     */
    @FXML
    private void sair(ActionEvent event) {
        helper.opcaoSair(event);
    }

    /**
     * Método para aceder ao menu do operador
     */
    @FXML
    private void openMenu(ActionEvent event) {
        try {
            if (login.getNivelLogin() == 1) {
                menuOperador menuO = new menuOperador();
                fecha();
                menuO.start(new Stage());
            } else {
                menuAdministrador menuAdmin = new menuAdministrador();
                fecha();
                menuAdmin.start(new Stage());
            }
        } catch (Exception ex) {
            Logger.getLogger(removerClienteAssociadoController.class.getName()).log(Level.SEVERE, null, ex);
            mensagemErroFatal();
        }

    }

    /**
     * Método para listar os detalhes de um produto associado a um cliente.<br>
     * Método primeiro extrai o id do Cliente da lista de registos
     * clienteProduto associados ao mesmo e passa esse id para o objeto
     * clienteProdutoBase.<br>
     * De seguida valida o código do produto do cliente usando o método genérico
     * de validação (código de produto é semelhante ao id do cliente) e verifica
     * se ele existe mesmo. Em caso afirmativo, volta a percorrer a lista de
     * registos clienteProduto associados ao cliente e procura uma
     * correspondencia de id de Cliente e código de Produto com os dados recém
     * adicionados ao clienteProdutoBase para obter o preço de venda do produto
     * para o adicionar ao clienteProdutoBase e com isso completar o objeto
     * clienteProduto.<br>
     * Finalmente invoca um método (que invoca outros métodos) para mostrar o
     * nome do cliente, a descrição do produto e o preço unitário sem iva do
     * produto para venda ao cliente específica.<br>
     * Caso a procura falhe, o utilizador é notificado e o textfield de input de
     * dados é limpo.
     */
    @FXML
    private void pesquisarCodProdutoCliente(ActionEvent event) {

        //preencher o objeto clienteProdutoBase
        //primeiro com id do cliente
        for (clienteProduto clienteProduto : listaProdutosCliente) {
            clienteProdutoBase.setIdCliente(clienteProduto.getIdCliente());
            break;
        }

        //validar o código do produto e adicionar ao clienteProdutoBase e fazer o que o método é suposto fazer
        if (validarPesquisaAtibutoCodProdPa(codProdutoClientePesquisar.getText()) == true
                && existsProdutoDoCliente(clienteProdutoBase.getIdCliente(), codProdutoClientePesquisar.getText()) == true) {
            clienteProdutoBase.setCodProdPa(codProdutoClientePesquisar.getText());
            for (clienteProduto novoClienteProduto : listaProdutosCliente) {
                if (clienteProdutoBase.getIdCliente() == novoClienteProduto.getIdCliente()
                        && clienteProdutoBase.getCodProdPa().equals(novoClienteProduto.getCodProdPa())) {
                    clienteProdutoBase.setPrecoUnitSIVA(novoClienteProduto.getPrecoUnitSIVA());
                    break;
                }
            }
            //só com o objeto clienteProdutoBase completo é que se prosegue o método.
            mostrarDadosClienteProduto(lblNomeCliente, nomeCliente, lblDescricaoProduto,
                    descricaoProduto, lblPrecoUniSIVA, precoUniSIVA, removerClienteAss,
                    lblPrecoUniSIVABase, precoUniSIVABase);

            listarDetalhesProdutoCliente(clienteProdutoBase, nomeCliente, descricaoProduto,
                    precoUniSIVA, precoUniSIVABase, lblPrecoUniSIVABase, lblPrecoUniSIVA);
        } else {
            mensagemErroPesquisaProdutoDoCliente();
            codProdutoClientePesquisar.setText("");
        }
    }

    /**
     *
     * Método para listar todos os clientes existentes na BD <br>
     *
     *
     * Neste método criada uma lista com todos os clientes existente na BD
     * apresentando o seu ID na forma de botões. Ao selecionar um cliente é
     * apresentada outra lista contendo os códigos de produto associados ao
     * cliente que depois o utilizador poderá manipular.<br>
     * Método também esconde e mostra elementos do ecrã aquando da sua
     * invocação, e envia mensagens de erro quase algo falhe.<br>
     * Método base da aplicação visto ser usado para passar muitos dos elementos
     * do ecrã e outros objetos e atributos aos restantes métodos da aplicação.
     *
     * @param listaClientes : lista de objetos do tipo cliente contendo os
     * clientes da BD
     * @param vBoxClientes : vbox que alberga os botões com os idClientes
     * @param altura_vbox2 : int usado para alterar a altura da VBox
     * vBoxProdutos
     * @param clienteProdutoBase : objeto do tipo clienteProduto para conter os
     * dados do produto do cliente em foco
     * @param vBoxProdutos : vbox que alberga os botões com os codProdPa (código
     * de produto) associados a um dado cliente
     * @param listaProdutosCliente :lista de objetos clienteProduto contendo
     * todas as referencias aos produtos associados a um dado cliente
     * @param altura_vbox : int usado para alterar a altura da VBox vBoxClientes
     * @param codProdutoClientePesquisar : textfield referente ao campo para
     * pesquisa de um produto associado a um cliente pelo código de produto
     * @param pesquisarCodProdutoCliente : botão para realizar a pesquisa de um
     * produto associado a um cliente pelo código de produto
     * @param scPProdutos : scrollpane que contém a VBox vBoxProdutos
     * @param lblNomeCliente : label que identifica a textarea nomeCliente
     * @param nomeCliente : textarea que mostra o nome do cliente
     * @param lblDescricaoProduto : label que identifica a textarea
     * escricaoProduto
     * @param descricaoProduto : textarea que mostra a descrição do produto
     * @param lblPrecoUniSIVA : label que descreve a textarea precoUniSIVA
     * @param precoUniSIVA : textarea que mostra o preço (unitário sem iva) de
     * venda de um produto a um cliente
     * @param alterarPrecoProduto : botão para se proceder à alteração do preço
     * (unitário sem iva) de venda de um produto a um cliente
     * @param lblPrecoUniSIVABase : label da textarea do preço base/compra do
     * produto
     * @param precoUniSIVABase : textarea contendo o preço base/compra do
     * produto
     */
    public static void listarTodosClientesBD(List<cliente> listaClientes, VBox vBoxClientes,
            VBox vBoxProdutos, List<clienteProduto> listaProdutosCliente, int altura_vbox,
            clienteProduto clienteProdutoBase, int altura_vbox2, TextField codProdutoClientePesquisar,
            Button pesquisarCodProdutoCliente, ScrollPane scPProdutos, Label lblNomeCliente,
            TextArea nomeCliente, Label lblDescricaoProduto, TextArea descricaoProduto, Label lblPrecoUniSIVA,
            TextArea precoUniSIVA, Button alterarPrecoProduto, Label lblPrecoUniSIVABase, TextArea precoUniSIVABase) {

        listaClientes = getClientesComProdutos();
        for (cliente cliente : listaClientes) {
            int idCliente = cliente.getIdCliente();
            int idCli = idCliente;
            Button botao = new Button(String.valueOf(idCliente));
            botao.setId("botaoVbox");
            botao.setPrefHeight(30);
            botao.setPrefWidth(265);
            botao.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {

                    idClientee = idCli;

                    botao.setId("botaoVboxChangeCollour");
                    vBoxProdutos.getChildren().removeAll(vBoxProdutos.getChildren()); //remoção é feita aqui dos items da lista de produtos
                    listaProdutosCliente.clear();

                    //os atributos quase todos que são passados entram nestes 3 métodos
                    mostarListaCliente(codProdutoClientePesquisar, pesquisarCodProdutoCliente, scPProdutos, vBoxProdutos);

                    esconderDadosClienteProduto(lblNomeCliente, nomeCliente, lblDescricaoProduto, descricaoProduto, lblPrecoUniSIVA,
                            precoUniSIVA, alterarPrecoProduto, lblPrecoUniSIVABase, precoUniSIVABase);

                    if (existsClienteComProdutos(idCliente) == true) {

                        listarIDProdutoCliente(idCliente, listaProdutosCliente, clienteProdutoBase, vBoxProdutos,
                                altura_vbox2, lblNomeCliente, nomeCliente, lblDescricaoProduto, descricaoProduto,
                                lblPrecoUniSIVA, precoUniSIVA, alterarPrecoProduto, lblPrecoUniSIVABase, precoUniSIVABase);
                    } else {
                        mensagemErroPesquisaCliente();
                        recarregarEcra();
                    }

                }
            });
            vBoxClientes.getChildren().addAll(botao);
            altura_vbox = altura_vbox + 30;
            if (altura_vbox > 515) {
                vBoxClientes.setPrefHeight(altura_vbox);
            }

        }
    }

    /**
     * Método usado para gerar uma lista de botões com os códigos dos produtos
     * associados a um cliente<br>
     * Método recebe todos os parametros do método que o invoca (seja o de
     * pesquisa seja o inicial de listagem), incluindo um objeto clienteProduto
     * que pode estar vazio ou não. A seguir procura na tabela clienteProduto
     * pelo id do cliente os registos associados ao mesmo listando-os pelo
     * código do produto na forma de botões.<br>
     * De seguida coloca a null todos os dados do objeto clienteProduto (passado
     * ao método) que irá conter os dados de um registo a manipular.<br>
     * Assim que um utilizador selecione um produto, o objeto clienteProdutoBase
     * é populado com os dados do produto (dados da tabela clienteProduto)
     * selecionado. Este objeto é depois passado a um método de listagem dos
     * dados do objeto que irá invocar outros métodos para que seja mostrado ao
     * utilizador o nome do cliente em vez do id e a descrição do produto em vez
     * do código do produto.<br>
     * Caso algo corra mal, o utilizador será notificado e o ecrã reiniciado -
     * em muitos dos casos automaticamente para parar o método.
     *
     * @param idCliente : String referente ao id do cliente que irá ser
     * manipulado
     * @param listaProdutosDOCliente :lista de objetos clienteProduto associado
     * ao cliente que serve para popular a lista global de nome semelhante
     * @param clienteProdutoBase : objeto clienteProduto que contém ou vai
     * conter os atributos de um registo da tabela clienteProduto
     * @param vBoxProdutos : VBox contendo a lista de produtos (objetos
     * clienteProduto) associados a um cliente
     * @param altura_vbox2 : int usado para alterar a altura da VBox
     * vBoxProdutos
     * @param lblNomeCliente : label que identifica a textarea nomeCliente
     * @param nomeCliente : textarea que mostra o nome do cliente
     * @param lblDescricaoProduto : label que identifica a textarea
     * escricaoProduto
     * @param descricaoProduto : textarea que mostra a descrição do produto
     * @param lblPrecoUniSIVA : label que descreve a textarea precoUniSIVA
     * @param precoUniSIVA : textarea que mostra o preço (unitário sem iva) de
     * venda de um produto a um cliente
     * @param alterarPrecoProduto : botão para se proceder à alteração do preço
     * (unitário sem iva) de venda de um produto a um cliente
     * @param lblPrecoUniSIVABase : label da textarea do preço base/compra do
     * produto
     * @param precoUniSIVABase : textarea contendo o preço base/compra do
     * produto
     */
    public static void listarIDProdutoCliente(int idCliente, List<clienteProduto> listaProdutosDOCliente,
            clienteProduto clienteProdutoBase, VBox vBoxProdutos, int altura_vbox2, Label lblNomeCliente,
            TextArea nomeCliente, Label lblDescricaoProduto, TextArea descricaoProduto,
            Label lblPrecoUniSIVA, TextArea precoUniSIVA, Button alterarPrecoProduto,
            Label lblPrecoUniSIVABase, TextArea precoUniSIVABase) {
        try {
            listaProdutosDOCliente = prodPorIdCliente(idCliente);
            listaProdutosCliente = listaProdutosDOCliente; //adição para colocar um dos métodos de pesquisa a funcionar
        } catch (Exception bc) {
            mensagemErroRecarregarEcraSemParar();
        }

        try {
            setNullAtributosClienteProdutoBase(clienteProdutoBase);
        } catch (Exception ab) {
            mensagemErroRecarregarEcraSemParar();
        }

        try {
            for (clienteProduto produtoCliente : listaProdutosDOCliente) {
                String codProdPa = produtoCliente.getCodProdPa();
                Button botao = new Button(codProdPa);
                botao.setId("botaoVbox");
                botao.setPrefHeight(30);
                botao.setPrefWidth(265);
                botao.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent e) {
                        try {

                            codProdPas = codProdPa;

                            botao.setId("botaoVboxChangeCollour");

                            clienteProdutoBase.setIdCliente(produtoCliente.getIdCliente());
                            clienteProdutoBase.setCodProdPa(produtoCliente.getCodProdPa());
                            clienteProdutoBase.setPrecoUnitSIVA(produtoCliente.getPrecoUnitSIVA());

                            mostrarDadosClienteProduto(lblNomeCliente, nomeCliente, lblDescricaoProduto,
                                    descricaoProduto, lblPrecoUniSIVA, precoUniSIVA,
                                    alterarPrecoProduto, lblPrecoUniSIVABase, precoUniSIVABase);

                            if (existsProdutoDoCliente(clienteProdutoBase.getIdCliente(), clienteProdutoBase.getCodProdPa()) == true) {

                                listarDetalhesProdutoCliente(clienteProdutoBase, nomeCliente,
                                        descricaoProduto, precoUniSIVA, precoUniSIVABase,
                                        lblPrecoUniSIVABase, lblPrecoUniSIVA);

                            } else {
                                mensagemErroPesquisaProdutoDoCliente();
                                recarregarEcra();
                            }

                        } catch (Exception bc) {
                            mensagemErroRecarregarEcra();
                        }
                    }
                });
                vBoxProdutos.getChildren().addAll(botao);
                altura_vbox2 = altura_vbox2 + 30;
                if (altura_vbox2 > 515) {
                    vBoxProdutos.setPrefHeight(altura_vbox2);
                }

            }

        } catch (Exception abc) {
            mensagemErroRecarregarEcra();
        }

    }

    /**
     * Método para mostrar os dados referentes a um objeto clienteProduto - os
     * dados de um produto associado a um cliente <br>
     * Método recebe o objeto clienteProduto e vai procurar nas tabelas cliente
     * e produto, o nome do cliente, a descrição e preço base do produto.
     * Depois, mostra ao utilizador esses atributos, além do preço unitário sem
     * iva de venda do produto ao cliente já presente no objeto
     * clienteProduto.<br>
     * Se algo correr mal, o utilizador é notificado e o ecrã reiniciado.
     *
     * @param clienteProdutoBase : objeto clienteProduto que contém os atributos
     * de um registo da tabela clienteProduto
     * @param nomeCliente : textarea que mostra o nome do cliente
     * @param descricaoProduto : textarea que mostra a descrição do produto
     * @param precoUniSIVA : textarea que mostra o preço (unitário sem iva) de
     * venda de um produto a um cliente
     * @param precoUniSIVABase : textarena que mostra o preço base do produto
     * @param lblPrecoUniSIVABase : label da textarea do preço base do produto
     * @param lblPrecoUniSIVA : label da textarea do preço de venda do produto
     */
    public static void listarDetalhesProdutoCliente(clienteProduto clienteProdutoBase, TextArea nomeCliente,
            TextArea descricaoProduto, TextArea precoUniSIVA, TextArea precoUniSIVABase,
            Label lblPrecoUniSIVABase, Label lblPrecoUniSIVA) {

        try {
            String nomeClienteProduto = getNomeClientePeloID(clienteProdutoBase.getIdCliente());
            String descricaoProdutoCliente = getDescricaoProdutoPeloCodProdPa(clienteProdutoBase.getCodProdPa());
            String precoBaseProduto = getPrecoProdutoPeloCodProdPa(clienteProdutoBase.getCodProdPa());
            if (!descricaoProdutoCliente.equals("") && !nomeClienteProduto.equals("")
                    && !precoBaseProduto.equals("")) {
                modificarLabelsPrecoTipoUnidade(clienteProdutoBase.getCodProdPa(), lblPrecoUniSIVABase, lblPrecoUniSIVA);
                nomeCliente.setText(nomeClienteProduto);
                descricaoProduto.setText(descricaoProdutoCliente);
                precoUniSIVA.setText(String.valueOf(clienteProdutoBase.getPrecoUnitSIVA()));
                precoUniSIVABase.setText(precoBaseProduto);
            } else {
                mensagemErroRecarregarEcra();
            }
        } catch (Exception falhouEsteTambem) {
            mensagemErroRecarregarEcra();
        }
    }

    /**
     * Este metodo vai listar clientes por id depois de selecionado o cliente vai aparecer os produtos associados a esse cliente.
     * depois de selecionado o aproduto vai ser listada informação do produto juntamente com um botão de remover o seu preco associado.
     * para remover o preco vao ser feitas varias verificações e depois de passar em todas as verificações o preco associado é removido
     * 
     * @param event evento
     */
    @FXML
    private void removerClienteAss(ActionEvent event) {

        int error = 0;

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Feira & Office ");
        alert.setHeaderText("Deseja mesmo remover este produto associado a este Cliente?");
        ButtonType buttonTypeYES = new ButtonType("Sim");
        ButtonType buttonTypeNO = new ButtonType("Não");

        alert.getButtonTypes().setAll(buttonTypeYES, buttonTypeNO);
        Optional<ButtonType> result = alert.showAndWait();

        if (result.get() == buttonTypeYES) {

            try {

                List<encomendas> encomenda = verificarClienteEncomenda(idClientee);
                for (encomendas enc : encomenda) {
                    List<encomendasProduto> encomendaProduto = listaEncomendaProduto(enc.getIdEncomenda());
                    
                    for (encomendasProduto encProd : encomendaProduto) {
                        if (encProd.getCodProdPa().equals(codProdPas)) {
                            error = 1;

                        }
                    }

                }
                if (error == 1) {

                    Alert a2 = new Alert(Alert.AlertType.NONE);

                    a2.setAlertType(Alert.AlertType.INFORMATION);
                    a2.setTitle("Cliente");
                    a2.setHeaderText("Cliente com encomendas!");
                    a2.setContentText("O Cliente selecionado tem encomendas ativas e NÃO pode ser removido!");
                    a2.getButtonTypes().setAll(ButtonType.OK);
                    Optional<ButtonType> result2 = a2.showAndWait();

                    removerClienteAssociado remvCli = new removerClienteAssociado();
                    fecha();
                    remvCli.start(new Stage());

                } else {

                    removerClienteAssociado(idClientee, codProdPas);

                    Alert a2 = new Alert(Alert.AlertType.NONE);

                    a2.setAlertType(Alert.AlertType.INFORMATION);
                    a2.setTitle("Produtos Clientes");
                    a2.setHeaderText("Produto removido com sucesso!");
                    a2.setContentText("O Produto selecionado foi removido da lista de produtos associados a este cliente!");
                    a2.getButtonTypes().setAll(ButtonType.OK);
                    Optional<ButtonType> result2 = a2.showAndWait();

                    removerClienteAssociado remvCli = new removerClienteAssociado();
                    fecha();
                    remvCli.start(new Stage());
                }
            } catch (Exception ex) {
                Logger.getLogger(removerClienteAssociadoController.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else {
            event.consume();
        }
    }

    /**
     * Método para lançar uam mensagem de erro e recarregar o ecrã caso algo
     * inesperado ocorra com o algoritmo<br>
     * Método usado quando as falhas são graves mas não graves de mais que a
     * funcionalidade se encontre inutilizável.<br>
     * É um das mensageens de erro de severidade mais leve neste ecrã
     */
    public static void mensagemErroRecarregarEcra() {
        Alert alertaNovo = new Alert(Alert.AlertType.NONE);
        alertaNovo.setAlertType(Alert.AlertType.ERROR);
        alertaNovo.getButtonTypes().setAll(ButtonType.OK);
        alertaNovo.setTitle("ERRO Lógica Aplicação");
        alertaNovo.setHeaderText("Os dados processados não são válidos");
        alertaNovo.setContentText("Ocorreu um erro ao validar os dados selecionados ou estes podem\nestar inacessíveis. "
                + "Tente novamente e caso não conseguir contacte o administrador");
        Optional<ButtonType> resulta = alertaNovo.showAndWait();
        if (resulta.get() == ButtonType.OK) {
            recarregarEcra();
        }

    }

    /**
     * Método para lançar uam mensagem de erro e recarregar o ecrã imediatamente
     * caso algo inesperado ocorra com o algoritmo<br>
     * Método usado quando as falhas são graves mas não graves de mais que a
     * funcionalidade se encontre inutilizável mas que requira quebra imediata
     * do método onde é invocado.<br>
     * É um das mensageens de erro de severidade mais leve neste ecrã
     */
    public static void mensagemErroRecarregarEcraSemParar() {
        Alert alertaNovo = new Alert(Alert.AlertType.NONE);
        alertaNovo.setAlertType(Alert.AlertType.ERROR);
        alertaNovo.getButtonTypes().setAll(ButtonType.OK);
        alertaNovo.setTitle("ERRO Lógica Aplicação");
        alertaNovo.setHeaderText("Os dados processados não são válidos");
        alertaNovo.setContentText("Ocorreu um erro ao validar os dados selecionados ou estes podem\nestar inacessíveis. "
                + "Tente novamente e caso não conseguir contacte o administrador");
        alertaNovo.show();
        recarregarEcra();

    }

    /**
     * Método para lançar uma mensagem de erro caso a pesquisa de um cliente não
     * obtenha um cliente<br>
     * Método usado para avisar o utilizador, semelhante ao
     * mensagemErroRecarregarEcra mas sem invocar o método de recarregar o ecrã
     * que é feito à parte.<br>
     * Método usado em associação com o recarregarEcra e com o comando para
     * limpar a textfield do campo de pesquisa pelo id do cliente. Como o uso é
     * separado mas o método é semelhante, a acção (limpar a textfield ou
     * recarregar o ecrã) fica fora do método.<br>
     * É um das mensagens de erro de severidade mais leve neste ecrã
     */
    public static void mensagemErroPesquisaCliente() {
        Alert alertaNovo = new Alert(Alert.AlertType.NONE);
        alertaNovo.setAlertType(Alert.AlertType.WARNING);
        alertaNovo.getButtonTypes().setAll(ButtonType.OK);
        alertaNovo.setTitle("ID Cliente");
        alertaNovo.setHeaderText("ID Cliente Inválido!");
        alertaNovo.setContentText("O ID de Cliente que inseriu ou selecionou não tem dados associados");
        Optional<ButtonType> resulta = alertaNovo.showAndWait();
        if (resulta.get() == ButtonType.OK) {
        }
    }

    /**
     * Método para lançar uma mensagem de erro caso a pesquisa de um produto não
     * obtenha resultado <br>
     * Método usado para avisar o utilizador, semelhante ao
     * mensagemErroRecarregarEcra mas sem invocar o método de recarregar o ecrã
     * que pode ser é feito à parte.<br>
     * Método usado em associação com o recarregarEcra e com o comando para
     * limpar a textfield do campo de pesquisa pelo código do produto. Como o
     * uso é separado mas o método é semelhante, a acção (limpar a textfield ou
     * recarregar o ecrã) fica fora do método.<br>
     * É um das mensagens de erro de severidade mais leve neste ecrã
     */
    public static void mensagemErroPesquisaProdutoDoCliente() {
        Alert alertaNovo = new Alert(Alert.AlertType.NONE);
        alertaNovo.setAlertType(Alert.AlertType.WARNING);
        alertaNovo.getButtonTypes().setAll(ButtonType.OK);
        alertaNovo.setTitle("Produto do Cliente");
        alertaNovo.setHeaderText("Código do Produto do Cliente Inválido!");
        alertaNovo.setContentText("O Código do Produto que inseriu ou selecionou não tem dados associados");
        Optional<ButtonType> resulta = alertaNovo.showAndWait();
        if (resulta.get() == ButtonType.OK) {
        }
    }

    /**
     * Método para lançar uma mensagem de erro a avisar o utilizador que não
     * insiriu um preço do produto<br>
     * Caso o método seja invocado, o método popula a textarea do preço do
     * produto com o atributo respectivo do objeto clienteProduto em foco
     *
     * @param clienteProdutoBase : objeto clienteProduto que contém os atributos
     * de um registo da tabela clienteProduto
     * @param precoUniSIVA : textarea que mostra o preço (unitário sem iva) de
     * venda de um produto a um cliente
     */
    public static void mensagemErroPrecoVazio(clienteProduto clienteProdutoBase, TextArea precoUniSIVA) {
        Alert alertaNovo = new Alert(Alert.AlertType.NONE);
        alertaNovo.setAlertType(Alert.AlertType.WARNING);
        alertaNovo.getButtonTypes().setAll(ButtonType.OK);
        alertaNovo.setTitle("Preço do Produto");
        alertaNovo.setHeaderText("Produto Sem Preço!");
        alertaNovo.setContentText("Associe um Preço ao Produto");
        Optional<ButtonType> resulta = alertaNovo.showAndWait();
        if (resulta.get() == ButtonType.OK) {
            precoUniSIVA.setText(String.valueOf(clienteProdutoBase.getPrecoUnitSIVA()));
        }
    }

    /**
     * Método para lançar uma mensagem de erro/aviso a avisar o utilizador que
     * não alterou o preço do produto<br>
     *
     */
    public static void mensagemErroPrecoInalterado() {
        Alert alertaNovo = new Alert(Alert.AlertType.NONE);
        alertaNovo.setAlertType(Alert.AlertType.WARNING);
        alertaNovo.getButtonTypes().setAll(ButtonType.OK);
        alertaNovo.setTitle("Preço do Produto");
        alertaNovo.setHeaderText("Produto Sem Alteração de Preço!");
        alertaNovo.setContentText("Altere o Preço ao Produto");
        Optional<ButtonType> resulta = alertaNovo.showAndWait();
        if (resulta.get() == ButtonType.OK) {
        }
    }

    /**
     * Método para lançar uma mensagem de erro/aviso a avisar o utilizador que o
     * preço do produto é inválido<br>
     * Método informa o utilizador que este inseriu um preço de produto inválido
     * e, se possível, mostra-lhe o preço válido mínimo que obedece à regra de
     * negócio (margem de lucro de 20%) e coloca na textarea que mostra o preço
     * do produto, o valor existente na base de dados referente ao registo da BD
     * em foco.<br>
     *
     * @param precoValidomin : float referente ao preço de venda (calculado)
     * mínimo ao cliente do produto em foco
     * @param clienteProdutoBase : objeto clienteProduto contendo os dados (ie,
     * preço de venda) do produto ao cliente em foco
     * @param precoUniSIVA : textarea que mostra o preço de venda do produto ao
     * cliente
     */
    public static void mensagemErroPrecoInvalido(float precoValidomin, clienteProduto clienteProdutoBase, TextArea precoUniSIVA) {
        Alert alertaNovo = new Alert(Alert.AlertType.NONE);
        alertaNovo.setAlertType(Alert.AlertType.WARNING);
        alertaNovo.getButtonTypes().setAll(ButtonType.OK);
        alertaNovo.setTitle("Preço do Produto");
        alertaNovo.setHeaderText("Preço do Produto Inválido");
        if (precoValidomin != -1.0f) {
            alertaNovo.setContentText("Insira um preço igual ou superior a " + precoValidomin + " euros");
        } else {
            alertaNovo.setContentText("Insira um preço 20% superior ao preço base do produto " + clienteProdutoBase.getCodProdPa());
        }
        Optional<ButtonType> resulta = alertaNovo.showAndWait();
        if (resulta.get() == ButtonType.OK) {
            precoUniSIVA.setText(String.valueOf(clienteProdutoBase.getPrecoUnitSIVA()));
        }
    }

    /**
     * Método para recarregar o ecrã
     */
    private static void recarregarEcra() {
        removerClienteAssociado remvCli = new removerClienteAssociado();
        fecha();
        try {
            remvCli.start(new Stage());
        } catch (Exception ex) {
            mensagemErroFatal();
        }
    }

    /**
     * Método para fechar o ecrã
     */
    private static void fecha() {
        removerClienteAssociado.getStage().close();
    }

    /**
     * Método para listar todos os registos da tabela clienteProduto associados
     * a um cliente<br>
     * Método valida o id do cliente inserido para pesquisa (existente e tipo de
     * dados/regex) e caso encontre uma correspondência válida, invoca o método
     * de listar os objetos da tabela clienteProduto que mostra o código de cada
     * produto e o método que oculta os dados referentes a um registo
     * clienteProduto a selecionar.<br>
     * Caso algo corra mal, o cliente é notificado e o campo de pesquisa é
     * limpo.
     */
    @FXML
    private void pesquisarIDCliente(ActionEvent event) {
        if (validarPesquisaAtibutoIDCliente(idClientePesquisar.getText()) == true
                && existsClienteComProdutos(Integer.parseInt(idClientePesquisar.getText())) == true) {
            vBoxProdutos.getChildren().removeAll(vBoxProdutos.getChildren());
            esconderDadosClienteProduto(lblNomeCliente, nomeCliente,
                    lblDescricaoProduto, descricaoProduto, lblPrecoUniSIVA, precoUniSIVA,
                    removerClienteAss, lblPrecoUniSIVABase, precoUniSIVABase);

            mostarListaCliente(codProdutoClientePesquisar, pesquisarCodProdutoCliente, scPProdutos, vBoxProdutos);

            listarIDProdutoCliente(Integer.parseInt(idClientePesquisar.getText()), listaProdutosCliente,
                    clienteProdutoBase, vBoxProdutos, altura_vbox2, lblNomeCliente,
                    nomeCliente, lblDescricaoProduto, descricaoProduto, lblPrecoUniSIVA,
                    precoUniSIVA, removerClienteAss, lblPrecoUniSIVABase, precoUniSIVABase);
        } else {
            mensagemErroPesquisaCliente();
            idClientePesquisar.setText("");
        }
    }

    /**
     * Método para validar por regex do atributo id do cliente a pesquisar<br>
     * Método recebe o String a validar e caso este obedeça à regra, retorna
     * verdadeiro. Se não, retorna falso.<br>
     * Caso algo corra mal, também retorna falso.
     *
     * @param atributoPesquisar : String referente ao id do cliente
     * @return : verdadeiro caso o String obdeca à regra, falso caso não o faça.
     */
    public static boolean validarPesquisaAtibutoIDCliente(String atributoPesquisar) {
        boolean atributoOK = false;
        try {
            atributoOK = !atributoPesquisar.equals("") && atributoPesquisar.length() <= 10
                    && Pattern.compile("[0-9]+").matcher(atributoPesquisar).find() == true;
        } catch (Exception a) {
            atributoOK = false;
        }
        return atributoOK;
    }

    /**
     * Método para validar por regex o atributo (seja o código do produto ou o
     * Id do Cliente) a pesquisar<br>
     * Método recebe o String a validar e caso este obedeça à regra, retorna
     * verdadeiro. Se não, retorna falso.<br>
     * Caso algo corra mal, também retorna falso.
     *
     * @param codProdPa : String referente ao código de produto a pesquisar
     * @return : verdadeiro caso o String obdeca à regra, falso caso não o faça.
     */
    public static boolean validarPesquisaAtibutoCodProdPa(String codProdPa) {

        boolean atributoOK = false;
        try {
            atributoOK = !codProdPa.equals("") && codProdPa.length() <= 25
                    && Pattern.compile("^[a-zA-Z0-9-.]+[a-zA-Z0-9]+$").matcher(codProdPa).find() == true;
        } catch (Exception a) {
            atributoOK = false;
        }
        return atributoOK;
    }

    /**
     * Método para esconder os elementos do ecrã referentes aos registos da
     * tabela clienteProduto associados a um cliente<br>
     *
     * @param codProdutoClientePesquisar : textfield de pesquisa de um registo
     * da tabela clienteProduto pelo código do produto
     * @param pesquisarCodProdutoCliente : botão para pesquisa de um registo da
     * tabela clienteProduto pelo código do produto
     * @param scPProdutos : scrollpane que incluí a VBOX VboxProdutos
     * @param vBoxProdutos : VBox que contém uma lista de botões com o código de
     * um produto referentes a registos da tabela clienteProduto associados a um
     * cliente
     * @param lblNomeCliente : label da textarea nome do cliente
     * @param nomeCliente : textarea contendo o nome do cliente
     * @param lblDescricaoProduto : label da textarea do descrição de um produto
     * @param descricaoProduto : textarea da descrição de um produto
     * @param lblPrecoUniSIVA : label da textarea do preço de venda de um
     * produto
     * @param precoUniSIVA : textarea contendo o preço de venda de um produto
     * @param alterarPrecoProduto : botão para realizar a alteração do preço de
     * venda do produto
     * @param lblPrecoUniSIVABase : label da textarea do preço de compra de um
     * produto
     * @param precoUniSIVABase : textarea contendo o preço de compra/base de um
     * produto
     */
    public static void esconderListaClienteDadosClienteProduto(TextField codProdutoClientePesquisar, Button pesquisarCodProdutoCliente,
            ScrollPane scPProdutos, VBox vBoxProdutos, Label lblNomeCliente, TextArea nomeCliente,
            Label lblDescricaoProduto, TextArea descricaoProduto, Label lblPrecoUniSIVA,
            TextArea precoUniSIVA, Button alterarPrecoProduto, Label lblPrecoUniSIVABase, TextArea precoUniSIVABase) {

        codProdutoClientePesquisar.setVisible(false);
        codProdutoClientePesquisar.setEditable(false);
        pesquisarCodProdutoCliente.setVisible(false);
        scPProdutos.setVisible(false);
        vBoxProdutos.setVisible(false);
        lblNomeCliente.setVisible(false);
        nomeCliente.setVisible(false);
        nomeCliente.setEditable(false);
        lblDescricaoProduto.setVisible(false);
        descricaoProduto.setVisible(false);
        descricaoProduto.setEditable(false);
        lblPrecoUniSIVA.setVisible(false);
        precoUniSIVA.setVisible(false);
        precoUniSIVA.setEditable(false);
        alterarPrecoProduto.setVisible(false);
        lblPrecoUniSIVABase.setVisible(false);
        precoUniSIVABase.setVisible(false);
        precoUniSIVABase.setEditable(false);

    }

    /**
     * Método para tornar invísíveis e não editáveis dados relacionados ou
     * pertencentes a um objeto clienteProduto.<br>
     *
     * @param lblNomeCliente : label da textarea nome do cliente
     * @param nomeCliente : textarea contendo o nome do cliente
     * @param lblDescricaoProduto : label da textarea do descrição de um produto
     * @param descricaoProduto : textarea da descrição de um produto
     * @param lblPrecoUniSIVA : label da textarea do preço de venda de um
     * produto
     * @param precoUniSIVA : textarea contendo o preço de venda de um produto
     * @param removerClienteAss : button para remover um cliente
     * @param lblPrecoUniSIVABase : label da textarea do preço de compra de um
     * produto
     * @param precoUniSIVABase : textarea contendo o preço de compra/base de um
     * produto
     */
    public static void esconderDadosClienteProduto(Label lblNomeCliente, TextArea nomeCliente,
            Label lblDescricaoProduto, TextArea descricaoProduto, Label lblPrecoUniSIVA,
            TextArea precoUniSIVA, Button removerClienteAss,
            Label lblPrecoUniSIVABase, TextArea precoUniSIVABase) {

        lblNomeCliente.setVisible(false);
        nomeCliente.setVisible(false);
        nomeCliente.setEditable(false);
        lblDescricaoProduto.setVisible(false);
        descricaoProduto.setVisible(false);
        descricaoProduto.setEditable(false);
        lblPrecoUniSIVA.setVisible(false);
        precoUniSIVA.setVisible(false);
        precoUniSIVA.setEditable(false);
        removerClienteAss.setVisible(false);
        lblPrecoUniSIVABase.setVisible(false);
        precoUniSIVABase.setVisible(false);
        precoUniSIVABase.setEditable(false);

    }

    /**
     * Método para mostrar a lista de produtos associados a um cliente -
     * registos da tabela clienteProduto.<br>
     *
     * @param codProdutoClientePesquisar : textfield de pesquisa de um registo
     * da tabela clienteProduto pelo código do produto
     * @param pesquisarCodProdutoCliente : botão para pesquisa de um registo da
     * tabela clienteProduto pelo código do produto
     * @param scPProdutos : scrollpane que incluí a VBOX VboxProdutos
     * @param vBoxProdutos : VBox que contém uma lista de botões com o código de
     * um produto referentes a registos da tabela clienteProduto associados a um
     * cliente
     */
    public static void mostarListaCliente(TextField codProdutoClientePesquisar, Button pesquisarCodProdutoCliente,
            ScrollPane scPProdutos, VBox vBoxProdutos) {

        codProdutoClientePesquisar.setVisible(true);
        codProdutoClientePesquisar.setEditable(true);
        pesquisarCodProdutoCliente.setVisible(true);
        scPProdutos.setVisible(true);
        vBoxProdutos.setVisible(true);
    }

    /**
     * Método para tornar visíveis dados relacionados ou pertencentes a um
     * objeto clienteProduto e tornar o atributo do preço do produto
     * editável.<br>
     *
     * @param lblNomeCliente : label da textarea nome do cliente
     * @param nomeCliente : textarea contendo o nome do cliente
     * @param lblDescricaoProduto : label da textarea do descrição de um produto
     * @param descricaoProduto : textarea da descrição de um produto
     * @param lblPrecoUniSIVA : label da textarea do preço de venda de um
     * produto
     * @param precoUniSIVA : textarea contendo o preço de venda de um produto
     * @param alterarPrecoProduto : botão para realizar a alteração do preço de
     * venda do produto
     * @param lblPrecoUniSIVABase : label da textarea do preço de compra de um
     * produto
     * @param precoUniSIVABase : textarea contendo o preço de compra/base de um
     * produto
     */
    public static void mostrarDadosClienteProduto(Label lblNomeCliente, TextArea nomeCliente,
            Label lblDescricaoProduto, TextArea descricaoProduto,
            Label lblPrecoUniSIVA, TextArea precoUniSIVA, Button alterarPrecoProduto,
            Label lblPrecoUniSIVABase, TextArea precoUniSIVABase) {

        lblNomeCliente.setVisible(true);
        nomeCliente.setVisible(true);
        lblDescricaoProduto.setVisible(true);
        descricaoProduto.setVisible(true);
        lblPrecoUniSIVA.setVisible(true);
        precoUniSIVA.setVisible(true);
        alterarPrecoProduto.setVisible(true);
        lblPrecoUniSIVABase.setVisible(true);
        precoUniSIVABase.setVisible(true);
    }

    /**
     * Método para fazer reset aos atributos de um objeto do tipo
     * clienteProduto<br>
     * O método recebe o objeto clienteProduto e torna nulos todos os seus
     * atributos. Caso algo corra mal, informa uma mensagem de erro.
     *
     * @param clienteProdutoBase : objeto clienteProduto
     */
    public static void setNullAtributosClienteProdutoBase(clienteProduto clienteProdutoBase) {
        try {
            clienteProdutoBase.setCodProdPa(null);
            clienteProdutoBase.setIdCliente(0);
            clienteProdutoBase.setPrecoUnitSIVA(null);
        } catch (Exception tentativaNullpointer) {
            mensagemErroRecarregarEcraSemParar();
        }
    }

    /**
     * Método para verificar se um cliente tem registos associados na tabela
     * clienteProduto.<br>
     * Método recebe o id do cliente e procura os registos associados na tabela
     * clienteProduto. Caso encontre registos retorna verdadeiro. De resto,
     * retorna falso.
     *
     * @param idCliente : id do cliente a pesquisar
     * @return : retorna verdadeiro caso o cliente tenha registos associados na
     * tabela clienteProduto e falso em caso contrário
     */
    public static boolean existsClienteComProdutos(int idCliente) {
        boolean clienteOK = false;
        try {
            List<clienteProduto> clienteProdutos = prodPorIdCliente(idCliente);
            clienteOK = !clienteProdutos.isEmpty();
        } catch (Exception testePreventivoFalhou) {
            clienteOK = false;
        }
        return clienteOK;
    }

    /**
     * Método para verificar se um registo da tabela clienteProduto existe.<br>
     * Método recebe o id de um cliente e o código do produto, procura por
     * registos na tabela clienteProduto da BD contendo ambos e retorna
     * verdadeiro caso encontre uma correspondência e falso, caso contrário.
     *
     * @param idCliente : id do cliente
     * @param codProdPa : código do produto
     * @return : retorna verdadeiro caso encontre o registo correspondente da
     * tabela clienteProduto e falso em caso contrário
     */
    public static boolean existsProdutoDoCliente(int idCliente, String codProdPa) {
        boolean clienteProdutoOK = false;
        try {
            List<clienteProduto> clienteProdutos = getClienteProdutoCodProdPaIdCliente(idCliente, codProdPa);
            clienteProdutoOK = !clienteProdutos.isEmpty();
        } catch (Exception testePreventivoProdutoFalhou) {
            clienteProdutoOK = false;
        }
        return clienteProdutoOK;
    }

    /**
     * Método para se obter o nome de um cliente através do seu ID.<br>
     * Método recebe um String correspondente ao ID do cliente, pesquisa pelo
     * cliente respectivo na BD e caso encontre uma correspondência, devolve um
     * String correspondente ao nome do cliente.<br>
     * Caso algo corra mal, o utilizador é informado e o ecrã reinicidado.<br>
     *
     * @param idCliente : ID do Cliente
     * @return : retorna um String com o nome do cliente
     */
    public static String getNomeClientePeloID(int idCliente) {
        String nomeCliente = "";
        try {
            List<cliente> clientes = getClienteID(idCliente);
            for (cliente a : clientes) {
                if (a.getIdCliente() == idCliente) {
                    nomeCliente = a.getNome();
                    break;
                }
            }
        } catch (Exception falhouProcuraCliente) {
            mensagemErroRecarregarEcraSemParar();
        }
        return nomeCliente;
    }

    /**
     * Método para obter a descrição de um produto.<br>
     * Método recebe o código de um produto, procura pela produto na BD e caso
     * encontra uma correspondência devolve um String correspondente à descrição
     * do produto. Se algo correr mal, informa o utilizador e reinica o ecrã.
     *
     * @param codProdPa: codigo do produto
     * @return : String contendo a descrição do produto
     */
    public static String getDescricaoProdutoPeloCodProdPa(String codProdPa) {
        produto produto = new produto();
        String descricaoProduto = "";
        try {
            List<produto> produtos = pesquisarProdutoID(codProdPa);
            for (produto a : produtos) {
                if (a.getCodProdPa().equals(codProdPa)) {
                    descricaoProduto = a.getDescricao();
                    break;
                }
            }
        } catch (Exception falhouProcuraProduto) {
            mensagemErroRecarregarEcraSemParar();
        }
        return descricaoProduto;
    }

    /**
     * Método para verificar se o novo preço de venda (unitário sem iva) do
     * produto ao cliente é válido<br>
     * Método verifica se o novo preço inserido obdece à regra para números do
     * tipo float. Em caso negativo ou se algo correr mal, retorna falso. Em
     * caso afirmativo, procede ao cálculo do preço de venda mínimo para o
     * produto e compara o novo preço com esse valor mínimo. Caso o novo preço
     * seja menor que o preço de venda mínimo ou se algo correr mal retorna mal.
     * Mas se o novo preço é maior ou igual ao preço de venda mínimo, o método
     * retorna verdadeiro.
     *
     * @param clienteProdutoBase : objeto clienteProduto corresponde ao registo
     * da tabela clienteProduto em foc
     * @param precoUniSIVA : textarea que mostra o preço de venda do produto ao
     * cliente
     * @return : retorna verdadeiro caso o preço de venda do produto ao cliente
     * alterado seja igual ou superior ao preço mínimo de venda do produto e
     * falso em todos os outros casos
     */
    public static boolean isValidNovoPrecoProdutoSIVACliente(clienteProduto clienteProdutoBase, TextArea precoUniSIVA) {
        boolean precoNovoOK = false;
        float precoValidoMin = -1.0f;
        try {
            if (!precoUniSIVA.getText().equals("") && Pattern.compile("^[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)$").
                    matcher(precoUniSIVA.getText()).find() == true && precoUniSIVA.getText().length() <= 30) {

                precoValidoMin = calcularPrecoProdSIVAMin(clienteProdutoBase);

                if (precoValidoMin != -1.0f) {
                    precoNovoOK = Float.parseFloat(precoUniSIVA.getText()) >= precoValidoMin;
                } else {
                    precoNovoOK = false;
                }
            } else {
                precoNovoOK = false;
            }
        } catch (NumberFormatException ee) {
            precoNovoOK = false;
        } catch (Exception ea) {
            precoNovoOK = false;
        }
        return precoNovoOK;
    }

    /**
     * Método para calcular o preço mínimo de venda de um produto a um
     * cliente<br>
     * Método procura pelo preço unitário sem iva do produto na tabela produtos
     * (o preço de compra por parte da empresa) e depois multiplica o mesmo por
     * 120% para se obter um float correspondente a um preço de venda com margem
     * de lucro de 20% e retorna esse valor.<br>
     * Caso algo corra mal, o utilizador é notificado e o ecrã reiniciado.
     *
     * @param clienteProdutoBase : objeto clienteProduto correspondente ao
     * registo da tabela clienteProduto em foco contendo o código do Produto e o
     * seu preço de venda
     * @return : float referente ao preço de venda mínimo do produto (20%
     * superior ao preço de compra)
     */
    public static float calcularPrecoProdSIVAMin(clienteProduto clienteProdutoBase) {
        float precoProdutoMinValido = -1.0f;
        try {
            List<produto> produtos = pesquisarProdutoID(clienteProdutoBase.getCodProdPa());
            for (produto produto : produtos) {
                if (produto.getCodProdPa().equals(clienteProdutoBase.getCodProdPa())) {
                    precoProdutoMinValido = (float) (produto.getPrecoUnitSIVA() * 1.20);
                    break;
                }
            }
        } catch (Exception falhouAConta) {
            mensagemErroRecarregarEcraSemParar();
        }
        return precoProdutoMinValido;
    }

    /**
     * Método para verificar se a alteração de preço de um venda de um produto a
     * um cliente na BD teve sucesso<br>
     * Método recebe os parâmetros referentes ao registo da tabela
     * clienteProduto em foco e verifica se há uma correspondência na base de
     * dados. Em caso afirmativo retorna versdadeiro e caso negativo ou caso
     * algo tenha corrido mal, retorna falso.
     *
     * @param idCliente : id do cliente
     * @param codProdPa : código do produto
     * @param precoProdutoUnitSIVA : preço de venda do produto ao cliente
     * @return : verdadeiro caso a alteração tenha tido sucesso, falso em caso
     * contrário ou caso não seja possível determinar o resultado do teste
     */
    public static boolean isValidAlteracaoPreco(int idCliente, String codProdPa, float precoProdutoUnitSIVA) {
        boolean alteracaoOK = false;
        try {
            List<clienteProduto> clienteAlterado = getClienteProdutoCodProdPaIdCliente(idCliente, codProdPa);
            for (clienteProduto clienteProduto : clienteAlterado) {
                if (clienteProduto.getPrecoUnitSIVA() == precoProdutoUnitSIVA) {
                    alteracaoOK = true;
                    break;
                }
            }
        } catch (Exception e) {
            alteracaoOK = false;
        }
        return alteracaoOK;
    }

    /**
     * Método para enviar o utilizador para o seu menu em caso de falha da
     * aplicação <br>
     *
     * Método deteta o tipo de utilizador consonante o seu nível e reencaminha
     * para o seu menu. Em caso de falha, fecha a aplicação. Este método,
     * semelhante ao método do botão menu, é invocado quando o programa
     * encontrar uma falha na lógica da aplicação (como aquando da atualização
     * do produto) para permitir o utilizador continuar a usar outras
     * funcionalidades da aplicação.
     */
    public static void abrirMenUtilizador() {
        if (login.getNivelLogin() == 1) { //se for operador ir para o menu do operador
            menuOperador menuOp = new menuOperador();
            fecha();
            try {
                menuOp.start(new Stage());
            } catch (Exception ex) {
                mensagemErroFatal();
            }

        } else if (login.getNivelLogin() == 2) {
            menuAdministrador menuAdmin = new menuAdministrador();
            fecha();
            try {
                menuAdmin.start(new Stage());
            } catch (Exception ex) {
                mensagemErroFatal();
            }
        } else {
            mensagemErroFatal();
        }
    }

    /**
     * método para substituir o conteudo do menu de opções para colocar as
     * opções por ordem caso seja o operador a abrir o ecrã
     */
    private void adicionarEcraPessoalOperadorMenuOpcoes() {

        List<utilizador> operadores = procurarEmail(login.getIdLogin());
        for (utilizador operador : operadores) {
            if (operador.getNivel() == 1) {
                opcoes.getItems().clear();
                MenuItem ePOp = new MenuItem("Ecrã Pessoal");
                MenuItem terminarSessao = new MenuItem("Terminar Sessão");
                MenuItem sair = new MenuItem("Sair");
                opcoes.getItems().add(ePOp);
                opcoes.getItems().add(terminarSessao);
                opcoes.getItems().add(sair);
                ePOp.setOnAction((ActionEvent e) -> {
                    try {
                        ecraPessoalOperador ePO = new ecraPessoalOperador();
                        fecha();
                        ePO.start(new Stage());
                    } catch (Exception ex) {
                        Logger.getLogger(removerClienteAssociadoController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
                terminarSessao.setOnAction((ActionEvent e) -> {
                    try {
                        login.setIdLogin(0);
                        login.setNivelLogin(10000);
                        login log = new login();
                        fecha();
                        log.start(new Stage());
                    } catch (Exception ex) {
                        Logger.getLogger(removerClienteAssociadoController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
                sair.setOnAction((ActionEvent e) -> {
                    System.exit(0);
                });
            }
        }
    }

    /**
     * Mensagem de erro para tentar fechar em segurança a aplicação no caso
     * desta encontrar um erro muito grave<br>
     * Se este erro for lançado é porque algo inesperado ocorreu (ex: falha de
     * acesso à base de dados, lançamento de alguns excepções em métodos
     * críticos ou falhas gerais e básicas de programação)<br>
     * É a mensagem de erro de severidade mais elevada deste ecrã
     */
    public static void mensagemErroFatal() {
        Alert a = new Alert(Alert.AlertType.NONE);
        a.setAlertType(Alert.AlertType.ERROR);
        a.setTitle("ERRO Fatal");
        a.setHeaderText("A aplicação comportou-se de forma inesperada!");
        a.setContentText("Encerrando a aplicação");
        a.getButtonTypes().setAll(ButtonType.OK);
        Optional<ButtonType> result3 = a.showAndWait();
        if (result3.get() == ButtonType.OK) {
            System.exit(0);
        }
    }

    /**
     * Método para obter o preço base/compra de um produto.<br>
     * Método recebe o código de um produto, procura pela produto na BD e caso
     * encontra uma correspondência devolve um String correspondente ao preço
     * base do produto. Se algo correr mal, informa o utilizador e reinica o
     * ecrã.
     *
     * @param codProdPa: codigo do produto
     * @return : String contendo o preço base do produto
     */
    public static String getPrecoProdutoPeloCodProdPa(String codProdPa) {
        produto produto = new produto();
        String precoProduto = "";
        try {
            List<produto> produtos = pesquisarProdutoID(codProdPa);
            for (produto a : produtos) {
                if (a.getCodProdPa().equals(codProdPa)) {
                    precoProduto = String.valueOf(a.getPrecoUnitSIVA());
                    break;
                }
            }
        } catch (Exception falhouProcuraProduto) {
            mensagemErroRecarregarEcraSemParar();
        }
        return precoProduto;
    }

    /**
     * Método para mostrar a unidade de compra/venda de um produto.<br>
     * Método recebe o código de um produto e procura pela produto na BD. Caso
     * encontra uma correspondência e verifica se este é tem unidade declarada
     * ou não.<br>
     * Caso tenha unidade, altera o label dos preços (compra e venda) para
     * mostrar essa unidade. Caso não tenha unidade declarada, assume como
     * unidade, o tipo de stock e altera o label dos preços (compra e venda)
     * para mostrar esse tipo de stock.<br>
     * Se algo correr mal, informa o utilizador e reinica o ecrã.
     *
     * @param codProdPa: codigo do produto
     * @param precoBase : label do preço base/compra do produto
     * @param precoVenda : label do preço de venda do produto
     */
    public static void modificarLabelsPrecoTipoUnidade(String codProdPa, Label precoBase, Label precoVenda) {
        produto produto = new produto();
        try {
            List<produto> produtos = pesquisarProdutoID(codProdPa);
            for (produto a : produtos) {
                if (a.getCodProdPa().equals(codProdPa)) {
                    if (!a.getTipoQuantStockUnit().equals("") && a.getTipoQuantStockUnit() != null) {
                        precoBase.setText("Preço Base Unitário (" + a.getTipoQuantStockUnit() + ") Sem IVA:");
                        precoVenda.setText("Preço Venda Unitário (" + a.getTipoQuantStockUnit() + ") Sem IVA:");
                    } else {
                        precoBase.setText("Preço Base Unitário (" + a.getTipoQuantStock() + ") Sem IVA:");
                        precoVenda.setText("Preço Venda Unitário (" + a.getTipoQuantStock() + ") Sem IVA:");
                    }
                    break; //pode haver erros daí o break
                }
            }
        } catch (Exception falhouProcuraUnidadeProduto) {
            mensagemErroRecarregarEcraSemParar();
        }
    }

}
