/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import bfgir.alterarCategorias;
import bfgir.ecraPessoalOperador;
import bfgir.login;
import bfgir.menuAdministrador;
import bfgir.menuOperador;
import classes.categoria;
import static classes.categoria.atualizarDescricaoCategoria;
import static classes.categoria.atualizarEstadoCategoria;
import static classes.categoria.atualizarNivelCategoria;
import static classes.categoria.atualizarNomeCategoria;
import static classes.categoria.listarCategorias;
import static classes.categoria.listarCategoriasPorId;
import static classes.categoria.procurarCategoriaID;
import classes.helper;
import classes.produto;
import static classes.produto.pesquisarProdutoIDCategoria;
import classes.produtoSubCategoria;
import static classes.produtoSubCategoria.getProdutosComCategoria;
import classes.utilizador;
import static classes.utilizador.procurarEmail;
import static controllers.inserirCategoriasController.validarDescricao;
import static controllers.inserirCategoriasController.validarNome;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import static controllers.inserirCategoriasController.isNotEmptyInvalidCatNomeDescricaoNivel;
import java.util.ArrayList;

/**
 * Classe controladora do ecrã alterarCategorias.
 *
 * @author Ilídio Magalhães.
 */
public class alterarCategoriasController implements Initializable {

    //variaveis do ecrã
    private int altura_vbox = 0;
    static Alert a = new Alert(Alert.AlertType.NONE);
    //variáveis para o algoritmo
    static categoria categoriaBase = new categoria();

    @FXML
    private Pane painelStockProdutos;
    @FXML
    private ScrollPane Produtos;
    @FXML
    private MenuBar menu;
    @FXML
    private Label lblDescricao;
    @FXML
    private Label lblEstado;
    @FXML
    private Menu opcoes;
    @FXML
    private ChoiceBox<String> cbestado;

    @FXML
    private VBox vBoxCategorias;
    @FXML
    private Label lblNome;
    @FXML
    private TextArea nomeCat;
    @FXML
    private TextArea descricao;
    @FXML
    private TextField codCategoriaPesquisar;
    @FXML
    private Button pesquisarCodCategoria;
    @FXML
    private Button botaoAtualizarCategoria;
    @FXML
    private ChoiceBox<String> choiceNivel;
    @FXML
    private Label lbNivel;
    @FXML
    private Label id;
    @FXML
    private TextArea idCat;

    /**
     * Método para inicializar o ecrã de alterar categorias de produtos.<br>
     * O método define os valores possíveis para a chombox do estado do produto,
     * esconde os campos alteráveis de um produto, lista todos os produtos pelo
     * seu código para para uma vbox contendo botões, carrega o método contendo
     * listeners de validação dos campos alteráveis de um produto e reformula o
     * menu de opções para incluir o ecrã pessoal do operador caso utilizador
     * seja do tipo operador.<br>
     *
     * @param url url.
     * @param rb rb.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            adicionarEcraPessoalOperadorMenuOpcoes();
            cbestado.getItems().add("ativo");
            cbestado.getItems().add("inativo");
            choiceNivel.getItems().add("1");
            choiceNivel.getItems().add("2");
            choiceNivel.getItems().add("3");
            setVisibleEditableFalse();

            listarTodasCategoriasBD();

            validarCampos();
        } catch (Exception e) {
            Logger.getLogger(alterarCategoriasController.class.getName()).log(Level.SEVERE, null, e);
            mensagemErroFatal();

        }

    }

    /**
     * Método para aceder ao menu do utilizador.<br>
     */
    @FXML
    private void openMenu(ActionEvent event) {
        try {
            if (login.getNivelLogin() == 1) {
                menuOperador menuO = new menuOperador();
                fecha();
                menuO.start(new Stage());
            } else {
                menuAdministrador menuAdmin = new menuAdministrador();
                fecha();
                menuAdmin.start(new Stage());
            }
        } catch (Exception ex) {
            Logger.getLogger(alterarCategoriasController.class.getName()).log(Level.SEVERE, null, ex);
            mensagemErroFatal();
        }

    }

    /**
     * Metodo para terminar sessao do utilizador.<br>
     *
     * @param event evento.
     */
    @FXML
    private void terminarSessao(ActionEvent event) {
        try {
            login.setIdLogin(0);
            login.setNivelLogin(10000);
            login log = new login();
            fecha();
            log.start(new Stage());

        } catch (Exception ex) {
            mensagemErroFatal();
            Logger.getLogger(alterarCategoriasController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Método para encerrar a aplicação.<br>
     */
    @FXML
    private void sair(ActionEvent event) {

        helper.opcaoSair(event);
    }

    /**
     * Método para fechar o ecrã.<br>
     */
    private static void fecha() {
        alterarCategorias.getStage().close();
    }

    /**
     *
     * Método para listar todas as categorias existentes na BD.<br>
     *
     *
     * Neste método criada uma lista com todas as categorias obtidas pelo método
     * listarCategorias.<br>De seguida para cada elemento dessa lista de
     * categorias é criado um botão com o id da categoria.<br>
     * Depois de selecionar uma categoria é feito um select com o codigo da
     * mesma através do método listarDetalhesCategoria para listar alguns dados
     * da categoria no ecra.<br> Esses dados ficam visíveis após a execução do
     * método setVisibleEditableTrue.<br>
     *
     */
    private void listarTodasCategoriasBD() {
        List<categoria> categorias = listarCategorias();
        for (categoria categoria : categorias) {
            int idCategoria = categoria.getIdCategoria();
            Button botao = new Button(String.valueOf(idCategoria));
            botao.setId("botaoVbox");
            botao.setPrefHeight(30);
            botao.setPrefWidth(vBoxCategorias.getPrefWidth());
            botao.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {
                    botao.setId("botaoVboxChangeCollour");

                    listarDetalhesCategoria(idCategoria);
                    setVisibleEditableTrue();

                }
            });
            altura_vbox = altura_vbox + 30;
            vBoxCategorias.getChildren().addAll(botao);
            if (altura_vbox > 548) {
                vBoxCategorias.setPrefHeight(altura_vbox);
            }

        }
    }

    /**
     * Método para mostrar os atributos da categoria quando carregados e
     * torná-los editáveis à excepção do id.<br>
     */
    private void setVisibleEditableTrue() {
        id.setVisible(true);
        idCat.setVisible(true);
        lblNome.setVisible(true);
        nomeCat.setVisible(true);
        nomeCat.setEditable(true);
        lblDescricao.setVisible(true);
        descricao.setVisible(true);
        descricao.setEditable(true);
        lblEstado.setVisible(true);
        cbestado.setVisible(true);
        lbNivel.setVisible(true);
        choiceNivel.setVisible(true);
        botaoAtualizarCategoria.setVisible(true);
    }

    /**
     * Método para esconder e bloquear a edição dos atributos ainda não
     * carregados.<br>
     */
    private void setVisibleEditableFalse() {
        id.setVisible(false);
        idCat.setVisible(false);
        idCat.setEditable(false);
        lblNome.setVisible(false);
        nomeCat.setVisible(false);
        nomeCat.setEditable(false);
        lblDescricao.setVisible(false);
        descricao.setVisible(false);
        descricao.setEditable(false);
        lblEstado.setVisible(false);
        cbestado.setVisible(false);
        lbNivel.setVisible(false);
        choiceNivel.setVisible(false);
        botaoAtualizarCategoria.setVisible(false);
    }

    /**
     * Método para substituir o conteudo do menu de opções para colocar as
     * opções por ordem caso seja o operador a abrir o ecrã.<br>
     * Método recebe um objeto do tipo menu referente ao menu de opções e depois
     * refaz o mesmo com as novas opções consonante o utilizador seja operador
     * ou administrador.<br>
     */
    private void adicionarEcraPessoalOperadorMenuOpcoes() {

        List<utilizador> operadores = procurarEmail(login.getIdLogin());
        for (utilizador operador : operadores) {
            if (operador.getNivel() == 1) {

                opcoes.getItems().clear();
                MenuItem ePOp = new MenuItem("Ecrã Pessoal");
                MenuItem terminarSessao = new MenuItem("Terminar Sessão");
                MenuItem sair = new MenuItem("Sair");
                opcoes.getItems().add(ePOp);
                opcoes.getItems().add(terminarSessao);
                opcoes.getItems().add(sair);
                ;

                ePOp.setOnAction((ActionEvent e) -> {
                    try {
                        ecraPessoalOperador ePO = new ecraPessoalOperador();
                        fecha();
                        ePO.start(new Stage());
                    } catch (Exception ex) {
                        mensagemErroFatal();
                    }
                });
                terminarSessao.setOnAction((ActionEvent e) -> {
                    try {
                        login.setIdLogin(0);
                        login.setNivelLogin(10000);
                        login log = new login();
                        fecha();
                        log.start(new Stage());
                    } catch (Exception ex) {
                        mensagemErroFatal();
                    }
                });
                sair.setOnAction((ActionEvent e) -> {
                    System.exit(0);
                });
            }
        }
    }

    /**
     * Método para listar o nome, descrição, e o estado da categoria de produto
     * <br>
     * O método valida o código da categoria e os atributos a serem alterados.
     * Se um destes atributos da categoria, por algum motivo, for nulo ou
     * estiver vazio, o utilizador é direcionado para o seu menu.<br>
     *
     *
     * @param idCategoria : código da categoria.
     */
    public void listarDetalhesCategoria(int idCategoria) {
        setVisibleEditableFalse();
        choiceNivel.setValue("");
        cbestado.setValue("");

        List<categoria> listaDetalhesCategoria = procurarCategoriaID(idCategoria);
        for (categoria categoria : listaDetalhesCategoria) {
            categoriaBase.setIdCategoria(categoria.getIdCategoria());
            if (categoriaBase.getIdCategoria() == 0) {
                mensagemErroPassagemMenUtilizador();
            } else {
                idCat.setText(String.valueOf(categoriaBase.getIdCategoria()));
                nomeCat.setText(categoria.getNomeCategoria());
                descricao.setText(categoria.getDescricaoCategoria());
                choiceNivel.setValue(String.valueOf(categoria.getNivelCategoria()));
                cbestado.setValue(categoria.getEstado());
                categoriaBase.setIdCategoria(idCategoria);
                categoriaBase.setNomeCategoria(categoria.getNomeCategoria());
                categoriaBase.setDescricaoCategoria(categoria.getDescricaoCategoria());
                categoriaBase.setNivelCategoria(categoria.getNivelCategoria());
                categoriaBase.setEstado(categoria.getEstado());

                if (nomeCat.getText().equals("") || nomeCat.getText() == null
                        || descricao.getText().equals("") || descricao.getText() == null
                        || choiceNivel.getValue().equals("") || choiceNivel.getValue() == null
                        || cbestado.getValue().equals("") || cbestado.getValue() == null) {

                    mensagemErroPassagemMenUtilizador();
                } else {
                }

            }

        }
    }

    /**
     * Método que executa validações aos campos manipuláveis pelo o utilizador.
     * <br>
     * Método usa listeners para verificar se o nome e a descrição e o estado
     * nova categoria assumem ou não valores válidos e não vazios/em branco.<br>
     * Validações de lógica de negócio são realizadas em métodos à parte.<br>
     */
    private void validarCampos() {
        try {
            nomeCat.focusedProperty().addListener((arg0, oldValue, newValue) -> {
                try {
                    if (!newValue) {
                        try {
                            if (!nomeCat.getText().equals("") && Pattern.compile("^\\s*[A-Za-z0-9]+(?:\\s+[A-Za-z0-9]+)*$").matcher(nomeCat.getText()).find() == false
                                    || nomeCat.getText().length() > 60) {
                                a.setAlertType(Alert.AlertType.WARNING);
                                a.getButtonTypes().setAll(ButtonType.OK);
                                a.setTitle("Nome da Categoria De Produto Inválida");
                                a.setHeaderText("Formato do nome da categoria de produto inválido");
                                a.setContentText("Insira um nome válido para a categoria de produto");
                                a.show();
                                nomeCat.setText(categoriaBase.getNomeCategoria());
                            }
                        } catch (IllegalArgumentException pi) {
                            Logger.getLogger(alterarCategoriasController.class.getName()).log(Level.SEVERE, null, pi);
                            mensagemErroRecarregarEcra();
                        }
                    }
                } catch (Exception e) {
                    nomeCat.setText(categoriaBase.getNomeCategoria());
                    mensagemErroRecarregarEcra();
                }
            }
            );

            descricao.focusedProperty().addListener((arg0, oldValue, newValue) -> {
                try {
                    if (!newValue) {
                        try {
                            if (!descricao.getText().equals("") && Pattern.compile("[\\p{L}\\p{Nd}]*").
                                    matcher(descricao.getText()).find() == false || descricao.getText().length() > 100) {
                                a.setAlertType(Alert.AlertType.WARNING);
                                a.getButtonTypes().setAll(ButtonType.OK);
                                a.setTitle("Descrição da Categoria De Produto Inválida");
                                a.setHeaderText("Formato da descrição da categoria de produto inválido");
                                a.setContentText("Insira uma descrição válida para a categoria de produto");
                                a.show();
                                descricao.setText(categoriaBase.getDescricaoCategoria());
                            }
                        } catch (Exception pi) {
                            mensagemErroRecarregarEcra();
                        }
                    }
                } catch (Exception e) {
                    descricao.setText(categoriaBase.getDescricaoCategoria());
                    mensagemErroRecarregarEcra();
                }
            }
            );

            //validar o estado da categoria
            cbestado.focusedProperty()
                    .addListener((arg0, oldValue, newValue) -> {
                        try {
                            if (!newValue) {
                                if (cbestado.getValue().equals("") || cbestado.getValue() == null) {
                                    a.setAlertType(Alert.AlertType.WARNING);
                                    a.getButtonTypes().setAll(ButtonType.OK);
                                    a.setTitle("Estado da Categoria Inválida");
                                    a.setHeaderText("O estado da categoria de produto assumiu um valor inválido");
                                    a.setContentText("Retornando ao estado inicial");
                                    a.show();
                                    cbestado.setValue(categoriaBase.getEstado());
                                } else if (!cbestado.getValue().equals(categoriaBase.getEstado())
                                        && cbestado.getValue().equals("inativo")
                                        && hasProductsCategoria(categoriaBase.getIdCategoria()) == true) {
                                    a.setAlertType(Alert.AlertType.WARNING);
                                    a.getButtonTypes().setAll(ButtonType.OK);
                                    a.setTitle("Estado da Categoria Inválid0");
                                    a.setHeaderText("A categoria tem produtos associados");
                                    a.setContentText("Não pode inativar uma categoria com produtos associados");
                                    a.show();
                                    cbestado.setValue(categoriaBase.getEstado());
                                }
                            }
                        } catch (Exception e) {
                            cbestado.setValue(categoriaBase.getEstado());
                            mensagemErroRecarregarEcra();
                        }
                    }
                    );

            choiceNivel.focusedProperty()
                    .addListener((arg0, oldValue, newValue) -> {
                        try {
                            if (!newValue) {
                                try {
                                    int nivel = 0;
                                    try {
                                        nivel = Integer.parseInt(choiceNivel.getValue());
                                    } catch (NumberFormatException ab) {
                                        choiceNivel.setValue(String.valueOf(categoriaBase.getNivelCategoria()));
                                        mensagemErroRecarregarEcra();
                                    }
                                    if (choiceNivel.getValue().equals("") || choiceNivel.getValue() == null
                                            || nivel < 1 || nivel > 3) {
                                        a.setAlertType(Alert.AlertType.WARNING);
                                        a.getButtonTypes().setAll(ButtonType.OK);
                                        a.setTitle("Nível de Categoria Inválido");
                                        a.setHeaderText("O nível da categoria assumiu um valor inválido");
                                        a.setContentText("A categoria só pode assumir um de 3 níveis: 1, 2 ou 3.\nSe desejar um nível novo,"
                                                + "contacte o administrador");
                                        a.show();
                                        choiceNivel.setValue(String.valueOf(categoriaBase.getNivelCategoria()));
                                    } else if (!choiceNivel.getValue().equals(String.valueOf(categoriaBase.getNivelCategoria()))
                                            && (nivel >= 1 && nivel <= 3)
                                            && hasProductsCategoria(categoriaBase.getIdCategoria()) == true) {
                                        a.setAlertType(Alert.AlertType.WARNING);
                                        a.getButtonTypes().setAll(ButtonType.OK);
                                        a.setTitle("Nivel de Categoria Inválido");
                                        a.setHeaderText("A categoria tem produtos associados!");
                                        a.setContentText("Não pode alterar o nível de uma categoria com produtos associados.");
                                        a.show();
                                        choiceNivel.setValue(String.valueOf(categoriaBase.getNivelCategoria()));
                                    }
                                } catch (Exception pi) {
                                    choiceNivel.setValue(String.valueOf(categoriaBase.getNivelCategoria()));
                                    mensagemErroRecarregarEcra();

                                }
                            }
                        } catch (Exception e) {
                            choiceNivel.setValue(String.valueOf(categoriaBase.getNivelCategoria()));
                            mensagemErroRecarregarEcra();
                        }
                    }
                    );

        } catch (Exception podeEstourar) {
            Logger.getLogger(alterarCategoriasController.class.getName()).log(Level.SEVERE, null, podeEstourar);
            mensagemErroRecarregarEcra();
        }
    }

    /**
     * Método para enviar o utilizador para o seu menu em caso de falha da
     * aplicação.<br>
     *
     * Método deteta o tipo de utilizador consonante o seu nível e reencaminha
     * para o seu menu.<br> Em caso de falha, fecha a aplicação.<br> Este
     * método, semelhante ao método do botão menu, é invocado quando o programa
     * encontrar uma falha na lógica da aplicação (como aquando da atualização
     * do produto) para permitir o utilizador continuar a usar outras
     * funcionalidades da aplicação.<br>
     */
    public static void abrirMenUtilizador() {
        if (login.getNivelLogin() == 1) { //se for operador ir para o menu do operador
            menuOperador menuOp = new menuOperador();
            fecha();
            try {
                menuOp.start(new Stage());
            } catch (Exception ex) {
                mensagemErroFatal();
            }

        } else if (login.getNivelLogin() == 2) {
            menuAdministrador menuAdmin = new menuAdministrador();
            fecha();
            try {
                menuAdmin.start(new Stage());
            } catch (Exception ex) {
                mensagemErroFatal();
            }
        } else {
            mensagemErroFatal();
        }
    }

    /**
     * Método para verificar se o id de uma categoria já existe.<br>
     * O método procura o id da categoria na base de dados e insere as
     * categorias possuidoras desse id (uma no máximo à partida) numa lista de
     * categorias.<br> Caso a lista estiver vazia, signifca que o id e por
     * conseguinte, a categoria não existe na base de dados. Desta forma o
     * método retorna verdadeiro.<br> Caso a lista não esteja vazia, isso signfica
     * que já existe uma categoria com um id correspondente pelo que o método
     * retorna falso.<br>
     * O id da categoria é PK na bd mas não é autoincrementável pelo que este id
     * é gerado pelo programa de forma aleatória.<br> Fez-se um ciclo while até se
     * obter um id aleatório.<br>
     *
     * @param idCategoria : o id da categoria a procurar na forma de uma número
     * aleatório de 4 digitos na forma de string.
     * @return : retorna verdadeiro caso a lista esteja vazia (id único) e falso
     * caso a lista esteja diferente de vazia (id não único).
     */
    public static boolean isNewIdCategoria(int idCategoria) {
        try {
            List<categoria> categorias = procurarCategoriaID(idCategoria);
            return categorias.isEmpty();
        } catch (Exception e) {
            return false; // mensagem de erro no método que chama isto caso não seja true
        }
    }

    /**
     * Método para procurar uma categoria na BD e listar os seus atributos.<br>
     * Método valida o id da categoria e verifica se esta existe e depois lista
     * os seus atributos caso a mesma exista.<br>
     *
     */
    @FXML
    private void pesquisarCodCategoria(ActionEvent event) {
        try {
            Alert alertaDeNovo = new Alert(Alert.AlertType.NONE);
            if (!codCategoriaPesquisar.getText().equals("") && Pattern.compile("^[1-9]+[0-9]*$").matcher(codCategoriaPesquisar.getText()).find() == true
                    && isNewIdCategoria(Integer.parseInt(codCategoriaPesquisar.getText())) == false && codCategoriaPesquisar.getText().length() <= 10) {
                int idCat = 0;
                try {
                    idCat = Integer.parseInt(codCategoriaPesquisar.getText());
                } catch (NumberFormatException ab) {
                    mensagemErroRecarregarEcra();
                }
                listarDetalhesCategoria(idCat);
                setVisibleEditableTrue();
                codCategoriaPesquisar.setText("");
            } else {
                alertaDeNovo.setAlertType(Alert.AlertType.WARNING);
                alertaDeNovo.getButtonTypes().setAll(ButtonType.OK);
                alertaDeNovo.setTitle("ID de categoria de produto Inválido");
                alertaDeNovo.setHeaderText("Categoria não existe ou inseriu paramêtros incorretos");
                alertaDeNovo.setContentText("Insira um ID na forma de número inteiro");
                Optional<ButtonType> result = alertaDeNovo.showAndWait();
                if (result.get() == ButtonType.OK) {
                    codCategoriaPesquisar.setText("");
                }
            }
        } catch (NumberFormatException e) {
            Alert alertaDeNovo = new Alert(Alert.AlertType.NONE);
            alertaDeNovo.setAlertType(Alert.AlertType.WARNING);
            alertaDeNovo.getButtonTypes().setAll(ButtonType.OK);
            alertaDeNovo.setTitle("ID de categoria de produto Inválido");
            alertaDeNovo.setHeaderText("Categoria não existe ou inseriu paramêtros incorretos");
            alertaDeNovo.setContentText("Insira um ID na forma de número inteiro com 10 digitos (máx)");
            Optional<ButtonType> result = alertaDeNovo.showAndWait();
            if (result.get() == ButtonType.OK) {
                codCategoriaPesquisar.setText("");
            }
        }
    }

    /**
     * Mensagem de erro para tentar fechar em segurança a aplicação no caso
     * desta encontrar um erro muito grave.<br>
     * Se este erro for lançado é porque algo inesperado ocorreu (ex: falha de
     * acesso à base de dados, lançamento de alguns excepções em métodos
     * críticos ou falhas gerais e básicas de programação).<br>
     * É a mensagem de erro de severidade mais elevada deste ecrã.<br>
     */
    public static void mensagemErroFatal() {
        a.setAlertType(Alert.AlertType.ERROR);
        a.setTitle("ERRO Fatal");
        a.setHeaderText("A aplicação comportou-se de forma inesperada!");
        a.setContentText("Encerrando a aplicação");
        a.getButtonTypes().setAll(ButtonType.OK);
        Optional<ButtonType> result3 = a.showAndWait();
        if (result3.get() == ButtonType.OK) {
            System.exit(0);
        }
    }

    /**
     * Método para lançar uma mensagem de erro caso um erro caso algo essencial
     * falhe na funcionalidade.<br>
     * Se este erro é lançado é porque a funcionalidade pode estar inoperável
     * temporaria ou permanentemente.<br>
     * É mensagem de erro de severidade intermédia do ecrã.<br>
     */
    public static void mensagemErroPassagemMenUtilizador() {
        a.setAlertType(Alert.AlertType.ERROR);
        a.setTitle("ERRO Fatal");
        a.setHeaderText("Impossível validar os dados do ecrã!");
        a.setContentText("Algumas funcionalidades do aplicação estão inacessíveis");
        a.getButtonTypes().setAll(ButtonType.OK);
        Optional<ButtonType> result3 = a.showAndWait();
        if (result3.get() == ButtonType.OK) {
            abrirMenUtilizador();
        }
    }

    /**
     * Método para lançar uam mensagem de erro e recarregar o ecrã caso algo
     * inesperado ocorra com o algoritmo.<br>
     * Método usado quando as falhas são graves mas não graves de mais que a
     * funcionalidade se encontre inutilizável.<br>
     * É a mensagem de erro de severidade mais leve neste ecrã.<br>
     */
    public static void mensagemErroRecarregarEcra() {
        Alert alertaNovo = new Alert(Alert.AlertType.NONE);
        alertaNovo.setAlertType(Alert.AlertType.ERROR);
        alertaNovo.getButtonTypes().setAll(ButtonType.OK);
        alertaNovo.setTitle("ERRO Lógica Aplicação");
        alertaNovo.setHeaderText("Os dados processados não são válidos");
        alertaNovo.setContentText("Ocorreu um erro ao validar os dados selecionados ou estes podem \nestar inacessíveis. "
                + "Tente novamente e caso não conseguir contacte o administrador");
        Optional<ButtonType> resulta = alertaNovo.showAndWait();
        if (resulta.get() == ButtonType.OK) {
            recarregarEcra();
        }

    }

    /**
     * Método para recarregar o ecrã de alterar categorias.<br>
     */
    private static void recarregarEcra() {
        alterarCategorias altCat = new alterarCategorias();
        fecha();
        try {
            altCat.start(new Stage());
        } catch (Exception ex) {
            mensagemErroFatal();
        }
    }

    /**
     * Método de alterar uma categoria.<br>
     * Método primeiro obtém a lista de categorias existente na bd. Caso se
     * obtenha uma lista vazia, o utilizador é enviado para o seu menu. Caso a
     * lista esteja completa, o método vai verificar se os atributos nome e
     * descrição contêm valores e envia uma mensagem de erro caso um ou ambos
     * estejam vazios.<br> Caso os campos contenham dados, o método procede à
     * atualização dos atributos caso estes tenham sido alterados.<br>
     * Para um novo nome, verifica se este já existe na BD e não vai alterar
     * nesse caso, marcando a ação como falhada.<br> Caso o nome seja único, aí sim,
     * já atualiza.<br> Depois, o método vai verificar se a atualização foi bem
     * sucedida e em caso afirmativo marca a operação como tendo tido sucesso e
     * em caso negativo marca a operação como falhada.<br>
     * Para uma nova descrição é semelhante.<br> Caso a descrição seja diferente da
     * atual, o método verifica se a descrição é única embora dando a
     * possibilidade de manter a mesma.<br> Depois, procede à atualização da
     * descrição e verifica se esta atualização ocorreu.<br> Em caso afirmativo
     * marca a operação como sucesso e em caso negativo marca a operação como
     * falhada<br>
     * Para um novo estado é algo diferente.<br> O estado foi previamente validado
     * para verificar se a categoria tem produtos ativos associados só sendo
     * permitida a atualização do estado de categorias sem produtos ativos.<br>
     * Assim, para alterar o estado da categoria, este método só procede à
     * atualização do mesmo e a uma verificação dessa mesma atualização.<br> Caso a
     * operação tenha sido bem sucesso, esta é marcada como sucesso e caso não
     * tenho sido bem sucedida, é marcada como falhada.<br>
     * Já para um novo nível, o método tenta converter o valor da choicebox em
     * int para inserção na BD pois o nível já foi validado para verificar se a
     * categoria (não) tem produtos associados.<br> Depois disso, o método faz a
     * atualização do nível e verifica se esta teve sucesso, marcando a operação
     * como tendo sucesso ou não.<br>
     * Depois das atualizações, procede-se ao tratamento dos identificadores.<br>
     * Caso não se tenham procedido a nenhuma alteração (os identificadores não
     * mudam de valores), o utilizador é notificado do ocorrido e o ecrã
     * recarregado.<br> Caso alguma falha de atualização tenha ocorrido, o
     * utilizador é notificado e o ecrã recarregado, tendo a atualização do nome
     * um tratamento personzalizado.<br> Caso a atualização tenha sido bem sucedida,
     * o utilizador é notificado do sucesso e é convidado a alterar uma nova
     * categoria ou passar ao seu menu de utilizador.<br> Caso tenha surgido outra
     * combinação de dados, o utilizador é enviado para o seu menu de utilizador
     * assim como quando for lançado um erro capturado pela escepção.<br>
     */
    @FXML
    private void botaoAtualizarCategoria(ActionEvent event) {
        String estadoCategoriaNomePesquisa = "ativo";
        int falhou = 0;
        int sucesso = -1;
        int nivelCat = 0;
        int nomeFalhou = 0;
        try {
            List<categoria> categorias = listarCategorias();
            if (!categorias.isEmpty()) {
                if (isNotEmptyInvalidCatNomeDescricaoNivel(nomeCat.getText(), descricao.getText(), choiceNivel.getValue()) == true) {
                    if (isNewIdCategoria(categoriaBase.getIdCategoria()) == false) {
                        if (!nomeCat.getText().equals(categoriaBase.getNomeCategoria()) && falhou == 0) {
                            if (validarNome(nomeCat.getText(), categorias, estadoCategoriaNomePesquisa) == true) {
                                atualizarNomeCategoria(nomeCat.getText(), categoriaBase.getIdCategoria());
                                if (isUpdatedNomeCat(nomeCat.getText(), categoriaBase.getIdCategoria()) == true) {
                                    sucesso = 1;
                                } else {
                                    falhou = 1;
                                }
                            } else {
                                falhou = 1;
                                nomeFalhou = 1;
                            }
                        }

                        if (!descricao.getText().equals(categoriaBase.getDescricaoCategoria()) && falhou == 0) {
                            if (validarDescricao(descricao.getText(), categorias) == true) {
                                atualizarDescricaoCategoria(descricao.getText(), categoriaBase.getIdCategoria());
                                if (isUpdatedDescricaoCat(descricao.getText(), categoriaBase.getIdCategoria()) == true) {
                                    sucesso = 1;
                                } else {
                                    falhou = 1;
                                }
                            } else {
                                falhou = 1;
                            }
                        }

                        if (!cbestado.getValue().equals(categoriaBase.getEstado()) && falhou == 0) {
                            atualizarEstadoCategoria(cbestado.getValue(), categoriaBase.getIdCategoria());
                            if (isUpdatedEstadoCat(cbestado.getValue(), categoriaBase.getIdCategoria()) == true) {
                                sucesso = 1;
                            } else {
                                falhou = 1;
                            }
                        }

                        if (!choiceNivel.getValue().equals(String.valueOf(categoriaBase.getNivelCategoria())) && falhou == 0) {
                            nivelCat = getNivelCategoria(choiceNivel.getValue());
                            if (nivelCat != 0) {
                                atualizarNivelCategoria(nivelCat, categoriaBase.getIdCategoria());
                                if (isUpdatedNivelCat(nivelCat, categoriaBase.getIdCategoria()) == true) {
                                    sucesso = 1;
                                } else {
                                    falhou = 1;
                                }
                            } else {
                                falhou = 1;
                            }
                        }

                        //tratamento de respostas
                        if (falhou == 0 && sucesso == 1) {
                            mensagemSucessoCategoria(categoriaBase.getIdCategoria());
                        } else if (sucesso == -1 && falhou == 0) {
                            mensagemCategoriasInalteradas(categoriaBase.getIdCategoria());
                        } else if (falhou == 1 && nomeFalhou == 0) {
                            mensagemErroInsucessoAtualizarCategoria(categoriaBase.getIdCategoria());
                        } else if (falhou == 1 && nomeFalhou == 1) {
                            //mensagem de erro já vem do método de validar o nome
                            recarregarEcra();
                        } else {
                            mensagemErroFatal();
                        }

                    } else {
                        mensagemErroCategoriaEliminada();
                    }

                } else {
                    mensagemErroRecarregarEcraCamposVazios();
                }

            } else {
                mensagemErroPassagemMenUtilizador();

            }
        } catch (Exception estourouUpdate) {
            mensagemErroPassagemMenUtilizador();
        }

    }

    /**
     * Método para lançar uma mensagem de erro e recarregar o ecrã caso o
     * utilizador deixe campos em brancos.<br>
     * Método derivado do mensagemErroRecarregarEcraCamposVazios() do ecra de
     * inserir categorias com mensagem personalizada para ser chamado apenas no
     * começo do processo de inserção de categoria nova.<br>
     * É uma das duas mensagens de erro de severidade mais leve neste ecrã.<br>
     */
    private static void mensagemErroRecarregarEcraCamposVazios() {
        Alert alertaNovo = new Alert(Alert.AlertType.NONE);
        alertaNovo.setAlertType(Alert.AlertType.ERROR);
        alertaNovo.getButtonTypes().setAll(ButtonType.OK);
        alertaNovo.setTitle("ERRO Lógica Aplicação");
        alertaNovo.setHeaderText("Os dados processados não são válidos");
        alertaNovo.setContentText("Tem de atribuir um nome e uma descrição à categoria que está a alterar");
        Optional<ButtonType> resulta = alertaNovo.showAndWait();
        if (resulta.get() == ButtonType.OK) {
            recarregarEcra();
        }

    }

    /**
     * Método para lançar uma mensagem de erro caso a categoria não se encontrar
     * na BD.<br>
     * Método chama também o método de recarregar o ecrã.<br>
     */
    private static void mensagemErroCategoriaEliminada() {
        Alert alertaNovo = new Alert(Alert.AlertType.NONE);
        alertaNovo.setAlertType(Alert.AlertType.ERROR);
        alertaNovo.getButtonTypes().setAll(ButtonType.OK);
        alertaNovo.setTitle("ERRO Lógica Aplicação");
        alertaNovo.setHeaderText("A categoria já não existe ou temporariamente indisponível");
        alertaNovo.setContentText("Ocorreu um erro ao validar a categoria ou esta pode \nnão estr estar inacessível. "
                + "Tente aceder novamente e caso não conseguir contacte o administrador");
        Optional<ButtonType> resulta = alertaNovo.showAndWait();
        if (resulta.get() == ButtonType.OK) {
            recarregarEcra();
        }
    }

    /**
     * Método para verificar se o nome de uma categoria foi atualizado.<br>
     * Método procura a categoria na BD pelo seu id e caso não a encontre lança
     * uma mensagem de erro para recarregar o ecrã.<br> Caso encontre a categoria,
     * verifica se há uma correspondência entre o nome da categoria com a aquela
     * encontrada na BD. Em caso positivo, retorna verdadeiro.<br>
     *
     * @param nome : nome atualizado da categoria.
     * @param idCat : id da categoria.
     * @return verdadeiro caso o nome atual da categoria corresponda ao nome
     * inserido para atualização e falso caso, ou não encontre a categoria, ou
     * caso o nome atual da categoria não corresponda ao nome inserido para
     * atualização.
     */
    private static boolean isUpdatedNomeCat(String nome, int idCat) {
        boolean newNomeOK = false;
        List<categoria> categoriaUpdated = listarCategoriasPorId(idCat);
        if (categoriaUpdated.isEmpty() == false) {
            for (categoria categoria : categoriaUpdated) {
                if (categoria.getNomeCategoria().equals(nome)) {
                    newNomeOK = true;
                }
            }
        } else {
            newNomeOK = false;
        }
        return newNomeOK;
    }

    /**
     * Método para verificar se a descriçao de uma categoria foi atualizada.<br>
     * Método procura a categoria na BD pelo seu id e caso não a encontre lança
     * uma mensagem de erro para recarregar o ecrã.<br> Caso encontre a categoria,
     * verifica se há uma correspondência entre a descrição da categoria com a
     * aquela encontrada na BD.<br> Em caso positivo, retorna verdadeiro.<br>
     *
     * @param descricao : descricao da categoria.
     * @param idCat : id da categoria.
     * @return verdadeiro caso a descrição atual da categoria corresponda à
     * descrição inserido para atualização e falso caso, ou não encontre a
     * categoria, ou caso a descrição atual da categoria não corresponda à
     * descrição inserida para atualização.
     */
    private static boolean isUpdatedDescricaoCat(String descricao, int idCat) {
        boolean newDescricaoOK = false;
        List<categoria> categoriaUpdated = listarCategoriasPorId(idCat);
        if (categoriaUpdated.isEmpty() == false) {
            for (categoria categoria : categoriaUpdated) {
                if (categoria.getDescricaoCategoria().equals(descricao)) {
                    newDescricaoOK = true;
                }
            }
        } else {
            newDescricaoOK = false;
        }
        return newDescricaoOK;
    }

    /**
     * Método para verificar se o estado de uma categoria foi atualizado.<br>
     * Método procura a categoria na BD pelo seu id e caso não a encontre lança
     * uma mensagem de erro para recarregar o ecrã.<br> Caso encontre a categoria,
     * verifica se há uma correspondência entre o estado da categoria com a
     * aquele encontrada na BD.<br> Em caso positivo, retorna verdadeiro.<br>
     *
     * @param estado : estado da categoria.
     * @param idCat : id da categoria.
     * @return verdadeiro caso o estado atual da categoria corresponda ao estado
     * inserido para atualização e falso caso, ou não encontre a categoria, ou
     * caso o estado atual da categoria não corresponda ao estado inserido para
     * atualização.
     */
    private static boolean isUpdatedEstadoCat(String estado, int idCat) {
        boolean newEstadoOK = false;
        List<categoria> categoriaUpdated = listarCategoriasPorId(idCat);
        if (categoriaUpdated.isEmpty() == false) {
            for (categoria categoria : categoriaUpdated) {
                if (categoria.getEstado().equals(estado)) {
                    newEstadoOK = true;
                }
            }
        } else {
            newEstadoOK = false;
        }
        return newEstadoOK;
    }

    /**
     * Método para notificar o utilizador do sucesso da atualização de uma
     * categoria e requisitar uma nova acção.<br>
     * Método informa o utilizador do sucesso da operação e pergunta se o
     * utilizador pretende alterar uma nova categoria ou não.<br> Em caso
     * afirmativo, recarrega o ecrã mas em caso negativo envia o utilizador para
     * o menu do utilizador.<br>
     *
     * @param categoria : categoria inserida para leitura do id e do nome.
     */
    private static void mensagemSucessoCategoria(int idCat) {
        Alert alerta = new Alert(Alert.AlertType.NONE);
        alerta.setAlertType(Alert.AlertType.INFORMATION);
        alerta.getButtonTypes().setAll(ButtonType.YES, ButtonType.NO);
        alerta.setTitle("Alterar Categoria");
        alerta.setHeaderText("Categoria atualizada com sucesso!");
        alerta.setContentText("A categoria " + idCat + " foi atualizada com sucesso!"
                + "\nDeseja alterar outra categoria?");
        alerta.setResizable(false);
        Optional<ButtonType> resultA = alerta.showAndWait();
        if (resultA.get() == ButtonType.YES) {
            recarregarEcra();
        } else if (resultA.get() == ButtonType.NO) {
            abrirMenUtilizador();
        } else {
            mensagemErroFatal();
        }
    }

    /**
     * Método para notificar o utilizador da inalteração dos atributos de uma
     * categoria e requisitir uma nova acção.<br>
     * Método informa o utilizador do facto deste não ter efectuado alguma
     * alteração e pergunta se o utilizador pretende alterar uma nova
     * categoria ou não.<br> Em caso afirmativo, recarrega o ecrã mas em caso
     * negativo envia o utilizador para o menu do utilizador.<br>
     *
     * @param idCat : id da categoria.
     */
    private static void mensagemCategoriasInalteradas(int idCat) {
        Alert alerta = new Alert(Alert.AlertType.NONE);
        alerta.setAlertType(Alert.AlertType.INFORMATION);
        alerta.getButtonTypes().setAll(ButtonType.YES, ButtonType.NO);
        alerta.setTitle("Alterar Categoria");
        alerta.setHeaderText("A Categoria não foi atualizada!");
        alerta.setContentText("Não modificou os atributos da categoria " + idCat + "!"
                + "\nDeseja tentar novamente ou alterar outra categoria?");
        alerta.setResizable(false);
        Optional<ButtonType> resultA = alerta.showAndWait();
        if (resultA.get() == ButtonType.YES) {
            recarregarEcra();
        } else if (resultA.get() == ButtonType.NO) {
            abrirMenUtilizador();
        } else {
            mensagemErroFatal();
        }
    }

    /**
     * Método para verificar se uma categoria tem produtos associados<br>
     * Método recebe o id de uma categoria e faz uma procura na BD por produtos
     * contendo o id da categoria nos seus atributos e por produtos com a
     * categoria na sua lista de subcategorias. Caso encontre correspondências
     * vai preencher as listas de objetos produto e produtoSubCategoria.<br>
     * Vai retornar verdadeiro caso uma ou ambas as listas tiverem registos e
     * retornar falso caso ambas não tenha registos ou algo corra mal.<br>
     *
     * @param idCategoria : id da categoria.
     * @return : retorna verdadeiro caso a categoria tenha registos na tabela
     * produto ou na tabela produtosubcategoria e falso em caso contrário.
     */
    public static boolean hasProductsCategoria(int idCategoria) {
        boolean nivelOK = false;
        boolean produtosOK = false;
        boolean categoriasOK = false;
        List<produto> produtos = new ArrayList<produto>();
        List<produtoSubCategoria> listaProdutosCategoria = new ArrayList<produtoSubCategoria>();
        //verificar se a categoria tem produtos
        try {
            produtos = pesquisarProdutoIDCategoria(idCategoria);
            produtosOK = !produtos.isEmpty();
        } catch (Exception e) {
            nivelOK = false;
        }
        //ver se a categoria faz parte da lista de sub categorias associadas ao produto
        try {
            listaProdutosCategoria = getProdutosComCategoria(idCategoria);
            categoriasOK = !listaProdutosCategoria.isEmpty();
        } catch (Exception e2) {
            nivelOK = false;
        }

        if (produtosOK == true || categoriasOK == true) {
            nivelOK = true;
        } else {
            nivelOK = false;
        }

        return nivelOK;
    }

    /**
     * Método para converter o String do nível da categoria em int para
     * manipulação posterior.<br>
     * Método recebe o valor da choicebox do nível e converte-o para int. Caso
     * falhe retorna o valor zero - que depois é validado e caso corra bem
     * retorna o valor correto: 1,2 ou 3.<br>
     *
     * @param nivel : String correspondente ao nivel da categoria visto na
     * choicebox do nivel.<br>
     * @return : retorna o int com o nível da categoria.
     */
    private static int getNivelCategoria(String nivel) {
        int nivelCat = 0;
        try {
            nivelCat = Integer.parseInt(nivel);
        } catch (NumberFormatException aede) {
            nivelCat = 0;
        } catch (Exception ab) {
            nivelCat = 0;
        }
        return nivelCat;
    }

    /**
     * Método para verificar se o nivel de uma categoria foi atualizado.<br>
     * Método procura a categoria na BD pelo seu id e caso não a encontre lança
     * uma mensagem de erro para recarregar o ecrã.<br> Caso encontre a categoria,
     * verifica se há uma correspondência entre o nivel da categoria com aquele
     * encontrada na BD.<br> Em caso positivo, retorna verdadeiro.<br>
     *
     * @param nivel ; nivel da categoria.
     * @param idCat : id da categoria.
     * @return verdadeiro caso o nível atual da categoria corresponda ao nível
     * inserido para atualização e falso caso, ou não encontre a categoria, ou
     * caso o nível atual da categoria não corresponda ao nível inserido para
     * atualização.
     */
    private static boolean isUpdatedNivelCat(int nivel, int idCat) {
        boolean newNivelOK = false;
        List<categoria> categoriaUpdated = listarCategoriasPorId(idCat);
        if (categoriaUpdated.isEmpty() == false) {
            for (categoria categoria : categoriaUpdated) {
                if (categoria.getNivelCategoria() == nivel) {
                    newNivelOK = true;
                }
            }
        } else {
            newNivelOK = false;
        }
        return newNivelOK;
    }

    /**
     * Método para lançar uma mensagem de erro e recarregar o ecrã caso a
     * atualização de um ou mais atributos não tenha sucesso.<br>
     * Método informa do insuceso da atualização da categoria em foco e depois
     * reinicia o ecrã.<br>
     * É uma das duas mensagens de erro de severidade mais leve neste ecrã.<br>
     *
     * @param idCategoria : id da categoria em foco.
     */
    private static void mensagemErroInsucessoAtualizarCategoria(int idCategoria) {
        Alert alertaNovo = new Alert(Alert.AlertType.NONE);
        alertaNovo.setAlertType(Alert.AlertType.ERROR);
        alertaNovo.getButtonTypes().setAll(ButtonType.OK);
        alertaNovo.setTitle("ERRO Lógica Aplicação");
        alertaNovo.setHeaderText("Não foi possível alterar a categoria " + idCategoria);
        alertaNovo.setContentText("Algo correu mal a alterar um ou mais atributos da categoria\n"
                + "A base de dados pode estar indisponível\nTente novamente");
        Optional<ButtonType> resulta = alertaNovo.showAndWait();
        if (resulta.get() == ButtonType.OK) {
            recarregarEcra();
        }

    }

}
