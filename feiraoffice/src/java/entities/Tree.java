/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Ruben
 */
@Entity
@XmlRootElement
public class Tree implements Serializable {

    @Id
    private Produto Produto;
    private List<Categoria> Categoria;

    /**
     *
     */
    public Tree() {
    }

    /**
     *
     * @param Produto recebe um objeto do tipo produto
     * @param Categoria recebe um lista de objetos do tipo categoria
     */
    public Tree(Produto Produto, List<Categoria> Categoria) {
        this.Produto = Produto;
        this.Categoria = Categoria;
    }

    /**
     * @return the Produto
     */
    public Produto getProduto() {
        return Produto;
    }

    /**
     * @param Produto the Produto to set
     */
    public void setProduto(Produto Produto) {
        this.Produto = Produto;
    }

    /**
     * @return the Categoria
     */
    public List<Categoria> getCategoria() {
        return Categoria;
    }

    /**
     * @param Categoria the Categoria to set
     */
    public void setCategoria(List<Categoria> Categoria) {
        this.Categoria = Categoria;
    }

}
