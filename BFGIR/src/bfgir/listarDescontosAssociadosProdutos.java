package bfgir;

import classes.helper;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * classe do ecrã listarDescontosAssociadosProdutos
 */
public class listarDescontosAssociadosProdutos extends Application {

    private static Stage stage;

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/ecras/listarDescontosAssociadosProdutos.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setTitle("F&O - Listar Descontos Associados a Produtos");
        stage.getIcons().add(helper.appIcon);
        stage.setResizable(false);
        stage.show();
        setStage(stage);
    }

    /**
     * método main do ecrã listarDescontosAssociadosProdutos
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * método getStage do ecrã listarFornecedores
     *
     * @return retorna o JAVAFX container do ecrã listarDescontosAssociadosProdutos
     */
    public static Stage getStage() {
        return stage;
    }

    /**
     * método setStage do ecrã listarDescontosAssociadosProdutos
     *
     * @param aStage the aStage to set
     */
    public static void setStage(Stage aStage) {
        stage = aStage;
    }

}
