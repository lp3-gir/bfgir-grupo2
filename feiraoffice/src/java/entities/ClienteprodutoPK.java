/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author ruben
 */
@Embeddable
public class ClienteprodutoPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "idCliente")
    private int idCliente;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "codProdPa")
    private String codProdPa;

    /**
     *
     */
    public ClienteprodutoPK() {
    }

    /**
     *
     * @param idCliente recebe um id de cliente
     * @param codProdPa recebe um código de produto
     */
    public ClienteprodutoPK(int idCliente, String codProdPa) {
        this.idCliente = idCliente;
        this.codProdPa = codProdPa;
    }

    /**
     *
     * @return idCliente
     */
    public int getIdCliente() {
        return idCliente;
    }

    /**
     *
     * @param idCliente
     */
    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    /**
     *
     * @return codProdPa
     */
    public String getCodProdPa() {
        return codProdPa;
    }

    /**
     *
     * @param codProdPa
     */
    public void setCodProdPa(String codProdPa) {
        this.codProdPa = codProdPa;
    }
}
