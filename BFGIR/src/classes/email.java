/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import com.sun.mail.smtp.SMTPTransport;
import java.util.Date;
import java.util.Properties;
import java.util.regex.Pattern;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * Classe email com metodos para enviar emails
 */
public class email {

    private static final String SMTP_SERVER = "smtp.elasticemail.com";
    private static final String USERNAME = "ruben.andre.amorim@gmail.com";
    private static final String PASSWORD = "5B84004FD8E294E1A5F7E67A52843F54190E";

    private static final String EMAIL_FROM = "pass@feiraEoffice.com";
    private static String EMAIL_TO = "";
    private static final String EMAIL_TO_CC = "";

    private static final String EMAIL_SUBJECT = "Pass conta feiraEoffice";
    private static String EMAIL_TEXT = "";

    /**
     * metodo para enviar email para um novo operador ou fornecedor
     *
     * @param email recebe o email do operador/fornecedor para o qual se deve
     * enviar o email
     * @param password password encriptada a ser enviada por email
     * @param codFornecedor codigo do fornecedor gerado no caso de ele ser novo
     */
    public static void enviarEmail(String email, String password, String codFornecedor) {

        EMAIL_TO = email;
        EMAIL_TEXT = "Pass conta software Feira & Office: \n" + password;
        if (!"".equals(codFornecedor)) {
            EMAIL_TEXT += "\nSeu código de fornecedor: \n" + codFornecedor;
            EMAIL_TEXT += "\nUtilize sempre este código na sua nota de encomenda!";
        }
        Properties prop = System.getProperties();
        prop.put("mail.smtp.host", SMTP_SERVER); //optional, defined in SMTPTransport
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.port", "2525"); // default port 25

        Session session = Session.getInstance(prop, null);
        Message msg = new MimeMessage(session);

        try {

            // from
            msg.setFrom(new InternetAddress(EMAIL_FROM));

            // to
            msg.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(EMAIL_TO, false));

            // cc
            msg.setRecipients(Message.RecipientType.CC,
                    InternetAddress.parse(EMAIL_TO_CC, false));

            // subject
            msg.setSubject(EMAIL_SUBJECT);

            // content
            msg.setText(EMAIL_TEXT);

            msg.setSentDate(new Date());

            // Get SMTPTransport
            SMTPTransport t = (SMTPTransport) session.getTransport("smtp");

            // connect
            t.connect(SMTP_SERVER, USERNAME, PASSWORD);

            // send
            t.sendMessage(msg, msg.getAllRecipients());

            //System.out.println("Response: " + t.getLastServerResponse());
            t.close();

        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    /**
     * metodo para verificar se o email é válido ou não
     *
     * @param email envia o email para verificar se é válido
     * @return retorna se é válido ou não
     */
    public static boolean isValid(String email) {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."
                + "[a-zA-Z0-9_+&*-]+)*@"
                + "(?:[a-zA-Z0-9-]+\\.)+[a-z"
                + "A-Z]{2,7}$";

        Pattern pat = Pattern.compile(emailRegex);
        if (email == null) {
            return false;
        }
        return pat.matcher(email).matches();
    }

    /**
     * metodo para enviar email para um operador ou fornecedor a notificar da
     * mudança de email
     *
     * @param email email do operador/fornecedor para o qual se deve
     * enviar o email
     * @param oldEmail antigo email do operador/fornecedor para
     * comparação pelo utilizador
     */
    public static void enviarEmailConfirmacao(String email, String oldEmail) {

        EMAIL_TO = email;
        EMAIL_TEXT = "Novo Email conta software Feira & Office: \n" + email
                + "\nEmail anterior da conta: \n" + oldEmail
                + "\nCaso tenha recebido este mail por engano, envie um email para "
                + USERNAME + " a informar da situação.\nMuito Obrigado.";
        //System.out.println("texto email confirmação: \n"+EMAIL_TEXT);
        Properties prop = System.getProperties();
        prop.put("mail.smtp.host", SMTP_SERVER); //optional, defined in SMTPTransport
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.port", "2525"); // default port 25

        Session session = Session.getInstance(prop, null);
        Message msg = new MimeMessage(session);

        try {

            // from
            msg.setFrom(new InternetAddress(EMAIL_FROM));

            // to
            msg.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(EMAIL_TO, false));

            // cc
            msg.setRecipients(Message.RecipientType.CC,
                    InternetAddress.parse(EMAIL_TO_CC, false));

            // subject
            msg.setSubject("NovoEmail conta feiraEoffice");

            // content
            msg.setText(EMAIL_TEXT);

            msg.setSentDate(new Date());

            // Get SMTPTransport
            SMTPTransport t = (SMTPTransport) session.getTransport("smtp");

            // connect
            t.connect(SMTP_SERVER, USERNAME, PASSWORD);

            // send
            t.sendMessage(msg, msg.getAllRecipients());

            //System.out.println("Response: " + t.getLastServerResponse());
            t.close();

        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    /**
     * metodo para enviar email para um operador ou fornecedor a notificar da
     * mudança de email e password
     *
     * @param email email do operador/fornecedor para o qual se deve
     * enviar o email
     * @param oldEmail antigo email do operador/fornecedor para
     * comparação pelo utilizador
     * @param password password encriptada a ser enviada por email
     */
    public static void enviarEmailDadosNovos(String email, String oldEmail, String password) {

        EMAIL_TO = email;
        EMAIL_TEXT = "Novo Email conta software Feira & Office: \n" + email
                + "\nEmail anterior da conta: \n" + oldEmail
                + "\nNova Pass conta software Feira & Office: \n" + password;

        //System.out.println("texto email confirmação: \n"+EMAIL_TEXT);
        Properties prop = System.getProperties();
        prop.put("mail.smtp.host", SMTP_SERVER); //optional, defined in SMTPTransport
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.port", "2525"); // default port 25

        Session session = Session.getInstance(prop, null);
        Message msg = new MimeMessage(session);

        try {

            // from
            msg.setFrom(new InternetAddress(EMAIL_FROM));

            // to
            msg.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(EMAIL_TO, false));

            // cc
            msg.setRecipients(Message.RecipientType.CC,
                    InternetAddress.parse(EMAIL_TO_CC, false));

            // subject
            msg.setSubject("PassMail conta feiraEoffice");

            // content
            msg.setText(EMAIL_TEXT);

            msg.setSentDate(new Date());

            // Get SMTPTransport
            SMTPTransport t = (SMTPTransport) session.getTransport("smtp");

            // connect
            t.connect(SMTP_SERVER, USERNAME, PASSWORD);

            // send
            t.sendMessage(msg, msg.getAllRecipients());

            //System.out.println("Response: " + t.getLastServerResponse());
            t.close();

        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

}
