/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import bfgir.BDConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *Classe codigoPostal com metodos de acesso a tabela de codigoPostalna BD.
 * 
 */
public class codigoPostal {

    String codPostal;
    String cidade;

    /**
     * Construtor da classe codigoPostal.
     */
    public codigoPostal() {
    }

    /**
     * Construtor da classe codigoPostal.
     *
     * @param codPostal codigo Postal.
     * @param cidade Cidade.
     */
    public codigoPostal(String codPostal, String cidade) {
        this.codPostal = codPostal;
        this.cidade = cidade;
    }

    /**
     * Método get Codigo Postal.
     *
     * @return the codPostal.
     */
    public String getCodPostal() {
        return codPostal;
    }

    /**
     * Método set Codigo Postal.
     *
     * @param codPostal the codPostal to set.
     */
    public void setCodPostal(String codPostal) {
        this.codPostal = codPostal;
    }

    /**
     * Método get Cidade.
     *
     * @return the cidade.
     */
    public String getCidade() {
        return cidade;
    }

    /**
     * Método set Cidade.
     *
     * @param cidade the cidade to set.
     */
    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    /**
     * Método para selecionar todos os atributos do codigo Postal na BD.<br>
     *
     * Este método recebe por parametro o codigo postal. Posto isto faz um
     * select à BD adicionando a um arrayList todos os atributos de um codigo
     * postal com aquele codigo postal.<br>
     *
     * @param codPostal codigo postal a pesquisar.
     * @return cidade e codigo postal.
     *
     */
    public static List<codigoPostal> procurarCodPostal(String codPostal) {

        List<codigoPostal> codigosPostais = new ArrayList<codigoPostal>();
        String sql = "SELECT * from codigoPostal where codPostal = (?)";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setString(1, codPostal);

            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                codigoPostal codPos = new codigoPostal();

                codPos.setCodPostal(results.getString(1));
                codPos.setCidade(results.getString(2));

                codigosPostais.add(codPos);
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return codigosPostais;
    }

    /**
     * Método para inserir um Codigo Postal e respectiva Cidade na BD.<br>
     *
     * Este método recebe dois objetos do tipo String (codigoPostal e cidade) para
     * inserir na tabela codigoPostal da base de dados um novo
     * cosigo Postal e respetiva cidade.<br>
     *
     * @param codigoPostal recebe o codigo postal a inserir.
     * @param cidade recebe a cidade a inserir.
     *
     */
    public static void inserirCodPostal(String codigoPostal, String cidade) {

        String sql = "INSERT INTO codigoPostal (codPostal,cidade) VALUES (?,?)";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setString(1, codigoPostal);
            pstmt.setString(2, cidade);

            pstmt.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

    }

}
