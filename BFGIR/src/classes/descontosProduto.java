/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import bfgir.BDConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe codigoPostal com metodos de acesso a tabela de codigoPostalna BD
 *
 */
public class descontosProduto {

    private int idDesconto;
    private String codProdPa;
    private int quantVend;

    /**
     * Construtor da classe desconto
     */
    public descontosProduto() {
    }

    /**
     * Construtor da classe desconto<br>
     *
     * @param idDesconto id do desconto
     * @param codProdPa código do produto associado ao desconto
     * @param quantVend quantidade vendida a aplicar desconto
     */
    public descontosProduto(int idDesconto, String codProdPa, int quantVend) {
        this.idDesconto = idDesconto;
        this.codProdPa = codProdPa;
        this.quantVend = quantVend;
    }

    /**
     * @return the idDesconto
     */
    public int getIdDesconto() {
        return idDesconto;
    }

    /**
     * @param idDesconto the idDesconto to set
     */
    public void setIdDesconto(int idDesconto) {
        this.idDesconto = idDesconto;
    }

    /**
     * @return the codProdPa
     */
    public String getCodProdPa() {
        return codProdPa;
    }

    /**
     * @param codProdPa the codProdPa to set
     */
    public void setCodProdPa(String codProdPa) {
        this.codProdPa = codProdPa;
    }

    /**
     * @return the quantVend
     */
    public int getQuantVend() {
        return quantVend;
    }

    /**
     * @param quantVend the quantVend to set
     */
    public void setQuantVend(int quantVend) {
        this.quantVend = quantVend;
    }

    /**
     * O metodo vai retornar uma lista de descontos associados a um produto<br>
     *
     * @param idDesc recebe o id de desconto a pesquisar
     * @return um desconto
     */
    public static List<descontosProduto> getListaDescontosPordutosIDDes(int idDesc) {

        List<descontosProduto> listaDescontosProds = new ArrayList();
        String sql = "Select * from descontosprodutos where idDesconto = (?)";

        PreparedStatement stmt = null;
        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setInt(1, idDesc);
            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                descontosProduto desProds = new descontosProduto();
                desProds.setIdDesconto(results.getInt(1));
                desProds.setCodProdPa(results.getString(2));
                desProds.setQuantVend(results.getInt(3));
                listaDescontosProds.add(desProds);
            }
        } catch (SQLException ex) {
            Logger.getLogger(descontosProduto.class.getName()).log(Level.SEVERE, null, ex);
        }

        return listaDescontosProds;

    }

    /**
     * O metodo vai retornar uma lista de descontos associados a um produto<br>
     *
     * @param idDesc recebe o id de desconto a pesquisar
     * @param cod recebe o codigo do produto a pesquisar
     * @return um desconto
     */
    public static List<descontosProduto> getListaDescontosPordutosIDDesCod(int idDesc, String cod) {

        List<descontosProduto> listaDescontosProds = new ArrayList();
        String sql = "Select * from descontosprodutos where idDesconto = (?) and codProdPa = (?)";

        PreparedStatement stmt = null;
        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setInt(1, idDesc);
            pstmt.setString(2, cod);
            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                descontosProduto desProds = new descontosProduto();
                desProds.setIdDesconto(results.getInt(1));
                desProds.setCodProdPa(results.getString(2));
                desProds.setQuantVend(results.getInt(3));
                listaDescontosProds.add(desProds);
            }
        } catch (SQLException ex) {
            Logger.getLogger(descontosProduto.class.getName()).log(Level.SEVERE, null, ex);
        }

        return listaDescontosProds;

    }

    /**
     * O metodo vai retornar uma lista de todos os descontos associados a um
     * produto<br>
     *
     *
     * @return um desconto
     */
    public static List<descontosProduto> getListaDescontosPordutos() {

        List<descontosProduto> listaDescontosProds = new ArrayList();
        String sql = "Select * from descontosprodutos";

        PreparedStatement stmt = null;
        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                descontosProduto desProds = new descontosProduto();
                desProds.setIdDesconto(results.getInt(1));
                desProds.setCodProdPa(results.getString(2));
                desProds.setQuantVend(results.getInt(3));
                listaDescontosProds.add(desProds);
            }
        } catch (SQLException ex) {
            Logger.getLogger(descontosProduto.class.getName()).log(Level.SEVERE, null, ex);
        }

        return listaDescontosProds;

    }

    /**
     * O metodo vai retornar uma lista de todos os descontos associados a um
     * produto não os repetindo<br>
     *
     *
     * @return um desconto
     */
    public static List<descontosProduto> getListaDescontosPordutosDistinct() {

        List<descontosProduto> listaDescontosProds = new ArrayList();
        String sql = "SELECT DISTINCT codProdPa from descontosprodutos";

        PreparedStatement stmt = null;
        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                descontosProduto desProds = new descontosProduto();
                desProds.setCodProdPa(results.getString(1));
                listaDescontosProds.add(desProds);
            }
        } catch (SQLException ex) {
            Logger.getLogger(descontosProduto.class.getName()).log(Level.SEVERE, null, ex);
        }

        return listaDescontosProds;

    }

    /**
     * Metodo para iinserir um desconto a um produto <br>
     *
     * @param idDesc : id do desconto
     * @param CodProd : código do produto
     * @param quant :qauntidade dos produtos ao que se vai aplicar desconto
     */
    public static void inserirDescontos(int idDesc, String CodProd, int quant) {

        String sql = "INSERT INTO descontosprodutos (idDesconto,codProdPa,quantVenda) VALUES (?,?,?)";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setInt(1, idDesc);
            pstmt.setString(2, CodProd);
            pstmt.setFloat(3, quant);

            pstmt.executeUpdate();

        } catch (SQLException e) {
        }

    }

    /**
     * Metodo para eliminar um desconto de um produto.<br>
     *
     * @param id : id do descont
     * @param codProd : código do produto
     *
     */
    public static void eliminarDescontosProdutos(int id, String codProd) {

        try {
            String sql = "DELETE FROM descontosprodutos WHERE idDesconto = (?) AND codProdPa = (?)";
            
            Connection conn = BDConnection.getConnection();
            PreparedStatement pstmt = conn.prepareStatement(sql);
            
            pstmt.setInt(1, id);
            pstmt.setString(2, codProd);
            
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(descontosProduto.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * O metodo vai retornar uma lista de descontos associados a um codigo do
     * produto<br>
     *
     * @param codProdPa recebe o codigo do produto a pesquisar
     * @return um desconto
     */
    public static List<descontosProduto> getListaDescontosPordutosCodProd(String codProdPa) {

        List<descontosProduto> listaDescontosProds = new ArrayList();
        String sql = "Select * from descontosprodutos where codProdPa = (?)";

        PreparedStatement stmt = null;
        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setString(1, codProdPa);
            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                descontosProduto desProds = new descontosProduto();
                desProds.setIdDesconto(results.getInt(1));
                desProds.setCodProdPa(results.getString(2));
                desProds.setQuantVend(results.getInt(3));
                listaDescontosProds.add(desProds);
            }
        } catch (SQLException ex) {
            Logger.getLogger(descontosProduto.class.getName()).log(Level.SEVERE, null, ex);
        }

        return listaDescontosProds;

    }

    /**
     * O metodo vai retornar uma lista de descontos associados a um codigo do
     * produto<br>
     *
     * @param codProdPa recebe o codigo do produto a pesquisar
     * @return um desconto
     */
    public static List<descontosProduto> getListaDescontosPordutosCodProdDistinct(String codProdPa) {

        List<descontosProduto> listaDescontosProds = new ArrayList();
        String sql = "Select DISTINCT codProdPa from descontosprodutos where codProdPa = (?)";

        PreparedStatement stmt = null;
        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setString(1, codProdPa);
            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                descontosProduto desProds = new descontosProduto();
                desProds.setCodProdPa(results.getString(1));
                listaDescontosProds.add(desProds);
            }
        } catch (SQLException ex) {
            Logger.getLogger(descontosProduto.class.getName()).log(Level.SEVERE, null, ex);
        }

        return listaDescontosProds;

    }

    /**
     * Metodo para alterar a quantidade de venda onde o idDesconto e o codigo
     * produto sejam os introduzidos
     *
     *
     * @param quantVenda quantidade de venda a alterar
     * @param idDesconto id de desconto a pesquisar
     * @param codProdPa codigo do produto a pesquisar
     */
    public static void alterarQuantidadeVenda(String quantVenda, int idDesconto, String codProdPa) {

        List<descontosProduto> descontoProduto = new ArrayList<descontosProduto>();
        String sql = "UPDATE descontosprodutos SET quantVenda = (?) WHERE idDesconto = (?) AND codProdPa = (?) ";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setString(1, quantVenda);
            pstmt.setInt(2, idDesconto);
            pstmt.setString(3, codProdPa);

            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Metodo para alterar o desconto de venda onde o idDesconto e o codigo
     * produto sejam os introduzidos
     *
     *
     * @param quantVenda quantidade mínima de venda
     * @param idDesconto id de desconto a pesquisar
     * @param codProdPa codigo do produto a pesquisar
     */
    public static void alterarDescontoProduto(long quantVenda, int idDesconto, String codProdPa) {

        List<descontosProduto> descontoProduto = new ArrayList<>();
        String sql = "UPDATE descontosprodutos SET quantVenda = (?) WHERE idDesconto = (?) AND codProdPa = (?) ";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setLong(1, quantVenda);
            pstmt.setInt(2, idDesconto);
            pstmt.setString(3, codProdPa);

            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

}
