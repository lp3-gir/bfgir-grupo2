/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import bfgir.BDConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe produtoSubCategoria com metodos de acesso a tabela de
 * produtoSubCategoria na BD
 *
 */
public class produtoSubCategoria {

    private int idProdSub;
    private String codProdPa;
    private int nivel1;
    private int nivel2;
    private int nivel3;

    /**
     * Construtor da classe produtoSubCategoria
     */
    public produtoSubCategoria() {
    }

    /**
     * Construtor da classe produtoSubCategoria com parametros
     *
     * @param idProdSub id de produto subcategoria
     * @param codProdPa codigo do produto
     * @param nivel1 id da categoria de nivel 1
     * @param nivel2 id da categoria de nivel 2
     * @param nivel3 id da categoria de nivel 3
     *
     */
    public produtoSubCategoria(int idProdSub, String codProdPa, int nivel1, int nivel2, int nivel3) {
        this.idProdSub = idProdSub;
        this.codProdPa = codProdPa;
        this.nivel1 = nivel1;
        this.nivel2 = nivel2;
        this.nivel3 = nivel3;

    }

    /**
     * @return the codProdPa
     */
    public String getCodProdPa() {
        return codProdPa;
    }

    /**
     * @param codProdPa the codProdPa to set
     */
    public void setCodProdPa(String codProdPa) {
        this.codProdPa = codProdPa;
    }

    /**
     * @return the nivel1
     */
    public int getNivel1() {
        return nivel1;
    }

    /**
     * @param nivel1 the nivel1 to set
     */
    public void setNivel1(int nivel1) {
        this.nivel1 = nivel1;
    }

    /**
     * @return the nivel2
     */
    public int getNivel2() {
        return nivel2;
    }

    /**
     * @param nivel2 the nivel2 to set
     */
    public void setNivel2(int nivel2) {
        this.nivel2 = nivel2;
    }

    /**
     * @return the nivel3
     */
    public int getNivel3() {
        return nivel3;
    }

    /**
     * @param nivel3 the nivel3 to set
     */
    public void setNivel3(int nivel3) {
        this.nivel3 = nivel3;
    }

    /**
     * @return the idProdSub
     */
    public int getIdProdSub() {
        return idProdSub;
    }

    /**
     * @param idProdSub the idProdSub to set
     */
    public void setIdProdSub(int idProdSub) {
        this.idProdSub = idProdSub;
    }

    /**
     * O metodo recebe um código do produto para depois pesquisar os niveis de
     * categorias associados ao produto escolhido<br>
     *
     *
     * @param codProdPa recebe o código do produto a pesquisar
     * @return niveis de categorias assoicados ao produto
     */
    public static List<produtoSubCategoria> getListaProdutosSubCat(String codProdPa) {

        List<produtoSubCategoria> produtoSubCategorias = new ArrayList();
        String sql = "Select * from produtosubcategoria where codProdPa = (?)";

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setString(1, codProdPa);

            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                produtoSubCategoria psc = new produtoSubCategoria();
                psc.setIdProdSub(results.getInt(1));
                psc.setCodProdPa(results.getString(2));
                psc.setNivel1(results.getInt(3));
                psc.setNivel2(results.getInt(4));
                psc.setNivel3(results.getInt(5));

                produtoSubCategorias.add(psc);
            }
        } catch (SQLException ex) {
            Logger.getLogger(produtoSubCategoria.class.getName()).log(Level.SEVERE, null, ex);
        }

        return produtoSubCategorias;

    }

    /**
     * Método para procurar produtos com um dada categoria pertencente ao
     * conjunto de subcatetorias do produto<br>
     * 
     * O método recebe um id da categoria do produto para depois pesquisar pela
     * existencia do mesmo em cada nível de categoria associada ao produto. Caso
     * a categoria esteja presente em qualquer dos niveis de categorias
     * associados ao produto escolhido, o método retorna uma lista de objetos
     * produtoSubCategoria preenchida com os mesmos. Caso contrário, retorna uma
     * lista de objetos produtoSubCategoria vazia<br>
     *
     *
     * @param idCategoria : id da categoria a pesquisar
     * @return : lista de objetos produtoSubCategoria no qual a categoria está
     * presente num ou mais níveis
     */
    public static List<produtoSubCategoria> getProdutosComCategoria(int idCategoria) {

        List<produtoSubCategoria> produtoSubCategorias = new ArrayList();
        String sql = "select * from produtosubcategoria where idCatNivel1 = (?) or idCatNivel2 = (?) or idCatNivel3 = (?)";

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setInt(1, idCategoria);
            pstmt.setInt(2, idCategoria);
            pstmt.setInt(3, idCategoria);

            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                produtoSubCategoria psc = new produtoSubCategoria();
                psc.setIdProdSub(results.getInt(1));
                psc.setCodProdPa(results.getString(2));
                psc.setNivel1(results.getInt(3));
                psc.setNivel2(results.getInt(4));
                psc.setNivel3(results.getInt(5));

                produtoSubCategorias.add(psc);
            }
        } catch (SQLException ex) {
            Logger.getLogger(produtoSubCategoria.class.getName()).log(Level.SEVERE, null, ex);
        }

        return produtoSubCategorias;

    }

    /**
     *
     * Método usado para associar niveis de categoria a um produto na BD<br>
     *
     * Este método recebe por parametro um codigo de produto e os niveis a associar. Posto isto é inserido na BD
     * descriminando cada atributo e nivel de categoria deste mesmo produto.<br>
     *
     *
     *
     * @param CodProd codigo do produto
     * @param niivel1 nivel a assocar no nivel 1 
     * @param nivel2 nivel a assocar no nivel 2
     * @param nivel3 nivel a assocar no nivel 3 
     */
    public static void associarProdutos(String CodProd, int niivel1, int nivel2, int nivel3) {

        String sql = "INSERT INTO produtosubcategoria (codProdPa,idCatNivel1,idCatNivel2,idCatNivel3) VALUES (?,?,?,?)";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setString(1, CodProd);
            pstmt.setInt(2, niivel1);
            pstmt.setInt(3, nivel2);
            pstmt.setInt(4, nivel3);

            pstmt.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

    }

    /**
     * Método para gerar uma lista de objetos produtoSubCategoria com todos os
     * registos da tabela produtosubcategoria
     *
     * @return: retorna todos os registos da tabela produtosubcategoria
     */
    public static List<produtoSubCategoria> getAllProdutosSubCat() {

        List<produtoSubCategoria> produtoSubCategorias = new ArrayList();
        String sql = "Select * from produtosubcategoria";

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                produtoSubCategoria psc = new produtoSubCategoria();
                psc.setIdProdSub(results.getInt(1));
                psc.setCodProdPa(results.getString(2));
                psc.setNivel1(results.getInt(3));
                psc.setNivel2(results.getInt(4));
                psc.setNivel3(results.getInt(5));

                produtoSubCategorias.add(psc);
            }
        } catch (SQLException ex) {
            Logger.getLogger(produtoSubCategoria.class.getName()).log(Level.SEVERE, null, ex);
        }

        return produtoSubCategorias;

    }

    /**
     * O metodo recebe o id do registo na BD para construir um objeto
     * produtoSubCategoria com os dados do mesmo e retorná-lo numa lista de
     * objetos produtoSubCategoria<br>
     *
     * @param idProdSub : id do registo a pesquisar
     * @return : retorna lista com o objeto produtoSubCategoria ou sem nada se
     * não o encontrar
     */
    public static List<produtoSubCategoria> getListaProdutosSubCatID(int idProdSub) {

        List<produtoSubCategoria> produtoSubCategorias = new ArrayList();
        String sql = "Select * from produtosubcategoria where idProdSub = (?)";

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setInt(1, idProdSub);

            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                produtoSubCategoria psc = new produtoSubCategoria();
                psc.setIdProdSub(results.getInt(1));
                psc.setCodProdPa(results.getString(2));
                psc.setNivel1(results.getInt(3));
                psc.setNivel2(results.getInt(4));
                psc.setNivel3(results.getInt(5));

                produtoSubCategorias.add(psc);
            }
        } catch (SQLException ex) {
            Logger.getLogger(produtoSubCategoria.class.getName()).log(Level.SEVERE, null, ex);
        }

        return produtoSubCategorias;

    }

    /**
     * Método recebe o id da subcategoria a atualizar, o nivel da mesma e o id
     * do registo. Adiciona o nivel ao string do query na posição do atributo do
     * query <b>NÃO MUDAR NOMES NA BD</b> e depois faz o update do registo.<br>
     * O método está assim para não se produzir 3 métodos.
     *
     * @param idsubCat : id de categoria da subcategoria
     * @param nivel : nivel da subcategoria
     * @param idProdSub : id do registo produtosubcategoria
     */
    public static void atualizarSubCategoriaProdutoSubCategoria(int idsubCat, int nivel, int idProdSub) {

        String sql = "UPDATE produtosubcategoria set idCatNivel" + nivel + " = (?) WHERE idProdSub = (?)";

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setInt(1, idsubCat);
            pstmt.setInt(2, idProdSub);

            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Método para procuro registos na BD com certos atributos na BD para
     * validação<br>
     * Método usado com uma validação do registo da tabela para impedir que
     * existam registos iguais à excepção do id do registo<br>
     * Método recebe todos os parâmetros de um objeto produtoSubCategoria à
     * excepção do id e verificar se existe na BD registos iguais e retorna o
     * que encontrar<br>
     *
     *
     * @param codProd : código do produto
     * @param idCatN1 : id subcategoria nível 1
     * @param idCatN2 : id subcategoria nível 2
     * @param idCatN3 : id subcategoria nível 3
     * @return : lista de objetos produtoSubCategoria no qual a categoria está
     * presente num ou mais níveis
     */
    public static List<produtoSubCategoria> getSameRecordProdSubCat(String codProd, int idCatN1, int idCatN2, int idCatN3) {

        List<produtoSubCategoria> produtoSubCategorias = new ArrayList();
        String sql = "select * from produtosubcategoria where codProdPa = (?) and idCatNivel1 = (?) and idCatNivel2 = (?) and idCatNivel3 = (?)";

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setString(1, codProd);
            pstmt.setInt(2, idCatN1);
            pstmt.setInt(3, idCatN2);
            pstmt.setInt(4, idCatN3);

            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                produtoSubCategoria psc = new produtoSubCategoria();
                psc.setIdProdSub(results.getInt(1));
                psc.setCodProdPa(results.getString(2));
                psc.setNivel1(results.getInt(3));
                psc.setNivel2(results.getInt(4));
                psc.setNivel3(results.getInt(5));

                produtoSubCategorias.add(psc);
            }
        } catch (SQLException ex) {
            Logger.getLogger(produtoSubCategoria.class.getName()).log(Level.SEVERE, null, ex);
        }

        return produtoSubCategorias;

    }
    
}
