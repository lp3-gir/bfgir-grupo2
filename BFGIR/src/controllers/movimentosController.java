/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import bfgir.ecraPessoalOperador;
import bfgir.login;
import bfgir.menuAdministrador;
import bfgir.menuOperador;
import bfgir.movimentos;
import classes.entradas;
import static classes.entradas.listarEntradasId;
import static classes.entradas.retornaIdEntrada;
import classes.entradasProdutos;
import static classes.entradasProdutos.listarProdutosEntrada;
import classes.fornecedor;
import static classes.fornecedor.listarFornecedorTodosAtivos;
import classes.produto;
import static classes.produto.pesquisarProdutoID;
import classes.utilizador;
import static classes.utilizador.procurarEmail;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * FXML Controller class
 */
public class movimentosController implements Initializable {

    String ids = "";
    String ids2 = "";

    @FXML
    private Pane painelMovimentos;
    @FXML
    private ScrollPane produtos;
    @FXML
    private ScrollPane entradaStock;
    @FXML
    private MenuBar menu;
    @FXML
    private VBox vBoxEntradas;

    @FXML
    private VBox vBoxFornecedores;

    @FXML
    private VBox vBoxProdutos;
    @FXML
    private TextField codEntradaPesquisar;

    private int altura_vbox = 0;
    private int altura_vbox2 = 0;
    private int altura_vbox3 = 0;
    @FXML
    private Menu opcoes;
    @FXML
    private Button pesquisarCodEnc;

    /**
     * Initializes the controller class.
     * 
     * Este método vai listar todos os fornecedores ativos 
     * para cad fornecedor é criado um botão com o seu nome 
     *
     * @param url url
     * @param rb rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        adicionarEcraPessoalOperadorMenuOpcoes();

        List<fornecedor> fornecedor = listarFornecedorTodosAtivos();
        for (fornecedor fr : fornecedor) {

            String id = fr.getIdFornecedor();
            ids = id;
            String nome = fr.getNome();
            Button botao = new Button(nome);
            botao.setId("botaoVbox");
            botao.setPrefHeight(30);
            botao.setPrefWidth(250);
            botao.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {

                    ids = botao.getId();
                     botao.setId("botaoVboxChangeCollour");
                    vBoxProdutos.getChildren().removeAll(vBoxProdutos.getChildren());
                    listarEntrada(id);

                }
            });
            altura_vbox = altura_vbox + 30;
            vBoxFornecedores.getChildren().addAll(botao);
            if (altura_vbox > 330) {
                if (altura_vbox > 330) {
                    vBoxProdutos.setPrefWidth(250);
                }
                vBoxProdutos.setPrefHeight(altura_vbox);
            }
        }
    }

    /**
     * Este método vai listar todas as encomendas associadas ao fornecedor selecionado
     * e para cada encomenda vai criar um botão com o respetivo ID
     *
     * @param idForn recebe o ID do Fornecedor
     */
    private void listarEntrada(String idForn) {
        vBoxEntradas.getChildren().removeAll(vBoxEntradas.getChildren());
        List<entradas> entradas = retornaIdEntrada(idForn);
        for (entradas enp : entradas) {

            String id = enp.getIdEntrada();
            String nome = enp.getIdEntrada();
            Label label1;
            Button botao = new Button(nome + " - " + enp.getDataEntrada());
            botao.setId("botaoVbox");
            botao.setPrefHeight(30);
            botao.setPrefWidth(250);
            botao.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {

                    ids2 = botao.getId();
                     botao.setId("botaoVboxChangeCollour");
                    listarProdutos(id);

                }
            });
            altura_vbox2 = altura_vbox2 + 30;
            vBoxEntradas.getChildren().addAll(botao);
            if (altura_vbox > 330) {
                if (altura_vbox > 330) {
                    vBoxProdutos.setPrefWidth(250);
                }
                vBoxProdutos.setPrefHeight(altura_vbox);
            }
        }
    }
   

    /**
     * Este metodo vai fazer a listagem organizada de varias categorias ( valor total, tipo do preço )
     * e de todas as informações da encomenda selecionado
     *
     * @param idEnt recebe o ID da entrada da encomenda
     */
    private void listarProdutos(String idEnt) {
        altura_vbox3 = 0;
        vBoxProdutos.getChildren().removeAll(vBoxProdutos.getChildren());

        String tipoMoeda = "";
        List<entradasProdutos> entradaProdutos = listarProdutosEntrada(idEnt);
        float valorTotal = 0;

        for (entradasProdutos enp : entradaProdutos) {

            List<produto> produtos = pesquisarProdutoID(enp.getCodProdPa());

            for (produto prod : produtos) {

                valorTotal = valorTotal + enp.getPrecoTotalCIVA();
                String id = enp.getIdEntrada();
                String nome = prod.getDescricao();
                List<entradasProdutos> entrada = entradasProdutos.procurarTipoQuantidadeProduto(enp.getCodProdPa());
                entradasProdutos ep = entrada.get(0);
                Label label1;
                Label label2;
                if (nome.length() > 25) {
                    label1 = new Label(ep.getCodProdPa() + " | " + nome.substring(0, 25) + " | " + ep.getQuantVendida() + " " + ep.getTipoVenda() + " | " + ep.getPrecoUnitSIVA() + " " + ep.getTipoPrecos() + "/" + ep.getTipoVenda() + " S.IVA " + " | " + " Total: " + ep.getPrecoTotalCIVA() + " " + ep.getTipoPrecos() + " C.IVA ");
                    tipoMoeda = ep.getTipoPrecos();
                } else {
                    label1 = new Label(ep.getCodProdPa() + " | " + nome.substring(0, nome.length()) + " | " + ep.getQuantVendida() + " " + ep.getTipoVenda() + " | " + ep.getPrecoUnitSIVA() + " " + ep.getTipoPrecos() + "/" + ep.getTipoVenda() + " S.IVA " + " | " + " Total: " + ep.getPrecoTotalCIVA() + " " + ep.getTipoPrecos() + " C.IVA ");
                    tipoMoeda = ep.getTipoPrecos();
                }

                label1.setId("labelProdutos");
                label1.setPrefHeight(30);
                label1.setPrefWidth(593);

                altura_vbox3 = altura_vbox3 + 30;
                vBoxProdutos.getChildren().addAll(label1);
                if (altura_vbox3 > 240) {
                    if (altura_vbox3 > 240) {
                        vBoxProdutos.setPrefWidth(580);
                    }
                    vBoxProdutos.setPrefHeight(altura_vbox3);
                }
            }

        }
        DecimalFormat df = new DecimalFormat("###########.##");
        float valor = valorTotal;
        Label label2;
        label2 = new Label("Total: " + df.format(valor) + " " + tipoMoeda);
        label2.setId("labelProdutos");
        label2.setPrefHeight(30);
        label2.setPrefWidth(593);
        vBoxProdutos.getChildren().addAll(label2);
    }

    /**
     * Metodo para terminar sessao do utilizador
     *
     * @param event evento
     */
    @FXML
    private void terminarSessao(ActionEvent event) {
        try {
            login.setIdLogin(0);
            login.setNivelLogin(10000);
            login log = new login();
            fecha();
            log.start(new Stage());

        } catch (Exception ex) {
            Logger.getLogger(menuAdministradorController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Método para encerrar a aplicação
     */
    @FXML
    private void sair(ActionEvent event) {
        System.exit(0);
    }

    /**
     * Método para aceder ao menu do operador
     */
    @FXML
    private void openMenu(ActionEvent event) {
        try {
            if (login.getNivelLogin() == 1) {
                menuOperador menuO = new menuOperador();
                fecha();
                menuO.start(new Stage());
            } else {
                menuAdministrador menuAdmin = new menuAdministrador();
                fecha();
                menuAdmin.start(new Stage());
            }
        } catch (Exception ex) {
            Logger.getLogger(eliminarFornecedorController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * método para fechar o ecrã dos movimentos
     */
    private void fecha() {
        movimentos.getStage().close();
    }

    /**
     * Este metodo vai fazer um select na BD e vai buscar todos os atributos 
     * da encomenda a base de daos
     * 
     * @param idEnt recebe o ID da entrada da encomenda
     */
    private void listarInfoEncProd(String idEnt) {
        vBoxEntradas.getChildren().removeAll(vBoxEntradas.getChildren());

        String tipoMoeda = "";
        List<entradasProdutos> entradaProdutos = listarProdutosEntrada(idEnt);
        float valorTotal = 0;

        for (entradasProdutos enp : entradaProdutos) {

            valorTotal = valorTotal + enp.getPrecoTotalCIVA();
            String id = enp.getIdEntrada();
            List<entradasProdutos> entrada = entradasProdutos.listarProdutosEntrada(enp.getIdEntrada());
            entradasProdutos ep = entrada.get(0);
            Label label1;
            if (id.length() > 25) {
                label1 = new Label(ep.getIdEntrada());
                tipoMoeda = ep.getTipoPrecos();
            } else {
                label1 = new Label(ep.getIdEntrada());
                tipoMoeda = ep.getTipoPrecos();
            }

            label1.setId("labelEntradas");
            label1.setPrefHeight(30);
            label1.setPrefWidth(593);

            altura_vbox3 = altura_vbox3 + 30;
            vBoxEntradas.getChildren().addAll(label1);
            if (altura_vbox3 > 240) {
                if (altura_vbox3 > 240) {
                    vBoxEntradas.setPrefWidth(580);
                }
                vBoxEntradas.setPrefHeight(altura_vbox3);
            }
        }

    }

    /**
     * Este método filtrar as encomendas pelo codigo do produto
     * e lista a encomenda 
     * 
     * @param event evento
     */
    @FXML
    private void pesquisarCodEnc(ActionEvent event) {
        vBoxEntradas.getChildren().removeAll(vBoxEntradas.getChildren());
        List<entradasProdutos> encProd = listarProdutosEntrada(codEntradaPesquisar.getText());
        
            List<entradas> entradas = listarEntradasId(codEntradaPesquisar.getText());
            for (entradas ec : entradas) {

                String id = ec.getIdEntrada();
                Button botao = new Button(id + " - " + ec.getDataEntrada());

                botao.setId("botaoVbox");
                botao.setPrefHeight(30);
                botao.setPrefWidth(250);
                botao.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent e) {

                        listarProdutos(codEntradaPesquisar.getText());

                    }
                });
                altura_vbox2 = altura_vbox2 + 30;
                vBoxEntradas.getChildren().addAll(botao);
                if (altura_vbox2 > 330) {
                    if (altura_vbox2 > 330) {
                        vBoxEntradas.setPrefWidth(250);
                    }
                    vBoxEntradas.setPrefHeight(altura_vbox2);
                }
            }
        }
    
      /**
     * método para substituir o conteudo do menu de opções para colocar as
     * opções por ordem caso seja o operador a abrir o ecrã
     */
    private void adicionarEcraPessoalOperadorMenuOpcoes() {
        List<utilizador> operadores = procurarEmail(login.getIdLogin());
        for (utilizador operador : operadores) {
            if (operador.getNivel() == 1) {
                opcoes.getItems().clear();
                MenuItem ePOp = new MenuItem("Ecrã Pessoal");
                MenuItem terminarSessao = new MenuItem("Terminar Sessão");
                MenuItem sair = new MenuItem("Sair");
                opcoes.getItems().add(ePOp);
                opcoes.getItems().add(terminarSessao);
                opcoes.getItems().add(sair);
                ePOp.setOnAction((ActionEvent e) -> {
                    try {
                        ecraPessoalOperador ePO = new ecraPessoalOperador();
                        fecha();
                        ePO.start(new Stage());
                    } catch (Exception ex) {
                        Logger.getLogger(movimentosController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
                terminarSessao.setOnAction((ActionEvent e) -> {
                    try {
                        login.setIdLogin(0);
                        login.setNivelLogin(10000);
                        login log = new login();
                        fecha();
                        log.start(new Stage());
                    } catch (Exception ex) {
                        Logger.getLogger(movimentosController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
                sair.setOnAction((ActionEvent e) -> {
                    System.exit(0);
                });
            }
        }
    }

}
