/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import bfgir.BDConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe administrador com metodos de acesso a tabela de utlizador na BD.
 *
 */
public class administrador extends operador {

    /**
     * Construtor com atributos da classe 'avó'.<br>
     *
     * @param idUtilizador variavel com o id de utilizador.
     * @param email variavel com o email do utilizador.
     * @param password variavel com a password do utilizador.
     * @param salt variavel com o salt da password do utilizador.
     * @param nivel variavel de nivel de utilizador.
     * @param estado variavel com o estado do utilizador.
     */
    public administrador(int idUtilizador, String email, String password, String salt, int nivel, String estado) {
        super(idUtilizador, email, password, salt, nivel, estado);
    }

    /**
     * Construtor.
     */
    public administrador() {
    }

    /**
     * Método para obter uma lista de todos utilizadores (ativos e desativos)
     * existentes na base de dados com base no seu tipo de utilizador através do
     * atributo nível.<br>
     * nível 0: utilizador fornecedor.<br>
     * nível 1: utilizador operador.<br>
     * nível 2: utilizador administrador.<br>
     *
     * @param nivel variavel de nivel de utilizador a pesquisar.
     * @return retorna lista de utilizadores.
     */
    public static List<utilizador> listarUtilizadoresPorTipo(int nivel) {

        List<utilizador> utilizadores = new ArrayList<utilizador>();
        String sql = "SELECT * from utilizador WHERE nivel = (?)";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setInt(1, nivel);

            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                utilizador utilizador = new utilizador();

                utilizador.setIdUtilizador(results.getInt(1));
                utilizador.setEmail(results.getString(2));
                utilizador.setPassword(results.getString(3));
                utilizador.setSalt(results.getString(4));
                utilizador.setNivel(results.getInt(5));
                utilizador.setEstado(results.getString(6));
                utilizadores.add(utilizador);
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return utilizadores;
    }

    /**
     * Método para procurar um utilizador por ID.<br>
     *
     * @param id_utilizador variavel com o id de utilizador a pesquisar.
     * @return retorna lista de utilizadores.
     */
    public static List<utilizador> procurarUtilizadorID(int id_utilizador) {

        List<utilizador> utilizadores = new ArrayList<>();
        String sql = "SELECT * from utilizador WHERE idUtilizador = (?)";

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setInt(1, id_utilizador);

            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                utilizador uti = new utilizador();

                uti.setIdUtilizador(results.getInt(1));
                uti.setEmail(results.getString(2));
                uti.setPassword(results.getString(3));
                uti.setSalt(results.getString(4));
                uti.setNivel(results.getInt(5));
                uti.setEstado(results.getString(6));
                utilizadores.add(uti);
               
            }
            

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        return utilizadores;
    }

    /**
     * Método para eliminar utilizadores com base no seu id e no seu nivel.<br>
     *
     * @param id_utilizador variavel com o id de utilizador a pesquisar.
     * @param nivel variavel para pesquisar o nivel de utilizador.
     */
    public static void eliminarUtilizador(int id_utilizador, int nivel) {

        List<utilizador> utilizadores = new ArrayList<utilizador>();
        String sql = "DELETE FROM utilizador WHERE idUtilizador = (?) AND nivel = (?)";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setInt(1, id_utilizador);
            pstmt.setInt(2, nivel);

            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Método para ativar desativar - utilizadores com base no seu id e no seu nível.<br>
     *
     * @param estado variavel com o estado a pesquisar.
     * @param id_utilizador variavel com o id de utilizador a pesquisar.
     * @param nivel variavel para pesquisar o nivel de utilizador.
     */
    public static void ativarInativarUtilizador(String estado, int id_utilizador, int nivel) {

        List<utilizador> utilizadores = new ArrayList<utilizador>();
        String sql = "UPDATE utilizador set estado = (?) WHERE idUtilizador = (?) AND nivel = (?)";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, estado);
            pstmt.setInt(2, id_utilizador);
            pstmt.setInt(3, nivel);

            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Método para alterar a PassWord e o seu Salt com o seu id de utilizador,
     * email, nivel e estado.<br>
     *
     * @param password variavel com a password a alterar.
     * @param salt variavel com o salt a alterar.
     * @param id_utilizador variavel com o id de utilizador a pesquisar.
     * @param email variavel com o email do utilizador.
     * @param nivel variavel para pesquisar o nivel de utilizador.
     * @param estado variavel com o estado a pesquisar.
     */
    public static void alterarPassUtilizador(String password, String salt, int id_utilizador, String email, int nivel, String estado) {

        List<utilizador> utilizadores = new ArrayList<utilizador>();
        String sql = "UPDATE utilizador SET password =(?), salt =(?) WHERE idUtilizador = (?) AND email = (?) AND nivel = (?) AND estado = (?)";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, password);
            pstmt.setString(2, salt);
            pstmt.setInt(3, id_utilizador);
            pstmt.setString(4, email);
            pstmt.setInt(5, nivel);
            pstmt.setString(6, estado);

            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Método para alterar a PassWord o seu Salt e o email do Utilizador onde o
     * id de utilizador, email e nivel sejam aqueles que foram fornecidos.<br>
     *
     * @param password variavel com a password a alterar.
     * @param salt variavel com o salt a alterar.
     * @param estado variavel com o estado a pesquisar.
     * @param id_utilizador variavel com o id de utilizador a pesquisar.
     * @param email variavel com o email do utilizador a pesquisar.
     * @param nivel variavel para pesquisar o nivel de utilizador.
     */
    public static void alterarPassEstadoUtilizador(String password, String salt, String estado, int id_utilizador, String email, int nivel) {

        List<utilizador> utilizadores = new ArrayList<utilizador>();
        String sql = "UPDATE utilizador SET password = (?), salt = (?),  estado = (?) WHERE idUtilizador = (?) And email = (?) AND nivel = (?)";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, password);
            pstmt.setString(2, salt);
            pstmt.setString(3, estado);
            pstmt.setInt(4, id_utilizador);
            pstmt.setString(5, email);
            pstmt.setInt(6, nivel);

            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Método para alterar o estado onde o fornecedor tenha aquele idUtilizador.<br>
     *
     * @param estado variavel com o estado a alterar.
     * @param id_utilizador variavel com o id de utilizador a pesquisar.
     */
    public static void alterarEstadoFornecedor(String estado, int id_utilizador) {

        List<utilizador> utilizadores = new ArrayList<utilizador>();
        String sql = "UPDATE fornecedor SET estado = (?) WHERE idUtilizador = (?) ";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setString(1, estado);
            pstmt.setInt(2, id_utilizador);

            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Método para alterar o email do utilizador onde o id de utilizador e nivel
     * sejam aqueles fornecidos.<br>
     *
     * @param email variavel com o email do utilizador a pesquisar.
     * @param id_utilizador variavel com o id de utilizador a pesquisar.
     * @param nivel variavel para pesquisar o nivel de utilizador.
     */
    public static void alterarEmail(String email, int id_utilizador, int nivel) {

        List<utilizador> utilizadores = new ArrayList<utilizador>();
        String sql = "UPDATE utilizador SET email = (?) WHERE idUtilizador = (?) AND nivel = (?)";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setString(1, email);
            pstmt.setInt(2, id_utilizador);
            pstmt.setInt(3, nivel);

            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Método para alterar o pais, morada e codigo postal do utilizador onde o
     * idFornecedor e o nome sejam aqueles fornecidos.<br>
     *
     * @param pais variavel com o pais a alterar.
     * @param morada variavel com a morada a alterar.
     * @param CP variavel com o codigo Postal a alterar.
     * @param idFornecedor variavel com id do fornecedor para pesquisar.
     * @param nome variavel com o nome a pesquisar.
     */
    public static void alterarPaisMoradaCP(String pais, String morada, String CP, String idFornecedor, String nome) {

        List<utilizador> utilizadores = new ArrayList<utilizador>();
        String sql = "UPDATE fornecedor SET pais = (?), morada = (?), codPostal = (?) WHERE idFornecedor = (?) AND nome = (?)";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setString(1, pais);
            pstmt.setString(2, morada);
            pstmt.setString(3, CP);
            pstmt.setString(4, idFornecedor);
            pstmt.setString(5, nome);

            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Método para alterar a Cidade do utilizador onde o Codigo Postal seja
     * aquele fornecido.<br>
     *
     * @param cidade variavel com a cidade a alterar.
     * @param CP variavel com o codigo Postal a pesquisar.
     */
    public static void alterarCidade(String cidade, String CP) {

        List<utilizador> utilizadores = new ArrayList<utilizador>();
        String sql = "UPDATE codigoPostal SET cidade = (?) WHERE codPostal = (?)";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setString(1, cidade);
            pstmt.setString(2, CP);

            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Método para procurar utilizador por ID e Email.<br>
     *
     * @param id_utilizador variavel com o id de utilizador a pesquisar.
     * @param email variavel com o email do utilizador a pesquisar.
     * @return retorna lista de utilizadores.
     */
    public static List<utilizador> procurarUtilizadorIDEmail(int id_utilizador, String email) {

        List<utilizador> utilizadores = new ArrayList<utilizador>();
        String sql = "SELECT * from utilizador WHERE idUtilizador = (?) AND email = (?)";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setInt(1, id_utilizador);
            pstmt.setString(2, email);
            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                utilizador utilizador = new utilizador();

                utilizador.setIdUtilizador(results.getInt(1));
                utilizador.setEmail(results.getString(2));
                utilizador.setPassword(results.getString(3));
                utilizador.setSalt(results.getString(4));
                utilizador.setNivel(results.getInt(5));
                utilizador.setEstado(results.getString(6));
                utilizadores.add(utilizador);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        return utilizadores;
    }
  
}
