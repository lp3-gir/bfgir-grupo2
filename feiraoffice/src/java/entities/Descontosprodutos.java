/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ruben
 */
@Entity
@Table(name = "descontosprodutos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Descontosprodutos.findAll", query = "SELECT d FROM Descontosprodutos d")
    , @NamedQuery(name = "Descontosprodutos.findByIdDesconto", query = "SELECT d FROM Descontosprodutos d WHERE d.descontosprodutosPK.idDesconto = :idDesconto")
    , @NamedQuery(name = "Descontosprodutos.findByCodProdPa", query = "SELECT d FROM Descontosprodutos d WHERE d.descontosprodutosPK.codProdPa = :codProdPa")
    , @NamedQuery(name = "Descontosprodutos.findByQuantVenda", query = "SELECT d FROM Descontosprodutos d WHERE d.quantVenda = :quantVenda")})
public class Descontosprodutos implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @EmbeddedId
    protected DescontosprodutosPK descontosprodutosPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "quantVenda")
    private int quantVenda;

    /**
     *
     */
    public Descontosprodutos() {
    }

    /**
     *
     * @param descontosprodutosPK recebe um objeto do tipo descontosprodutosPK
     */
    public Descontosprodutos(DescontosprodutosPK descontosprodutosPK) {
        this.descontosprodutosPK = descontosprodutosPK;
    }

    /**
     *
     * @param descontosprodutosPK
     * @param quantVenda
     */
    public Descontosprodutos(DescontosprodutosPK descontosprodutosPK, int quantVenda) {
        this.descontosprodutosPK = descontosprodutosPK;
        this.quantVenda = quantVenda;
    }

    /**
     *
     * @param idDesconto
     * @param codProdPa
     */
    public Descontosprodutos(int idDesconto, String codProdPa) {
        this.descontosprodutosPK = new DescontosprodutosPK(idDesconto, codProdPa);
    }

    /**
     *
     * @return descontosprodutosPK
     */ 
    public DescontosprodutosPK getDescontosprodutosPK() {
        return descontosprodutosPK;
    }

    /**
     *
     * @param descontosprodutosPK
     */
    public void setDescontosprodutosPK(DescontosprodutosPK descontosprodutosPK) {
        this.descontosprodutosPK = descontosprodutosPK;
    }

    /**
     *
     * @return quantVenda
     */
    public int getQuantVenda() {
        return quantVenda;
    }

    /**
     *
     * @param quantVenda
     */
    public void setQuantVenda(int quantVenda) {
        this.quantVenda = quantVenda;
    }
}
