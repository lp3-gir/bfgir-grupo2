/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ruben
 */
public class EntradaprodutoPKTest {



    /**
     * Test of getIdEntrada method, of class EntradaprodutoPK.
     */
    @Test
    public void testGetIdEntrada() {
        System.out.println("getIdEntrada");
        EntradaprodutoPK instance = new EntradaprodutoPK();
        instance.setIdEntrada("15632");

        assertEquals("15632", instance.getIdEntrada());

        //
    }

    /**
     * Test of setIdEntrada method, of class EntradaprodutoPK.
     */
    @Test
    public void testSetIdEntrada() {
        System.out.println("setIdEntrada");
        EntradaprodutoPK instance = new EntradaprodutoPK();
        instance.setIdEntrada("15632");

    }

    /**
     * Test of getCodProdPa method, of class EntradaprodutoPK.
     */
    @Test
    public void testGetCodProdPa() {
        System.out.println("getCodProdPa");
        EntradaprodutoPK instance = new EntradaprodutoPK();
        instance.setCodProdPa("T.123");
        assertEquals("T.123", instance.getCodProdPa());

    }

    /**
     * Test of setCodProdPa method, of class EntradaprodutoPK.
     */
    @Test
    public void testSetCodProdPa() {
        System.out.println("setCodProdPa");
        EntradaprodutoPK instance = new EntradaprodutoPK();
        instance.setCodProdPa("T.123");

    }
}
