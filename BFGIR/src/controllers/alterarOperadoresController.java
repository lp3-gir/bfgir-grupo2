/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import bfgir.alterarOperadores;
import bfgir.login;
import bfgir.menuAdministrador;
import static classes.PasswordHelper.generatePassword;
import static classes.administrador.alterarPassEstadoUtilizador;
import static classes.administrador.alterarPassUtilizador;
import static classes.administrador.ativarInativarUtilizador;
import static classes.administrador.listarUtilizadoresPorTipo;
import static classes.administrador.procurarUtilizadorID;
import static classes.administrador.procurarUtilizadorIDEmail;
import static classes.email.enviarEmail;
import classes.passwordUtils;
import classes.utilizador;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Classe controladora do ecrã alterarOperadores.<br>
 */
public class alterarOperadoresController implements Initializable {

    int id_operador_atualizar = 0;
    String email_operador_atualizar = "";
    int nivel_operador_atualizar = 1;
    String alterarPassBase = "não";
    String estado_Operador = "";
    Alert a = new Alert(Alert.AlertType.NONE);

    @FXML
    private Pane PaneComp;
    @FXML
    private Label lbEmail;
    @FXML
    private TextArea email;
    @FXML
    private Button alterarOperador;
    @FXML
    private Label lbPass;

    @FXML
    private AnchorPane painelOperadores;
    @FXML
    private ScrollPane operadoresScrollPane;
    @FXML
    private VBox vBoxOperadores;
    @FXML
    private MenuBar menu;
    @FXML
    private Label lbEstado;
    @FXML
    private ChoiceBox<String> cbpass;
    @FXML
    private ChoiceBox<String> cbestado;

    private int altura_vbox = 0;

    /**
     * Método Initialize.<br>
     *
     * Neste método inicia o programa e gera a lista de operadores.<br>
     *
     * @param url url.
     * @param rb rb.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        lbEmail.setVisible(false);
        lbPass.setVisible(false);
        lbEstado.setVisible(false);
        email.setVisible(false);
        cbpass.setVisible(false);
        cbestado.setVisible(false);
        alterarOperador.setVisible(false);
        email.setEditable(false);
        cbpass.setDisable(true);
        cbestado.setDisable(true);

        cbpass.getItems().add("sim");
        cbpass.getItems().add("não");
        cbestado.getItems().add("ativo");
        cbestado.getItems().add("inativo");

        List<utilizador> operadores = listarUtilizadoresPorTipo(1);
        for (utilizador operador : operadores) {

            int id = operador.getIdUtilizador();

            Button botao = new Button(String.valueOf(id));
            botao.setId("botaoVbox");
            botao.setPrefHeight(30);
            botao.setPrefWidth(275);
            botao.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {
                    botao.setId("botaoVboxChangeCollour");
                    apresentarOperador(id);
                }
            });
            altura_vbox = altura_vbox + 30;
            vBoxOperadores.getChildren().addAll(botao);
            if (altura_vbox > 493) {
                vBoxOperadores.setPrefHeight(altura_vbox);
            }
        }

    }

    /**
     * Método para tornar visivel os atributos do operador.<br>
     *
     *
     * @param id_operador id de utilizador do operado a pesquisar.
     */
    private void apresentarOperador(int id_operador) {

        lbEmail.setVisible(true);
        lbPass.setVisible(true);
        lbEstado.setVisible(true);
        email.setVisible(true);
        cbpass.setVisible(true);
        cbestado.setVisible(true);
        alterarOperador.setVisible(true);
        email.setEditable(false);
        cbpass.setDisable(false);
        cbestado.setDisable(false);
        List<utilizador> operadores = procurarUtilizadorID(id_operador);
        for (utilizador operador : operadores) {
            id_operador_atualizar = id_operador;
            email.setText(operador.getEmail());
            email_operador_atualizar = email.getText();
            cbpass.setValue(alterarPassBase);
            cbestado.setValue(operador.getEstado());
            estado_Operador = cbestado.getValue(); //se não passar este valor colocar para dar erro
            if (estado_Operador.equals("")) {
                a.setAlertType(Alert.AlertType.ERROR);
                a.setTitle("ERRO SQL");
                a.setHeaderText("Dados do operador em falta!");
                a.setContentText("Não foi possível obter os dados necessários.Tente aceder a outro operador.\n"
                        + "\nCaso esteja tudo em ordem com o segundo operador,"
                        + "\nconsulte a base de dados para verificar se existem dados do primeiro operador em falta."
                        + "\nCaso estes também estejam em ordem, consulte a equipa de desenvolvimento da aplicação\n"
                        + "pois existe um problema com as configurações da aplicação.");
                a.show();
                alterarOperadores altOps = new alterarOperadores();
                fecha();
                try {
                    altOps.start(new Stage());
                } catch (Exception ex) {
                    a.setAlertType(Alert.AlertType.ERROR);
                    a.setTitle("ERRO Fatal");
                    a.setHeaderText("A aplicação está corrompida!");
                    a.setContentText("Algumas funcionalidades do aplicação estão inacessíveis. Tente uma reinstalação.");
                    a.show();
                    System.out.println("Erro Exception ex " + ex.getMessage());
                    Logger.getLogger(alterarOperadoresController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    /**
     * Este método vai fazer varias confirmaçoes as alteraçoes que ocorreram ao
     * operador e depois de passar a elas todas vai alterar os valores na BD.<br>
     *
     * @param event evento
     */
    @FXML
    private void alterarOperador(ActionEvent event) {

        a.setAlertType(Alert.AlertType.CONFIRMATION);
        a.setTitle("Operadores");
        a.setHeaderText("Deseja mesmo atualizar o operador?");
        a.setResizable(false);
        a.getButtonTypes().setAll(ButtonType.YES, ButtonType.NO);
        Optional<ButtonType> result = a.showAndWait();

        if (result.get() == ButtonType.YES) {

            Alert a2 = new Alert(Alert.AlertType.NONE);

            //verificar se o utilizador ainda existe na BD
            List<utilizador> utilizadores = procurarUtilizadorIDEmail(id_operador_atualizar, email_operador_atualizar);

            int count = 0;
            for (utilizador uti : utilizadores) {
                count = 1;
            }
            if (count == 0) {
                a.setAlertType(Alert.AlertType.INFORMATION);
                a.setTitle("Operador");
                a.setHeaderText("O operador que estava a alterar foi removido!");
                a.setContentText("O operador for eliminado enquanto o alterava.");
                a.setResizable(false);
                a.show();
                alterarOperadores altOps = new alterarOperadores();
                fecha();
                try {
                    altOps.start(new Stage());
                } catch (Exception ex) {
                    a.setAlertType(Alert.AlertType.ERROR);
                    a.setTitle("ERRO Fatal");
                    a.setHeaderText("A aplicação está corrompida!");
                    a.setContentText("Algumas funcionalidades do aplicação estão inacessíveis. Tente uma reinstalação.");
                    a.show();
                    System.out.println("Erro Exception ex " + ex.getMessage());
                    Logger.getLogger(alterarOperadoresController.class.getName()).log(Level.SEVERE, null, ex);
                }

            } else { // existindo o utilizador, proceder à validação
                //dupla validação do estado do operador e restante validação dos restantes elementos
                if (estado_Operador.equals("") || estado_Operador == null || alterarPassBase.equals("") || alterarPassBase == null
                        || "".equals(cbpass.getValue()) || cbpass.getValue() == null
                        || "".equals(cbestado.getValue()) || cbestado.getValue() == null) {
                    a.setAlertType(Alert.AlertType.ERROR);
                    a.setTitle("ERRO Programação");
                    a.setHeaderText("Dados do operador ou de configuração da aplicação em falta!");
                    a.setContentText("Não foi possível obter os dados necessários.Tente aceder a outro operador.\n"
                            + "\nCaso esteja tudo em ordem com o segundo operador,"
                            + "\nconsulte a base de dados para verificar se existem dados do primeiro operador em falta."
                            + "\nCaso estes também estejam em ordem, consulte a equipa de desenvolvimento da aplicação\n"
                            + "pois existe um problema com as configurações da aplicação.");
                    a.show();
                    alterarOperadores altOps = new alterarOperadores();
                    fecha();
                    try {
                        altOps.start(new Stage());
                    } catch (Exception ex) {
                        a.setAlertType(Alert.AlertType.ERROR);
                        a.setTitle("ERRO Fatal");
                        a.setHeaderText("A aplicação está corrompida!");
                        a.setContentText("Algumas funcionalidades do aplicação estão inacessíveis. Tente uma reinstalação.");
                        a.show();
                        System.out.println("Erro Exception ex " + ex.getMessage());
                        Logger.getLogger(alterarOperadoresController.class.getName()).log(Level.SEVERE, null, ex);
                    }

                } else {//tendo os campos validados, proceder à alteração do utilizador

                    //aviso para o caso do utilizador clicar no botão de alterar sem mudar parametros (pass e estado inalterados))
                    if (alterarPassBase.equals(cbpass.getValue()) && estado_Operador.equals(cbestado.getValue())) {
                        a2.setAlertType(Alert.AlertType.INFORMATION);
                        a2.setTitle("Operadores");
                        a2.setHeaderText("Não efectuou nenhuma alteração ao operador!");
                        a2.setContentText("Modifique um atributo que pretenda alterar.");
                        a2.getButtonTypes().setAll(ButtonType.OK);
                        Optional<ButtonType> result2 = a2.showAndWait();
                    } else //notificação de alteração de estado de utilizador sem alteração da pass (pass inalterada, estado alterado)
                    if (alterarPassBase.equals(cbpass.getValue()) && !estado_Operador.equals(cbestado.getValue())) {

                        ativarInativarUtilizador(cbestado.getValue(), id_operador_atualizar, nivel_operador_atualizar);
                        a2.setAlertType(Alert.AlertType.INFORMATION);

                        a2.setTitle("Operadores");
                        a2.setHeaderText("Estado do operador alterado com sucesso!");
                        a2.setContentText("O estado do operador foi alterado para " + cbestado.getValue() + ".");
                        a2.getButtonTypes().setAll(ButtonType.OK);
                        Optional<ButtonType> result2 = a2.showAndWait();

                        alterarOperadores altOps = new alterarOperadores();
                        fecha();
                        try {
                            altOps.start(new Stage());
                        } catch (Exception ex) {
                            a.setAlertType(Alert.AlertType.ERROR);
                            a.setTitle("ERRO Fatal");
                            a.setHeaderText("A aplicação está corrompida!");
                            a.setContentText("Algumas funcionalidades do aplicação estão inacessíveis. Tente uma reinstalação.");
                            a.show();
                            System.out.println("Erro Exception ex " + ex.getMessage());
                            Logger.getLogger(alterarOperadoresController.class.getName()).log(Level.SEVERE, null, ex);
                        }

                    } else //notificação de alteração inválida de password de utilizador inativo (pass alterada e estado inativo)
                    if ("sim".equals(cbpass.getValue()) && "inativo".equals(cbestado.getValue())) {
                        a2.setAlertType(Alert.AlertType.INFORMATION);
                        a2.setTitle("Operadores");
                        a2.setHeaderText("Tentou alterar password de um operador inativo!");
                        a2.setContentText("Ative o operador caso também pretenda alterar a sua password.");
                        a2.getButtonTypes().setAll(ButtonType.OK);
                        Optional<ButtonType> result2 = a2.showAndWait();

                    } else //notificação de alteração de estado de utilizador sem alteração da pass (pass inalterada, estado alterado)
                    if ("sim".equals(cbpass.getValue()) && "ativo".equals(cbestado.getValue())) {

                        //se o estado já está ativo e não se altera o estado
                        //algoritmo de alterar a password apenas - o estado é o inicial do utilizador
                        if (estado_Operador.equals(cbestado.getValue())) {

                            try {

                                //tirado da classe inserirOperadoresController
                                String myPassword = generatePassword(20);

                                // Generate Salt. The generated value can be stored in DB.
                                String salt = passwordUtils.getSalt(30);

                                // Protect user's password. The generated value can be stored in DB.
                                String mySecurePassword = passwordUtils.generateSecurePassword(myPassword, salt);

                                //enviar email ao operador
                                enviarEmail(email_operador_atualizar, myPassword, "");

                                //alterar pass operador
                                alterarPassUtilizador(mySecurePassword, salt, id_operador_atualizar, email_operador_atualizar, nivel_operador_atualizar, estado_Operador);

                                a2.setAlertType(Alert.AlertType.INFORMATION);
                                a2.setTitle("Operadores");
                                a2.setHeaderText("Password do operador alterada com sucesso!");
                                a2.setContentText("Foi enviada um email ao operador com a sua nova password.");
                                a2.getButtonTypes().setAll(ButtonType.OK);
                                Optional<ButtonType> result2 = a2.showAndWait();

                                alterarOperadores altOps = new alterarOperadores();
                                fecha();
                                try {
                                    altOps.start(new Stage());
                                } catch (Exception ex) {
                                    a.setAlertType(Alert.AlertType.ERROR);
                                    a.setTitle("ERRO Fatal");
                                    a.setHeaderText("A aplicação está corrompida!");
                                    a.setContentText("Algumas funcionalidades do aplicação estão inacessíveis. Tente uma reinstalação.");
                                    a.show();
                                    System.out.println("Erro Exception ex " + ex.getMessage());
                                    Logger.getLogger(alterarOperadoresController.class.getName()).log(Level.SEVERE, null, ex);
                                }

                            } catch (Exception passwordUpdate) {
                                a.setAlertType(Alert.AlertType.ERROR);
                                a.setTitle("ERRO ATUALIZAR PASSWORD OPERADOR");
                                a.setHeaderText("Não foi possível atualizar a password do operador!");
                                a.setContentText("A base de dados pode estar ocupa ou indisponível ou não foi possível contactar o operador.");
                                a.show();
                                System.out.println("Erro SQL: " + passwordUpdate.getMessage());
                                Logger.getLogger(listarOperadoresController.class.getName()).log(Level.SEVERE, null, passwordUpdate);
                            }

                        } else // altera pass e estado
                        {
                            try {
                                //tirado da classe inserirOperadoresController
                                String myPassword = generatePassword(20);

                                // Generate Salt. The generated value can be stored in DB.
                                String salt = passwordUtils.getSalt(30);

                                // Protect user's password. The generated value can be stored in DB.
                                String mySecurePassword = passwordUtils.generateSecurePassword(myPassword, salt);

                                //enviar email ao operador
                                enviarEmail(email_operador_atualizar, myPassword, "");

                                //alterar pass e estado
                                alterarPassEstadoUtilizador(mySecurePassword, salt, cbestado.getValue(), id_operador_atualizar, email_operador_atualizar, nivel_operador_atualizar);

                                a2.setAlertType(Alert.AlertType.INFORMATION);

                                a2.setTitle("Operadores");
                                a2.setHeaderText("Estado e password do operador alterados com sucesso!");
                                a2.setContentText("O estado do operador foi alterado para " + cbestado.getValue() + ".\nFoi enviada um email ao operador com a sua nova password.");
                                a2.getButtonTypes().setAll(ButtonType.OK);
                                Optional<ButtonType> result2 = a2.showAndWait();

                                alterarOperadores altOps = new alterarOperadores();
                                fecha();
                                try {
                                    altOps.start(new Stage());
                                } catch (Exception ex) {
                                    a.setAlertType(Alert.AlertType.ERROR);
                                    a.setTitle("ERRO Fatal");
                                    a.setHeaderText("A aplicação está corrompida!");
                                    a.setContentText("Algumas funcionalidades do aplicação estão inacessíveis. Tente uma reinstalação.");
                                    a.show();
                                    System.out.println("Erro Exception ex " + ex.getMessage());
                                    Logger.getLogger(alterarOperadoresController.class.getName()).log(Level.SEVERE, null, ex);
                                }

                            } catch (Exception passwordEstadoUpdate) {
                                a.setAlertType(Alert.AlertType.ERROR);
                                a.setTitle("ERRO ATUALIZAR PASSWORD E ESTADO OPERADOR");
                                a.setHeaderText("Não foi possível atualizar o operador!");
                                a.setContentText("A base de dados pode estar ocupa ou indisponível ou não foi possível contactar o operador.");
                                a.show();
                                System.out.println("Erro SQL: " + passwordEstadoUpdate.getMessage());
                                Logger.getLogger(listarOperadoresController.class.getName()).log(Level.SEVERE, null, passwordEstadoUpdate);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Método para terminar sessao do utilizador.<br>
     *
     * @param event evento
     */
    @FXML
    private void terminarSessao(ActionEvent event) {
        try {
            login.setIdLogin(0);
            login.setNivelLogin(10000);
            login log = new login();
            fecha();
            log.start(new Stage());

        } catch (Exception ex) {
            Logger.getLogger(menuAdministradorController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Método para encerrar a aplicação.<br>
     */
    @FXML
    private void sair(ActionEvent event
    ) {
        System.exit(0);
    }

    /**
     * Método para aceder ao menu do administrador.<br>
     */
    @FXML
    private void openMenu(ActionEvent event
    ) {
        try {
            menuAdministrador menuAdmin = new menuAdministrador();
            fecha();
            menuAdmin.start(new Stage());

        } catch (Exception ex) {
            Logger.getLogger(eliminarOperadoresController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Método para fechar o ecrã de alteração do operador.<br>
     */
    private void fecha() {
        alterarOperadores.getStage().close();
    }
}
