/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bfgir;

import classes.helper;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * classe do ecrã removerClienteAssociado
 */
public class removerClienteAssociado extends Application {

    private static Stage stage;

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/ecras/removerClienteAssociado.fxml"));

        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.setTitle("F&O - Remover Preços Associados");
        stage.getIcons().add(helper.appIcon);
        stage.setResizable(false);
        stage.show();
        setStage(stage);

    }

    /**
     * método main do ecrã removerClienteAssociado
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * método getStage do ecrã removerClienteAssociado
     *
     * @return retorna o JAVAFX container do ecrã removerClienteAssociado
     */
    public static Stage getStage() {
        return stage;
    }

    /**
     * método setStage do ecrã removerClienteAssociado
     *
     * @param aStage the aStage to set
     */
    public static void setStage(Stage aStage) {
        stage = aStage;
    }

}