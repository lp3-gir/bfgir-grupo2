/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ruben
 */
public class ClienteprodutoTest {



    /**
     * Teste para definir o objeto clienteprodutoPK.
     */
    @Test
    public void testGetClienteprodutoPK() {
        System.out.println("getClienteprodutoPK");

        ClienteprodutoPK cppk = new ClienteprodutoPK();
        cppk.setIdCliente(1);
        cppk.setCodProdPa("T.123");

        Clienteproduto instance = new Clienteproduto();
        instance.setClienteprodutoPK(cppk);

        assertEquals(cppk, instance.getClienteprodutoPK());

    }

    /**
     * Teste para retirar o objeto clienteprodutoPK.
     */
    @Test
    public void testSetClienteprodutoPK() {
        System.out.println("setClienteprodutoPK");
        ClienteprodutoPK cppk = new ClienteprodutoPK();
        cppk.setIdCliente(1);
        cppk.setCodProdPa("T.123");

        Clienteproduto instance = new Clienteproduto();
        instance.setClienteprodutoPK(cppk);

    }

    /**
     * Teste para retirar o preço do produto por unidade.
     */
    @Test
    public void testGetPrecoUnitSIVA() {
        System.out.println("getPrecoUnitSIVA");
        Clienteproduto instance = new Clienteproduto();
        instance.setPrecoUnitSIVA(2.53);
        
        assertEquals(2.53, instance.getPrecoUnitSIVA(), 0.0);

    }

    /**
     * Teste para retirar o preço do produto por unidade.
     */
    @Test
    public void testSetPrecoUnitSIVA() {
        System.out.println("setPrecoUnitSIVA");
        Clienteproduto instance = new Clienteproduto();
        instance.setPrecoUnitSIVA(2.53);

    }

}
