/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import bfgir.listarOperadores;
import bfgir.login;
import bfgir.menuAdministrador;
import static classes.administrador.listarUtilizadoresPorTipo;
import static classes.administrador.procurarUtilizadorID;
import classes.utilizador;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * FXML Controller class
 */
public class listarOperadoresController implements Initializable {

    Alert a = new Alert(Alert.AlertType.NONE);
    @FXML
    private Pane painelListarOperadores;
    @FXML
    private ScrollPane operadoresScrollPane;
    @FXML
    private VBox vBoxListarOperadores;
    @FXML
    private AnchorPane fundo;
    @FXML
    private Label lbEmail;
    @FXML
    private Label lbEstado;
    @FXML
    private TextArea email;
    @FXML
    private TextArea estado;
    @FXML
    private Label lbPass;
    @FXML
    private TextArea password;
    @FXML
    private MenuBar menu;

    private int altura_vbox = 0;

    /**
     * Initializes the controller class. Lista por id todos os utlizadores
     * operadores. Cria tambem um botão para cada operador e com o seu id de
     * Utilizador
     *
     * @param url url
     * @param rb rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        lbEmail.setVisible(false);
        lbPass.setVisible(false);
        lbEstado.setVisible(false);
        email.setVisible(false);
        password.setVisible(false);
        estado.setVisible(false);
        email.setEditable(false);
        password.setEditable(false);
        estado.setEditable(false);
        List<utilizador> operadores = listarUtilizadoresPorTipo(1);
        for (utilizador operador : operadores) {

            int id = operador.getIdUtilizador();
            Button botao = new Button(String.valueOf(id));
            botao.setId("botaoVbox");
            botao.setPrefHeight(30);
            botao.setPrefWidth(275);
            botao.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {

                    botao.setId("botaoVboxChangeCollour");
                    apresentarOperador(id);

                }
            });

            altura_vbox = altura_vbox + 30;
            vBoxListarOperadores.getChildren().addAll(botao);
            if (altura_vbox > 550) {
                vBoxListarOperadores.setPrefHeight(altura_vbox);
            }

        }

    }

    /**
     * Método para mostrar alguns atributos do operador selecionado. Não mostra
     * a password real.
     *
     * @param id_operador ID do operador
     */
    private void apresentarOperador(int id_operador) {

        lbEmail.setVisible(true);
        lbPass.setVisible(true);
        lbEstado.setVisible(true);
        email.setVisible(true);
        password.setVisible(true);
        estado.setVisible(true);

        List<utilizador> operadores = procurarUtilizadorID(id_operador);

        for (utilizador operador : operadores) {
            email.setText(operador.getEmail());
            password.setText("*********");
            estado.setText(operador.getEstado());
        }

    }

    /**
     * método para fechar o ecrã de listar os operadores
     */
    private void fecha() {
        listarOperadores.getStage().close();
    }

    /**
     * Metodo para terminar sessao do utilizador
     *
     * @param event evento
     */
    @FXML
    private void terminarSessao(ActionEvent event) {
        try {
            login.setIdLogin(0);
            login.setNivelLogin(10000);
            login log = new login();
            fecha();
            log.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuAdministradorController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Método para aceder ao menu do administrador
     */
    @FXML
    private void openMenu(ActionEvent event) {
        try {
            menuAdministrador menuAdmin = new menuAdministrador();
            fecha();
            menuAdmin.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(listarOperadoresController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Método para encerrar a aplicação
     */
    @FXML
    private void sair(ActionEvent event) {
        System.exit(0);

    }

}
