/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.service;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ruben
 */
public class ProdutoFacadeRESTTest {

    /**
     *
     */
    public ProdutoFacadeRESTTest() {
    }

    /**
     * Test of calcPrecoTotalCIVA method, of class ProdutoFacadeREST.
     */
    @Test
    public void testCalcPrecoTotalCIVA() {
        System.out.println("calcPrecoTotalCIVA");
        double precoProd = 10;
        double taxaDesc = 10;

        ProdutoFacadeREST pfr = new ProdutoFacadeREST();

        double expResult = 13.42;
        double result = Math.round(pfr.calcPrecoTotalCIVA(precoProd, taxaDesc) * 100.0) / 100.0;

        assertEquals(expResult, result, 0.0);
    }

}
