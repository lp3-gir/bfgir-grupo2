/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import bfgir.ecraPessoalFornecedor;
import bfgir.login;
import bfgir.menuFornecedor;
import bfgir.uploadFicheiroFornecedor;
import classes.entradas;
import static classes.entradas.inserirEntradas;
import static classes.entradas.verifEnt;
import classes.entradasProdutos;
import static classes.entradasProdutos.atualizarEntradasProdutos;
import static classes.entradasProdutos.inserirEntradasProdutos;
import static classes.entradasProdutos.validaProdutoEntrada;
import classes.fornecedor;
import static classes.fornecedor.compararIDFornecedor;
import static classes.peso.pesquisarPeso;
import classes.produto;
import static classes.produto.inserirProdutos;
import static classes.produto.verifProd;
import classes.produtoFornecedor;
import static classes.produtoFornecedor.procurarCodProdPa;
import classes.utilizador;
import static classes.utilizador.procurarEmail;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.net.URL;
import java.nio.file.Path;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.MenuBar;
import javafx.stage.Stage;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.Random;
import java.util.Scanner;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.stage.FileChooser;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * FXML Controller class
 *
 */
public class uploadFicheiroFornecedorController implements Initializable {

    @FXML
    private Button carregarFicheiro;
    @FXML
    private Button enviarFicheiro;

    private Path to;
    private Path to2;
    private Path from;
    private File selectedFile;

    private static String nomeFicheiro = "";
    private static String nomeFicheiro2 = "";

    private static String OrderConfirmationReference = "";

    Alert a = new Alert(Alert.AlertType.NONE);
    @FXML
    private MenuBar menu;

    private String extFicheiro;
    @FXML
    private Menu opcoes;

    /**
     * Initializes the controller class.
     *
     * @param url url
     * @param rb rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        adicionarEcraPessoalFornecedorMenuOpcoes();
    }

    /**
     * Metodo que vai fazer as verificações para se o ficheiro pode ser
     * carregado e se se confirmar carrega o ficheiro
     *
     * @param event evento
     */
    @FXML
    private void carregarFicheiro(ActionEvent event) {

        FileChooser fc = new FileChooser();
        fc.setTitle("Adicionar ficheiro XML/JSON");
        selectedFile = fc.showOpenDialog(null);

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        java.util.Date data = new java.util.Date();
        String timestamp = dateFormat.format(data);

        String conteudoFicheiro = "";
        if (selectedFile != null) {
            try {
                from = Paths.get(selectedFile.toURI());
                to = Paths.get("src/XML/" + addTimestamp(selectedFile.getName(), timestamp));
                to2 = Paths.get("src/JSON/" + addTimestamp(selectedFile.getName(), timestamp));
                //Files.copy(from.toFile(), to.toFile());
                String ficheiroTexto = selectedFile.getName();
                extFicheiro = ficheiroTexto.substring(ficheiroTexto.lastIndexOf(".") + 1, selectedFile.getName().length());
                if ("xml".equals(extFicheiro)) {
                    nomeFicheiro = "src\\XML\\" + addTimestamp(selectedFile.getName(), timestamp);
                    //copiar ficheiro
                    Files.copy(from, to);
                    a.setAlertType(Alert.AlertType.INFORMATION);
                    a.setTitle("Ficheiro selecionado");
                    a.setHeaderText("Ficheiro selecionado com sucesso!");
                    a.setContentText("O ficheiro XML escolhido foi selecionado com sucesso.");
                    a.show();
                } else if ("json".equals(extFicheiro)) {
                    nomeFicheiro2 = "src\\JSON\\" + addTimestamp(selectedFile.getName(), timestamp);
                    Files.copy(from, to2);
                    //
                    //ler dados do ficheiro copiado
                    File myObj = new File(nomeFicheiro2);
                    Scanner myReader = new Scanner(myObj);
                    while (myReader.hasNextLine()) {
                        conteudoFicheiro += myReader.nextLine();
                    }
                    myReader.close();
                    char charZero = conteudoFicheiro.charAt(0);
                    if ("{".equals(String.valueOf(charZero))) {
                        //
                        //transformar ficheiro em JSONArray
                        try {
                            FileWriter myWriter = new FileWriter(nomeFicheiro2);
                            myWriter.write("[" + conteudoFicheiro + "]");
                            myWriter.close();
                        } catch (Exception e) {
                            a.setAlertType(Alert.AlertType.ERROR);
                            a.setTitle("Formato incorreto");
                            a.setHeaderText("Formato ficheiro incorreto!");
                            a.setContentText("Ficheiro JSON com formato incorreto. \n "
                                    + "Introduza um ficheiro correto.");
                            a.show();
                        }
                    }

                    a.setAlertType(Alert.AlertType.INFORMATION);
                    a.setTitle("Ficheiro selecionado");
                    a.setHeaderText("Ficheiro selecionado com sucesso!");
                    a.setContentText("O ficheiro JSON escolhido foi selecionado com sucesso.");
                    a.show();
                } else {
                    nomeFicheiro = "";
                    a.setAlertType(Alert.AlertType.ERROR);
                    a.setTitle("Introduza um ficheiro XML ou JSON");
                    a.setHeaderText("Introduza um ficheiro XML ou JSON!");
                    a.setContentText("Introduza um ficheiro XML ou JSON para poder efetuar o envio do comprovativo da entrada.");
                    a.show();
                }
            } catch (IOException ex) {
                Logger.getLogger(uploadFicheiroFornecedorController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    /**
     * Metodo que vai fazer as verificaçoes necessarias e depois enviar o
     * ficheiro
     *
     * @param event evento
     */
    @FXML
    private void enviarFicheiro(ActionEvent event
    ) {
        if ("".equals(nomeFicheiro) && "".equals(nomeFicheiro2)) {
            a.setAlertType(Alert.AlertType.ERROR);
            a.setTitle("Introduza um ficheiro XML");
            a.setHeaderText("Introduza um ficheiro XML!");
            a.setContentText("Introduza um ficheiro XML para poder efetuar o envio do comprovativo da entrada.");
            a.show();
        } else {
            try {
                if ("XML".equals(extFicheiro) || "xml".equals(extFicheiro)) {
                    File fXmlFile = new File(nomeFicheiro);
                    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                    Document doc = dBuilder.parse(fXmlFile);
                    //optional, but recommended
                    //read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work

                    doc.getDocumentElement().normalize();
                    //System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
                    NodeList nList;
                    nList = doc.getElementsByTagName("OrderConfirmationHeader");
                    boolean verif = false;
                    boolean verif2 = false;
                    String idForn = "";
                    //verificar se já a entrada já foi introduzida
                    for (int temp = 0; temp < nList.getLength(); temp++) {
                        Node node = nList.item(temp);
                        if (node.getNodeType() == Node.ELEMENT_NODE) {
                            Element eElement = (Element) node;
                            verif = verifEnt(eElement.getElementsByTagName("OrderConfirmationReference").item(0).getTextContent());
                        }
                    }
                    //
                    //verificar se o id de fornecedor da entrada corresponde ao seu ID
                    for (int temp = 0; temp < nList.getLength(); temp++) {
                        Node node = nList.item(temp);
                        if (node.getNodeType() == Node.ELEMENT_NODE) {
                            Element eElement = (Element) node;
                            idForn = eElement.getElementsByTagName("PartyIdentifier").item(0).getTextContent();

                            List<fornecedor> fornecedores = compararIDFornecedor(login.getIdLogin());
                            for (fornecedor fornecedor : fornecedores) {
                                if (idForn.equals(fornecedor.getIdFornecedor())) {
                                    verif2 = true;
                                }
                            }
                        }
                    }
                    if (verif2 == true) {
                        if (verif == false) {
                            /*
                            System.out.println("------------------------------------------------");
                            System.out.println("---------------------HEAD-----------------------");
                            System.out.println("------------------------------------------------");*/
                            nList = doc.getElementsByTagName("OrderConfirmationHeader");
                            getHeaderFromXML(nList);

                            /*System.out.println("------------------------------------------------");
                            System.out.println("---------------------ITEM-----------------------");
                            System.out.println("------------------------------------------------");*/
                            nList = doc.getElementsByTagName("OrderConfirmationLineItem");
                            getItemsFromXML(nList);

                        } else {
                            a.setAlertType(Alert.AlertType.ERROR);
                            a.setTitle("Entrada já introduzida");
                            a.setHeaderText("Entrada já introduzida!");
                            a.setContentText("A entrada escolhida para upload já se encontra registada na BD.");
                            a.show();
                        }
                    } else {
                        a.setAlertType(Alert.AlertType.ERROR);
                        a.setTitle("Ficheiro entrada incorreto");
                        a.setHeaderText("Ficheiro entrada incorreto!");
                        a.setContentText("O código de fornecedor presente no ficheiro de entrada não corresponde ao seu código de fornecedor."
                                + "\n Introduza um ficheiro com o seu id de fornecedor.");
                        a.show();
                    }
                } else if ("JSON".equals(extFicheiro) || "json".equals(extFicheiro)) {
                    JSONParser parser = new JSONParser();
                    JSONArray jsonFileData = null;
                    try {
                        jsonFileData = (JSONArray) parser.parse(new FileReader(nomeFicheiro2));
                    } catch (ParseException e) {
                        a.setAlertType(Alert.AlertType.ERROR);
                        a.setTitle("Formato incorreto");
                        a.setHeaderText("Formato ficheiro incorreto!");
                        a.setContentText("Ficheiro JSON com formato incorreto. \n "
                                + "Introduza um ficheiro correto.");
                        a.show();
                    }
                    boolean verif = false;
                    boolean verif2 = false;
                    boolean verif3 = true;
                    String idForn = "";
                    try {
                        //verificar se já a entrada já foi introduzida
                        for (Object o : jsonFileData) {
                            JSONObject orderConfirmation = (JSONObject) o;
                            //retirar dados sobre o fornecedor
                            JSONObject orderConfirmationObject = (JSONObject) orderConfirmation.get("OrderConfirmation");
                            JSONObject orderConfirmationHeader = (JSONObject) orderConfirmationObject.get("OrderConfirmationHeader");
                            verif = verifEnt((String) orderConfirmationHeader.get("OrderConfirmationReference"));
                        }
                        //
                        //
                        //verificar se o id de fornecedor da entrada corresponde ao seu ID
                        for (Object o : jsonFileData) {
                            JSONObject orderConfirmation = (JSONObject) o;
                            //retirar dados sobre o fornecedor
                            JSONObject orderConfirmationObject = (JSONObject) orderConfirmation.get("OrderConfirmation");
                            JSONObject orderConfirmationHeader = (JSONObject) orderConfirmationObject.get("OrderConfirmationHeader");
                            JSONObject supplierParty = (JSONObject) orderConfirmationHeader.get("SupplierParty");
                            idForn = (String) supplierParty.get("PartyIdentifier");

                            List<fornecedor> fornecedores = compararIDFornecedor(login.getIdLogin());
                            for (fornecedor fornecedor : fornecedores) {
                                if (idForn.equals(fornecedor.getIdFornecedor())) {
                                    verif2 = true;
                                }
                            }
                        }
                    } catch (Exception e) {
                        verif3 = false;
                    }

                    if (verif3 == true) {
                        if (verif2 == true) {
                            if (verif == false) {
                                getHeaderFromJSON(jsonFileData);
                                getItemsFromJSON(jsonFileData);

                            } else {
                                a.setAlertType(Alert.AlertType.ERROR);
                                a.setTitle("Entrada já introduzida");
                                a.setHeaderText("Entrada já introduzida!");
                                a.setContentText("A entrada escolhida para upload já se encontra registada na BD.");
                                a.show();
                            }
                        } else {
                            a.setAlertType(Alert.AlertType.ERROR);
                            a.setTitle("Ficheiro entrada incorreto");
                            a.setHeaderText("Ficheiro entrada incorreto!");
                            a.setContentText("O código de fornecedor presente no ficheiro de entrada não corresponde ao seu código de fornecedor."
                                    + "\n Introduza um ficheiro com o seu id de fornecedor.");
                            a.show();
                        }
                    } else {
                        a.setAlertType(Alert.AlertType.ERROR);
                        a.setTitle("Formato incorreto");
                        a.setHeaderText("Formato ficheiro incorreto!");
                        a.setContentText("Ficheiro JSON com formato incorreto. \n "
                                + "Introduza um ficheiro correto.");
                        a.show();
                    }

                }
            } catch (SAXException | IOException | ParserConfigurationException ex) {
                Logger.getLogger(uploadFicheiroFornecedorController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Metodo para terminar sessao do utilizador
     *
     * @param event evento
     */
    @FXML
    private void terminarSessao(ActionEvent event
    ) {
        try {
            login.setIdLogin(0);
            login.setNivelLogin(10000);
            login log = new login();
            fecha();
            log.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(uploadFicheiroFornecedorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Este metodo vai criar e adicionar um TimeStamp
     *
     * @param name  recebe o nome do ficheiro
     * @param ts recebe o timeStamp
     * @return timeStamp
     */
    private static String addTimestamp(String name, String ts) {
        int lastIndexOf = name.lastIndexOf('.');
        return (lastIndexOf == -1
                ? ts
                : ts
                + name.substring(lastIndexOf))
                .replaceAll("[\\/:\\*\\?\"<>| ]", "_");
    }

    /**
     * Metodo que vai buscar o header do XML
     *
     * @param nList recebe uma node list
     */
    private void getHeaderFromXML(NodeList nList) {

        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node node = nList.item(temp);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) node;
                OrderConfirmationReference = eElement.getElementsByTagName("OrderConfirmationReference").item(0).getTextContent();
                String Year = eElement.getElementsByTagName("Year").item(0).getTextContent();
                String Month = eElement.getElementsByTagName("Month").item(0).getTextContent();
                String Day = eElement.getElementsByTagName("Day").item(0).getTextContent();
                String PartyIdentifier = eElement.getElementsByTagName("PartyIdentifier").item(0).getTextContent();
                String Name = eElement.getElementsByTagName("Name").item(0).getTextContent();
                String Address1 = eElement.getElementsByTagName("Address1").item(0).getTextContent();
                String Address2 = "";
                try {
                    Address2 = eElement.getElementsByTagName("Address2").item(0).getTextContent();
                } catch (Exception e) {
                    //String exception = "Address 2 não existe";
                    //System.out.println("");
                }
                String Address = Address1 + " " + Address2;
                String City = eElement.getElementsByTagName("City").item(0).getTextContent();
                String PostalCode = eElement.getElementsByTagName("PostalCode").item(0).getTextContent();
                NamedNodeMap ISOCountryCode = eElement.getElementsByTagName("Country").item(0).getAttributes();
                String str = String.valueOf(ISOCountryCode.getNamedItem("ISOCountryCode"));
                String[] arrOfStr = str.split("=", 2);
                entradas enc = new entradas();
                enc.setIdEntrada(OrderConfirmationReference);
                enc.setIdFornecedor(PartyIdentifier);
                enc.setDataEntrada(Day + "/" + Month + "/" + Year);
                enc.setEstado("pendente");
                inserirEntradas(enc);

            }
        }
    }

    /**
     * Metodo que vai buscar o header do JSON
     *
     * @param jsonFileData recebe a informação do ficheiro JSON
     */
    private void getHeaderFromJSON(JSONArray jsonFileData) {

        for (Object o : jsonFileData) {
            JSONObject orderConfirmation = (JSONObject) o;
            //retirar dados sobre o fornecedor
            JSONObject orderConfirmationObject = (JSONObject) orderConfirmation.get("OrderConfirmation");
            JSONObject orderConfirmationHeader = (JSONObject) orderConfirmationObject.get("OrderConfirmationHeader");
            OrderConfirmationReference = (String) orderConfirmationHeader.get("OrderConfirmationReference");
            String orderConfirmationIssuedDate = (String) orderConfirmationHeader.get("OrderConfirmationIssuedDate");
            JSONObject supplierParty = (JSONObject) orderConfirmationHeader.get("SupplierParty");
            String partyIdentifier = (String) supplierParty.get("PartyIdentifier");
            JSONObject nameAddress = (JSONObject) supplierParty.get("NameAddress");
            String name = (String) nameAddress.get("Name");
            String address1 = (String) nameAddress.get("Address1");
            String address2 = "";
            try {
                address2 = (String) nameAddress.get("Address2");
            } catch (Exception e) {
                //String exception = "Address 2 não existe";
                //System.out.println("");
            }
            String Address = address1 + " " + address2;
            String city = (String) nameAddress.get("City");
            String postalCode = (String) nameAddress.get("PostalCode");
            JSONObject country = (JSONObject) nameAddress.get("Country");
            String iSOCountryCode = (String) country.get("ISOCountryCode");
            String value = (String) country.get("Value");
            //
            entradas ent = new entradas();
            ent.setIdEntrada(OrderConfirmationReference);
            ent.setIdFornecedor(partyIdentifier);
            String str = orderConfirmationIssuedDate;
            String[] data = str.split("-", 3);
            ent.setDataEntrada(data[2] + "/" + data[1] + "/" + data[0]);
            ent.setEstado("pendente");
            inserirEntradas(ent);

        }
    }

    /**
     * Metodo que vai buscar todos os items do ficheiro XML
     *
     * @param nList recebe uma node List
     */
    private void getItemsFromXML(NodeList nList) {

        int temp;
        int x = 0;
        float peso = 0;
        String tipoPeso = "";
        int quantVend = 0;
        String tipoQuantVend = "";
        for (temp = 0; temp < nList.getLength(); temp++) {
            Node node = nList.item(temp);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) node;
                String OrderConfirmationLineItemNumber = eElement.getElementsByTagName("OrderConfirmationLineItemNumber").item(0).getTextContent();
                String ProductIdentifierBuyer = null;
                NamedNodeMap buyer = findAttributesInNodes("Product", "ProductIdentifier", temp);
                String str8 = String.valueOf(buyer.getNamedItem("Agency"));
                String[] arrOfStr8 = str8.split("=", 2);
                if ("Buyer".equals(arrOfStr8[1].replace("\"", ""))) {
                    ProductIdentifierBuyer = eElement.getElementsByTagName("ProductIdentifier").item(0).getTextContent();
                } else {
                    String ProductIdentifierSupplier = eElement.getElementsByTagName("ProductIdentifier").item(0).getTextContent();

                    List<produtoFornecedor> prodForn = procurarCodProdPa(ProductIdentifierSupplier);
                    for (produtoFornecedor produtosFornecedores : prodForn) {
                        ProductIdentifierBuyer = produtosFornecedores.getCodProdPa();
                    }

                    if ("".equals(ProductIdentifierBuyer) || ProductIdentifierBuyer == null) {
                        Random gerador = new Random();
                        Long c = gerador.nextLong();
                        while (c < 999999999) {
                            c = gerador.nextLong();
                        }
                        ProductIdentifierBuyer = String.valueOf(c);
                    }
                }
                String ProductDescription = eElement.getElementsByTagName("ProductDescription").item(0).getTextContent();
                String CurrencyValueUnit = eElement.getElementsByTagName("CurrencyValue").item(0).getTextContent();
                //
                NamedNodeMap CurrencyType = findAttributesInNodes("PriceDetails", "CurrencyValue", temp);
                String str = String.valueOf(CurrencyType.getNamedItem("CurrencyType"));
                String[] arrOfStr = str.split("=", 2);
                //
                NamedNodeMap ValueUOMTipoProduto = findAttributesInNodes("PriceDetails", "Value", temp);
                String str2 = String.valueOf(ValueUOMTipoProduto.getNamedItem("UOM"));
                String[] arrOfStr2 = str2.split("=", 2);
                String CurrencyValueTax = findElementsInNodes("MonetaryAdjustmentStartAmount", "CurrencyValue", temp);
                String TaxPercent = eElement.getElementsByTagName("TaxPercent").item(0).getTextContent();
                String TaxAmount = findElementsInNodes("TaxAmount", "CurrencyValue", temp);
                String TaxLocation = eElement.getElementsByTagName("TaxLocation").item(0).getTextContent();
                String Quantity = findElementsInNodes("Quantity", "Value", temp);

                NamedNodeMap ValueUOMQuantity = findAttributesInNodes("Quantity", "Value", temp);
                String str3 = String.valueOf(ValueUOMQuantity.getNamedItem("UOM"));
                String[] arrOfStr3 = str3.split("=", 2);
                String CurrencyValueTotal = findElementsInNodes("LineBaseAmount", "CurrencyValue", temp);
                int teste = 0;
                try {
                    eElement.getElementsByTagName("InformationalQuantity").item(0).getTextContent();
                } catch (Exception e) {
                    teste = 1;
                    peso = 0;
                    tipoPeso = "";
                    quantVend = 0;
                    tipoQuantVend = "";
                }
                if (teste == 1) {
                } else {
                    //for (int i = 0; i < 2; i = i + 2) {
                    String InformationalQuantity = findElementsInNodes("InformationalQuantity", "Value", x);
                    NamedNodeMap ValueUOMInformationalQuantity = findAttributesInNodes("InformationalQuantity", "Value", x);
                    String str4 = String.valueOf(ValueUOMInformationalQuantity.getNamedItem("UOM"));
                    String[] arrOfStr4 = str4.split("=", 2);

                    tipoPeso = arrOfStr4[1].replace("\"", "");
                    x++;
                    if (null != arrOfStr4[1].replace("\"", "")) {
                        switch (arrOfStr4[1].replace("\"", "")) {
                            case "Kilogram":
                                //visto o anterior ser primeiro o peso então vamos buscar a quantidade vendida
                                peso = Float.parseFloat(InformationalQuantity);
                                //
                                //
                                //procurar quantidade vendida
                                String InformationalQuantity2 = findElementsInNodes("InformationalQuantity", "Value", x);
                                NamedNodeMap ValueUOMInformationalQuantity2 = findAttributesInNodes("InformationalQuantity", "Value", x);
                                String str5 = String.valueOf(ValueUOMInformationalQuantity2.getNamedItem("UOM"));
                                String[] arrOfStr5 = str5.split("=", 2);
                                //quantidade vendida
                                quantVend = Integer.parseInt(InformationalQuantity2);
                                tipoQuantVend = arrOfStr5[1].replace("\"", "");
                                x++;
                                break;
                            case "Sheet": {
                                //quantidade vendida
                                quantVend = Integer.parseInt(InformationalQuantity);
                                tipoQuantVend = arrOfStr4[1].replace("\"", "");
                                //
                                //
                                //calcular peso total quantidade vendida
                                float gram = pesquisarPeso(ProductIdentifierBuyer);
                                peso = (Float.parseFloat(InformationalQuantity) * gram) / 1000;
                                tipoPeso = "Kilogram";
                                break;
                            }
                            case "Box": {
                                //quantidade vendida
                                quantVend = Integer.parseInt(InformationalQuantity);
                                tipoQuantVend = arrOfStr4[1].replace("\"", "");
                                //
                                //
                                //calcular peso total quantidade vendida
                                if ("Sheet".equals(arrOfStr3[1].replace("\"", "")) || "Unit".equals(arrOfStr3[1].replace("\"", ""))) {
                                    float gram = pesquisarPeso(ProductIdentifierBuyer);
                                    peso = (Float.parseFloat(Quantity) * gram) / 1000;
                                    tipoPeso = "Kilogram";
                                }
                                break;
                            }
                            case "Unit": {
                                //quantidade vendida
                                quantVend = Integer.parseInt(InformationalQuantity);
                                tipoQuantVend = arrOfStr4[1].replace("\"", "");
                                //
                                //
                                //calcular peso total quantidade vendida
                                float gram = pesquisarPeso(ProductIdentifierBuyer);
                                peso = (Float.parseFloat(InformationalQuantity) * gram) / 1000;
                                tipoPeso = "Kilogram";
                                break;
                            }
                            case "Ream": {
                                //quantidade vendida
                                quantVend = Integer.parseInt(InformationalQuantity);
                                tipoQuantVend = arrOfStr4[1].replace("\"", "");
                                //
                                //
                                //calcular peso total quantidade vendida
                                if ("Sheet".equals(arrOfStr3[1].replace("\"", ""))) {
                                    float gram = pesquisarPeso(ProductIdentifierBuyer);
                                    peso = (Float.parseFloat(Quantity) * gram) / 1000;
                                    tipoPeso = "Kilogram";
                                }
                            }

                            default:
                                break;
                        }
                    }

                    //}
                }
                //verificar se o produto existe
                boolean validaProd = verifProd(ProductIdentifierBuyer);
                if (validaProd == false) {
                    produto prod = new produto();
                    prod.setCodProdPa(ProductIdentifierBuyer);
                    prod.setDescricao(ProductDescription);
                    prod.setQuantStock(0);
                    prod.setTipoQuantStock("");
                    prod.setQuantStockUnit(0);
                    prod.setTipoQuantStockUnit("");
                    prod.setPrecoUnitSIVA(Float.parseFloat("0"));
                    prod.setIdCategoria(0);
                    prod.setEstado("ativo");
                    inserirProdutos(prod);
                }

                int validaProdEnt = 0;
                List<entradasProdutos> entradaProduto = validaProdutoEntrada(OrderConfirmationReference, ProductIdentifierBuyer);
                for (entradasProdutos entProds : entradaProduto) {
                    validaProdEnt = 1;
                    entradasProdutos encProd = new entradasProdutos();
                    encProd.setPrecoTotalSIVA(Float.parseFloat(CurrencyValueTax) + entProds.getPrecoTotalSIVA());
                    encProd.setTotalIVA(Float.parseFloat(TaxAmount) + entProds.getTotalIVA());
                    encProd.setQuantVendida(Integer.parseInt(Quantity) + entProds.getQuantVendida());
                    encProd.setPesoTotalVenda(peso + entProds.getPesoTotalVenda());
                    encProd.setQuantTotalVendaUni(quantVend + entProds.getQuantTotalVendaUni());
                    encProd.setPrecoTotalCIVA(Float.parseFloat(CurrencyValueTotal) + entProds.getPrecoTotalCIVA());
                    atualizarEntradasProdutos(encProd, OrderConfirmationReference, ProductIdentifierBuyer);
                }

                if (validaProdEnt == 0) {
                    entradasProdutos encProd = new entradasProdutos();
                    encProd.setIdEntrada(OrderConfirmationReference);
                    encProd.setCodProdPa(ProductIdentifierBuyer);
                    encProd.setPrecoUnitSIVA(Float.parseFloat(CurrencyValueUnit));
                    encProd.setTipoPrecos(arrOfStr[1].replace("\"", ""));
                    encProd.setPrecoTotalSIVA(Float.parseFloat(CurrencyValueTax));
                    encProd.setIva(Integer.parseInt(TaxPercent));
                    encProd.setTotalIVA(Float.parseFloat(TaxAmount));
                    encProd.setLocalTax(TaxLocation);
                    encProd.setQuantVendida(Integer.parseInt(Quantity));
                    encProd.setTipoVenda(arrOfStr3[1].replace("\"", ""));
                    encProd.setPesoTotalVenda(peso);
                    encProd.setTipoPeso(tipoPeso);
                    encProd.setQuantTotalVendaUni(quantVend);
                    encProd.setTipoVendaUni(tipoQuantVend);
                    encProd.setPrecoTotalCIVA(Float.parseFloat(CurrencyValueTotal));
                    encProd.setEstado("pendente");
                    inserirEntradasProdutos(encProd);
                }

                a.setAlertType(Alert.AlertType.INFORMATION);
                a.setTitle("Entrada introduzida!");
                a.setHeaderText("Entrada introduzida com sucesso!");
                a.setContentText("A entrada escolhida para upload foi registada com sucesso.");
                a.show();
                ProductIdentifierBuyer = "";
            }
        }
    }

    /**
     * Metodo que vai buscar todos os items do ficheiro JSON
     *
     * @param jsonFileData recebe a informação do ficheiro JSON
     */
    private void getItemsFromJSON(JSONArray jsonFileData) {
        int temp;
        int x = 0;
        float peso = 0;
        String tipoPeso = "";
        int quantVend = 0;
        String tipoQuantVend = "";
        String codeSu = null;
        String codeBu = null;
        for (Object o : jsonFileData) {
            JSONObject orderConfirmation = (JSONObject) o;

            //dados da entrada
            JSONObject orderConfirmationObject = (JSONObject) orderConfirmation.get("OrderConfirmation");
            JSONArray orderConfirmationLineItem = (JSONArray) orderConfirmationObject.get("OrderConfirmationLineItem");

            //ler produtos
            Iterator<JSONObject> iterator = orderConfirmationLineItem.iterator();
            while (iterator.hasNext()) {
                JSONObject orderConfirmationLineItemData = (JSONObject) iterator.next();
                Long orderConfirmationLineItemNumber = (Long) orderConfirmationLineItemData.get("OrderConfirmationLineItemNumber");

                //Product
                JSONObject product = (JSONObject) orderConfirmationLineItemData.get("Product");
                //ProductIdentifier
                JSONArray productIdentifier = (JSONArray) product.get("ProductIdentifier");
                Iterator<JSONObject> iterator2 = productIdentifier.iterator();
                while (iterator2.hasNext()) {

                    JSONObject ProductIdentifierData = (JSONObject) iterator2.next();
                    String agency = (String) ProductIdentifierData.get("Agency");
                    String productIdentifierType = (String) ProductIdentifierData.get("ProductIdentifierType");

                    if ("Buyer".equals(agency)) {
                        codeSu = (String) ProductIdentifierData.get("Code");
                    } else if ("Supplier".equals(agency)) {
                        codeSu = (String) ProductIdentifierData.get("Code");
                        List<produtoFornecedor> prodForn = procurarCodProdPa(codeSu);
                        for (produtoFornecedor produtosFornecedores : prodForn) {
                            codeBu = produtosFornecedores.getCodProdPa();
                        }
                        if ("".equals(codeBu) || codeBu == null) {
                            Random gerador = new Random();
                            Long c = gerador.nextLong();
                            while (c < 999999999) {
                                c = gerador.nextLong();
                            }
                            codeBu = String.valueOf(c);
                        }
                    }

                }
                //end ProductIdentifier
                String productDescription = (String) product.get("ProductDescription");
                //end Product
                //
                //PriceDetails
                //PricePerUnit
                //CurrencyValue
                JSONObject priceDetails = (JSONObject) orderConfirmationLineItemData.get("PriceDetails");
                JSONObject pricePerUnit = (JSONObject) priceDetails.get("PricePerUnit");
                JSONObject currencyValueUnit = (JSONObject) pricePerUnit.get("CurrencyValue");
                String currencyTypeUnit = (String) currencyValueUnit.get("CurrencyType");
                Double valueUnit = (Double) currencyValueUnit.get("Value");
                //end CurrencyValue
                JSONObject valueFornType = (JSONObject) pricePerUnit.get("Value");
                String UOM = (String) valueFornType.get("UOM");
                //end PricePerUnit
                //end PriceDetails
                //
                //
                //MonetaryAdjustment
                JSONObject monetaryAdjustment = (JSONObject) orderConfirmationLineItemData.get("MonetaryAdjustment");
                String adjustmentType = (String) monetaryAdjustment.get("AdjustmentType");
                //MonetaryAdjustmentStartAmount
                JSONObject monetaryAdjustmentStartAmount = (JSONObject) monetaryAdjustment.get("MonetaryAdjustmentStartAmount");
                //CurrencyValue
                JSONObject currencyValueSIVA = (JSONObject) monetaryAdjustmentStartAmount.get("CurrencyValue");
                Double valueSIVA = (Double) currencyValueSIVA.get("Value");
                //end CurrencyValue
                //end MonetaryAdjustmentStartAmount
                //
                //TaxAdjustment
                JSONObject taxAdjustment = (JSONObject) monetaryAdjustment.get("TaxAdjustment");
                String taxType = (String) taxAdjustment.get("TaxType");
                Long taxPercent = (Long) taxAdjustment.get("TaxPercent");
                //TaxAmount
                JSONObject taxAmount = (JSONObject) taxAdjustment.get("TaxAmount");
                //CurrencyValue
                JSONObject currencyValueIVA = (JSONObject) taxAmount.get("CurrencyValue");
                Double valueIVA = (Double) currencyValueIVA.get("Value");
                //end CurrencyValue
                //end TaxAmount
                String taxLocation = (String) taxAdjustment.get("TaxLocation");

                //end TaxAdjustment
                //end MonetaryAdjustment
                //
                //
                //Quantity
                JSONObject quantity = (JSONObject) orderConfirmationLineItemData.get("Quantity");
                //Value
                JSONObject valueQuantity = (JSONObject) quantity.get("Value");
                String UOMQuantity = (String) valueQuantity.get("UOM");
                Long valorQuantidade = (Long) valueQuantity.get("Value");
                //end Value
                //end Quantity
                //
                //
                //InformationalQuantity
                int teste = 0;
                try {
                    orderConfirmationLineItemData.get("InformationalQuantity");
                } catch (Exception e) {
                    teste = 1;
                    peso = 0;
                    tipoPeso = "";
                    quantVend = 0;
                    tipoQuantVend = "";
                }
                if (teste == 1) {
                } else {
                    int val = 0;
                    int val1 = 0;
                    Long valorInformationalQuantidadePeso2 = null;

                    JSONArray InformationalQuantity = (JSONArray) orderConfirmationLineItemData.get("InformationalQuantity");
                    Iterator<JSONObject> iterator3 = InformationalQuantity.iterator();
                    while (iterator3.hasNext()) {
                        JSONObject InformationalQuantityData = (JSONObject) iterator3.next();
                        //Value
                        JSONObject valueInformationalQuantity = (JSONObject) InformationalQuantityData.get("Value");
                        String UOMInformationalQuantity = (String) valueInformationalQuantity.get("UOM");
                        if ("Kilogram".equals(UOMInformationalQuantity)) {
                            try {
                                Long valorInformationalQuantidadePeso = (Long) valueInformationalQuantity.get("Value");
                                peso = valorInformationalQuantidadePeso;
                            } catch (Exception e) {
                                Double valorInformationalQuantidadePeso = (Double) valueInformationalQuantity.get("Value");
                                peso = (float) valorInformationalQuantidadePeso.doubleValue();
                            }
                            tipoPeso = "Kilogram";
                            val = 1;
                        } else if ("Sheet".equals(UOMInformationalQuantity)) {
                            valorInformationalQuantidadePeso2 = (Long) valueInformationalQuantity.get("Value");
                            quantVend = (int) valorInformationalQuantidadePeso2.doubleValue();
                            tipoQuantVend = "Sheet";
                            if (val == 0) {
                                //calcular peso total quantidade vendida
                                float gram = pesquisarPeso(codeBu);
                                peso = (quantVend * gram) / 1000;
                                tipoPeso = "Kilogram";
                            }
                        } else if ("Box".equals(UOMInformationalQuantity)) {
                            valorInformationalQuantidadePeso2 = (Long) valueInformationalQuantity.get("Value");
                            quantVend = (int) valorInformationalQuantidadePeso2.doubleValue();
                            tipoQuantVend = "Box";
                            if (val == 0) {
                                if ("Sheet".equals(UOMQuantity) || "Unit".equals(UOMQuantity)) {
                                    //calcular peso total quantidade vendida
                                    float gram = pesquisarPeso(codeBu);
                                    peso = (valorQuantidade * gram) / 1000;
                                    tipoPeso = "Kilogram";
                                }
                            }
                        } else if ("Unit".equals(UOMInformationalQuantity)) {
                            valorInformationalQuantidadePeso2 = (Long) valueInformationalQuantity.get("Value");
                            quantVend = (int) valorInformationalQuantidadePeso2.doubleValue();
                            tipoQuantVend = "Unit";
                            if (val == 0) {
                                //calcular peso total quantidade vendida
                                float gram = pesquisarPeso(codeBu);
                                peso = (quantVend * gram) / 1000;
                                tipoPeso = "Kilogram";
                            }
                        } else if ("Ream".equals(UOMInformationalQuantity)) {
                            valorInformationalQuantidadePeso2 = (Long) valueInformationalQuantity.get("Value");
                            quantVend = (int) valorInformationalQuantidadePeso2.doubleValue();
                            tipoQuantVend = "Box";
                            if (val == 0) {
                                if ("Sheet".equals(UOMQuantity)) {
                                    //calcular peso total quantidade vendida
                                    float gram = pesquisarPeso(codeBu);
                                    peso = (valorQuantidade * gram) / 1000;
                                    tipoPeso = "Kilogram";
                                }
                            }
                        }

                        //end Value
                    }
                }
                //end InformationalQuantity
                //
                //
                //LineBaseAmount
                JSONObject lineBaseAmount = (JSONObject) orderConfirmationLineItemData.get("LineBaseAmount");
                JSONObject CurrencyValueLineBaseAmount = (JSONObject) lineBaseAmount.get("CurrencyValue");
                Double valueTotalCIVA = (Double) CurrencyValueLineBaseAmount.get("Value");

                //verificar se o produto existe
                boolean validaProd = verifProd(codeBu);
                if (validaProd == false) {
                    produto prod = new produto();
                    prod.setCodProdPa(codeBu);
                    prod.setDescricao(productDescription);
                    prod.setQuantStock(0);
                    prod.setTipoQuantStock("");
                    prod.setQuantStockUnit(0);
                    prod.setTipoQuantStockUnit("");
                    prod.setPrecoUnitSIVA(0.0f);
                    prod.setIdCategoria(0);
                    prod.setEstado("ativo");
                    inserirProdutos(prod);

                }

                int validaProdEnc = 0;
                List<entradasProdutos> entradaProduto = validaProdutoEntrada(OrderConfirmationReference, codeBu);
                for (entradasProdutos encProds : entradaProduto) {
                    validaProdEnc = 1;
                    entradasProdutos encProd = new entradasProdutos();
                    encProd.setPrecoTotalSIVA(valueSIVA.floatValue() + encProds.getPrecoTotalSIVA());
                    encProd.setTotalIVA(valueIVA.floatValue() + encProds.getTotalIVA());
                    encProd.setQuantVendida(valorQuantidade.intValue() + encProds.getQuantVendida());
                    encProd.setPesoTotalVenda(peso + encProds.getPesoTotalVenda());
                    encProd.setQuantTotalVendaUni(quantVend + encProds.getQuantTotalVendaUni());
                    encProd.setPrecoTotalCIVA(valueTotalCIVA.floatValue() + encProds.getPrecoTotalCIVA());
                    atualizarEntradasProdutos(encProd, OrderConfirmationReference, codeBu);
                }

                if (validaProdEnc == 0) {
                    entradasProdutos encProd = new entradasProdutos();
                    encProd.setIdEntrada(OrderConfirmationReference);
                    encProd.setCodProdPa(codeBu);
                    encProd.setPrecoUnitSIVA(valueUnit.floatValue());
                    encProd.setTipoPrecos(currencyTypeUnit);
                    encProd.setPrecoTotalSIVA(valueSIVA.floatValue());
                    encProd.setIva(taxPercent.intValue());
                    encProd.setTotalIVA(valueIVA.floatValue());
                    encProd.setLocalTax(taxLocation);
                    encProd.setQuantVendida(valorQuantidade.intValue());
                    encProd.setTipoVenda(UOMQuantity);
                    encProd.setPesoTotalVenda(peso);
                    encProd.setTipoPeso(tipoPeso);//
                    encProd.setQuantTotalVendaUni(quantVend);//
                    encProd.setTipoVendaUni(tipoQuantVend);
                    encProd.setPrecoTotalCIVA(valueTotalCIVA.floatValue());
                    encProd.setEstado("pendente");
                    inserirEntradasProdutos(encProd);

                }

                a.setAlertType(Alert.AlertType.INFORMATION);
                a.setTitle("Entrada introduzida!");
                a.setHeaderText("Entrada introduzida com sucesso!");
                a.setContentText("A entrada escolhida para upload foi registada com sucesso.");
                a.show();
                codeBu = "";
            }
        }
    }

    /**
     * Metodo para encontrar os elementos nos Nodes
     *
     * @param tagName recebe a name tag a q o elemento esta associado
     * @param elementName nome do elemento
     * @param localizacao localizacao do elemento na node list
     * @return dados
     */
    private static String findElementsInNodes(String tagName, String elementName, int localizacao) {
        String dados = "";
        try {
            File fXmlFile = new File(nomeFicheiro);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);
            doc.getDocumentElement().normalize();
            NodeList nList = doc.getElementsByTagName(tagName);

            Node node = nList.item(localizacao);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) node;
                dados = eElement.getElementsByTagName(elementName).item(0).getTextContent();
            }

        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(uploadFicheiroFornecedorController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return dados;
    }

    /**
     * Metodo para encontrar os atributos nos Nodes
     *
     * @param tagName recebe a name tag a q o elemento esta associado
     * @param elementName nome do elemento
     * @param localizacao localizacao do elemento na node list
     * @return dados
     */
    private static NamedNodeMap findAttributesInNodes(String tagName, String elementName, int localizacao) {
        NamedNodeMap dados = null;
        try {
            File fXmlFile = new File(nomeFicheiro);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);
            doc.getDocumentElement().normalize();
            NodeList nList = doc.getElementsByTagName(tagName);

            Node node = nList.item(localizacao);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) node;
                dados = eElement.getElementsByTagName(elementName).item(0).getAttributes();
            }

        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(uploadFicheiroFornecedorController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return dados;
    }

    /**
     * Método para encerrar a aplicação
     */
    @FXML
    private void sair(ActionEvent event) {
        System.exit(0);
    }

    /**
     * fechar
     */
    public void fecha() {
        uploadFicheiroFornecedor.getStage().close();
    }

    /**
     * Método para aceder ao menu do fornecedor
     */
    @FXML
    private void openMenu(ActionEvent event) {
        try {
            menuFornecedor menuF = new menuFornecedor();
            fecha();
            menuF.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(uploadFicheiroFornecedorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * método para substituir o conteudo do menu de opções para colocar as
     * opções por ordem caso seja o fornecedor a abrir o ecrã
     */
    private void adicionarEcraPessoalFornecedorMenuOpcoes() {
        List<utilizador> fornecedores = procurarEmail(login.getIdLogin());
        for (utilizador forncedor : fornecedores) {
            if (forncedor.getNivel() == 0) {
                opcoes.getItems().clear();
                MenuItem ePOp = new MenuItem("Ecrã Pessoal");
                MenuItem terminarSessao = new MenuItem("Terminar Sessão");
                MenuItem sair = new MenuItem("Sair");
                opcoes.getItems().add(ePOp);
                opcoes.getItems().add(terminarSessao);
                opcoes.getItems().add(sair);
                ePOp.setOnAction((ActionEvent e) -> {
                    try {
                        ecraPessoalFornecedor eF = new ecraPessoalFornecedor();
                        fecha();
                        eF.start(new Stage());
                    } catch (Exception ex) {
                        Logger.getLogger(uploadFicheiroFornecedorController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
                terminarSessao.setOnAction((ActionEvent e) -> {
                    try {
                        login.setIdLogin(0);
                        login.setNivelLogin(10000);
                        login log = new login();
                        fecha();
                        log.start(new Stage());
                    } catch (Exception ex) {
                        Logger.getLogger(uploadFicheiroFornecedorController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
                sair.setOnAction((ActionEvent e) -> {
                    System.exit(0);
                });
            }
        }
    }
}
