/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.service;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ruben
 */
public class OrderFRTest {

    /**
     *
     */
    public OrderFRTest() {
    }

    /**
     * Test of precoTotalCIVA method, of class OrderFR.
     */
    @Test
    public void testPrecoTotalSIVA() {
        System.out.println("testPrecoTotalSIVA");

        OrderFR ofr = new OrderFR();

        long quantVe = 100;
        double precoUnitSIVA = 5;
        double taxaDesc = 20;

        double expResult = 500;
        double result = Math.round(ofr.precoTotalSIVA(quantVe, precoUnitSIVA, taxaDesc) * 100.0) / 100.0;
        assertEquals(expResult, result, 0.0);

    }

    /**
     * Test of precoTotalSIVA method, of class OrderFR.
     */
    @Test
    public void testPrecoTotalCIVA() {
        System.out.println("testPrecoTotalCIVA");

        OrderFR ofr = new OrderFR();

        long quantVe = 100;
        double precoUnitSIVA = 5;
        double iva = 23;
        double taxaDesc = 20;

        double expResult = 615;
        double result = Math.round(ofr.precoTotalCIVA(quantVe, precoUnitSIVA, iva, taxaDesc) * 100.0) / 100.0;
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of totalIVA method, of class OrderFR.
     */
    @Test
    public void testTotalIVA() {
        System.out.println("totalIVA");

        OrderFR ofr = new OrderFR();

        long quantVe = 100;
        double precoUnitSIVA = 5;
        double iva = 23;
        double taxaDesc = 20;

        double expResult = 115;
        double result = Math.round(ofr.totalIVA(quantVe, precoUnitSIVA, iva, taxaDesc) * 100.0) / 100.0;
        assertEquals(expResult, result, 0.0);
    }

}
