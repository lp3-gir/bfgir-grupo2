/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ruben
 */
@Entity
@Table(name = "categoria")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Categoria.findAll", query = "SELECT c FROM Categoria c")
    , @NamedQuery(name = "Categoria.findByIdCategoria", query = "SELECT c FROM Categoria c WHERE c.idCategoria = :idCategoria")
    , @NamedQuery(name = "Categoria.findByNomeCategoria", query = "SELECT c FROM Categoria c WHERE c.nomeCategoria = :nomeCategoria")
    , @NamedQuery(name = "Categoria.findByDescricaoCategoria", query = "SELECT c FROM Categoria c WHERE c.descricaoCategoria = :descricaoCategoria")
    , @NamedQuery(name = "Categoria.findByNivelCategoria", query = "SELECT c FROM Categoria c WHERE c.nivelCategoria = :nivelCategoria")
    , @NamedQuery(name = "Categoria.findByEstado", query = "SELECT c FROM Categoria c WHERE c.estado = :estado")})
public class Categoria implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idCategoria")
    private Integer idCategoria;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "nomeCategoria")
    private String nomeCategoria;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "descricaoCategoria")
    private String descricaoCategoria;
    @Basic(optional = false)
    @NotNull
    @Column(name = "nivelCategoria")
    private int nivelCategoria;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "estado")
    private String estado;

    /**
     *
     */
    public Categoria() {
    }

    /**
     *
     * @param idCategoria
     */
    public Categoria(Integer idCategoria) {
        this.idCategoria = idCategoria;
    }

    /**
     *
     * @param idCategoria
     * @param nomeCategoria
     * @param descricaoCategoria
     * @param nivelCategoria
     * @param estado
     */
    public Categoria(Integer idCategoria, String nomeCategoria, String descricaoCategoria, int nivelCategoria, String estado) {
        this.idCategoria = idCategoria;
        this.nomeCategoria = nomeCategoria;
        this.descricaoCategoria = descricaoCategoria;
        this.nivelCategoria = nivelCategoria;
        this.estado = estado;
    }

    /**
     *
     * @return idCategoria
     */
    public Integer getIdCategoria() {
        return idCategoria;
    }

    /**
     *
     * @param idCategoria
     */
    public void setIdCategoria(Integer idCategoria) {
        this.idCategoria = idCategoria;
    }

    /**
     *
     * @return nomeCategoria
     */
    public String getNomeCategoria() {
        return nomeCategoria;
    }

    /**
     *
     * @param nomeCategoria
     */
    public void setNomeCategoria(String nomeCategoria) {
        this.nomeCategoria = nomeCategoria;
    }

    /**
     *
     * @return descricaoCategoria
     */
    public String getDescricaoCategoria() {
        return descricaoCategoria;
    }

    /**
     *
     * @param descricaoCategoria
     */
    public void setDescricaoCategoria(String descricaoCategoria) {
        this.descricaoCategoria = descricaoCategoria;
    }

    /**
     *
     * @return nivelCategoria
     */
    public int getNivelCategoria() {
        return nivelCategoria;
    }

    /**
     *
     * @param nivelCategoria
     */
    public void setNivelCategoria(int nivelCategoria) {
        this.nivelCategoria = nivelCategoria;
    }

    /**
     *
     * @return estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     *
     * @param estado
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }
}
