/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ruben
 */
public class EntradaTest {



    /**
     * Test of getIdEntrada method, of class Entrada.
     */
    @Test
    public void testGetIdEntrada() {
        System.out.println("getIdEntrada");
        Entrada instance = new Entrada();
        instance.setIdEntrada("R132");
        assertEquals("R132", instance.getIdEntrada());

    }

    /**
     * Test of setIdEntrada method, of class Entrada.
     */
    @Test
    public void testSetIdEntrada() {
        System.out.println("setIdEntrada");
        Entrada instance = new Entrada();
        instance.setIdEntrada("R123");

    }

    /**
     * Test of getIdFornecedor method, of class Entrada.
     */
    @Test
    public void testGetIdFornecedor() {
        System.out.println("getIdFornecedor");
        Entrada instance = new Entrada();
        instance.setIdFornecedor("F987");
        assertEquals("F987", instance.getIdFornecedor());

    }

    /**
     * Test of setIdFornecedor method, of class Entrada.
     */
    @Test
    public void testSetIdFornecedor() {
        System.out.println("setIdFornecedor");
        Entrada instance = new Entrada();
        instance.setIdFornecedor("F987");

    }

    /**
     * Test of getDataEntrada method, of class Entrada.
     */
    @Test
    public void testGetDataEntrada() {
        System.out.println("getDataEntrada");
        Entrada instance = new Entrada();
        instance.setDataEntrada("11/10/2021");
        assertEquals("11/10/2021", instance.getDataEntrada());

    }

    /**
     * Test of setDataEntrada method, of class Entrada.
     */
    @Test
    public void testSetDataEntrada() {
        System.out.println("setDataEntrada");
        Entrada instance = new Entrada();
        instance.setDataEntrada("11/10/2021");

    }

    /**
     * Test of getEstado method, of class Entrada.
     */
    @Test
    public void testGetEstado() {
        System.out.println("getEstado");
        Entrada instance = new Entrada();   
        instance.setEstado("ativo");
        assertEquals("ativo", instance.getEstado());

    }

    /**
     * Test of setEstado method, of class Entrada.
     */
    @Test
    public void testSetEstado() {
        System.out.println("setEstado");
        Entrada instance = new Entrada();
        instance.setEstado("ativo");

    }

}
