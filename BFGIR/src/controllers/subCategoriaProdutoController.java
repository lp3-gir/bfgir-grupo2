package controllers;

import bfgir.associarSubCategoria;
import bfgir.login;
import bfgir.menuAdministrador;
import bfgir.menuOperador;
import classes.categoria;
import static classes.categoria.listarCategoriasPorNivel;
import static classes.categoria.procurarCategoriaNome;
import classes.helper;
import classes.produto;
import static classes.produto.getListaProdutos;
import static classes.produto.pesquisarProdutoID;
import classes.produtoSubCategoria;
import static classes.produtoSubCategoria.associarProdutos;
import static classes.produtoSubCategoria.getSameRecordProdSubCat;
import static controllers.alterarPrecoProdutoClienteController.mensagemErroFatal;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class subCategoriaProdutoController implements Initializable {

    static Alert a = new Alert(Alert.AlertType.NONE);
    static Alert as = new Alert(Alert.AlertType.NONE);
    @FXML
    private Pane painelDescontos;
    private int altura_vbox = 0;

    @FXML
    private VBox vBoxProdutos;

    @FXML
    private ScrollPane produtosCliente;

    @FXML
    private Menu opcoes;

    @FXML
    private MenuBar menu;

    @FXML
    private TextField codClienteProd;

    @FXML
    private Button CodProd;

    private TextField quantVendida;

    @FXML
    private Button guardaValor;

    private TextField codDesconto;
    String ids = "";
    String idProduto = "";
    int idDesconto = 0;
    float precoProd = 0;
    float desconto = 0;
    private VBox vBoxDescontos;
    @FXML
    private ChoiceBox<String> choiceNivel;
    @FXML
    private ChoiceBox<String> choiceNivel2;
    @FXML
    private ChoiceBox<String> choiceNivel3;
    int nivel1 = 0;
    int nivel2 = 0;
    int nivel3 = 0;
    @FXML
    private Text lbN1;
    @FXML
    private Text lbN2;
    @FXML
    private Text lbN3;
    @FXML
    private Button cleanScreen;

    /**
     * Initializes the controller class.
     *
     * Faz a listagem de todos os produtos e é criado um botão com o codigo do
     * produto mais a sua descrição o botão de fazer a pesquisa por desconto é
     * invisivel para impedir que o uitlizador pesquise um desconto sem ter
     * escolhido um produto. Depois de escolher um produto essa ferramenta de
     * pesquisa passa a visivel
     *
     * @param url url
     * @param rb rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        lbN1.setVisible(false);
        lbN2.setVisible(false);
        lbN3.setVisible(false);
        choiceNivel.setVisible(false);
        choiceNivel2.setVisible(false);
        choiceNivel3.setVisible(false);
        guardaValor.setVisible(false);

        listarTodasCategoriasBD();
        vBoxProdutos.getChildren().removeAll(vBoxProdutos.getChildren());

        List<produto> produtos = getListaProdutos();

        for (produto prod : produtos) {
            String id = prod.getCodProdPa();
            String codpr = prod.getCodProdPa();
            String nome = prod.getCodProdPa() + " - " + prod.getDescricao();
            Button botao = new Button(nome);
            botao.setId("botaoVbox");
            botao.setPrefHeight(30);
            botao.setPrefWidth(570);
            botao.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {
                    lbN1.setVisible(true);
                    lbN2.setVisible(true);
                    lbN3.setVisible(true);
                    choiceNivel.setVisible(true);
                    choiceNivel2.setVisible(true);
                    choiceNivel3.setVisible(true);
                    guardaValor.setVisible(true);
                    botao.setId("botaoVboxChangeCollour");
                    ids = id;

                }
            });
            vBoxProdutos.getChildren().addAll(botao);

        }

    }

    /**
     * Pesquisa produto por codigo
     *
     * Faz a listagem de todos os produtos que recebe atrávés da pesquisa
     * recebida realizada pelo utilizador e cria um botão com o codigo do
     * produto e a sua descrição
     *
     * @param codProds= variavel que é recebida do método de pesquisa e serve
     * para filtrar os produtos para o codigo recebido
     */
    public void listarProdutosPorPesquisa(String codProds) {
        vBoxProdutos.getChildren().removeAll(vBoxProdutos.getChildren());

        List<produto> produtos = pesquisarProdutoID(codProds);

        for (produto prod : produtos) {
            String id = codProds;
            String nome = prod.getCodProdPa() + " - " + prod.getDescricao();
            Button botao = new Button(nome);
            botao.setId("botaoVbox");
            botao.setPrefHeight(30);
            //botao.setPrefWidth(403);
            botao.setPrefWidth(vBoxProdutos.getPrefWidth());
            botao.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {

                    lbN1.setVisible(true);
                    lbN2.setVisible(true);
                    lbN3.setVisible(true);
                    choiceNivel.setVisible(true);
                    choiceNivel2.setVisible(true);
                    choiceNivel3.setVisible(true);
                    guardaValor.setVisible(true);
                    botao.setId("botaoVboxChangeCollour");
                    ids = id;

                }
            });
            vBoxProdutos.getChildren().addAll(botao);

        }

    }

    /**
     * método para fechar o ecrã dos subCategoriaproduto
     */
    private static void fecha() {
        associarSubCategoria.getStage().close();
    }

    /**
     * Este método permite o utilizador a terminar sessão
     */
    @FXML
    private void terminarSessao(ActionEvent event) {
        try {
            login.setIdLogin(0);
            login.setNivelLogin(10000);
            login log = new login();
            fecha();
            log.start(new Stage());

        } catch (Exception ex) {
            mensagemErroFatal();
            Logger.getLogger(alterarPrecoProdutoClienteController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Método para encerrar a aplicação
     */
    @FXML
    private void sair(ActionEvent event) {
        helper.opcaoSair(event);
    }

    /**
     * Método para aceder ao menu do operador
     */
    @FXML
    private void openMenu(ActionEvent event) {
        try {
            if (login.getNivelLogin() == 1) {
                menuOperador menuO = new menuOperador();
                fecha();
                menuO.start(new Stage());
            } else {
                menuAdministrador menuAdmin = new menuAdministrador();
                fecha();
                menuAdmin.start(new Stage());
            }
        } catch (Exception ex) {
            Logger.getLogger(alterarPrecoProdutoClienteController.class.getName()).log(Level.SEVERE, null, ex);
            mensagemErroFatal();
        }

    }

    /**
     * Este método faz ligação com o método de pesquisar o produto por codigo de
     * produto e envia o que recebe do campo codClienteProd
     *
     */
    @FXML
    private void pesquisarCodProd(ActionEvent event) {
        listarProdutosPorPesquisa(codClienteProd.getText());
    }

    /**
     * Envia os valores para a base de dados
     *
     * Este metodo recebe os valores dos outros de métodos e vai fazer a
     * inserção se possivel da associação na base de dados
     *
     *
     * Os dados são enviados e o utilizador recebe uma notiificação a dizer que
     * foi associado com sucesso
     *
     */
    @FXML
    private void guardarValor(ActionEvent event) {
        prenche();
        int j = 0;
        List<produtoSubCategoria> categorias = getSameRecordProdSubCat(ids, nivel1, nivel2, nivel3);
        for (produtoSubCategoria c : categorias) {
            j++;
        }

        if (j >= 1) {
            try {
                a.setAlertType(Alert.AlertType.WARNING);
                a.getButtonTypes().setAll(ButtonType.OK);
                a.setTitle("Produto duplicado");
                a.setHeaderText("Este produto já existe");
                a.show();
                event.consume();
                fecha();
                associarSubCategoria as = new associarSubCategoria();
                as.start(new Stage());
            } catch (Exception ex) {
                Logger.getLogger(subCategoriaProdutoController.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            try {
                if (choiceNivel.getValue() == null) {
                    a.setAlertType(Alert.AlertType.WARNING);
                    a.getButtonTypes().setAll(ButtonType.OK);
                    a.setTitle("Falta de dados");
                    a.setHeaderText("Preencha pelo menos a categoria de nível 1!");
                    a.show();
                } else {
                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                    alert.setTitle("Feira & Office ");
                    alert.setHeaderText("Deseja mesmo associar este produto a estes niveis de categoria");
                    ButtonType buttonTypeYES = new ButtonType("Sim");
                    ButtonType buttonTypeNO = new ButtonType("Não");
                    alert.getButtonTypes().setAll(buttonTypeYES, buttonTypeNO);
                    Optional<ButtonType> results = alert.showAndWait();
                    if (results.get() == buttonTypeYES) {
                        associarProdutos(ids, nivel1, nivel2, nivel3);
                        Alert alertsf = new Alert(Alert.AlertType.INFORMATION);
                        alertsf.setTitle("Feira & Office ");
                        alertsf.setHeaderText("Associação completa com sucesso");
                        alertsf.showAndWait();
                        event.consume();
                        fecha();
                        associarSubCategoria as = new associarSubCategoria();
                        as.start(new Stage());

                    } else {
                        event.consume();

                    }
                }
            } catch (Exception e) {
                try {
                    a.setAlertType(Alert.AlertType.WARNING);
                    a.getButtonTypes().setAll(ButtonType.OK);
                    a.setTitle("Erro ao associar");
                    a.setHeaderText("Não foi possivel realizar a associação");
                    a.show();
                    event.consume();
                    fecha();
                    associarSubCategoria as = new associarSubCategoria();
                    as.start(new Stage());
                } catch (Exception ex) {
                    Logger.getLogger(subCategoriaProdutoController.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }
    }

    /**
     * Metodo que é responsavel por prencher as 3 combox cada uma corresponde a
     * um nivel de suncategoria diferentes
     *
     */
    private void listarTodasCategoriasBD() {
        List<categoria> categorias = listarCategoriasPorNivel(1);
        for (categoria categoria : categorias) {
            int idCategoria = categoria.getIdCategoria();
            choiceNivel.getItems().add(categoria.getNomeCategoria());

        }

        List<categoria> categorias2 = listarCategoriasPorNivel(2);
        for (categoria categoria : categorias2) {
            int idCategoria = categoria.getIdCategoria();
            choiceNivel2.getItems().add(categoria.getNomeCategoria());

        }
        List<categoria> categorias3 = listarCategoriasPorNivel(3);
        for (categoria categoria : categorias3) {
            int idCategoria = categoria.getIdCategoria();
            choiceNivel3.getItems().add(categoria.getNomeCategoria());

        }

    }

    /**
     * 
     * Este metodo vai defenir em variaveis  varios niveis de categorias
     * 
     */
    public void prenche() {
        nivel1 = 0;
        nivel2 = 0;
        nivel3 = 0;
        List<categoria> categorias1 = procurarCategoriaNome(choiceNivel.getValue());
        for (categoria ct : categorias1) {
            nivel1 = ct.getIdCategoria();
        }

        List<categoria> categorias2 = procurarCategoriaNome(choiceNivel2.getValue());
        for (categoria ct : categorias2) {
            nivel2 = ct.getIdCategoria();
        }

        List<categoria> categorias3 = procurarCategoriaNome(choiceNivel3.getValue());
        for (categoria ct : categorias3) {
            nivel3 = ct.getIdCategoria();
        }

    }

    @FXML
    private void cleanScreen(ActionEvent event) {

        try {
            event.consume();
            fecha();
            associarSubCategoria as = new associarSubCategoria();
            as.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(subCategoriaProdutoController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
