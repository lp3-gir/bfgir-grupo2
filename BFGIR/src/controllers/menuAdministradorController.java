/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import bfgir.alterarCategorias;
import bfgir.alterarDescontoAssociadoProduto;
import bfgir.alterarFornecedores;
import bfgir.alterarOperadores;
import bfgir.alterarPrecoProdutoCliente;
import bfgir.alterarProdutos;
import bfgir.alterarSubCategoriasProduto;
import bfgir.aprovarClientes;
import bfgir.eliminarOperadores;
import bfgir.listarOperadores;
import bfgir.login;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import bfgir.aprovarEntradaStocks;
import bfgir.associarDescontoProduto;
import bfgir.associarPrecoProdutoCliente;
import bfgir.associarSubCategoria;
import bfgir.eliminarCategorias;
import bfgir.eliminarDescontos;
import bfgir.eliminarDescontosProdutos;
import bfgir.eliminarFornecedores;
import bfgir.inserirCategorias;
import bfgir.inserirDesconto;
import bfgir.inserirFornecedores;
import bfgir.inserirOperadores;
import bfgir.listarCategorias;
import bfgir.listarDescontos;
import bfgir.listarDescontosAssociadosProdutos;
import bfgir.listarFornecedores;
import bfgir.listarProdutoCliente;
import bfgir.listarStockProdutos;
import bfgir.menuAdministrador;
import bfgir.movimentos;
import bfgir.movimentosProduto;
import bfgir.removerClienteAssociado;

/**
 * FXML Controller class
 */
public class menuAdministradorController implements Initializable {

    Alert a = new Alert(Alert.AlertType.NONE);
    @FXML
    private Pane PaneComp;
    @FXML
    private Button eliminarOperadores;
    @FXML
    private Button listarOperadores;
    @FXML
    private Button alterarOperadores;
    @FXML
    private Button inserirOperadores;
    @FXML
    private Button eliminarFornecedor;
    @FXML
    private Button listarFornecedores;
    @FXML
    private Button inserirFornecedores;
    @FXML
    private Button listarStockProdutos;
    @FXML
    private Button consultarMovimentos;
    @FXML
    private Button aprovacaoEntradaStock;
    @FXML
    private Button alterar_Fornecedores;
    @FXML
    private Button consultarMovimentosProduto;
    @FXML
    private Button alterarProdutos;
    @FXML
    private Button alterarCategorias;
    @FXML
    private Button listarCategorias;
    @FXML
    private Button eliminarCategorias;
    @FXML
    private Button inserirCategorias;
    @FXML
    private Button removerPrecoCliente;
    @FXML
    private Button listarPreçoCliente;
    @FXML
    private Button alterarPrecoCliente;
    @FXML
    private Button associarPrecosClientes;
    @FXML
    private Button eliminarDescontos;
    @FXML
    private Button listarDescontos;
    @FXML
    private Button inserirDescontos;
    @FXML
    private Button eliminarDescontosAssociados;
    @FXML
    private Button listarDescontosAssociados;
    @FXML
    private Button alterarDescontosAssociados;
    @FXML
    private Button associarDescontos;
    @FXML
    private Button aprovarClientes;
    @FXML
    private Button alterarSubCatProdutos;
    @FXML
    private Button associarSubCatProd;
 

    /**
     * Initializes the controller class.
     *
     * @param url url
     * @param rb rb
     */
    @Override

    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    /**
     * Metodo para iniciar a pagina eliminarOperadores
     *
     * @param event evento
     */
    @FXML
    private void eliminarOperadores(ActionEvent event) {
        try {
            eliminarOperadores eo = new eliminarOperadores();
            fecha();
            eo.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuAdministradorController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Metodo para iniciar a pagina listarOperadores
     *
     * @param event evento
     */
    @FXML
    private void listarOperadores(ActionEvent event) {
        try {
            listarOperadores lo = new listarOperadores();
            fecha();
            lo.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuAdministradorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para iniciar a pagina alterarOperadores
     *
     * @param event evento
     */
    @FXML
    private void alterarOperadores(ActionEvent event) {
        alterarOperadores alto = new alterarOperadores();
        fecha();
        try {
            alto.start(new Stage());
        } catch (Exception ex) {
            a.setAlertType(Alert.AlertType.ERROR);
            a.setTitle("ERRO Fatal");
            a.setHeaderText("A aplicação está corrompida!");
            a.setContentText("Algumas funcionalidades do aplicação estão inacessíveis. Tente uma reinstalação.");
            a.show();
            System.out.println("Erro Exception ex " + ex.getMessage());
            Logger.getLogger(menuAdministradorController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Metodo para iniciar a pagina inserirOperadores
     *
     * @param event evento
     */
    @FXML
    private void inserirOperadores(ActionEvent event) {
        try {
            inserirOperadores insOpe = new inserirOperadores();
            fecha();
            insOpe.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuAdministradorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para terminar sessao do utilizador
     *
     * @param event evento
     */
    @FXML
    private void terminarSessao(ActionEvent event) {
        try {
            login.setIdLogin(0);
            login.setNivelLogin(10000);
            login log = new login();
            fecha();
            log.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuAdministradorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para iniciar a pagina eliminarFornecedor
     *
     * @param event evento
     */
    @FXML
    private void eliminarFornecedor(ActionEvent event) {
        try {
            eliminarFornecedores ef = new eliminarFornecedores();
            fecha();
            ef.start(new Stage());

        } catch (Exception ex) {
            Logger.getLogger(menuAdministradorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * método para fechar o ecrã do meno do administrador
     */
    private void fecha() {
        menuAdministrador.getStage().close();
    }

    /**
     * Metodo para iniciar a pagina listarFornecedores
     *
     * @param event evento
     */
    @FXML
    private void listarFornecedores(ActionEvent event) {
        try {
            listarFornecedores lf = new listarFornecedores();
            fecha();
            lf.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuAdministradorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para iniciar a pagina alterar_Fornecedores
     *
     * @param event evento
     */
    @FXML
    private void alterar_Fornecedores(ActionEvent event) {
        alterarFornecedores af = new alterarFornecedores();
        fecha();
        try {
            af.start(new Stage());
        } catch (Exception ex) {
            a.setAlertType(Alert.AlertType.ERROR);
            a.setTitle("ERRO Fatal");
            a.setHeaderText("A aplicação está corrompida!");
            a.setContentText("Algumas funcionalidades do aplicação estão inacessíveis. Tente uma reinstalação.");
            a.show();
            System.out.println("Erro Exception ex " + ex.getMessage());
            Logger.getLogger(menuAdministradorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para iniciar a pagina inserirFornecedores
     *
     * @param event evento
     */
    @FXML
    private void inserirFornecedores(ActionEvent event) {
        try {
            inserirFornecedores insFor = new inserirFornecedores();
            fecha();
            insFor.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuAdministradorController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para iniciar a pagina listarStockProdutos
     *
     * @param event evento
     */
    @FXML
    private void listarStockProdutos(ActionEvent event) {
        try {
            listarStockProdutos list = new listarStockProdutos();
            fecha();
            list.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuOperadorController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Metodo para iniciar a pagina consultarMovimentos
     *
     * @param event evento
     */
    @FXML
    private void consultarMovimentos(ActionEvent event) {
        try {
            movimentos mv = new movimentos();
            fecha();
            mv.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuAdministradorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para iniciar a pagina aprovacaoEntradaStock
     *
     * @param event evento
     */
    @FXML
    private void aprovacaoEntradaStock(ActionEvent event) {
        try {
            aprovarEntradaStocks sc = new aprovarEntradaStocks();
            fecha();
            sc.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuAdministradorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Método para encerrar a aplicação
     */
    @FXML
    private void sair(ActionEvent event) {
        System.exit(0);
    }

    /**
     * Metodo para iniciar a pagina consultarMovimentosProduto
     *
     * @param event evento
     */
    @FXML
    private void consultarMovimentosProduto(ActionEvent event) {
        try {
            movimentosProduto mvp = new movimentosProduto();
            fecha();
            mvp.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuAdministradorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para iniciar a pagina alterarProdutos
     *
     * @param event evento
     */
    @FXML
    private void alterarProdutos(ActionEvent event) {
        try {
            alterarProdutos altProd = new alterarProdutos();
            fecha();
            altProd.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuAdministradorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para iniciar a pagina alterarCategorias
     *
     * @param event evento
     */
    @FXML
    private void alterarCategorias(ActionEvent event) {
        try {
            alterarCategorias altCat = new alterarCategorias();
            fecha();
            altCat.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuOperadorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para iniciar a pagina listarCategorias
     *
     * @param event evento
     */
    @FXML
    private void listarCategorias(ActionEvent event) {
        try {
            listarCategorias listCat = new listarCategorias();
            fecha();
            listCat.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuAdministradorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para iniciar a pagina eliminarCategorias
     *
     * @param event evento
     */
    @FXML
    private void eliminarCategorias(ActionEvent event) {
        try {
            eliminarCategorias eliCat = new eliminarCategorias();
            fecha();
            eliCat.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuAdministradorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para iniciar a pagina inserirCategorias
     *
     * @param event evento
     */
    @FXML
    private void inserirCategorias(ActionEvent event) {
        try {
            inserirCategorias insCat = new inserirCategorias();
            fecha();
            insCat.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuAdministradorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para iniciar a pagina removerPrecoCliente
     *
     * @param event evento
     */
    @FXML
    private void removerPrecoCliente(ActionEvent event) {
        try {
            removerClienteAssociado remvCli = new removerClienteAssociado();
            fecha();
            remvCli.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuAdministradorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para iniciar a pagina listarPrecoCliente
     *
     * @param event evento
     */
    @FXML
    private void listarPrecoCliente(ActionEvent event) {
        try {
            listarProdutoCliente lc = new listarProdutoCliente();
            fecha();
            lc.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuAdministradorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para iniciar a pagina alterarPrecoCliente
     *
     * @param event evento
     */
    @FXML
    private void alterarPrecoCliente(ActionEvent event) {

        try {
            alterarPrecoProdutoCliente altPPC = new alterarPrecoProdutoCliente();
            fecha();
            altPPC.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuAdministradorController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Metodo para iniciar a pagina aprovarClientes
     *
     * @param event evento
     */
    @FXML
    private void aprovarClientes(ActionEvent event) {
        try {
            aprovarClientes AC = new aprovarClientes();
            fecha();
            AC.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuAdministradorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para iniciar a pagina associarPrecosClientes
     *
     * @param event evento
     */
    @FXML
    private void associarPrecosClientes(ActionEvent event) {
        try {
            associarPrecoProdutoCliente ap = new associarPrecoProdutoCliente();
            fecha();
            ap.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuAdministradorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para iniciar a pagina de eliminar descontos
     *
     * @param event evento
     */
    @FXML
    private void eliminarDescontos(ActionEvent event) {
        try {
            eliminarDescontos eliDes = new eliminarDescontos();
            fecha();
            eliDes.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuAdministradorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para iniciar a pagina de listar descontos
     *
     * @param event evento
     */
    @FXML
    private void listarDescontos(ActionEvent event) {
        try {
            listarDescontos ldes = new listarDescontos();
            fecha();
            ldes.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuAdministradorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para iniciar a pagina de inserir descontos
     *
     * @param event evento
     */
    @FXML
    private void inserirDescontos(ActionEvent event) {
        try {
            inserirDesconto insdes = new inserirDesconto();
            fecha();
            insdes.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuAdministradorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para iniciar a pagina de eliminar descontos associados a produtos
     *
     * @param event evento
     */
    @FXML
    private void eliminarDescontosAssociados(ActionEvent event) {
        try {
            eliminarDescontosProdutos ep = new eliminarDescontosProdutos();
            fecha();
            ep.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuOperadorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para iniciar a pagina de listra os descontos associados a vários
     * produtos
     *
     * @param event evento
     */
    @FXML
    private void listarDescontosAssociados(ActionEvent event) {
         try {
            listarDescontosAssociadosProdutos listPA = new listarDescontosAssociadosProdutos();
            fecha();
            listPA.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(listarDescontosAssociadosProdutosController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para iniciar a pagina de alterar descontos associados a produtos
     *
     * @param event evento
     */
    @FXML
    private void alterarDescontosAssociados(ActionEvent event) {
        try {
            alterarDescontoAssociadoProduto altDescP = new alterarDescontoAssociadoProduto();
            fecha();
            altDescP.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(alterarDescontoAssociadoProdutoController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para iniciar a pagina de associar descontos a produtos
     *
     * @param event evento
     */
    @FXML
    private void associarDescontos(ActionEvent event) {
        try {
            associarDescontoProduto ap = new associarDescontoProduto();
            fecha();
            ap.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuOperadorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para iniciar a pagina associarSubCatProd
     *
     * @param event evento
     */
    @FXML
    private void associarSubCatProd(ActionEvent event) {
        try {
            associarSubCategoria as = new associarSubCategoria();
            fecha();
            as.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuOperadorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para iniciar a pagina alterarSubCatProdutos
     *
     * @param event evento
     */
    @FXML
    private void alterarSubCatProdutos(ActionEvent event) {
        try {
            alterarSubCategoriasProduto altSCP = new alterarSubCategoriasProduto();
            fecha();
            altSCP.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuOperadorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
