/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ruben
 */
public class PesosTest {



    /**
     * Test of getCodPeso method, of class Pesos.
     */
    @Test
    public void testGetCodPeso() {
        System.out.println("getCodPeso");
        Pesos instance = new Pesos();
        instance.setCodPeso("P.123");
        assertEquals("P.123", instance.getCodPeso());
        
        
    }

    /**
     * Test of setCodPeso method, of class Pesos.
     */
    @Test
    public void testSetCodPeso() {
        System.out.println("setCodPeso");
        Pesos instance = new Pesos();
        instance.setCodPeso("P.123");
        
        
    }

    /**
     * Test of getPeso method, of class Pesos.
     */
    @Test
    public void testGetPeso() {
        System.out.println("getPeso");
        Pesos instance = new Pesos();
        instance.setPeso(10.5);
        assertEquals(10.5, instance.getPeso(), 0.0);
        
        
    }

    /**
     * Test of setPeso method, of class Pesos.
     */
    @Test
    public void testSetPeso() {
        System.out.println("setPeso");
        Pesos instance = new Pesos();
        instance.setPeso(1.5);
        
        
    }

    /**
     * Test of getTipoPeso method, of class Pesos.
     */
    @Test
    public void testGetTipoPeso() {
        System.out.println("getTipoPeso");
        Pesos instance = new Pesos();
        instance.setTipoPeso("Teste");
        assertEquals("Teste", instance.getTipoPeso());
        
        
    }

    /**
     * Test of setTipoPeso method, of class Pesos.
     */
    @Test
    public void testSetTipoPeso() {
        System.out.println("setTipoPeso");
        Pesos instance = new Pesos();
        instance.setTipoPeso("Teste");
        
        
    }
}
