/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Classe de testes da classe desconto.
 *
 * @author Ilídio Magalhães
 */
public class descontoTest {

    private static desconto dc = new desconto();

    /**
     * Construdor da classe descontoTest
     */
    public descontoTest() {
    }

    /**
     *
     * Método para estabelecer os atributos do objeto dc.
     */
    @Before
    public void setUp() {
        dc.setIdDesconto(0);
        dc.setQuantDesconto(0.0f);
    }

    /**
     * Teste ao método getIdDesconto da classe desconto.<br>
     * Método tenta buscar o id do objeto previamente criado na classe.<br>
     * O teste é válido caso o valor obtido seja igual ao valor esperado.
     */
    @Test
    public void testGetIdDesconto() {
        System.out.println("\nTeste ao método getIdDesconto");
        int expResult = 0;
        int result = dc.getIdDesconto();
        if (expResult == result) {
            System.out.println("Teste ao método getIdDesconto passado com um sucesso!\n");
        }
        assertEquals(expResult, result);
    }

    /**
     *
     * Teste ao método setIdDesconto da classe desconto.<br>
     * Método modifica o id do objeto dc e depois verifica se o consegiu
     * alterar.<br>
     * O teste é válido caso tal se verifique.<br>
     * No fim, restaura o atributo ao estado inicial.
     */
    @Test
    public void testSetIdDesconto() {
        System.out.println("\nTeste ao método setIdDesconto");
        int id = 1234567;
        int idBackup = dc.getIdDesconto();
        dc.setIdDesconto(id);
        boolean result = false;
        if (id == dc.getIdDesconto()) {
            result = true;
            System.out.println("Teste ao método setIdDesconto passado com um sucesso!\n");
        }
        assertTrue(result);
        dc.setIdDesconto(idBackup);
    }

    /**
     * Teste ao método getQuantDesconto da classe desconto.<br>
     * Método tenta buscar o desconto do objeto previamente criado na
     * classe.<br>
     * O teste é válido caso o valor obtido seja igual ao valor esperado.
     */
    @Test
    public void testGetQuantDesconto() {
        System.out.println("\nTeste ao método getQuantDesconto");
        float expResult = 0.0f;
        float result = dc.getQuantDesconto();
        if (expResult == result) {
            System.out.println("Teste ao método getQuantDesconto passado com um sucesso!\n");
        }
        assertEquals(expResult, result, 0.0f);
    }

    /**
     * Teste ao método setQuantDesconto da classe desconto.<br>
     * Método modifica o desconto do objeto dc e depois verifica se o
     * consegiu alterar.<br>
     * O teste é válido caso tal se verifique.<br>
     * No fim, restaura o atributo ao estado inicial.
     */
    @Test
    public void testSetQuantDesconto() {
        System.out.println("\nTeste ao método setQuantDesconto");
        float dI = 35.5f;
        float dF = dc.getQuantDesconto();
        dc.setQuantDesconto(dI);
        boolean result = false;
        if (dI == (dc.getQuantDesconto())) {
            result = true;
            System.out.println("Teste ao método setQuantDesconto passado com um sucesso!\n");
        }
        assertTrue(result);
        dc.setQuantDesconto(dF);
    }

}
