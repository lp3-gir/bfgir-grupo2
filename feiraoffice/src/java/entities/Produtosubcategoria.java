/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ruben
 */
@Entity
@Table(name = "produtosubcategoria")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Produtosubcategoria.findAll", query = "SELECT p FROM Produtosubcategoria p")
    , @NamedQuery(name = "Produtosubcategoria.findByIdProdSub", query = "SELECT p FROM Produtosubcategoria p WHERE p.idProdSub = :idProdSub")
    , @NamedQuery(name = "Produtosubcategoria.findByCodProdPa", query = "SELECT p FROM Produtosubcategoria p WHERE p.codProdPa = :codProdPa")
    , @NamedQuery(name = "Produtosubcategoria.findByIdCatNivel1", query = "SELECT p FROM Produtosubcategoria p WHERE p.idCatNivel1 = :idCatNivel1")
    , @NamedQuery(name = "Produtosubcategoria.findByIdCatNivel2", query = "SELECT p FROM Produtosubcategoria p WHERE p.idCatNivel2 = :idCatNivel2")
    , @NamedQuery(name = "Produtosubcategoria.findByIdCatNivel3", query = "SELECT p FROM Produtosubcategoria p WHERE p.idCatNivel3 = :idCatNivel3")})
public class Produtosubcategoria implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idProdSub")
    private Integer idProdSub;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "codProdPa")
    private String codProdPa;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idCatNivel1")
    private int idCatNivel1;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idCatNivel2")
    private int idCatNivel2;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idCatNivel3")
    private int idCatNivel3;

    /**
     *
     */
    public Produtosubcategoria() {
    }

    /**
     *
     * @param idProdSub recebe o idProdSub
     */
    public Produtosubcategoria(Integer idProdSub) {
        this.idProdSub = idProdSub;
    }

    /**
     *
     * @param idProdSub recebe o idProdSub
     * @param codProdPa recebe o código de um produto
     * @param idCatNivel1 recebe o id de nivel 1 de uma categoria
     * @param idCatNivel2 recebe o id de nivel 2 de uma categoria
     * @param idCatNivel3 recebe o id de nivel 3 de uma categoria
     */
    public Produtosubcategoria(Integer idProdSub, String codProdPa, int idCatNivel1, int idCatNivel2, int idCatNivel3) {
        this.idProdSub = idProdSub;
        this.codProdPa = codProdPa;
        this.idCatNivel1 = idCatNivel1;
        this.idCatNivel2 = idCatNivel2;
        this.idCatNivel3 = idCatNivel3;
    }

    /**
     *
     * @return idProdSub
     */
    public Integer getIdProdSub() {
        return idProdSub;
    }

    /**
     *
     * @param idProdSub
     */
    public void setIdProdSub(Integer idProdSub) {
        this.idProdSub = idProdSub;
    }

    /**
     *
     * @return codProdPa
     */
    public String getCodProdPa() {
        return codProdPa;
    }

    /**
     *
     * @param codProdPa
     */
    public void setCodProdPa(String codProdPa) {
        this.codProdPa = codProdPa;
    }

    /**
     *
     * @return idCatNivel1
     */
    public int getIdCatNivel1() {
        return idCatNivel1;
    }

    /**
     *
     * @param idCatNivel1
     */
    public void setIdCatNivel1(int idCatNivel1) {
        this.idCatNivel1 = idCatNivel1;
    }

    /**
     *
     * @return idCatNivel2
     */
    public int getIdCatNivel2() {
        return idCatNivel2;
    }

    /**
     *
     * @param idCatNivel2
     */
    public void setIdCatNivel2(int idCatNivel2) {
        this.idCatNivel2 = idCatNivel2;
    }

    /**
     *
     * @return idCatNivel3
     */
    public int getIdCatNivel3() {
        return idCatNivel3;
    }

    /**
     *
     * @param idCatNivel3
     */
    public void setIdCatNivel3(int idCatNivel3) {
        this.idCatNivel3 = idCatNivel3;
    }

}
