/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ruben
 */
public class DescontosprodutosPKTest {



    /**
     * Teste para retirar o id de um desconto.
     */
    @Test
    public void testGetIdDesconto() {
        System.out.println("getIdDesconto");
        DescontosprodutosPK instance = new DescontosprodutosPK();
        instance.setIdDesconto(1);
        int expResult = 1;
        assertEquals(expResult, instance.getIdDesconto());

    }

    /**
     * Teste para definir o id de um desconto.
     */
    @Test
    public void testSetIdDesconto() {
        System.out.println("setIdDesconto");
        DescontosprodutosPK instance = new DescontosprodutosPK();
        instance.setIdDesconto(1);

    }

    /**
     * Teste para definir o código de um produto.
     */
    @Test
    public void testGetCodProdPa() {
        System.out.println("getCodProdPa");
        DescontosprodutosPK instance = new DescontosprodutosPK();
        instance.setCodProdPa("T.123");
        assertEquals("T.123", instance.getCodProdPa());

    }

    /**
     * Teste para definir o código de um produto.
     */
    @Test
    public void testSetCodProdPa() {
        System.out.println("setCodProdPa");
        DescontosprodutosPK instance = new DescontosprodutosPK();
        instance.setCodProdPa("T.123");

    }

}
