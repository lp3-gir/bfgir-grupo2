/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import bfgir.ecraPessoalOperador;
import bfgir.login;
import bfgir.menuAdministrador;
import bfgir.menuOperador;
import bfgir.movimentosProduto;
import classes.entradas;
import static classes.entradas.listarEntradasId;
import classes.entradasProdutos;
import static classes.entradasProdutos.procurarTipoQuantidadeProduto;
import static classes.entradasProdutos.validaProdutoEntrada;
import classes.produto;
import static classes.produto.getListaProdutos;
import static classes.produto.pesquisarProdutoID;
import classes.utilizador;
import static classes.utilizador.procurarEmail;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 */
public class movimentosProdutoController implements Initializable {

    @FXML
    private Pane painelMovimentos;
    @FXML
    private VBox vBoxEntradas;
    @FXML
    private ScrollPane entradaStock;
    @FXML
    private VBox vBoxProdutos;
    @FXML
    private MenuBar menu;
    @FXML
    private TextField codProdutoPesquisar;
    @FXML
    private Button pesquisarCodProduto;
    @FXML
    private VBox vBoxDadosProdutos;

    private int altura_vbox = 0;
    private int altura_vbox2 = 0;
    private int altura_vbox3 = 0;

    @FXML
    private Menu opcoes;
    @FXML
    private ScrollPane entradas;

    /**
     * Método Intialize
     *
     * Este método vai listar todos os produtos
     * e para cada produto vai criar um botão com o seu respetivo ID
     *
     * @param url url
     * @param rb rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        adicionarEcraPessoalOperadorMenuOpcoes();

        List<produto> listaProdutos = getListaProdutos();
        for (produto prods : listaProdutos) {

            String id = prods.getCodProdPa();
            Button botao = new Button(id);
            botao.setId("botaoVbox");
            botao.setPrefHeight(30);
            botao.setPrefWidth(250);
            botao.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {

                    vBoxDadosProdutos.getChildren().removeAll(vBoxDadosProdutos.getChildren());
                     botao.setId("botaoVboxChangeCollour");
                    listarEntrada(id);

                }
            });
            altura_vbox = altura_vbox + 30;
            vBoxProdutos.getChildren().addAll(botao);
            if (altura_vbox > 330) {
                if (altura_vbox > 330) {
                    vBoxProdutos.setPrefWidth(250);
                }
                vBoxProdutos.setPrefHeight(altura_vbox);
            }

        }
    }

    /**
     * Metodo Listar Entrada
     *
     * Este método vai listar todas as encomendas associadas ao produto selecionado
     * e para cada encomenda vai criar um botão com o respetivo ID
     *
     * @param codProdPa CodProdPa
     */
    private void listarEntrada(String codProdPa) {
        altura_vbox2 = 0;
        vBoxEntradas.getChildren().removeAll(vBoxEntradas.getChildren());
        List<entradasProdutos> encProd = procurarTipoQuantidadeProduto(codProdPa);
        for (entradasProdutos enp : encProd) {
            List<entradas> entradas = listarEntradasId(enp.getIdEntrada());
            for (entradas ec : entradas) {

                String id = ec.getIdEntrada();
                Button botao = new Button(id + " - " + ec.getDataEntrada());
                botao.setId("botaoVbox");
                botao.setPrefHeight(30);
                botao.setPrefWidth(250);
                botao.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent e) {
                         botao.setId("botaoVboxChangeCollour");
                        listarInfoEncProd(id, codProdPa);

                    }
                });

                altura_vbox2 = altura_vbox2 + 30;
                vBoxEntradas.getChildren().addAll(botao);
                if (altura_vbox2 > 330) {
                    if (altura_vbox2 > 330) {
                        vBoxEntradas.setPrefWidth(250);
                    }
                    vBoxEntradas.setPrefHeight(altura_vbox2);
                }
            }
        }
    }

    /**
     * Metodo Listar Informação da entrada de produto
     *
     * Este metodo vai fazer a listagem organizada 
     * de todas as informações da encomenda 
     *
     * @param idEnt recebe o ID da entrada
     * @param codProd Codigo do Produto
     */
    private void listarInfoEncProd(String idEnt, String codProd) {
        altura_vbox3 = 0;
        vBoxDadosProdutos.getChildren().removeAll(vBoxDadosProdutos.getChildren());

        String tipoMoeda = "";
        float valorTotal = 0;

        List<entradasProdutos> entradaProdutos = validaProdutoEntrada(idEnt, codProd);
        for (entradasProdutos ep : entradaProdutos) {

            List<produto> produtos = pesquisarProdutoID(ep.getCodProdPa());
            for (produto prod : produtos) {

                valorTotal = valorTotal + ep.getPrecoTotalCIVA();
                String id = ep.getIdEntrada();
                String nome = prod.getDescricao();

                Label label1;
                Label label2;
                if (nome.length() > 25) {
                    label1 = new Label(ep.getCodProdPa() + " | " + nome.substring(0, 25) + " | " + ep.getQuantVendida() + " " + ep.getTipoVenda() + " | " + ep.getPrecoUnitSIVA() + " " + ep.getTipoPrecos() + "/" + ep.getTipoVenda() + " S.IVA " + " | " + " Total: " + ep.getPrecoTotalCIVA() + " " + ep.getTipoPrecos() + " C.IVA ");
                    tipoMoeda = ep.getTipoPrecos();
                } else {
                    label1 = new Label(ep.getCodProdPa() + " | " + nome.substring(0, nome.length()) + " | " + ep.getQuantVendida() + " " + ep.getTipoVenda() + " | " + ep.getPrecoUnitSIVA() + " " + ep.getTipoPrecos() + "/" + ep.getTipoVenda() + " S.IVA " + " | " + " Total: " + ep.getPrecoTotalCIVA() + " " + ep.getTipoPrecos() + " C.IVA ");
                    tipoMoeda = ep.getTipoPrecos();
                }

                label1.setId("labelProdutos");
                label1.setPrefHeight(30);
                label1.setPrefWidth(593);

                altura_vbox3 = altura_vbox3 + 30;
                vBoxDadosProdutos.getChildren().addAll(label1);
                if (altura_vbox3 > 240) {
                    if (altura_vbox3 > 240) {
                        vBoxDadosProdutos.setPrefWidth(580);
                    }
                    vBoxDadosProdutos.setPrefHeight(altura_vbox3);
                }
            }

        }
    }

    /**
     * Metodo para terminar sessao do utilizador
     *
     * @param event evento
     */
    @FXML
    private void terminarSessao(ActionEvent event) {
        try {
            login.setIdLogin(0);
            login.setNivelLogin(10000);
            login log = new login();
            fecha();
            log.start(new Stage());

        } catch (Exception ex) {
            Logger.getLogger(menuAdministradorController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Método para encerrar a aplicação
     */
    @FXML
    private void sair(ActionEvent event) {
        System.exit(0);
    }

    /**
     * Método para aceder ao menu do operador ou do administrador conforme o seu nivel de login
     */
    @FXML
    private void openMenu(ActionEvent event) {
        try {
            if (login.getNivelLogin() == 1) {
                menuOperador menuO = new menuOperador();
                fecha();
                menuO.start(new Stage());
            } else {
                menuAdministrador menuAdmin = new menuAdministrador();
                fecha();
                menuAdmin.start(new Stage());
            }
        } catch (Exception ex) {
            Logger.getLogger(eliminarFornecedorController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * método para fechar o ecrã de movimentos do produto
     */
    private void fecha() {
        movimentosProduto.getStage().close();
    }

    /**
     * Meodo Pesquisa codigo do produto
     *
     * Neste metodo vai filtrar e listar os produtos procuradas pelo tipo de quantidade
     * para os correspondeste vai criar um botão com o seu id de entrada
     *
     * @param event evento
     */
    @FXML
    private void pesquisarCodProduto(ActionEvent event) {
        vBoxEntradas.getChildren().removeAll(vBoxEntradas.getChildren());
        List<entradasProdutos> encProd = procurarTipoQuantidadeProduto(codProdutoPesquisar.getText());
        for (entradasProdutos enp : encProd) {
            List<entradas> entradas = listarEntradasId(enp.getIdEntrada());
            for (entradas ec : entradas) {

                String id = ec.getIdEntrada();
                Button botao = new Button(id + " - " + ec.getDataEntrada());
                botao.setId("botaoVbox");
                botao.setPrefHeight(30);
                botao.setPrefWidth(250);
                botao.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent e) {

                        listarInfoEncProd(id, codProdutoPesquisar.getText());

                    }
                });
                altura_vbox2 = altura_vbox2 + 30;
                vBoxEntradas.getChildren().addAll(botao);
                if (altura_vbox2 > 330) {
                    if (altura_vbox2 > 330) {
                        vBoxEntradas.setPrefWidth(250);
                    }
                    vBoxEntradas.setPrefHeight(altura_vbox2);
                }
            }
        }
    }

    /**
     * método para substituir o conteudo do menu de opções para colocar as
     * opções por ordem caso seja o operador a abrir o ecrã
     */
    private void adicionarEcraPessoalOperadorMenuOpcoes() {
        List<utilizador> operadores = procurarEmail(login.getIdLogin());
        for (utilizador operador : operadores) {
            if (operador.getNivel() == 1) {
                opcoes.getItems().clear();
                MenuItem ePOp = new MenuItem("Ecrã Pessoal");
                MenuItem terminarSessao = new MenuItem("Terminar Sessão");
                MenuItem sair = new MenuItem("Sair");
                opcoes.getItems().add(ePOp);
                opcoes.getItems().add(terminarSessao);
                opcoes.getItems().add(sair);
                ePOp.setOnAction((ActionEvent e) -> {
                    try {
                        ecraPessoalOperador ePO = new ecraPessoalOperador();
                        fecha();
                        ePO.start(new Stage());

                    } catch (Exception ex) {
                        Logger.getLogger(movimentosProdutoController.class
                                .getName()).log(Level.SEVERE, null, ex);
                    }
                });
                terminarSessao.setOnAction((ActionEvent e) -> {
                    try {
                        login.setIdLogin(0);
                        login.setNivelLogin(10000);
                        login log = new login();
                        fecha();
                        log.start(new Stage());

                    } catch (Exception ex) {
                        Logger.getLogger(movimentosProdutoController.class
                                .getName()).log(Level.SEVERE, null, ex);
                    }
                });
                sair.setOnAction((ActionEvent e) -> {
                    System.exit(0);
                });
            }
        }
    }

}
