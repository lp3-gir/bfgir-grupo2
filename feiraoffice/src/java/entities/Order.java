/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Ruben
 */
@Entity
@XmlRootElement
public class Order implements Serializable {

    @Id
    private List<Encomendas> Encomendas;
    private List<Encomendasproduto> Encomendasproduto;
    private List<Produto> Produto;

    /**
     *
     */
    public Order() {
    }

    /**
     *
     * @param Encomendas recebe uma lista de objetos do tipo encomendas
     * @param Encomendasproduto recebe uma lista de objetos do tipo encomendas produto
     * @param Produto recebe uma lista de objetos do tipo produto
     */
    public Order(List<Encomendas> Encomendas, List<Encomendasproduto> Encomendasproduto, List<Produto> Produto) {
        this.Encomendas = Encomendas;
        this.Encomendasproduto = Encomendasproduto;
        this.Produto = Produto;
    }

    /**
     * @return the Encomendas
     */
    public List<Encomendas> getEncomendas() {
        return Encomendas;
    }

    /**
     * @param Encomendas the Encomendas to set
     */
    public void setEncomendas(List<Encomendas> Encomendas) {
        this.Encomendas = Encomendas;
    }

    /**
     * @return the Encomendasproduto
     */
    public List<Encomendasproduto> getEncomendasproduto() {
        return Encomendasproduto;
    }

    /**
     * @param Encomendasproduto the Encomendasproduto to set
     */
    public void setEncomendasproduto(List<Encomendasproduto> Encomendasproduto) {
        this.Encomendasproduto = Encomendasproduto;
    }

    /**
     * @return the Produto
     */
    public List<Produto> getProduto() {
        return Produto;
    }

    /**
     * @param Produto the Produto to set
     */
    public void setProduto(List<Produto> Produto) {
        this.Produto = Produto;
    }

}
