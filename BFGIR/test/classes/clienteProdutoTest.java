/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.util.List;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Classe de testes da classe clienteProduto.
 *
 * @author Ilídio Magalhães
 */
public class clienteProdutoTest {

    private static clienteProduto cli = new clienteProduto();

    /**
     * Construtor da classe clienteProdutoTest.
     */
    public clienteProdutoTest() {
    }

    /**
     * Método para estabelecer os atributos do objeto cli.
     */
    @Before
    public void setUp() {
        cli.setIdCliente(0);
        cli.setCodProdPa("abc");
        cli.setPrecoUnitSIVA(1.1f);
    }

    /**
     * Teste ao método getIdCliente da classe clienteProduto.<br>
     * Método tenta buscar o id do objeto previamente criado na classe.<br>
     * O teste é válido caso o valor obtido seja igual ao valor esperado.
     */
    @Test
    public void testGetIdCliente() {
        System.out.println("\nTeste ao método getIdCliente");
        int expResult = 0;
        int result = cli.getIdCliente();
        if (expResult == result) {
            System.out.println("Teste ao método getIdCliente passado com um sucesso!\n");
        }
        assertEquals(expResult, result);
    }

    /**
     * Teste ao método setIdCliente da classe clienteProduto.<br>
     * Método modifica o id do objeto cli e depois verifica se o consegiu
     * alterar.<br>
     * O teste é válido caso tal se verifique.<br>
     * No fim, restaura o atributo ao estado inicial.
     */
    @Test
    public void testSetIdCliente() {
        System.out.println("\nTeste ao método setIdCliente");
        int id = 1;
        int idBackup = cli.getIdCliente();
        cli.setIdCliente(id);
        boolean result = false;
        if (id == cli.getIdCliente()) {
            result = true;
            System.out.println("Teste ao método setIdCliente passado com um sucesso!\n");
        }
        assertTrue(result);
        cli.setIdCliente(idBackup);
    }

    /**
     * Teste ao método getCodProdPa da classe clienteProduto.<br>
     * Método tenta buscar o código do produto do objeto previamente criado na
     * classe.<br>
     * O teste é válido caso o valor obtido seja igual ao valor esperado.
     */
    @Test
    public void testGetCodProdPa() {
        System.out.println("\nTeste ao método getCodProdPa");
        String expResult = "abc";
        String result = cli.getCodProdPa();
        if (expResult.equals(result)) {
            System.out.println("Teste ao método getCodProdPa passado com um sucesso!\n");
        }
        assertEquals(expResult, result);
    }

    /**
     * Teste ao método setCodProdPa da classe clienteProduto.<br>
     * Método modifica o código do produto do objeto cli e depois verifica se o
     * consegiu alterar.<br>
     * O teste é válido caso tal se verifique.<br>
     * No fim, restaura o atributo ao estado inicial.
     */
    @Test
    public void testSetCodProdPa() {
        System.out.println("\nTeste ao método setCodProdPa");
        String cpI = "defg";
        String cpF = cli.getCodProdPa();
        cli.setCodProdPa(cpI);
        boolean result = false;
        if (cpI.equals(cli.getCodProdPa())) {
            result = true;
            System.out.println("Teste ao método setCodProdPa passado com um sucesso!\n");
        }
        assertTrue(result);
        cli.setCodProdPa(cpF);
    }

    /**
     * Teste ao método getPrecoUnitSIVA da classe clienteProduto.<br>
     * Método tenta buscar o preço do produto do objeto previamente criado na
     * classe.<br>
     * O teste é válido caso o valor obtido seja igual ao valor esperado.
     */
    @Test
    public void testGetPrecoUnitSIVA() {
        System.out.println("\nTeste ao método getPrecoUnitSIVA");
        float expResult = 1.1f;
        float result = cli.getPrecoUnitSIVA();
        if (expResult == result) {
            System.out.println("Teste ao método getPrecoUnitSIVA passado com um sucesso!\n");
        }
        assertEquals(expResult, result, 0.0f);
    }

    /**
     *
     * Teste ao método setPrecoUnitSIVA da classe clienteProduto.<br>
     * Método modifica o preço do produto do objeto cli e depois verifica se o
     * consegiu alterar.<br>
     * O teste é válido caso tal se verifique.<br>
     * No fim, restaura o atributo ao estado inicial.
     */
    @Test
    public void testSetPrecoUnitSIVA() {
        System.out.println("\nTeste ao método setPrecoUnitSIVA");
        float cI = 12.2f;
        float cF = cli.getPrecoUnitSIVA();
        cli.setPrecoUnitSIVA(cI);
        boolean result = false;
        if (cI == (cli.getPrecoUnitSIVA())) {
            result = true;
            System.out.println("Teste ao método setPrecoUnitSIVA passado com um sucesso!\n");
        }
        assertTrue(result);
        cli.setPrecoUnitSIVA(cF);
    }
}
