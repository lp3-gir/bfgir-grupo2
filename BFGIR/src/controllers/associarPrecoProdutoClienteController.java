/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import bfgir.associarPrecoProdutoCliente;
import bfgir.ecraPessoalOperador;
import bfgir.login;
import bfgir.menuAdministrador;
import bfgir.menuOperador;
import classes.cliente;
import static classes.cliente.getClienteID;
import static classes.cliente.listarClienteAct;
import static classes.clienteProduto.inserirProdutos;
import classes.produto;
import static classes.produto.getListaProdutos;
import static classes.produto.pesquisarProdutoID;
import classes.utilizador;
import static classes.utilizador.procurarEmail;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Classe controladora do ecrã associarPrecoProdutoCliente.<br>
 *
 * @author gonca
 */
public class associarPrecoProdutoClienteController implements Initializable {

    @FXML
    private Pane painelProdutosClientes;
    @FXML
    private ScrollPane produtos;
    private int altura_vbox = 0;
    @FXML
    private VBox vBoxProdutos;
    @FXML
    private ScrollPane produtosCliente;
    @FXML
    private VBox vBoxClientes;
    @FXML
    private Menu opcoes;
    @FXML
    private MenuBar menu;
    private String idGlob;

    int ids;
    Float atualizarPrecoUnit = 0.0f;
    private TextField codClientePesquisar;
    @FXML
    private TextField inserePreço;
    @FXML
    private Button guardaValor;
    @FXML
    private TextField codClienteProd;
    @FXML
    private Button pesquisarCodProd;
    private String codProd = "";
    private int idCliente = 0;
    private String preco = "";
    private float pbase = 0;
    @FXML
    private TextField codCliente;
    @FXML
    private Button pesquisarClientes;

    Alert a2 = new Alert(Alert.AlertType.NONE);

    /**
     * Método de inicialização do ecrã.<br>
     *
     * Faz a listagem de todos os clientes e é criado um botão com o nome do
     * cliente mais o id produto, o botão de fazer a pesquisa por cliente , de
     * guardar o valor e o de atribuir preço são invisives para impedir que o
     * uitlizador escolha primeiro um cliente antes de fazer a pesquisa por
     * produto ou até mesmo atribuir um preço.<br> Depois de escolher um produto
     * a ferramenta de pesquisar por produto fica disponivel.<br>
     *
     * @param url url.
     * @param rb rb.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        inserePreço.setVisible(false);
        codClienteProd.setVisible(false);
        pesquisarCodProd.setVisible(false);
        guardaValor.setVisible(false);

        adicionarEcraPessoalOperadorMenuOpcoes();
        List<cliente> cl = listarClienteAct();
        for (cliente cli : cl) {

            int id = cli.getIdCliente();
            ids = id;
            String nome = cli.getNome() + " - " + cli.getIdCliente();
            Button botao = new Button(nome);
            botao.setId("botaoVbox");
            botao.setPrefHeight(30);
            //botao.setPrefWidth(275);
            botao.setPrefWidth(vBoxClientes.getPrefWidth());
            botao.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {

                    botao.setId("botaoVboxChangeCollour");
                    idCliente = id;
                    listarProdutos(id);
                    codClienteProd.setVisible(true);
                    pesquisarCodProd.setVisible(true);

                }
            });
            vBoxClientes.getChildren().addAll(botao);

        }

    }

    /**
     * Método para terminar sessao do utilizador.<br>
     *
     * @param event evento.
     */
    @FXML
    private void terminarSessao(ActionEvent event) {
        try {
            login.setIdLogin(0);
            login.setNivelLogin(10000);
            login log = new login();
            fecha();
            log.start(new Stage());

        } catch (Exception ex) {
            Logger.getLogger(menuAdministradorController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Método para encerrar a aplicação.<br>
     */
    @FXML
    private void sair(ActionEvent event) {
        System.exit(0);
    }

    /**
     * Método para aceder ao menu do operador ou do administrador conforme o seu
     * nivel de login.<br>
     */
    @FXML
    private void openMenu(ActionEvent event) {
        try {
            if (login.getNivelLogin() == 1) {
                menuOperador menuO = new menuOperador();
                fecha();
                menuO.start(new Stage());
            } else {
                menuAdministrador menuAdmin = new menuAdministrador();
                fecha();
                menuAdmin.start(new Stage());
            }
        } catch (Exception ex) {
            Logger.getLogger(eliminarFornecedorController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Método para listar todos os produtos.<br>
     *
     * Método fas a listagem de todos os produtos e é criado um botão com o
     * codigo do produto mais a sua descrição.<br> Quando o utilizador escolhe
     * um produto a ferramenta de atriburi preço e de guardar a associação fica
     * disponivel.<br>
     *
     * @param idCli recebe um id de Cliente
     */
    public void listarProdutos(int idCli) {
        vBoxProdutos.getChildren().removeAll(vBoxProdutos.getChildren());

        List<produto> produtos = getListaProdutos();

        for (produto prod : produtos) {
            String id = prod.getCodProdPa();
            String codpr = prod.getCodProdPa();
            String nome = prod.getCodProdPa() + " - " + prod.getDescricao() + " " + "Preço base: " + prod.getPrecoUnitSIVA();
            Button botao = new Button(nome);
            botao.setId("botaoVbox");
            botao.setPrefHeight(30);
            botao.setPrefWidth(570);
            botao.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {
                    botao.setId("botaoVboxChangeCollour");

                    inserePreço.setVisible(true);
                    codProd = codpr;
                    guardaValor.setVisible(true);

                }
            });
            vBoxProdutos.getChildren().addAll(botao);

        }

    }

    /**
     * Método de pesquisa de produto pelo seu código.<br>
     *
     * Método fas a listagem de todos os produtos que recebe atrávés da pesquisa
     * recebida realizada pelo utilizador e cria um botão com o codigo do
     * produto e a sua descrição.<br> Quando o utilizador escolhe um produto a
     * ferramenta de atribuir preço e de guardar a associação fica
     * disponivel.<br>
     *
     * @param codProds variável que é recebida do método de pesquisa e serve
     * para filtrar os produtos para o codigo recebido.
     */
    public void listarProdutosPorPesquisa(String codProds) {
        vBoxProdutos.getChildren().removeAll(vBoxProdutos.getChildren());

        List<produto> produtos = pesquisarProdutoID(codProds);

        for (produto prod : produtos) {
            String id = codProds;
            String nome = prod.getCodProdPa() + " - " + prod.getDescricao() + " " + "Preço base: " + prod.getPrecoUnitSIVA();
            pbase = prod.getPrecoUnitSIVA();
            Button botao = new Button(nome);
            botao.setId("botaoVbox");
            botao.setPrefHeight(30);
            //botao.setPrefWidth(403);
            botao.setPrefWidth(vBoxProdutos.getPrefWidth());
            botao.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {
                    botao.setId("botaoVboxChangeCollour");
                    inserePreço.setVisible(true);
                    codProd = codProds;
                    guardaValor.setVisible(true);

                }
            });
            vBoxProdutos.getChildren().addAll(botao);

        }

    }

    /**
     * Método pesquisar clientes pelo seu identificador.<br>
     *
     * Método faz a listagem de todos os clientes que recebe atrávés da pesquisa
     * recebida realizada pelo utilizador e cria um botão com o codigo do
     * produto e a sua descrição.<br>
     *
     * Depois de escolher um produto a ferramenta de pesquisar por produto fica
     * disponivel.<br>
     *
     * @param idClientes variável que é recebida do método de pesquisa e serve
     * para filtrar os clientes para o id recebido.
     */
    public void listarClientesPorPesquisa(int idClientes) {

        vBoxClientes.getChildren().removeAll(vBoxClientes.getChildren());

        List<cliente> cli = getClienteID(idClientes);

        for (cliente cl : cli) {

            String nome = cl.getNome() + " - " + cl.getIdCliente();
            Button botao = new Button(nome);
            botao.setId("botaoVbox");
            botao.setPrefHeight(30);
            botao.setPrefWidth(vBoxClientes.getPrefWidth());
            botao.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {
                    botao.setId("botaoVboxChangeCollour");
                    idCliente = idClientes;
                    listarProdutos(idCliente);
                    codClienteProd.setVisible(true);
                    pesquisarCodProd.setVisible(true);

                }
            });
            vBoxClientes.getChildren().addAll(botao);

        }

    }

    /**
     * Método para substituir o conteudo do menu de opções para colocar as
     * opções por ordem caso seja o operador a abrir o ecrã.<br>
     */
    private void adicionarEcraPessoalOperadorMenuOpcoes() {
        //substituir o conteudo do menu de opções para colocar as opções por ordem caso seja o operador a abrir o ecrã
        List<utilizador> operadores = procurarEmail(login.getIdLogin());
        for (utilizador operador : operadores) {
            if (operador.getNivel() == 1) {
                opcoes.getItems().clear();
                MenuItem ePOp = new MenuItem("Ecrã Pessoal");
                MenuItem terminarSessao = new MenuItem("Terminar Sessão");
                MenuItem sair = new MenuItem("Sair");
                opcoes.getItems().add(ePOp);
                opcoes.getItems().add(terminarSessao);
                opcoes.getItems().add(sair);
                ePOp.setOnAction((ActionEvent e) -> {
                    try {
                        ecraPessoalOperador ePO = new ecraPessoalOperador();
                        fecha();
                        ePO.start(new Stage());
                    } catch (Exception ex) {
                        Logger.getLogger(aprovarEntradaStocksController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
                terminarSessao.setOnAction((ActionEvent e) -> {
                    try {
                        login.setIdLogin(0);
                        login.setNivelLogin(10000);
                        login log = new login();
                        fecha();
                        log.start(new Stage());
                    } catch (Exception ex) {
                        Logger.getLogger(aprovarEntradaStocksController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
                sair.setOnAction((ActionEvent e) -> {
                    System.exit(0);
                });
            }
        }
    }

    /**
     * Método para fechar o ecrã.<br>
     */
    private void fecha() {
        associarPrecoProdutoCliente.getStage().close();

    }

    /**
     * Método de associar preços a um produto.<br>
     *
     * Método recebe os valores dos outros de métodos e vai fazer a inserção se
     * possivel da associação na base de dados.<br>
     *
     * Antes de fazer o envio tem de passar no teste onde verifica se o preço
     * inserido pelo utilizador é 20% superior ao preço base e se o teste passar
     * não é possivel fazer o envio de dados e dá um erro ao utilizador a
     * informar que não foi possivel realizar a operação.<br>Se for possivel os
     * dados são enviados e o utilizador recebe uma notiificação a dizer que foi
     * associado com sucesso.<br>
     *
     */
    @FXML
    private void guardarValor(ActionEvent event) {

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Feira & Office ");
        alert.setHeaderText("Deseja mesmo associar este produto + preço ao cliente?");
        ButtonType buttonTypeYES = new ButtonType("Sim");
        ButtonType buttonTypeNO = new ButtonType("Não");

        alert.getButtonTypes().setAll(buttonTypeYES, buttonTypeNO);
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == buttonTypeYES) {

            String a = inserePreço.getText();
            float f = Float.parseFloat(a);
            List<produto> produtos = pesquisarProdutoID(codProd);
            for (produto prod : produtos) {
                float ptotal = (float) (prod.getPrecoUnitSIVA() * 1.20);
                if (f < ptotal) {
                    try {
                        Alert alerts = new Alert(Alert.AlertType.WARNING);
                        alerts.setTitle("Feira & Office ");
                        alerts.setHeaderText("O valor inserido tem de ser 20% maior que o preço base");
                        associarPrecoProdutoCliente ac = new associarPrecoProdutoCliente();
                        ButtonType buttonTypeOK = new ButtonType("Confirmar");
                        alert.getButtonTypes().setAll(buttonTypeOK);
                        Optional<ButtonType> resultsError = alerts.showAndWait();
                        event.consume();
                        fecha();

                        ac.start(new Stage());
                    } catch (Exception ex) {
                        Logger.getLogger(associarPrecoProdutoClienteController.class.getName()).log(Level.SEVERE, null, ex);
                    }

                } else if (f >= ptotal) {
                    try {
                        inserirProdutos(idCliente, codProd, f);
                        associarPrecoProdutoCliente ac = new associarPrecoProdutoCliente();
                        Alert alerts = new Alert(Alert.AlertType.INFORMATION);
                        alerts.setTitle("Feira & Office ");
                        alerts.setHeaderText("Associação completa com sucesso");
                        ButtonType buttonTypeOK = new ButtonType("Ok");
                        alerts.getButtonTypes().setAll(buttonTypeOK);
                        Optional<ButtonType> results = alerts.showAndWait();
                        event.consume();
                        fecha();

                        ac.start(new Stage());
                    } catch (Exception e) {
                        Alert alerts = new Alert(Alert.AlertType.WARNING);
                        alerts.setTitle("Feira & Office ");
                        alerts.setHeaderText("Ocorreu um erro durante a associação");
                        ButtonType buttonTypeOK = new ButtonType("Ok");
                        alerts.getButtonTypes().setAll(buttonTypeOK);
                        Optional<ButtonType> results = alert.showAndWait();
                        event.consume();
                        fecha();

                    }
                } else {
                    Alert alerts = new Alert(Alert.AlertType.WARNING);
                    alerts.setTitle("Feira & Office ");
                    alerts.setHeaderText("Ocorreu um erro durante a associação");
                    ButtonType buttonTypeOK = new ButtonType("Confirmar");
                    alerts.getButtonTypes().setAll(buttonTypeOK);
                    Optional<ButtonType> results = alert.showAndWait();
                    event.consume();
                    fecha();

                    associarPrecoProdutoCliente ac = new associarPrecoProdutoCliente();

                }
            }
        } else {
            event.consume();
            fecha();
        }
    }

    /**
     * Método para filtrar produtos específicos.<br>
     * Método pesquisa por produtos pelo seu código e remove todos os outros
     * resultados da lista de produtos mostrada.<br>
     */
    @FXML
    private void pesquisarCodCliente(ActionEvent event) {
        try {
            listarProdutosPorPesquisa(codClienteProd.getText());
        } catch (Exception e) {

            a2.setAlertType(Alert.AlertType.INFORMATION);
            a2.setTitle("Associar Preço Produto");
            a2.setHeaderText("Código de cliente inválido!");
            a2.setContentText("Introduza apenas valores numéricos!");
            a2.getButtonTypes().setAll(ButtonType.OK);
            Optional<ButtonType> result2 = a2.showAndWait();
        }
    }

   /**
     * Método para filtrar clientes específicos.<br>
     * Método pesquisa por clientes pelo seu identificador e remove todos os outros
     * resultados da lista de produtos mostrada.<br>
     */
    @FXML
    private void pesquisarCliente(ActionEvent event) {

        try {
            String prov = codCliente.getText();
            int numero = Integer.parseInt(prov);
            listarClientesPorPesquisa(numero);
        } catch (NumberFormatException e) {

            a2.setAlertType(Alert.AlertType.INFORMATION);
            a2.setTitle("Associar Preço Produto");
            a2.setHeaderText("Código de cliente inválido!");
            a2.setContentText("Introduza apenas valores numéricos!");
            a2.getButtonTypes().setAll(ButtonType.OK);
            Optional<ButtonType> result2 = a2.showAndWait();
        }
    }
}
