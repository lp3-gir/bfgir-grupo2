/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import bfgir.alterarFornecedores;
import bfgir.login;
import bfgir.menuAdministrador;
import bfgir.menuOperador;
import static classes.PasswordHelper.generatePassword;
import static classes.administrador.alterarPassEstadoUtilizador;
import static classes.administrador.procurarUtilizadorIDEmail;
import static classes.administrador.alterarCidade;
import static classes.administrador.alterarEmail;
import static classes.administrador.alterarEstadoFornecedor;
import static classes.administrador.alterarPaisMoradaCP;
import classes.codigoPostal;
import static classes.codigoPostal.procurarCodPostal;
import static classes.email.enviarEmail;
import classes.fornecedor;
import static classes.fornecedor.listarFornecedor;
import static classes.fornecedor.listarFornecedorTodos;
import classes.passwordUtils;
import classes.utilizador;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Classe controladora do ecrã alterarFornecedores.<br>
 */
public class alterarFornecedoresController implements Initializable {

    String id_fornecedor_atualizar = "";
    String email_fornecedor_atualizar = "";
    String alterarPassBase = "não";
    String estado_Fornecedor = "";
    int id_fornecedor_alterar = 0;
    int nivel_fornecedor = 0;
    int id_utilizador_atualizar = 0;
    int nivel_fornecedor_atualizar = 0;
    Alert a = new Alert(Alert.AlertType.NONE);

    @FXML
    private Label lbNome;
    @FXML
    private TextArea nome;
    @FXML
    private Label lbContribuinte;
    @FXML
    private TextArea contribuinte;
    @FXML
    private Label lbCodigoPostal;
    @FXML
    private Label lbMorada;
    @FXML
    private Label lbEstado;
    @FXML
    private TextArea codigoPostal;
    @FXML
    private TextArea morada;
    @FXML
    private Button alterarFornecedor;
    @FXML
    private Label lbPais;
    @FXML
    private TextArea pais;
    @FXML
    private Label lbEmail;
    @FXML
    private TextArea email;
    @FXML
    private AnchorPane painelFornecedor;
    @FXML
    private ScrollPane fornecedorScrollPane;
    @FXML
    private VBox vBoxFornecedor;
    @FXML
    private MenuBar menu;
    @FXML
    private ChoiceBox<String> cbpass;
    @FXML
    private ChoiceBox<String> cbestado;
    @FXML
    private TextArea cidade;
    @FXML
    private Label lbCidade;
    @FXML
    private Label lbPass;
    @FXML
    private Button botaoAlterarFornecedor;

    private int altura_vbox = 0;

    /**
     * Método Intialize.<br>
     *
     * Neste método criada uma lista com todos os fornecedores ordenados por
     * ordem crescente do seu ID de Utilizador obtidos pelo método
     * listarFornecedorTodos.<br>
     * De seguida para cada elemento dessa lista de fornecedores é criado um
     * botão com o ID Utilizador do fornecedor.<br>
     * Depois de selecionar um fornecedor é feito um select com o codigo do
     * mesmo (idUtilizador) através do método listarFornecedor que retorna o
     * fornecedor correspondente ao idUtilizador, este fornecedor é descriminado
     * por cada atributo e listado no ecrã.<br>
     *
     * @param url url.
     * @param rb rb.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        lbNome.setVisible(false);
        lbContribuinte.setVisible(false);
        lbMorada.setVisible(false);
        lbCodigoPostal.setVisible(false);
        lbPais.setVisible(false);
        lbEstado.setVisible(false);
        lbEmail.setVisible(false);
        lbCidade.setVisible(false);
        lbPass.setVisible(false);

        botaoAlterarFornecedor.setVisible(false);

        nome.setVisible(false);
        contribuinte.setVisible(false);
        morada.setVisible(false);
        codigoPostal.setVisible(false);
        pais.setVisible(false);
        email.setVisible(false);
        cidade.setVisible(false);
        cbpass.setVisible(false);
        cbestado.setVisible(false);

        email.setEditable(false);
        cbpass.setDisable(true);
        cbestado.setDisable(true);

        cbpass.getItems().add("sim");
        cbpass.getItems().add("não");
        cbestado.getItems().add("ativo");
        cbestado.getItems().add("inativo");

        List<fornecedor> forn = listarFornecedorTodos();
        for (fornecedor fornecedores : forn) {

            String idF = fornecedores.getIdFornecedor();
            int id = fornecedores.getIdUtilizador();
            Button botao = new Button(String.valueOf(id));
            botao.setId("botaoVbox");
            botao.setPrefHeight(30);
            botao.setPrefWidth(275);
            botao.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {
                    botao.setId("botaoVboxChangeCollour");
                    setVisibleTrue();

                    List<fornecedor> fornecedores = listarFornecedor(id);

                    List<utilizador> utEm = utilizador.procurarEmail(id);
                    utilizador ut = utEm.get(0);
                    String email123 = ut.getEmail();

                    fornecedor forn = fornecedores.get(0);
                    String codP = forn.getCodPostal();

                    List<codigoPostal> codCidade = procurarCodPostal(codP);
                    codigoPostal cdO = codCidade.get(0);
                    String city = cdO.getCidade();

                    for (fornecedor fornecedor : fornecedores) {
                        id_fornecedor_atualizar = idF;
                        id_utilizador_atualizar = id;
                        nome.setText(fornecedor.getNome());
                        contribuinte.setText(String.valueOf(fornecedor.getContribuinte()));
                        morada.setText(fornecedor.getMorada());
                        codigoPostal.setText(fornecedor.getCodPostal());
                        pais.setText(fornecedor.getPais());
                        email.setText(email123);
                        cidade.setText(city);
                        email_fornecedor_atualizar = email.getText();
                        cbestado.setValue(fornecedor.getEstado());
                        cbpass.setValue(alterarPassBase);
                        cbestado.setValue(fornecedor.getEstado());
                        estado_Fornecedor = cbestado.getValue(); //se não passar este valor colocar para dar erro
                        if (estado_Fornecedor.equals("")) {
                            a.setAlertType(Alert.AlertType.ERROR);
                            a.setTitle("ERRO SQL");
                            a.setHeaderText("Dados do fornecedor em falta!");
                            a.setContentText("Não foi possível obter os dados necessários.Tente aceder a outro fornecedor.\n"
                                    + "\nCaso esteja tudo em ordem com o segundo fornecedor,"
                                    + "\nconsulte a base de dados para verificar se existem dados do primeiro fornecedor em falta."
                                    + "\nCaso estes também estejam em ordem, consulte a equipa de desenvolvimento da aplicação\n"
                                    + "pois existe um problema com as configurações da aplicação.");
                            a.show();
                            alterarFornecedores altForn = new alterarFornecedores();
                            fecha();

                            try {
                                altForn.start(new Stage());
                            } catch (Exception ex) {
                                a.setAlertType(Alert.AlertType.ERROR);
                                a.setTitle("ERRO Fatal");
                                a.setHeaderText("A aplicação está corrompida!");
                                a.setContentText("Algumas funcionalidades do aplicação estão inacessíveis. Tente uma reinstalação.");
                                a.show();
                                System.out.println("Erro Exception ex " + ex.getMessage());
                                Logger.getLogger(alterarFornecedoresController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }

                }

            });
            altura_vbox = altura_vbox + 30;
            vBoxFornecedor.getChildren().addAll(botao);
            if (altura_vbox > 550) {
                vBoxFornecedor.setPrefHeight(altura_vbox);
            }
        }
    }

    /**
     * Método para tornar visivel atributos do fornecedor selecionado.<br>
     */
    private void setVisibleTrue() {

        lbNome.setVisible(true);
        lbContribuinte.setVisible(true);
        lbMorada.setVisible(true);
        lbCodigoPostal.setVisible(true);
        lbPais.setVisible(true);
        lbEstado.setVisible(true);
        lbCidade.setVisible(true);
        lbEmail.setVisible(true);
        lbPass.setVisible(true);

        nome.setVisible(true);
        contribuinte.setVisible(true);
        morada.setVisible(true);
        codigoPostal.setVisible(true);
        pais.setVisible(true);
        cidade.setVisible(true);
        email.setVisible(true);
        cbestado.setVisible(true);
        cbpass.setVisible(true);

        email.setEditable(true);
        cbpass.setDisable(false);
        cbestado.setDisable(false);

        botaoAlterarFornecedor.setVisible(true);

    }

    /**
     * Este método vai fazer varias confirmaçoes as alteraçoes que ocorreram ao
     * fornecedor e depois de passar a elas todas vai alterar os valores na BD.<br>
     *
     * @param event evento.
     *
     */
    @FXML
    private void botaoAlterarFornecedor(ActionEvent event) {

        a.setAlertType(Alert.AlertType.CONFIRMATION);
        a.setTitle("Fornecedores");
        a.setHeaderText("Deseja mesmo atualizar o fornecedor?");
        a.setResizable(false);
        a.getButtonTypes().setAll(ButtonType.YES, ButtonType.NO);
        Optional<ButtonType> result = a.showAndWait();

        if (result.get() == ButtonType.YES) {

            Alert a2 = new Alert(Alert.AlertType.NONE);

            //verificar se o utilizador ainda existe na BD
            List<utilizador> utilizadores = procurarUtilizadorIDEmail(id_utilizador_atualizar, email_fornecedor_atualizar);

            int count = 0;
            for (utilizador uti : utilizadores) {
                count = 1;
            }
            //notificação se o fornecedor não existir na BD
            if (count == 0) {
                a.setAlertType(Alert.AlertType.INFORMATION);
                a.setTitle("Fornecedor");
                a.setHeaderText("O fornecedor que estava a alterar foi removido!");
                a.setContentText("O fornecedor for eliminado enquanto o alterava.");
                a.setResizable(false);
                Optional<ButtonType> result2 = a.showAndWait();
                alterarFornecedores altForn = new alterarFornecedores();
                fecha();
                try {
                    altForn.start(new Stage());
                } catch (Exception ex) {
                    a.setAlertType(Alert.AlertType.ERROR);
                    a.setTitle("ERRO Fatal");
                    a.setHeaderText("A aplicação está corrompida!");
                    a.setContentText("Algumas funcionalidades do aplicação estão inacessíveis. Tente uma reinstalação.");
                    Optional<ButtonType> result3 = a.showAndWait();
                    Logger.getLogger(alterarFornecedoresController.class.getName()).log(Level.SEVERE, null, ex);
                }
                // verificação se os campos do fornecedor n estao nulos, se sim, notificação 
            } else if (estado_Fornecedor.equals("") | estado_Fornecedor == null | email.equals("") | email == null | alterarPassBase.equals("") | alterarPassBase == null
                    | "".equals(cbpass.getValue()) | cbpass.getValue() == null
                    | "".equals(cbestado.getValue()) | cbestado.getValue() == null) {

                a.setAlertType(Alert.AlertType.INFORMATION);
                a.setTitle("Fornecedor");
                a.setHeaderText("Fornecedor com campos nulos.");
                a.setContentText("O Fornecedor não pode ter campos NULOS!");
                a.setResizable(false);
                Optional<ButtonType> result2 = a.showAndWait();
                alterarFornecedores altForn = new alterarFornecedores();
                fecha();
                try {
                    altForn.start(new Stage());
                } catch (Exception ex) {
                    a.setAlertType(Alert.AlertType.ERROR);
                    a.setTitle("ERRO Fatal");
                    a.setHeaderText("A aplicação está corrompida!");
                    a.setContentText("Algumas funcionalidades do aplicação estão inacessíveis. Tente uma reinstalação.");
                    Optional<ButtonType> result3 = a.showAndWait();
                    Logger.getLogger(alterarFornecedoresController.class.getName()).log(Level.SEVERE, null, ex);
                }
                // verificação e notificação se os campos do utilizador são validos
            } else if (isValidEmailAddress(email.getText()) == false | isValidPaisCidade(pais.getText()) == false | isValidPaisCidade(cidade.getText()) == false
                    | isValidCodigoPostal(codigoPostal.getText()) == false) {

                a.setAlertType(Alert.AlertType.INFORMATION);
                a.setTitle("Fornecedor");
                a.setHeaderText("Fornecedor com campos invalidos.");
                a.setContentText("Os campos 'Email' ou 'País' ou 'Cidade' ou "
                        + "\n'Morada' ou 'Codigo Postal' têm valores invalidos!");
                a.setResizable(false);
                Optional<ButtonType> result2 = a.showAndWait();
                alterarFornecedores altForn = new alterarFornecedores();
                fecha();
                try {
                    altForn.start(new Stage());
                } catch (Exception ex) {
                    a.setAlertType(Alert.AlertType.ERROR);
                    a.setTitle("ERRO Fatal");
                    a.setHeaderText("A aplicação está corrompida!");
                    a.setContentText("Algumas funcionalidades do aplicação estão inacessíveis. Tente uma reinstalação.");
                    Optional<ButtonType> result3 = a.showAndWait();
                    Logger.getLogger(alterarFornecedoresController.class.getName()).log(Level.SEVERE, null, ex);
                }

            } else //notificação de alteração inválida de password de utilizador inativo (pass alterada e estado inativo)
            if ("sim".equals(cbpass.getValue()) && "inativo".equals(cbestado.getValue())) {
                a2.setAlertType(Alert.AlertType.INFORMATION);
                a2.setTitle("Operadores");
                a2.setHeaderText("Tentou alterar password de um fornecedor inativo!");
                a2.setContentText("Ative o fornecedor caso também pretenda alterar a sua password.");
                a2.getButtonTypes().setAll(ButtonType.OK);
                Optional<ButtonType> result2 = a2.showAndWait();

            } else {

                try {
                    //tirado da classe inserirOFornecedoresController
                    String myPassword = generatePassword(20);
                    // Generate Salt. The generated value can be stored in DB.
                    String salt = passwordUtils.getSalt(30);
                    // Protect user's password. The generated value can be stored in DB.
                    String mySecurePassword = passwordUtils.generateSecurePassword(myPassword, salt);
                    //enviar email ao fornecedor
                    enviarEmail(email_fornecedor_atualizar, myPassword, "");
                    //alterar pass e estado
                    alterarPassEstadoUtilizador(mySecurePassword, salt, cbestado.getValue(), id_utilizador_atualizar, email_fornecedor_atualizar, nivel_fornecedor_atualizar);
                    //alterar estado do fornecedor
                    alterarEstadoFornecedor(cbestado.getValue(), id_utilizador_atualizar);
                    //alterar email do fornecedor
                    alterarEmail(email.getText(), id_utilizador_atualizar, nivel_fornecedor_atualizar);
                    //alterar pais, morada e Codigo Postal
                    alterarPaisMoradaCP(pais.getText(), morada.getText(), codigoPostal.getText(), id_fornecedor_atualizar, nome.getText());
                    //alterar cidade
                    alterarCidade(cidade.getText(), codigoPostal.getText());

                    a2.setAlertType(Alert.AlertType.INFORMATION);
                    //notificação de alteração de que o fornecedor foi alterado com sucesso
                    a2.setTitle("Fornecedores");
                    a2.setHeaderText("O fornecedor foi alterado com sucesso.");
                    a2.setContentText("As alterações feitas ao fornecedor foram registadas com sucesso!");
                    a2.getButtonTypes().setAll(ButtonType.OK);
                    Optional<ButtonType> result2 = a2.showAndWait();

                    alterarFornecedores altForn = new alterarFornecedores();
                    fecha();
                    try {
                        altForn.start(new Stage());
                    } catch (Exception ex) {
                        a.setAlertType(Alert.AlertType.ERROR);
                        a.setTitle("ERRO Fatal");
                        a.setHeaderText("A aplicação está corrompida!");
                        a.setContentText("Algumas funcionalidades do aplicação estão inacessíveis. Tente uma reinstalação.");
                        Optional<ButtonType> result3 = a.showAndWait();
                        Logger.getLogger(alterarFornecedoresController.class.getName()).log(Level.SEVERE, null, ex);
                    }

                } catch (Exception passwordEstadoUpdate) {
                    a.setAlertType(Alert.AlertType.ERROR);
                    a.setTitle("ERRO ATUALIZAR PASSWORD E ESTADO FORNECEDOR");
                    a.setHeaderText("Não foi possível atualizar o fornecedor!");
                    a.setContentText("A base de dados pode estar ocupa ou indisponível ou não foi possível contactar o fornecedor.");
                    Optional<ButtonType> result2 = a.showAndWait();
                    Logger.getLogger(alterarFornecedoresController.class.getName()).log(Level.SEVERE, null, passwordEstadoUpdate);
                }

            }

        }

    }

    /**
     * Método para fechar o ecrã de alteração do fornecedor.<br>
     */
    private void fecha() {
        alterarFornecedores.getStage().close();
    }

    /**
     * Método para terminar sessao do utilizador.<br>
     *
     * @param event evento.
     */
    @FXML
    private void terminarSessao(ActionEvent event) {
        try {
            login.setIdLogin(0);
            login.setNivelLogin(10000);
            login log = new login();
            fecha();
            log.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuAdministradorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Método para encerrar a aplicação.<br>
     */
    @FXML
    private void sair(ActionEvent event) {
        System.exit(0);
    }

    /**
     * Método para aceder ao menu do fornecedor.<br>
     */
    @FXML
    private void openMenu(ActionEvent event) {
        try {
            if (login.getNivelLogin() == 1) {
                menuOperador menuO = new menuOperador();
                fecha();
                menuO.start(new Stage());
            } else {
                menuAdministrador menuAdmin = new menuAdministrador();
                fecha();
                menuAdmin.start(new Stage());
            }
        } catch (Exception ex) {
            Logger.getLogger(alterarFornecedoresController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Método para verificar se o email é valido.<br>
     *
     * @param email email a ser verificado.
     * @return boolean se o email é valido ou não.
     */
    public static boolean isValidEmailAddress(String email) {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."
                + "[a-zA-Z0-9_+&*-]+)*@"
                + "(?:[a-zA-Z0-9-]+\\.)+[a-z"
                + "A-Z]{2,7}$";

        Pattern pat = Pattern.compile(emailRegex);
        if (email == null) {
            return false;
        }
        return pat.matcher(email).matches();
    }

    /**
     * Método para verificar se a cidade e o pais são válidos.<br>
     *
     * @param PC variavel com o pais ou a cidade.
     * @return boolean se o pais e a cidade são validos ou não.
     */
    public static boolean isValidPaisCidade(String PC) {
        String paisCidade = "^[a-zA-Z\\s]+$";

        Pattern pat = Pattern.compile(paisCidade);
        if (PC == null) {
            return false;
        }
        return pat.matcher(PC).matches();
    }

    /**
     * Método para verificar se o codigo postal é válido.<br>
     *
     * @param CP codigo postal a ser verificado.
     * @return boolean se o codigo Postal é valido ou não.
     */
    public static boolean isValidCodigoPostal(String CP) {
        String paisCidade = "^[0-9\\-\\s]+$";

        Pattern pat = Pattern.compile(paisCidade);
        if (CP == null) {
            return false;
        }
        return pat.matcher(CP).matches();
    }
}
