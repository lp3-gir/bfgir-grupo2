/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bfgir;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe de ligação à base de dados.
 */
public class BDConnection {

    private static Connection conn;

    private static String url = "jdbc:sqlserver://ctespbd.dei.isep.ipp.pt:1433;database=lp3_grupo2";
    private static String user = "lp3_grupo2";
    private static String pass = "Pa$$worD123";

    /**
     * Cria instância do driver.<br>
     *
     * @return conexão-
     */
    public static Connection connect() {

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
            //Class.forName("com.mysql.jdbc.Driver").newInstance();

            conn = DriverManager.getConnection(url, user, pass);

        } catch (SQLException | ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(BDConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        return conn;
    }

    /**
     * Estabelece conexão à base de dados.<br>
     *
     * @return conexão.
     */
    public static Connection getConnection(){
        try {
            if (conn != null && !conn.isClosed()) {
                return conn;
            }
            connect();
 
        } catch (SQLException ex) {
            Logger.getLogger(BDConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
           return conn;
    }
}
