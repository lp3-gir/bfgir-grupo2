/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Ruben
 */
@Entity
@Table(name = "pesos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pesos.findAll", query = "SELECT p FROM Pesos p")
    , @NamedQuery(name = "Pesos.findByCodPeso", query = "SELECT p FROM Pesos p WHERE p.codPeso = :codPeso")
    , @NamedQuery(name = "Pesos.findByPeso", query = "SELECT p FROM Pesos p WHERE p.peso = :peso")
    , @NamedQuery(name = "Pesos.findByTipoPeso", query = "SELECT p FROM Pesos p WHERE p.tipoPeso = :tipoPeso")})
public class Pesos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "codPeso")
    private String codPeso;
    @Basic(optional = false)
    @NotNull
    @Column(name = "peso")
    private double peso;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "tipoPeso")
    private String tipoPeso;

    /**
     *
     */
    public Pesos() {
    }

    /**
     *
     * @param codPeso recebe um código de peso
     */
    public Pesos(String codPeso) {
        this.codPeso = codPeso;
    }

    /**
     *
     * @param codPeso recebe um código de peso
     * @param peso recebe um peso
     * @param tipoPeso recebe um tipo de peso
     */
    public Pesos(String codPeso, double peso, String tipoPeso) {
        this.codPeso = codPeso;
        this.peso = peso;
        this.tipoPeso = tipoPeso;
    }

    /**
     *
     * @return codPeso
     */
    public String getCodPeso() {
        return codPeso;
    }

    /**
     *
     * @param codPeso
     */
    public void setCodPeso(String codPeso) {
        this.codPeso = codPeso;
    }

    /**
     *
     * @return peso
     */
    public double getPeso() {
        return peso;
    }

    /**
     *
     * @param peso
     */
    public void setPeso(double peso) {
        this.peso = peso;
    }

    /**
     *
     * @return tipoPeso
     */ 
    public String getTipoPeso() {
        return tipoPeso;
    }

    /**
     *
     * @param tipoPeso
     */
    public void setTipoPeso(String tipoPeso) {
        this.tipoPeso = tipoPeso;
    }
    
}
