/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ruben
 */
public class EncomendasprodutoTest {



    /**
     * Teste retirar id da encomenda produto.
     */
    @Test
    public void testGetIdEncProd() {
        System.out.println("getIdEncProd");
        Encomendasproduto instance = new Encomendasproduto();
        instance.setIdEncProd(1);
        Integer expResult = 1;
        assertEquals(expResult, instance.getIdEncProd());

    }

    /**
     * Teste definir id da encomenda produto.
     */
    @Test
    public void testSetIdEncProd() {
        System.out.println("setIdEncProd");
        Encomendasproduto instance = new Encomendasproduto();
        instance.setIdEncProd(1);

    }

    /**
     * Teste retirar id da encomenda.
     */
    @Test
    public void testGetIdEncomenda() {
        System.out.println("getIdEncomenda");
        Encomendasproduto instance = new Encomendasproduto();
        instance.setIdEncomenda(1);
        assertEquals(1, instance.getIdEncomenda());

    }

    /**
     * Teste definir ida da encomenda.
     */
    @Test
    public void testSetIdEncomenda() {
        System.out.println("setIdEncomenda");
        Encomendasproduto instance = new Encomendasproduto();
        instance.setIdEncomenda(1);

    }

    /**
     * Teste retirar código do produto associado a encomenda.
     */
    @Test
    public void testGetCodProdPa() {
        System.out.println("getCodProdPa");
        Encomendasproduto instance = new Encomendasproduto();
        instance.setCodProdPa("T.123");
        assertEquals("T.123", instance.getCodProdPa());

    }

    /**
     * Teste definir código do produto associado a encomenda.
     */
    @Test
    public void testSetCodProdPa() {
        System.out.println("setCodProdPa");
        Encomendasproduto instance = new Encomendasproduto();
        instance.setCodProdPa("T.123");

    }

    /**
     * Teste retirar quantidade de vendida associada ao produto da encomenda.
     */
    @Test
    public void testGetQuantVendida() {
        System.out.println("getQuantVendida");
        Encomendasproduto instance = new Encomendasproduto();
        instance.setQuantVendida(100);
        assertEquals(100, instance.getQuantVendida());

    }

    /**
     * Teste definir quantidade de vendida associada ao produto da encomenda.
     */
    @Test
    public void testSetQuantVendida() {
        System.out.println("setQuantVendida");
        Encomendasproduto instance = new Encomendasproduto();
        instance.setQuantVendida(100);

    }

    /**
     * Teste retirar tipo de venda associada ao produto da encomenda.
     */
    @Test
    public void testGetTipoVenda() {
        System.out.println("getTipoVenda");
        Encomendasproduto instance = new Encomendasproduto();
        instance.setTipoVenda("Teste");
        assertEquals("Teste", instance.getTipoVenda());

    }

    /**
     * Teste definir tipo de venda associada ao produto da encomenda.
     */
    @Test
    public void testSetTipoVenda() {
        System.out.println("setTipoVenda");
        Encomendasproduto instance = new Encomendasproduto();
        instance.setTipoVenda("Teste");

    }

    /**
     * Teste retirar preço por unidade sem IVA associado ao produto da
     * encomenda.
     */
    @Test
    public void testGetPrecoUnitSIVA() {
        System.out.println("getPrecoUnitSIVA");
        Encomendasproduto instance = new Encomendasproduto();
        instance.setPrecoUnitSIVA(10.5);
        assertEquals(10.5, instance.getPrecoUnitSIVA(), 0.0);

    }

    /**
     * Teste definir preço por unidade sem IVA associado ao produto da
     * encomenda.
     */
    @Test
    public void testSetPrecoUnitSIVA() {
        System.out.println("setPrecoUnitSIVA");
        Encomendasproduto instance = new Encomendasproduto();
        instance.setPrecoUnitSIVA(10.5);

    }

    /**
     * Teste retirar preço por unidade com IVA associado ao produto da
     * encomenda.
     */
    @Test
    public void testGetPrecoUnitCIVA() {
        System.out.println("getPrecoUnitCIVA");
        Encomendasproduto instance = new Encomendasproduto();
        instance.setPrecoUnitCIVA(24);
        assertEquals(24, instance.getPrecoUnitCIVA(), 0.0);

    }

    /**
     * Teste definir preço por unidade sem IVA associado ao produto da
     * encomenda.
     */
    @Test
    public void testSetPrecoUnitCIVA() {
        System.out.println("setPrecoUnitCIVA");
        Encomendasproduto instance = new Encomendasproduto();
        instance.setPrecoUnitCIVA(24);

    }

    /**
     * Teste retirar estado associado ao produto da encomenda.
     */
    @Test
    public void testGetEstado() {
        System.out.println("getEstado");
        Encomendasproduto instance = new Encomendasproduto();
        instance.setEstado("ativo");
        assertEquals("ativo", instance.getEstado());

    }

    /**
     * Teste definir estado associado ao produto da encomenda.
     */
    @Test
    public void testSetEstado() {
        System.out.println("setEstado");
        Encomendasproduto instance = new Encomendasproduto();
        instance.setEstado("ativo");

    }

}
