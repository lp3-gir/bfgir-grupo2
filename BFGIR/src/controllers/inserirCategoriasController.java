/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import bfgir.alterarCategorias;
import bfgir.ecraPessoalOperador;
import bfgir.inserirCategorias;
import bfgir.login;
import bfgir.menuAdministrador;
import bfgir.menuOperador;
import classes.categoria;
import static classes.categoria.inserirCategoria;
import static classes.categoria.listarCategoriaAtributos;
import static classes.categoria.listarCategorias;
import classes.helper;
import classes.utilizador;
import static classes.utilizador.procurarEmail;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

/**
 * classe do ecrã inserirCategorias
 *
 * @author Ilídio Magalhães
 */
public class inserirCategoriasController implements Initializable {

    static Alert a = new Alert(Alert.AlertType.NONE);
    final static String ESTADOCATEGORIA = "ativo";
    static List<categoria> listaCategoriasProduto = new ArrayList<categoria>();

    @FXML
    private MenuBar menu;
    @FXML
    private TextArea nome;
    @FXML
    private Menu opcoes;
    @FXML
    private TextArea descricao;
    @FXML
    private Button inserirCategoriasBD;
    @FXML
    private ChoiceBox<String> choiceNivel;
    @FXML
    private Label lbNivel;

    /**
     * Método para inicializar algumas funcionalidades do ecrã, nomeadamente o
     * ecrã pessoal do operador - caso o utilizador seja um operador, carregar a
     * lista de categorias existentes na BD em memória, popular a choicebox dos
     * niveis de categorias e carregar os listeners para validação dos atributos
     * da categoria a serem inseridos pelo utilizador<br>
     * Initializes the controller class.<br>
     *
     * @param url : url - não usado
     * @param rb : rb : não usado
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            adicionarEcraPessoalOperadorMenuOpcoes();
            listaCategoriasProduto = listarCategoriasProdutoTotais();
            atribuirNiveisCBNivel(choiceNivel);
            validarCampos();
        } catch (Exception e) {
            mensagemErroFatal();
            Logger.getLogger(inserirCategoriasController.class.getName()).log(Level.SEVERE, null, e);
        }

    }

    /**
     * Metodo para terminar sessao do utilizador<br>
     *
     * @param event evento
     */
    @FXML
    private void terminarSessao(ActionEvent event) {
        try {
            login.setIdLogin(0);
            login.setNivelLogin(10000);
            login log = new login();
            fecha();
            log.start(new Stage());

        } catch (Exception ex) {
            Logger.getLogger(alterarProdutosController.class.getName()).log(Level.SEVERE, null, ex);
            mensagemErroFatal();
        }
    }

    /**
     * Método para encerrar a aplicação
     */
    @FXML
    private void sair(ActionEvent event) {
        helper.opcaoSair(event);
    }

    /**
     * Método para aceder ao menu do operador
     */
    @FXML
    private void openMenu(ActionEvent event) {
        try {
            if (login.getNivelLogin() == 1) {
                menuOperador menuO = new menuOperador();
                fecha();
                menuO.start(new Stage());
            } else {
                menuAdministrador menuAdmin = new menuAdministrador();
                fecha();
                menuAdmin.start(new Stage());
            }
        } catch (Exception ex) {
            Logger.getLogger(alterarProdutosController.class.getName()).log(Level.SEVERE, null, ex);
            mensagemErroFatal();
        }
    }

    /**
     * Método para inserir a nova categoria na BD<br>
     * Método primeiro verifica se os campos do nome e da descrição estão
     * vazidos. Depois, valida a lógica do nome (se é único) e de seguida
     * verifica a descrição (se é única ou não). De seguida, cria uma nova
     * categoria usando os paramêtros inseridos pelo utlizador mais o id e o
     * estado definidos pela aplicação. A seguir valida a nova categoria e
     * insere-a na BD. Por fim, verifica se esta foi inserida com efeito na BD e
     * notifica o utilizador, ao qual se é dada a possibilidade de inserir nova
     * categoria ou regressar ao seu menu<br>
     *
     * @param event evento
     */
    @FXML
    private void inserirCategoriasBD(ActionEvent event) {
        if (isNotEmptyInvalidCatNomeDescricaoNivel(nome.getText(), descricao.getText(), choiceNivel.getValue()) == true) {
            if (validarNome(nome.getText(), listaCategoriasProduto, ESTADOCATEGORIA) == true) {
                if (validarDescricao(descricao.getText(), listaCategoriasProduto) == true) {
                    categoria novaCat = gerarCategoria(nome.getText(), descricao.getText(),
                            Integer.parseInt(choiceNivel.getValue()), ESTADOCATEGORIA);
                    if (isValidCategoriaNova(novaCat) == true) {
                        inserirCategoria(novaCat.getNomeCategoria(),
                                novaCat.getDescricaoCategoria(), novaCat.getNivelCategoria(), novaCat.getEstado());
                        if (wasEffetiveInsertCatBD(novaCat) == true) {
                            mensagemSucessoCategoria(novaCat);
                        } else {
                            mensagemErroRecarregarEcra();
                        }
                    } else {
                        mensagemErroRecarregarEcra();
                    }
                } else {
                    recarregarEcra();
                }
            } else {
                recarregarEcra();
            }
        } else {
            mensagemErroRecarregarEcraCamposVaziosInvalidos();
        }
    }

    /**
     * Método que executa validações aos campos manipuláveis pelo o utilizador.
     * <br>
     * Método usa listeners para verificar se o nome e a descrição da nova
     * categoria assumem ou não valores válidos. Validações de lógica de negócio
     * e de campos vazios são realizadas em métodos à parte.
     */
    private void validarCampos() {
        try {
            nome.focusedProperty()
                    .addListener((arg0, oldValue, newValue) -> {
                        try {
                            if (!newValue) {
                                try {
                                    if (!nome.getText().equals("") && Pattern.compile("^\\s*[A-Za-z0-9]+(?:\\s+[A-Za-z0-9]+)*$").matcher(nome.getText()).find() == false
                                            || nome.getText().length() > 60) {
                                        a.setAlertType(Alert.AlertType.WARNING);
                                        a.getButtonTypes().setAll(ButtonType.OK);
                                        a.setTitle("Nome da Categoria Do Produto Inválida");
                                        a.setHeaderText("Formato do nome da nova categoria de produto inválido");
                                        a.setContentText("Insira um nome válido para a nova categoria de produto");
                                        a.show();
                                        nome.setText("");
                                    }
                                } catch (IllegalArgumentException pi) {
                                    Logger.getLogger(inserirCategoriasController.class.getName()).log(Level.SEVERE, null, pi);
                                    mensagemErroRecarregarEcra();
                                }
                            }
                        } catch (Exception e) {
                            nome.setText("placeholder");
                            Logger.getLogger(inserirCategoriasController.class.getName()).log(Level.SEVERE, null, e);
                            mensagemErroRecarregarEcra();
                        }
                    }
                    );

            descricao.focusedProperty()
                    .addListener((arg0, oldValue, newValue) -> {
                        try {
                            if (!newValue) {
                                try {
                                    if (!descricao.getText().equals("") && Pattern.compile("[\\p{L}\\p{Nd}]*").
                                            matcher(descricao.getText()).find() == false || descricao.getText().length() > 100) {
                                        a.setAlertType(Alert.AlertType.WARNING);
                                        a.getButtonTypes().setAll(ButtonType.OK);
                                        a.setTitle("Descrição da Categoria De Produto Inválida");
                                        a.setHeaderText("Formato da descrição da nova categoria do produto Inválido");
                                        a.setContentText("Insira uma descrição válida para a nova categoria de produto");
                                        a.show();
                                        descricao.setText("");
                                    }
                                } catch (Exception pi) {
                                    Logger.getLogger(inserirCategoriasController.class.getName()).log(Level.SEVERE, null, pi);
                                    mensagemErroRecarregarEcra();
                                }
                            }
                        } catch (Exception e) {
                            descricao.setText("");
                            Logger.getLogger(inserirCategoriasController.class.getName()).log(Level.SEVERE, null, e);
                            mensagemErroRecarregarEcra();
                        }
                    }
                    );

            choiceNivel.focusedProperty()
                    .addListener((arg0, oldValue, newValue) -> {
                        try {
                            if (!newValue) {
                                try {
                                    int nivel = 0;
                                    try {
                                        nivel = Integer.parseInt(choiceNivel.getValue());
                                    } catch (NumberFormatException ab) {
                                        choiceNivel.setValue("3");
                                        mensagemErroRecarregarEcra();
                                    }
                                    if (!choiceNivel.getValue().equals("") && (nivel < 1 || nivel > 3)) {
                                        a.setAlertType(Alert.AlertType.WARNING);
                                        a.getButtonTypes().setAll(ButtonType.OK);
                                        a.setTitle("Nível de Categoria Inválido");
                                        a.setHeaderText("O nível da categoria assumiu um valor inválido");
                                        a.setContentText("A categoria só pode assumir um de 3 níveis: 1, 2 ou 3.\nSe desejar um nível novo,"
                                                + "contacte o administrador");
                                        a.show();
                                        choiceNivel.setValue("3");
                                    }
                                } catch (Exception pi) {
                                    Logger.getLogger(inserirCategoriasController.class.getName()).log(Level.SEVERE, null, pi);
                                    mensagemErroRecarregarEcra();
                                }
                            }
                        } catch (Exception e) {
                            choiceNivel.setValue("3");
                            Logger.getLogger(inserirCategoriasController.class.getName()).log(Level.SEVERE, null, e);
                            mensagemErroRecarregarEcra();
                        }
                    }
                    );

        } catch (Exception podeEstourar) {
            Logger.getLogger(inserirCategoriasController.class.getName()).log(Level.SEVERE, null, podeEstourar);
            mensagemErroRecarregarEcra();
        }
    }

    /**
     * Método para enviar o utilizador para o seu menu em caso de falha da
     * aplicação <br>
     *
     * Método deteta o tipo de utilizador consonante o seu nível e reencaminha
     * para o seu menu. Em caso de falha, fecha a aplicação. Este método,
     * semelhante ao método do botão menu, é invocado quando o programa
     * encontrar uma falha na lógica da aplicação (como aquando da atualização
     * do produto) para permitir o utilizador continuar a usar outras
     * funcionalidades da aplicação.
     */
    private static void abrirMenUtilizador() {
        if (login.getNivelLogin() == 1) { //se for operador ir para o menu do operador
            menuOperador menuOp = new menuOperador();
            fecha();
            try {
                menuOp.start(new Stage());
            } catch (Exception ex) {
                mensagemErroFatal();
            }

        } else if (login.getNivelLogin() == 2) {
            menuAdministrador menuAdmin = new menuAdministrador();
            fecha();
            try {
                menuAdmin.start(new Stage());
            } catch (Exception ex) {
                mensagemErroFatal();
            }
        } else {
            mensagemErroFatal();
        }
    }

    /**
     * Método para fechar o ecrã
     */
    private static void fecha() {
        inserirCategorias.getStage().close();
    }

    /**
     * método para substituir o conteudo do menu de opções para colocar as
     * opções por ordem caso seja o operador a abrir o ecrã.
     */
    private void adicionarEcraPessoalOperadorMenuOpcoes() {

        List<utilizador> operadores = procurarEmail(login.getIdLogin());
        for (utilizador operador : operadores) {
            if (operador.getNivel() == 1) {
                opcoes.getItems().clear();
                MenuItem ePOp = new MenuItem("Ecrã Pessoal");
                MenuItem terminarSessao = new MenuItem("Terminar Sessão");
                MenuItem sair = new MenuItem("Sair");
                opcoes.getItems().add(ePOp);
                opcoes.getItems().add(terminarSessao);
                opcoes.getItems().add(sair);
                ePOp.setOnAction((ActionEvent e) -> {
                    try {
                        ecraPessoalOperador ePO = new ecraPessoalOperador();
                        fecha();
                        ePO.start(new Stage());
                    } catch (Exception ex) {
                        mensagemErroFatal();
                    }
                });
                terminarSessao.setOnAction((ActionEvent e) -> {
                    try {
                        login.setIdLogin(0);
                        login.setNivelLogin(10000);
                        login log = new login();
                        fecha();
                        log.start(new Stage());
                    } catch (Exception ex) {
                        mensagemErroFatal();
                    }
                });
                sair.setOnAction((ActionEvent e) -> {
                    System.exit(0);
                });
            }
        }
    }

    /**
     * Método para carregar todas as categorias existentes na BD e guardá-las
     * numa lista de categorias<br>
     * Método gera uma lista com as categorias existentes na BD. C aso não
     * existam categorias, informa o utilizador disso e caso algo corra mal, faz
     * o mesmo.<br>
     *
     * @return : retorna a lista de categorias existentes na BD
     */
    public static List<categoria> listarCategoriasProdutoTotais() {
        List<categoria> categorias = new ArrayList<>();
        try {
            categorias = listarCategorias();
            if (!categorias.isEmpty()) {
            } else if (categorias.isEmpty()) {
                mensagemAusenciaCategorias();
            } else {
                mensagemErroFatal();
            }
        } catch (Exception e) {
            mensagemErroFatal();
        }
        return categorias;
    }

    /**
     * Mensagem de erro para tentar fechar em segurança a aplicação no caso
     * desta encontrar um erro muito grave<br>
     * Se este erro for lançado é porque algo inesperado ocorreu (ex: falha de
     * acesso à base de dados, lançamento de alguns excepções em métodos
     * críticos ou falhas gerais e básicas de programação)<br>
     * É a mensagem de erro de severidade mais elevada deste ecrã
     */
    public static void mensagemErroFatal() {
        a.setAlertType(Alert.AlertType.ERROR);
        a.setTitle("ERRO Fatal");
        a.setHeaderText("A aplicação comportou-se de forma inesperada!");
        a.setContentText("Encerrando a aplicação");
        a.getButtonTypes().setAll(ButtonType.OK);
        Optional<ButtonType> result3 = a.showAndWait();
        if (result3.get() == ButtonType.OK) {
            System.exit(0);
        }
    }

    /**
     * Método para lançar uma mensagem de erro caso um erro caso algo essencial
     * falhe na funcionalidade<br>
     * Se este erro é lançado é porque a funcionalidade pode estar inoperável
     * temporaria ou permanentemente<br>
     * É mensagem de erro de severidade intermédia do ecrã
     */
    public static void mensagemErroPassagemMenUtilizador() {
        a.setAlertType(Alert.AlertType.ERROR);
        a.setTitle("ERRO Fatal");
        a.setHeaderText("Impossível validar os dados do ecrã!");
        a.setContentText("Algumas funcionalidades do aplicação estão inacessíveis");
        a.getButtonTypes().setAll(ButtonType.OK);
        Optional<ButtonType> result3 = a.showAndWait();
        if (result3.get() == ButtonType.OK) {
            abrirMenUtilizador();
        }
    }

    /**
     * Método para lançar uam mensagem de erro e recarregar o ecrã caso algo
     * inesperado ocorra com o algoritmo<br>
     * Método usado quando as falhas são graves mas não graves de mais que a
     * funcionalidade se encontre inutilizável.<br>
     * É uma das duas mensagens de erro de severidade mais leve neste ecrã
     */
    public static void mensagemErroRecarregarEcra() {
        Alert alertaNovo = new Alert(Alert.AlertType.NONE);
        alertaNovo.setAlertType(Alert.AlertType.ERROR);
        alertaNovo.getButtonTypes().setAll(ButtonType.OK);
        alertaNovo.setTitle("ERRO Lógica Aplicação");
        alertaNovo.setHeaderText("Os dados processados não são válidos");
        alertaNovo.setContentText("Ocorreu um erro ao validar os dados selecionados ou estes podem estar inacessíveis."
                + "\nTente novamente e caso não conseguir contacte o administrador");
        Optional<ButtonType> resulta = alertaNovo.showAndWait();
        if (resulta.get() == ButtonType.OK) {
            recarregarEcra();
        }

    }

    /**
     * Método para retornar o id de uma categoria através da correspondência com
     * o nome<br>
     * O método recebe o nome da categoria a pesquisar e a lista de categorias
     * da BD. Depois procura na lista por uma categoria com o nome escolhido e
     * retorna o id da categoria caso faça correspondência. Caso não encontre,
     * vai lançar uma mensagem de erro para levar o utilizador para o seu
     * menu<br>
     *
     * @param nomeCategoria : nome da categoria em pesquisa
     * @param categorias : lista de categorias na BD
     * @return : um String com o id da categoria
     */
    public static int retornarIDCategoria(String nomeCategoria, List<categoria> categorias) {
        int idCategoria = 0;
        for (categoria categoria : categorias) {
            if (nomeCategoria.equals(categoria.getNomeCategoria())) {
                idCategoria = categoria.getIdCategoria();
            }
        }
        if (idCategoria != 0) {
        } else {
            mensagemErroPassagemMenUtilizador();
        }
        return idCategoria;
    }

    /**
     * Método para recarregar o ecrã de inserir categorias
     */
    private static void recarregarEcra() {
        inserirCategorias insCat = new inserirCategorias();
        fecha();
        try {
            insCat.start(new Stage());
        } catch (Exception ex) {
            mensagemErroFatal();
        }
    }

    /**
     * Método para verificar se o nome da categoria é unica e se pretence a uma
     * categoria inativa<br>
     * Método recebe o nome da nova categoria e a lista de categorias na BD.
     * Depois verifica se o nome é único e se pretence a uma categoria
     * inativa.<br>
     * Caso o nome não seja único e a categoria esteja inativa, o método retorna
     * falso. Caso o nome seja único, o método retorna verdadeiro. Caso o nome
     * não seja único e a categoria esteja inativa, o método pergunta ao
     * utilizador se este pretende ativar a categoria. Em caso negativo, o
     * método retorna falso mas em caso positivo, apesar de nominalmente o
     * método retornar negativo (nota: retornar negativo deve levar ao fecho da
     * aplicação na validação seguinte), no fundo ele vai redirecionar o
     * utilizador para o ecrâ de alterar categorias, não sem antes fornecer o id
     * da categoria a ativar ao utilizador<br>
     *
     * @param nomeCategoria : nome da nova categoria
     * @param categorias : lista de categorias na BD
     * @param estado : estado da nova categoria
     * @return : retorna verdadeiro caso o nome seja único e falso caso não seja
     */
    public static boolean validarNome(String nomeCategoria, List<categoria> categorias, String estado) {
        Alert alertaNovo = new Alert(Alert.AlertType.NONE);
        int apontadorNome = 0;
        boolean nomeOK = false;
        try {
            if (alterarProdutosController.isUniqueNomeCategoria(nomeCategoria, categorias) == false
                    && !nomeCategoria.equals("") && apontadorNome == 0
                    && alterarProdutosController.isAtiveCategoria(nomeCategoria, categorias, estado) == true) {
                apontadorNome = 1;
                alertaNovo.setAlertType(Alert.AlertType.WARNING);
                alertaNovo.getButtonTypes().setAll(ButtonType.OK);
                alertaNovo.setTitle("Nome da Categoria do Produto Já Atribuído");
                alertaNovo.setHeaderText("O nome de categoria inserido já se encontra atribuído a outra categoria");
                alertaNovo.setContentText("Insira um nome único para a nova categoria do produto");
                Optional<ButtonType> resulta = alertaNovo.showAndWait();
                if (resulta.get() == ButtonType.OK) {
                }
            } else if (alterarProdutosController.isUniqueNomeCategoria(nomeCategoria, categorias) == false
                    && !nomeCategoria.equals("") && apontadorNome == 0
                    && alterarProdutosController.isAtiveCategoria(nomeCategoria, categorias, estado) == false) {
                apontadorNome = 1;
                alertaNovo.setAlertType(Alert.AlertType.CONFIRMATION);
                alertaNovo.getButtonTypes().setAll(ButtonType.YES, ButtonType.NO);
                alertaNovo.setTitle("Nome de Categoria do Produto Inativa");
                alertaNovo.setHeaderText("O nome inserido pertence à categoria "
                        + retornarIDCategoria(nomeCategoria, categorias) + " que se encontra inativa");
                alertaNovo.setContentText("Deseja ativá-la?");
                Optional<ButtonType> resultb = alertaNovo.showAndWait();
                if (resultb.get() == ButtonType.YES) {
                    if (retornarIDCategoria(nomeCategoria, categorias) != 0) {
                        abrirEcraAlterarCategorias();
                    } else {
                        mensagemErroPassagemMenUtilizador();
                    }
                } else if (resultb.get() == ButtonType.NO) {
                }
            } else if (alterarProdutosController.isUniqueNomeCategoria(nomeCategoria, categorias) == true
                    && !nomeCategoria.equals("") && apontadorNome == 0) {
                apontadorNome = 2;
            }
        } catch (Exception validarNomeBotao) {
            Logger.getLogger(inserirCategoriasController.class.getName()).log(Level.SEVERE, null, validarNomeBotao);
            nomeOK = false;
            mensagemErroPassagemMenUtilizador();
        }

        switch (apontadorNome) {
            case 2:
                nomeOK = true;
                break;
            case 1:
                nomeOK = false;
                break;
            default:
                mensagemErroFatal();
                break;
        }

        return nomeOK;
    }

    /**
     * Método para verificar se a descricao é única ou não por forma a notificar
     * o utilizador<br>
     * Método recebe a descrição da nova categoria e a lista de categorias na
     * BD. Depois verifica se a descrição é única. Caso não seja, não faz mais
     * nada além de retornar verdadeiro. Por outro lado, caso a descrição não
     * seja única, o método pergunta ao utilizador se este a pretende manter. Em
     * caso afirmativo, retorna verdadeiro e em caso negativo (ou se algo correr
     * mal) retorna falso<br>
     *
     * @param descricaoCategoria : descrição da nova categoria
     * @param categorias : lista de categorias na BD
     * @return : retorna verdadeiro caso a descrição não seja única ou caso o
     * utilizador queira manter a descrição não original e retorna falso caso
     * não pretenda manter a descrição repetida
     */
    public static boolean validarDescricao(String descricaoCategoria, List<categoria> categorias) {
        Alert a = new Alert(Alert.AlertType.NONE);
        int apontadorDescricao = 0;
        boolean descricaoOK = false;
        try {
            if (alterarProdutosController.isUniqueDescricaoCategoria(descricaoCategoria, categorias) == false
                    && !descricaoCategoria.equals("")) {
                a.setAlertType(Alert.AlertType.CONFIRMATION);
                a.getButtonTypes().setAll(ButtonType.YES, ButtonType.NO);
                a.setTitle("Descrição da Categoria do Produto Inválida");
                a.setHeaderText("A descrição da categoria inserida já se encontra atribuída a outra categoria");
                a.setContentText("Pode usar esta descrição mas não se confunda com as categorias produtos mais tarde\nDeseja manter a descrição?");
                Optional<ButtonType> result3 = a.showAndWait();
                if (result3.get() == ButtonType.YES) {
                    apontadorDescricao = 2;
                } else if (result3.get() == ButtonType.NO) {
                    apontadorDescricao = 1;
                } else {
                    apontadorDescricao = 0;
                }
            } else if (alterarProdutosController.isUniqueDescricaoCategoria(descricaoCategoria, categorias) == true
                    && !descricaoCategoria.equals("")) {
                apontadorDescricao = 2;
            }
        } catch (Exception validarDescricao) {
            Logger.getLogger(inserirCategoriasController.class.getName()).log(Level.SEVERE, null, validarDescricao);
            descricaoOK = false;
            mensagemErroPassagemMenUtilizador();
        }

        switch (apontadorDescricao) {
            case 2:
                descricaoOK = true;
                break;
            case 1:
                descricaoOK = false;
                break;
            default:
                mensagemErroFatal();
                break;
        }

        return descricaoOK;
    }

    /**
     * Método para construir um objeto categoria com os atributos selecionados
     * pelo utilizador<br>
     * Método recebe o nome, a descrição e o nível da nova categoria
     * selecionados pelo utilizador mais o estado selecionado pela aplicação. De
     * seguida, associa estes parametros a um objeto categoria novo. Por fim,
     * retorna a categoria nova.<br>
     *
     * @param nomeCategoria : nome da nova categoria
     * @param descricaoCategoria : descrição da nova categoria
     * @param nivelCategoria : nivel da categoria
     * @param estadoCategoria : estado da nova categoria
     * @return : um objeto categoria referente à nova categoria a inserir na BD
     */
    public static categoria gerarCategoria(String nomeCategoria, String descricaoCategoria, int nivelCategoria, String estadoCategoria) {
        categoria categoriaNova = new categoria();
        try {
            categoriaNova.setNomeCategoria(nomeCategoria);
            categoriaNova.setDescricaoCategoria(descricaoCategoria);
            categoriaNova.setNivelCategoria(nivelCategoria);
            categoriaNova.setEstado(estadoCategoria);
        } catch (Exception gerarCategoria) {
            Logger.getLogger(inserirCategoriasController.class.getName()).log(Level.SEVERE, null, gerarCategoria);
        }

        return categoriaNova;
    }

    /**
     * Método para validar os paramêtros da nova categoria<br>
     * Método recebe a categoria nova e verifica se os atributos são vazios ou
     * nulos<br>
     *
     * @param categoriaNova : objeto categoria nova criada com os atributos
     * inseridos pelo utilizador mais os gerados pela aplicação
     * @return : retorna verdadeiro caso a categoria não atributos vazios ou
     * nulos e falso caso tenha
     */
    public static boolean isValidCategoriaNova(categoria categoriaNova) {
        return !(categoriaNova.getNomeCategoria().equals("") || categoriaNova.getDescricaoCategoria().equals("")
                || categoriaNova.getEstado().equals("")
                || categoriaNova.getNomeCategoria() == null || categoriaNova.getDescricaoCategoria() == null
                || categoriaNova.getEstado() == null);
    }

    /**
     * Método para verificar se a nova categoria foi inserida na BD<br>
     * Método recebe a categoria nova, procura por ela na BD e retorna
     * verdadeiro caso exista uma categoria com os mesmos atributos da categoria
     * nova<br>
     *
     * @param novaCategoria :categoria inserida para leitura do id e do nome
     * @return : retorna verdadeiro caso a nova categoria se encontre na bd
     */
    public static boolean wasEffetiveInsertCatBD(categoria novaCategoria) {
        boolean insertOK = false;
        List<categoria> categoriaRegistada = listarCategoriaAtributos(novaCategoria.getNomeCategoria(),
                novaCategoria.getDescricaoCategoria(), novaCategoria.getNivelCategoria(), novaCategoria.getEstado());
        if (categoriaRegistada.size() == 1) {
            for (categoria categoria : categoriaRegistada) {
                insertOK = categoria.getNomeCategoria().equals(novaCategoria.getNomeCategoria())
                        && categoria.getDescricaoCategoria().equals(novaCategoria.getDescricaoCategoria())
                        && categoria.getNivelCategoria() == novaCategoria.getNivelCategoria()
                        && categoria.getEstado().equals(novaCategoria.getEstado());
            }
        } else {
            insertOK = false;
        }

        return insertOK;
    }

    /**
     * Método para notificar o utilizador do sucesso da inserção de uma
     * categoria e requisitir uma nova acção<br>
     * Método informa o utilizador do sucesso da operação e pergunta se o
     * utilizador pretende inserir uma nova categoria ou não. Em caso
     * afirmativo, recarrega o ecrã mas em caso negativo envia o utilizador para
     * o menu do utilizador<br>
     *
     * @param categoria : categoria inserida para leitura do id e do nome
     */
    private static void mensagemSucessoCategoria(categoria categoria) {
        Alert alerta = new Alert(Alert.AlertType.NONE);
        alerta.setAlertType(Alert.AlertType.INFORMATION);
        alerta.getButtonTypes().setAll(ButtonType.YES, ButtonType.NO);
        alerta.setTitle("Inserir Categorias");
        alerta.setHeaderText("Categoria inserida com sucesso!");
        alerta.setContentText("A categoria " + categoria.getNomeCategoria() + " foi inserida com sucesso!"
                + "\nDeseja inserir outra categoria?");
        alerta.setResizable(false);
        Optional<ButtonType> resultA = alerta.showAndWait();
        if (resultA.get() == ButtonType.YES) {
            recarregarEcra();
        } else if (resultA.get() == ButtonType.NO) {
            abrirMenUtilizador();
        } else {
            mensagemErroFatal();
        }
    }

    /**
     * Método para redirecionar o utilizador para o ecrã alterarCategorias caso
     * este pretenda ativar uma categoria após tentar inserir uma categoria com
     * o nome de uma categoria inativa
     */
    private static void abrirEcraAlterarCategorias() {
        try {
            alterarCategorias altCat = new alterarCategorias();
            fecha();
            altCat.start(new Stage());
        } catch (Exception ex) {
            mensagemErroFatal();
        }
    }

    /**
     * Método para validar se os campos manipulados pelo utilizador contêm dados
     * ou não<br>
     * Complemento às validações por listeners<br>
     * Método recebe os paramentros das textareas do nome e da descricao assim
     * como o valor da choicebox do nivel. Verifica se é possível converter o
     * String do nivel para int e em caso afirmativo verifica se os campos estão
     * vazios e se o nivel está entre 1 e 3.<br>
     * O método retorna verdadeiro caso as textareas ambos não estejam vazias e
     * o nivel esteja entre 1 e 3 e falso caso uma das textarareas esteja vazia
     * ou o nível diferente de 1,2 ou 3.<br>
     *
     * @param nome : nome da nova categoria
     * @param descricao : descricao da nova categoria
     * @param nivel : nivel da categoria
     * @return : verdadeiro caso os campos não estejam vazios e o nível entre 1
     * e 3 e falso caso um ou ambos os campos estejam vazios e o nível diferente
     * de 1,2 ou 3.
     */
    public static boolean isNotEmptyInvalidCatNomeDescricaoNivel(String nome, String descricao, String nivel) {
        boolean nomeDescricaoNivelOK = false;
        boolean nivelOK = false;
        int nivelCat = -1;
        try {
            nivelCat = Integer.parseInt(nivel);
            nivelOK = true;
        } catch (NumberFormatException abcdef) {
            nivelOK = false;
        } catch (Exception bc) {
            nivelOK = false;
        }

        if (nivelOK == true) {
            try {
                nomeDescricaoNivelOK = !(nome.equals("") || descricao.equals("") || nivelCat < 1 || nivelCat > 3);
            } catch (Exception erroNullPointerAquiTalvez) {
                nomeDescricaoNivelOK = false;
            }

        } else {
            nomeDescricaoNivelOK = false;
        }

        return nomeDescricaoNivelOK;
    }

    /**
     * Método para lançar uam mensagem de erro e recarregar o ecrã caso o
     * utilizador deixe campos em brancos <br>
     * Método derivado do mensagemErroRecarregarEcra para ser chamado apenas no
     * começo do processo de inserção de categoria nova<br>
     * É uma das duas mensagens de erro de severidade mais leve neste ecrã
     */
    private static void mensagemErroRecarregarEcraCamposVaziosInvalidos() {
        Alert alertaNovo = new Alert(Alert.AlertType.NONE);
        alertaNovo.setAlertType(Alert.AlertType.ERROR);
        alertaNovo.getButtonTypes().setAll(ButtonType.OK);
        alertaNovo.setTitle("ERRO Lógica Aplicação");
        alertaNovo.setHeaderText("Os dados processados não são válidos");
        alertaNovo.setContentText("Tem de atribuir um nome, uma descrição e um nível válidos à nova categoria");
        Optional<ButtonType> resulta = alertaNovo.showAndWait();
        if (resulta.get() == ButtonType.OK) {
            recarregarEcra();
        }
    }

    /**
     * Método para atribuir valores à choicebox dos niveis de categorias.<br>
     * Método recebe a choicebox e popula-a com os valores de nível existentes
     * na BD no presente e retorna a choicebox já com os valores
     * adicionados.<br>
     * Se algo correr mal, informa o utilizador.<br>
     *
     * @param choiceNivel : choibebox dos níveis de categorias
     * @return : retorna a choicebox preenchida
     */
    public static ChoiceBox<String> atribuirNiveisCBNivel(ChoiceBox<String> choiceNivel) {
        try {
            choiceNivel.getItems().add("1");
            choiceNivel.getItems().add("2");
            choiceNivel.getItems().add("3");
            choiceNivel.setValue("3");
        } catch (Exception atribuirNiveisCBNivelFalhou) {

            mensagemErroFatal();
        }
        return choiceNivel;
    }

    /**
     * Mensagem para informar o utilizador que a lista de categorias está
     * vaiza
     */
    public static void mensagemAusenciaCategorias() {
        Alert alertaNovo = new Alert(Alert.AlertType.NONE);
        alertaNovo.setAlertType(Alert.AlertType.INFORMATION);
        alertaNovo.setTitle("Lista Categorias");
        alertaNovo.setHeaderText("Não existem categorias de produto na base de dados!");
        alertaNovo.setContentText("Adicione as categorias que achar necessário");
        alertaNovo.getButtonTypes().setAll(ButtonType.OK);
        Optional<ButtonType> result3 = alertaNovo.showAndWait();
        if (result3.get() == ButtonType.OK) {
        }
    }

}
