/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bfgir;

import classes.helper;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * classe do ecrã menuAdministrador
 */
public class menuAdministrador extends Application {

    private static Stage stage;

    @Override
    public void start(Stage stage) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/ecras/menuAdmin.fxml"));

            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.setTitle("F&O - Menu Admin");
            stage.getIcons().add(helper.appIcon);
            stage.setResizable(false);
            stage.show();
            setStage(stage);
        } catch (IOException ex) {
            Logger.getLogger(menuAdministrador.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * método main do ecrã menuAdministrador
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * método getStage do ecrã menuAdministrador
     *
     * @return retorna o JAVAFX container do ecrã menuAdministrador
     */
    public static Stage getStage() {
        return stage;
    }

    /**
     * método setStage do ecrã menuAdministrador
     *
     * @param aStage the aStage to set
     */
    public static void setStage(Stage aStage) {
        stage = aStage;
    }
}
