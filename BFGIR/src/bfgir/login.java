/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bfgir;

import classes.helper;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * classe do ecrã login
 */
public class login extends Application {

    private static Stage stage;

    private static int nivelLogin = 100000;

    private static int idLogin = 0;

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/ecras/login.fxml"));

        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.setTitle("Feira & Office");
        stage.getIcons().add(helper.appIcon);
        stage.setResizable(false);
        stage.setHeight(600);
        stage.show();
        setStage(stage);
    }

    /**
     * método main do ecrã login
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * método getStage do ecrã login
     *
     * @return retorna o JAVAFX container do ecrãlogin
     */
    public static Stage getStage() {
        return stage;
    }

    /**
     * método setStage do ecrã login
     *
     * @param aStage the aStage to set
     */
    public static void setStage(Stage aStage) {
        stage = aStage;
    }

    /**
     * método getNivelLogin para se obter o nível do utilizador para efeitos de
     * autenticação
     *
     * @return the nivelLogin
     */
    public static int getNivelLogin() {
        return nivelLogin;
    }

    /**
     * método setNivelLogin para se estabelecer o nível do utilizador para
     * efeitos de autenticação
     *
     * @param aNivelLogin the nivelLogin to set
     */
    public static void setNivelLogin(int aNivelLogin) {
        nivelLogin = aNivelLogin;
    }

    /**
     * método getIdLogin para se obter o id do utilizador para efeitos de
     * autenticação
     *
     * @return the idLogin
     */
    public static int getIdLogin() {
        return idLogin;
    }

    /**
     * método etIdLogin para se estabelecer o id do utilizador para efeitos de
     * autenticação
     *
     * @param aIdLogin the idLogin to set
     */
    public static void setIdLogin(int aIdLogin) {
        idLogin = aIdLogin;
    }

}
