/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.service;

import entities.Categoria;
import entities.Clienteproduto;
import entities.ClienteprodutoPK;
import entities.Descontos;
import entities.Descontosprodutos;
import entities.Produto;
import entities.Produtosubcategoria;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Ruben
 */
@Stateless
@Path("/product/")
public class ProdutoFacadeREST extends AbstractFacade<Produto> {

    @PersistenceContext(unitName = "feiraofficePU")
    private EntityManager em;

    /**
     *
     */
    public ProdutoFacadeREST() {
        super(Produto.class);
    }

    /**
     *
     * @param entity recebe uma entidade do tipo produto
     */
    @POST
    @Override
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void create(Produto entity) {
        super.create(entity);
    }

    /**
     *
     * @param id recebe um id de um produto
     * @param entity recebe uma entidade do tipo produto
     */
    @PUT
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void edit(@PathParam("id") String id, Produto entity) {
        super.edit(entity);
    }

    /**
     *
     * @param id recebe um id de um produto
     */
    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") String id) {
        super.remove(super.find(id));
    }

    /**
     *
     * @return uma lista de objetos do tipo produto
     */
    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Produto> findEstado() {

        List<Produto> prodsReturn = new ArrayList<>();
        String estado = "ativo";

        Query query = getEntityManager().createNamedQuery("Produto.findByEstado");
        query.setParameter("estado", estado);
        List<Produto> produtos = query.getResultList();
        for (Produto prods : produtos) {

            Produto prod = new Produto();
            prod.setCodProdPa(prods.getCodProdPa());
            prod.setDescricao(prods.getDescricao());
            prod.setQuantStock(prods.getQuantStock());
            prod.setTipoQuantStock(prods.getTipoQuantStock());
            prod.setQuantStockUnit(prods.getQuantStockUnit());
            prod.setTipoQuantStockUnit(prods.getTipoQuantStockUnit());
            prod.setPrecoUnitSIVA(Math.round(prods.getPrecoUnitSIVA() * 100.0) / 100.0);
            prod.setIdCategoria(prods.getIdCategoria());
            prod.setEstado(prods.getEstado());

            prodsReturn.add(prod);
        }

        return prodsReturn;
    }

    /**
     *
     * @param id recebe um id de um produto
     * @return um objeto do tipo produto
     */
    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Produto find(@PathParam("id") String id) {

        String idProd = id;
        Produto prod = new Produto();

        Query query = getEntityManager().createNamedQuery("Produto.findByCodProdPa");
        query.setParameter("codProdPa", idProd);
        List<Produto> produtos = query.getResultList();
        for (Produto prods : produtos) {

            prod.setCodProdPa(prods.getCodProdPa());
            prod.setDescricao(prods.getDescricao());
            prod.setQuantStock(prods.getQuantStock());
            prod.setTipoQuantStock(prods.getTipoQuantStock());
            prod.setQuantStockUnit(prods.getQuantStockUnit());
            prod.setTipoQuantStockUnit(prods.getTipoQuantStockUnit());
            prod.setPrecoUnitSIVA(Math.round(prods.getPrecoUnitSIVA() * 100.0) / 100.0);
            prod.setIdCategoria(prods.getIdCategoria());
            prod.setEstado(prods.getEstado());

        }

        return prod;

    }

    /**
     *
     * @param id recebe um id de um produto
     * @return uma lista de objetod do tipo produto
     */
    @GET
    @Path("/_type/{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Produto> findCat(@PathParam("id") int id) {

        List<Produto> prodsReturn = new ArrayList<Produto>();
        int idCat = id;

        Query query = getEntityManager().createNamedQuery("Produto.findByIdCategoria");
        query.setParameter("idCategoria", idCat);
        List<Produto> produtos = query.getResultList();
        for (Produto prods : produtos) {

            Produto prod = new Produto();

            prod.setCodProdPa(prods.getCodProdPa());
            prod.setDescricao(prods.getDescricao());
            prod.setQuantStock(prods.getQuantStock());
            prod.setTipoQuantStock(prods.getTipoQuantStock());
            prod.setQuantStockUnit(prods.getQuantStockUnit());
            prod.setTipoQuantStockUnit(prods.getTipoQuantStockUnit());
            prod.setPrecoUnitSIVA(Math.round(prods.getPrecoUnitSIVA() * 100.0) / 100.0);
            prod.setIdCategoria(prods.getIdCategoria());
            prod.setEstado(prods.getEstado());

            prodsReturn.add(prod);
        }

        return prodsReturn;

    }

    /**
     *
     * @param id recebe um id de um produto
     * @return uma lista de objetod do tipo clienteproduto
     */
    @GET
    @Path("/{id}/price/")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Clienteproduto> findPrice(@PathParam("id") String id) {

        List<Clienteproduto> cpsReturn = new ArrayList<Clienteproduto>();
        String idProd = id;
        Clienteproduto csprs = new Clienteproduto();

        Query query = getEntityManager().createNamedQuery("Produto.findByCodProdPa");
        query.setParameter("codProdPa", idProd);
        List<Produto> produtos = query.getResultList();
        for (Produto prods : produtos) {

            double precoUnitSIVA = prods.getPrecoUnitSIVA();

            ClienteprodutoPK cppk = new ClienteprodutoPK();
            cppk.setIdCliente(0);
            cppk.setCodProdPa(prods.getCodProdPa());

            csprs.setClienteprodutoPK(cppk);
            csprs.setPrecoUnitSIVA(Math.round(precoUnitSIVA * 100.0) / 100.0);
        }

        cpsReturn.add(csprs);

        Query query2 = getEntityManager().createNamedQuery("Clienteproduto.findByCodProdPa");
        query2.setParameter("codProdPa", id);
        List<Clienteproduto> Clientesprodutos = query2.getResultList();
        for (Clienteproduto cps : Clientesprodutos) {

            Clienteproduto csprs2 = new Clienteproduto();

            ClienteprodutoPK cppk = new ClienteprodutoPK();
            cppk.setIdCliente(cps.getClienteprodutoPK().getIdCliente());
            cppk.setCodProdPa(cps.getClienteprodutoPK().getCodProdPa());

            csprs2.setClienteprodutoPK(cppk);
            csprs2.setPrecoUnitSIVA(Math.round(cps.getPrecoUnitSIVA() * 100.0) / 100.0);

            cpsReturn.add(csprs2);
        }

        return cpsReturn;
    }

    /**
     *
     * @param id recebe um id de um produto
     * @return uma lista de objetod do tipo produto
     */
    @GET
    @Path("/category/{id}/")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Produto> findProdCat(@PathParam("id") int id) {

        List<Produto> Produtos = new ArrayList<Produto>();
        int idCat = id;
        Categoria Categ = new Categoria();

        Query query = getEntityManager().createNamedQuery("Categoria.findByIdCategoria");
        query.setParameter("idCategoria", idCat);
        List<Categoria> Categorias = query.getResultList();
        for (Categoria cats : Categorias) {
            Categ.setNivelCategoria(cats.getNivelCategoria());
        }

        if (Categ.getNivelCategoria() == 1) {
            query = getEntityManager().createNamedQuery("Produtosubcategoria.findByIdCatNivel1");
            query.setParameter("idCatNivel1", idCat);
            Produtos = getProdsCat(query);

        } else if (Categ.getNivelCategoria() == 2) {
            query = getEntityManager().createNamedQuery("Produtosubcategoria.findByIdCatNivel2");
            query.setParameter("idCatNivel2", idCat);
            Produtos = getProdsCat(query);
        } else if (Categ.getNivelCategoria() == 3) {
            query = getEntityManager().createNamedQuery("Produtosubcategoria.findByIdCatNivel3");
            query.setParameter("idCatNivel3", idCat);
            Produtos = getProdsCat(query);
        }

        return Produtos;
    }

    /**
     *
     * @param id recebe um id de um produto
     * @param quant recebe um quantidade 
     * @return um objeto do tipo clienteproduto
     */
    @GET
    @Path("/{id}/price/{quant}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Clienteproduto calcPrecoProd(@PathParam("id") String id, @PathParam("quant") long quant) {

        long quantVe = quant;
        int quantVendDesc = 0;
        int idDesc = 0;

        String idProd = id;
        Query query = getEntityManager().createNamedQuery("Descontosprodutos.findByCodProdPa");
        query.setParameter("codProdPa", idProd);
        List<Descontosprodutos> descProd = query.getResultList();
        for (Descontosprodutos dp : descProd) {

            if (quantVe >= dp.getQuantVenda()) {
                if (quantVendDesc <= dp.getQuantVenda()) {
                    quantVendDesc = dp.getQuantVenda();
                    idDesc = dp.getDescontosprodutosPK().getIdDesconto();

                }
            }
        }

        double taxaDesc = 0;
        query = getEntityManager().createNamedQuery("Descontos.findByIdDesconto");
        query.setParameter("idDesconto", idDesc);
        List<Descontos> Desc = query.getResultList();
        for (Descontos d : Desc) {
            taxaDesc = d.getQuantDesconto();
        }

        double precoProd = 0;
        double iva = 0;
        query = getEntityManager().createNamedQuery("Produto.findByCodProdPa");
        query.setParameter("codProdPa", idProd);
        List<Produto> produtos = query.getResultList();
        for (Produto prods : produtos) {
            precoProd = prods.getPrecoUnitSIVA();
        }

        double precoTotalCIva = calcPrecoTotalCIVA(precoProd, taxaDesc);

        Clienteproduto csprs = new Clienteproduto();
        ClienteprodutoPK cppk = new ClienteprodutoPK();

        cppk.setIdCliente(0);
        cppk.setCodProdPa(idProd);

        csprs.setClienteprodutoPK(cppk);
        csprs.setPrecoUnitSIVA(Math.round(precoTotalCIva * 100.0) / 100.0);

        return csprs;
    }

    /**
     *
     * @return a entidade 
     */
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    /**
     *
     * @param query recebe uma query de dados
     * @return uma lista de objetos do tipo produto
     */
    public List<Produto> getProdsCat(Query query) {

        List<Produto> Produtos = new ArrayList<>();
        Produto prod = new Produto();

        List<Produtosubcategoria> prodSubCat = query.getResultList();
        for (Produtosubcategoria psc : prodSubCat) {

            query = getEntityManager().createNamedQuery("Produto.findByCodProdPa");
            query.setParameter("codProdPa", psc.getCodProdPa());
            List<Produto> produtos = query.getResultList();
            for (Produto prods : produtos) {

                prod.setCodProdPa(prods.getCodProdPa());
                prod.setDescricao(prods.getDescricao());
                prod.setQuantStock(prods.getQuantStock());
                prod.setTipoQuantStock(prods.getTipoQuantStock());
                prod.setQuantStockUnit(prods.getQuantStockUnit());
                prod.setTipoQuantStockUnit(prods.getTipoQuantStockUnit());
                prod.setPrecoUnitSIVA(Math.round(prods.getPrecoUnitSIVA() * 100.0) / 100.0);
                prod.setIdCategoria(prods.getIdCategoria());
                prod.setEstado(prods.getEstado());

            }
            Produtos.add(prod);
        }

        return Produtos;
    }

    /**
     *
     * @param precoProd recebe um preço de produto
     * @param taxaDesc recebe uma taxa de desconto
     * @return um valor do tipo double
     */
    public double calcPrecoTotalCIVA(double precoProd, double taxaDesc) {

        return ((precoProd * ((20 / (float) 100) + 1)) * ((23 / (float) 100) + 1)) / ((taxaDesc / (float) 100) + 1);
    }
}
