/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ruben
 */
public class TreeTest {



    /**
     * Test of getProduto method, of class Tree.
     */
    @Test
    public void testGetProduto() {
        System.out.println("getProduto");
        Tree instance = new Tree();

        Produto prod = new Produto();
        prod.setCodProdPa("T.123");
        prod.setDescricao("Descricao Teste");
        prod.setQuantStock(5000);
        prod.setTipoQuantStock("Teste");
        prod.setQuantStockUnit(500000);
        prod.setTipoQuantStockUnit("Teste");
        prod.setPrecoUnitSIVA(500);
        prod.setIdCategoria(1);
        prod.setEstado("Teste");

        instance.setProduto(prod);
        assertEquals(prod, instance.getProduto());

    }

    /**
     * Test of setProduto method, of class Tree.
     */
    @Test
    public void testSetProduto() {
        System.out.println("setProduto");
        Produto prod = new Produto();
        prod.setCodProdPa("T.123");
        prod.setDescricao("Descricao Teste");
        prod.setQuantStock(5000);
        prod.setTipoQuantStock("Teste");
        prod.setQuantStockUnit(500000);
        prod.setTipoQuantStockUnit("Teste");
        prod.setPrecoUnitSIVA(500);
        prod.setIdCategoria(1);
        prod.setEstado("Teste");

        Tree instance = new Tree();
        instance.setProduto(prod);

    }

    /**
     * Test of getCategoria method, of class Tree.
     */
    @Test
    public void testGetCategoria() {
        System.out.println("getCategoria");
        Categoria cat = new Categoria();
        cat.setIdCategoria(1);
        cat.setDescricaoCategoria("Descricao teste");
        cat.setNomeCategoria("Teste");
        cat.setNivelCategoria(1);
        cat.setEstado("ativa");

        List<Categoria> Categoria = new ArrayList<>();
        Categoria.add(cat);

        Tree instance = new Tree();
        instance.setCategoria(Categoria);

        assertEquals(Categoria, instance.getCategoria());

    }

    /**
     * Test of setCategoria method, of class Tree.
     */
    @Test
    public void testSetCategoria() {
        System.out.println("setCategoria");
        Categoria cat = new Categoria();
        cat.setIdCategoria(1);
        cat.setDescricaoCategoria("Descricao teste");
        cat.setNomeCategoria("Teste");
        cat.setNivelCategoria(1);
        cat.setEstado("ativa");

        List<Categoria> Categoria = new ArrayList<>();
        Categoria.add(cat);
        
        Tree instance = new Tree();
        instance.setCategoria(Categoria);

    }

}
