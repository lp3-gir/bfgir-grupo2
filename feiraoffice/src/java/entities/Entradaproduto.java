/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Ruben
 */
@Entity
@Table(name = "entradaproduto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Entradaproduto.findAll", query = "SELECT e FROM Entradaproduto e")
    , @NamedQuery(name = "Entradaproduto.findByIdEntrada", query = "SELECT e FROM Entradaproduto e WHERE e.entradaprodutoPK.idEntrada = :idEntrada")
    , @NamedQuery(name = "Entradaproduto.findByCodProdPa", query = "SELECT e FROM Entradaproduto e WHERE e.entradaprodutoPK.codProdPa = :codProdPa")
    , @NamedQuery(name = "Entradaproduto.findByPrecoUnitSIVA", query = "SELECT e FROM Entradaproduto e WHERE e.precoUnitSIVA = :precoUnitSIVA")
    , @NamedQuery(name = "Entradaproduto.findByTipoPrecos", query = "SELECT e FROM Entradaproduto e WHERE e.tipoPrecos = :tipoPrecos")
    , @NamedQuery(name = "Entradaproduto.findByPrecoTotalSIVA", query = "SELECT e FROM Entradaproduto e WHERE e.precoTotalSIVA = :precoTotalSIVA")
    , @NamedQuery(name = "Entradaproduto.findByIva", query = "SELECT e FROM Entradaproduto e WHERE e.iva = :iva")
    , @NamedQuery(name = "Entradaproduto.findByTotalIVA", query = "SELECT e FROM Entradaproduto e WHERE e.totalIVA = :totalIVA")
    , @NamedQuery(name = "Entradaproduto.findByLocalTAX", query = "SELECT e FROM Entradaproduto e WHERE e.localTAX = :localTAX")
    , @NamedQuery(name = "Entradaproduto.findByQuantVendida", query = "SELECT e FROM Entradaproduto e WHERE e.quantVendida = :quantVendida")
    , @NamedQuery(name = "Entradaproduto.findByTipoVenda", query = "SELECT e FROM Entradaproduto e WHERE e.tipoVenda = :tipoVenda")
    , @NamedQuery(name = "Entradaproduto.findByPesoTotalVenda", query = "SELECT e FROM Entradaproduto e WHERE e.pesoTotalVenda = :pesoTotalVenda")
    , @NamedQuery(name = "Entradaproduto.findByTipoPeso", query = "SELECT e FROM Entradaproduto e WHERE e.tipoPeso = :tipoPeso")
    , @NamedQuery(name = "Entradaproduto.findByQuantTotalVendaUni", query = "SELECT e FROM Entradaproduto e WHERE e.quantTotalVendaUni = :quantTotalVendaUni")
    , @NamedQuery(name = "Entradaproduto.findByTipoVendaUni", query = "SELECT e FROM Entradaproduto e WHERE e.tipoVendaUni = :tipoVendaUni")
    , @NamedQuery(name = "Entradaproduto.findByPrecoTotalCIVA", query = "SELECT e FROM Entradaproduto e WHERE e.precoTotalCIVA = :precoTotalCIVA")
    , @NamedQuery(name = "Entradaproduto.findByEstado", query = "SELECT e FROM Entradaproduto e WHERE e.estado = :estado")})
public class Entradaproduto implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @EmbeddedId
    protected EntradaprodutoPK entradaprodutoPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "precoUnitSIVA")
    private double precoUnitSIVA;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "tipoPrecos")
    private String tipoPrecos;
    @Basic(optional = false)
    @NotNull
    @Column(name = "precoTotalSIVA")
    private double precoTotalSIVA;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IVA")
    private int iva;
    @Basic(optional = false)
    @NotNull
    @Column(name = "totalIVA")
    private double totalIVA;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "localTAX")
    private String localTAX;
    @Basic(optional = false)
    @NotNull
    @Column(name = "quantVendida")
    private long quantVendida;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "tipoVenda")
    private String tipoVenda;
    @Basic(optional = false)
    @NotNull
    @Column(name = "pesoTotalVenda")
    private double pesoTotalVenda;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "tipoPeso")
    private String tipoPeso;
    @Basic(optional = false)
    @NotNull
    @Column(name = "quantTotalVendaUni")
    private long quantTotalVendaUni;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "tipoVendaUni")
    private String tipoVendaUni;
    @Basic(optional = false)
    @NotNull
    @Column(name = "precoTotalCIVA")
    private double precoTotalCIVA;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "estado")
    private String estado;

    /**
     *
     */
    public Entradaproduto() {
    }

    /**
     *
     * @param entradaprodutoPK recebe um objeto do tipo entradaprodutoPK
     */
    public Entradaproduto(EntradaprodutoPK entradaprodutoPK) {
        this.entradaprodutoPK = entradaprodutoPK;
    }

    /**
     *
     * @param entradaprodutoPK recebe um objeto do tipo entradaprodutoPK
     * @param precoUnitSIVA recebe um preço por unidade sem IVA
     * @param tipoPrecos recebe um tipo de preços
     * @param precoTotalSIVA recebe um preço total sem IVA
     * @param iva recebe uma taxa de IVA
     * @param totalIVA recebe um total de IVA
     * @param localTAX recebe um tipo de IVA
     * @param quantVendida recebe uma quantidade de produtos vendida
     * @param tipoVenda recebe um tipo de venda de produtos
     * @param pesoTotalVenda recebe um peso total de venda
     * @param tipoPeso recebe um tipo de pesos
     * @param quantTotalVendaUni recebe uma quantidade total de venda em unidades
     * @param tipoVendaUni recebe um tipo de venda para a venda em unidades
     * @param precoTotalCIVA recebe um preço total com IVA
     * @param estado recebe um estado da entrada de produtos
     */
    public Entradaproduto(EntradaprodutoPK entradaprodutoPK, double precoUnitSIVA, String tipoPrecos, double precoTotalSIVA, int iva, double totalIVA, String localTAX, long quantVendida, String tipoVenda, double pesoTotalVenda, String tipoPeso, long quantTotalVendaUni, String tipoVendaUni, double precoTotalCIVA, String estado) {
        this.entradaprodutoPK = entradaprodutoPK;
        this.precoUnitSIVA = precoUnitSIVA;
        this.tipoPrecos = tipoPrecos;
        this.precoTotalSIVA = precoTotalSIVA;
        this.iva = iva;
        this.totalIVA = totalIVA;
        this.localTAX = localTAX;
        this.quantVendida = quantVendida;
        this.tipoVenda = tipoVenda;
        this.pesoTotalVenda = pesoTotalVenda;
        this.tipoPeso = tipoPeso;
        this.quantTotalVendaUni = quantTotalVendaUni;
        this.tipoVendaUni = tipoVendaUni;
        this.precoTotalCIVA = precoTotalCIVA;
        this.estado = estado;
    }

    /**
     *
     * @param idEntrada recebe um id de entrada 
     * @param codProdPa recebe um código de produto
     */
    public Entradaproduto(String idEntrada, String codProdPa) {
        this.entradaprodutoPK = new EntradaprodutoPK(idEntrada, codProdPa);
    }

    /**
     *
     * @return entradaprodutoPK
     */
    public EntradaprodutoPK getEntradaprodutoPK() {
        return entradaprodutoPK;
    }

    /**
     *
     * @param entradaprodutoPK
     */
    public void setEntradaprodutoPK(EntradaprodutoPK entradaprodutoPK) {
        this.entradaprodutoPK = entradaprodutoPK;
    }

    /**
     *
     * @return precoUnitSIVA
     */
    public double getPrecoUnitSIVA() {
        return precoUnitSIVA;
    }

    /**
     *
     * @param precoUnitSIVA
     */
    public void setPrecoUnitSIVA(double precoUnitSIVA) {
        this.precoUnitSIVA = precoUnitSIVA;
    }

    /**
     *
     * @return tipoPrecos
     */
    public String getTipoPrecos() {
        return tipoPrecos;
    }

    /**
     *
     * @param tipoPrecos
     */
    public void setTipoPrecos(String tipoPrecos) {
        this.tipoPrecos = tipoPrecos;
    }

    /**
     *
     * @return precoTotalSIVA
     */
    public double getPrecoTotalSIVA() {
        return precoTotalSIVA;
    }

    /**
     *
     * @param precoTotalSIVA
     */
    public void setPrecoTotalSIVA(double precoTotalSIVA) {
        this.precoTotalSIVA = precoTotalSIVA;
    }

    /**
     *
     * @return iva
     */
    public int getIva() {
        return iva;
    }

    /**
     *
     * @param iva
     */
    public void setIva(int iva) {
        this.iva = iva;
    }

    /**
     *
     * @return totalIVA
     */
    public double getTotalIVA() {
        return totalIVA;
    }

    /**
     *
     * @param totalIVA
     */
    public void setTotalIVA(double totalIVA) {
        this.totalIVA = totalIVA;
    }

    /**
     *
     * @return localTAX
     */
    public String getLocalTAX() {
        return localTAX;
    }

    /**
     *
     * @param localTAX
     */
    public void setLocalTAX(String localTAX) {
        this.localTAX = localTAX;
    }

    /**
     *
     * @return quantVendida
     */
    public long getQuantVendida() {
        return quantVendida;
    }

    /**
     *
     * @param quantVendida
     */
    public void setQuantVendida(long quantVendida) {
        this.quantVendida = quantVendida;
    }

    /**
     *
     * @return tipoVenda
     */
    public String getTipoVenda() {
        return tipoVenda;
    }

    /**
     *
     * @param tipoVenda
     */
    public void setTipoVenda(String tipoVenda) {
        this.tipoVenda = tipoVenda;
    }

    /**
     *
     * @return pesoTotalVenda
     */
    public double getPesoTotalVenda() {
        return pesoTotalVenda;
    }

    /**
     *
     * @param pesoTotalVenda
     */
    public void setPesoTotalVenda(double pesoTotalVenda) {
        this.pesoTotalVenda = pesoTotalVenda;
    }

    /**
     *
     * @return tipoPeso
     */
    public String getTipoPeso() {
        return tipoPeso;
    }

    /**
     *
     * @param tipoPeso
     */
    public void setTipoPeso(String tipoPeso) {
        this.tipoPeso = tipoPeso;
    }

    /**
     *
     * @return quantTotalVendaUni
     */
    public long getQuantTotalVendaUni() {
        return quantTotalVendaUni;
    }

    /**
     *
     * @param quantTotalVendaUni
     */
    public void setQuantTotalVendaUni(long quantTotalVendaUni) {
        this.quantTotalVendaUni = quantTotalVendaUni;
    }

    /**
     *
     * @return tipoVendaUni
     */
    public String getTipoVendaUni() {
        return tipoVendaUni;
    }

    /**
     *
     * @param tipoVendaUni
     */
    public void setTipoVendaUni(String tipoVendaUni) {
        this.tipoVendaUni = tipoVendaUni;
    }

    /**
     *
     * @return precoTotalCIVA
     */
    public double getPrecoTotalCIVA() {
        return precoTotalCIVA;
    }

    /**
     *
     * @param precoTotalCIVA
     */
    public void setPrecoTotalCIVA(double precoTotalCIVA) {
        this.precoTotalCIVA = precoTotalCIVA;
    }

    /**
     *
     * @return estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     *
     * @param estado
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }
}
