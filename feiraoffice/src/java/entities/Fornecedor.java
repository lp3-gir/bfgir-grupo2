/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Ruben
 */
@Entity
@Table(name = "fornecedor")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Fornecedor.findAll", query = "SELECT f FROM Fornecedor f")
    , @NamedQuery(name = "Fornecedor.findByIdFornecedor", query = "SELECT f FROM Fornecedor f WHERE f.idFornecedor = :idFornecedor")
    , @NamedQuery(name = "Fornecedor.findByIdUtilizador", query = "SELECT f FROM Fornecedor f WHERE f.idUtilizador = :idUtilizador")
    , @NamedQuery(name = "Fornecedor.findByNome", query = "SELECT f FROM Fornecedor f WHERE f.nome = :nome")
    , @NamedQuery(name = "Fornecedor.findByContribuinte", query = "SELECT f FROM Fornecedor f WHERE f.contribuinte = :contribuinte")
    , @NamedQuery(name = "Fornecedor.findByMorada", query = "SELECT f FROM Fornecedor f WHERE f.morada = :morada")
    , @NamedQuery(name = "Fornecedor.findByCodPostal", query = "SELECT f FROM Fornecedor f WHERE f.codPostal = :codPostal")
    , @NamedQuery(name = "Fornecedor.findByPais", query = "SELECT f FROM Fornecedor f WHERE f.pais = :pais")
    , @NamedQuery(name = "Fornecedor.findByEstado", query = "SELECT f FROM Fornecedor f WHERE f.estado = :estado")})
public class Fornecedor implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "idFornecedor")
    private String idFornecedor;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idUtilizador")
    private int idUtilizador;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "nome")
    private String nome;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "contribuinte")
    private Integer contribuinte;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "morada")
    private String morada;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "codPostal")
    private String codPostal;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "pais")
    private String pais;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "estado")
    private String estado;

    /**
     *
     */
    public Fornecedor() {
    }

    /**
     *
     * @param contribuinte recebe um contribuinte de um fornecedor
     */
    public Fornecedor(Integer contribuinte) {
        this.contribuinte = contribuinte;
    }

    /**
     *
     * @param contribuinte recebe um contribuinte de um fornecedor
     * @param idFornecedor recebe um id de um fornecedor
     * @param idUtilizador recebe um id de utilizador de um fornecedor
     * @param nome recebe um nome de um fornecedor
     * @param morada recebe uma morada de um fornecedor
     * @param codPostal recebe um código postal de um fornecedor
     * @param pais recebe um país de um fornecedor
     * @param estado recebe um estado de um fornecedor
     */
    public Fornecedor(Integer contribuinte, String idFornecedor, int idUtilizador, String nome, String morada, String codPostal, String pais, String estado) {
        this.contribuinte = contribuinte;
        this.idFornecedor = idFornecedor;
        this.idUtilizador = idUtilizador;
        this.nome = nome;
        this.morada = morada;
        this.codPostal = codPostal;
        this.pais = pais;
        this.estado = estado;
    }

    /**
     *
     * @return idFornecedor
     */
    public String getIdFornecedor() {
        return idFornecedor;
    }

    /**
     *
     * @param idFornecedor
     */
    public void setIdFornecedor(String idFornecedor) {
        this.idFornecedor = idFornecedor;
    }

    /**
     *
     * @return idUtilizador
     */
    public int getIdUtilizador() {
        return idUtilizador;
    }

    /**
     *
     * @param idUtilizador
     */
    public void setIdUtilizador(int idUtilizador) {
        this.idUtilizador = idUtilizador;
    }

    /**
     *
     * @return nome
     */
    public String getNome() {
        return nome;
    }

    /**
     *
     * @param nome
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     *
     * @return contribuinte
     */
    public Integer getContribuinte() {
        return contribuinte;
    }

    /**
     *
     * @param contribuinte
     */
    public void setContribuinte(Integer contribuinte) {
        this.contribuinte = contribuinte;
    }

    /**
     *
     * @return morada
     */
    public String getMorada() {
        return morada;
    }

    /**
     *
     * @param morada
     */
    public void setMorada(String morada) {
        this.morada = morada;
    }

    /**
     *
     * @return codPostal
     */
    public String getCodPostal() {
        return codPostal;
    }

    /**
     *
     * @param codPostal
     */
    public void setCodPostal(String codPostal) {
        this.codPostal = codPostal;
    }

    /**
     *
     * @return pais
     */
    public String getPais() {
        return pais;
    }

    /**
     *
     * @param pais
     */
    public void setPais(String pais) {
        this.pais = pais;
    }

    /**
     *
     * @return estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     *
     * @param estado
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

}
