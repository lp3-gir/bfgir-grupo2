/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.service;

import io.restassured.RestAssured;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author ruben
 */
public class ClienteFacadeRESTTest {

    /**
     *
     */
    public ClienteFacadeRESTTest() {
    }

    /**
     * preparar o url base de acesso a API
     */
    @BeforeClass
    public static void setUpClass() {
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = 8080;

    }

    /**
     * Metodo de teste para criar um cliente
     */
    @Test
    public void testCreate() {
        System.out.println("testCreate");
        /*

        Random gerador = new Random();

        Cliente cli = new Cliente();
        cli.setNome("teste2");
        cli.setEmail("teste2");
        cli.setMorada("teste2");
        cli.setCodPostal("teste2");
        cli.setPais("teste2");
        cli.setContribuinte(gerador.nextInt());
        cli.setEstado("teste2");

        given()
                .contentType("application/xml")
                .body(cli)
                .when()
                .post("/feiraoffice/resources/client/")
                .then()
                .statusCode(204);
*/
    }

    /**
     * Metodo de teste para testar a eliminação de um cliente
     */
    @Test
    public void testRemove_Integer() {
        System.out.println("testRemove_Integer");
/*
        Cliente cli = new Cliente();
        cli.setNome("teste22");
        cli.setEmail("teste22");
        cli.setMorada("teste22");
        cli.setCodPostal("teste22");
        cli.setPais("teste22");
        cli.setContribuinte(400);
        cli.setEstado("teste22");

        given()
                .contentType("application/JSON")
                .body(cli)
                .when()
                .post("/feiraoffice/resources/client/")
                .then();

        Response response = given()
                .headers("Content-Type", ContentType.JSON, "Accept", ContentType.JSON)
                .when()
                .get("/feiraoffice/resources/client/")
                .then()
                .contentType(ContentType.JSON)
                .extract()
                .response();

        String clientes = response.jsonPath().getString("idCliente");

        clientes = clientes.replace("[", "");
        clientes = clientes.replace("]", "");

        String[] clientesDataArray = clientes.split(",");

        String fi = clientesDataArray[clientesDataArray.length - 1];

        fi = fi.replace(" ", "");

        int id = Integer.parseInt(fi);

        given().pathParam("idCliente", id)
                .when()
                .delete("/feiraoffice/resources/client/{idCliente}")
                .then()
                .statusCode(204);
        */
    }

    /**
     * Metodo de teste para testar a pesquisa de um cliente por ID
     */
    @Test
    public void testFind_Integer() {

        System.out.println("testFind_Integer");

        /*
        given()
                .contentType("application/xml")
                .when()
                .get("/feiraoffice/resources/client/1/")
                .then()
                .body("cliente.idCliente", equalTo("1"))
                .body("cliente.nome", equalTo("ruben"))
                .body("cliente.email", equalTo("1191362@isep.ipp.pt"))
                .body("cliente.morada", equalTo("testess"))
                .body("cliente.codPostal", equalTo("4415-914"))
                .body("cliente.pais", equalTo("portugal"))
                .body("cliente.contribuinte", equalTo("256398741"))
                .body("cliente.estado", equalTo("ativo"))
                .log()
                .all()
                .statusCode(200);
        */
    }

    /**
     * Metodo de teste para testar uma pesquisa de todos os clientes
     */
    @Test
    public void testFindAll() {
        System.out.println("testFindAll");
        /*
        given()
                .when()
                .get("/feiraoffice/resources/client/")
                .then()
                .log()
                .all()
                .statusCode(200);
        */
    }

    /**
     * Metodo de teste para editar um cliente por ID
     */
    @Test
    public void testEdit_Integer_Utilizador() {

        System.out.println("testEdit_Integer_Utilizador");

        /*
        String requestBody = ""
                + "{\n"
                + "    \"idCliente\": 213,\n"
                + "    \"nome\": \"teste50\",\n"
                + "    \"email\": \"teste50\",\n"
                + "    \"morada\": \"teste50\",\n"
                + "    \"codPostal\": \"teste50\",\n"
                + "    \"pais\": \"teste50\",\n"
                + "    \"contribuinte\": 500,\n"
                + "    \"estado\": \"teste50\"\n"
                + "}";

        Response response = given()
                .header("Content-type", "application/json")
                .and()
                .body(requestBody)
                .when()
                .put("/feiraoffice/resources/client/213")
                .then()
                .extract()
                .response();
*/
    }

}
