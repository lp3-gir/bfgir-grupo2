/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Ruben
 */
@Entity
@Table(name = "codigoPostal")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CodigoPostal.findAll", query = "SELECT c FROM CodigoPostal c")
    , @NamedQuery(name = "CodigoPostal.findByCodPostal", query = "SELECT c FROM CodigoPostal c WHERE c.codPostal = :codPostal")
    , @NamedQuery(name = "CodigoPostal.findByCidade", query = "SELECT c FROM CodigoPostal c WHERE c.cidade = :cidade")})
public class CodigoPostal implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "codPostal")
    private String codPostal;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "cidade")
    private String cidade;

    /**
     *
     */
    public CodigoPostal() {
    }

    /**
     *
     * @param codPostal recebe um código postal
     */
    public CodigoPostal(String codPostal) {
        this.codPostal = codPostal;
    }

    /**
     *
     * @param codPostal recebe um código postal
     * @param cidade recebe um cidade
     */
    public CodigoPostal(String codPostal, String cidade) {
        this.codPostal = codPostal;
        this.cidade = cidade;
    }

    /**
     *
     * @return codPostal
     */
    public String getCodPostal() {
        return codPostal;
    }

    /**
     *
     * @param codPostal
     */
    public void setCodPostal(String codPostal) {
        this.codPostal = codPostal;
    }

    /**
     *
     * @return cidade
     */
    public String getCidade() {
        return cidade;
    }

    /**
     *
     * @param cidade
     */
    public void setCidade(String cidade) {
        this.cidade = cidade;
    }
}
