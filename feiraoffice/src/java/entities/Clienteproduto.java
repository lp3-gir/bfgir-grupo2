/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ruben
 */
@Entity
@Table(name = "clienteproduto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Clienteproduto.findAll", query = "SELECT c FROM Clienteproduto c")
    , @NamedQuery(name = "Clienteproduto.findByIdCliente", query = "SELECT c FROM Clienteproduto c WHERE c.clienteprodutoPK.idCliente = :idCliente")
    , @NamedQuery(name = "Clienteproduto.findByCodProdPa", query = "SELECT c FROM Clienteproduto c WHERE c.clienteprodutoPK.codProdPa = :codProdPa")
    , @NamedQuery(name = "Clienteproduto.findByPrecoUnitSIVA", query = "SELECT c FROM Clienteproduto c WHERE c.precoUnitSIVA = :precoUnitSIVA")
    , @NamedQuery(name = "Clienteproduto.findByidCliProd", query = "SELECT c FROM Clienteproduto c WHERE c.clienteprodutoPK.idCliente = :idCliente and c.clienteprodutoPK.codProdPa = :codProdPa")})
public class Clienteproduto implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @EmbeddedId
    protected ClienteprodutoPK clienteprodutoPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "precoUnitSIVA")
    private double precoUnitSIVA;

    /**
     *
     */
    public Clienteproduto() {
    }

    /**
     *
     * @param clienteprodutoPK recebe um objeto do tipo clienteprodutoPK
     */
    public Clienteproduto(ClienteprodutoPK clienteprodutoPK) {
        this.clienteprodutoPK = clienteprodutoPK;
    }

    /**
     *
     * @param clienteprodutoPK recebe um objeto do tipo clienteprodutoPK
     * @param precoUnitSIVA recebe um preço por unidade sem IVA
     */
    public Clienteproduto(ClienteprodutoPK clienteprodutoPK, double precoUnitSIVA) {
        this.clienteprodutoPK = clienteprodutoPK;
        this.precoUnitSIVA = precoUnitSIVA;
    }

    /**
     * 
     * @param idCliente recebe um id de cliente
     * @param codProdPa recebe um código de produto
     */
    public Clienteproduto(int idCliente, String codProdPa) {
        this.clienteprodutoPK = new ClienteprodutoPK(idCliente, codProdPa);
    }

    /**
     *
     * @return clienteprodutoPK
     */
    public ClienteprodutoPK getClienteprodutoPK() {
        return clienteprodutoPK;
    }

    /**
     *
     * @param clienteprodutoPK
     */
    public void setClienteprodutoPK(ClienteprodutoPK clienteprodutoPK) {
        this.clienteprodutoPK = clienteprodutoPK;
    }

    /**
     *
     * @return precoUnitSIVA
     */
    public double getPrecoUnitSIVA() {
        return precoUnitSIVA;
    }

    /**
     *
     * @param precoUnitSIVA
     */
    public void setPrecoUnitSIVA(double precoUnitSIVA) {
        this.precoUnitSIVA = precoUnitSIVA;
    }
}
