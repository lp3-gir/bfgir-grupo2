/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ruben
 */
public class FornecedorTest {



    /**
     * Test of getIdFornecedor method, of class Fornecedor.
     */
    @Test
    public void testGetIdFornecedor() {
        System.out.println("getIdFornecedor");
        Fornecedor instance = new Fornecedor();
        instance.setIdFornecedor("F.123");
        assertEquals("F.123", instance.getIdFornecedor());

    }

    /**
     * Test of setIdFornecedor method, of class Fornecedor.
     */
    @Test
    public void testSetIdFornecedor() {
        System.out.println("setIdFornecedor");
        Fornecedor instance = new Fornecedor();
        instance.setIdFornecedor("F.123");

    }

    /**
     * Test of getIdUtilizador method, of class Fornecedor.
     */
    @Test
    public void testGetIdUtilizador() {
        System.out.println("getIdUtilizador");
        Fornecedor instance = new Fornecedor();
        instance.setIdUtilizador(1);
        assertEquals(1, instance.getIdUtilizador());

    }

    /**
     * Test of setIdUtilizador method, of class Fornecedor.
     */
    @Test
    public void testSetIdUtilizador() {
        System.out.println("setIdUtilizador");
        Fornecedor instance = new Fornecedor();
        instance.setIdUtilizador(1);

    }

    /**
     * Test of getNome method, of class Fornecedor.
     */
    @Test
    public void testGetNome() {
        System.out.println("getNome");
        Fornecedor instance = new Fornecedor();
        instance.setNome("Teste");
        assertEquals("Teste", instance.getNome());

    }

    /**
     * Test of setNome method, of class Fornecedor.
     */
    @Test
    public void testSetNome() {
        System.out.println("setNome");
        Fornecedor instance = new Fornecedor();
        instance.setNome("Teste");

    }

    /**
     * Test of getContribuinte method, of class Fornecedor.
     */
    @Test
    public void testGetContribuinte() {
        System.out.println("getContribuinte");
        Fornecedor instance = new Fornecedor();
        instance.setContribuinte(1);
        assertEquals(1, instance.getContribuinte().intValue());

    }

    /**
     * Test of setContribuinte method, of class Fornecedor.
     */
    @Test
    public void testSetContribuinte() {
        System.out.println("setContribuinte");
        Fornecedor instance = new Fornecedor();
        instance.setContribuinte(156398247);

    }

    /**
     * Test of getMorada method, of class Fornecedor.
     */
    @Test
    public void testGetMorada() {
        System.out.println("getMorada");
        Fornecedor instance = new Fornecedor();
        instance.setMorada("Rua dos testes");
        assertEquals("Rua dos testes", instance.getMorada());

    }

    /**
     * Test of setMorada method, of class Fornecedor.
     */
    @Test
    public void testSetMorada() {
        System.out.println("setMorada");
        Fornecedor instance = new Fornecedor();
        instance.setMorada("Rua dos testes");

    }

    /**
     * Test of getCodPostal method, of class Fornecedor.
     */
    @Test
    public void testGetCodPostal() {
        System.out.println("getCodPostal");
        Fornecedor instance = new Fornecedor();
        instance.setCodPostal("4415-900");
        assertEquals("4415-900", instance.getCodPostal());

    }

    /**
     * Test of setCodPostal method, of class Fornecedor.
     */
    @Test
    public void testSetCodPostal() {
        System.out.println("setCodPostal");
        Fornecedor instance = new Fornecedor();
        instance.setCodPostal("4415-900");

    }

    /**
     * Test of getPais method, of class Fornecedor.
     */
    @Test
    public void testGetPais() {
        System.out.println("getPais");
        Fornecedor instance = new Fornecedor();
        instance.setPais("Portugal");
        assertEquals("Portugal", instance.getPais());

    }

    /**
     * Test of setPais method, of class Fornecedor.
     */
    @Test
    public void testSetPais() {
        System.out.println("setPais");
        Fornecedor instance = new Fornecedor();
        instance.setPais("Portugal");

    }

    /**
     * Test of getEstado method, of class Fornecedor.
     */
    @Test
    public void testGetEstado() {
        System.out.println("getEstado");
        Fornecedor instance = new Fornecedor();
        instance.setEstado("ativo");
        assertEquals("ativo", instance.getEstado());

    }

    /**
     * Test of setEstado method, of class Fornecedor.
     */
    @Test
    public void testSetEstado() {
        System.out.println("setEstado");
        Fornecedor instance = new Fornecedor();
        instance.setEstado("Estado");

    }

}
