/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import bfgir.ecraPessoalFornecedor;
import bfgir.login;
import bfgir.menuFornecedor;
import bfgir.uploadFicheiroFornecedor;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.stage.Stage;

/**
 * FXML Controller class
 */
public class menuFornecedorController implements Initializable {

    @FXML
    private Button inserirEncomenda;

    /**
     * Initializes the controller class.
     *
     * @param url url
     * @param rb rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    /**
     * Metodo para iniciar a pagina inserirEncomenda
     * 
     * @param event evento
     */
    @FXML
    private void inserirEncomenda(ActionEvent event) {
        try {
            uploadFicheiroFornecedor upload = new uploadFicheiroFornecedor();
            fecha();
            upload.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuFornecedorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para terminar sessao do utilizador 
     * 
     * @param event evento
     */
    @FXML
    private void terminarSessao(ActionEvent event) {
        try {
            login.setIdLogin(0);
            login.setNivelLogin(10000);
            login log = new login();
            fecha();
            log.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuFornecedorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * fechar
     */
    public void fecha() {
        menuFornecedor.getStage().close();
    }

    /**
     * Método para encerrar a aplicação
     */
    @FXML
    private void sair(ActionEvent event) {
        System.exit(0);
    }
    
    
    /**
     * Metodo para iniciar a pagina ecraPessoalOperador
     * 
     * @param event evento
     */
    @FXML
    private void ecraPessoalFornecedor(ActionEvent event) {
        try {
            ecraPessoalFornecedor ePForn = new ecraPessoalFornecedor ();
            fecha();
            ePForn.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(menuFornecedorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
