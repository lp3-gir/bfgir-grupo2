/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ruben
 */
public class ClienteprodutoPKTest {



    /**
     * Teste para retirar o id do cliente.
     */
    @Test
    public void testGetIdCliente() {
        System.out.println("getIdCliente");
        ClienteprodutoPK instance = new ClienteprodutoPK();
        instance.setIdCliente(1);

        int expResult = 1;
        assertEquals(expResult, instance.getIdCliente());

    }

    /**
     * Teste para definir o id do cliente.
     */
    @Test
    public void testSetIdCliente() {
        System.out.println("setIdCliente");
        ClienteprodutoPK instance = new ClienteprodutoPK();
        instance.setIdCliente(1);

    }

    /**
     * Teste para retirar o código do produto.
     */
    @Test
    public void testGetCodProdPa() {
        System.out.println("getCodProdPa");
        ClienteprodutoPK instance = new ClienteprodutoPK();
        instance.setCodProdPa("T.123");
        assertEquals("T.123", instance.getCodProdPa());

    }

    /**
     * Teste para definir o código do produto.
     */
    @Test
    public void testSetCodProdPa() {
        System.out.println("setCodProdPa");
        ClienteprodutoPK instance = new ClienteprodutoPK();
        instance.setCodProdPa("T.123");

    }

}
