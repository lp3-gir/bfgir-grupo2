/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.util.List;
import java.util.Objects;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author gonca
 */
public class encomendasProdutoTest {

    public encomendasProdutoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getIdEncomenda method, of class encomendasProduto.
     */
    @Test
    public void testGetIdEncomenda() {
        System.out.println("getIdEncomenda");
        encomendasProduto instance = new encomendasProduto();
        instance.setIdEncomenda(2);
        int expResult = 2;
        int result = instance.getIdEncomenda();
        if (expResult == result) {
            System.out.println("Sucesso");
        } else {
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of setIdEncomenda method, of class encomendasProduto.
     */
    @Test
    public void testSetIdEncomenda() {
        System.out.println("setIdEncomenda");
        encomendasProduto instance = new encomendasProduto();
        int expResult = 2;
        instance.setIdEncomenda(expResult);
        int result = instance.getIdEncomenda();
        if (expResult == result) {
            System.out.println("Sucesso");
        } else {
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of getCodProdPa method, of class encomendasProduto.
     */
    @Test
    public void testGetCodProdPa() {
        System.out.println("getCodProdPa");
        encomendasProduto instance = new encomendasProduto();
        String expResult = "prod";
        instance.setCodProdPa("prod");
        String result = instance.getCodProdPa();
        if (expResult.equals(result)) {
            System.out.println("Sucesso");
        } else {
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of setCodProdPa method, of class encomendasProduto.
     */
    @Test
    public void testSetCodProdPa() {
        System.out.println("setCodProdPa");
        encomendasProduto instance = new encomendasProduto();
        String expResult = "prod";
        instance.setCodProdPa(expResult);
        String result = instance.getCodProdPa();
        if (expResult.equals(result)) {
            System.out.println("Sucesso");
        } else {
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of getQuantVendida method, of class encomendasProduto.
     */
    @Test
    public void testGetQuantVendida() {
        System.out.println("getQuantVendida");
        encomendasProduto instance = new encomendasProduto();
        int expResult = 2;
        instance.setQuantVendida(2);
        int result = instance.getQuantVendida();
        if (expResult == result) {
            System.out.println("Sucesso");
        } else {
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of setQuantVendida method, of class encomendasProduto.
     */
    @Test
    public void testSetQuantVendida() {
        System.out.println("setQuantVendida");
        encomendasProduto instance = new encomendasProduto();
        int expResult = 2;
        instance.setQuantVendida(expResult);
        int result = instance.getQuantVendida();
        if (expResult == result) {
            System.out.println("Sucesso");
        } else {
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of getTipoVenda method, of class encomendasProduto.
     */
    @Test
    public void testGetTipoVenda() {
        System.out.println("getTipoVenda");
        encomendasProduto instance = new encomendasProduto();
        String expResult = "venda1";
        instance.setTipoVenda("venda1");
        String result = instance.getTipoVenda();
        if (expResult.equals(result)) {
            System.out.println("Sucesso");
        } else {
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of setTipoVenda method, of class encomendasProduto.
     */
    @Test
    public void testSetTipoVenda() {
        System.out.println("setTipoVenda");
        encomendasProduto instance = new encomendasProduto();
        String expResult = "venda1";
        instance.setTipoVenda(expResult);
        String result = instance.getTipoVenda();
        if (expResult.equals(result)) {
            System.out.println("Sucesso");
        } else {
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of getPrecoUnitSIVA method, of class encomendasProduto.
     */
    @Test
    public void testGetPrecoUnitSIVA() {
        System.out.println("getPrecoUnitSIVA");
        encomendasProduto instance = new encomendasProduto();
        Float expResult = 0f;
        instance.setPrecoUnitSIVA(0f);
        Float result = instance.getPrecoUnitSIVA();
        if (Objects.equals(result, expResult)) {
            System.out.println("sucesso");
        } else {
            // TODO review the generated test code and remove the default call to fail.
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of setPrecoUnitSIVA method, of class encomendasProduto.
     */
    @Test
    public void testSetPrecoUnitSIVA() {
        System.out.println("setPrecoUnitSIVA");
        encomendasProduto instance = new encomendasProduto();
        Float expResult = 0f;
        instance.setPrecoUnitSIVA(expResult);
        Float result = instance.getPrecoUnitSIVA();
        if (Objects.equals(result, expResult)) {
            System.out.println("sucesso");
        } else {
            // TODO review the generated test code and remove the default call to fail.
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of getPrecoUnitCIVA method, of class encomendasProduto.
     */
    @Test
    public void testGetPrecoUnitCIVA() {
        System.out.println("getPrecoUnitCIVA");
        encomendasProduto instance = new encomendasProduto();
        Float expResult = 0f;
        instance.setPrecoUnitCIVA(0f);
        Float result = instance.getPrecoUnitCIVA();
        if (Objects.equals(result, expResult)) {
            System.out.println("sucesso");
        } else {
            // TODO review the generated test code and remove the default call to fail.
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of setPrecoUnitCIVA method, of class encomendasProduto.
     */
    @Test
    public void testSetPrecoUnitCIVA() {
        System.out.println("setPrecoUnitCIVA");
        encomendasProduto instance = new encomendasProduto();
        Float expResult = 0f;
        instance.setPrecoUnitCIVA(expResult);
        Float result = instance.getPrecoUnitCIVA();
        if (Objects.equals(result, expResult)) {
            System.out.println("sucesso");
        } else {
            // TODO review the generated test code and remove the default call to fail.
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of getEstado method, of class encomendasProduto.
     */
    @Test
    public void testGetEstado() {
        System.out.println("getEstado");
        encomendasProduto instance = new encomendasProduto();
        String expResult = "estado";
        instance.setEstado("estado");
        String result = instance.getEstado();
        if (expResult.equals(result)) {
            System.out.println("Sucesso");
        } else {
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of setEstado method, of class encomendasProduto.
     */
    @Test
    public void testSetEstado() {
        System.out.println("setEstado");
        encomendasProduto instance = new encomendasProduto();
        String expResult = "estado";
        instance.setEstado(expResult);
        String result = instance.getEstado();
        if (expResult.equals(result)) {
            System.out.println("Sucesso");
        } else {
            fail("The test case is a prototype.");
        }
    }



}
