/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ruben
 */
public class ProdutoTest {



    /**
     * Test of getCodProdPa method, of class Produto.
     */
    @Test
    public void testGetCodProdPa() {
        System.out.println("getCodProdPa");
        Produto instance = new Produto();
        instance.setCodProdPa("T.123");
        assertEquals("T.123", instance.getCodProdPa());

    }

    /**
     * Test of setCodProdPa method, of class Produto.
     */
    @Test
    public void testSetCodProdPa() {
        System.out.println("setCodProdPa");
        Produto instance = new Produto();
        instance.setCodProdPa("T.123");

    }

    /**
     * Test of getDescricao method, of class Produto.
     */
    @Test
    public void testGetDescricao() {
        System.out.println("getDescricao");
        Produto instance = new Produto();
        instance.setDescricao("Descricao Teste");
        assertEquals("Descricao Teste", instance.getDescricao());

    }

    /**
     * Test of setDescricao method, of class Produto.
     */
    @Test
    public void testSetDescricao() {
        System.out.println("setDescricao");
        Produto instance = new Produto();
        instance.setDescricao("Descricao Teste");

    }

    /**
     * Test of getQuantStock method, of class Produto.
     */
    @Test
    public void testGetQuantStock() {
        System.out.println("getQuantStock");
        Produto instance = new Produto();
        instance.setQuantStock(5000);
        assertEquals(5000, instance.getQuantStock());

    }

    /**
     * Test of setQuantStock method, of class Produto.
     */
    @Test
    public void testSetQuantStock() {
        System.out.println("setQuantStock");
        Produto instance = new Produto();
        instance.setQuantStock(5000);

    }

    /**
     * Test of getTipoQuantStock method, of class Produto.
     */
    @Test
    public void testGetTipoQuantStock() {
        System.out.println("getTipoQuantStock");
        Produto instance = new Produto();
        instance.setTipoQuantStock("Teste");
        assertEquals("Teste", instance.getTipoQuantStock());

    }

    /**
     * Test of setTipoQuantStock method, of class Produto.
     */
    @Test
    public void testSetTipoQuantStock() {
        System.out.println("setTipoQuantStock");
        Produto instance = new Produto();
        instance.setTipoQuantStock("Teste");

    }

    /**
     * Test of getQuantStockUnit method, of class Produto.
     */
    @Test
    public void testGetQuantStockUnit() {
        System.out.println("getQuantStockUnit");
        Produto instance = new Produto();
        instance.setQuantStockUnit(500000);
        assertEquals(500000, instance.getQuantStockUnit());

    }

    /**
     * Test of setQuantStockUnit method, of class Produto.
     */
    @Test
    public void testSetQuantStockUnit() {
        System.out.println("setQuantStockUnit");
        Produto instance = new Produto();
        instance.setQuantStockUnit(500000);

    }

    /**
     * Test of getTipoQuantStockUnit method, of class Produto.
     */
    @Test
    public void testGetTipoQuantStockUnit() {
        System.out.println("getTipoQuantStockUnit");
        Produto instance = new Produto();
        instance.setTipoQuantStockUnit("Teste");
        assertEquals("Teste", instance.getTipoQuantStockUnit());

    }

    /**
     * Test of setTipoQuantStockUnit method, of class Produto.
     */
    @Test
    public void testSetTipoQuantStockUnit() {
        System.out.println("setTipoQuantStockUnit");
        Produto instance = new Produto();
        instance.setTipoQuantStockUnit("Teste");

    }

    /**
     * Test of getPrecoUnitSIVA method, of class Produto.
     */
    @Test
    public void testGetPrecoUnitSIVA() {
        System.out.println("getPrecoUnitSIVA");
        Produto instance = new Produto();
        instance.setPrecoUnitSIVA(500);
        assertEquals(500, instance.getPrecoUnitSIVA(), 0.0);

    }

    /**
     * Test of setPrecoUnitSIVA method, of class Produto.
     */
    @Test
    public void testSetPrecoUnitSIVA() {
        System.out.println("setPrecoUnitSIVA");
        Produto instance = new Produto();
        instance.setPrecoUnitSIVA(500);

    }

    /**
     * Test of getIdCategoria method, of class Produto.
     */
    @Test
    public void testGetIdCategoria() {
        System.out.println("getIdCategoria");
        Produto instance = new Produto();
        instance.setIdCategoria(1);
        assertEquals(1, instance.getIdCategoria());

    }

    /**
     * Test of setIdCategoria method, of class Produto.
     */
    @Test
    public void testSetIdCategoria() {
        System.out.println("setIdCategoria");
        Produto instance = new Produto();
        instance.setIdCategoria(1);

    }

    /**
     * Test of getEstado method, of class Produto.
     */
    @Test
    public void testGetEstado() {
        System.out.println("getEstado");
        Produto instance = new Produto();
        instance.setEstado("Teste");
        assertEquals("Teste", instance.getEstado());

    }

    /**
     * Test of setEstado method, of class Produto.
     */
    @Test
    public void testSetEstado() {
        System.out.println("setEstado");
        Produto instance = new Produto();
        instance.setEstado("Teste");
    }
}
