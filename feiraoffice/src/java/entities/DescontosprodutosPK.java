/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author ruben
 */
@Embeddable
public class DescontosprodutosPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "idDesconto")
    private int idDesconto;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "codProdPa")
    private String codProdPa;

    /**
     *
     */
    public DescontosprodutosPK() {
    }

    /**
     *
     * @param idDesconto recebe um id de desconto
     * @param codProdPa recebe um código de produto
     */
    public DescontosprodutosPK(int idDesconto, String codProdPa) {
        this.idDesconto = idDesconto;
        this.codProdPa = codProdPa;
    }

    /**
     *
     * @return idDesconto
     */
    public int getIdDesconto() {
        return idDesconto;
    }

    /**
     *
     * @param idDesconto
     */
    public void setIdDesconto(int idDesconto) {
        this.idDesconto = idDesconto;
    }

    /**
     *
     * @return codProdPa
     */
    public String getCodProdPa() {
        return codProdPa;
    }

    /**
     *
     * @param codProdPa
     */
    public void setCodProdPa(String codProdPa) {
        this.codProdPa = codProdPa;
    }
}
