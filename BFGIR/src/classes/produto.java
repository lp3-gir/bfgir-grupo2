/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import bfgir.BDConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe Produto com metodos de acesso a tabela de produtos na BD
 *
 */
public class produto {

    private String codProdPa;
    private String descricao;
    private int quantStock;
    private String tipoQuantStock;
    private int quantStockUnit;
    private String tipoQuantStockUnit;
    private Float precoUnitSIVA;
    private int idCategoria;
    private String estado;

    /**
     * Construtor da classe produto
     */
    public produto() {
    }

    /**
     * Construtor da classe produto com parametros
     *
     * @param codProdPa codigo do produto
     * @param descricao descrição
     * @param quantStock quantidade
     * @param tipoQuantStock tipo de quantidade do stock
     * @param quantStockUnit quantidade stock unidade
     * @param tipoQuantStockUnit tipo quantidade stock unidade
     * @param estado estado do produto
     * @param idCategoria id da categoria do produto
     * @param precoUnitSIVA preco do produto sem preco
     */
    public produto(String codProdPa, String descricao, int quantStock, String tipoQuantStock, int quantStockUnit, String tipoQuantStockUnit, String estado, int idCategoria, Float precoUnitSIVA) {
        this.codProdPa = codProdPa;
        this.descricao = descricao;
        this.quantStock = quantStock;
        this.estado = estado;
        this.quantStockUnit = quantStockUnit;
        this.tipoQuantStock = tipoQuantStock;
        this.tipoQuantStockUnit = tipoQuantStockUnit;
        this.idCategoria = idCategoria;
        this.precoUnitSIVA = precoUnitSIVA;

    }

    /**
     * @return the codProdPa
     */
    public String getCodProdPa() {
        return codProdPa;
    }

    /**
     * @param codProdPa the codProdPa to set
     */
    public void setCodProdPa(String codProdPa) {
        this.codProdPa = codProdPa;
    }

    /**
     * @return the descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * @param descricao the descricao to set
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     * @return the quantStock
     */
    public int getQuantStock() {
        return quantStock;
    }

    /**
     * @param quantStock the quantStock to set
     */
    public void setQuantStock(int quantStock) {
        this.quantStock = quantStock;
    }

    /**
     * @return the tipoQuantStock
     */
    public String getTipoQuantStock() {
        return tipoQuantStock;
    }

    /**
     * @param tipoQuantStock the tipoQuantStock to set
     */
    public void setTipoQuantStock(String tipoQuantStock) {
        this.tipoQuantStock = tipoQuantStock;
    }

    /**
     * @return the quantStockUnit
     */
    public int getQuantStockUnit() {
        return quantStockUnit;
    }

    /**
     * @param quantStockUnit the quantStockUnit to set
     */
    public void setQuantStockUnit(int quantStockUnit) {
        this.quantStockUnit = quantStockUnit;
    }

    /**
     * @return the tipoQuantStockUnit
     */
    public String getTipoQuantStockUnit() {
        return tipoQuantStockUnit;
    }

    /**
     * @param tipoQuantStockUnit the tipoQuantStockUnit to set
     */
    public void setTipoQuantStockUnit(String tipoQuantStockUnit) {
        this.tipoQuantStockUnit = tipoQuantStockUnit;
    }

    /**
     * @return the precoUnitSIVA
     */
    public Float getPrecoUnitSIVA() {
        return precoUnitSIVA;
    }

    /**
     * @param precoUnitSIVA the precoUnitSIVA to set
     */
    public void setPrecoUnitSIVA(Float precoUnitSIVA) {
        this.precoUnitSIVA = precoUnitSIVA;
    }

    /**
     * @return the idCategoria
     */
    public int getIdCategoria() {
        return idCategoria;
    }

    /**
     * @param idCategoria the idCategoria to set
     */
    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }

    /**
     * @return the estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * Método para verificar a existência de um produto na BD<br>
     *
     * Este método recebe por parametro o codigo de um produto (codProdPa).
     * Posto isto faz um select à BD verificando se este código existe ou não
     * retornando uma resposta true ou false.<br>
     *
     * @param codProd codigo do produto
     * @return boolean se codProdPa é nulo
     *
     *
     */
    public static boolean verifProd(String codProd) {
        boolean prod = false;

        String sql = "Select * from produto where codProdPa = (?)";

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setString(1, codProd);
            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                if ("".equals(results.getString("CodProdPa"))) {
                    prod = false;
                } else {
                    prod = true;
                }

            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return prod;
    }

    /**
     *
     * Método usado para inserir um produto na BD<br>
     *
     * Este método recebe por parametro um produto. Posto isto é inserido na BD
     * descriminando cada atributo deste mesmo produto.<br>
     *
     * @param produtos objeto produto
     *
     *
     */
    public static void inserirProdutos(produto produtos) {

        String sql = "INSERT INTO produto (codProdPa,descricao,quantStock,tipoQuantStock,quantStockUnit,tipoQuantStockUnit,precoUnitSIVA,idCategoria,estado) VALUES (?,?,?,?,?,?,?,?,?)";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setString(1, produtos.getCodProdPa());
            pstmt.setString(2, produtos.getDescricao());
            pstmt.setInt(3, produtos.getQuantStock());
            pstmt.setString(4, produtos.getTipoQuantStock());
            pstmt.setInt(5, produtos.getQuantStockUnit());
            pstmt.setString(6, produtos.getTipoQuantStockUnit());
            pstmt.setFloat(7, produtos.getPrecoUnitSIVA());
            pstmt.setInt(8, produtos.getIdCategoria());
            pstmt.setString(9, produtos.getEstado());

            pstmt.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

    }

    /**
     * Método usado para obter todos os produtos armazenados na Base de
     * Dados<br>
     *
     * Este método faz um select de todos os produtos armazenados na BD, criando
     * para resultado um respetivo produto com a seu codigo e respectiva
     * descrição. Depois de criados os produtos estes são adicionados de
     * imetiato a uma lista que é returnada pelo método.<br>
     *
     * @return lista de produtos
     *
     *
     *
     */
    public static List<produto> getListaProdutos() {

        List<produto> listaProdutos = new ArrayList();
        String sql = "Select * from produto";

        PreparedStatement stmt = null;
        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                produto item = new produto();
                item.setCodProdPa(results.getString(1));
                item.setDescricao(results.getString(2));
                item.setQuantStock(results.getInt(3));
                item.setTipoQuantStock(results.getString(4));
                item.setQuantStockUnit(results.getInt(5));
                item.setTipoQuantStockUnit(results.getString(6));
                item.setPrecoUnitSIVA(results.getFloat(7));
                item.setIdCategoria(results.getInt(8));
                item.setEstado(results.getString(9));
                listaProdutos.add(item);
            }
        } catch (SQLException ex) {
            Logger.getLogger(produto.class.getName()).log(Level.SEVERE, null, ex);
        }

        return listaProdutos;

    }

    /**
     * Método usado para obter o produto, armazenado na Base de Dados, pelo seu
     * codigo<br>
     *
     * Este método faz um select de todos os atributos do produto selecionado
     * que vem por parametro codProdPa, após a ida á base de dados cria um novo
     * produto com todos os seus atributos e retorna a lista com esse
     * produto.<br>
     *
     * Este método poderia ser do tipo produto visto que não precisamos de uma
     * lista com um produto apenas<br>
     *
     * @param codProdPa id do produto a pesquisar
     * @return lista de produtos
     *
     *
     *
     */
    public static List<produto> pesquisarProdutoID(String codProdPa) {

        List<produto> listaProdutos = new ArrayList();
        String sql = "Select * from produto where codProdPa = (?)";

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setString(1, codProdPa);

            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                produto item = new produto();
                item.setCodProdPa(results.getString(1));
                item.setDescricao(results.getString(2));
                item.setQuantStock(results.getInt(3));
                item.setTipoQuantStock(results.getString(4));
                item.setQuantStockUnit(results.getInt(5));
                item.setTipoQuantStockUnit(results.getString(6));
                item.setPrecoUnitSIVA(results.getFloat(7));
                item.setIdCategoria(results.getInt(8));
                item.setEstado(results.getString(9));
                listaProdutos.add(item);
            }
        } catch (SQLException ex) {
            Logger.getLogger(produto.class.getName()).log(Level.SEVERE, null, ex);
        }

        return listaProdutos;

    }

    /**
     * Método usado para atualizar o stock de um Produto na BD<br>
     *
     * Este método recebe 2 parametros, o id que corresponde ao codProdPa e a
     * quant que corresponde à quantStock. Posto isto este método faz um update
     * da quantidade de stock ao produto com o respectivo codProdPa<br>
     *
     * @param id id do produto a pesquisar
     * @param quantStock quantida de produto a alterar
     * @param tipoQuantStock tipo da quantidade do produto a alterar
     * @param quantStockUnit quantidade stock unidade a alterar
     * @param tipoQuantStockUnit tipo de quantidade stock unidade a alterar
     * @param precoUnitSIVA preco na unidade sem iva a alterar
     *
     *
     *
     */
    public static void atualizarQuantStock(String id, int quantStock, String tipoQuantStock, int quantStockUnit, String tipoQuantStockUnit, Float precoUnitSIVA) {

        String sql = "UPDATE produto set quantStock = (?), tipoQuantStock = (?), quantStockUnit = (?), tipoQuantStockUnit = (?), precoUnitSIVA = (?) WHERE codProdPa = (?)";

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setInt(1, quantStock);
            pstmt.setString(2, tipoQuantStock);
            pstmt.setInt(3, quantStockUnit);
            pstmt.setString(4, tipoQuantStockUnit);
            pstmt.setFloat(5, precoUnitSIVA);
            pstmt.setString(6, id);

            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * ToString
     *
     * @return Este texto remete para o objeto produto
     */
    @Override
    public String toString() {
        return "produto{" + getCodProdPa() + "," + getDescricao() + "," + getQuantStock() + "," + getTipoQuantStock() + "," + getQuantStockUnit() + "," + getTipoQuantStockUnit() + "," + getPrecoUnitSIVA() + "," + getEstado() + '}';
    }

    /**
     * Método usado para atualizar a descrição de um Produto na BD<br>
     *
     * Este método recebe 2 parametros, o código do produto que se pretende
     * altear e a descricao que se pretende atualizar. Posto isto, este método
     * faz um update da descrição do produto com o respectivo codProdPa<br>
     *
     * @param descricao : descrição do produto a alterar
     * @param codProdPa : código (id) do produto a alterar
     */
    public static void atualizarDescricaoProduto(String descricao, String codProdPa) {

        String sql = "UPDATE produto set descricao = (?) WHERE codProdPa = (?)";

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, descricao);
            pstmt.setString(2, codProdPa);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Método usado para atualizar o preço unitário de um Produto na BD<br>
     *
     * Este método recebe 2 parametros, o código do produto que se pretende
     * altear e o preço unitário sem iva que se pretende atualizar. Posto isto,
     * este método faz um update ao preço unitário do produto com o respectivo
     * codProdPa<br>
     *
     * @param codProdPa : código (id) do produto a altera
     * @param precoUnitSIVA preco na unidade sem iva a alterar
     *
     *
     *
     */
    public static void atualizarPrecoUniProduto(Float precoUnitSIVA, String codProdPa) {

        String sql = "UPDATE produto set precoUnitSIVA = (?) WHERE codProdPa = (?)";

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setFloat(1, precoUnitSIVA);
            pstmt.setString(2, codProdPa);

            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Método usado para atualizar o estado de um Produto na BD<br>
     *
     * Este método recebe 2 parametros, o código do produto que se pretende
     * alterar e o estado do produto que se pretende atualizar. Posto isto, este
     * método faz um update do estado do produto com o respectivo codProdPa<br>
     *
     * @param estado : estado do produto a alterar
     * @param codProdPa : código (id) do produto a alterar
     */
    public static void atualizarEstadoProduto(String estado, String codProdPa) {

        String sql = "UPDATE produto set estado = (?) WHERE codProdPa = (?)";

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, estado);
            pstmt.setString(2, codProdPa);

            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Método usado para atualizar a categoria de um Produto na BD<br>
     *
     * Este método recebe 2 parametros, o código do produto que se pretende
     * altear e o id da categoria do produto que se pretende atualizar. Posto
     * isto, este método faz um update ao id da categoria do produto com o
     * respectivo codProdPa<br>
     *
     * @param categoriaProduto : código (id) da categoria do produto a ser
     * alterado
     * @param codProdPa : código (id) do produto a alterar
     */
    public static void atualizarCategoriaProduto(int categoriaProduto, String codProdPa) {

        String sql = "UPDATE produto set idCategoria = (?) WHERE codProdPa = (?)";

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setInt(1, categoriaProduto);
            pstmt.setString(2, codProdPa);

            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Método usado para procurar por produtos com uma dada categoria
     * associada.<br>
     *
     * Método procura na BD por todos os produtos com uma dada categoria e
     * retorna uma lista de objetos do tipo de produto contendo os mesmos.<br>
     * Método pensado para validar alterações em categorias e produtos.<br>
     *
     * @param idCategoria : id de categoria
     * @return lista de produtos da categoria em pesquisa
     *
     */
    public static List<produto> pesquisarProdutoIDCategoria(int idCategoria) {

        List<produto> listaProdutos = new ArrayList();
        String sql = "Select * from produto where idCategoria = (?)";

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setInt(1, idCategoria);

            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                produto item = new produto();
                item.setCodProdPa(results.getString(1));
                item.setDescricao(results.getString(2));
                item.setQuantStock(results.getInt(3));
                item.setTipoQuantStock(results.getString(4));
                item.setQuantStockUnit(results.getInt(5));
                item.setTipoQuantStockUnit(results.getString(6));
                item.setPrecoUnitSIVA(results.getFloat(7));
                item.setIdCategoria(results.getInt(8));
                item.setEstado(results.getString(9));
                listaProdutos.add(item);
            }
        } catch (SQLException ex) {
            Logger.getLogger(produto.class.getName()).log(Level.SEVERE, null, ex);
        }

        return listaProdutos;

    }
}
