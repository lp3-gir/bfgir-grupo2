/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.service;

import entities.Cliente;
import entities.Clienteproduto;
import entities.Descontos;
import entities.Descontosprodutos;
import entities.Encomendas;
import entities.Encomendasproduto;
import entities.Order;
import entities.Produto;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Ruben
 */
@Stateless
@Path("/order/")
public class OrderFR {

    @PersistenceContext(unitName = "feiraofficePU")
    private EntityManager em;

    /**
     *
     * @param entity recebe um entidade do tipo order
     */
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    public void createJSON(Order entity) {

        String testeDados = "";
        List<Encomendas> enc = entity.getEncomendas();
        List<Encomendasproduto> EncProd = entity.getEncomendasproduto();
        int idCli = 0;
        float iva = 0.0f;
        String postEncProd = "";
        for (Encomendas ec : enc) {

            iva = ec.getIva();
            idCli = ec.getIdCliente();
            double precoTotalSIva = 0.0;
            double precoTotalCIva = 0.0;
            double totalIva = 0.0;
            long quantVe = 0;
            String idProd = "";

            for (int i = 0; i < EncProd.size(); i++) {

                idProd = EncProd.get(i).getCodProdPa();

                quantVe = EncProd.get(i).getQuantVendida();

                int idDesc = getIDTaxaDesc(idProd, quantVe);

                double taxaDesc = getTaxaDesc(idDesc);

                Query query = getEntityManager().createNamedQuery("Clienteproduto.findByidCliProd");
                query.setParameter("idCliente", idCli);
                query.setParameter("codProdPa", idProd);

                if (query.getResultList().isEmpty()) {
                    query = getEntityManager().createNamedQuery("Produto.findByCodProdPa");
                    query.setParameter("codProdPa", idProd);

                    List<Produto> produtos = query.getResultList();
                    for (Produto prods : produtos) {

                        precoTotalSIva += precoTotalSIVA(quantVe, prods.getPrecoUnitSIVA(), taxaDesc);
                        precoTotalCIva += precoTotalCIVA(quantVe, prods.getPrecoUnitSIVA(), iva, taxaDesc);
                        totalIva += totalIVA(quantVe, prods.getPrecoUnitSIVA(), iva, taxaDesc);

                    }
                } else {

                    List<Clienteproduto> Clienteprodutos = query.getResultList();
                    for (Clienteproduto cliProds : Clienteprodutos) {

                        precoTotalSIva += precoTotalSIVA(quantVe, cliProds.getPrecoUnitSIVA(), taxaDesc);
                        precoTotalCIva += precoTotalCIVA(quantVe, cliProds.getPrecoUnitSIVA(), iva, taxaDesc);
                        totalIva += totalIVA(quantVe, cliProds.getPrecoUnitSIVA(), iva, taxaDesc);

                    }
                }

            }

            Cliente cli = getCliente(idCli);

            double pts = Math.round(precoTotalSIva * 100.0) / 100.0;
            double ptc = Math.round(precoTotalCIva * 100.0) / 100.0;
            double ti = Math.round(totalIva * 100.0) / 100.0;

            postEncProd = ""
                    + "{\n"
                    + "        \"idCliente\": \"" + ec.getIdCliente() + "\",\n"
                    + "        \"dataEncomenda\": \"" + ec.getDataEncomenda() + "\",\n"
                    + "        \"moradaEntrega\": \"" + ec.getMoradaEntrega() + "\",\n"
                    + "        \"codPostalEntrega\": \"" + ec.getCodPostalEntrega() + "\",\n"
                    + "        \"paisEntrega\": \"" + ec.getPaisEntrega() + "\",\n"
                    + "        \"moradaFaturacao\": \"" + cli.getMorada() + "\",\n"
                    + "        \"codPostalFaturacao\": \"" + cli.getCodPostal() + "\",\n"
                    + "        \"paisFaturacao\": \"" + cli.getPais() + "\",\n"
                    + "        \"precoTotalSIVA\":" + pts + ",\n"
                    + "        \"iva\": " + ec.getIva() + ",\n"
                    + "        \"totalIVA\": " + ti + ",\n"
                    + "        \"precoTotalCIVA\": " + ptc + ",\n"
                    + "        \"estado\": \"ativo\"\n"
                    + "    }"
                    + "";
        }
        saveOrders(postEncProd, "json");
        int idEncLast = getLastOrder();
        //encomendas produtos
        for (int i = 0; i < EncProd.size(); i++) {

            double precoUnitSIVA = 0.0;
            double precoUnitCIVA = 0.0;
            String idProd = EncProd.get(i).getCodProdPa();
            long quantVe = EncProd.get(i).getQuantVendida();

            int idDesc = getIDTaxaDesc(idProd, quantVe);

            double taxaDesc = getTaxaDesc(idDesc);

            Query query = getEntityManager().createNamedQuery("Clienteproduto.findByidCliProd");
            query.setParameter("idCliente", idCli);
            query.setParameter("codProdPa", idProd);

            if (query.getResultList().isEmpty()) {
                query = getEntityManager().createNamedQuery("Produto.findByCodProdPa");
                query.setParameter("codProdPa", idProd);
                List<Produto> produtos = query.getResultList();
                for (Produto prods : produtos) {

                    precoUnitSIVA = Math.round(prods.getPrecoUnitSIVA() * 100.0) / 100.0;

                }
            } else {

                List<Clienteproduto> Clienteprodutos = query.getResultList();
                for (Clienteproduto cliProds : Clienteprodutos) {

                    precoUnitSIVA = Math.round(cliProds.getPrecoUnitSIVA() * 100.0) / 100.0;

                }
            }
            precoUnitSIVA = (precoUnitSIVA * 1.20);
            precoUnitCIVA = precoUnitSIVA * ((iva / (float) 100) + 1);

            double pts = precoUnitSIVA / ((taxaDesc / (float) 100) + 1);
            double ptc = precoUnitCIVA / ((taxaDesc / (float) 100) + 1);

            pts = Math.round(pts * 100.0) / 100.0;
            ptc = Math.round(ptc * 100.0) / 100.0;

            testeDados += precoUnitSIVA + " | " + ((taxaDesc / (float) 100) + 1) + "\n";

            postEncProd = ""
                    + "{\n"
                    + "            \"idEncomenda\": \"" + idEncLast + "\",\n"
                    + "            \"codProdPa\": \"" + EncProd.get(i).getCodProdPa() + "\",\n"
                    + "            \"quantVendida\": " + EncProd.get(i).getQuantVendida() + ",\n"
                    + "            \"tipoVenda\": \"" + EncProd.get(i).getTipoVenda() + "\",\n"
                    + "            \"precoUnitSIVA\":" + pts + ",\n"
                    + "            \"precoUnitCIVA\":" + ptc + ",\n"
                    + "            \"estado\": \"ativo\"\n"
                    + "        }"
                    + "";

            saveOrderdProducts(postEncProd, "json");

        }

    }

    /**
     *
     * @param entity recebe um entidade do tipo order
     */
    @POST
    @Consumes({MediaType.APPLICATION_XML})
    public void createXML(Order entity) {
        //String testeDados = "";

        //String testeDados = "";
        List<Encomendas> enc = entity.getEncomendas();
        List<Encomendasproduto> EncProd = entity.getEncomendasproduto();
        int idCli = 0; //return testeDados;
        float iva = 0.0f;
        String postEncProd = "";
        for (Encomendas ec : enc) {

            iva = ec.getIva();
            idCli = ec.getIdCliente();
            double precoTotalSIva = 0.0;
            double precoTotalCIva = 0.0;
            double totalIva = 0.0;
            long quantVe = 0;
            String idProd = "";

            for (int i = 0; i < EncProd.size(); i++) {
                idProd = EncProd.get(i).getCodProdPa();

                quantVe = EncProd.get(i).getQuantVendida();

                int idDesc = getIDTaxaDesc(idProd, quantVe);

                double taxaDesc = getTaxaDesc(idDesc);

                Query query = getEntityManager().createNamedQuery("Clienteproduto.findByidCliProd");
                query.setParameter("idCliente", idCli);
                query.setParameter("codProdPa", idProd);

                if (query.getResultList().isEmpty()) {
                    query = getEntityManager().createNamedQuery("Produto.findByCodProdPa");
                    query.setParameter("codProdPa", idProd);

                    List<Produto> produtos = query.getResultList();
                    for (Produto prods : produtos) {

                        precoTotalSIva += precoTotalSIVA(quantVe, prods.getPrecoUnitSIVA(), taxaDesc);
                        precoTotalCIva += precoTotalCIVA(quantVe, prods.getPrecoUnitSIVA(), iva, taxaDesc);
                        totalIva += totalIVA(quantVe, prods.getPrecoUnitSIVA(), iva, taxaDesc);

                    }
                } else {

                    List<Clienteproduto> Clienteprodutos = query.getResultList();
                    for (Clienteproduto cliProds : Clienteprodutos) {

                        precoTotalSIva += precoTotalSIVA(quantVe, cliProds.getPrecoUnitSIVA(), taxaDesc);
                        precoTotalCIva += precoTotalCIVA(quantVe, cliProds.getPrecoUnitSIVA(), iva, taxaDesc);
                        totalIva += totalIVA(quantVe, cliProds.getPrecoUnitSIVA(), iva, taxaDesc);

                    }
                }

            }

            Cliente cli = getCliente(idCli);

            double pts = Math.round(precoTotalSIva * 100.0) / 100.0;
            double ptc = Math.round(precoTotalCIva * 100.0) / 100.0;
            double ti = Math.round(totalIva * 100.0) / 100.0;

            postEncProd = ""
                    + "    <encomendas>\n"
                    + "        <codPostalEntrega>" + ec.getCodPostalEntrega() + "</codPostalEntrega>\n"
                    + "        <codPostalFaturacao>" + cli.getCodPostal() + "</codPostalFaturacao>\n"
                    + "        <dataEncomenda>" + ec.getDataEncomenda() + "</dataEncomenda>\n"
                    + "        <estado>ativo</estado>\n"
                    + "        <idCliente>" + ec.getIdCliente() + "</idCliente>\n"
                    + "        <iva>" + ec.getIva() + "</iva>\n"
                    + "        <moradaEntrega>" + ec.getMoradaEntrega() + "</moradaEntrega>\n"
                    + "        <moradaFaturacao>" + cli.getMorada() + "</moradaFaturacao>\n"
                    + "        <paisEntrega>" + ec.getPaisEntrega() + "</paisEntrega>\n"
                    + "        <paisFaturacao>" + cli.getPais() + "</paisFaturacao>\n"
                    + "        <precoTotalCIVA>" + ptc + "</precoTotalCIVA>\n"
                    + "        <precoTotalSIVA>" + pts + "</precoTotalSIVA>\n"
                    + "        <totalIVA>" + ti + "</totalIVA>\n"
                    + "    </encomendas>"
                    + "";
        }
        saveOrders(postEncProd, "xml");

        //testeDados += postEncProd + "\n";
        int idEncLast = getLastOrder();
        //encomendas produtos
        for (int i = 0; i < EncProd.size(); i++) {

            double precoUnitSIVA = 0.0;
            double precoUnitCIVA = 0.0;
            String idProd = EncProd.get(i).getCodProdPa();
            long quantVe = EncProd.get(i).getQuantVendida();

            int idDesc = getIDTaxaDesc(idProd, quantVe);

            double taxaDesc = getTaxaDesc(idDesc);

            Query query = getEntityManager().createNamedQuery("Clienteproduto.findByidCliProd");
            query.setParameter("idCliente", idCli);
            query.setParameter("codProdPa", idProd);

            if (query.getResultList().isEmpty()) {
                query = getEntityManager().createNamedQuery("Produto.findByCodProdPa");
                query.setParameter("codProdPa", idProd);
                List<Produto> produtos = query.getResultList();
                for (Produto prods : produtos) {

                    precoUnitSIVA = Math.round(prods.getPrecoUnitSIVA() * 100.0) / 100.0;

                }
            } else {

                List<Clienteproduto> Clienteprodutos = query.getResultList();
                for (Clienteproduto cliProds : Clienteprodutos) {

                    precoUnitSIVA = Math.round(cliProds.getPrecoUnitSIVA() * 100.0) / 100.0;

                }
            }
            precoUnitSIVA = (precoUnitSIVA * 1.20);
            precoUnitCIVA = precoUnitSIVA * ((iva / (float) 100) + 1);

            double pts = precoUnitSIVA / ((taxaDesc / (float) 100) + 1);
            double ptc = precoUnitCIVA / ((taxaDesc / (float) 100) + 1);

            pts = Math.round(pts * 100.0) / 100.0;
            ptc = Math.round(ptc * 100.0) / 100.0;

            postEncProd = ""
                    + "    <encomendasproduto>\n"
                    + "        <codProdPa>" + EncProd.get(i).getCodProdPa() + "</codProdPa>\n"
                    + "        <estado>ativo</estado>\n"
                    + "        <idEncomenda>" + idEncLast + "</idEncomenda>\n"
                    + "        <precoUnitCIVA>" + ptc + "</precoUnitCIVA>\n"
                    + "        <precoUnitSIVA>" + pts + "</precoUnitSIVA>\n"
                    + "        <quantVendida>" + EncProd.get(i).getQuantVendida() + "</quantVendida>\n"
                    + "        <tipoVenda>" + EncProd.get(i).getTipoVenda() + "</tipoVenda>\n"
                    + "    </encomendasproduto>"
                    + "";

            saveOrderdProducts(postEncProd, "xml");

            //testeDados += postEncProd + "\n";
        }
        //return testeDados;

    }

    /**
     *
     * @param id recebe um id da order
     * @return um objeto do tipor order
     */
    @GET
    @Path("/client/{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Order findEncCli(@PathParam("id") int id) {

        Order ecpr = null;
        int idCli = id;

        List<Encomendas> Encomends = new ArrayList<Encomendas>();
        List<Encomendasproduto> ListEP = new ArrayList<Encomendasproduto>();
        List<Produto> prds = new ArrayList<Produto>();

        Query query = getEntityManager().createNamedQuery("Encomendas.findByIdCliente");
        query.setParameter("idCliente", idCli);
        List<Encomendas> Encomenda = query.getResultList();
        for (Encomendas Encs : Encomenda) {

            Encomendas ec = new Encomendas();
            ec.setIdEncomenda(Encs.getIdEncomenda());
            ec.setIdCliente(Encs.getIdCliente());
            ec.setDataEncomenda(Encs.getDataEncomenda());
            ec.setMoradaEntrega(Encs.getMoradaEntrega());
            ec.setCodPostalEntrega(Encs.getCodPostalEntrega());
            ec.setPaisEntrega(Encs.getPaisEntrega());
            ec.setMoradaFaturacao(Encs.getMoradaFaturacao());
            ec.setCodPostalFaturacao(Encs.getCodPostalFaturacao());
            ec.setPaisFaturacao(Encs.getPaisFaturacao());
            ec.setPrecoTotalSIVA(Math.round(Encs.getPrecoTotalSIVA() * 100.0) / 100.0);
            ec.setIva(Encs.getIva());
            ec.setTotalIVA(Math.round(Encs.getTotalIVA() * 100.0) / 100.0);
            ec.setPrecoTotalCIVA(Math.round(Encs.getPrecoTotalCIVA() * 100.0) / 100.0);
            ec.setEstado(Encs.getEstado());

            Encomends.add(ec);

            Query query2 = getEntityManager().createNamedQuery("Encomendasproduto.findByIdEncomenda");
            query2.setParameter("idEncomenda", Encs.getIdEncomenda());
            List<Encomendasproduto> EncProds = query2.getResultList();
            for (Encomendasproduto EncProd : EncProds) {

                String codProduto = EncProd.getCodProdPa();
                Encomendasproduto ep = new Encomendasproduto();
                ep.setIdEncProd(EncProd.getIdEncProd());
                ep.setIdEncomenda(EncProd.getIdEncomenda());
                ep.setCodProdPa(EncProd.getCodProdPa());
                ep.setQuantVendida(EncProd.getQuantVendida());
                ep.setTipoVenda(EncProd.getTipoVenda());
                ep.setPrecoUnitSIVA(Math.round(EncProd.getPrecoUnitSIVA() * 100.0) / 100.0);
                ep.setPrecoUnitCIVA(Math.round(EncProd.getPrecoUnitCIVA() * 100.0) / 100.0);
                ep.setEstado(EncProd.getEstado());

                ListEP.add(ep);

                Query query3 = getEntityManager().createNamedQuery("Produto.findByCodProdPa");
                query3.setParameter("codProdPa", codProduto);
                List<Produto> produtos = query3.getResultList();
                for (Produto prods : produtos) {
                    Produto pr = new Produto();
                    pr.setCodProdPa(prods.getCodProdPa());
                    pr.setDescricao(prods.getDescricao());
                    pr.setQuantStock(prods.getQuantStock());
                    pr.setTipoQuantStock(prods.getTipoQuantStock());
                    pr.setQuantStockUnit(prods.getQuantStockUnit());
                    pr.setTipoQuantStockUnit(prods.getTipoQuantStockUnit());
                    pr.setPrecoUnitSIVA(Math.round(prods.getPrecoUnitSIVA() * 100.0) / 100.0);
                    pr.setIdCategoria(prods.getIdCategoria());
                    pr.setEstado(prods.getEstado());

                    prds.add(pr);

                    Order o = new Order(Encomends, ListEP, prds);
                    ecpr = o;

                }
            }

        }
        return ecpr;
    }

    private int getIDTaxaDesc(String idProd, long quantVe) {
        int quantVendDesc = 0;
        int idDesc = 0;

        Query query = getEntityManager().createNamedQuery("Descontosprodutos.findByCodProdPa");
        query.setParameter("codProdPa", idProd);
        List<Descontosprodutos> descProd = query.getResultList();
        for (Descontosprodutos dp : descProd) {

            if (quantVe >= dp.getQuantVenda()) {
                if (quantVendDesc <= dp.getQuantVenda()) {
                    quantVendDesc = dp.getQuantVenda();
                    idDesc = dp.getDescontosprodutosPK().getIdDesconto();

                }
            }
        }
        return idDesc;
    }

    private double getTaxaDesc(int idDesc) {
        double taxaDesc = 0;
        Query query = getEntityManager().createNamedQuery("Descontos.findByIdDesconto");
        query.setParameter("idDesconto", idDesc);
        List<Descontos> Desc = query.getResultList();
        for (Descontos d : Desc) {
            taxaDesc = d.getQuantDesconto();
        }
        return taxaDesc;
    }

    private Cliente getCliente(int idCli) {
        Cliente cli = new Cliente();

        Query query = getEntityManager().createNamedQuery("Cliente.findByIdCliente");
        query.setParameter("idCliente", idCli);
        List<Cliente> Clientes = query.getResultList();
        for (Cliente clis : Clientes) {

            cli.setIdCliente(clis.getIdCliente());
            cli.setNome(clis.getNome());
            cli.setEmail(clis.getEmail());
            cli.setMorada(clis.getMorada());
            cli.setCodPostal(clis.getCodPostal());
            cli.setPais(clis.getPais());
            cli.setContribuinte(clis.getContribuinte());
            cli.setEstado(clis.getEstado());
        }
        return cli;
    }

    private int getLastOrder() {
        int idEncLast = 0;
        Query query = getEntityManager().createNamedQuery("Encomendas.findAll");
        List<Encomendas> encGetlast = query.getResultList();
        for (Encomendas egl : encGetlast) {
            idEncLast = egl.getIdEncomenda();
        }
        return idEncLast;
    }

    private void saveOrders(String postEncProd, String tipOp) {

        try {
            //encomendas
            // HTTP Post request
            String url = "http://127.0.0.1:8080/feiraoffice/resources/encomendas/";
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            // Setting basic post request
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/" + tipOp);
            con.setRequestProperty("Accept", "application/" + tipOp);
            // Send post request
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(postEncProd);
            wr.flush();
            wr.close();
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            in.close();
            //end encomendas
        } catch (MalformedURLException ex) {
            Logger.getLogger(OrderFR.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ProtocolException ex) {
            Logger.getLogger(OrderFR.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(OrderFR.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void saveOrderdProducts(String postEncProd, String tipOp) {
        try {
            //encomendas produtos
            // HTTP Post request
            String url = "http://127.0.0.1:8080/feiraoffice/resources/encomendasproduto/";
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            // Setting basic post request
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/" + tipOp);
            con.setRequestProperty("Accept", "application/" + tipOp);
            // Send post request
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(postEncProd);
            wr.flush();
            wr.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            in.close();
            //end encomendas produtos}
        } catch (MalformedURLException ex) {
            Logger.getLogger(OrderFR.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(OrderFR.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @return a entidade order
     */
    protected EntityManager getEntityManager() {
        return em;
    }

    /**
     *
     * @param quantVe recebe uma quantidade de venda
     * @param precoUnitSIVA recebe um preço por unidade sem IVA
     * @param taxaDesc recebe um taxa de desconto
     * @return um valor do tipo double
     */
    public double precoTotalSIVA(long quantVe, double precoUnitSIVA, double taxaDesc) {
        return ((quantVe * precoUnitSIVA) * ((20 / (float) 100) + 1)) / ((taxaDesc / (float) 100) + 1);
    }

    /**
     *
     * @param quantVe recebe uma quantidade de venda
     * @param precoUnitSIVA recebe um preço por unidade sem IVA
     * @param iva recebe uma taxa de IVA
     * @param taxaDesc recebe um taxa de desconto
     * @return um valor do tipo double
     */
    public double precoTotalCIVA(long quantVe, double precoUnitSIVA, double iva, double taxaDesc) {
        return (((quantVe * precoUnitSIVA) * ((20 / (float) 100) + 1)) * ((iva / (float) 100) + 1)) / ((taxaDesc / (float) 100) + 1);
    }

    /**
     *
     * @param quantVe recebe uma quantidade de venda
     * @param precoUnitSIVA recebe um preço por unidade sem IVA
     * @param iva recebe uma taxa de IVA
     * @param taxaDesc recebe um taxa de desconto
     * @return um valor do tipo double
     */
    public double totalIVA(long quantVe, double precoUnitSIVA, double iva, double taxaDesc) {
        return (((quantVe * precoUnitSIVA) * ((20 / (float) 100) + 1)) * (iva / (float) 100)) / ((taxaDesc / (float) 100) + 1);
    }

}
