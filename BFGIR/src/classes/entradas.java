/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import bfgir.BDConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe entradas com metodos de acesso a tabela de entradas na BD
 */
public class entradas {

    private String idEntrada;
    private String idFornecedor;
    private String dataEntrada;
    private String estado;

    /**
     * Construtor da classe entradas
     */
    public entradas() {
    }

    /**
     * Construtor da classe entradas<br>
     *
     * @param idEntrada ID da entrada
     * @param idFornecedor ID do fornecedor
     * @param dataEntrada data da entrada
     * @param estado estado da entrada
     */
    public entradas(String idEntrada, String idFornecedor, String dataEntrada, String estado) {
        this.idEntrada = idEntrada;
        this.idFornecedor = idFornecedor;
        this.dataEntrada = dataEntrada;
        this.estado = estado;
    }

    /**
     * @return the idEntrada
     */
    public String getIdEntrada() {
        return idEntrada;
    }

    /**
     * @param idEntrada the idEntrada to set
     */
    public void setIdEntrada(String idEntrada) {
        this.idEntrada = idEntrada;
    }

    /**
     * @return the idFornecedor
     */
    public String getIdFornecedor() {
        return idFornecedor;
    }

    /**
     * @param idFornecedor the idFornecedor to set
     */
    public void setIdFornecedor(String idFornecedor) {
        this.idFornecedor = idFornecedor;
    }

    /**
     * @return the dataEntrada
     */
    public String getDataEntrada() {
        return dataEntrada;
    }

    /**
     * @param dataEntrada the dataEntrada to set
     */
    public void setDataEntrada(String dataEntrada) {
        this.dataEntrada = dataEntrada;
    }

    /**
     * @return the estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * Método para selecionar todos os atributos da entrada de produtos na
     * BD<br>
     *
     * Este método recebe por parametro o ID da entrada (idEnt). Posto isto faz
     * um select à BD defenindo o resultado numa variavel, se essa variavel
     * estiver vazia retorna o valor 'false' se não retorna o valor 'true'.<br>
     *
     * @param idEnt id da entrada a pesquisar
     * @return boolean para a verificação do entrada
     *
     *
     *
     */
    public static boolean verifEnt(String idEnt) {
        boolean ent = false;

        String sql = "Select * from entrada where idEntrada = (?)";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setString(1, idEnt);
            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                if ("".equals(results.getString("idEntrada"))) {
                    ent = false;
                } else {
                    ent = true;
                }

            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return ent;
    }

    /**
     * Método para inserir todos os atributos de uma entrada de produtos na
     * BD<br>
     *
     * Este método recebe por parametro o objeto entrada (ent). Posto isto faz
     * um insert à BD adicionando a um arrayList todos os atributos de um
     * fornecedor com aquele ID de Utilizador.<br>
     *
     * @param ent objeto entrada
     *
     */
    public static void inserirEntradas(entradas ent) {

        String sql = "INSERT INTO entrada (idEntrada,idFornecedor,dataEntrada,estado) VALUES (?,?,?,?)";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setString(1, ent.getIdEntrada());
            pstmt.setString(2, ent.getIdFornecedor());
            pstmt.setString(3, ent.getDataEntrada());
            pstmt.setString(4, ent.getEstado());

            pstmt.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

    }

    /**
     * Método para selecionar o ID da entrada na BD<br>
     *
     * Este método faz um select à BD adicionando a um arrayList o atributo ID
     * de entrada de uma entrada de produtos com estado pendente.<br>
     *
     * @return id da entrada
     *
     *
     */
    public static List<entradas> listarPendentes() {

        List<entradas> entradas = new ArrayList<entradas>();
        String sql = "SELECT idEntrada FROM entrada WHERE estado = 'pendente'";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                entradas et = new entradas();
                et.setIdEntrada(results.getString(1));
                entradas.add(et);
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return entradas;
    }

    /**
     * Método para Atualizar o estado da entrada na BD<br>
     *
     * Este método recebe por parametro o ID da entrada (id). Posto isto faz um
     * update à BD alterando o estado da entrada de produtos para 'aprovado'
     * para aquele ID da entrada.<br>
     *
     * @param id id da entrada a pesquisar
     *
     *
     */
    public static void atualizarPendenteEnt(String id) {

        String sql = "UPDATE entrada set estado='aprovado' WHERE idEntrada=(?)";

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, id);

            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Método para selecionar todos os atributos da entrada de produtos na
     * BD<br>
     *
     * Este método recebe por parametro o ID da entrada (idEnt). Posto isto faz
     * um select à BD adicionando a um arrayList o atributo idFornecedo de uma
     * entrada com aquele ID de entrada.<br>
     *
     * @param idEnt id da entrada a pesquisar
     * @return id do fornecedor
     *
     *
     */
    public static List<entradas> retornaIdFornecedor(String idEnt) {

        List<entradas> entrada = new ArrayList<entradas>();
        String sql = "Select * from entrada where idEntrada = (?)";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, idEnt);

            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                entradas et = new entradas();
                et.setIdFornecedor(results.getString(2));
                entrada.add(et);
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return entrada;
    }

    /**
     * Método para selecionar todos os atributos da entrada de produtos na BD<br>
     *
     * Este método recebe por parametro o ID do Fornecedor (idForn). Posto isto
     * faz um select à BD adicionando a um arrayList os atributos idEntrada e
     * dataEntrada de uma entrada com aquele ID de Fornecedor.<br>
     *
     * @param idForn id do fornecedor a pesquisar
     * @return ID e Data da entrada
     *
     */
    public static List<entradas> retornaIdEntrada(String idForn) {

        List<entradas> entradas = new ArrayList<entradas>();
        String sql = "Select * from entrada where idFornecedor = (?) and estado = 'aprovado'";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, idForn);

            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                entradas et = new entradas();
                et.setIdEntrada(results.getString(1));
                et.setDataEntrada(results.getString(3));
                entradas.add(et);
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return entradas;
    }

    /**
     * Método para pesquisar entradas por ID<br>
     *
     * Este método faz um select à BD adicionando a um arrayList todas as
     * entradas com o id escolhido.<br>
     *
     * @param codEnt codigo da entrada a pesquisar
     * @return retorna lista de entradas
     *
     *
     */
    public static List<entradas> listarEntradasId(String codEnt) {

        List<entradas> entradas = new ArrayList<>();
        String sql = "SELECT * FROM entrada WHERE idEntrada = (?)";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, codEnt);
            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                entradas et = new entradas();
                et.setIdEntrada(results.getString(1));
                et.setIdFornecedor(results.getString(2));
                et.setDataEntrada(results.getString(3));
                et.setEstado(results.getString(4));
                entradas.add(et);
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return entradas;
    }
}
