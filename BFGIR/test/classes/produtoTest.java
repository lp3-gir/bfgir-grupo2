/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import static classes.produto.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 */
public class produtoTest {

    /**
     * construtor da classe produto teste
     */
    public produtoTest() {
    }

    /**
     * set up calss
     */
    @BeforeClass
    public static void setUpClass() {

    }

    /**
     * tear down class
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * set up
     */
    @Before
    public void setUp() {
    }

    /**
     * tear down
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getCodProdPa method, of class produto.
     */
    @Test
    public void testGetCodProdPa() {
        System.out.println("getCodProdPa");
        produto instance = new produto();
        instance.setCodProdPa("123456");
        String expResult = "123456";
        String result = instance.getCodProdPa();
        if (expResult == result) {
            System.out.println("O método passou com sucesso");
        } else {
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of setCodProdPa method, of class produto.
     */
    @Test
    public void testSetCodProdPa() {
        System.out.println("setCodProdPa");
        produto instance = new produto();
        String expResult  = "123456";
        instance.setCodProdPa(expResult);
        String result = instance.getCodProdPa();
        if (expResult == result) {
            System.out.println("O método passou com sucesso");
        } else {
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of getDescricao method, of class produto.
     */
    @Test
    public void testGetDescricao() {
        System.out.println("getDescricao");
        produto instance = new produto();
        instance.setDescricao("descricao");
        String expResult = "descricao";
        String result = instance.getDescricao();
        if (expResult == result) {
            System.out.println("O método passou com sucesso");
        } else {
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of setDescricao method, of class produto.
     */
    @Test
    public void testSetDescricao() {
        System.out.println("setDescricao");
        produto instance = new produto();
        String expResult  = "descricao";
        instance.setDescricao(expResult);
        String result = instance.getDescricao();
        if (expResult == result) {
            System.out.println("O método passou com sucesso");
        } else {
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of getQuantStock method, of class produto.
     */
    @Test
    public void testGetQuantStock() {
        System.out.println("getQuantStock");
        produto instance = new produto();
        instance.setQuantStock(1);
        int expResult = 1;
        int result = instance.getQuantStock();
        if (expResult == result) {
            System.out.println("O método passou com sucesso");
        } else {
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of setQuantStock method, of class produto.
     */
    @Test
    public void testSetQuantStock() {
        System.out.println("setQauntStock");
        produto instance = new produto();
        int expResult  = 1;
        instance.setQuantStock(expResult);
        int result = instance.getQuantStock();
        if (expResult == result) {
            System.out.println("O método passou com sucesso");
        } else {
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of getTipoQuantStock method, of class produto.
     */
    @Test
    public void testGetTipoQuantStock() {
        System.out.println("getTipoQuantStock");
        produto instance = new produto();
        instance.setTipoQuantStock("tipo");
        String expResult = "tipo";
        String result = instance.getTipoQuantStock();
        if (expResult == result) {
            System.out.println("O método passou com sucesso");
        } else {
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of setTipoQuantStock method, of class produto.
     */
    @Test
    public void testSetTipoQuantStock() {
        System.out.println("setTipoQauntStock");
        produto instance = new produto();
        String expResult  = "tipo";
        instance.setTipoQuantStock(expResult);
        String result = instance.getTipoQuantStock();
        if (expResult == result) {
            System.out.println("O método passou com sucesso");
        } else {
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of getQuantStockUnit method, of class produto.
     */
    @Test
    public void testGetQuantStockUnit() {
        System.out.println("getQuantStockUnit");
        produto instance = new produto();
        instance.setQuantStockUnit(1);
        int expResult = 1;
        int result = instance.getQuantStockUnit();
        if (expResult == result) {
            System.out.println("O método passou com sucesso");
        } else {
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of setQuantStockUnit method, of class produto.
     */
    @Test
    public void testSetQuantStockUnit() {
        System.out.println("setQauntStockUnit");
        produto instance = new produto();
        int expResult  = 1;
        instance.setQuantStockUnit(expResult);
        int result = instance.getQuantStockUnit();
        if (expResult == result) {
            System.out.println("O método passou com sucesso");
        } else {
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of getTipoQuantStockUnit method, of class produto.
     */
    @Test
    public void testGetTipoQuantStockUnit() {
        System.out.println("getTipoQuantStockUnit");
        produto instance = new produto();
        instance.setTipoQuantStockUnit("tipo");
        String expResult = "tipo";
        String result = instance.getTipoQuantStockUnit();
        if (expResult == result) {
            System.out.println("O método passou com sucesso");
        } else {
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of setTipoQuantStockUnit method, of class produto.
     */
    @Test
    public void testSetTipoQuantStockUnit() {
        System.out.println("setTipoQauntStockUnit");
        produto instance = new produto();
        String expResult  = "tipo";
        instance.setTipoQuantStockUnit(expResult);
        String result = instance.getTipoQuantStockUnit();
        if (expResult == result) {
            System.out.println("O método passou com sucesso");
        } else {
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of getPrecoUnitSIVA method, of class produto.
     */
    @Test
    public void testGetPrecoUnitSIVA() {
        System.out.println("getPrecoUnitSIVA");
        produto instance = new produto();
        instance.setPrecoUnitSIVA(2.2f);
        float expResult = 2.2f;
        float result = instance.getPrecoUnitSIVA();
        if (expResult == result) {
            System.out.println("O método passou com sucesso");
        } else {
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of setPrecoUnitSIVA method, of class produto.
     */
    @Test
    public void testSetPrecoUnitSIVA() {
        System.out.println("setPrecoUnitSIVA");
        produto instance = new produto();
        float expResult  = 2.2f;
        instance.setPrecoUnitSIVA(expResult);
        float result = instance.getPrecoUnitSIVA();
        if (expResult == result) {
            System.out.println("O método passou com sucesso");
        } else {
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of getIdCategoria method, of class produto.
     */
    @Test
    public void testGetIdCategoria() {
        System.out.println("getIdCategoria");
        produto instance = new produto();
        instance.setIdCategoria(2);
        int expResult = 2;
        int result = instance.getIdCategoria();
        if (expResult == result) {
            System.out.println("O método passou com sucesso");
        } else {
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of setIdCategoria method, of class produto.
     */
    @Test
    public void testSetIdCategoria() {
        System.out.println("setIdCategoria");
        produto instance = new produto();
        int expResult  = 2;
        instance.setIdCategoria(expResult);
        int result = instance.getIdCategoria();
        if (expResult == result) {
            System.out.println("O método passou com sucesso");
        } else {
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of getEstado method, of class produto.
     */
    @Test
    public void testGetEstado() {
        System.out.println("getEstado");
        produto instance = new produto();
        instance.setEstado("estado");
        String expResult = "estado";
        String result = instance.getEstado();
        if (expResult == result) {
            System.out.println("O método passou com sucesso");
        } else {
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of setEstado method, of class produto.
     */
    @Test
    public void testSetEstado() {
        System.out.println("setEstado");
        produto instance = new produto();
        String expResult  = "estado";
        instance.setEstado(expResult);
        String result = instance.getEstado();
        if (expResult == result) {
            System.out.println("O método passou com sucesso");
        } else {
            fail("The test case is a prototype.");
        }
    }

}
