package bfgir;

import classes.helper;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author chico
 */
public class eliminarCategorias {

    private static Stage stage;

    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/ecras/eliminarCategoria.fxml"));

        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.setTitle("F&O - Eliminar Categorias");
        stage.getIcons().add(helper.appIcon);
        stage.setResizable(false);
        stage.show();
        setStage(stage);

    }

    /**
     * método main do ecrã eliminarCategoria
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * método getStage do ecrã eliminarCategorias
     *
     * @return retorna o JAVAFX container do ecrã eliminarCategoria
     */
    public static Stage getStage() {
        return stage;
    }

    /**
     * método setStage do ecrã eliminarCategoria
     *
     * @param aStage the aStage to set
     */
    public static void setStage(Stage aStage) {
        stage = aStage;
    }

}