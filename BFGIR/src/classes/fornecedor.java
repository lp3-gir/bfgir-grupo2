/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import bfgir.BDConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe fornecedor com metodos de acesso a tabela de fornecedor na BD
 */
public class fornecedor {

    private String idFornecedor;
    private int idUtilizador;
    private String nome;
    private int contribuinte;
    private String morada;
    private String codPostal;
    private String pais;
    private String estado;

    /**
     * Construtor da classe fornecedor
     */
    public fornecedor() {
    }

    /**
     * Construtor da classe fornecedor
     *
     * @param idFornecedor id do fornecedor
     * @param nome nome do fornecedor
     * @param morada morada do fornecedor
     * @param codPostal codigo postal do fornecedor
     * @param pais pais do fornecedor
     * @param estado estado do fornecedor
     * @param idUtilizador id do utilizador
     * @param contribuinte contribuinte do fornecedor
     */
    public fornecedor(String idFornecedor, String nome, String morada, String codPostal, String pais, String estado, int idUtilizador, int contribuinte) {
        this.idFornecedor = idFornecedor;
        this.nome = nome;
        this.morada = morada;
        this.codPostal = codPostal;
        this.pais = pais;
        this.estado = estado;
        this.idUtilizador = idUtilizador;
        this.contribuinte = contribuinte;
    }

    /**
     * Método get ID do fornecedor
     *
     * @return the idFornecedor
     */
    public String getIdFornecedor() {
        return idFornecedor;
    }

    /**
     * Método set ID do fornecedor
     *
     * @param idFornecedor the idFornecedor to set
     */
    public void setIdFornecedor(String idFornecedor) {
        this.idFornecedor = idFornecedor;
    }

    /**
     * Método get Nome do fornecedor
     *
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * Método set ID do fornecedor
     *
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * Método get Morada do fornecedor
     *
     * @return the morada
     */
    public String getMorada() {
        return morada;
    }

    /**
     * Método set ID do fornecedor
     *
     * @param morada the morada to set
     */
    public void setMorada(String morada) {
        this.morada = morada;
    }

    /**
     * Método get Codigo Postal do fornecedor
     *
     * @return the codPostal
     */
    public String getCodPostal() {
        return codPostal;
    }

    /**
     * Método set ID do fornecedor
     *
     * @param codPostal the codPostal to set
     */
    public void setCodPostal(String codPostal) {
        this.codPostal = codPostal;
    }

    /**
     * Método get Pais do fornecedor
     *
     * @return the pais
     */
    public String getPais() {
        return pais;
    }

    /**
     * Método set ID do fornecedor
     *
     * @param pais the pais to set
     */
    public void setPais(String pais) {
        this.pais = pais;
    }

    /**
     * Método get Estado do fornecedor
     *
     * @return the estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * Método set ID do fornecedor
     *
     * @param estado the estado to set
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * Método get ID de Utilizador do fornecedor
     *
     * @return the estado
     */
    public int getIdUtilizador() {
        return idUtilizador;
    }

    /**
     * Método set ID do fornecedor
     *
     * @param idUtilizador the idUtilizador to set
     */
    public void setIdUtilizador(int idUtilizador) {
        this.idUtilizador = idUtilizador;
    }

    /**
     * Método get contribuinte do fornecedor
     *
     * @return the contribuinte
     */
    public int getContribuinte() {
        return contribuinte;
    }

    /**
     * Método set ID do fornecedor
     *
     * @param contribuinte the contribuinte to set
     */
    public void setContribuinte(int contribuinte) {
        this.contribuinte = contribuinte;
    }

    /**
     * Método para selecionar todos os atributos do fornecedor na BD<br>
     *
     * Este método recebe por parametro o ID de Utilizador do fornecedor
     * (idUtilizador). Posto isto faz um select à BD adicionando a um arrayList
     * todos os atributos de um fornecedor com aquele ID de Utilizador.<br>
     *
     * @param idUtilizador id do utilizador
     * @return lista de fornecedores
     *
     *
     */
    public static List<fornecedor> listarFornecedor(int idUtilizador) {

        List<fornecedor> fornecedor = new ArrayList<fornecedor>();
        String sql = "SELECT * from fornecedor where idUtilizador = (?)";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setInt(1, idUtilizador);

            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                fornecedor forn = new fornecedor();

                forn.setIdFornecedor(results.getString(1));
                forn.setIdUtilizador(results.getInt(2));
                forn.setNome(results.getString(3));
                forn.setContribuinte(results.getInt(4));
                forn.setMorada(results.getString(5));
                forn.setCodPostal(results.getString(6));
                forn.setPais(results.getString(7));
                forn.setEstado(results.getString(8));

                fornecedor.add(forn);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        return fornecedor;
    }

    /**
     * Método para selecionar todos os atributos do fornecedor que tenha o
     * estado ativo na BD<br>
     *
     * Posto isto faz um select à BD adicionando a um arrayList todos os
     * atributos de um fornecedor com aquele ID de Utilizador e que o seu estado
     * seja 'ativo'.<br>
     *
     * @return lista fornecedores ativos ordenados pelo Id Utilizador
     *
     *
     *
     */
    public static List<fornecedor> listarFornecedorTodosAtivos() {

        List<fornecedor> fornecedor = new ArrayList<fornecedor>();
        String sql = "SELECT * from fornecedor WHERE estado = 'ativo' order by idUtilizador";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                fornecedor forn = new fornecedor();

                forn.setIdFornecedor(results.getString(1));
                forn.setIdUtilizador(results.getInt(2));
                forn.setNome(results.getString(3));
                forn.setContribuinte(results.getInt(4));
                forn.setMorada(results.getString(5));
                forn.setCodPostal(results.getString(6));
                forn.setPais(results.getString(7));
                forn.setEstado(results.getString(8));

                fornecedor.add(forn);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        return fornecedor;
    }

    /**
     * Método para selecionar todos os atributos do fornecedor na BD<br>
     *
     * Posto isto faz um select à BD adicionando a um arrayList todos os
     * atributos de um fornecedor ordenados pelo seu ID de Utilizador.<br>
     *
     * @return lista fornecedores ordenado pelo id utilizador
     *
     *
     *
     */
    public static List<fornecedor> listarFornecedorTodos() {

        List<fornecedor> fornecedor = new ArrayList<fornecedor>();
        String sql = "SELECT * from fornecedor order by idUtilizador";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                fornecedor forn = new fornecedor();

                forn.setIdFornecedor(results.getString(1));
                forn.setIdUtilizador(results.getInt(2));
                forn.setNome(results.getString(3));
                forn.setContribuinte(results.getInt(4));
                forn.setMorada(results.getString(5));
                forn.setCodPostal(results.getString(6));
                forn.setPais(results.getString(7));
                forn.setEstado(results.getString(8));

                fornecedor.add(forn);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        return fornecedor;
    }

    /**
     * Método para selecionar todos os atributos do fornecedor na BD<br>
     *
     * Este método recebe por parametro o ID do fornecedor (idFornecedor). Posto
     * isto faz um select à BD adicionando a um arrayList todos os atributos de
     * um fornecedor com aquele ID de Fornecedor.<br>
     *
     *
     * @param idFornecedor id de fornecedor a pesquisar
     * @return lista fornecedores
     *
     *
     *
     */
    public static List<fornecedor> procurarFornecedorID(String idFornecedor) {

        List<fornecedor> fornecedor = new ArrayList<fornecedor>();
        String sql = "SELECT * from fornecedor where idFornecedor = (?)";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setString(1, idFornecedor);

            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                fornecedor forn = new fornecedor();
                forn.setIdFornecedor(results.getString(1));
                forn.setIdUtilizador(results.getInt(2));
                forn.setNome(results.getString(3));
                forn.setContribuinte(results.getInt(4));
                forn.setMorada(results.getString(5));
                forn.setCodPostal(results.getString(6));
                forn.setPais(results.getString(7));
                forn.setEstado(results.getString(8));

                fornecedor.add(forn);
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return fornecedor;
    }

    /**
     * Método para selecionar todos os atributos do fornecedor na BD<br>
     *
     * Este método recebe por parametro o ID de Utilizador do fornecedor
     * (idUtilizador). Posto isto faz um select à BD adicionando a um arrayList
     * o atributo idFornecedor de um fornecedor com aquele ID de Utilizador.<br>
     *
     * @param idUtilizador id de utilizador a pesquisar
     * @return id fornecedor
     *
     *
     *
     */
    public static List<fornecedor> compararIDFornecedor(int idUtilizador) {

        List<fornecedor> fornecedor = new ArrayList<fornecedor>();
        String sql = "SELECT * from fornecedor where idUtilizador = (?)";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setInt(1, idUtilizador);

            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                fornecedor forn = new fornecedor();

                forn.setIdFornecedor(results.getString(1));

                fornecedor.add(forn);
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return fornecedor;
    }

    /**
     * Método para selecionar todos os atributos do fornecedor na BD<br>
     *
     * Este método recebe por parametro o contribuinte do fornecedor
     * (contribuinte). Posto isto faz um select à BD adicionando a um arrayList
     * todos os atributos de um fornecedor com aquele contribuinte.<br>
     *
     * @param contribuinte contribuinte do fornecedor a pesquisar
     * @return lista fornecedores
     *
     *
     *
     */
    public static List<fornecedor> procurarFornecedorNIF(int contribuinte) {

        List<fornecedor> fornecedor = new ArrayList<fornecedor>();
        String sql = "SELECT * from fornecedor where contribuinte = (?)";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setInt(1, contribuinte);

            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                fornecedor forn = new fornecedor();

                forn.setIdFornecedor(results.getString(1));
                forn.setIdUtilizador(results.getInt(2));
                forn.setNome(results.getString(3));
                forn.setContribuinte(results.getInt(4));
                forn.setMorada(results.getString(5));
                forn.setCodPostal(results.getString(6));
                forn.setPais(results.getString(7));
                forn.setEstado(results.getString(8));

                fornecedor.add(forn);
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return fornecedor;
    }

    /**
     * Método para selecionar o objeto todo da tabela encomenda pelo ID de
     * Fornecedor na BD<br>
     *
     * Este método recebe por parametro o ID do Fornecedor (idFornecedor). Posto
     * isto faz um select à BD adicionando a um arrayList o atributo
     * idFornecedor de um fornecedor com aquele ID de fornecedor.<br>
     *
     * @param idFornecedor ide de fornecedor a pesquisar
     * @return id de fornecedor
     *
     *
     *
     */
    public static List<fornecedor> verificarFornecedorEntrada(String idFornecedor) {

        List<fornecedor> fornecedor = new ArrayList<fornecedor>();
        String sql = "SELECT * from entrada where idFornecedor = (?)";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setString(1, idFornecedor);

            ResultSet results = pstmt.executeQuery();

            while (results.next()) {
                fornecedor forn = new fornecedor();

                forn.setIdFornecedor(results.getString(1));

                fornecedor.add(forn);
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return fornecedor;
    }

    /**
     * Método para inserir um fornecedor na BD<br>
     *
     * Este método recebe por parametro um objeto do tipo Fornecedor (forn).
     * Posto isto faz um insert à BD defenindo cada atributo do fornecedor que
     * pertença aquele objeto.<br>
     *
     * @param forn obejeto do tipo fornecedor
     *
     *
     *
     */
    public static void inserirFornecedores(fornecedor forn) {

        List<utilizador> utilizadores = new ArrayList<utilizador>();
        String sql = "INSERT INTO fornecedor (idFornecedor,idUtilizador,nome,contribuinte,morada,codPostal,pais,estado) VALUES (?,?,?,?,?,?,?,?)";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setString(1, forn.getIdFornecedor());
            pstmt.setInt(2, forn.getIdUtilizador());
            pstmt.setString(3, forn.getNome());
            pstmt.setInt(4, forn.getContribuinte());
            pstmt.setString(5, forn.getMorada());
            pstmt.setString(6, forn.getCodPostal());
            pstmt.setString(7, forn.getPais());
            pstmt.setString(8, forn.getEstado());
            pstmt.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

    }

    /**
     * Método para apagar um fornecedor da BD<br>
     *
     * Este método recebe por parametro o ID de Utilizador do fornecedor
     * (idUtilizador). Posto isto o método apaga da tabela fornecedor o
     * fornecedor que tenha aquele ID de Utilizador.<br>
     *
     * @param id_utilizador ide de utilizador
     *
     *
     *
     */
    public static void eliminarFornecedorr(int id_utilizador) {

        List<utilizador> utilizadores = new ArrayList<utilizador>();
        String sql = "DELETE FROM fornecedor WHERE idUtilizador = (?)";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setInt(1, id_utilizador);

            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Metodo para alterar o nome onde o fornecedor tenha aquele idUtilizador
     *
     * @param nome variavel com o nome a alterar
     * @param id_utilizador variavel com o id de utilizador a pesquisar
     */
    public static void alterarNomeFornecedor(String nome, int id_utilizador) {

        List<utilizador> utilizadores = new ArrayList<utilizador>();
        String sql = "UPDATE fornecedor SET nome = (?) WHERE idUtilizador = (?) ";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setString(1, nome);
            pstmt.setInt(2, id_utilizador);

            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Metodo para alterar a morada onde o fornecedor tenha aquele idUtilizador
     *
     * @param morada variavel com o nome a alterar
     * @param id_utilizador variavel com o id de utilizador a pesquisar
     */
    public static void alterarMoradaFornecedor(String morada, int id_utilizador) {

        List<utilizador> utilizadores = new ArrayList<utilizador>();
        String sql = "UPDATE fornecedor SET morada = (?) WHERE idUtilizador = (?) ";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setString(1, morada);
            pstmt.setInt(2, id_utilizador);

            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Metodo para alterar o país onde o fornecedor tenha aquele idUtilizador
     *
     * @param pais variavel com o país a alterar
     * @param id_utilizador variavel com o id de utilizador a pesquisar
     */
    public static void alterarPaisFornecedor(String pais, int id_utilizador) {

        List<utilizador> utilizadores = new ArrayList<utilizador>();
        String sql = "UPDATE fornecedor SET pais = (?) WHERE idUtilizador = (?) ";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setString(1, pais);
            pstmt.setInt(2, id_utilizador);

            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Metodo para alterar o código postal onde o fornecedor tenha aquele
     * idUtilizador
     *
     * @param cp variavel com o nome a alterar
     * @param id_utilizador variavel com o id de utilizador a pesquisar
     */
    public static void alterarCodPosFornecedor(String cp, int id_utilizador) {

        List<utilizador> utilizadores = new ArrayList<utilizador>();
        String sql = "UPDATE fornecedor SET codPostal = (?) WHERE idUtilizador = (?) ";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setString(1, cp);
            pstmt.setInt(2, id_utilizador);

            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

}
