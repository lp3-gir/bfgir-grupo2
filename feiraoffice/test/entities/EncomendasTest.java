/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ruben
 */
public class EncomendasTest {
    


    /**
     * Teste para retirar o id da encomenda.
     */
    @Test
    public void testGetIdEncomenda() {
        System.out.println("getIdEncomenda");
        Encomendas instance = new Encomendas();
        instance.setIdEncomenda(1);
        Integer expResult = 1;
        assertEquals(expResult, instance.getIdEncomenda());
        
    }

    /**
     * Teste para definir o id da encomenda.
     */
    @Test
    public void testSetIdEncomenda() {
        System.out.println("setIdEncomenda");
        Encomendas instance = new Encomendas();
        instance.setIdEncomenda(1);
        
    }

    /**
     * Teste para retirar o id do cliente associado a encomenda.
     */
    @Test
    public void testGetIdCliente() {
        System.out.println("getIdCliente");
        Encomendas instance = new Encomendas();
        instance.setIdCliente(1);
        int expResult = 1;
        assertEquals(expResult, instance.getIdCliente());
        
    }

    /**
     * Teste para definir o id do cliente associado a encomenda.
     */
    @Test
    public void testSetIdCliente() {
        System.out.println("setIdCliente");
        Encomendas instance = new Encomendas();
        instance.setIdCliente(1);
        
    }

    /**
     * Teste para retirar a data da encomenda.
     */
    @Test
    public void testGetDataEncomenda() {
        System.out.println("getDataEntrada");
        Encomendas instance = new Encomendas();
        instance.setDataEncomenda("11/01/2021");
        assertEquals("11/01/2021", instance.getDataEncomenda());
        
    }

    /**
     * Teste para definir a data de encomenda.
     */
    @Test
    public void testSetDataEncomenda() {
        System.out.println("setDataEntrada");
        Encomendas instance = new Encomendas();
        instance.setDataEncomenda("11/01/2021");
        
    }

    /**
     * Teste para retirar a morada de entrega da encomenda.
     */
    @Test
    public void testGetMoradaEntrega() {
        System.out.println("getMoradaEntrega");
        Encomendas instance = new Encomendas();
        instance.setMoradaEntrega("Rua de teste");
        assertEquals("Rua de teste", instance.getMoradaEntrega());
        
    }

    /**
     * Teste para definir a morada de entrega da encomenda.
     */
    @Test
    public void testSetMoradaEntrega() {
        System.out.println("setMoradaEntrega");
        Encomendas instance = new Encomendas();
        instance.setMoradaEntrega("Rua de teste");
        
    }

    /**
     * Teste para retirar o código postal de entrega da encomenda.
     */
    @Test
    public void testGetCodPostalEntrega() {
        System.out.println("getCodPostalEntrega");
        Encomendas instance = new Encomendas();
        instance.setCodPostalEntrega("4415-900");
        assertEquals("4415-900", instance.getCodPostalEntrega());
        
    }

    /**
     * Teste para definir o código postal de entrega da encomenda.
     */
    @Test
    public void testSetCodPostalEntrega() {
        System.out.println("setCodPostalEntrega");
        Encomendas instance = new Encomendas();
        instance.setCodPostalEntrega("4415-900");
        
    }

    /**
     * Teste para retirar o pais de entrega da encomenda.
     */
    @Test
    public void testGetPaisEntrega() {
        System.out.println("getPaisEntrega");
        Encomendas instance = new Encomendas();
        instance.setPaisEntrega("Portugal");
        assertEquals("Portugal", instance.getPaisEntrega());
        
    }

    /**
     * Teste para definir o pais de entrega da encomenda.
     */
    @Test
    public void testSetPaisEntrega() {
        System.out.println("setPaisEntrega");
        Encomendas instance = new Encomendas();
        instance.setPaisEntrega("Portugal");
        
    }

    /**
     * Teste para retirar a morada de faturação da encomenda.
     */
    @Test
    public void testGetMoradaFaturacao() {
        System.out.println("getMoradaFaturacao");
        Encomendas instance = new Encomendas();
        instance.setMoradaFaturacao("Rua dos testes");
        assertEquals("Rua dos testes", instance.getMoradaFaturacao());
        
    }

    /**
     * Teste para definir a morada de faturação da encomenda.
     */
    @Test
    public void testSetMoradaFaturacao() {
        System.out.println("setMoradaFaturacao");
        Encomendas instance = new Encomendas();
        instance.setMoradaFaturacao("Rua dos testes");
        
    }

    /**
     * Teste para retirar o código postal de faturação da encomenda.
     */
    @Test
    public void testGetCodPostalFaturacao() {
        System.out.println("getCodPostalFaturacao");
        Encomendas instance = new Encomendas();
        instance.setCodPostalFaturacao("4415-900");
        assertEquals("4415-900", instance.getCodPostalFaturacao());
        
    }

    /**
     * Teste para definir o código postal de faturação da encomenda.
     */
    @Test
    public void testSetCodPostalFaturacao() {
        System.out.println("setCodPostalFaturacao");
        Encomendas instance = new Encomendas();
        instance.setCodPostalFaturacao("4415-900");
        
    }

    /**
     * Teste para retirar o pais de faturação da encomenda.
     */
    @Test
    public void testGetPaisFaturacao() {
        System.out.println("getPaisFaturacao");
        Encomendas instance = new Encomendas();
        instance.setPaisFaturacao("Portugal");
        assertEquals("Portugal", instance.getPaisFaturacao());
        
    }

    /**
     * Teste para definir o pais de faturação da encomenda.
     */
    @Test
    public void testSetPaisFaturacao() {
        System.out.println("setPaisFaturacao");
        String paisFaturacao = "";
        Encomendas instance = new Encomendas();
        instance.setPaisFaturacao(paisFaturacao);
        
    }

    /**
     * Teste para retirar o preço total da encomenda sem IVA.
     */
    @Test
    public void testGetPrecoTotalSIVA() {
        System.out.println("getPrecoTotalSIVA");
        Encomendas instance = new Encomendas();
        instance.setPrecoTotalSIVA(50.3);
        assertEquals(50.3, instance.getPrecoTotalSIVA(), 0.0);
        
    }

    /**
     * Teste para definir o preço total da encomenda sem IVA.
     */
    @Test
    public void testSetPrecoTotalSIVA() {
        System.out.println("setPrecoTotalSIVA");
        Encomendas instance = new Encomendas();
        instance.setPrecoTotalSIVA(50.3);
        
    }

    /**
     * Teste para retirar o IVA da encomenda.
     */
    @Test
    public void testGetIva() {
        System.out.println("getIva");
        Encomendas instance = new Encomendas();
        instance.setIva(23);
        assertEquals(23, instance.getIva());
        
    }

    /**
     * Teste para definir o preço total da encomenda sem IVA.
     */
    @Test
    public void testSetIva() {
        System.out.println("setIva");
        Encomendas instance = new Encomendas();
        instance.setIva(23);
        
    }

    /**
     * Teste para retirar o total de IVA da encomenda.
     */
    @Test
    public void testGetTotalIVA() {
        System.out.println("getTotalIVA");
        Encomendas instance = new Encomendas();
        instance.setTotalIVA(20);
        assertEquals(20, instance.getTotalIVA(), 0.0);
        
    }

    /**
     * Teste para definir o total de IVA da encomenda.
     */
    @Test
    public void testSetTotalIVA() {
        System.out.println("setTotalIVA");
        Encomendas instance = new Encomendas();
        instance.setTotalIVA(20);
        
    }

    /**
     * Teste para retirar o preço total da encomenda com IVA.
     */
    @Test
    public void testGetPrecoTotalCIVA() {
        System.out.println("getPrecoTotalCIVA");
        Encomendas instance = new Encomendas();
        instance.setPrecoTotalCIVA(70);
        assertEquals(70, instance.getPrecoTotalCIVA(), 0.0);
        
    }

    /**
     * Teste para definir o total de IVA da encomenda.
     */
    @Test
    public void testSetPrecoTotalCIVA() {
        System.out.println("setPrecoTotalCIVA");
        Encomendas instance = new Encomendas();
        instance.setPrecoTotalCIVA(70);
        
    }

    /**
     * Teste para retirar o estado da encomenda.
     */
    @Test
    public void testGetEstado() {
        System.out.println("getEstado");
        Encomendas instance = new Encomendas();
        instance.setEstado("ativa");
        assertEquals("ativa", instance.getEstado());
        
    }

    /**
     * Teste para definir o estado da encomenda.
     */
    @Test
    public void testSetEstado() {
        System.out.println("setEstado");
        Encomendas instance = new Encomendas();
        instance.setEstado("ativa");
        
    }
    
}
