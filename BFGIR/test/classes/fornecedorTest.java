
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 */
public class fornecedorTest {

    /**
     * construtor da classe fornecedor teste
     */
    public fornecedorTest() {
    }

    /**
     * set up class
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     * tear down class
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * set up
     */
    @Before
    public void setUp() {
    }

    /**
     * tear down
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of listarFornecedor method, of class fornecedor.
     */
    @Test
    public void testListarFornecedor() {
        System.out.println("listarFornecedor");
        int idUtilizador = 6;
        List<fornecedor> detalhes = fornecedor.listarFornecedor(idUtilizador);
        boolean result = detalhes.isEmpty();
        assertFalse(result);
        System.out.println("Resultado: " + result);

    }

    /**
     * Test of listarFornecedorTodosAtivos method, of class fornecedor.
     */
    @Test
    public void testListarFornecedorTodosAtivos() {
        System.out.println("listarFornecedorTodosAtivos");
        List<fornecedor> detalhes = fornecedor.listarFornecedorTodosAtivos();
        boolean result = detalhes.isEmpty();
        assertFalse(result);
        System.out.println("Result: " + result);

    }

    /**
     * Test of listarFornecedorTodos method, of class fornecedor.
     */
    @Test
    public void testListarFornecedorTodos() {
        System.out.println("listarFornecedorTodos");
        List<fornecedor> forn = fornecedor.listarFornecedorTodos();
        boolean result = forn.isEmpty();
        assertFalse(result);
        System.out.println("Resultado: " + result);

    }

    /**
     * Test of procurarFornecedorID method, of class fornecedor.
     */
    @Test
    public void testProcurarFornecedorID() {
        System.out.println("procurarFornecedorID");
        String idFornecedor = "188707021";
        List<fornecedor> detalhes = fornecedor.procurarFornecedorID(idFornecedor);
        boolean result = detalhes.isEmpty();
        assertFalse(result);
        System.out.println("Resultado: " + result);

    }

    /**
     * Test of compararIDFornecedor method, of class fornecedor.
     */
    @Test
    public void testCompararIDFornecedor() {
        System.out.println("compararIDFornecedor");
        int idUtilizador = 6;
        List<fornecedor> detalhes = fornecedor.compararIDFornecedor(idUtilizador);
        boolean result = detalhes.isEmpty();
        assertFalse(result);
        System.out.println("Resultado: " + result);

    }

    /**
     * Test of procurarFornecedorNIF method, of class fornecedor.
     */
    @Test
    public void testProcurarFornecedorNIF() {
        System.out.println("procurarFornecedorNIF");
        int contribuinte = 123456789;
        List<fornecedor> detalhes = fornecedor.procurarFornecedorNIF(contribuinte);
        boolean result = detalhes.isEmpty();
        assertFalse(result);
        System.out.println("Reusltado: " + result);

    }

}
