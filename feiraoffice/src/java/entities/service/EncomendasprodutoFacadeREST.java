/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.service;

import entities.Encomendasproduto;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author ruben
 */
@Stateless
@Path("/encomendasproduto/")
public class EncomendasprodutoFacadeREST extends AbstractFacade<Encomendasproduto> {

    @PersistenceContext(unitName = "feiraofficePU")
    private EntityManager em;

    /**
     *
     */
    public EncomendasprodutoFacadeREST() {
        super(Encomendasproduto.class);
    }

    /**
     *
     * @param entity recebe uma entidade do tipo encomendasproduto
     */
    @POST
    @Override
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void create(Encomendasproduto entity) {
        super.create(entity);
    }

    /**
     *
     * @param id recebe um id de encomendasproduto
     * @param entity recebe uma entidade do tipo encomendasproduto
     */
    @PUT
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void edit(@PathParam("id") Integer id, Encomendasproduto entity) {
        super.edit(entity);
    }

    /**
     *
     * @param id recebe um id de encomendasproduto
     */
    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        super.remove(super.find(id));
    }

    /**
     *
     * @param id recebe um id de encomendasproduto
     * @return um objeto do tipo encomendasproduto
     */
    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Encomendasproduto find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    /**
     *
     * @return uma lista de objetos do tipo encomendasproduto
     */
    @GET
    @Override
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Encomendasproduto> findAll() {
        return super.findAll();
    }

    /**
     *
     * @return a entidade encomendasproduto
     */
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
