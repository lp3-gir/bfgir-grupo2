/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import bfgir.BDConnection;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static classes.entradasProdutos.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 *
 * @author bruno
 */
public class entradasProdutosTest {

    private static final entradasProdutos etProd = new entradasProdutos();
    private entradasProdutos resultado = new entradasProdutos();
    private List<entradasProdutos> listaResultados = new ArrayList();

    /**
     *
     */
    public entradasProdutosTest() {
    }

    /**
     * set up class
     */
    @BeforeClass
    public static void setUpClass() {

        etProd.setIdEntrada("testId");
        etProd.setCodProdPa("testCodProdPa");
        etProd.setPrecoUnitSIVA((float) 562.6);
        etProd.setTipoPrecos("testTipoPreto");
        etProd.setPrecoTotalSIVA((float) 592.29);
        etProd.setIva(23);
        etProd.setTotalIVA((float) 160.5);
        etProd.setLocalTax("testLocalTax");
        etProd.setQuantVendida(0);
        etProd.setTipoVenda("testTipoVenda");
        etProd.setPesoTotalVenda(200);
        etProd.setTipoPeso("testTipoPeso");
        etProd.setQuantTotalVendaUni(9000);
        etProd.setTipoVendaUni("testTipoVendaUni");
        etProd.setPrecoTotalCIVA(6262);
        etProd.setEstado("testEstado");
        inserirEntradasProdutos(etProd);
    }

    /**
     * tear down class
     */
    @AfterClass
    public static void tearDownClass() {

        String sql = "Delete FROM encomendaproduto where idEntrada = (?)";

        PreparedStatement stmt = null;

        try (Connection conn = BDConnection.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setString(1, etProd.getIdEntrada());
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(entradasProdutosTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * set up
     */
    @Before
    public void setUp() {
    }

    /**
     * tear down
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getIdEntrada method, of class entradas.
     */
    @Test
    public void testGetIdEntrada() {
        System.out.println("getIdEntrada");
        entradasProdutos instance = new entradasProdutos();
        String expResult = "12";
        instance.setIdEntrada("12");
        String result = instance.getIdEntrada();
        if (result.equals(expResult)) {
            System.out.println("sucesso");
        } else {
            // TODO review the generated test code and remove the default call to fail.
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of setIdEntrada method, of class entradas.
     */
    @Test
    public void testSetIdEntrada() {
        System.out.println("setIdEntrada");
        entradasProdutos instance = new entradasProdutos();
        String expResult = "12";
        instance.setIdEntrada(expResult);
        String result = instance.getIdEntrada();
        if (result.equals(expResult)) {
            System.out.println("sucesso");
        } else {
            // TODO review the generated test code and remove the default call to fail.
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of getCodProdPa method, of class entradasProdutos.
     */
    @Test
    public void testGetCodProdPa() {
        System.out.println("getCodProdPa");
        entradasProdutos instance = new entradasProdutos();
        String expResult = "cod";
        instance.setCodProdPa("cod");
        String result = instance.getCodProdPa();
        if (result.equals(expResult)) {
            System.out.println("sucesso");
        } else {
            // TODO review the generated test code and remove the default call to fail.
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of setCodProdPa method, of class entradasProdutos.
     */
    @Test
    public void testSetCodProdPa() {
        System.out.println("setCodProdPa");
        entradasProdutos instance = new entradasProdutos();
        String expResult = "cod";
        instance.setCodProdPa(expResult);
        String result = instance.getCodProdPa();
        if (result.equals(expResult)) {
            System.out.println("sucesso");
        } else {
            // TODO review the generated test code and remove the default call to fail.
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of getPrecoUnitSIVA method, of class entradasProdutos.
     */
    @Test
    public void testGetPrecoUnitSIVA() {
        System.out.println("getPrecoUnitSIVA");
        entradasProdutos instance = new entradasProdutos();
        float expResult = 0.0f;
        instance.setPrecoUnitSIVA(0.0f);
        float result = instance.getPrecoUnitSIVA();
        if (Objects.equals(result, expResult)) {
            System.out.println("sucesso");
        } else {
            // TODO review the generated test code and remove the default call to fail.
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of setPrecoUnitSIVA method, of class entradasProdutos.
     */
    @Test
    public void testSetPrecoUnitSIVA() {
        System.out.println("setPrecoUnitSIVA");
        entradasProdutos instance = new entradasProdutos();
        float expResult = 0.0f;
        instance.setPrecoUnitSIVA(expResult);
        float result = instance.getPrecoUnitSIVA();
        if (Objects.equals(result, expResult)) {
            System.out.println("sucesso");
        } else {
            // TODO review the generated test code and remove the default call to fail.
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of getTipoPrecos method, of class entradasProdutos.
     */
    @Test
    public void testGetTipoPrecos() {
        System.out.println("getTipoPrecos");
        entradasProdutos instance = new entradasProdutos();
        String expResult = "caro";
        instance.setTipoPrecos("caro");
        String result = instance.getTipoPrecos();
        if (result.equals(expResult)) {
            System.out.println("sucesso");
        } else {
            // TODO review the generated test code and remove the default call to fail.
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of setTipoPrecos method, of class entradasProdutos.
     */
    @Test
    public void testSetTipoPrecos() {
        System.out.println("setTipoPrecos");
        entradasProdutos instance = new entradasProdutos();
        String expResult = "caro";
        instance.setTipoPrecos(expResult);
        String result = instance.getTipoPrecos();
        if (result.equals(expResult)) {
            System.out.println("sucesso");
        } else {
            // TODO review the generated test code and remove the default call to fail.
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of getPrecoTotalSIVA method, of class entradasProdutos.
     */
    @Test
    public void testGetPrecoTotalSIVA() {
        System.out.println("getPrecoTotalSIVA");
        entradasProdutos instance = new entradasProdutos();
        float expResult = 0.0f;
        instance.setPrecoTotalSIVA(0.0f);
        float result = instance.getPrecoTotalSIVA();
        if (Objects.equals(result, expResult)) {
            System.out.println("sucesso");
        } else {
            // TODO review the generated test code and remove the default call to fail.
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of setPrecoTotalSIVA method, of class entradasProdutos.
     */
    @Test
    public void testSetPrecoTotalSIVA() {
        System.out.println("setPrecoTotalSIVA");
        entradasProdutos instance = new entradasProdutos();
        float expResult = 0.0f;
        instance.setPrecoTotalSIVA(expResult);
        float result = instance.getPrecoTotalSIVA();
        if (Objects.equals(result, expResult)) {
            System.out.println("sucesso");
        } else {
            // TODO review the generated test code and remove the default call to fail.
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of getIva method, of class entradasProdutos.
     */
    @Test
    public void testGetIva() {
        System.out.println("getIva");
        entradasProdutos instance = new entradasProdutos();
        int expResult = 2;
        instance.setIva(2);
        int result = instance.getIva();
        if (result == expResult) {
            System.out.println("sucesso");
        } else {
            // TODO review the generated test code and remove the default call to fail.
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of setIva method, of class entradasProdutos.
     */
    @Test
    public void testSetIva() {
        System.out.println("getIva");
        entradasProdutos instance = new entradasProdutos();
        int expResult = 2;
        instance.setIva(expResult);
        int result = instance.getIva();
        if (result == expResult) {
            System.out.println("sucesso");
        } else {
            // TODO review the generated test code and remove the default call to fail.
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of getTotalIVA method, of class entradasProdutos.
     */
    @Test
    public void testGetTotalIVA() {
        System.out.println("getTotalIVA");
        entradasProdutos instance = new entradasProdutos();
        float expResult = 0.0F;
        instance.setTotalIVA(0.0f);
        float result = instance.getTotalIVA();
        if (Objects.equals(result, expResult)) {
            System.out.println("sucesso");
        } else {
            // TODO review the generated test code and remove the default call to fail.
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of setTotalIVA method, of class entradasProdutos.
     */
    @Test
    public void testSetTotalIVA() {
        System.out.println("setTotalIVA");
        entradasProdutos instance = new entradasProdutos();
        float expResult = 0.0F;
        instance.setTotalIVA(expResult);
        float result = instance.getTotalIVA();
        if (Objects.equals(result, expResult)) {
            System.out.println("sucesso");
        } else {
            // TODO review the generated test code and remove the default call to fail.
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of getLocalTax method, of class entradasProdutos.
     */
    @Test
    public void testGetLocalTax() {
        System.out.println("getLocalTax");
        entradasProdutos instance = new entradasProdutos();
        String expResult = "alto";
        instance.setLocalTax("alto");
        String result = instance.getLocalTax();
        if (result.equals(expResult)) {
            System.out.println("sucesso");
        } else {
            // TODO review the generated test code and remove the default call to fail.
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of setLocalTax method, of class entradasProdutos.
     */
    @Test
    public void testSetLocalTax() {
        System.out.println("setLocalTax");
        entradasProdutos instance = new entradasProdutos();
        String expResult = "alto";
        instance.setLocalTax(expResult);
        String result = instance.getLocalTax();
        if (result.equals(expResult)) {
            System.out.println("sucesso");
        } else {
            // TODO review the generated test code and remove the default call to fail.
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of getQuantVendida method, of class entradasProdutos.
     */
    @Test
    public void testGetQuantVendida() {
        System.out.println("getQuantVendida");
        entradasProdutos instance = new entradasProdutos();
        int expResult = 3;
        instance.setQuantVendida(3);
        int result = instance.getQuantVendida();
        if (result == expResult) {
            System.out.println("sucesso");
        } else {
            // TODO review the generated test code and remove the default call to fail.
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of setQuantVendida method, of class entradasProdutos.
     */
    @Test
    public void testSetQuantVendida() {
        System.out.println("setQuantVendida");
        entradasProdutos instance = new entradasProdutos();
        int expResult = 3;
        instance.setQuantVendida(expResult);
        int result = instance.getQuantVendida();
        if (result == expResult) {
            System.out.println("sucesso");
        } else {
            // TODO review the generated test code and remove the default call to fail.
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of getPesoTotalVenda method, of class entradasProdutos.
     */
    @Test
    public void testGetPesoTotalVenda() {
        System.out.println("getPesoTotalVenda");
        entradasProdutos instance = new entradasProdutos();
        float expResult = 0.0F;
        instance.setPesoTotalVenda(0.0f);
        float result = instance.getPesoTotalVenda();
        if (Objects.equals(result, expResult)) {
            System.out.println("sucesso");
        } else {
            // TODO review the generated test code and remove the default call to fail.
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of setPesoTotalVenda method, of class entradasProdutos.
     */
    @Test
    public void testSetPesoTotalVenda() {
        System.out.println("setPesoTotalVenda");
        entradasProdutos instance = new entradasProdutos();
        float expResult = 0.0F;
        instance.setPesoTotalVenda(expResult);
        float result = instance.getPesoTotalVenda();
        if (Objects.equals(result, expResult)) {
            System.out.println("sucesso");
        } else {
            // TODO review the generated test code and remove the default call to fail.
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of getTipoPeso method, of class entradasProdutos.
     */
    @Test
    public void testGetTipoPeso() {
        System.out.println("getTipoPeso");
        entradasProdutos instance = new entradasProdutos();
        String expResult = "tipo";
        instance.setTipoPeso("tipo");
        String result = instance.getTipoPeso();
        if (result.equals(expResult)) {
            System.out.println("sucesso");
        } else {
            // TODO review the generated test code and remove the default call to fail.
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of setTipoPeso method, of class entradasProdutos.
     */
    @Test
    public void testSetTipoPeso() {
        System.out.println("setTipoPeso");
        entradasProdutos instance = new entradasProdutos();
        String expResult = "tipo";
        instance.setTipoPeso(expResult);
        String result = instance.getTipoPeso();
        if (result.equals(expResult)) {
            System.out.println("sucesso");
        } else {
            // TODO review the generated test code and remove the default call to fail.
            fail("The test case is a prototype.");

        }
    }

    /**
     * Test of getQuantTotalVendaUni method, of class entradasProdutos.
     */
    @Test
    public void testGetQuantTotalVendaUni() {
        System.out.println("getQuantTotalVendaUni");
        entradasProdutos instance = new entradasProdutos();
        int expResult = 2;
        instance.setQuantTotalVendaUni(2);
        int result = instance.getQuantTotalVendaUni();
        if (result == expResult) {
            System.out.println("sucesso");
        } else {
            // TODO review the generated test code and remove the default call to fail.
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of setQuantTotalVendaUni method, of class entradasProdutos.
     */
    @Test
    public void testSetQuantTotalVendaUni() {
        System.out.println("setQuantTotalVendaUni");
        entradasProdutos instance = new entradasProdutos();
        int expResult = 2;
        instance.setQuantTotalVendaUni(expResult);
        int result = instance.getQuantTotalVendaUni();
        if (result == expResult) {
            System.out.println("sucesso");
        } else {
            // TODO review the generated test code and remove the default call to fail.
            fail("The test case is a prototype.");
        }
    }



    /**
     * Test of getPrecoTotalCIVA method, of class entradasProdutos.
     */
    @Test
    public void testGetPrecoTotalCIVA() {
        System.out.println("getPrecoTotalCIVA");
        entradasProdutos instance = new entradasProdutos();
        float expResult = 2.0F;
        instance.setPrecoTotalCIVA(2.0f);
        float result = instance.getPrecoTotalCIVA();
        if (Objects.equals(result, expResult)) {
            System.out.println("sucesso");
        } else {
            // TODO review the generated test code and remove the default call to fail.
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of setPrecoTotalCIVA method, of class entradasProdutos.
     */
    @Test
    public void testSetPrecoTotalCIVA() {
        System.out.println("setPrecoTotalCIVA");
        entradasProdutos instance = new entradasProdutos();
        float expResult = 2.0F;
        instance.setPrecoTotalCIVA(expResult);
        float result = instance.getPrecoTotalCIVA();
        if (Objects.equals(result, expResult)) {
            System.out.println("sucesso");
        } else {
            // TODO review the generated test code and remove the default call to fail.
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of getEstado method, of class entradasProdutos.
     */
    @Test
    public void testGetEstado() {
        System.out.println("getEstado");
        entradasProdutos instance = new entradasProdutos();
        String expResult = "ativo";
        instance.setEstado("ativo");
        String result = instance.getEstado();
        if (result.equals(expResult)) {
            System.out.println("sucesso");
        } else {
            // TODO review the generated test code and remove the default call to fail.
            fail("The test case is a prototype.");
        }
    }

    /**
     * Test of setEstado method, of class entradasProdutos.
     */
    @Test
    public void testSetEstado() {
        System.out.println("setEstado");
        entradasProdutos instance = new entradasProdutos();
        String expResult = "ativo";
        instance.setEstado(expResult);
        String result = instance.getEstado();
        if (result.equals(expResult)) {
            System.out.println("sucesso");
        } else {
            // TODO review the generated test code and remove the default call to fail.
            fail("The test case is a prototype.");
        }
    }

}
