/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Classe de testes da classe cliente.
 *
 * @author Ilídio Magalhães
 */
public class clienteTest {

    private static cliente cli = new cliente();

    /**
     * Construtor da classe clienteTest.
     */
    public clienteTest() {
    }

    /**
     * Método para estabelecer os atributos do objeto cli.
     */
    @Before
    public void setUp() {
        cli.setIdCliente(0);
        cli.setNome("nome");
        cli.setEmail("email@email.com");
        cli.setMorada("rua");
        cli.setCodPostal("0000-000");
        cli.setPais("JUNIT");
        cli.setContribuinte(923496789);
        cli.setEstado("inativo");
    }

    /**
     * Teste ao método setEmail da classe cliente.<br>
     * Método modifica o email do objeto cli e depois verifica se o consegiu
     * alterar.<br>
     * O teste é válido caso tal se verifique.<br>
     * No fim, restaura o atributo ao estado inicial.
     */
    @Test
    public void testSetEmail() {
        System.out.println("\nTeste ao método setEmail");
        String mailI = "set@set.cliente.com";
        String mailF = cli.getEmail();
        cli.setEmail(mailI);
        boolean result = false;
        if (mailI.equals(cli.getEmail())) {
            result = true;
            System.out.println("Teste ao método setEmail passado com um sucesso!\n");
        }
        assertTrue(result);
        cli.setEmail(mailF);
    }

    /**
     * Teste ao método getEmail da classe cliente.<br>
     * Método tenta buscar o email do objeto previamente criado na classe.<br>
     * O teste é válido caso o valor obtido seja igual ao valor esperado.
     */
    @Test
    public void testGetEmail() {
        System.out.println("\nTeste ao método getEmail");
        String expResult = "email@email.com";
        String result = cli.getEmail();
        if (expResult.equals(result)) {
            System.out.println("Teste ao método getEmail passado com um sucesso!\n");
        }
        assertEquals(expResult, result);
    }

    /**
     * Teste ao método getIdCliente da classe cliente.<br>
     * Método tenta buscar o id do objeto previamente criado na classe.<br>
     * O teste é válido caso o valor obtido seja igual ao valor esperado.
     */
    @Test
    public void testGetIdCliente() {
        System.out.println("\nTeste ao método getIdCliente");
        int expResult = 0;
        int result = cli.getIdCliente();
        if (expResult == result) {
            System.out.println("Teste ao método getIdCliente passado com um sucesso!\n");
        }
        assertEquals(expResult, result);
    }

    /**
     * Teste ao método setIdCliente da classe cliente.<br>
     * Método modifica o id do objeto cli e depois verifica se o consegiu
     * alterar.<br>
     * O teste é válido caso tal se verifique.<br>
     * No fim, restaura o atributo ao estado inicial.
     */
    @Test
    public void testSetIdCliente() {
        System.out.println("setIdCliente");
        System.out.println("\nTeste ao método setIdCliente");
        int id = 1;
        int idBackup = cli.getIdCliente();
        cli.setIdCliente(id);
        boolean result = false;
        if (id == cli.getIdCliente()) {
            result = true;
            System.out.println("Teste ao método setIdCliente passado com um sucesso!\n");
        }
        assertTrue(result);
        cli.setIdCliente(idBackup);
    }

    /**
     * Teste ao método getNome da classe cliente.<br>
     * Método tenta buscar o nome do objeto previamente criado na classe.<br>
     * O teste é válido caso o valor obtido seja igual ao valor esperado.
     */
    @Test
    public void testGetNome() {
        System.out.println("\nTeste ao método getNome");
        String expResult = "nome";
        String result = cli.getNome();
        if (expResult.equals(result)) {
            System.out.println("Teste ao método getNome passado com um sucesso!\n");
        }
        assertEquals(expResult, result);
    }

    /**
     * Teste ao método setNome da classe cliente.<br>
     * Método modifica o nome do objeto cli e depois verifica se o consegiu
     * alterar.<br>
     * O teste é válido caso tal se verifique.<br>
     * No fim, restaura o atributo ao estado inicial.
     */
    @Test
    public void testSetNome() {
        System.out.println("\nTeste ao método setNome");
        String nomeI = "muda";
        String nomeF = cli.getNome();
        cli.setNome(nomeI);
        boolean result = false;
        if (nomeI.equals(cli.getNome())) {
            result = true;
            System.out.println("Teste ao método setNome passado com um sucesso!\n");
        }
        assertTrue(result);
        cli.setNome(nomeF);
    }

    /**
     * Teste ao método getMorada da classe cliente.<br>
     * Método tenta buscar a morada do objeto previamente criado na classe.<br>
     * O teste é válido caso o valor obtido seja igual ao valor esperado.
     */
    @Test
    public void testGetMorada() {
        System.out.println("\nTeste ao método getMorada");
        String expResult = "rua";
        String result = cli.getMorada();
        if (expResult.equals(result)) {
            System.out.println("Teste ao método getMorada passado com um sucesso!\n");
        }
        assertEquals(expResult, result);
    }

    /**
     * Teste ao setMorada setNome da classe cliente.<br>
     * Método modifica a morada do objeto cli e depois verifica se a consegiu
     * alterar.<br>
     * O teste é válido caso tal se verifique.<br>
     * No fim, restaura o atributo ao estado inicial.
     */
    @Test
    public void testSetMorada() {
        System.out.println("\nTeste ao método setMorada");
        String moradaI = "muda";
        String moradaF = cli.getMorada();
        cli.setMorada(moradaI);
        boolean result = false;
        if (moradaI.equals(cli.getMorada())) {
            result = true;
            System.out.println("Teste ao método setMorada passado com um sucesso!\n");
        }
        assertTrue(result);
        cli.setMorada(moradaF);
    }

    /**
     * Teste ao método getCodPostal da classe cliente.<br>
     * Método tenta buscar o código postal do objeto previamente criado na
     * classe.<br>
     * O teste é válido caso o valor obtido seja igual ao valor esperado.
     */
    @Test
    public void testGetCodPostal() {
        System.out.println("\nTeste ao método getCodPostal");
        String expResult = "0000-000";
        String result = cli.getCodPostal();
        if (expResult.equals(result)) {
            System.out.println("Teste ao método getCodPostal passado com um sucesso!\n");
        }
        assertEquals(expResult, result);
    }

    /**
     * Teste ao método setCodPostal da classe cliente.<br>
     * Método modifica o código postal do objeto cli e depois verifica se o
     * consegiu alterar.<br>
     * O teste é válido caso tal se verifique.<br>
     * No fim, restaura o atributo ao estado inicial.
     */
    @Test
    public void testSetCodPostal() {
        System.out.println("\nTeste ao método setCodPostal");
        String cpI = "1111-111";
        String cpF = cli.getCodPostal();
        cli.setCodPostal(cpI);
        boolean result = false;
        if (cpI.equals(cli.getCodPostal())) {
            result = true;
            System.out.println("Teste ao método setCodPostal passado com um sucesso!\n");
        }
        assertTrue(result);
        cli.setCodPostal(cpF);
    }

    /**
     * Teste ao método getPais da classe cliente.<br>
     * Método tenta buscar o país do objeto previamente criado na classe.<br>
     * O teste é válido caso o valor obtido seja igual ao valor esperado.
     */
    @Test
    public void testGetPais() {
        System.out.println("\nTeste ao método getPais");
        String expResult = "JUNIT";
        String result = cli.getPais();
        if (expResult.equals(result)) {
            System.out.println("Teste ao método getPais passado com um sucesso!\n");
        }
        assertEquals(expResult, result);
    }

    /**
     * Teste ao método setPais da classe cliente.<br>
     * Método modifica o país do objeto cli e depois verifica se o consegiu
     * alterar.<br>
     * O teste é válido caso tal se verifique.<br>
     * No fim, restaura o atributo ao estado inicial.
     */
    @Test
    public void testSetPais() {
        System.out.println("\nTeste ao método setPais");
        String pI = "Portugal";
        String pF = cli.getPais();
        cli.setPais(pI);
        boolean result = false;
        if (pI.equals(cli.getPais())) {
            result = true;
            System.out.println("Teste ao método setPais passado com um sucesso!\n");
        }
        assertTrue(result);
        cli.setPais(pF);
    }

    /**
     * Teste ao método getEstado da classe cliente.<br>
     * Método tenta buscar o estado do objeto previamente criado na classe.<br>
     * O teste é válido caso o valor obtido seja igual ao valor esperado.
     */
    @Test
    public void testGetEstado() {
        System.out.println("\nTeste ao método getEstado");
        String expResult = "inativo";
        String result = cli.getEstado();
        if (expResult.equals(result)) {
            System.out.println("Teste ao método getEstado passado com um sucesso!\n");
        }
        assertEquals(expResult, result);
    }

    /**
     * Teste ao método setEstado da classe cliente.<br>
     * Método modifica o estado do objeto cli e depois verifica se o consegiu
     * alterar.<br>
     * O teste é válido caso tal se verifique.<br>
     * No fim, restaura o atributo ao estado inicial.
     */
    @Test
    public void testSetEstado() {
        System.out.println("\nTeste ao método setEstado");
        String sI = "pendente";
        String sF = cli.getEstado();
        cli.setEstado(sI);
        boolean result = false;
        if (sI.equals(cli.getEstado())) {
            result = true;
            System.out.println("Teste ao método setEstado passado com um sucesso!\n");
        }
        assertTrue(result);
        cli.setEstado(sF);
    }

    /**
     * Teste ao método getContribuinte da classe cliente.<br>
     * Método tenta buscar o estado do objeto previamente criado na classe.<br>
     * O teste é válido caso o valor obtido seja igual ao valor esperado.
     */
    @Test
    public void testGetContribuinte() {
        System.out.println("\nTeste ao método getContribuinte");
        int expResult = 923496789;
        int result = cli.getContribuinte();
        if (expResult == result) {
            System.out.println("Teste ao método getContribuinte passado com um sucesso!\n");
        }
        assertEquals(expResult, result);
    }

    /**
     * Teste ao método setContribuinte da classe cliente.<br>
     * Método modifica o nif do objeto cli e depois verifica se o consegiu
     * alterar.<br>
     * O teste é válido caso tal se verifique.<br>
     * No fim, restaura o atributo ao estado inicial.
     */
    @Test
    public void testSetContribuinte() {
        System.out.println("\nTeste ao método setContribuinte");
        int cI = 123456789;
        int cF = cli.getContribuinte();
        cli.setContribuinte(cI);
        boolean result = false;
        if (cI == (cli.getContribuinte())) {
            result = true;
            System.out.println("Teste ao método setContribuinte passado com um sucesso!\n");
        }
        assertTrue(result);
        cli.setContribuinte(cF);
    }
}
