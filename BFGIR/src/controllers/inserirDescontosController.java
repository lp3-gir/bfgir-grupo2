/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import bfgir.inserirDesconto;
import bfgir.login;
import bfgir.menuAdministrador;
import bfgir.menuOperador;
import classes.desconto;
import static classes.desconto.getListaDescontosValor;
import static classes.desconto.inserirDescontos;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author ruben
 */
public class inserirDescontosController implements Initializable {

    @FXML
    private TextArea desconto;
    @FXML
    private Button inserirOperador;
    @FXML
    private MenuBar menu;
    @FXML
    private Label lbPass;

    static Alert a = new Alert(Alert.AlertType.NONE);

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO

    }
    
    /**
     * Inserir desconto<br>
     *
     *Este metodo vai veirificar se o desconto já existe e se não exisitir vai inserir o desconto que o utilizador escolheu
     * na base de dados. <br>
     *
     * @param desconto = valor inserido pelo utilizador
     */

    @FXML
    private void inserirDesconto(ActionEvent event) {

        try {

            int i = 0;
            String quantDesc = desconto.getText();

            List<desconto> descontos = getListaDescontosValor(Float.parseFloat(quantDesc));
            for (desconto des : descontos) {
                i++;
            }

            if (i == 0) {
                inserirDescontos(Float.parseFloat(quantDesc));

                a.setAlertType(Alert.AlertType.INFORMATION);
                a.getButtonTypes().setAll(ButtonType.OK);
                a.setTitle("Desconto inserido");
                a.setHeaderText("A taxa de desconto escolhida foi inserida com sucesso!");
                a.show();

            } else {
                a.setAlertType(Alert.AlertType.WARNING);
                a.getButtonTypes().setAll(ButtonType.OK);
                a.setTitle("Desconto já existente");
                a.setHeaderText("A taxa de desconto escolhida já existe!");
                a.show();
            }
        } catch (Exception e) {
            a.setAlertType(Alert.AlertType.WARNING);
            a.getButtonTypes().setAll(ButtonType.OK);
            a.setTitle("Apenas números");
            a.setHeaderText("Introduza apenas números!");
            a.show();
        }
    }

    /**
     * Metodo para terminar sessao do utilizador<br>
     *
     * @param event evento
     */
    @FXML
    private void terminarSessao(ActionEvent event) {
        try {
            login.setIdLogin(0);
            login.setNivelLogin(10000);
            login log = new login();
            fecha();
            log.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(listarDescontosController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Método para encerrar a aplicação
     */
    @FXML
    private void sair(ActionEvent event) {
        System.exit(0);

    }

    /**
     * Método para aceder ao menu do administrador ou do operador
     */
    @FXML
    private void openMenu(ActionEvent event) {
        try {
            if (login.getNivelLogin() == 1) {
                menuOperador menuO = new menuOperador();
                fecha();
                menuO.start(new Stage());
            } else {
                menuAdministrador menuAdmin = new menuAdministrador();
                fecha();
                menuAdmin.start(new Stage());
            }

        } catch (Exception ex) {
            Logger.getLogger(inserirDescontosController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * método para fechar o ecrã de inserir descontos
     */
    private void fecha() {
        inserirDesconto.getStage().close();
    }

}
