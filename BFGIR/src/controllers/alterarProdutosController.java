/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import bfgir.alterarProdutos;
import bfgir.ecraPessoalOperador;
import bfgir.login;
import bfgir.menuAdministrador;
import bfgir.menuOperador;
import classes.categoria;
import static classes.categoria.listarCategoriasAtivas;
import static classes.categoria.procurarCategoriaID;
import static classes.categoria.procurarCategoriaNome;
import classes.helper;
import classes.produto;
import static classes.produto.atualizarCategoriaProduto;
import static classes.produto.atualizarDescricaoProduto;
import static classes.produto.atualizarEstadoProduto;
import static classes.produto.atualizarPrecoUniProduto;
import static classes.produto.getListaProdutos;
import static classes.produto.pesquisarProdutoID;
import classes.utilizador;
import static classes.utilizador.procurarEmail;
import static controllers.alterarPrecoProdutoClienteController.validarPesquisaAtibutoCodProdPa;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Classe controladora do ecrã alterarProdutos.<br>
 *
 * @author Ilídio Magalhães
 */
public class alterarProdutosController implements Initializable {

    //variaveis do ecrã
    private int altura_vbox = 0;
    Alert a = new Alert(Alert.AlertType.NONE);
    static String novoItemCbCategoria = "Sem Tipo Produto";
    //variáveis para o algoritmo
    static produto produtoBase = new produto();
    static String nomeCatProd = "";
    static List<categoria> listaCategoriasProduto = new ArrayList<categoria>();
    static float precoCompraProdutoMaximo = 0.0f;

    @FXML
    private Pane painelStockProdutos;
    @FXML
    private ScrollPane Produtos;
    @FXML
    private MenuBar menu;
    @FXML
    private VBox vBoxProdutos;
    @FXML
    private TextArea descricaoTF;
    @FXML
    private Label lblDescricao;
    @FXML
    private Label lblEstado;
    @FXML
    private Menu opcoes;
    @FXML
    private TextField codProdutoPesquisar;
    @FXML
    private Button pesquisarCodProduto;
    @FXML
    private Label lblPrecoUniSIVA;
    @FXML
    private TextArea precoUniSIVA;
    @FXML
    private ChoiceBox<String> cbestado;
    @FXML
    private Button botaoAtualizarProduto;
    @FXML
    private Label lblCategoria;
    @FXML
    private ChoiceBox<String> cbCategoria;
    @FXML
    private Label lblID;
    @FXML
    private TextArea textID;

    /**
     * Método para inicializar o ecrã de alterar produtos <br>
     * O método define os valores possíveis para a chombox do estado do produto,
     * esconde os campos alteráveis de um produto, lista todos os produtos pelo
     * seu código para para uma vbox contendo botões, carrega o método contendo
     * listeners de validação dos campos alteráveis de um produto e reformula o
     * menu de opções para incluir o ecrã pessoal do operador caso utilizador
     * seja do tipo operador.<br>
     *
     * @param url url.
     * @param rb rb.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            adicionarEcraPessoalOperadorMenuOpcoes();

            cbestado.getItems().add("ativo");
            cbestado.getItems().add("inativo");
            setVisibleFalse();

            listarTodosProdutosBD();

            validarCampos();
        } catch (Exception e) {
            mensagemErroFatal();

        }
    }

    /**
     * Método para aceder ao menu do operador.<br>
     */
    @FXML
    private void openMenu(ActionEvent event) {
        try {
            if (login.getNivelLogin() == 1) {
                menuOperador menuO = new menuOperador();
                fecha();
                menuO.start(new Stage());
            } else {
                menuAdministrador menuAdmin = new menuAdministrador();
                fecha();
                menuAdmin.start(new Stage());
            }
        } catch (Exception ex) {
            Logger.getLogger(alterarProdutosController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Metodo para terminar sessao do utilizador.<br>
     *
     * @param event evento.
     */
    @FXML
    private void terminarSessao(ActionEvent event
    ) {
        try {
            login.setIdLogin(0);
            login.setNivelLogin(10000);
            login log = new login();
            fecha();
            log.start(new Stage());

        } catch (Exception ex) {
            Logger.getLogger(alterarProdutosController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Método para encerrar a aplicação.<br>
     */
    @FXML
    private void sair(ActionEvent event
    ) {

        helper.opcaoSair(event);
    }

    /**
     * Método para fechar o ecrã.<br>
     */
    private static void fecha() {
        alterarProdutos.getStage().close();
    }

    /**
     *
     * Método para listar todos os produtos existentes na BD.<br>
     *
     *
     * Neste método criada uma lista com todos os produtos obtidos pelo método
     * getListaProdutos.<br> De seguida para cada elemento dessa lista de
     * produtos é criado um botão com o id do produto.<br> Depois de selecionar
     * um produto é feito um select com o codigo do mesmo (codProdPa) através do
     * método listarDetalhesProduto para listar alguns dados do produto no
     * ecra.<br> Esses dados ficam visíveis após a execução do método
     * setVisibleTrue.<br>
     *
     */
    private void listarTodosProdutosBD() {
        try {
            List<produto> produtos = getListaProdutos();
            for (produto produto : produtos) {
                String codProdPa = produto.getCodProdPa();
                Button botao = new Button(codProdPa);
                botao.setId("botaoVbox");
                botao.setPrefHeight(30);
                botao.setPrefWidth(275);
                botao.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent e) {
                        botao.setId("botaoVboxChangeCollour");
                        listarDetalhesProduto(codProdPa);
                        setVisibleEditableTrue();

                    }
                });
                vBoxProdutos.getChildren().addAll(botao);
                if (altura_vbox > 554) {
                    Produtos.setPrefWidth(218);
                    vBoxProdutos.setPrefHeight(altura_vbox);
                }

            }
        } catch (Exception e) {
            mensagemErroFatal();
        }
    }

    /**
     * Método para mostrar os atributos do produto quando carregados e torná-los
     * editáveis.<br>
     */
    private void setVisibleEditableTrue() {

        lblDescricao.setVisible(true);
        descricaoTF.setVisible(true);
        descricaoTF.setEditable(true);
        lblPrecoUniSIVA.setVisible(true);
        precoUniSIVA.setVisible(true);
        precoUniSIVA.setEditable(false);//não editável
        lblEstado.setVisible(true);
        cbestado.setVisible(true);
        botaoAtualizarProduto.setVisible(true);
        lblCategoria.setVisible(true);
        cbCategoria.setVisible(true);
        lblID.setVisible(true);
        textID.setVisible(true);
    }

    /**
     * Método para esconder os atributos ainda não carregados.<br>
     */
    private void setVisibleFalse() {

        lblDescricao.setVisible(false);
        descricaoTF.setVisible(false);
        lblPrecoUniSIVA.setVisible(false);
        precoUniSIVA.setVisible(false);
        lblEstado.setVisible(false);
        cbestado.setVisible(false);
        botaoAtualizarProduto.setVisible(false);
        lblCategoria.setVisible(false);
        cbCategoria.setVisible(false);
        lblID.setVisible(false);
        textID.setVisible(false);
        textID.setEditable(false);
    }

    /**
     * método para substituir o conteudo do menu de opções para colocar as
     * opções por ordem caso seja o operador a abrir o ecrã.<br>
     */
    private void adicionarEcraPessoalOperadorMenuOpcoes() {

        List<utilizador> operadores = procurarEmail(login.getIdLogin());
        for (utilizador operador : operadores) {
            if (operador.getNivel() == 1) {
                opcoes.getItems().clear();
                MenuItem ePOp = new MenuItem("Ecrã Pessoal");
                MenuItem terminarSessao = new MenuItem("Terminar Sessão");
                MenuItem sair = new MenuItem("Sair");
                opcoes.getItems().add(ePOp);
                opcoes.getItems().add(terminarSessao);
                opcoes.getItems().add(sair);
                ePOp.setOnAction((ActionEvent e) -> {
                    try {
                        ecraPessoalOperador ePO = new ecraPessoalOperador();
                        fecha();
                        ePO.start(new Stage());
                    } catch (Exception ex) {
                        Logger.getLogger(alterarProdutosController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
                terminarSessao.setOnAction((ActionEvent e) -> {
                    try {
                        login.setIdLogin(0);
                        login.setNivelLogin(10000);
                        login log = new login();
                        fecha();
                        log.start(new Stage());
                    } catch (Exception ex) {
                        Logger.getLogger(alterarProdutosController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
                sair.setOnAction((ActionEvent e) -> {
                    System.exit(0);
                });
            }
        }
    }

    /**
     * Metodo para listar um produto selecionado pelo utilizador através do seu
     * código.<br>
     * Este método recebe o código de um produto inserido pelo utlizador e
     * precede à sua validação.<br>Caso este seja válido, primeiro passa-o ao
     * método listarDetalhesProduto que lista os detalhes do
     * produto.<br>Depois,chama também o método setVisibleTrue para tornar
     * visíveis os parâmetros listados.<br>Caso o código do produto não seja
     * válido, o método informa o utilizador.<br>No final, o utilizador irá ver,
     * para depois editar, a descrição, o preço unitário sem iva e o estado de
     * um produto específico.<br>
     *
     * @param event evento.
     */
    @FXML
    private void pesquisarCodProduto(ActionEvent event) {
        try {
            if (validarPesquisaAtibutoCodProdPa(codProdutoPesquisar.getText()) == true && encontrarProduto(codProdutoPesquisar.getText()) == true) {
                listarDetalhesProduto(codProdutoPesquisar.getText());
                setVisibleEditableTrue();
            } else {
                a.setAlertType(Alert.AlertType.WARNING);
                a.getButtonTypes().setAll(ButtonType.OK);
                a.setTitle("Código de Produto Inválido");
                a.setHeaderText("Formato do Código de Produto Inválido");
                a.setContentText("Insira um código de produto contendo apenas careteres alfanuméricos");
                Optional<ButtonType> result = a.showAndWait();
                if (result.get() == ButtonType.OK) {
                    codProdutoPesquisar.setText("");
                }
            }
        } catch (Exception pesquisa) {

            mensagemErroRecarregarEcra();
        }

    }

    /**
     * Método para listar a descrição, o preço unitário sem iva, o estado e a
     * categoria (nome) de um produto e guardar os dados em memória num objeto
     * do tipo produto para validações futuras.<br>
     * O método valida o código do produto e os atributos a serem
     * alterados.<br>Se um destes atributos do produto, por algum motivo, for
     * nulo ou estiver vazio, o utilizador é direcionado para o seu menu.<br>
     *
     * O método também deteta a unidade do produto e insere-a no label do preço
     * unitário sem iva do produto.<br>Quando o produto não for vendido à
     * unidade.<br>
     * As especifidades da categoria do produto são tratadas pelo método
     * mostarNomeCategoriaProduto que é invocado aquando da execução deste
     * método.<br>
     *
     * @param codProdPa : código de um produto.
     */
    public void listarDetalhesProduto(String codProdPa) {
        try {
            cbCategoria.getItems().clear();
            nomeCatProd = "";
            setVisibleFalse();
            listarCategoriasProduto();
            //para não transpor valores de um produto para outro
            cbCategoria.setValue("");
            cbestado.setValue("");

            List<produto> listaDetalhesProduto = pesquisarProdutoID(codProdPa);
            for (produto produto : listaDetalhesProduto) {
                produtoBase.setCodProdPa(codProdPa);
                if (produtoBase.getCodProdPa().equals("") || produtoBase.getCodProdPa() == null) {
                    a.setAlertType(Alert.AlertType.ERROR);
                    a.setTitle("ERRO Fatal");
                    a.setHeaderText("Impossível carregar dados do produto!");
                    a.setContentText("Algumas funcionalidades do aplicação estão inacessíveis");
                    a.getButtonTypes().setAll(ButtonType.OK);
                    Optional<ButtonType> result3 = a.showAndWait();
                    if (result3.get() == ButtonType.OK) {
                        abrirMenUtilizador();
                    }
                } else {
                    textID.setText(produto.getCodProdPa());
                    descricaoTF.setText(produto.getDescricao());
                    produtoBase.setDescricao(produto.getDescricao());
                    precoUniSIVA.setText(String.valueOf(produto.getPrecoUnitSIVA()));
                    produtoBase.setPrecoUnitSIVA(produto.getPrecoUnitSIVA());
                    cbestado.setValue(produto.getEstado());
                    produtoBase.setEstado(produto.getEstado());
                    produtoBase.setTipoQuantStockUnit(produto.getTipoQuantStockUnit());
                    produtoBase.setTipoQuantStock(produto.getTipoQuantStock());
                    // caso o produto não seja vendido à unidade, apresentar o tipo de quantidade existente porque de facto é a unidade
                    if (!produtoBase.getTipoQuantStockUnit().equals("") && produtoBase.getTipoQuantStockUnit() != null) {
                        lblPrecoUniSIVA.setText("Preço base por " + produtoBase.getTipoQuantStockUnit().toLowerCase() + " sem IVA:");
                    } else {
                        lblPrecoUniSIVA.setText("Preço base por " + produtoBase.getTipoQuantStock().toLowerCase() + " sem IVA:");
                    }

                    produtoBase.setIdCategoria(produto.getIdCategoria());

                    if (produtoBase.getIdCategoria() == 0) {
                        cbCategoria.getItems().add(novoItemCbCategoria);
                        cbCategoria.setValue(novoItemCbCategoria);
                        nomeCatProd = novoItemCbCategoria;
                    } else {
                        String nomeCat = mostarNomeCategoriaProduto(produtoBase.getIdCategoria());

                        cbCategoria.setValue(nomeCat);
                        nomeCatProd = nomeCat;
                    }
                }

            }
        } catch (Exception a) {
            mensagemErroRecarregarEcra();
        }
    }

    /**
     * Método para aceder à base de dados pelo código de um produto para
     * verificar se o produto existe.<br>Usado na validação do código do produto
     * a ser procurado com o botão pesquisarCodProduto.<br>
     *
     * @param codProdPa : código de um produto.
     */
    private boolean encontrarProduto(String codProdPa) {
        boolean produtoExiste = false;
        int produtoN = 0;
        List<produto> listaDetalhesProduto = pesquisarProdutoID(codProdPa);
        for (produto produto : listaDetalhesProduto) {
            if (produto.getCodProdPa().equals(codProdPa)) {
                produtoN = 1;
            }
        }
        if (produtoN == 1) {
            produtoExiste = true;
        } else {
            produtoExiste = false;
        }
        return produtoExiste;
    }

    /**
     * Método que executa validações aos campos manipuláveis pelo o
     * utilizador.<br>
     * Método usa listeners para verificar dados novos nos campos descrição e
     * preço unitário sem iva para garantir que o utilizador só pesquise por
     * produtos que existam na BD e que os dados de um produto sejam iguais aos
     * iniciais ou novos, num formato apropriado e numa lógica de negócio (ex:
     * preço novo maior que o anterior e menor que menor preço de venda quando
     * aplicável) correta.<br>
     * O estado e a categoria/tipo do produto são validados em métodos
     * separados.<br>
     *
     */
    private void validarCampos() {
        try {
            //validar a descrição do produto
            descricaoTF.focusedProperty()
                    .addListener((arg0, oldValue, newValue) -> {
                        try {
                            if (!newValue) {
                                if (Pattern.compile("[\\p{L}\\p{Nd}]*").
                                        matcher(descricaoTF.getText()).find() == false || descricaoTF.getText().length() > 150) {
                                    a.setAlertType(Alert.AlertType.WARNING);
                                    a.getButtonTypes().setAll(ButtonType.OK);
                                    a.setTitle("Descrição do Produto Inválido");
                                    a.setHeaderText("Formato da Descrição do Produto Inválida");
                                    a.setContentText("Insira uma descrição para o produto válida");
                                    Optional<ButtonType> result = a.showAndWait();
                                    if (result.get() == ButtonType.OK) {
                                        descricaoTF.setText(produtoBase.getDescricao());
                                    }
                                } else if (descricaoTF.getText().equals("")) {
                                    descricaoTF.setText(produtoBase.getDescricao());
                                }
                            }
                        } catch (Exception e) {
                            descricaoTF.setText(produtoBase.getDescricao());
                        }
                    }
                    );
        } catch (Exception podeEstourar) {
            Logger.getLogger(alterarProdutosController.class.getName()).log(Level.SEVERE, null, podeEstourar);
        }
    }

    /**
     * Este método vai fazer varias confirmaçoes as alteraçoes que ocorreram num
     * produto e à medida que os dados vão sendo validados, vai atualizando os
     * dados de um produto na BD.<br>
     *
     * @param event evento on click.<br>
     *
     */
    @FXML
    private void botaoAtualizarProduto(ActionEvent event
    ) {
        try {
            Alert alerta = new Alert(Alert.AlertType.NONE);
            alerta.setAlertType(Alert.AlertType.CONFIRMATION);
            alerta.setTitle("Alterar Produtos");
            alerta.setHeaderText("Deseja mesmo alterar o produto?");
            alerta.setResizable(false);
            alerta.getButtonTypes().setAll(ButtonType.YES, ButtonType.NO);
            Optional<ButtonType> result = alerta.showAndWait();

            if (result.get() == ButtonType.YES) {

                //verificar se o produto ainda existe na BD
                List<produto> produtos = pesquisarProdutoID(produtoBase.getCodProdPa());
                int count = 0;
                for (produto produto : produtos) {
                    count = 1;
                }
                if (count == 0) {
                    alerta.setAlertType(Alert.AlertType.ERROR);
                    alerta.setTitle("Alterar Produtos");
                    alerta.setHeaderText("O Produto foi removido!");
                    alerta.setContentText("O produto foi eliminado da base de dados enquanto o atualizava!");
                    alerta.setResizable(false);
                    alerta.getButtonTypes().setAll(ButtonType.OK);
                    Optional<ButtonType> resultA = alerta.showAndWait();
                    if (resultA.get() == ButtonType.OK) {
                        recarregarEcra();
                    }
                } else {

                    int contadorSucesso = -1; // para validar a alteração
                    int falhou = 0; //parar método após falha de uma alteração

                    //obter id da categoria ou colocar o id a zero
                    int idCat = 0;
                    try {
                        idCat = getIDCategoria(cbCategoria.getValue(), novoItemCbCategoria);

                    } catch (Exception semCategoria) {
                        mensagemErroRecarregarEcra();
                    }

                    //aplicar alterações
                    //alterar descrição do produto
                    if (!descricaoTF.getText().equals(produtoBase.getDescricao()) && !descricaoTF.getText().equals("") && falhou == 0) {

                        atualizarDescricaoProduto(descricaoTF.getText(), produtoBase.getCodProdPa());

                        List<produto> produtosDescricao = pesquisarProdutoID(produtoBase.getCodProdPa());
                        for (produto produto : produtosDescricao) {
                            if (produto.getDescricao().equals(descricaoTF.getText())) {
                                contadorSucesso = 1;
                            } else {
                                contadorSucesso = 0;
                                falhou = 1;

                            }
                        }
                    }
                    //alterar preço unitário do produto
                    if (!precoUniSIVA.getText().equals("") && Float.valueOf(precoUniSIVA.getText()) > produtoBase.getPrecoUnitSIVA() && falhou == 0) {

                        atualizarPrecoUniProduto(Float.valueOf(precoUniSIVA.getText()), produtoBase.getCodProdPa());

                        List<produto> produtosDescricao = pesquisarProdutoID(produtoBase.getCodProdPa());
                        for (produto produto : produtosDescricao) {
                            if (produto.getDescricao().equals(descricaoTF.getText())) {
                                contadorSucesso = 1;
                            } else {
                                contadorSucesso = 0;
                                falhou = 1;
                            }
                        }
                    }

                    //alterar estado do produto
                    if (!cbestado.getValue().equals("") && cbestado.getValue() != null && !cbestado.getValue().equals(produtoBase.getEstado()) && falhou == 0) {
                        atualizarEstadoProduto(cbestado.getValue(), produtoBase.getCodProdPa());
                        List<produto> produtosDescricao = pesquisarProdutoID(produtoBase.getCodProdPa());
                        for (produto produto : produtosDescricao) {
                            if (produto.getDescricao().equals(descricaoTF.getText())) {
                                contadorSucesso = 1;
                            } else {
                                contadorSucesso = 0;
                                falhou = 1;

                            }
                        }
                    } //alterar categoria/tipo do produto
                    else if (idCat != produtoBase.getIdCategoria() && idCat > 0 && falhou == 0) {
                        //alterar produto
                        atualizarCategoriaProduto(idCat, produtoBase.getCodProdPa());
                        //verificar se o produto foi atualizado
                        List<produto> produtosAtualizados = pesquisarProdutoID(produtoBase.getCodProdPa());
                        for (produto produto : produtosAtualizados) {
                            if (produto.getCodProdPa().equals(produtoBase.getCodProdPa()) && produto.getIdCategoria() == idCat) {
                                contadorSucesso = 1;
                            } else {
                                contadorSucesso = 0;
                                falhou = 1;

                            }
                        }
                    }
                    //validar alteração de dados
                    if (contadorSucesso == 1 && falhou == 0) {
                        a.setAlertType(Alert.AlertType.INFORMATION);
                        a.setTitle("Alterar Produtos");
                        a.setHeaderText("Produto Atualizado com Sucesso!");
                        a.setContentText("As alterações foram aplicadas!");
                        a.getButtonTypes().setAll(ButtonType.OK);
                        Optional<ButtonType> result2 = a.showAndWait();
                        if (result2.get() == ButtonType.OK) {
                            recarregarEcra();
                        }
                    } else if (contadorSucesso == -1) {
                        a.setAlertType(Alert.AlertType.WARNING);
                        a.setTitle("Alterar Produtos");
                        a.setHeaderText("Actualização Dados Produto");
                        a.setContentText("Não efectuou nenhuma alteração!");
                        a.setResizable(false);
                        a.getButtonTypes().setAll(ButtonType.OK);
                        Optional<ButtonType> resultZ = a.showAndWait();
                        if (resultZ.get() == ButtonType.OK) {
                            recarregarEcra();
                        }

                    } else if (contadorSucesso == 0 && falhou == 1) {
                        a.setAlertType(Alert.AlertType.WARNING);
                        a.setTitle("Alterar Produtos");
                        a.setHeaderText("Actualização Dados Produto");
                        a.setContentText("Não foi possível alterar um ou mais atributos do produto!\nTente novamente");
                        a.setResizable(false);
                        a.getButtonTypes().setAll(ButtonType.OK);
                        Optional<ButtonType> resultZ = a.showAndWait();
                        if (resultZ.get() == ButtonType.OK) {
                            recarregarEcra();
                        }
                    } else {
                        mensagemErroRecarregarEcraSemParar();
                    }
                }
            } else if (result.get() == ButtonType.NO) {
                recarregarEcra();
            }

        } catch (Exception ae) {
            mensagemErroRecarregarEcra();
        }
    }

    /**
     * Método para enviar o utilizador para o seu menu em caso de falha da
     * aplicação.<br>
     *
     * Método deteta o tipo de utilizador consonante o seu nível e reencaminha
     * para o seu menu.<br> Em caso de falha, fecha a aplicação.<br>Este método,
     * semelhante ao método do botão menu, é invocado quando o programa
     * encontrar uma falha na lógica da aplicação (como aquando da atualização
     * do produto) para permitir o utilizador continuar a usar outras
     * funcionalidades da aplicação.<br>
     */
    private void abrirMenUtilizador() {
        if (login.getNivelLogin() == 1) { //se for operador ir para o menu do operador
            menuOperador menuOp = new menuOperador();
            fecha();
            try {
                menuOp.start(new Stage());
            } catch (Exception ex) {
                a.setAlertType(Alert.AlertType.ERROR);
                a.setTitle("ERRO Fatal");
                a.setHeaderText("A aplicação comportou-se de forma inesperada!");
                a.setContentText("Encerrando a aplicação.");
                Logger
                        .getLogger(alterarProdutosController.class
                                .getName()).log(Level.SEVERE, null, ex);
                a.getButtonTypes().setAll(ButtonType.OK);
                Optional<ButtonType> result3 = a.showAndWait();
                if (result3.get() == ButtonType.OK) {
                    System.exit(0);
                }
            }

        } else if (login.getNivelLogin() == 2) { //se for admin ir para o menu do admin
            menuAdministrador menuAdmin = new menuAdministrador();
            fecha();
            try {
                menuAdmin.start(new Stage());
            } catch (Exception ex) {
                a.setAlertType(Alert.AlertType.ERROR);
                a.setTitle("ERRO Fatal");
                a.setHeaderText("A aplicação comportou-se de forma inesperada!");
                a.setContentText("Encerrando a aplicação.");
                Logger
                        .getLogger(alterarProdutosController.class
                                .getName()).log(Level.SEVERE, null, ex);
                a.getButtonTypes().setAll(ButtonType.OK);
                Optional<ButtonType> result3 = a.showAndWait();
                if (result3.get() == ButtonType.OK) {
                    System.exit(0);
                }
            }
        } else { //crasha/fecha
            a.setAlertType(Alert.AlertType.ERROR);
            a.setTitle("ERRO Fatal");
            a.setHeaderText("A aplicação comportou-se de forma inesperada!");
            a.setContentText("Encerrando a aplicação.");
            a.getButtonTypes().setAll(ButtonType.OK);
            Optional<ButtonType> result3 = a.showAndWait();
            if (result3.get() == ButtonType.OK) {
                System.exit(0);
            }
        }
    }

    /**
     * Método para listar as categorias ativas de produtos.<br>
     *
     * Método vai buscar a categoria dos produtos à base de dados.<br>Depois,se
     * as encontrar, adiciona o nome de cada um à observable list. Se não
     * encontrar nada avisa o utilizador e insere um valor genérico.<br>
     *
     * Método aplicado aquando da inicialização do ecrã e aquando da atualização
     * das categorias ativas.<br>
     */
    private void listarCategoriasProduto() {
        try {
            listaCategoriasProduto = listarCategoriasAtivas();
            if (!listaCategoriasProduto.isEmpty()) {
                for (categoria categoria : listaCategoriasProduto) {
                    cbCategoria.getItems().add(categoria.getNomeCategoria());
                }
            } else {
            }

            cbCategoria.getItems().sorted();
        } catch (Exception e) {
            cbCategoria.getItems().add(novoItemCbCategoria);
            Logger.getLogger(alterarProdutosController.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    /**
     * Método para apresentar o nome da categoria de um produto.<br>
     *
     * O método recebe o id da categoria do produto e vai procurar pela mesma na
     * base de dados.Se não encontrar, coloca um valor genérico para alteração
     * do utilizador.<br>
     * Todo o método esta protegido com try catch para lidar com
     * imponderáveis.<br>
     *
     *
     * @param idCategoria : o id da categoria do produto a ser apresentada.
     * @return : nome da categoria do produto
     */
    public static String mostarNomeCategoriaProduto(int idCategoria) {
        List<categoria> categoriaProdutos = new ArrayList<categoria>();
        String nomeCat = "";
        try {
            categoriaProdutos = procurarCategoriaID(idCategoria);
            if (!categoriaProdutos.isEmpty()) {
                for (categoria categoria : categoriaProdutos) {
                    if (idCategoria == categoria.getIdCategoria()) {
                        nomeCat = categoria.getNomeCategoria();
                        break;
                    }
                }
            } else {
                nomeCat = novoItemCbCategoria;
            }
            if (nomeCat.equals("")) {
                nomeCat = novoItemCbCategoria;
            }

        } catch (Exception nullPointerMaisImprevistos) {
            nomeCat = novoItemCbCategoria;
            Logger.getLogger(alterarProdutosController.class.getName()).log(Level.SEVERE, null, nullPointerMaisImprevistos);
        }
        return nomeCat;
    }

    /**
     * Método para verificar se uma categoria com um atributo proposto existe e
     * com isto verificar se é uma categoria nova.<br>
     *
     *
     * Método impede a repetição de nomes e força o uso de nomes distintos.<br>
     * Método recebe o nome da categoria, e a lista de todas as categorias já
     * carregada em memória.<br> Caso a lista esteja vazia, o nome é único à
     * partida.<br> Caso tenha elementos, o método vai procurar na lista de
     * categorias pelo mesmo e tenta encontrar correspondências.<br>Em caso
     * positivo vai retornar falso e em caso negativo vai retornar verdadeiro -
     * o nome é único e por sua vez, a categoria também.<br>
     *
     * @param nomeCategoria : texto da textarea nomeCategoriaNova correspondente
     * ao nome da nova categoria.
     * @param categorias : lista de categorias existentes na bd já carregada em
     * memória aquando da invocação do método.
     * @return : retorna verdadeiro se não existir uma categoria com o nome
     * proposto e falso caso exista uma categoria com o mesmo nome.
     *
     * Método deprecado neste ecrã mas em uso noutros.<br>
     */
    public static boolean isUniqueNomeCategoria(String nomeCategoria, List<categoria> categorias) {
        boolean nomeCatOK = false;
        String propostanomeCategoria = nomeCategoria.trim().toLowerCase();
        String nomeCategoriaExistente = "";
        int count = 0;
        if (categorias.isEmpty()) { // para o caso da lista de categorias estar vazia
            count = 2;
        } else {
            for (categoria categoria : categorias) {
                nomeCategoriaExistente = categoria.getNomeCategoria().trim().toLowerCase();
                if (propostanomeCategoria.equals(nomeCategoriaExistente)) {
                    count = 1;
                    break;
                } else if (!propostanomeCategoria.equals(nomeCategoriaExistente)) {
                    count = 2;
                }
            }

        }

        switch (count) {
            case 1:
                nomeCatOK = false;
                break;
            case 2:
                nomeCatOK = true;
                break;
            default:
                mensagemErroFatal();
                break;
        }

        return nomeCatOK;

    }

    /**
     * Método para verificar se já existe um produto com a mesma descrição na
     * BD.<br>
     * Método recebe o texto da descrição bem como a lista de categorias
     * existentes.Caso a lista esteja vazia, a descrição é única à
     * partida.<br>Caso tenha elementos tem de prosseguir com a validação
     * começando por remover os espaços e colocar em minúsculas o texto da
     * descrição.<br>Após isso, percorre a lista de categorias e para a
     * descrição de cada uma destas remove os espaços e coloca em minúsculas
     * também.<br>Nesta altura, procura fazer uma correspondência entre a nova
     * descrição de categoria e a categoria da lista em causa.<br>Caso encontre
     * uma correspondência ou um valor esquisito, vai retornar falso e caso não
     * encontre correspondência, vai retornar verdadeiro.<br>
     *
     * @param descricaoCategoria : texto da textarea descricaoCategoriaNova
     * correspondente à descrição da nova categoria.
     * @param categorias : lista de categorias existentes na bd já carregada em
     * memória aquando da invocação do método.
     * @return : retorna verdadeiro se não existir uma categoria com a descrição
     * proposta e falso caso exista uma categoria com a mesma descrição.
     *
     * Método deprecado neste ecrã mas em uso noutros<br>
     */
    public static boolean isUniqueDescricaoCategoria(String descricaoCategoria, List<categoria> categorias) {
        boolean descrCatOK = false;
        String propostadescrCategoria = descricaoCategoria.trim().toLowerCase();
        String descrCategoriaExistente = "";
        int count = 0;
        if (categorias.isEmpty()) { // para o caso da lista de categorias estar vazia
            count = 2;
        } else {
            for (categoria categoria : categorias) {
                descrCategoriaExistente = categoria.getDescricaoCategoria().trim().toLowerCase();
                if (propostadescrCategoria.equals(descrCategoriaExistente)) {
                    count = 1;
                    break;
                } else if (!propostadescrCategoria.equals(descrCategoriaExistente)) {
                    count = 2;
                }
            }
        }

        switch (count) {
            case 1:
                descrCatOK = false;
                break;
            case 2:
                descrCatOK = true;
                break;
            default:
                mensagemErroFatal();
                break;
        }
        return descrCatOK;
    }

    /**
     * Método para verificar se uma categoria está ativa e notificar utilizador
     * do mesmo.<br>
     * Método usado em conjunção com o listener de validação do nome da
     * categoria nova.<br>
     * Método recebe o nome de categoria inserido pelo utilizador e o estado da
     * categoria que se pretende avaliar (ativo nesta logica).<br> Depois
     * verifica se este é único por segurança ao mesmo tempo que verifica o
     * estado da categoria.<br> Caso a categoria esteja inativa, o método
     * retorna falso (que é o uso pretendido) e atribui o id da categoria a uma
     * variável da classe que é usado num método usado em conjunção com este (no
     * uso preferencial) para adicionar a categoria à lista de categorias
     * apresentadas no ecrã.<br>O método retorna verdadeiro caso a categoria
     * seja ativa e falsa noutros casos imprevistos.<br>
     *
     * @param nomeCategoria : nome da categoria inativa.
     * @param categorias : lista de categorias existentes na bd e em memória.
     * @param estado : estado da categoria que se pretende testar - ativo
     * idealmente.
     * @return : retorna verdadeiro caso a categoria esteja ativa e falso caso
     * não esteja ou tenha corrido algum problema.<br> Quando retorna falso para
     * categoria inativa, também atribui um id de categoria a uma variavel que
     * depois é validada e usada para eventualmente identificar a categoria para
     * esta ser mostrada no ecrã.
     *
     * Método deprecado aqui mas em uso noutros ecrãs.<br>
     */
    public static boolean isAtiveCategoria(String nomeCategoria, List<categoria> categorias, String estado) {
        boolean estadoAtivoOK = false;
        String propostanomeCategoria = nomeCategoria.trim().toLowerCase();
        String nomeCategoriaExistente = "";
        int count = 0;
        for (categoria categoria : categorias) {
            nomeCategoriaExistente = categoria.getNomeCategoria().trim().toLowerCase();
            if (propostanomeCategoria.equals(nomeCategoriaExistente) && !categoria.getEstado().equals(estado)) {
                count = 1;
            } else if (propostanomeCategoria.equals(nomeCategoriaExistente) && categoria.getEstado().equals(estado)) {
                count = 2;
            }
        }

        switch (count) {
            case 1:
                estadoAtivoOK = false;
                break;
            case 2:
                estadoAtivoOK = true;
                break;
            default:
                estadoAtivoOK = false;
                break;
        }
        return estadoAtivoOK;
    }

    /**
     * Método para lançar uma mensagem de erro e recarregar o ecrã caso algo
     * inesperado ocorra com o algoritmo.<br>
     * Método usado quando as falhas são graves mas não graves de mais que a
     * funcionalidade se encontre inutilizável.<br>
     * É um das mensageens de erro de severidade mais leve neste ecrã.<br>
     */
    public void mensagemErroRecarregarEcra() {
        Alert alertaNovo = new Alert(Alert.AlertType.NONE);
        alertaNovo.setAlertType(Alert.AlertType.ERROR);
        alertaNovo.getButtonTypes().setAll(ButtonType.OK);
        alertaNovo.setTitle("ERRO Lógica Aplicação");
        alertaNovo.setHeaderText("Os dados processados não são válidos");
        alertaNovo.setContentText("Ocorreu um erro ao validar os dados selecionados ou estes podem\nestar inacessíveis. "
                + "Tente novamente e caso não conseguir contacte o administrador");
        Optional<ButtonType> resulta = alertaNovo.showAndWait();
        if (resulta.get() == ButtonType.OK) {
            recarregarEcra();
        }

    }

    /**
     * Método para recarregar o ecrã.<br>
     */
    private static void recarregarEcra() {
        alterarProdutos altPPC = new alterarProdutos();
        fecha();
        try {
            altPPC.start(new Stage());
        } catch (Exception ex) {
            mensagemErroFatal();
        }
    }

    /**
     * Mensagem de erro para tentar fechar em segurança a aplicação no caso
     * desta encontrar um erro muito grave.<br>
     * Se este erro for lançado é porque algo inesperado ocorreu (ex: falha de
     * acesso à base de dados, lançamento de alguns excepções em métodos
     * críticos ou falhas gerais e básicas de programação).<br>
     * É a mensagem de erro de severidade mais elevada deste ecrã.<br>
     */
    public static void mensagemErroFatal() {
        Alert a = new Alert(Alert.AlertType.NONE);
        a.setAlertType(Alert.AlertType.ERROR);
        a.setTitle("ERRO Fatal");
        a.setHeaderText("A aplicação comportou-se de forma inesperada!");
        a.setContentText("Encerrando a aplicação");
        a.getButtonTypes().setAll(ButtonType.OK);
        Optional<ButtonType> result3 = a.showAndWait();
        if (result3.get() == ButtonType.OK) {
            System.exit(0);
        }
    }

    /**
     * Método para lançar uma mensagem de erro caso um erro caso algo essencial
     * falhe na funcionalidade.<br>
     * Se este erro é lançado é porque a funcionalidade pode estar inoperável
     * temporaria ou permanentemente.<br>
     * É mensagem de erro de severidade intermédia do ecrã.<br>
     */
    public void mensagemErroPassagemMenUtilizador() {
        Alert ab = new Alert(Alert.AlertType.NONE);
        ab.setAlertType(Alert.AlertType.ERROR);
        ab.setTitle("ERRO Fatal");
        ab.setHeaderText("Impossível validar os dados do ecrã!");
        ab.setContentText("Algumas funcionalidades do aplicação estão inacessíveis");
        ab.getButtonTypes().setAll(ButtonType.OK);
        Optional<ButtonType> result3b = ab.showAndWait();
        if (result3b.get() == ButtonType.OK) {
            abrirMenUtilizador();
        }
    }

    /**
     * Método para lançar uam mensagem de erro e recarregar o ecrã imediatamente
     * caso algo inesperado ocorra com o algoritmo.<br>
     * Método usado quando as falhas são graves mas não graves de mais que a
     * funcionalidade se encontre inutilizável mas que requira quebra imediata
     * do método onde é invocado.<br>
     * É um das mensageens de erro de severidade mais leve neste ecrã.<br>
     */
    public static void mensagemErroRecarregarEcraSemParar() {
        Alert alertaNovo = new Alert(Alert.AlertType.NONE);
        alertaNovo.setAlertType(Alert.AlertType.ERROR);
        alertaNovo.getButtonTypes().setAll(ButtonType.OK);
        alertaNovo.setTitle("ERRO Lógica Aplicação");
        alertaNovo.setHeaderText("Os dados processados não são válidos");
        alertaNovo.setContentText("Ocorreu um erro ao validar os dados selecionados ou estes podem\nestar inacessíveis. "
                + "Tente novamente e caso não conseguir contacte o administrador");
        alertaNovo.show();
        recarregarEcra();

    }

    /**
     * Método para retornar o id do tipo/categoria do produto a partir do seu
     * nome<br>
     * Método procura na BD por uma categoria pelo seu nome e retorna o seu ID
     * na forma de inteiro.<br> Caso não encontre ou antes disso, caso o nome da
     * categoria corresponda ao string equivalente ao id zero de sem
     * categoria/tipo de produto, retorna logo zero.<br>
     *
     * A validação do id é feita posteriormente no método do botão de alterar o
     * produto para não permitir a alteração do id da categoria para zero no
     * atributo do produto na base de dados.<br>
     *
     * @param nomeCat : nome da categoria a pesquisar.
     * @param semCategoria : placeholder de para valor sem tipo de categoria.
     * @return : retorna um inteiro diferente de zero caso encontre o id da
     * categoria do produto e retorna zero caso o produto não tenha
     * tipo/categoria associado, caso o nome se refira à ausência de
     * categoria/tipo e caso corra mal.
     */
    public static int getIDCategoria(String nomeCat, String semCategoria) {
        int idCat = 0;
        try {
            if (!nomeCat.equals(semCategoria)) {
                List<categoria> categoriaAtualizar = procurarCategoriaNome(nomeCat);
                if (!categoriaAtualizar.isEmpty()) {
                    for (categoria categoria : categoriaAtualizar) {
                        if (categoria.getNomeCategoria().equals(nomeCat)) {
                            idCat = categoria.getIdCategoria();
                            break;
                        } else {
                            idCat = 0;
                        }
                    }
                } else {
                    idCat = 0;
                }
            }
        } catch (Exception azc) {
            idCat = 0;
        }

        return idCat;
    }

    /**
     * Método para validar o estado do produto.<br>
     * Método verifica se há problema ou não com o estado selecionado e notifica
     * o utilizador em caso de problemas.<br>
     */
    @FXML
    private void validarEstado(MouseEvent event) {
        try {

            if (cbestado.getValue().equals("") || cbestado.getValue() == null) {
                a.setAlertType(Alert.AlertType.WARNING);
                a.getButtonTypes().setAll(ButtonType.OK);
                a.setTitle("Estado do Produto Inválido");
                a.setHeaderText("O estado do produto assumiu um valor inválido");
                a.setContentText("Retornando ao estado inicial");
                Optional<ButtonType> result = a.showAndWait();
                if (result.get() == ButtonType.OK) {
                    cbestado.setValue(produtoBase.getEstado());
                }
            }
        } catch (Exception estadoP) {
            mensagemErroRecarregarEcraSemParar();
        }
    }

    /**
     * Método para validar o tipo/categoria do produto.<br>
     * Método verifica se há problema ou não com o tipo/categoria de produto
     * selecionado e notifica o utilizador em caso de problemas.<br>
     */
    @FXML
    private void validarTipo(MouseEvent event) {

        try {
            if (cbCategoria.getValue().equals("") || cbCategoria.getValue() == null) {
                a.setAlertType(Alert.AlertType.WARNING);
                a.getButtonTypes().setAll(ButtonType.OK);
                a.setTitle("Tipo de Produto Inválido");
                a.setHeaderText("O tipo do produto assumiu um valor inválido");
                a.setContentText("Retornando ao estado inicial");
                Optional<ButtonType> result = a.showAndWait();
                if (result.get() == ButtonType.OK) {
                    cbCategoria.setValue(nomeCatProd);
                }
            }
        } catch (Exception categoriaTipoP) {
            mensagemErroRecarregarEcraSemParar();
        }

    }
}
